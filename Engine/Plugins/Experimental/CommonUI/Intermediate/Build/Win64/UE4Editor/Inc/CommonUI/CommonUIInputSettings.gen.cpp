// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Input/CommonUIInputSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUIInputSettings() {}
// Cross Module References
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FCommonAnalogCursorSettings();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FUIInputAction();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FUIActionTag();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FUIActionKeyMapping();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIInputSettings_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIInputSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FCommonAnalogCursorSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings, Z_Construct_UPackage__Script_CommonUI(), TEXT("CommonAnalogCursorSettings"), sizeof(FCommonAnalogCursorSettings), Get_Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FCommonAnalogCursorSettings>()
{
	return FCommonAnalogCursorSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonAnalogCursorSettings(FCommonAnalogCursorSettings::StaticStruct, TEXT("/Script/CommonUI"), TEXT("CommonAnalogCursorSettings"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFCommonAnalogCursorSettings
{
	FScriptStruct_CommonUI_StaticRegisterNativesFCommonAnalogCursorSettings()
	{
		UScriptStruct::DeferCppStructOps<FCommonAnalogCursorSettings>(FName(TEXT("CommonAnalogCursorSettings")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFCommonAnalogCursorSettings;
	struct Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreprocessorPriority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PreprocessorPriority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableCursorAcceleration_MetaData[];
#endif
		static void NewProp_bEnableCursorAcceleration_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableCursorAcceleration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CursorAcceleration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CursorAcceleration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CursorMaxSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CursorMaxSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CursorDeadZone_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CursorDeadZone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoverSlowdownFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HoverSlowdownFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScrollDeadZone_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScrollDeadZone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScrollUpdatePeriod_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScrollUpdatePeriod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScrollMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScrollMultiplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonAnalogCursorSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_PreprocessorPriority_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** The registration priority of the analog cursor preprocessor. */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "The registration priority of the analog cursor preprocessor." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_PreprocessorPriority = { "PreprocessorPriority", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, PreprocessorPriority), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_PreprocessorPriority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_PreprocessorPriority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration_SetBit(void* Obj)
	{
		((FCommonAnalogCursorSettings*)Obj)->bEnableCursorAcceleration = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration = { "bEnableCursorAcceleration", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonAnalogCursorSettings), &Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorAcceleration_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "EditCondition", "bEnableAnalogCursorAcceleration" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorAcceleration = { "CursorAcceleration", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, CursorAcceleration), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorAcceleration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorAcceleration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorMaxSpeed_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorMaxSpeed = { "CursorMaxSpeed", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, CursorMaxSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorMaxSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorMaxSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorDeadZone_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ClampMax", "0.9" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorDeadZone = { "CursorDeadZone", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, CursorDeadZone), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorDeadZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorDeadZone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_HoverSlowdownFactor_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_HoverSlowdownFactor = { "HoverSlowdownFactor", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, HoverSlowdownFactor), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_HoverSlowdownFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_HoverSlowdownFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollDeadZone_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ClampMax", "0.9" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollDeadZone = { "ScrollDeadZone", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, ScrollDeadZone), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollDeadZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollDeadZone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollUpdatePeriod_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollUpdatePeriod = { "ScrollUpdatePeriod", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, ScrollUpdatePeriod), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollUpdatePeriod_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollUpdatePeriod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollMultiplier_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollMultiplier = { "ScrollMultiplier", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonAnalogCursorSettings, ScrollMultiplier), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollMultiplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_PreprocessorPriority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_bEnableCursorAcceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorAcceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorMaxSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_CursorDeadZone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_HoverSlowdownFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollDeadZone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollUpdatePeriod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::NewProp_ScrollMultiplier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		nullptr,
		&NewStructOps,
		"CommonAnalogCursorSettings",
		sizeof(FCommonAnalogCursorSettings),
		alignof(FCommonAnalogCursorSettings),
		Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonAnalogCursorSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonAnalogCursorSettings"), sizeof(FCommonAnalogCursorSettings), Get_Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Hash() { return 1259778880U; }
class UScriptStruct* FUIInputAction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FUIInputAction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUIInputAction, Z_Construct_UPackage__Script_CommonUI(), TEXT("UIInputAction"), sizeof(FUIInputAction), Get_Z_Construct_UScriptStruct_FUIInputAction_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FUIInputAction>()
{
	return FUIInputAction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUIInputAction(FUIInputAction::StaticStruct, TEXT("/Script/CommonUI"), TEXT("UIInputAction"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFUIInputAction
{
	FScriptStruct_CommonUI_StaticRegisterNativesFUIInputAction()
	{
		UScriptStruct::DeferCppStructOps<FUIInputAction>(FName(TEXT("UIInputAction")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFUIInputAction;
	struct Z_Construct_UScriptStruct_FUIInputAction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionTag_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActionTag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DefaultDisplayName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyMappings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyMappings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KeyMappings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIInputAction_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUIInputAction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUIInputAction>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_ActionTag_MetaData[] = {
		{ "Category", "UI Input Action" },
		{ "Comment", "/** The UI.Action tag that acts as the universal identifier of this action. */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "The UI.Action tag that acts as the universal identifier of this action." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_ActionTag = { "ActionTag", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUIInputAction, ActionTag), Z_Construct_UScriptStruct_FUIActionTag, METADATA_PARAMS(Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_ActionTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_ActionTag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_DefaultDisplayName_MetaData[] = {
		{ "Category", "UI Input Action" },
		{ "Comment", "/** \n\x09 * Whenever a UI input action is bound, an override display name can optionally be provided.\n\x09 * This is the default generic display name of this action for use in the absence of such an override.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "Whenever a UI input action is bound, an override display name can optionally be provided.\nThis is the default generic display name of this action for use in the absence of such an override." },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_DefaultDisplayName = { "DefaultDisplayName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUIInputAction, DefaultDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_DefaultDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_DefaultDisplayName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings_Inner = { "KeyMappings", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUIActionKeyMapping, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings_MetaData[] = {
		{ "Category", "UI Input Action" },
		{ "Comment", "/** All key mappings that will trigger this action */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "TitleProperty", "Key" },
		{ "ToolTip", "All key mappings that will trigger this action" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings = { "KeyMappings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUIInputAction, KeyMappings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUIInputAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_ActionTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_DefaultDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUIInputAction_Statics::NewProp_KeyMappings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUIInputAction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		nullptr,
		&NewStructOps,
		"UIInputAction",
		sizeof(FUIInputAction),
		alignof(FUIInputAction),
		Z_Construct_UScriptStruct_FUIInputAction_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIInputAction_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUIInputAction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIInputAction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUIInputAction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUIInputAction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UIInputAction"), sizeof(FUIInputAction), Get_Z_Construct_UScriptStruct_FUIInputAction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUIInputAction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUIInputAction_Hash() { return 1735964762U; }
class UScriptStruct* FUIActionKeyMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FUIActionKeyMapping_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUIActionKeyMapping, Z_Construct_UPackage__Script_CommonUI(), TEXT("UIActionKeyMapping"), sizeof(FUIActionKeyMapping), Get_Z_Construct_UScriptStruct_FUIActionKeyMapping_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FUIActionKeyMapping>()
{
	return FUIActionKeyMapping::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUIActionKeyMapping(FUIActionKeyMapping::StaticStruct, TEXT("/Script/CommonUI"), TEXT("UIActionKeyMapping"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFUIActionKeyMapping
{
	FScriptStruct_CommonUI_StaticRegisterNativesFUIActionKeyMapping()
	{
		UScriptStruct::DeferCppStructOps<FUIActionKeyMapping>(FName(TEXT("UIActionKeyMapping")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFUIActionKeyMapping;
	struct Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoldTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HoldTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUIActionKeyMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "UI Action Key Mapping" },
		{ "Comment", "/** A key that triggers this action */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "A key that triggers this action" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUIActionKeyMapping, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_HoldTime_MetaData[] = {
		{ "Category", "UI Action Key Mapping" },
		{ "Comment", "/** How long must the key be held down for the action to be executed? */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "How long must the key be held down for the action to be executed?" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_HoldTime = { "HoldTime", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUIActionKeyMapping, HoldTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_HoldTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_HoldTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::NewProp_HoldTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		nullptr,
		&NewStructOps,
		"UIActionKeyMapping",
		sizeof(FUIActionKeyMapping),
		alignof(FUIActionKeyMapping),
		Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUIActionKeyMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUIActionKeyMapping_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UIActionKeyMapping"), sizeof(FUIActionKeyMapping), Get_Z_Construct_UScriptStruct_FUIActionKeyMapping_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUIActionKeyMapping_Hash() { return 1439502142U; }
	void UCommonUIInputSettings::StaticRegisterNativesUCommonUIInputSettings()
	{
	}
	UClass* Z_Construct_UClass_UCommonUIInputSettings_NoRegister()
	{
		return UCommonUIInputSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUIInputSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLinkCursorToGamepadFocus_MetaData[];
#endif
		static void NewProp_bLinkCursorToGamepadFocus_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLinkCursorToGamepadFocus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UIActionProcessingPriority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UIActionProcessingPriority;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputActions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActionOverrides_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActionOverrides;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnalogCursorSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AnalogCursorSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUIInputSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Project-wide input settings for UI input actions */" },
		{ "IncludePath", "Input/CommonUIInputSettings.h" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "Project-wide input settings for UI input actions" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** True to have the mouse pointer automatically moved to the center of whatever widget is currently focused while using a gamepad. */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "True to have the mouse pointer automatically moved to the center of whatever widget is currently focused while using a gamepad." },
	};
#endif
	void Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus_SetBit(void* Obj)
	{
		((UCommonUIInputSettings*)Obj)->bLinkCursorToGamepadFocus = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus = { "bLinkCursorToGamepadFocus", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonUIInputSettings), &Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_UIActionProcessingPriority_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** \n\x09 * The input priority of the input components that process UI input actions.\n\x09 * The lower the value, the higher the priority of the component.\n\x09 *\n\x09 * By default, this value is incredibly high to ensure UI action processing priority over game elements.\n\x09 * Adjust as needed for the UI input components to be processed at the appropriate point in the input stack in your project.\n\x09 *\n\x09 * NOTE: When the active input mode is ECommonInputMode::Menu, ALL input components with lower priority than this will be fully blocked.\n\x09 *\x09\x09 Thus, if any game agent input components are registered with higher priority than this, behavior will not match expectation.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "The input priority of the input components that process UI input actions.\nThe lower the value, the higher the priority of the component.\n\nBy default, this value is incredibly high to ensure UI action processing priority over game elements.\nAdjust as needed for the UI input components to be processed at the appropriate point in the input stack in your project.\n\nNOTE: When the active input mode is ECommonInputMode::Menu, ALL input components with lower priority than this will be fully blocked.\n              Thus, if any game agent input components are registered with higher priority than this, behavior will not match expectation." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_UIActionProcessingPriority = { "UIActionProcessingPriority", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIInputSettings, UIActionProcessingPriority), METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_UIActionProcessingPriority_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_UIActionProcessingPriority_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions_Inner = { "InputActions", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUIInputAction, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions_MetaData[] = {
		{ "Category", "Actions" },
		{ "Comment", "/** All UI input action mappings for the project */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "TitleProperty", "ActionTag" },
		{ "ToolTip", "All UI input action mappings for the project" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions = { "InputActions", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIInputSettings, InputActions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides_Inner = { "ActionOverrides", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUIInputAction, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides_MetaData[] = {
		{ "Comment", "/** Config-only set of input action overrides - if an entry for a given action is both here and in the InputActions array, this entry wins completely. */" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
		{ "ToolTip", "Config-only set of input action overrides - if an entry for a given action is both here and in the InputActions array, this entry wins completely." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides = { "ActionOverrides", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIInputSettings, ActionOverrides), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_AnalogCursorSettings_MetaData[] = {
		{ "Category", "AnalogCursor" },
		{ "ModuleRelativePath", "Public/Input/CommonUIInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_AnalogCursorSettings = { "AnalogCursorSettings", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIInputSettings, AnalogCursorSettings), Z_Construct_UScriptStruct_FCommonAnalogCursorSettings, METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_AnalogCursorSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_AnalogCursorSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonUIInputSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_bLinkCursorToGamepadFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_UIActionProcessingPriority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_InputActions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_ActionOverrides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputSettings_Statics::NewProp_AnalogCursorSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUIInputSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUIInputSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUIInputSettings_Statics::ClassParams = {
		&UCommonUIInputSettings::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonUIInputSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::PropPointers),
		0,
		0x000800A6u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUIInputSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUIInputSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUIInputSettings, 655417901);
	template<> COMMONUI_API UClass* StaticClass<UCommonUIInputSettings>()
	{
		return UCommonUIInputSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUIInputSettings(Z_Construct_UClass_UCommonUIInputSettings, &UCommonUIInputSettings::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonUIInputSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUIInputSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
