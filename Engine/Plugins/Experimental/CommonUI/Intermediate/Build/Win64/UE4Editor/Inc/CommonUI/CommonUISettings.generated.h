// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonUISettings_generated_h
#error "CommonUISettings.generated.h already included, missing '#pragma once' in CommonUISettings.h"
#endif
#define COMMONUI_CommonUISettings_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUISettings(); \
	friend struct Z_Construct_UClass_UCommonUISettings_Statics; \
public: \
	DECLARE_CLASS(UCommonUISettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUISettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUISettings(); \
	friend struct Z_Construct_UClass_UCommonUISettings_Statics; \
public: \
	DECLARE_CLASS(UCommonUISettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUISettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUISettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUISettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUISettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUISettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUISettings(UCommonUISettings&&); \
	NO_API UCommonUISettings(const UCommonUISettings&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUISettings(UCommonUISettings&&); \
	NO_API UCommonUISettings(const UCommonUISettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUISettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUISettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUISettings)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAutoLoadData() { return STRUCT_OFFSET(UCommonUISettings, bAutoLoadData); } \
	FORCEINLINE static uint32 __PPO__DefaultImageResourceObject() { return STRUCT_OFFSET(UCommonUISettings, DefaultImageResourceObject); } \
	FORCEINLINE static uint32 __PPO__DefaultThrobberMaterial() { return STRUCT_OFFSET(UCommonUISettings, DefaultThrobberMaterial); } \
	FORCEINLINE static uint32 __PPO__DefaultRichTextDataClass() { return STRUCT_OFFSET(UCommonUISettings, DefaultRichTextDataClass); } \
	FORCEINLINE static uint32 __PPO__DefaultImageResourceObjectInstance() { return STRUCT_OFFSET(UCommonUISettings, DefaultImageResourceObjectInstance); } \
	FORCEINLINE static uint32 __PPO__DefaultThrobberMaterialInstance() { return STRUCT_OFFSET(UCommonUISettings, DefaultThrobberMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__DefaultThrobberBrush() { return STRUCT_OFFSET(UCommonUISettings, DefaultThrobberBrush); } \
	FORCEINLINE static uint32 __PPO__RichTextDataInstance() { return STRUCT_OFFSET(UCommonUISettings, RichTextDataInstance); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_18_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUISettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUISettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
