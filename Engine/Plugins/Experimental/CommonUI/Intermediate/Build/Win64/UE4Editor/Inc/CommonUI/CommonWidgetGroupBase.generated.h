// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWidget;
#ifdef COMMONUI_CommonWidgetGroupBase_generated_h
#error "CommonWidgetGroupBase.generated.h already included, missing '#pragma once' in CommonWidgetGroupBase.h"
#endif
#define COMMONUI_CommonWidgetGroupBase_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRemoveAll); \
	DECLARE_FUNCTION(execRemoveWidget); \
	DECLARE_FUNCTION(execAddWidget);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRemoveAll); \
	DECLARE_FUNCTION(execRemoveWidget); \
	DECLARE_FUNCTION(execAddWidget);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonWidgetGroupBase(); \
	friend struct Z_Construct_UClass_UCommonWidgetGroupBase_Statics; \
public: \
	DECLARE_CLASS(UCommonWidgetGroupBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonWidgetGroupBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUCommonWidgetGroupBase(); \
	friend struct Z_Construct_UClass_UCommonWidgetGroupBase_Statics; \
public: \
	DECLARE_CLASS(UCommonWidgetGroupBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonWidgetGroupBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonWidgetGroupBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonWidgetGroupBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonWidgetGroupBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonWidgetGroupBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonWidgetGroupBase(UCommonWidgetGroupBase&&); \
	NO_API UCommonWidgetGroupBase(const UCommonWidgetGroupBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonWidgetGroupBase(UCommonWidgetGroupBase&&); \
	NO_API UCommonWidgetGroupBase(const UCommonWidgetGroupBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonWidgetGroupBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonWidgetGroupBase); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UCommonWidgetGroupBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_10_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonWidgetGroupBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Groups_CommonWidgetGroupBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
