// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonVisibilitySwitcherSlot.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonVisibilitySwitcherSlot() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonVisibilitySwitcherSlot_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonVisibilitySwitcherSlot();
	UMG_API UClass* Z_Construct_UClass_UOverlaySlot();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
// End Cross Module References
	void UCommonVisibilitySwitcherSlot::StaticRegisterNativesUCommonVisibilitySwitcherSlot()
	{
	}
	UClass* Z_Construct_UClass_UCommonVisibilitySwitcherSlot_NoRegister()
	{
		return UCommonVisibilitySwitcherSlot::StaticClass();
	}
	struct Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOverlaySlot,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CommonVisibilitySwitcherSlot.h" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcherSlot.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonVisibilitySwitcherSlot>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::ClassParams = {
		&UCommonVisibilitySwitcherSlot::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonVisibilitySwitcherSlot()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonVisibilitySwitcherSlot, 2041575016);
	template<> COMMONUI_API UClass* StaticClass<UCommonVisibilitySwitcherSlot>()
	{
		return UCommonVisibilitySwitcherSlot::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonVisibilitySwitcherSlot(Z_Construct_UClass_UCommonVisibilitySwitcherSlot, &UCommonVisibilitySwitcherSlot::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonVisibilitySwitcherSlot"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonVisibilitySwitcherSlot);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
