// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonUIInputSettings_generated_h
#error "CommonUIInputSettings.generated.h already included, missing '#pragma once' in CommonUIInputSettings.h"
#endif
#define COMMONUI_CommonUIInputSettings_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonAnalogCursorSettings_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct();


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonAnalogCursorSettings>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUIInputAction_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct();


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FUIInputAction>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUIActionKeyMapping_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct();


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FUIActionKeyMapping>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUIInputSettings(); \
	friend struct Z_Construct_UClass_UCommonUIInputSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonUIInputSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), COMMONUI_API) \
	DECLARE_SERIALIZER(UCommonUIInputSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Input");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUIInputSettings(); \
	friend struct Z_Construct_UClass_UCommonUIInputSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonUIInputSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), COMMONUI_API) \
	DECLARE_SERIALIZER(UCommonUIInputSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Input");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMMONUI_API UCommonUIInputSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIInputSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMMONUI_API, UCommonUIInputSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIInputSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMMONUI_API UCommonUIInputSettings(UCommonUIInputSettings&&); \
	COMMONUI_API UCommonUIInputSettings(const UCommonUIInputSettings&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMMONUI_API UCommonUIInputSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMMONUI_API UCommonUIInputSettings(UCommonUIInputSettings&&); \
	COMMONUI_API UCommonUIInputSettings(const UCommonUIInputSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMMONUI_API, UCommonUIInputSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIInputSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIInputSettings)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bLinkCursorToGamepadFocus() { return STRUCT_OFFSET(UCommonUIInputSettings, bLinkCursorToGamepadFocus); } \
	FORCEINLINE static uint32 __PPO__UIActionProcessingPriority() { return STRUCT_OFFSET(UCommonUIInputSettings, UIActionProcessingPriority); } \
	FORCEINLINE static uint32 __PPO__InputActions() { return STRUCT_OFFSET(UCommonUIInputSettings, InputActions); } \
	FORCEINLINE static uint32 __PPO__ActionOverrides() { return STRUCT_OFFSET(UCommonUIInputSettings, ActionOverrides); } \
	FORCEINLINE static uint32 __PPO__AnalogCursorSettings() { return STRUCT_OFFSET(UCommonUIInputSettings, AnalogCursorSettings); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_106_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h_109_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUIInputSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIInputSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
