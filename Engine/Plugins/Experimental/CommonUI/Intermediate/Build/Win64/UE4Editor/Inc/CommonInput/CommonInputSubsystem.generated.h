// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ECommonInputType : uint8;
#ifdef COMMONINPUT_CommonInputSubsystem_generated_h
#error "CommonInputSubsystem.generated.h already included, missing '#pragma once' in CommonInputSubsystem.h"
#endif
#define COMMONINPUT_CommonInputSubsystem_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_15_DELEGATE \
struct _Script_CommonInput_eventInputMethodChangedDelegate_Parms \
{ \
	ECommonInputType bNewInputType; \
}; \
static inline void FInputMethodChangedDelegate_DelegateWrapper(const FMulticastScriptDelegate& InputMethodChangedDelegate, ECommonInputType bNewInputType) \
{ \
	_Script_CommonInput_eventInputMethodChangedDelegate_Parms Parms; \
	Parms.bNewInputType=bNewInputType; \
	InputMethodChangedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execShouldShowInputKeys); \
	DECLARE_FUNCTION(execIsUsingPointerInput); \
	DECLARE_FUNCTION(execSetGamepadInputType); \
	DECLARE_FUNCTION(execGetCurrentGamepadName); \
	DECLARE_FUNCTION(execSetCurrentInputType); \
	DECLARE_FUNCTION(execGetDefaultInputType); \
	DECLARE_FUNCTION(execGetCurrentInputType); \
	DECLARE_FUNCTION(execIsInputMethodActive);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShouldShowInputKeys); \
	DECLARE_FUNCTION(execIsUsingPointerInput); \
	DECLARE_FUNCTION(execSetGamepadInputType); \
	DECLARE_FUNCTION(execGetCurrentGamepadName); \
	DECLARE_FUNCTION(execSetCurrentInputType); \
	DECLARE_FUNCTION(execGetDefaultInputType); \
	DECLARE_FUNCTION(execGetCurrentInputType); \
	DECLARE_FUNCTION(execIsInputMethodActive);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonInputSubsystem(); \
	friend struct Z_Construct_UClass_UCommonInputSubsystem_Statics; \
public: \
	DECLARE_CLASS(UCommonInputSubsystem, ULocalPlayerSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonInputSubsystem)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUCommonInputSubsystem(); \
	friend struct Z_Construct_UClass_UCommonInputSubsystem_Statics; \
public: \
	DECLARE_CLASS(UCommonInputSubsystem, ULocalPlayerSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonInputSubsystem)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonInputSubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonInputSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonInputSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonInputSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonInputSubsystem(UCommonInputSubsystem&&); \
	NO_API UCommonInputSubsystem(const UCommonInputSubsystem&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonInputSubsystem(UCommonInputSubsystem&&); \
	NO_API UCommonInputSubsystem(const UCommonInputSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonInputSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonInputSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonInputSubsystem)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnInputMethodChanged() { return STRUCT_OFFSET(UCommonInputSubsystem, OnInputMethodChanged); } \
	FORCEINLINE static uint32 __PPO__NumberOfInputMethodChangesRecently() { return STRUCT_OFFSET(UCommonInputSubsystem, NumberOfInputMethodChangesRecently); } \
	FORCEINLINE static uint32 __PPO__LastInputMethodChangeTime() { return STRUCT_OFFSET(UCommonInputSubsystem, LastInputMethodChangeTime); } \
	FORCEINLINE static uint32 __PPO__LastTimeInputMethodThrashingBegan() { return STRUCT_OFFSET(UCommonInputSubsystem, LastTimeInputMethodThrashingBegan); } \
	FORCEINLINE static uint32 __PPO__LastInputType() { return STRUCT_OFFSET(UCommonInputSubsystem, LastInputType); } \
	FORCEINLINE static uint32 __PPO__CurrentInputType() { return STRUCT_OFFSET(UCommonInputSubsystem, CurrentInputType); } \
	FORCEINLINE static uint32 __PPO__GamepadInputType() { return STRUCT_OFFSET(UCommonInputSubsystem, GamepadInputType); } \
	FORCEINLINE static uint32 __PPO__CurrentInputLocks() { return STRUCT_OFFSET(UCommonInputSubsystem, CurrentInputLocks); } \
	FORCEINLINE static uint32 __PPO__bIsGamepadSimulatedClick() { return STRUCT_OFFSET(UCommonInputSubsystem, bIsGamepadSimulatedClick); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_17_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONINPUT_API UClass* StaticClass<class UCommonInputSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
