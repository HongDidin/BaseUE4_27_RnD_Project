// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonActionHandlerInterface_generated_h
#error "CommonActionHandlerInterface.generated.h already included, missing '#pragma once' in CommonActionHandlerInterface.h"
#endif
#define COMMONUI_CommonActionHandlerInterface_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_39_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__State() { return STRUCT_OFFSET(FCommonInputActionHandlerData, State); }


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonInputActionHandlerData>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_33_DELEGATE \
struct _Script_CommonUI_eventCommonActionProgress_Parms \
{ \
	float HeldPercent; \
}; \
static inline void FCommonActionProgress_DelegateWrapper(const FMulticastScriptDelegate& CommonActionProgress, float HeldPercent) \
{ \
	_Script_CommonUI_eventCommonActionProgress_Parms Parms; \
	Parms.HeldPercent=HeldPercent; \
	CommonActionProgress.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_31_DELEGATE \
struct _Script_CommonUI_eventCommonActionProgressSingle_Parms \
{ \
	float HeldPercent; \
}; \
static inline void FCommonActionProgressSingle_DelegateWrapper(const FScriptDelegate& CommonActionProgressSingle, float HeldPercent) \
{ \
	_Script_CommonUI_eventCommonActionProgressSingle_Parms Parms; \
	Parms.HeldPercent=HeldPercent; \
	CommonActionProgressSingle.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_24_DELEGATE \
static inline void FCommonActionComplete_DelegateWrapper(const FMulticastScriptDelegate& CommonActionComplete) \
{ \
	CommonActionComplete.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_22_DELEGATE \
static inline void FCommonActionCompleteSingle_DelegateWrapper(const FScriptDelegate& CommonActionCompleteSingle) \
{ \
	CommonActionCompleteSingle.ProcessDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_15_DELEGATE \
struct _Script_CommonUI_eventCommonActionCommited_Parms \
{ \
	bool bPassThrough; \
}; \
static inline void FCommonActionCommited_DelegateWrapper(const FScriptDelegate& CommonActionCommited, bool& bPassThrough) \
{ \
	_Script_CommonUI_eventCommonActionCommited_Parms Parms; \
	Parms.bPassThrough=bPassThrough ? true : false; \
	CommonActionCommited.ProcessDelegate<UObject>(&Parms); \
	bPassThrough=Parms.bPassThrough; \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActionHandlerInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActionHandlerInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActionHandlerInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActionHandlerInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActionHandlerInterface(UCommonActionHandlerInterface&&); \
	NO_API UCommonActionHandlerInterface(const UCommonActionHandlerInterface&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActionHandlerInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActionHandlerInterface(UCommonActionHandlerInterface&&); \
	NO_API UCommonActionHandlerInterface(const UCommonActionHandlerInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActionHandlerInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActionHandlerInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActionHandlerInterface)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUCommonActionHandlerInterface(); \
	friend struct Z_Construct_UClass_UCommonActionHandlerInterface_Statics; \
public: \
	DECLARE_CLASS(UCommonActionHandlerInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActionHandlerInterface)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ICommonActionHandlerInterface() {} \
public: \
	typedef UCommonActionHandlerInterface UClassType; \
	typedef ICommonActionHandlerInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_INCLASS_IINTERFACE \
protected: \
	virtual ~ICommonActionHandlerInterface() {} \
public: \
	typedef UCommonActionHandlerInterface UClassType; \
	typedef ICommonActionHandlerInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_109_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_118_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_118_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h_112_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActionHandlerInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActionHandlerInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
