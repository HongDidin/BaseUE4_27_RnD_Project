// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonUIRichTextData_generated_h
#error "CommonUIRichTextData.generated.h already included, missing '#pragma once' in CommonUIRichTextData.h"
#endif
#define COMMONUI_CommonUIRichTextData_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRichTextIconData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FRichTextIconData>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUIRichTextData(); \
	friend struct Z_Construct_UClass_UCommonUIRichTextData_Statics; \
public: \
	DECLARE_CLASS(UCommonUIRichTextData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIRichTextData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUIRichTextData(); \
	friend struct Z_Construct_UClass_UCommonUIRichTextData_Statics; \
public: \
	DECLARE_CLASS(UCommonUIRichTextData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIRichTextData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUIRichTextData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIRichTextData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIRichTextData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIRichTextData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIRichTextData(UCommonUIRichTextData&&); \
	NO_API UCommonUIRichTextData(const UCommonUIRichTextData&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUIRichTextData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIRichTextData(UCommonUIRichTextData&&); \
	NO_API UCommonUIRichTextData(const UCommonUIRichTextData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIRichTextData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIRichTextData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIRichTextData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__InlineIconSet() { return STRUCT_OFFSET(UCommonUIRichTextData, InlineIconSet); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_29_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUIRichTextData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIRichTextData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
