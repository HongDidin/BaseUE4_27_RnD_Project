// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonBoundActionBar_generated_h
#error "CommonBoundActionBar.generated.h already included, missing '#pragma once' in CommonBoundActionBar.h"
#endif
#define COMMONUI_CommonBoundActionBar_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetDisplayOwningPlayerActionsOnly);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetDisplayOwningPlayerActionsOnly);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonBoundActionBar(); \
	friend struct Z_Construct_UClass_UCommonBoundActionBar_Statics; \
public: \
	DECLARE_CLASS(UCommonBoundActionBar, UDynamicEntryBoxBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBoundActionBar)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonBoundActionBar(); \
	friend struct Z_Construct_UClass_UCommonBoundActionBar_Statics; \
public: \
	DECLARE_CLASS(UCommonBoundActionBar, UDynamicEntryBoxBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBoundActionBar)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBoundActionBar(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBoundActionBar) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBoundActionBar); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBoundActionBar); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBoundActionBar(UCommonBoundActionBar&&); \
	NO_API UCommonBoundActionBar(const UCommonBoundActionBar&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBoundActionBar(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBoundActionBar(UCommonBoundActionBar&&); \
	NO_API UCommonBoundActionBar(const UCommonBoundActionBar&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBoundActionBar); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBoundActionBar); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBoundActionBar)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActionButtonClass() { return STRUCT_OFFSET(UCommonBoundActionBar, ActionButtonClass); } \
	FORCEINLINE static uint32 __PPO__bDisplayOwningPlayerActionsOnly() { return STRUCT_OFFSET(UCommonBoundActionBar, bDisplayOwningPlayerActionsOnly); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_12_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonBoundActionBar>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionBar_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
