// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonInputMode_generated_h
#error "CommonInputMode.generated.h already included, missing '#pragma once' in CommonInputMode.h"
#endif
#define COMMONUI_CommonInputMode_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonInputMode_h


#define FOREACH_ENUM_ECOMMONINPUTMODE(op) \
	op(ECommonInputMode::Menu) \
	op(ECommonInputMode::Game) \
	op(ECommonInputMode::All) 

enum class ECommonInputMode : uint8;
template<> COMMONUI_API UEnum* StaticEnum<ECommonInputMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
