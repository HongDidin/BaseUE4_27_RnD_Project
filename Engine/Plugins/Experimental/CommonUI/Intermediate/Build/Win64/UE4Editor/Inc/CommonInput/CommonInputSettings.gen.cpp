// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonInput/Public/CommonInputSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonInputSettings() {}
// Cross Module References
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonInputSettings_NoRegister();
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonInputSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonInput();
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonUIInputData_NoRegister();
	COMMONINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputPlatformBaseData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	DEFINE_FUNCTION(UCommonInputSettings::execGetRegisteredPlatforms)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FName>*)Z_Param__Result=UCommonInputSettings::GetRegisteredPlatforms();
		P_NATIVE_END;
	}
	void UCommonInputSettings::StaticRegisterNativesUCommonInputSettings()
	{
		UClass* Class = UCommonInputSettings::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetRegisteredPlatforms", &UCommonInputSettings::execGetRegisteredPlatforms },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics
	{
		struct CommonInputSettings_eventGetRegisteredPlatforms_Parms
		{
			TArray<FName> ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000008000582, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonInputSettings_eventGetRegisteredPlatforms_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonInputSettings, nullptr, "GetRegisteredPlatforms", nullptr, nullptr, sizeof(CommonInputSettings_eventGetRegisteredPlatforms_Parms), Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonInputSettings_NoRegister()
	{
		return UCommonInputSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCommonInputSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputData_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_InputData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CommonInputPlatformData_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CommonInputPlatformData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonInputPlatformData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CommonInputPlatformData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableInputMethodThrashingProtection_MetaData[];
#endif
		static void NewProp_bEnableInputMethodThrashingProtection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableInputMethodThrashingProtection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMethodThrashingLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InputMethodThrashingLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMethodThrashingWindowInSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_InputMethodThrashingWindowInSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMethodThrashingCooldownInSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_InputMethodThrashingCooldownInSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowOutOfFocusDeviceInput_MetaData[];
#endif
		static void NewProp_bAllowOutOfFocusDeviceInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowOutOfFocusDeviceInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputDataClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InputDataClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentPlatform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentPlatform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonInputSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonInput,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonInputSettings_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonInputSettings_GetRegisteredPlatforms, "GetRegisteredPlatforms" }, // 4173540630
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CommonInputSettings.h" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputData_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Create a derived asset from UCommonUIInputData to store Input data for your game.*/" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
		{ "ToolTip", "Create a derived asset from UCommonUIInputData to store Input data for your game." },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputData = { "InputData", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, InputData), Z_Construct_UClass_UCommonUIInputData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_ValueProp = { "CommonInputPlatformData", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCommonInputPlatformBaseData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_Key_KeyProp = { "CommonInputPlatformData_Key", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_MetaData[] = {
		{ "Category", "Input" },
		{ "GetOptions", "GetRegisteredPlatforms" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData = { "CommonInputPlatformData", nullptr, (EPropertyFlags)0x0040000000004041, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, CommonInputPlatformData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection_MetaData[] = {
		{ "Category", "Thrashing Settings" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	void Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection_SetBit(void* Obj)
	{
		((UCommonInputSettings*)Obj)->bEnableInputMethodThrashingProtection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection = { "bEnableInputMethodThrashingProtection", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonInputSettings), &Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingLimit_MetaData[] = {
		{ "Category", "Thrashing Settings" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingLimit = { "InputMethodThrashingLimit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, InputMethodThrashingLimit), METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingLimit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingWindowInSeconds_MetaData[] = {
		{ "Category", "Thrashing Settings" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingWindowInSeconds = { "InputMethodThrashingWindowInSeconds", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, InputMethodThrashingWindowInSeconds), METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingWindowInSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingWindowInSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingCooldownInSeconds_MetaData[] = {
		{ "Category", "Thrashing Settings" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingCooldownInSeconds = { "InputMethodThrashingCooldownInSeconds", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, InputMethodThrashingCooldownInSeconds), METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingCooldownInSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingCooldownInSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput_MetaData[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	void Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput_SetBit(void* Obj)
	{
		((UCommonInputSettings*)Obj)->bAllowOutOfFocusDeviceInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput = { "bAllowOutOfFocusDeviceInput", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonInputSettings), &Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputDataClass_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputDataClass = { "InputDataClass", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, InputDataClass), Z_Construct_UClass_UCommonUIInputData_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputDataClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputDataClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CurrentPlatform_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CurrentPlatform = { "CurrentPlatform", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputSettings, CurrentPlatform), Z_Construct_UScriptStruct_FCommonInputPlatformBaseData, METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CurrentPlatform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CurrentPlatform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonInputSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CommonInputPlatformData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bEnableInputMethodThrashingProtection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingWindowInSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputMethodThrashingCooldownInSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_bAllowOutOfFocusDeviceInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_InputDataClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputSettings_Statics::NewProp_CurrentPlatform,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonInputSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonInputSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonInputSettings_Statics::ClassParams = {
		&UCommonInputSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonInputSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonInputSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonInputSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonInputSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonInputSettings, 669896608);
	template<> COMMONINPUT_API UClass* StaticClass<UCommonInputSettings>()
	{
		return UCommonInputSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonInputSettings(Z_Construct_UClass_UCommonInputSettings, &UCommonInputSettings::StaticClass, TEXT("/Script/CommonInput"), TEXT("UCommonInputSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonInputSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
