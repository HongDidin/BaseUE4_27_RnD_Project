// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_UITag_generated_h
#error "UITag.generated.h already included, missing '#pragma once' in UITag.h"
#endif
#define COMMONUI_UITag_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_UITag_h_125_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUIActionTag_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct(); \
	typedef FUITag Super;


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FUIActionTag>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_UITag_h_118_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUITag_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct(); \
	typedef FGameplayTag Super;


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FUITag>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_UITag_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
