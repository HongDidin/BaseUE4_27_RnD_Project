// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Input/CommonUIActionRouterBase.h"
#include "Engine/Classes/Engine/LocalPlayer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUIActionRouterBase() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIActionRouterBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIActionRouterBase();
	ENGINE_API UClass* Z_Construct_UClass_ULocalPlayerSubsystem();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
// End Cross Module References
	void UCommonUIActionRouterBase::StaticRegisterNativesUCommonUIActionRouterBase()
	{
	}
	UClass* Z_Construct_UClass_UCommonUIActionRouterBase_NoRegister()
	{
		return UCommonUIActionRouterBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUIActionRouterBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUIActionRouterBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULocalPlayerSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIActionRouterBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The nucleus of the CommonUI input routing system\n * @todo DanH: Explain what that means more fully\n */" },
		{ "IncludePath", "Input/CommonUIActionRouterBase.h" },
		{ "ModuleRelativePath", "Public/Input/CommonUIActionRouterBase.h" },
		{ "ToolTip", "The nucleus of the CommonUI input routing system\n@todo DanH: Explain what that means more fully" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUIActionRouterBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUIActionRouterBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUIActionRouterBase_Statics::ClassParams = {
		&UCommonUIActionRouterBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUIActionRouterBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIActionRouterBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUIActionRouterBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUIActionRouterBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUIActionRouterBase, 4269216815);
	template<> COMMONUI_API UClass* StaticClass<UCommonUIActionRouterBase>()
	{
		return UCommonUIActionRouterBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUIActionRouterBase(Z_Construct_UClass_UCommonUIActionRouterBase, &UCommonUIActionRouterBase::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonUIActionRouterBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUIActionRouterBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
