// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Input/CommonBoundActionBar.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonBoundActionBar() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBoundActionBar_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBoundActionBar();
	UMG_API UClass* Z_Construct_UClass_UDynamicEntryBoxBase();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBoundActionButton_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UCommonBoundActionBar::execSetDisplayOwningPlayerActionsOnly)
	{
		P_GET_UBOOL(Z_Param_bShouldOnlyDisplayOwningPlayerActions);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDisplayOwningPlayerActionsOnly(Z_Param_bShouldOnlyDisplayOwningPlayerActions);
		P_NATIVE_END;
	}
	void UCommonBoundActionBar::StaticRegisterNativesUCommonBoundActionBar()
	{
		UClass* Class = UCommonBoundActionBar::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetDisplayOwningPlayerActionsOnly", &UCommonBoundActionBar::execSetDisplayOwningPlayerActionsOnly },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics
	{
		struct CommonBoundActionBar_eventSetDisplayOwningPlayerActionsOnly_Parms
		{
			bool bShouldOnlyDisplayOwningPlayerActions;
		};
		static void NewProp_bShouldOnlyDisplayOwningPlayerActions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldOnlyDisplayOwningPlayerActions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::NewProp_bShouldOnlyDisplayOwningPlayerActions_SetBit(void* Obj)
	{
		((CommonBoundActionBar_eventSetDisplayOwningPlayerActionsOnly_Parms*)Obj)->bShouldOnlyDisplayOwningPlayerActions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::NewProp_bShouldOnlyDisplayOwningPlayerActions = { "bShouldOnlyDisplayOwningPlayerActions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonBoundActionBar_eventSetDisplayOwningPlayerActionsOnly_Parms), &Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::NewProp_bShouldOnlyDisplayOwningPlayerActions_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::NewProp_bShouldOnlyDisplayOwningPlayerActions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonBoundActionBar" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionBar.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonBoundActionBar, nullptr, "SetDisplayOwningPlayerActionsOnly", nullptr, nullptr, sizeof(CommonBoundActionBar_eventSetDisplayOwningPlayerActionsOnly_Parms), Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonBoundActionBar_NoRegister()
	{
		return UCommonBoundActionBar::StaticClass();
	}
	struct Z_Construct_UClass_UCommonBoundActionBar_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActionButtonClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActionButtonClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayOwningPlayerActionsOnly_MetaData[];
#endif
		static void NewProp_bDisplayOwningPlayerActionsOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayOwningPlayerActionsOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonBoundActionBar_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDynamicEntryBoxBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonBoundActionBar_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonBoundActionBar_SetDisplayOwningPlayerActionsOnly, "SetDisplayOwningPlayerActionsOnly" }, // 1496452933
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBoundActionBar_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Input/CommonBoundActionBar.h" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionBar.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_ActionButtonClass_MetaData[] = {
		{ "Category", "EntryLayout" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionBar.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_ActionButtonClass = { "ActionButtonClass", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonBoundActionBar, ActionButtonClass), Z_Construct_UClass_UCommonBoundActionButton_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_ActionButtonClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_ActionButtonClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly_MetaData[] = {
		{ "Category", "Display" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionBar.h" },
	};
#endif
	void Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly_SetBit(void* Obj)
	{
		((UCommonBoundActionBar*)Obj)->bDisplayOwningPlayerActionsOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly = { "bDisplayOwningPlayerActionsOnly", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonBoundActionBar), &Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonBoundActionBar_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_ActionButtonClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBoundActionBar_Statics::NewProp_bDisplayOwningPlayerActionsOnly,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonBoundActionBar_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonBoundActionBar>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonBoundActionBar_Statics::ClassParams = {
		&UCommonBoundActionBar::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonBoundActionBar_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionBar_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonBoundActionBar_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionBar_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonBoundActionBar()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonBoundActionBar_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonBoundActionBar, 3317106105);
	template<> COMMONUI_API UClass* StaticClass<UCommonBoundActionBar>()
	{
		return UCommonBoundActionBar::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonBoundActionBar(Z_Construct_UClass_UCommonBoundActionBar, &UCommonBoundActionBar::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonBoundActionBar"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonBoundActionBar);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
