// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonActivatableWidget;
#ifdef COMMONUI_CommonActivatableWidgetContainer_generated_h
#error "CommonActivatableWidgetContainer.generated.h already included, missing '#pragma once' in CommonActivatableWidgetContainer.h"
#endif
#define COMMONUI_CommonActivatableWidgetContainer_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRemoveWidget); \
	DECLARE_FUNCTION(execBP_AddWidget); \
	DECLARE_FUNCTION(execClearWidgets); \
	DECLARE_FUNCTION(execGetActiveWidget);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRemoveWidget); \
	DECLARE_FUNCTION(execBP_AddWidget); \
	DECLARE_FUNCTION(execClearWidgets); \
	DECLARE_FUNCTION(execGetActiveWidget);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetContainerBase(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetContainerBase, UWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetContainerBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetContainerBase(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetContainerBase_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetContainerBase, UWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetContainerBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetContainerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetContainerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetContainerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetContainerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetContainerBase(UCommonActivatableWidgetContainerBase&&); \
	NO_API UCommonActivatableWidgetContainerBase(const UCommonActivatableWidgetContainerBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetContainerBase(UCommonActivatableWidgetContainerBase&&); \
	NO_API UCommonActivatableWidgetContainerBase(const UCommonActivatableWidgetContainerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetContainerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetContainerBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetContainerBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TransitionType() { return STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, TransitionType); } \
	FORCEINLINE static uint32 __PPO__TransitionCurveType() { return STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, TransitionCurveType); } \
	FORCEINLINE static uint32 __PPO__TransitionDuration() { return STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, TransitionDuration); } \
	FORCEINLINE static uint32 __PPO__WidgetList() { return STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, WidgetList); } \
	FORCEINLINE static uint32 __PPO__DisplayedWidget() { return STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, DisplayedWidget); } \
	FORCEINLINE static uint32 __PPO__GeneratedWidgetsPool() { return STRUCT_OFFSET(UCommonActivatableWidgetContainerBase, GeneratedWidgetsPool); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_18_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActivatableWidgetContainerBase>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetStack(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetStack_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetStack, UCommonActivatableWidgetContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetStack)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_INCLASS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetStack(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetStack_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetStack, UCommonActivatableWidgetContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetStack)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetStack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetStack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetStack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetStack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetStack(UCommonActivatableWidgetStack&&); \
	NO_API UCommonActivatableWidgetStack(const UCommonActivatableWidgetStack&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetStack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetStack(UCommonActivatableWidgetStack&&); \
	NO_API UCommonActivatableWidgetStack(const UCommonActivatableWidgetStack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetStack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetStack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetStack)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootContentWidgetClass() { return STRUCT_OFFSET(UCommonActivatableWidgetStack, RootContentWidgetClass); } \
	FORCEINLINE static uint32 __PPO__RootContentWidget() { return STRUCT_OFFSET(UCommonActivatableWidgetStack, RootContentWidget); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_171_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_174_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActivatableWidgetStack>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetQueue(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetQueue, UCommonActivatableWidgetContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetQueue)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_INCLASS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidgetQueue(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidgetQueue_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidgetQueue, UCommonActivatableWidgetContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidgetQueue)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetQueue(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetQueue) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetQueue); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetQueue); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetQueue(UCommonActivatableWidgetQueue&&); \
	NO_API UCommonActivatableWidgetQueue(const UCommonActivatableWidgetQueue&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidgetQueue(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidgetQueue(UCommonActivatableWidgetQueue&&); \
	NO_API UCommonActivatableWidgetQueue(const UCommonActivatableWidgetQueue&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidgetQueue); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidgetQueue); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidgetQueue)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_204_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h_207_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActivatableWidgetQueue>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Widgets_CommonActivatableWidgetContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
