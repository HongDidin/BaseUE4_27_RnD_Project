// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonVideoPlayer_generated_h
#error "CommonVideoPlayer.generated.h already included, missing '#pragma once' in CommonVideoPlayer.h"
#endif
#define COMMONUI_CommonVideoPlayer_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonVideoPlayer(); \
	friend struct Z_Construct_UClass_UCommonVideoPlayer_Statics; \
public: \
	DECLARE_CLASS(UCommonVideoPlayer, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVideoPlayer)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUCommonVideoPlayer(); \
	friend struct Z_Construct_UClass_UCommonVideoPlayer_Statics; \
public: \
	DECLARE_CLASS(UCommonVideoPlayer, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVideoPlayer)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonVideoPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVideoPlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVideoPlayer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVideoPlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVideoPlayer(UCommonVideoPlayer&&); \
	NO_API UCommonVideoPlayer(const UCommonVideoPlayer&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVideoPlayer(UCommonVideoPlayer&&); \
	NO_API UCommonVideoPlayer(const UCommonVideoPlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVideoPlayer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVideoPlayer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVideoPlayer)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Video() { return STRUCT_OFFSET(UCommonVideoPlayer, Video); } \
	FORCEINLINE static uint32 __PPO__MediaPlayer() { return STRUCT_OFFSET(UCommonVideoPlayer, MediaPlayer); } \
	FORCEINLINE static uint32 __PPO__MediaTexture() { return STRUCT_OFFSET(UCommonVideoPlayer, MediaTexture); } \
	FORCEINLINE static uint32 __PPO__VideoMaterial() { return STRUCT_OFFSET(UCommonVideoPlayer, VideoMaterial); } \
	FORCEINLINE static uint32 __PPO__SoundComponent() { return STRUCT_OFFSET(UCommonVideoPlayer, SoundComponent); } \
	FORCEINLINE static uint32 __PPO__VideoBrush() { return STRUCT_OFFSET(UCommonVideoPlayer, VideoBrush); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_19_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonVideoPlayer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVideoPlayer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
