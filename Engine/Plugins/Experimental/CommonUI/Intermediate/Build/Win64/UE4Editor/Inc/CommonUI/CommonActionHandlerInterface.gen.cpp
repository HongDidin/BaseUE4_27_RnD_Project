// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonActionHandlerInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonActionHandlerInterface() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputActionHandlerData();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDataTableRowHandle();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_EInputActionState();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActionHandlerInterface_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActionHandlerInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventCommonActionProgress_Parms
		{
			float HeldPercent;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeldPercent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::NewProp_HeldPercent = { "HeldPercent", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventCommonActionProgress_Parms, HeldPercent), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::NewProp_HeldPercent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "CommonActionProgress__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventCommonActionProgress_Parms), Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_CommonActionProgress__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventCommonActionProgressSingle_Parms
		{
			float HeldPercent;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeldPercent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::NewProp_HeldPercent = { "HeldPercent", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventCommonActionProgressSingle_Parms, HeldPercent), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::NewProp_HeldPercent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * Action progress delegate will tell a listener about the progress of an action being held. The \n * single delegate will be used for binding with a listener that the multicast delegate calls.\n */" },
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
		{ "ToolTip", "Action progress delegate will tell a listener about the progress of an action being held. The\nsingle delegate will be used for binding with a listener that the multicast delegate calls." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "CommonActionProgressSingle__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventCommonActionProgressSingle_Parms), Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_CommonActionProgressSingle__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "CommonActionComplete__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_CommonActionComplete__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * Action complete delegate will tell a listener if a held action completed. The single delegate\n * will be used for binding with a listener that the multicast delegate calls.\n */" },
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
		{ "ToolTip", "Action complete delegate will tell a listener if a held action completed. The single delegate\nwill be used for binding with a listener that the multicast delegate calls." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "CommonActionCompleteSingle__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_CommonActionCompleteSingle__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventCommonActionCommited_Parms
		{
			bool bPassThrough;
		};
		static void NewProp_bPassThrough_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPassThrough;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::NewProp_bPassThrough_SetBit(void* Obj)
	{
		((_Script_CommonUI_eventCommonActionCommited_Parms*)Obj)->bPassThrough = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::NewProp_bPassThrough = { "bPassThrough", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_CommonUI_eventCommonActionCommited_Parms), &Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::NewProp_bPassThrough_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::NewProp_bPassThrough,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** \n * Action committed delegate tells the handler that an action is ready to handle. Return value\n * is used to determine if the action was handled or ignored.\n */" },
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
		{ "ToolTip", "Action committed delegate tells the handler that an action is ready to handle. Return value\nis used to determine if the action was handled or ignored." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "CommonActionCommited__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventCommonActionCommited_Parms), Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_CommonActionCommited__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FCommonInputActionHandlerData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonInputActionHandlerData, Z_Construct_UPackage__Script_CommonUI(), TEXT("CommonInputActionHandlerData"), sizeof(FCommonInputActionHandlerData), Get_Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FCommonInputActionHandlerData>()
{
	return FCommonInputActionHandlerData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonInputActionHandlerData(FCommonInputActionHandlerData::StaticStruct, TEXT("/Script/CommonUI"), TEXT("CommonInputActionHandlerData"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFCommonInputActionHandlerData
{
	FScriptStruct_CommonUI_StaticRegisterNativesFCommonInputActionHandlerData()
	{
		UScriptStruct::DeferCppStructOps<FCommonInputActionHandlerData>(FName(TEXT("CommonInputActionHandlerData")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFCommonInputActionHandlerData;
	struct Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActionRow_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActionRow;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_State_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_State_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_State;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonInputActionHandlerData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_InputActionRow_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_InputActionRow = { "InputActionRow", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionHandlerData, InputActionRow), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_InputActionRow_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_InputActionRow_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State = { "State", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionHandlerData, State), Z_Construct_UEnum_CommonUI_EInputActionState, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_InputActionRow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::NewProp_State,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		nullptr,
		&NewStructOps,
		"CommonInputActionHandlerData",
		sizeof(FCommonInputActionHandlerData),
		alignof(FCommonInputActionHandlerData),
		Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonInputActionHandlerData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonInputActionHandlerData"), sizeof(FCommonInputActionHandlerData), Get_Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonInputActionHandlerData_Hash() { return 497926167U; }
	void UCommonActionHandlerInterface::StaticRegisterNativesUCommonActionHandlerInterface()
	{
	}
	UClass* Z_Construct_UClass_UCommonActionHandlerInterface_NoRegister()
	{
		return UCommonActionHandlerInterface::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActionHandlerInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActionHandlerInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionHandlerInterface_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonActionHandlerInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActionHandlerInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ICommonActionHandlerInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActionHandlerInterface_Statics::ClassParams = {
		&UCommonActionHandlerInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActionHandlerInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionHandlerInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActionHandlerInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActionHandlerInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActionHandlerInterface, 142428584);
	template<> COMMONUI_API UClass* StaticClass<UCommonActionHandlerInterface>()
	{
		return UCommonActionHandlerInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActionHandlerInterface(Z_Construct_UClass_UCommonActionHandlerInterface, &UCommonActionHandlerInterface::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActionHandlerInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActionHandlerInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
