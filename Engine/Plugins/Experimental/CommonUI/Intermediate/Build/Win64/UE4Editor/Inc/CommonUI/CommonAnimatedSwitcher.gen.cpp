// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonAnimatedSwitcher.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonAnimatedSwitcher() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonAnimatedSwitcher_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonAnimatedSwitcher();
	UMG_API UClass* Z_Construct_UClass_UWidgetSwitcher();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ETransitionCurve();
// End Cross Module References
	DEFINE_FUNCTION(UCommonAnimatedSwitcher::execSetDisableTransitionAnimation)
	{
		P_GET_UBOOL(Z_Param_bDisableAnimation);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDisableTransitionAnimation(Z_Param_bDisableAnimation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonAnimatedSwitcher::execHasWidgets)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasWidgets();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonAnimatedSwitcher::execActivatePreviousWidget)
	{
		P_GET_UBOOL(Z_Param_bCanWrap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ActivatePreviousWidget(Z_Param_bCanWrap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonAnimatedSwitcher::execActivateNextWidget)
	{
		P_GET_UBOOL(Z_Param_bCanWrap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ActivateNextWidget(Z_Param_bCanWrap);
		P_NATIVE_END;
	}
	void UCommonAnimatedSwitcher::StaticRegisterNativesUCommonAnimatedSwitcher()
	{
		UClass* Class = UCommonAnimatedSwitcher::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ActivateNextWidget", &UCommonAnimatedSwitcher::execActivateNextWidget },
			{ "ActivatePreviousWidget", &UCommonAnimatedSwitcher::execActivatePreviousWidget },
			{ "HasWidgets", &UCommonAnimatedSwitcher::execHasWidgets },
			{ "SetDisableTransitionAnimation", &UCommonAnimatedSwitcher::execSetDisableTransitionAnimation },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics
	{
		struct CommonAnimatedSwitcher_eventActivateNextWidget_Parms
		{
			bool bCanWrap;
		};
		static void NewProp_bCanWrap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanWrap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::NewProp_bCanWrap_SetBit(void* Obj)
	{
		((CommonAnimatedSwitcher_eventActivateNextWidget_Parms*)Obj)->bCanWrap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::NewProp_bCanWrap = { "bCanWrap", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonAnimatedSwitcher_eventActivateNextWidget_Parms), &Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::NewProp_bCanWrap_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::NewProp_bCanWrap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Widget Switcher" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonAnimatedSwitcher, nullptr, "ActivateNextWidget", nullptr, nullptr, sizeof(CommonAnimatedSwitcher_eventActivateNextWidget_Parms), Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics
	{
		struct CommonAnimatedSwitcher_eventActivatePreviousWidget_Parms
		{
			bool bCanWrap;
		};
		static void NewProp_bCanWrap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanWrap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::NewProp_bCanWrap_SetBit(void* Obj)
	{
		((CommonAnimatedSwitcher_eventActivatePreviousWidget_Parms*)Obj)->bCanWrap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::NewProp_bCanWrap = { "bCanWrap", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonAnimatedSwitcher_eventActivatePreviousWidget_Parms), &Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::NewProp_bCanWrap_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::NewProp_bCanWrap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Widget Switcher" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonAnimatedSwitcher, nullptr, "ActivatePreviousWidget", nullptr, nullptr, sizeof(CommonAnimatedSwitcher_eventActivatePreviousWidget_Parms), Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics
	{
		struct CommonAnimatedSwitcher_eventHasWidgets_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonAnimatedSwitcher_eventHasWidgets_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonAnimatedSwitcher_eventHasWidgets_Parms), &Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Widget Switcher" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonAnimatedSwitcher, nullptr, "HasWidgets", nullptr, nullptr, sizeof(CommonAnimatedSwitcher_eventHasWidgets_Parms), Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics
	{
		struct CommonAnimatedSwitcher_eventSetDisableTransitionAnimation_Parms
		{
			bool bDisableAnimation;
		};
		static void NewProp_bDisableAnimation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableAnimation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::NewProp_bDisableAnimation_SetBit(void* Obj)
	{
		((CommonAnimatedSwitcher_eventSetDisableTransitionAnimation_Parms*)Obj)->bDisableAnimation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::NewProp_bDisableAnimation = { "bDisableAnimation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonAnimatedSwitcher_eventSetDisableTransitionAnimation_Parms), &Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::NewProp_bDisableAnimation_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::NewProp_bDisableAnimation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Widget Switcher" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonAnimatedSwitcher, nullptr, "SetDisableTransitionAnimation", nullptr, nullptr, sizeof(CommonAnimatedSwitcher_eventSetDisableTransitionAnimation_Parms), Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonAnimatedSwitcher_NoRegister()
	{
		return UCommonAnimatedSwitcher::StaticClass();
	}
	struct Z_Construct_UClass_UCommonAnimatedSwitcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransitionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransitionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransitionType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransitionCurveType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransitionCurveType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransitionCurveType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransitionDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TransitionDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWidgetSwitcher,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivateNextWidget, "ActivateNextWidget" }, // 1832501599
		{ &Z_Construct_UFunction_UCommonAnimatedSwitcher_ActivatePreviousWidget, "ActivatePreviousWidget" }, // 2571762592
		{ &Z_Construct_UFunction_UCommonAnimatedSwitcher_HasWidgets, "HasWidgets" }, // 1610420029
		{ &Z_Construct_UFunction_UCommonAnimatedSwitcher_SetDisableTransitionAnimation, "SetDisableTransitionAnimation" }, // 981541086
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CommonAnimatedSwitcher.h" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType_MetaData[] = {
		{ "Category", "Transition" },
		{ "Comment", "/** The type of transition to play between widgets */" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
		{ "ToolTip", "The type of transition to play between widgets" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType = { "TransitionType", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonAnimatedSwitcher, TransitionType), Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition, METADATA_PARAMS(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType_MetaData[] = {
		{ "Category", "Transition" },
		{ "Comment", "/** The curve function type to apply to the transition animation */" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
		{ "ToolTip", "The curve function type to apply to the transition animation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType = { "TransitionCurveType", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonAnimatedSwitcher, TransitionCurveType), Z_Construct_UEnum_CommonUI_ETransitionCurve, METADATA_PARAMS(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionDuration_MetaData[] = {
		{ "Category", "Transition" },
		{ "Comment", "/** The total duration of a single transition between widgets */" },
		{ "ModuleRelativePath", "Public/CommonAnimatedSwitcher.h" },
		{ "ToolTip", "The total duration of a single transition between widgets" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionDuration = { "TransitionDuration", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonAnimatedSwitcher, TransitionDuration), METADATA_PARAMS(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionCurveType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::NewProp_TransitionDuration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonAnimatedSwitcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::ClassParams = {
		&UCommonAnimatedSwitcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonAnimatedSwitcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonAnimatedSwitcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonAnimatedSwitcher, 1055545240);
	template<> COMMONUI_API UClass* StaticClass<UCommonAnimatedSwitcher>()
	{
		return UCommonAnimatedSwitcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonAnimatedSwitcher(Z_Construct_UClass_UCommonAnimatedSwitcher, &UCommonAnimatedSwitcher::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonAnimatedSwitcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonAnimatedSwitcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
