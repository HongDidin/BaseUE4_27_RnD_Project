// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonLazyWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonLazyWidget() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonLazyWidget_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonLazyWidget();
	UMG_API UClass* Z_Construct_UClass_UWidget();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_OnLoadGuardStateChangedDynamic__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UCommonLazyWidget::execIsLoading)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsLoading();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonLazyWidget::execGetContent)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UUserWidget**)Z_Param__Result=P_THIS->GetContent();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonLazyWidget::execSetLazyContent)
	{
		P_GET_SOFTCLASS(TSoftClassPtr<UUserWidget> ,Z_Param_SoftWidget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetLazyContent(Z_Param_SoftWidget);
		P_NATIVE_END;
	}
	void UCommonLazyWidget::StaticRegisterNativesUCommonLazyWidget()
	{
		UClass* Class = UCommonLazyWidget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetContent", &UCommonLazyWidget::execGetContent },
			{ "IsLoading", &UCommonLazyWidget::execIsLoading },
			{ "SetLazyContent", &UCommonLazyWidget::execSetLazyContent },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics
	{
		struct CommonLazyWidget_eventGetContent_Parms
		{
			UUserWidget* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonLazyWidget_eventGetContent_Parms, ReturnValue), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::Function_MetaDataParams[] = {
		{ "Category", "LazyContent" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonLazyWidget, nullptr, "GetContent", nullptr, nullptr, sizeof(CommonLazyWidget_eventGetContent_Parms), Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonLazyWidget_GetContent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonLazyWidget_GetContent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics
	{
		struct CommonLazyWidget_eventIsLoading_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonLazyWidget_eventIsLoading_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonLazyWidget_eventIsLoading_Parms), &Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::Function_MetaDataParams[] = {
		{ "Category", "LazyContent" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonLazyWidget, nullptr, "IsLoading", nullptr, nullptr, sizeof(CommonLazyWidget_eventIsLoading_Parms), Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonLazyWidget_IsLoading()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonLazyWidget_IsLoading_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics
	{
		struct CommonLazyWidget_eventSetLazyContent_Parms
		{
			TSoftClassPtr<UUserWidget>  SoftWidget;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_SoftWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::NewProp_SoftWidget_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::NewProp_SoftWidget = { "SoftWidget", nullptr, (EPropertyFlags)0x0014000000000082, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonLazyWidget_eventSetLazyContent_Parms, SoftWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::NewProp_SoftWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::NewProp_SoftWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::NewProp_SoftWidget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::Function_MetaDataParams[] = {
		{ "Category", "LazyContent" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonLazyWidget, nullptr, "SetLazyContent", nullptr, nullptr, sizeof(CommonLazyWidget_eventSetLazyContent_Parms), Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonLazyWidget_NoRegister()
	{
		return UCommonLazyWidget::StaticClass();
	}
	struct Z_Construct_UClass_UCommonLazyWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LoadingBackgroundBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LoadingBackgroundBrush;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Content_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Content;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BP_OnLoadingStateChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_BP_OnLoadingStateChanged;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonLazyWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonLazyWidget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonLazyWidget_GetContent, "GetContent" }, // 3815445999
		{ &Z_Construct_UFunction_UCommonLazyWidget_IsLoading, "IsLoading" }, // 2213208600
		{ &Z_Construct_UFunction_UCommonLazyWidget_SetLazyContent, "SetLazyContent" }, // 3392290910
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonLazyWidget_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A special Image widget that can show unloaded images and takes care of the loading for you!\n */" },
		{ "IncludePath", "CommonLazyWidget.h" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
		{ "ToolTip", "A special Image widget that can show unloaded images and takes care of the loading for you!" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_LoadingBackgroundBrush_MetaData[] = {
		{ "Category", "Appearance" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_LoadingBackgroundBrush = { "LoadingBackgroundBrush", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonLazyWidget, LoadingBackgroundBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_LoadingBackgroundBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_LoadingBackgroundBrush_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_Content_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_Content = { "Content", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonLazyWidget, Content), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_Content_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_Content_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_BP_OnLoadingStateChanged_MetaData[] = {
		{ "Category", "LazyImage" },
		{ "DisplayName", "On Loading State Changed" },
		{ "ModuleRelativePath", "Public/CommonLazyWidget.h" },
		{ "ScriptName", "OnLoadingStateChanged" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_BP_OnLoadingStateChanged = { "BP_OnLoadingStateChanged", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonLazyWidget, BP_OnLoadingStateChanged), Z_Construct_UDelegateFunction_CommonUI_OnLoadGuardStateChangedDynamic__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_BP_OnLoadingStateChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_BP_OnLoadingStateChanged_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonLazyWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_LoadingBackgroundBrush,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_Content,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonLazyWidget_Statics::NewProp_BP_OnLoadingStateChanged,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonLazyWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonLazyWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonLazyWidget_Statics::ClassParams = {
		&UCommonLazyWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonLazyWidget_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonLazyWidget_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonLazyWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonLazyWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonLazyWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonLazyWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonLazyWidget, 4144674036);
	template<> COMMONUI_API UClass* StaticClass<UCommonLazyWidget>()
	{
		return UCommonLazyWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonLazyWidget(Z_Construct_UClass_UCommonLazyWidget, &UCommonLazyWidget::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonLazyWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonLazyWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
