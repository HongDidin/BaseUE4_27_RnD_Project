// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonVisibilitySwitcherSlot_generated_h
#error "CommonVisibilitySwitcherSlot.generated.h already included, missing '#pragma once' in CommonVisibilitySwitcherSlot.h"
#endif
#define COMMONUI_CommonVisibilitySwitcherSlot_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonVisibilitySwitcherSlot(); \
	friend struct Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics; \
public: \
	DECLARE_CLASS(UCommonVisibilitySwitcherSlot, UOverlaySlot, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVisibilitySwitcherSlot)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonVisibilitySwitcherSlot(); \
	friend struct Z_Construct_UClass_UCommonVisibilitySwitcherSlot_Statics; \
public: \
	DECLARE_CLASS(UCommonVisibilitySwitcherSlot, UOverlaySlot, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVisibilitySwitcherSlot)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonVisibilitySwitcherSlot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVisibilitySwitcherSlot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVisibilitySwitcherSlot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVisibilitySwitcherSlot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVisibilitySwitcherSlot(UCommonVisibilitySwitcherSlot&&); \
	NO_API UCommonVisibilitySwitcherSlot(const UCommonVisibilitySwitcherSlot&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVisibilitySwitcherSlot(UCommonVisibilitySwitcherSlot&&); \
	NO_API UCommonVisibilitySwitcherSlot(const UCommonVisibilitySwitcherSlot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVisibilitySwitcherSlot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVisibilitySwitcherSlot); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVisibilitySwitcherSlot)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_12_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonVisibilitySwitcherSlot>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcherSlot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
