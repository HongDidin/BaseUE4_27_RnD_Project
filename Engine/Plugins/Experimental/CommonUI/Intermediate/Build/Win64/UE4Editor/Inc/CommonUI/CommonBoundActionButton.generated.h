// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonBoundActionButton_generated_h
#error "CommonBoundActionButton.generated.h already included, missing '#pragma once' in CommonBoundActionButton.h"
#endif
#define COMMONUI_CommonBoundActionButton_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_EVENT_PARMS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonBoundActionButton(); \
	friend struct Z_Construct_UClass_UCommonBoundActionButton_Statics; \
public: \
	DECLARE_CLASS(UCommonBoundActionButton, UCommonButtonBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBoundActionButton)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonBoundActionButton(); \
	friend struct Z_Construct_UClass_UCommonBoundActionButton_Statics; \
public: \
	DECLARE_CLASS(UCommonBoundActionButton, UCommonButtonBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBoundActionButton)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBoundActionButton(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBoundActionButton) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBoundActionButton); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBoundActionButton); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBoundActionButton(UCommonBoundActionButton&&); \
	NO_API UCommonBoundActionButton(const UCommonBoundActionButton&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBoundActionButton(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBoundActionButton(UCommonBoundActionButton&&); \
	NO_API UCommonBoundActionButton(const UCommonBoundActionButton&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBoundActionButton); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBoundActionButton); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBoundActionButton)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Text_ActionName() { return STRUCT_OFFSET(UCommonBoundActionButton, Text_ActionName); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_12_PROLOG \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonBoundActionButton>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonBoundActionButton_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
