// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonPoolableWidgetInterface_generated_h
#error "CommonPoolableWidgetInterface.generated.h already included, missing '#pragma once' in CommonPoolableWidgetInterface.h"
#endif
#define COMMONUI_CommonPoolableWidgetInterface_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_RPC_WRAPPERS \
	virtual void OnReleaseToPool_Implementation() {}; \
	virtual void OnAcquireFromPool_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnReleaseToPool); \
	DECLARE_FUNCTION(execOnAcquireFromPool);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnReleaseToPool_Implementation() {}; \
	virtual void OnAcquireFromPool_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnReleaseToPool); \
	DECLARE_FUNCTION(execOnAcquireFromPool);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_EVENT_PARMS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonPoolableWidgetInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonPoolableWidgetInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonPoolableWidgetInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonPoolableWidgetInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonPoolableWidgetInterface(UCommonPoolableWidgetInterface&&); \
	NO_API UCommonPoolableWidgetInterface(const UCommonPoolableWidgetInterface&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonPoolableWidgetInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonPoolableWidgetInterface(UCommonPoolableWidgetInterface&&); \
	NO_API UCommonPoolableWidgetInterface(const UCommonPoolableWidgetInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonPoolableWidgetInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonPoolableWidgetInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonPoolableWidgetInterface)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUCommonPoolableWidgetInterface(); \
	friend struct Z_Construct_UClass_UCommonPoolableWidgetInterface_Statics; \
public: \
	DECLARE_CLASS(UCommonPoolableWidgetInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonPoolableWidgetInterface)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ICommonPoolableWidgetInterface() {} \
public: \
	typedef UCommonPoolableWidgetInterface UClassType; \
	typedef ICommonPoolableWidgetInterface ThisClass; \
	static void Execute_OnAcquireFromPool(UObject* O); \
	static void Execute_OnReleaseToPool(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~ICommonPoolableWidgetInterface() {} \
public: \
	typedef UCommonPoolableWidgetInterface UClassType; \
	typedef ICommonPoolableWidgetInterface ThisClass; \
	static void Execute_OnAcquireFromPool(UObject* O); \
	static void Execute_OnReleaseToPool(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_10_PROLOG \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonPoolableWidgetInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonPoolableWidgetInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
