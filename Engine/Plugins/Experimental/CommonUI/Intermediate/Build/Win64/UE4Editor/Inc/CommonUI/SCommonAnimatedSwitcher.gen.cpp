// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Slate/SCommonAnimatedSwitcher.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSCommonAnimatedSwitcher() {}
// Cross Module References
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ETransitionCurve();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition();
// End Cross Module References
	static UEnum* ETransitionCurve_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonUI_ETransitionCurve, Z_Construct_UPackage__Script_CommonUI(), TEXT("ETransitionCurve"));
		}
		return Singleton;
	}
	template<> COMMONUI_API UEnum* StaticEnum<ETransitionCurve>()
	{
		return ETransitionCurve_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransitionCurve(ETransitionCurve_StaticEnum, TEXT("/Script/CommonUI"), TEXT("ETransitionCurve"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonUI_ETransitionCurve_Hash() { return 2015279675U; }
	UEnum* Z_Construct_UEnum_CommonUI_ETransitionCurve()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransitionCurve"), 0, Get_Z_Construct_UEnum_CommonUI_ETransitionCurve_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransitionCurve::Linear", (int64)ETransitionCurve::Linear },
				{ "ETransitionCurve::QuadIn", (int64)ETransitionCurve::QuadIn },
				{ "ETransitionCurve::QuadOut", (int64)ETransitionCurve::QuadOut },
				{ "ETransitionCurve::QuadInOut", (int64)ETransitionCurve::QuadInOut },
				{ "ETransitionCurve::CubicIn", (int64)ETransitionCurve::CubicIn },
				{ "ETransitionCurve::CubicOut", (int64)ETransitionCurve::CubicOut },
				{ "ETransitionCurve::CubicInOut", (int64)ETransitionCurve::CubicInOut },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CubicIn.Comment", "/** Cubic ease in */" },
				{ "CubicIn.Name", "ETransitionCurve::CubicIn" },
				{ "CubicIn.ToolTip", "Cubic ease in" },
				{ "CubicInOut.Comment", "/** Cubic ease in, cubic ease out */" },
				{ "CubicInOut.Name", "ETransitionCurve::CubicInOut" },
				{ "CubicInOut.ToolTip", "Cubic ease in, cubic ease out" },
				{ "CubicOut.Comment", "/** Cubic ease out */" },
				{ "CubicOut.Name", "ETransitionCurve::CubicOut" },
				{ "CubicOut.ToolTip", "Cubic ease out" },
				{ "Linear.Comment", "/** Linear interpolation, with no easing */" },
				{ "Linear.Name", "ETransitionCurve::Linear" },
				{ "Linear.ToolTip", "Linear interpolation, with no easing" },
				{ "ModuleRelativePath", "Public/Slate/SCommonAnimatedSwitcher.h" },
				{ "QuadIn.Comment", "/** Quadratic ease in */" },
				{ "QuadIn.Name", "ETransitionCurve::QuadIn" },
				{ "QuadIn.ToolTip", "Quadratic ease in" },
				{ "QuadInOut.Comment", "/** Quadratic ease in, quadratic ease out */" },
				{ "QuadInOut.Name", "ETransitionCurve::QuadInOut" },
				{ "QuadInOut.ToolTip", "Quadratic ease in, quadratic ease out" },
				{ "QuadOut.Comment", "/** Quadratic ease out */" },
				{ "QuadOut.Name", "ETransitionCurve::QuadOut" },
				{ "QuadOut.ToolTip", "Quadratic ease out" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonUI,
				nullptr,
				"ETransitionCurve",
				"ETransitionCurve",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ECommonSwitcherTransition_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition, Z_Construct_UPackage__Script_CommonUI(), TEXT("ECommonSwitcherTransition"));
		}
		return Singleton;
	}
	template<> COMMONUI_API UEnum* StaticEnum<ECommonSwitcherTransition>()
	{
		return ECommonSwitcherTransition_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECommonSwitcherTransition(ECommonSwitcherTransition_StaticEnum, TEXT("/Script/CommonUI"), TEXT("ECommonSwitcherTransition"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition_Hash() { return 932607024U; }
	UEnum* Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECommonSwitcherTransition"), 0, Get_Z_Construct_UEnum_CommonUI_ECommonSwitcherTransition_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECommonSwitcherTransition::FadeOnly", (int64)ECommonSwitcherTransition::FadeOnly },
				{ "ECommonSwitcherTransition::Horizontal", (int64)ECommonSwitcherTransition::Horizontal },
				{ "ECommonSwitcherTransition::Vertical", (int64)ECommonSwitcherTransition::Vertical },
				{ "ECommonSwitcherTransition::Zoom", (int64)ECommonSwitcherTransition::Zoom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "FadeOnly.Comment", "/** Fade transition only with no movement */" },
				{ "FadeOnly.Name", "ECommonSwitcherTransition::FadeOnly" },
				{ "FadeOnly.ToolTip", "Fade transition only with no movement" },
				{ "Horizontal.Comment", "/** Increasing the active index goes right, decreasing goes left */" },
				{ "Horizontal.Name", "ECommonSwitcherTransition::Horizontal" },
				{ "Horizontal.ToolTip", "Increasing the active index goes right, decreasing goes left" },
				{ "ModuleRelativePath", "Public/Slate/SCommonAnimatedSwitcher.h" },
				{ "Vertical.Comment", "/** Increasing the active index goes up, decreasing goes down */" },
				{ "Vertical.Name", "ECommonSwitcherTransition::Vertical" },
				{ "Vertical.ToolTip", "Increasing the active index goes up, decreasing goes down" },
				{ "Zoom.Comment", "/** Increasing the active index zooms in, decreasing zooms out */" },
				{ "Zoom.Name", "ECommonSwitcherTransition::Zoom" },
				{ "Zoom.ToolTip", "Increasing the active index zooms in, decreasing zooms out" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonUI,
				nullptr,
				"ECommonSwitcherTransition",
				"ECommonSwitcherTransition",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
