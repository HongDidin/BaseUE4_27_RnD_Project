// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonUISubsystemBase.h"
#include "Engine/Classes/Engine/GameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUISubsystemBase() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUISubsystemBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUISubsystemBase();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstanceSubsystem();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDataTableRowHandle();
	COMMONINPUT_API UEnum* Z_Construct_UEnum_CommonInput_ECommonInputType();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
// End Cross Module References
	DEFINE_FUNCTION(UCommonUISubsystemBase::execGetInputActionButtonIcon)
	{
		P_GET_STRUCT_REF(FDataTableRowHandle,Z_Param_Out_InputActionRowHandle);
		P_GET_ENUM(ECommonInputType,Z_Param_InputType);
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_GamepadName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSlateBrush*)Z_Param__Result=P_THIS->GetInputActionButtonIcon(Z_Param_Out_InputActionRowHandle,ECommonInputType(Z_Param_InputType),Z_Param_Out_GamepadName);
		P_NATIVE_END;
	}
	void UCommonUISubsystemBase::StaticRegisterNativesUCommonUISubsystemBase()
	{
		UClass* Class = UCommonUISubsystemBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetInputActionButtonIcon", &UCommonUISubsystemBase::execGetInputActionButtonIcon },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics
	{
		struct CommonUISubsystemBase_eventGetInputActionButtonIcon_Parms
		{
			FDataTableRowHandle InputActionRowHandle;
			ECommonInputType InputType;
			FName GamepadName;
			FSlateBrush ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActionRowHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActionRowHandle;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InputType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InputType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GamepadName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GamepadName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputActionRowHandle_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputActionRowHandle = { "InputActionRowHandle", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonUISubsystemBase_eventGetInputActionButtonIcon_Parms, InputActionRowHandle), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputActionRowHandle_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputActionRowHandle_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputType = { "InputType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonUISubsystemBase_eventGetInputActionButtonIcon_Parms, InputType), Z_Construct_UEnum_CommonInput_ECommonInputType, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_GamepadName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_GamepadName = { "GamepadName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonUISubsystemBase_eventGetInputActionButtonIcon_Parms, GamepadName), METADATA_PARAMS(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_GamepadName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_GamepadName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonUISubsystemBase_eventGetInputActionButtonIcon_Parms, ReturnValue), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputActionRowHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_InputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_GamepadName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonUISubsystem" },
		{ "Comment", "// Gets Action Button Icon for current gamepad\n" },
		{ "ModuleRelativePath", "Public/CommonUISubsystemBase.h" },
		{ "ToolTip", "Gets Action Button Icon for current gamepad" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonUISubsystemBase, nullptr, "GetInputActionButtonIcon", nullptr, nullptr, sizeof(CommonUISubsystemBase_eventGetInputActionButtonIcon_Parms), Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonUISubsystemBase_NoRegister()
	{
		return UCommonUISubsystemBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUISubsystemBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUISubsystemBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstanceSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonUISubsystemBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonUISubsystemBase_GetInputActionButtonIcon, "GetInputActionButtonIcon" }, // 330381217
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUISubsystemBase_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "CommonUI" },
		{ "IncludePath", "CommonUISubsystemBase.h" },
		{ "ModuleRelativePath", "Public/CommonUISubsystemBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUISubsystemBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUISubsystemBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUISubsystemBase_Statics::ClassParams = {
		&UCommonUISubsystemBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUISubsystemBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUISubsystemBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUISubsystemBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUISubsystemBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUISubsystemBase, 3143187261);
	template<> COMMONUI_API UClass* StaticClass<UCommonUISubsystemBase>()
	{
		return UCommonUISubsystemBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUISubsystemBase(Z_Construct_UClass_UCommonUISubsystemBase, &UCommonUISubsystemBase::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonUISubsystemBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUISubsystemBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
