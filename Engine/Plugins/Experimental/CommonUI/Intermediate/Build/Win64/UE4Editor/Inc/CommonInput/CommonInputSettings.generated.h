// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONINPUT_CommonInputSettings_generated_h
#error "CommonInputSettings.generated.h already included, missing '#pragma once' in CommonInputSettings.h"
#endif
#define COMMONINPUT_CommonInputSettings_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRegisteredPlatforms);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRegisteredPlatforms);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonInputSettings(); \
	friend struct Z_Construct_UClass_UCommonInputSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonInputSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonInputSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUCommonInputSettings(); \
	friend struct Z_Construct_UClass_UCommonInputSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonInputSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonInputSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonInputSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonInputSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonInputSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonInputSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonInputSettings(UCommonInputSettings&&); \
	NO_API UCommonInputSettings(const UCommonInputSettings&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonInputSettings(UCommonInputSettings&&); \
	NO_API UCommonInputSettings(const UCommonInputSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonInputSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonInputSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonInputSettings)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__InputData() { return STRUCT_OFFSET(UCommonInputSettings, InputData); } \
	FORCEINLINE static uint32 __PPO__CommonInputPlatformData() { return STRUCT_OFFSET(UCommonInputSettings, CommonInputPlatformData); } \
	FORCEINLINE static uint32 __PPO__bEnableInputMethodThrashingProtection() { return STRUCT_OFFSET(UCommonInputSettings, bEnableInputMethodThrashingProtection); } \
	FORCEINLINE static uint32 __PPO__InputMethodThrashingLimit() { return STRUCT_OFFSET(UCommonInputSettings, InputMethodThrashingLimit); } \
	FORCEINLINE static uint32 __PPO__InputMethodThrashingWindowInSeconds() { return STRUCT_OFFSET(UCommonInputSettings, InputMethodThrashingWindowInSeconds); } \
	FORCEINLINE static uint32 __PPO__InputMethodThrashingCooldownInSeconds() { return STRUCT_OFFSET(UCommonInputSettings, InputMethodThrashingCooldownInSeconds); } \
	FORCEINLINE static uint32 __PPO__bAllowOutOfFocusDeviceInput() { return STRUCT_OFFSET(UCommonInputSettings, bAllowOutOfFocusDeviceInput); } \
	FORCEINLINE static uint32 __PPO__InputDataClass() { return STRUCT_OFFSET(UCommonInputSettings, InputDataClass); } \
	FORCEINLINE static uint32 __PPO__CurrentPlatform() { return STRUCT_OFFSET(UCommonInputSettings, CurrentPlatform); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_18_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONINPUT_API UClass* StaticClass<class UCommonInputSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
