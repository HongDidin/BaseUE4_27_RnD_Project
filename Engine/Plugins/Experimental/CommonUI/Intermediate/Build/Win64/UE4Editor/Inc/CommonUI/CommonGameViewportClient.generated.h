// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonGameViewportClient_generated_h
#error "CommonGameViewportClient.generated.h already included, missing '#pragma once' in CommonGameViewportClient.h"
#endif
#define COMMONUI_CommonGameViewportClient_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonGameViewportClient(); \
	friend struct Z_Construct_UClass_UCommonGameViewportClient_Statics; \
public: \
	DECLARE_CLASS(UCommonGameViewportClient, UGameViewportClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonGameViewportClient)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUCommonGameViewportClient(); \
	friend struct Z_Construct_UClass_UCommonGameViewportClient_Statics; \
public: \
	DECLARE_CLASS(UCommonGameViewportClient, UGameViewportClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonGameViewportClient)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonGameViewportClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonGameViewportClient) \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonGameViewportClient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonGameViewportClient(UCommonGameViewportClient&&); \
	NO_API UCommonGameViewportClient(const UCommonGameViewportClient&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonGameViewportClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonGameViewportClient(UCommonGameViewportClient&&); \
	NO_API UCommonGameViewportClient(const UCommonGameViewportClient&); \
public: \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonGameViewportClient); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonGameViewportClient)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_18_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonGameViewportClient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonGameViewportClient_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
