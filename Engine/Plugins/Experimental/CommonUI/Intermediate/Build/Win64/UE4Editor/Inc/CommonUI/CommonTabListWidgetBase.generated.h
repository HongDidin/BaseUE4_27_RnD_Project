// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonButtonBase;
enum class ESlateVisibility : uint8;
class UWidget;
class UCommonAnimatedSwitcher;
#ifdef COMMONUI_CommonTabListWidgetBase_generated_h
#error "CommonTabListWidgetBase.generated.h already included, missing '#pragma once' in CommonTabListWidgetBase.h"
#endif
#define COMMONUI_CommonTabListWidgetBase_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonRegisteredTabInfo_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct();


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonRegisteredTabInfo>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_61_DELEGATE \
struct CommonTabListWidgetBase_eventOnTabButtonRemoval_Parms \
{ \
	FName TabId; \
	UCommonButtonBase* TabButton; \
}; \
static inline void FOnTabButtonRemoval_DelegateWrapper(const FMulticastScriptDelegate& OnTabButtonRemoval, FName TabId, UCommonButtonBase* TabButton) \
{ \
	CommonTabListWidgetBase_eventOnTabButtonRemoval_Parms Parms; \
	Parms.TabId=TabId; \
	Parms.TabButton=TabButton; \
	OnTabButtonRemoval.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_54_DELEGATE \
struct CommonTabListWidgetBase_eventOnTabButtonCreation_Parms \
{ \
	FName TabId; \
	UCommonButtonBase* TabButton; \
}; \
static inline void FOnTabButtonCreation_DelegateWrapper(const FMulticastScriptDelegate& OnTabButtonCreation, FName TabId, UCommonButtonBase* TabButton) \
{ \
	CommonTabListWidgetBase_eventOnTabButtonCreation_Parms Parms; \
	Parms.TabId=TabId; \
	Parms.TabButton=TabButton; \
	OnTabButtonCreation.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_47_DELEGATE \
struct CommonTabListWidgetBase_eventOnTabSelected_Parms \
{ \
	FName TabId; \
}; \
static inline void FOnTabSelected_DelegateWrapper(const FMulticastScriptDelegate& OnTabSelected, FName TabId) \
{ \
	CommonTabListWidgetBase_eventOnTabSelected_Parms Parms; \
	Parms.TabId=TabId; \
	OnTabSelected.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_RPC_WRAPPERS \
	virtual void HandleTabRemoval_Implementation(FName TabNameID, UCommonButtonBase* TabButton); \
	virtual void HandleTabCreation_Implementation(FName TabNameID, UCommonButtonBase* TabButton); \
 \
	DECLARE_FUNCTION(execHandleNextTabInputAction); \
	DECLARE_FUNCTION(execHandlePreviousTabInputAction); \
	DECLARE_FUNCTION(execHandleTabButtonSelected); \
	DECLARE_FUNCTION(execGetTabButtonBaseByID); \
	DECLARE_FUNCTION(execHandleTabRemoval); \
	DECLARE_FUNCTION(execHandleTabCreation); \
	DECLARE_FUNCTION(execSetListeningForInput); \
	DECLARE_FUNCTION(execDisableTabWithReason); \
	DECLARE_FUNCTION(execSetTabInteractionEnabled); \
	DECLARE_FUNCTION(execSetTabEnabled); \
	DECLARE_FUNCTION(execSetTabVisibility); \
	DECLARE_FUNCTION(execGetTabIdAtIndex); \
	DECLARE_FUNCTION(execGetSelectedTabId); \
	DECLARE_FUNCTION(execSelectTabByID); \
	DECLARE_FUNCTION(execGetTabCount); \
	DECLARE_FUNCTION(execRemoveAllTabs); \
	DECLARE_FUNCTION(execRemoveTab); \
	DECLARE_FUNCTION(execRegisterTab); \
	DECLARE_FUNCTION(execGetLinkedSwitcher); \
	DECLARE_FUNCTION(execSetLinkedSwitcher); \
	DECLARE_FUNCTION(execGetActiveTab);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void HandleTabRemoval_Implementation(FName TabNameID, UCommonButtonBase* TabButton); \
	virtual void HandleTabCreation_Implementation(FName TabNameID, UCommonButtonBase* TabButton); \
 \
	DECLARE_FUNCTION(execHandleNextTabInputAction); \
	DECLARE_FUNCTION(execHandlePreviousTabInputAction); \
	DECLARE_FUNCTION(execHandleTabButtonSelected); \
	DECLARE_FUNCTION(execGetTabButtonBaseByID); \
	DECLARE_FUNCTION(execHandleTabRemoval); \
	DECLARE_FUNCTION(execHandleTabCreation); \
	DECLARE_FUNCTION(execSetListeningForInput); \
	DECLARE_FUNCTION(execDisableTabWithReason); \
	DECLARE_FUNCTION(execSetTabInteractionEnabled); \
	DECLARE_FUNCTION(execSetTabEnabled); \
	DECLARE_FUNCTION(execSetTabVisibility); \
	DECLARE_FUNCTION(execGetTabIdAtIndex); \
	DECLARE_FUNCTION(execGetSelectedTabId); \
	DECLARE_FUNCTION(execSelectTabByID); \
	DECLARE_FUNCTION(execGetTabCount); \
	DECLARE_FUNCTION(execRemoveAllTabs); \
	DECLARE_FUNCTION(execRemoveTab); \
	DECLARE_FUNCTION(execRegisterTab); \
	DECLARE_FUNCTION(execGetLinkedSwitcher); \
	DECLARE_FUNCTION(execSetLinkedSwitcher); \
	DECLARE_FUNCTION(execGetActiveTab);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_EVENT_PARMS \
	struct CommonTabListWidgetBase_eventHandleTabCreation_Parms \
	{ \
		FName TabNameID; \
		UCommonButtonBase* TabButton; \
	}; \
	struct CommonTabListWidgetBase_eventHandleTabRemoval_Parms \
	{ \
		FName TabNameID; \
		UCommonButtonBase* TabButton; \
	};


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTabListWidgetBase(); \
	friend struct Z_Construct_UClass_UCommonTabListWidgetBase_Statics; \
public: \
	DECLARE_CLASS(UCommonTabListWidgetBase, UCommonUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTabListWidgetBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTabListWidgetBase(); \
	friend struct Z_Construct_UClass_UCommonTabListWidgetBase_Statics; \
public: \
	DECLARE_CLASS(UCommonTabListWidgetBase, UCommonUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTabListWidgetBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTabListWidgetBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTabListWidgetBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTabListWidgetBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTabListWidgetBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTabListWidgetBase(UCommonTabListWidgetBase&&); \
	NO_API UCommonTabListWidgetBase(const UCommonTabListWidgetBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTabListWidgetBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTabListWidgetBase(UCommonTabListWidgetBase&&); \
	NO_API UCommonTabListWidgetBase(const UCommonTabListWidgetBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTabListWidgetBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTabListWidgetBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTabListWidgetBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NextTabInputActionData() { return STRUCT_OFFSET(UCommonTabListWidgetBase, NextTabInputActionData); } \
	FORCEINLINE static uint32 __PPO__PreviousTabInputActionData() { return STRUCT_OFFSET(UCommonTabListWidgetBase, PreviousTabInputActionData); } \
	FORCEINLINE static uint32 __PPO__bAutoListenForInput() { return STRUCT_OFFSET(UCommonTabListWidgetBase, bAutoListenForInput); } \
	FORCEINLINE static uint32 __PPO__LinkedSwitcher() { return STRUCT_OFFSET(UCommonTabListWidgetBase, LinkedSwitcher); } \
	FORCEINLINE static uint32 __PPO__TabButtonGroup() { return STRUCT_OFFSET(UCommonTabListWidgetBase, TabButtonGroup); } \
	FORCEINLINE static uint32 __PPO__RegisteredTabsByID() { return STRUCT_OFFSET(UCommonTabListWidgetBase, RegisteredTabsByID); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_40_PROLOG \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h_43_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonTabListWidgetBase."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonTabListWidgetBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTabListWidgetBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
