// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONINPUT_CommonInputBaseTypes_generated_h
#error "CommonInputBaseTypes.generated.h already included, missing '#pragma once' in CommonInputBaseTypes.h"
#endif
#define COMMONINPUT_CommonInputBaseTypes_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_132_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bSupported() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, bSupported); } \
	FORCEINLINE static uint32 __PPO__DefaultInputType() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, DefaultInputType); } \
	FORCEINLINE static uint32 __PPO__bSupportsMouseAndKeyboard() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, bSupportsMouseAndKeyboard); } \
	FORCEINLINE static uint32 __PPO__bSupportsGamepad() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, bSupportsGamepad); } \
	FORCEINLINE static uint32 __PPO__DefaultGamepadName() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, DefaultGamepadName); } \
	FORCEINLINE static uint32 __PPO__bCanChangeGamepadType() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, bCanChangeGamepadType); } \
	FORCEINLINE static uint32 __PPO__bSupportsTouch() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, bSupportsTouch); } \
	FORCEINLINE static uint32 __PPO__ControllerData() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, ControllerData); } \
	FORCEINLINE static uint32 __PPO__ControllerDataClasses() { return STRUCT_OFFSET(FCommonInputPlatformBaseData, ControllerDataClasses); }


template<> COMMONINPUT_API UScriptStruct* StaticStruct<struct FCommonInputPlatformBaseData>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_61_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONINPUT_API UScriptStruct* StaticStruct<struct FCommonInputKeySetBrushConfiguration>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONINPUT_API UScriptStruct* StaticStruct<struct FCommonInputKeyBrushConfiguration>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUIInputData(); \
	friend struct Z_Construct_UClass_UCommonUIInputData_Statics; \
public: \
	DECLARE_CLASS(UCommonUIInputData, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIInputData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUIInputData(); \
	friend struct Z_Construct_UClass_UCommonUIInputData_Statics; \
public: \
	DECLARE_CLASS(UCommonUIInputData, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIInputData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUIInputData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIInputData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIInputData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIInputData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIInputData(UCommonUIInputData&&); \
	NO_API UCommonUIInputData(const UCommonUIInputData&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUIInputData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIInputData(UCommonUIInputData&&); \
	NO_API UCommonUIInputData(const UCommonUIInputData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIInputData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIInputData); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIInputData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_77_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_80_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONINPUT_API UClass* StaticClass<class UCommonUIInputData>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRegisteredGamepads);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRegisteredGamepads);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonInputBaseControllerData(); \
	friend struct Z_Construct_UClass_UCommonInputBaseControllerData_Statics; \
public: \
	DECLARE_CLASS(UCommonInputBaseControllerData, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonInputBaseControllerData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_INCLASS \
private: \
	static void StaticRegisterNativesUCommonInputBaseControllerData(); \
	friend struct Z_Construct_UClass_UCommonInputBaseControllerData_Statics; \
public: \
	DECLARE_CLASS(UCommonInputBaseControllerData, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonInput"), NO_API) \
	DECLARE_SERIALIZER(UCommonInputBaseControllerData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonInputBaseControllerData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonInputBaseControllerData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonInputBaseControllerData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonInputBaseControllerData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonInputBaseControllerData(UCommonInputBaseControllerData&&); \
	NO_API UCommonInputBaseControllerData(const UCommonInputBaseControllerData&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonInputBaseControllerData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonInputBaseControllerData(UCommonInputBaseControllerData&&); \
	NO_API UCommonInputBaseControllerData(const UCommonInputBaseControllerData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonInputBaseControllerData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonInputBaseControllerData); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonInputBaseControllerData)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_94_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h_97_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONINPUT_API UClass* StaticClass<class UCommonInputBaseControllerData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonInput_Public_CommonInputBaseTypes_h


#define FOREACH_ENUM_ECOMMONINPUTTYPE(op) \
	op(ECommonInputType::MouseAndKeyboard) \
	op(ECommonInputType::Gamepad) \
	op(ECommonInputType::Touch) \
	op(ECommonInputType::Count) 

enum class ECommonInputType : uint8;
template<> COMMONINPUT_API UEnum* StaticEnum<ECommonInputType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
