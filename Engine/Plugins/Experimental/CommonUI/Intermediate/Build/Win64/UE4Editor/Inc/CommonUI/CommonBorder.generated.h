// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSlateBrush;
class UCommonBorderStyle;
#ifdef COMMONUI_CommonBorder_generated_h
#error "CommonBorder.generated.h already included, missing '#pragma once' in CommonBorder.h"
#endif
#define COMMONUI_CommonBorder_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBackgroundBrush);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBackgroundBrush);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonBorderStyle(); \
	friend struct Z_Construct_UClass_UCommonBorderStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonBorderStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBorderStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUCommonBorderStyle(); \
	friend struct Z_Construct_UClass_UCommonBorderStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonBorderStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBorderStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBorderStyle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBorderStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBorderStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBorderStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBorderStyle(UCommonBorderStyle&&); \
	NO_API UCommonBorderStyle(const UCommonBorderStyle&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBorderStyle(UCommonBorderStyle&&); \
	NO_API UCommonBorderStyle(const UCommonBorderStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBorderStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBorderStyle); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UCommonBorderStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_13_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonBorderStyle>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetStyle);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetStyle);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonBorder(); \
	friend struct Z_Construct_UClass_UCommonBorder_Statics; \
public: \
	DECLARE_CLASS(UCommonBorder, UBorder, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBorder) \
	static const TCHAR* StaticConfigName() {return TEXT("CommonUI");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUCommonBorder(); \
	friend struct Z_Construct_UClass_UCommonBorder_Statics; \
public: \
	DECLARE_CLASS(UCommonBorder, UBorder, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonBorder) \
	static const TCHAR* StaticConfigName() {return TEXT("CommonUI");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBorder(UCommonBorder&&); \
	NO_API UCommonBorder(const UCommonBorder&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonBorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonBorder(UCommonBorder&&); \
	NO_API UCommonBorder(const UCommonBorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonBorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonBorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonBorder)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_30_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h_33_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonBorder."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonBorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonBorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
