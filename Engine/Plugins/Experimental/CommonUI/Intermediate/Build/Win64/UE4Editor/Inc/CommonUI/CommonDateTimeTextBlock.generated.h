// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDateTime;
struct FTimespan;
#ifdef COMMONUI_CommonDateTimeTextBlock_generated_h
#error "CommonDateTimeTextBlock.generated.h already included, missing '#pragma once' in CommonDateTimeTextBlock.h"
#endif
#define COMMONUI_CommonDateTimeTextBlock_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDateTime); \
	DECLARE_FUNCTION(execSetCountDownCompletionText); \
	DECLARE_FUNCTION(execSetTimespanValue); \
	DECLARE_FUNCTION(execSetDateTimeValue);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDateTime); \
	DECLARE_FUNCTION(execSetCountDownCompletionText); \
	DECLARE_FUNCTION(execSetTimespanValue); \
	DECLARE_FUNCTION(execSetDateTimeValue);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonDateTimeTextBlock(); \
	friend struct Z_Construct_UClass_UCommonDateTimeTextBlock_Statics; \
public: \
	DECLARE_CLASS(UCommonDateTimeTextBlock, UCommonTextBlock, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonDateTimeTextBlock)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUCommonDateTimeTextBlock(); \
	friend struct Z_Construct_UClass_UCommonDateTimeTextBlock_Statics; \
public: \
	DECLARE_CLASS(UCommonDateTimeTextBlock, UCommonTextBlock, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonDateTimeTextBlock)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonDateTimeTextBlock(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonDateTimeTextBlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonDateTimeTextBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonDateTimeTextBlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonDateTimeTextBlock(UCommonDateTimeTextBlock&&); \
	NO_API UCommonDateTimeTextBlock(const UCommonDateTimeTextBlock&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonDateTimeTextBlock(UCommonDateTimeTextBlock&&); \
	NO_API UCommonDateTimeTextBlock(const UCommonDateTimeTextBlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonDateTimeTextBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonDateTimeTextBlock); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonDateTimeTextBlock)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_8_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonDateTimeTextBlock>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonDateTimeTextBlock_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
