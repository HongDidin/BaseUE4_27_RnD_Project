// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FMargin;
#ifdef COMMONUI_CommonLoadGuard_generated_h
#error "CommonLoadGuard.generated.h already included, missing '#pragma once' in CommonLoadGuard.h"
#endif
#define COMMONUI_CommonLoadGuard_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_199_DELEGATE \
struct CommonLoadGuard_eventOnAssetLoaded_Parms \
{ \
	UObject* Object; \
}; \
static inline void FOnAssetLoaded_DelegateWrapper(const FScriptDelegate& OnAssetLoaded, UObject* Object) \
{ \
	CommonLoadGuard_eventOnAssetLoaded_Parms Parms; \
	Parms.Object=Object; \
	OnAssetLoaded.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_134_DELEGATE \
struct _Script_CommonUI_eventOnLoadGuardStateChangedDynamic_Parms \
{ \
	bool bIsLoading; \
}; \
static inline void FOnLoadGuardStateChangedDynamic_DelegateWrapper(const FMulticastScriptDelegate& OnLoadGuardStateChangedDynamic, bool bIsLoading) \
{ \
	_Script_CommonUI_eventOnLoadGuardStateChangedDynamic_Parms Parms; \
	Parms.bIsLoading=bIsLoading ? true : false; \
	OnLoadGuardStateChangedDynamic.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetVerticalAlignment); \
	DECLARE_FUNCTION(execSetHorizontalAlignment); \
	DECLARE_FUNCTION(execSetPadding);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetVerticalAlignment); \
	DECLARE_FUNCTION(execSetHorizontalAlignment); \
	DECLARE_FUNCTION(execSetPadding);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULoadGuardSlot(); \
	friend struct Z_Construct_UClass_ULoadGuardSlot_Statics; \
public: \
	DECLARE_CLASS(ULoadGuardSlot, UPanelSlot, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(ULoadGuardSlot)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_INCLASS \
private: \
	static void StaticRegisterNativesULoadGuardSlot(); \
	friend struct Z_Construct_UClass_ULoadGuardSlot_Statics; \
public: \
	DECLARE_CLASS(ULoadGuardSlot, UPanelSlot, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(ULoadGuardSlot)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULoadGuardSlot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULoadGuardSlot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULoadGuardSlot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULoadGuardSlot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULoadGuardSlot(ULoadGuardSlot&&); \
	NO_API ULoadGuardSlot(const ULoadGuardSlot&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULoadGuardSlot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULoadGuardSlot(ULoadGuardSlot&&); \
	NO_API ULoadGuardSlot(const ULoadGuardSlot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULoadGuardSlot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULoadGuardSlot); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULoadGuardSlot)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Padding() { return STRUCT_OFFSET(ULoadGuardSlot, Padding); } \
	FORCEINLINE static uint32 __PPO__HorizontalAlignment() { return STRUCT_OFFSET(ULoadGuardSlot, HorizontalAlignment); } \
	FORCEINLINE static uint32 __PPO__VerticalAlignment() { return STRUCT_OFFSET(ULoadGuardSlot, VerticalAlignment); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_94_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_97_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class ULoadGuardSlot>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBP_GuardAndLoadAsset); \
	DECLARE_FUNCTION(execIsLoading); \
	DECLARE_FUNCTION(execSetIsLoading); \
	DECLARE_FUNCTION(execSetLoadingText);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBP_GuardAndLoadAsset); \
	DECLARE_FUNCTION(execIsLoading); \
	DECLARE_FUNCTION(execSetIsLoading); \
	DECLARE_FUNCTION(execSetLoadingText);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UCommonLoadGuard, NO_API)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonLoadGuard(); \
	friend struct Z_Construct_UClass_UCommonLoadGuard_Statics; \
public: \
	DECLARE_CLASS(UCommonLoadGuard, UContentWidget, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonLoadGuard) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_ARCHIVESERIALIZER \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_INCLASS \
private: \
	static void StaticRegisterNativesUCommonLoadGuard(); \
	friend struct Z_Construct_UClass_UCommonLoadGuard_Statics; \
public: \
	DECLARE_CLASS(UCommonLoadGuard, UContentWidget, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonLoadGuard) \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_ARCHIVESERIALIZER \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonLoadGuard(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonLoadGuard) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonLoadGuard); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonLoadGuard); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonLoadGuard(UCommonLoadGuard&&); \
	NO_API UCommonLoadGuard(const UCommonLoadGuard&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonLoadGuard(UCommonLoadGuard&&); \
	NO_API UCommonLoadGuard(const UCommonLoadGuard&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonLoadGuard); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonLoadGuard); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonLoadGuard)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LoadingBackgroundBrush() { return STRUCT_OFFSET(UCommonLoadGuard, LoadingBackgroundBrush); } \
	FORCEINLINE static uint32 __PPO__ThrobberAlignment() { return STRUCT_OFFSET(UCommonLoadGuard, ThrobberAlignment); } \
	FORCEINLINE static uint32 __PPO__ThrobberPadding() { return STRUCT_OFFSET(UCommonLoadGuard, ThrobberPadding); } \
	FORCEINLINE static uint32 __PPO__LoadingText() { return STRUCT_OFFSET(UCommonLoadGuard, LoadingText); } \
	FORCEINLINE static uint32 __PPO__TextStyle() { return STRUCT_OFFSET(UCommonLoadGuard, TextStyle); } \
	FORCEINLINE static uint32 __PPO__BP_OnLoadingStateChanged() { return STRUCT_OFFSET(UCommonLoadGuard, BP_OnLoadingStateChanged); } \
	FORCEINLINE static uint32 __PPO__SpinnerMaterialPath() { return STRUCT_OFFSET(UCommonLoadGuard, SpinnerMaterialPath); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_144_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h_147_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonLoadGuard>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLoadGuard_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
