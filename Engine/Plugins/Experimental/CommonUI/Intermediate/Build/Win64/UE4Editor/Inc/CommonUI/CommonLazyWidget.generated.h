// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUserWidget;
#ifdef COMMONUI_CommonLazyWidget_generated_h
#error "CommonLazyWidget.generated.h already included, missing '#pragma once' in CommonLazyWidget.h"
#endif
#define COMMONUI_CommonLazyWidget_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsLoading); \
	DECLARE_FUNCTION(execGetContent); \
	DECLARE_FUNCTION(execSetLazyContent);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsLoading); \
	DECLARE_FUNCTION(execGetContent); \
	DECLARE_FUNCTION(execSetLazyContent);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonLazyWidget(); \
	friend struct Z_Construct_UClass_UCommonLazyWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonLazyWidget, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonLazyWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUCommonLazyWidget(); \
	friend struct Z_Construct_UClass_UCommonLazyWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonLazyWidget, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonLazyWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonLazyWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonLazyWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonLazyWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonLazyWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonLazyWidget(UCommonLazyWidget&&); \
	NO_API UCommonLazyWidget(const UCommonLazyWidget&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonLazyWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonLazyWidget(UCommonLazyWidget&&); \
	NO_API UCommonLazyWidget(const UCommonLazyWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonLazyWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonLazyWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonLazyWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LoadingBackgroundBrush() { return STRUCT_OFFSET(UCommonLazyWidget, LoadingBackgroundBrush); } \
	FORCEINLINE static uint32 __PPO__Content() { return STRUCT_OFFSET(UCommonLazyWidget, Content); } \
	FORCEINLINE static uint32 __PPO__BP_OnLoadingStateChanged() { return STRUCT_OFFSET(UCommonLazyWidget, BP_OnLoadingStateChanged); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_17_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonLazyWidget."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonLazyWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonLazyWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
