// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonHierarchicalScrollBox_generated_h
#error "CommonHierarchicalScrollBox.generated.h already included, missing '#pragma once' in CommonHierarchicalScrollBox.h"
#endif
#define COMMONUI_CommonHierarchicalScrollBox_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonHierarchicalScrollBox(); \
	friend struct Z_Construct_UClass_UCommonHierarchicalScrollBox_Statics; \
public: \
	DECLARE_CLASS(UCommonHierarchicalScrollBox, UScrollBox, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonHierarchicalScrollBox)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUCommonHierarchicalScrollBox(); \
	friend struct Z_Construct_UClass_UCommonHierarchicalScrollBox_Statics; \
public: \
	DECLARE_CLASS(UCommonHierarchicalScrollBox, UScrollBox, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonHierarchicalScrollBox)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonHierarchicalScrollBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonHierarchicalScrollBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonHierarchicalScrollBox); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonHierarchicalScrollBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonHierarchicalScrollBox(UCommonHierarchicalScrollBox&&); \
	NO_API UCommonHierarchicalScrollBox(const UCommonHierarchicalScrollBox&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonHierarchicalScrollBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonHierarchicalScrollBox(UCommonHierarchicalScrollBox&&); \
	NO_API UCommonHierarchicalScrollBox(const UCommonHierarchicalScrollBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonHierarchicalScrollBox); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonHierarchicalScrollBox); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonHierarchicalScrollBox)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_13_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonHierarchicalScrollBox."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonHierarchicalScrollBox>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonHierarchicalScrollBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
