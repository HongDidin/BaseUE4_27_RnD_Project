// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonVisibilityWidgetBase_generated_h
#error "CommonVisibilityWidgetBase.generated.h already included, missing '#pragma once' in CommonVisibilityWidgetBase.h"
#endif
#define COMMONUI_CommonVisibilityWidgetBase_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRegisteredPlatforms);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRegisteredPlatforms);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonVisibilityWidgetBase(); \
	friend struct Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics; \
public: \
	DECLARE_CLASS(UCommonVisibilityWidgetBase, UCommonBorder, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVisibilityWidgetBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCommonVisibilityWidgetBase(); \
	friend struct Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics; \
public: \
	DECLARE_CLASS(UCommonVisibilityWidgetBase, UCommonBorder, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVisibilityWidgetBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonVisibilityWidgetBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVisibilityWidgetBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVisibilityWidgetBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVisibilityWidgetBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVisibilityWidgetBase(UCommonVisibilityWidgetBase&&); \
	NO_API UCommonVisibilityWidgetBase(const UCommonVisibilityWidgetBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonVisibilityWidgetBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVisibilityWidgetBase(UCommonVisibilityWidgetBase&&); \
	NO_API UCommonVisibilityWidgetBase(const UCommonVisibilityWidgetBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVisibilityWidgetBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVisibilityWidgetBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVisibilityWidgetBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_14_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonVisibilityWidgetBase."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonVisibilityWidgetBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilityWidgetBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
