// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonRotator_generated_h
#error "CommonRotator.generated.h already included, missing '#pragma once' in CommonRotator.h"
#endif
#define COMMONUI_CommonRotator_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_15_DELEGATE \
struct _Script_CommonUI_eventOnRotated_Parms \
{ \
	int32 Value; \
}; \
static inline void FOnRotated_DelegateWrapper(const FMulticastScriptDelegate& OnRotated, int32 Value) \
{ \
	_Script_CommonUI_eventOnRotated_Parms Parms; \
	Parms.Value=Value; \
	OnRotated.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execShiftTextRight); \
	DECLARE_FUNCTION(execShiftTextLeft); \
	DECLARE_FUNCTION(execGetSelectedIndex); \
	DECLARE_FUNCTION(execSetSelectedItem); \
	DECLARE_FUNCTION(execGetSelectedText); \
	DECLARE_FUNCTION(execPopulateTextLabels);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShiftTextRight); \
	DECLARE_FUNCTION(execShiftTextLeft); \
	DECLARE_FUNCTION(execGetSelectedIndex); \
	DECLARE_FUNCTION(execSetSelectedItem); \
	DECLARE_FUNCTION(execGetSelectedText); \
	DECLARE_FUNCTION(execPopulateTextLabels);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_EVENT_PARMS \
	struct CommonRotator_eventBP_OnOptionSelected_Parms \
	{ \
		int32 Index; \
	}; \
	struct CommonRotator_eventBP_OnOptionsPopulated_Parms \
	{ \
		int32 Count; \
	};


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonRotator(); \
	friend struct Z_Construct_UClass_UCommonRotator_Statics; \
public: \
	DECLARE_CLASS(UCommonRotator, UCommonButtonBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonRotator)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUCommonRotator(); \
	friend struct Z_Construct_UClass_UCommonRotator_Statics; \
public: \
	DECLARE_CLASS(UCommonRotator, UCommonButtonBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonRotator)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonRotator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonRotator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonRotator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonRotator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonRotator(UCommonRotator&&); \
	NO_API UCommonRotator(const UCommonRotator&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonRotator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonRotator(UCommonRotator&&); \
	NO_API UCommonRotator(const UCommonRotator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonRotator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonRotator); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonRotator)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MyText() { return STRUCT_OFFSET(UCommonRotator, MyText); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_22_PROLOG \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonRotator."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonRotator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonRotator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
