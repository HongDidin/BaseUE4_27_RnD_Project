// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonAnimatedSwitcher_generated_h
#error "CommonAnimatedSwitcher.generated.h already included, missing '#pragma once' in CommonAnimatedSwitcher.h"
#endif
#define COMMONUI_CommonAnimatedSwitcher_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetDisableTransitionAnimation); \
	DECLARE_FUNCTION(execHasWidgets); \
	DECLARE_FUNCTION(execActivatePreviousWidget); \
	DECLARE_FUNCTION(execActivateNextWidget);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetDisableTransitionAnimation); \
	DECLARE_FUNCTION(execHasWidgets); \
	DECLARE_FUNCTION(execActivatePreviousWidget); \
	DECLARE_FUNCTION(execActivateNextWidget);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonAnimatedSwitcher(); \
	friend struct Z_Construct_UClass_UCommonAnimatedSwitcher_Statics; \
public: \
	DECLARE_CLASS(UCommonAnimatedSwitcher, UWidgetSwitcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonAnimatedSwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCommonAnimatedSwitcher(); \
	friend struct Z_Construct_UClass_UCommonAnimatedSwitcher_Statics; \
public: \
	DECLARE_CLASS(UCommonAnimatedSwitcher, UWidgetSwitcher, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonAnimatedSwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonAnimatedSwitcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonAnimatedSwitcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonAnimatedSwitcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonAnimatedSwitcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonAnimatedSwitcher(UCommonAnimatedSwitcher&&); \
	NO_API UCommonAnimatedSwitcher(const UCommonAnimatedSwitcher&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonAnimatedSwitcher(UCommonAnimatedSwitcher&&); \
	NO_API UCommonAnimatedSwitcher(const UCommonAnimatedSwitcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonAnimatedSwitcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonAnimatedSwitcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonAnimatedSwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TransitionType() { return STRUCT_OFFSET(UCommonAnimatedSwitcher, TransitionType); } \
	FORCEINLINE static uint32 __PPO__TransitionCurveType() { return STRUCT_OFFSET(UCommonAnimatedSwitcher, TransitionCurveType); } \
	FORCEINLINE static uint32 __PPO__TransitionDuration() { return STRUCT_OFFSET(UCommonAnimatedSwitcher, TransitionDuration); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_14_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonAnimatedSwitcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonAnimatedSwitcher_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
