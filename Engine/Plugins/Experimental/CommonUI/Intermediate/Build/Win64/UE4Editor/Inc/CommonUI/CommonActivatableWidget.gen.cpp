// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonActivatableWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonActivatableWidget() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidget_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActivatableWidget();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUserWidget();
	UMG_API UClass* Z_Construct_UClass_UWidget_NoRegister();
	UMG_API UEnum* Z_Construct_UEnum_UMG_ESlateVisibility();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "OnWidgetActivationChanged__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UCommonActivatableWidget::execDeactivateWidget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeactivateWidget();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActivatableWidget::execActivateWidget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ActivateWidget();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActivatableWidget::execIsActivated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsActivated();
		P_NATIVE_END;
	}
	static FName NAME_UCommonActivatableWidget_BP_GetDesiredFocusTarget = FName(TEXT("BP_GetDesiredFocusTarget"));
	UWidget* UCommonActivatableWidget::BP_GetDesiredFocusTarget() const
	{
		CommonActivatableWidget_eventBP_GetDesiredFocusTarget_Parms Parms;
		const_cast<UCommonActivatableWidget*>(this)->ProcessEvent(FindFunctionChecked(NAME_UCommonActivatableWidget_BP_GetDesiredFocusTarget),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UCommonActivatableWidget_BP_OnActivated = FName(TEXT("BP_OnActivated"));
	void UCommonActivatableWidget::BP_OnActivated()
	{
		ProcessEvent(FindFunctionChecked(NAME_UCommonActivatableWidget_BP_OnActivated),NULL);
	}
	static FName NAME_UCommonActivatableWidget_BP_OnDeactivated = FName(TEXT("BP_OnDeactivated"));
	void UCommonActivatableWidget::BP_OnDeactivated()
	{
		ProcessEvent(FindFunctionChecked(NAME_UCommonActivatableWidget_BP_OnDeactivated),NULL);
	}
	static FName NAME_UCommonActivatableWidget_BP_OnHandleBackAction = FName(TEXT("BP_OnHandleBackAction"));
	bool UCommonActivatableWidget::BP_OnHandleBackAction()
	{
		CommonActivatableWidget_eventBP_OnHandleBackAction_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_UCommonActivatableWidget_BP_OnHandleBackAction),&Parms);
		return !!Parms.ReturnValue;
	}
	void UCommonActivatableWidget::StaticRegisterNativesUCommonActivatableWidget()
	{
		UClass* Class = UCommonActivatableWidget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ActivateWidget", &UCommonActivatableWidget::execActivateWidget },
			{ "DeactivateWidget", &UCommonActivatableWidget::execDeactivateWidget },
			{ "IsActivated", &UCommonActivatableWidget::execIsActivated },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "ActivateWidget", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActivatableWidget_eventBP_GetDesiredFocusTarget_Parms, ReturnValue), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "Comment", "/** \n\x09 * Implement to provide the desired widget to focus if/when this activatable becomes the primary active widget.\n\x09 * Note: This is a fallback used only if the native class parentage does not provide a target.\n\x09 */" },
		{ "DisplayName", "Get Desired Focus Target" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "Implement to provide the desired widget to focus if/when this activatable becomes the primary active widget.\nNote: This is a fallback used only if the native class parentage does not provide a target." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "BP_GetDesiredFocusTarget", nullptr, nullptr, sizeof(CommonActivatableWidget_eventBP_GetDesiredFocusTarget_Parms), Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "DisplayName", "On Activated" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "BP_OnActivated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "DisplayName", "On Deactivated" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "BP_OnDeactivated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics
	{
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonActivatableWidget_eventBP_OnHandleBackAction_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonActivatableWidget_eventBP_OnHandleBackAction_Parms), &Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "Comment", "/** \n\x09 * Override in BP implementations to provide custom behavior when receiving a back action \n\x09 * Note: Only called if native code in the base class hasn't handled it in NativeOnHandleBackAction \n\x09 */" },
		{ "DisplayName", "On Handle Back Action" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "Override in BP implementations to provide custom behavior when receiving a back action\nNote: Only called if native code in the base class hasn't handled it in NativeOnHandleBackAction" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "BP_OnHandleBackAction", nullptr, nullptr, sizeof(CommonActivatableWidget_eventBP_OnHandleBackAction_Parms), Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "DeactivateWidget", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics
	{
		struct CommonActivatableWidget_eventIsActivated_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonActivatableWidget_eventIsActivated_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonActivatableWidget_eventIsActivated_Parms), &Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::Function_MetaDataParams[] = {
		{ "Category", "ActivatableWidget" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActivatableWidget, nullptr, "IsActivated", nullptr, nullptr, sizeof(CommonActivatableWidget_eventIsActivated_Parms), Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActivatableWidget_IsActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActivatableWidget_IsActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonActivatableWidget_NoRegister()
	{
		return UCommonActivatableWidget::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActivatableWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoActivate_MetaData[];
#endif
		static void NewProp_bAutoActivate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoActivate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsBackHandler_MetaData[];
#endif
		static void NewProp_bIsBackHandler_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsBackHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsActivationFocus_MetaData[];
#endif
		static void NewProp_bSupportsActivationFocus_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsActivationFocus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsModal_MetaData[];
#endif
		static void NewProp_bIsModal_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsModal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoRestoreFocus_MetaData[];
#endif
		static void NewProp_bAutoRestoreFocus_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoRestoreFocus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSetVisibilityOnActivated_MetaData[];
#endif
		static void NewProp_bSetVisibilityOnActivated_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetVisibilityOnActivated;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ActivatedVisibility_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivatedVisibility_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ActivatedVisibility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSetVisibilityOnDeactivated_MetaData[];
#endif
		static void NewProp_bSetVisibilityOnDeactivated_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetVisibilityOnDeactivated;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeactivatedVisibility_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeactivatedVisibility_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeactivatedVisibility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BP_OnWidgetActivated_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_BP_OnWidgetActivated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BP_OnWidgetDeactivated_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_BP_OnWidgetDeactivated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsActive_MetaData[];
#endif
		static void NewProp_bIsActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsActive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActivatableWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonActivatableWidget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonActivatableWidget_ActivateWidget, "ActivateWidget" }, // 4152220334
		{ &Z_Construct_UFunction_UCommonActivatableWidget_BP_GetDesiredFocusTarget, "BP_GetDesiredFocusTarget" }, // 1415692818
		{ &Z_Construct_UFunction_UCommonActivatableWidget_BP_OnActivated, "BP_OnActivated" }, // 2458021802
		{ &Z_Construct_UFunction_UCommonActivatableWidget_BP_OnDeactivated, "BP_OnDeactivated" }, // 943360502
		{ &Z_Construct_UFunction_UCommonActivatableWidget_BP_OnHandleBackAction, "BP_OnHandleBackAction" }, // 967522117
		{ &Z_Construct_UFunction_UCommonActivatableWidget_DeactivateWidget, "DeactivateWidget" }, // 3739694569
		{ &Z_Construct_UFunction_UCommonActivatableWidget_IsActivated, "IsActivated" }, // 3111968450
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * The base for widgets that are capable of being \"activated\" and \"deactivated\" during their lifetime without being otherwise modified or destroyed. \n *\n * This is generally desired for one or more of the following purposes:\n *\x09- This widget can turn on/off without being removed from the hierarchy (or otherwise reconstructing the underlying SWidgets), so Construct/Destruct are insufficient\n *\x09- You'd like to be able to \"go back\" from this widget, whether that means back a breadcrumb, closing a modal, or something else. This is built-in here.\n *\x09- This widget's place in the hierarchy is such that it defines a meaningful node-point in the tree of activatable widgets through which input is routed to all widgets.\n *\n * By default, an activatable widget:\n *\x09- Is not automatically activated upon construction\n *\x09- Does not register to receive back actions (or any other actions, for that matter)\n *\x09- If classified as a back handler, is automatically deactivated (but not destroyed) when it receives a back action\n * \n * Note that removing an activatable widget from the UI (i.e. triggering Destruct()) will always deactivate it, even if the UWidget is not destroyed.\n * Re-constructing the underlying SWidget will only result in re-activation if auto-activate is enabled.\n *\n * TODO: ADD MORE INFO ON INPUTS\n */" },
		{ "IncludePath", "CommonActivatableWidget.h" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "The base for widgets that are capable of being \"activated\" and \"deactivated\" during their lifetime without being otherwise modified or destroyed.\n\nThis is generally desired for one or more of the following purposes:\n    - This widget can turn on/off without being removed from the hierarchy (or otherwise reconstructing the underlying SWidgets), so Construct/Destruct are insufficient\n    - You'd like to be able to \"go back\" from this widget, whether that means back a breadcrumb, closing a modal, or something else. This is built-in here.\n    - This widget's place in the hierarchy is such that it defines a meaningful node-point in the tree of activatable widgets through which input is routed to all widgets.\n\nBy default, an activatable widget:\n    - Is not automatically activated upon construction\n    - Does not register to receive back actions (or any other actions, for that matter)\n    - If classified as a back handler, is automatically deactivated (but not destroyed) when it receives a back action\n\nNote that removing an activatable widget from the UI (i.e. triggering Destruct()) will always deactivate it, even if the UWidget is not destroyed.\nRe-constructing the underlying SWidget will only result in re-activation if auto-activate is enabled.\n\nTODO: ADD MORE INFO ON INPUTS" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate_MetaData[] = {
		{ "Category", "Activation" },
		{ "Comment", "/** True to automatically activate upon construction */" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "True to automatically activate upon construction" },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bAutoActivate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate = { "bAutoActivate", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler_MetaData[] = {
		{ "Category", "Activation" },
		{ "Comment", "/** True to receive \"Back\" actions automatically. Custom back handler behavior can be provided, default is to deactivate. */" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "True to receive \"Back\" actions automatically. Custom back handler behavior can be provided, default is to deactivate." },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bIsBackHandler = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler = { "bIsBackHandler", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus_MetaData[] = {
		{ "Category", "Activation" },
		{ "Comment", "/**\n\x09 * True if this widget is a candidate to receive/route focus or specify a desired UIInputConfig when active.\n\x09 * Primary reason for disabling is for utility sub-widgets within a larger screen that possess actions, but are never\n\x09 * intended to be involved in navigation or dictate changes to the active UI input config.\n\x09 */" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "True if this widget is a candidate to receive/route focus or specify a desired UIInputConfig when active.\nPrimary reason for disabling is for utility sub-widgets within a larger screen that possess actions, but are never\nintended to be involved in navigation or dictate changes to the active UI input config." },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bSupportsActivationFocus = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus = { "bSupportsActivationFocus", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal_MetaData[] = {
		{ "Category", "Activation" },
		{ "Comment", "/** \n\x09 * True to have this widget be treated as a root node for input routing, regardless of its actual parentage.\n\x09 * Should seldom be needed, but useful in cases where a child widget should prevent all action processing by parents, even though they remain active (ex: modal popup menu).\n\x09 */" },
		{ "EditCondition", "bSupportsActivationFocus" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "True to have this widget be treated as a root node for input routing, regardless of its actual parentage.\nShould seldom be needed, but useful in cases where a child widget should prevent all action processing by parents, even though they remain active (ex: modal popup menu)." },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bIsModal = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal = { "bIsModal", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus_MetaData[] = {
		{ "Category", "Activation" },
		{ "Comment", "/** \n\x09 * True to prefer automatically restoring focus to the widget that was focused when this widget last became the non-leafmost-active-widget.\n\x09 * If true and a valid restoration candidate exists, we'll use that. If it doesn't, we rely on GetDesiredFocusTarget()\n\x09 * If false, we simply always rely on GetDesiredFocusTarget()\n\x09 */" },
		{ "EditCondition", "bSupportsActivationFocus" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "True to prefer automatically restoring focus to the widget that was focused when this widget last became the non-leafmost-active-widget.\nIf true and a valid restoration candidate exists, we'll use that. If it doesn't, we rely on GetDesiredFocusTarget()\nIf false, we simply always rely on GetDesiredFocusTarget()" },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bAutoRestoreFocus = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus = { "bAutoRestoreFocus", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated_MetaData[] = {
		{ "Category", "Activation" },
		{ "InlineEditConditionToggle", "ActivatedVisibility" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bSetVisibilityOnActivated = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated = { "bSetVisibilityOnActivated", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility_MetaData[] = {
		{ "Category", "Activation" },
		{ "EditCondition", "bSetVisibilityOnActivated" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility = { "ActivatedVisibility", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidget, ActivatedVisibility), Z_Construct_UEnum_UMG_ESlateVisibility, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated_MetaData[] = {
		{ "Category", "Activation" },
		{ "InlineEditConditionToggle", "DeactivatedVisibility" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bSetVisibilityOnDeactivated = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated = { "bSetVisibilityOnDeactivated", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility_MetaData[] = {
		{ "Category", "Activation" },
		{ "EditCondition", "bSetVisibilityOnDeactivated" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility = { "DeactivatedVisibility", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidget, DeactivatedVisibility), Z_Construct_UEnum_UMG_ESlateVisibility, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetActivated_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Events" },
		{ "Comment", "/** Fires when the widget is activated. */" },
		{ "DisplayName", "On Widget Activated" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "Fires when the widget is activated." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetActivated = { "BP_OnWidgetActivated", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidget, BP_OnWidgetActivated), Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetActivated_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetActivated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetDeactivated_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Events" },
		{ "Comment", "/** Fires when the widget is deactivated. */" },
		{ "DisplayName", "On Widget Deactivated" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
		{ "ToolTip", "Fires when the widget is deactivated." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetDeactivated = { "BP_OnWidgetDeactivated", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActivatableWidget, BP_OnWidgetDeactivated), Z_Construct_UDelegateFunction_CommonUI_OnWidgetActivationChanged__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetDeactivated_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetDeactivated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "ActivatableWidget" },
		{ "ModuleRelativePath", "Public/CommonActivatableWidget.h" },
	};
#endif
	void Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive_SetBit(void* Obj)
	{
		((UCommonActivatableWidget*)Obj)->bIsActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive = { "bIsActive", nullptr, (EPropertyFlags)0x0040000000000014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonActivatableWidget), &Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonActivatableWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoActivate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsBackHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSupportsActivationFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsModal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bAutoRestoreFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnActivated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_ActivatedVisibility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bSetVisibilityOnDeactivated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_DeactivatedVisibility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetActivated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_BP_OnWidgetDeactivated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActivatableWidget_Statics::NewProp_bIsActive,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActivatableWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonActivatableWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActivatableWidget_Statics::ClassParams = {
		&UCommonActivatableWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonActivatableWidget_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActivatableWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActivatableWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActivatableWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActivatableWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActivatableWidget, 1169104006);
	template<> COMMONUI_API UClass* StaticClass<UCommonActivatableWidget>()
	{
		return UCommonActivatableWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActivatableWidget(Z_Construct_UClass_UCommonActivatableWidget, &UCommonActivatableWidget::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActivatableWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActivatableWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
