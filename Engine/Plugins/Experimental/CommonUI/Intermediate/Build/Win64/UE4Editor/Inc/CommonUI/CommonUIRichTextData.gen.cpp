// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonUIRichTextData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUIRichTextData() {}
// Cross Module References
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FRichTextIconData();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIRichTextData_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonUIRichTextData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FRichTextIconData>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FRichTextIconData cannot be polymorphic unless super FTableRowBase is polymorphic");

class UScriptStruct* FRichTextIconData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FRichTextIconData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRichTextIconData, Z_Construct_UPackage__Script_CommonUI(), TEXT("RichTextIconData"), sizeof(FRichTextIconData), Get_Z_Construct_UScriptStruct_FRichTextIconData_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FRichTextIconData>()
{
	return FRichTextIconData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRichTextIconData(FRichTextIconData::StaticStruct, TEXT("/Script/CommonUI"), TEXT("RichTextIconData"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFRichTextIconData
{
	FScriptStruct_CommonUI_StaticRegisterNativesFRichTextIconData()
	{
		UScriptStruct::DeferCppStructOps<FRichTextIconData>(FName(TEXT("RichTextIconData")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFRichTextIconData;
	struct Z_Construct_UScriptStruct_FRichTextIconData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResourceObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_ResourceObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRichTextIconData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CommonUIRichTextData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRichTextIconData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "RichText Icon" },
		{ "ModuleRelativePath", "Public/CommonUIRichTextData.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRichTextIconData, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ResourceObject_MetaData[] = {
		{ "AllowedClasses", "Texture2D,MaterialInterface,SlateTextureAtlasInterface" },
		{ "Category", "RichText Icon" },
		{ "DisallowedClasses", "MediaTexture" },
		{ "DisplayName", "Image" },
		{ "DisplayThumbnail", "true" },
		{ "ModuleRelativePath", "Public/CommonUIRichTextData.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ResourceObject = { "ResourceObject", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRichTextIconData, ResourceObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ResourceObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ResourceObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ImageSize_MetaData[] = {
		{ "Category", "RichText Icon" },
		{ "ModuleRelativePath", "Public/CommonUIRichTextData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ImageSize = { "ImageSize", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRichTextIconData, ImageSize), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ImageSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ImageSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRichTextIconData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ResourceObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRichTextIconData_Statics::NewProp_ImageSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRichTextIconData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"RichTextIconData",
		sizeof(FRichTextIconData),
		alignof(FRichTextIconData),
		Z_Construct_UScriptStruct_FRichTextIconData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRichTextIconData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRichTextIconData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRichTextIconData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRichTextIconData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRichTextIconData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RichTextIconData"), sizeof(FRichTextIconData), Get_Z_Construct_UScriptStruct_FRichTextIconData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRichTextIconData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRichTextIconData_Hash() { return 2750795161U; }
	void UCommonUIRichTextData::StaticRegisterNativesUCommonUIRichTextData()
	{
	}
	UClass* Z_Construct_UClass_UCommonUIRichTextData_NoRegister()
	{
		return UCommonUIRichTextData::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUIRichTextData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InlineIconSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InlineIconSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUIRichTextData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIRichTextData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "Common UI" },
		{ "Comment", "/* Derive from this class for rich text data per game\n * it is referenced in Common UI Settings, found in project settings UI\n */" },
		{ "IncludePath", "CommonUIRichTextData.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonUIRichTextData.h" },
		{ "ToolTip", "Derive from this class for rich text data per game\n* it is referenced in Common UI Settings, found in project settings UI" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIRichTextData_Statics::NewProp_InlineIconSet_MetaData[] = {
		{ "Category", "Inline Icons" },
		{ "ModuleRelativePath", "Public/CommonUIRichTextData.h" },
		{ "RowType", "RichTextIconData" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonUIRichTextData_Statics::NewProp_InlineIconSet = { "InlineIconSet", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIRichTextData, InlineIconSet), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonUIRichTextData_Statics::NewProp_InlineIconSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIRichTextData_Statics::NewProp_InlineIconSet_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonUIRichTextData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIRichTextData_Statics::NewProp_InlineIconSet,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUIRichTextData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUIRichTextData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUIRichTextData_Statics::ClassParams = {
		&UCommonUIRichTextData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonUIRichTextData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIRichTextData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUIRichTextData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIRichTextData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUIRichTextData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUIRichTextData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUIRichTextData, 2260681249);
	template<> COMMONUI_API UClass* StaticClass<UCommonUIRichTextData>()
	{
		return UCommonUIRichTextData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUIRichTextData(Z_Construct_UClass_UCommonUIRichTextData, &UCommonUIRichTextData::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonUIRichTextData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUIRichTextData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
