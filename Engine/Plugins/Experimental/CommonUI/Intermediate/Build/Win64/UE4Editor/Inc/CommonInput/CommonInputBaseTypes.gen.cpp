// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonInput/Public/CommonInputBaseTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonInputBaseTypes() {}
// Cross Module References
	COMMONINPUT_API UEnum* Z_Construct_UEnum_CommonInput_ECommonInputType();
	UPackage* Z_Construct_UPackage__Script_CommonInput();
	COMMONINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputPlatformBaseData();
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonInputBaseControllerData_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COMMONINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
	COMMONINPUT_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration();
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonUIInputData_NoRegister();
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonUIInputData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDataTableRowHandle();
	COMMONINPUT_API UClass* Z_Construct_UClass_UCommonInputBaseControllerData();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
	static UEnum* ECommonInputType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonInput_ECommonInputType, Z_Construct_UPackage__Script_CommonInput(), TEXT("ECommonInputType"));
		}
		return Singleton;
	}
	template<> COMMONINPUT_API UEnum* StaticEnum<ECommonInputType>()
	{
		return ECommonInputType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECommonInputType(ECommonInputType_StaticEnum, TEXT("/Script/CommonInput"), TEXT("ECommonInputType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonInput_ECommonInputType_Hash() { return 234243071U; }
	UEnum* Z_Construct_UEnum_CommonInput_ECommonInputType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonInput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECommonInputType"), 0, Get_Z_Construct_UEnum_CommonInput_ECommonInputType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECommonInputType::MouseAndKeyboard", (int64)ECommonInputType::MouseAndKeyboard },
				{ "ECommonInputType::Gamepad", (int64)ECommonInputType::Gamepad },
				{ "ECommonInputType::Touch", (int64)ECommonInputType::Touch },
				{ "ECommonInputType::Count", (int64)ECommonInputType::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Count.Name", "ECommonInputType::Count" },
				{ "Gamepad.Name", "ECommonInputType::Gamepad" },
				{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
				{ "MouseAndKeyboard.Name", "ECommonInputType::MouseAndKeyboard" },
				{ "Touch.Name", "ECommonInputType::Touch" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonInput,
				nullptr,
				"ECommonInputType",
				"ECommonInputType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FCommonInputPlatformBaseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONINPUT_API uint32 Get_Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData, Z_Construct_UPackage__Script_CommonInput(), TEXT("CommonInputPlatformBaseData"), sizeof(FCommonInputPlatformBaseData), Get_Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Hash());
	}
	return Singleton;
}
template<> COMMONINPUT_API UScriptStruct* StaticStruct<FCommonInputPlatformBaseData>()
{
	return FCommonInputPlatformBaseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonInputPlatformBaseData(FCommonInputPlatformBaseData::StaticStruct, TEXT("/Script/CommonInput"), TEXT("CommonInputPlatformBaseData"), false, nullptr, nullptr);
static struct FScriptStruct_CommonInput_StaticRegisterNativesFCommonInputPlatformBaseData
{
	FScriptStruct_CommonInput_StaticRegisterNativesFCommonInputPlatformBaseData()
	{
		UScriptStruct::DeferCppStructOps<FCommonInputPlatformBaseData>(FName(TEXT("CommonInputPlatformBaseData")));
	}
} ScriptStruct_CommonInput_StaticRegisterNativesFCommonInputPlatformBaseData;
	struct Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupported_MetaData[];
#endif
		static void NewProp_bSupported_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupported;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultInputType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultInputType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DefaultInputType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsMouseAndKeyboard_MetaData[];
#endif
		static void NewProp_bSupportsMouseAndKeyboard_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsMouseAndKeyboard;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsGamepad_MetaData[];
#endif
		static void NewProp_bSupportsGamepad_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsGamepad;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultGamepadName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DefaultGamepadName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCanChangeGamepadType_MetaData[];
#endif
		static void NewProp_bCanChangeGamepadType_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanChangeGamepadType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsTouch_MetaData[];
#endif
		static void NewProp_bSupportsTouch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsTouch;
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_ControllerData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControllerData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ControllerData;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ControllerDataClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControllerDataClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ControllerDataClasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonInputPlatformBaseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported_SetBit(void* Obj)
	{
		((FCommonInputPlatformBaseData*)Obj)->bSupported = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported = { "bSupported", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonInputPlatformBaseData), &Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType_MetaData[] = {
		{ "Category", "Properties" },
		{ "EditCondition", "bSupported" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType = { "DefaultInputType", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputPlatformBaseData, DefaultInputType), Z_Construct_UEnum_CommonInput_ECommonInputType, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard_MetaData[] = {
		{ "Category", "Properties" },
		{ "EditCondition", "bSupported" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard_SetBit(void* Obj)
	{
		((FCommonInputPlatformBaseData*)Obj)->bSupportsMouseAndKeyboard = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard = { "bSupportsMouseAndKeyboard", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonInputPlatformBaseData), &Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad_MetaData[] = {
		{ "Category", "Gamepad" },
		{ "EditCondition", "bSupported" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad_SetBit(void* Obj)
	{
		((FCommonInputPlatformBaseData*)Obj)->bSupportsGamepad = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad = { "bSupportsGamepad", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonInputPlatformBaseData), &Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultGamepadName_MetaData[] = {
		{ "Category", "Gamepad" },
		{ "EditCondition", "bSupportsGamepad" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultGamepadName = { "DefaultGamepadName", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputPlatformBaseData, DefaultGamepadName), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultGamepadName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultGamepadName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType_MetaData[] = {
		{ "Category", "Gamepad" },
		{ "EditCondition", "bSupportsGamepad" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType_SetBit(void* Obj)
	{
		((FCommonInputPlatformBaseData*)Obj)->bCanChangeGamepadType = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType = { "bCanChangeGamepadType", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonInputPlatformBaseData), &Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch_MetaData[] = {
		{ "Category", "Properties" },
		{ "EditCondition", "bSupported" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch_SetBit(void* Obj)
	{
		((FCommonInputPlatformBaseData*)Obj)->bSupportsTouch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch = { "bSupportsTouch", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonInputPlatformBaseData), &Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch_MetaData)) };
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData_Inner = { "ControllerData", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCommonInputBaseControllerData_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData_MetaData[] = {
		{ "Category", "Properties" },
		{ "EditCondition", "bSupported" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "TitleProperty", "Key" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData = { "ControllerData", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputPlatformBaseData, ControllerData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses_Inner = { "ControllerDataClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCommonInputBaseControllerData_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses = { "ControllerDataClasses", nullptr, (EPropertyFlags)0x0024080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputPlatformBaseData, ControllerDataClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupported,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultInputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsMouseAndKeyboard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsGamepad,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_DefaultGamepadName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bCanChangeGamepadType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_bSupportsTouch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::NewProp_ControllerDataClasses,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonInput,
		nullptr,
		&NewStructOps,
		"CommonInputPlatformBaseData",
		sizeof(FCommonInputPlatformBaseData),
		alignof(FCommonInputPlatformBaseData),
		Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonInputPlatformBaseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonInputPlatformBaseData"), sizeof(FCommonInputPlatformBaseData), Get_Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonInputPlatformBaseData_Hash() { return 4237974988U; }
class UScriptStruct* FCommonInputKeySetBrushConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONINPUT_API uint32 Get_Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration, Z_Construct_UPackage__Script_CommonInput(), TEXT("CommonInputKeySetBrushConfiguration"), sizeof(FCommonInputKeySetBrushConfiguration), Get_Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Hash());
	}
	return Singleton;
}
template<> COMMONINPUT_API UScriptStruct* StaticStruct<FCommonInputKeySetBrushConfiguration>()
{
	return FCommonInputKeySetBrushConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonInputKeySetBrushConfiguration(FCommonInputKeySetBrushConfiguration::StaticStruct, TEXT("/Script/CommonInput"), TEXT("CommonInputKeySetBrushConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_CommonInput_StaticRegisterNativesFCommonInputKeySetBrushConfiguration
{
	FScriptStruct_CommonInput_StaticRegisterNativesFCommonInputKeySetBrushConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FCommonInputKeySetBrushConfiguration>(FName(TEXT("CommonInputKeySetBrushConfiguration")));
	}
} ScriptStruct_CommonInput_StaticRegisterNativesFCommonInputKeySetBrushConfiguration;
	struct Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Keys_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Keys_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Keys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyBrush;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonInputKeySetBrushConfiguration>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys_Inner = { "Keys", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys_MetaData[] = {
		{ "Category", "Key Brush Configuration" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "TitleProperty", "KeyName" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys = { "Keys", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputKeySetBrushConfiguration, Keys), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_KeyBrush_MetaData[] = {
		{ "Category", "Key Brush Configuration" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_KeyBrush = { "KeyBrush", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputKeySetBrushConfiguration, KeyBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_KeyBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_KeyBrush_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_Keys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::NewProp_KeyBrush,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonInput,
		nullptr,
		&NewStructOps,
		"CommonInputKeySetBrushConfiguration",
		sizeof(FCommonInputKeySetBrushConfiguration),
		alignof(FCommonInputKeySetBrushConfiguration),
		Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonInputKeySetBrushConfiguration"), sizeof(FCommonInputKeySetBrushConfiguration), Get_Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration_Hash() { return 2701772223U; }
class UScriptStruct* FCommonInputKeyBrushConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONINPUT_API uint32 Get_Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration, Z_Construct_UPackage__Script_CommonInput(), TEXT("CommonInputKeyBrushConfiguration"), sizeof(FCommonInputKeyBrushConfiguration), Get_Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Hash());
	}
	return Singleton;
}
template<> COMMONINPUT_API UScriptStruct* StaticStruct<FCommonInputKeyBrushConfiguration>()
{
	return FCommonInputKeyBrushConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonInputKeyBrushConfiguration(FCommonInputKeyBrushConfiguration::StaticStruct, TEXT("/Script/CommonInput"), TEXT("CommonInputKeyBrushConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_CommonInput_StaticRegisterNativesFCommonInputKeyBrushConfiguration
{
	FScriptStruct_CommonInput_StaticRegisterNativesFCommonInputKeyBrushConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FCommonInputKeyBrushConfiguration>(FName(TEXT("CommonInputKeyBrushConfiguration")));
	}
} ScriptStruct_CommonInput_StaticRegisterNativesFCommonInputKeyBrushConfiguration;
	struct Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyBrush;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonInputKeyBrushConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "Key Brush Configuration" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputKeyBrushConfiguration, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_KeyBrush_MetaData[] = {
		{ "Category", "Key Brush Configuration" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_KeyBrush = { "KeyBrush", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputKeyBrushConfiguration, KeyBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_KeyBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_KeyBrush_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::NewProp_KeyBrush,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonInput,
		nullptr,
		&NewStructOps,
		"CommonInputKeyBrushConfiguration",
		sizeof(FCommonInputKeyBrushConfiguration),
		alignof(FCommonInputKeyBrushConfiguration),
		Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonInput();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonInputKeyBrushConfiguration"), sizeof(FCommonInputKeyBrushConfiguration), Get_Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration_Hash() { return 3675010347U; }
	void UCommonUIInputData::StaticRegisterNativesUCommonUIInputData()
	{
	}
	UClass* Z_Construct_UClass_UCommonUIInputData_NoRegister()
	{
		return UCommonUIInputData::StaticClass();
	}
	struct Z_Construct_UClass_UCommonUIInputData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultClickAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultClickAction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBackAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultBackAction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonUIInputData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "Common Input" },
		{ "ClassGroupNames", "Input" },
		{ "Comment", "/* Derive from this class to store the Input data. It is referenced in the Common Input Settings, found in the project settings UI. */" },
		{ "IncludePath", "CommonInputBaseTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "ToolTip", "Derive from this class to store the Input data. It is referenced in the Common Input Settings, found in the project settings UI." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultClickAction_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "RowType", "CommonInputActionDataBase" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultClickAction = { "DefaultClickAction", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIInputData, DefaultClickAction), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultClickAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultClickAction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultBackAction_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "RowType", "CommonInputActionDataBase" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultBackAction = { "DefaultBackAction", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonUIInputData, DefaultBackAction), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultBackAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultBackAction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonUIInputData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultClickAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonUIInputData_Statics::NewProp_DefaultBackAction,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonUIInputData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonUIInputData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonUIInputData_Statics::ClassParams = {
		&UCommonUIInputData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonUIInputData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputData_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonUIInputData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonUIInputData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonUIInputData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonUIInputData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonUIInputData, 1187368469);
	template<> COMMONINPUT_API UClass* StaticClass<UCommonUIInputData>()
	{
		return UCommonUIInputData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonUIInputData(Z_Construct_UClass_UCommonUIInputData, &UCommonUIInputData::StaticClass, TEXT("/Script/CommonInput"), TEXT("UCommonUIInputData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonUIInputData);
	DEFINE_FUNCTION(UCommonInputBaseControllerData::execGetRegisteredGamepads)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FName>*)Z_Param__Result=UCommonInputBaseControllerData::GetRegisteredGamepads();
		P_NATIVE_END;
	}
	void UCommonInputBaseControllerData::StaticRegisterNativesUCommonInputBaseControllerData()
	{
		UClass* Class = UCommonInputBaseControllerData::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetRegisteredGamepads", &UCommonInputBaseControllerData::execGetRegisteredGamepads },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics
	{
		struct CommonInputBaseControllerData_eventGetRegisteredGamepads_Parms
		{
			TArray<FName> ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000008000582, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonInputBaseControllerData_eventGetRegisteredGamepads_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonInputBaseControllerData, nullptr, "GetRegisteredGamepads", nullptr, nullptr, sizeof(CommonInputBaseControllerData_eventGetRegisteredGamepads_Parms), Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonInputBaseControllerData_NoRegister()
	{
		return UCommonInputBaseControllerData::StaticClass();
	}
	struct Z_Construct_UClass_UCommonInputBaseControllerData_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InputType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InputType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GamepadName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GamepadName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControllerTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_ControllerTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControllerButtonMaskTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_ControllerButtonMaskTexture;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputBrushDataMap_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputBrushDataMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputBrushDataMap;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputBrushKeySets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputBrushKeySets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputBrushKeySets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonInputBaseControllerData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonInput,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonInputBaseControllerData_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonInputBaseControllerData_GetRegisteredGamepads, "GetRegisteredGamepads" }, // 2726859875
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "Common Input" },
		{ "ClassGroupNames", "Input" },
		{ "Comment", "/* Derive from this class to store the Input data. It is referenced in the Common Input Settings, found in the project settings UI. */" },
		{ "IncludePath", "CommonInputBaseTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "ToolTip", "Derive from this class to store the Input data. It is referenced in the Common Input Settings, found in the project settings UI." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType = { "InputType", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputBaseControllerData, InputType), Z_Construct_UEnum_CommonInput_ECommonInputType, METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_GamepadName_MetaData[] = {
		{ "Category", "Properties" },
		{ "GetOptions", "GetRegisteredGamepads" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_GamepadName = { "GamepadName", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputBaseControllerData, GamepadName), METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_GamepadName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_GamepadName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerTexture_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerTexture = { "ControllerTexture", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputBaseControllerData, ControllerTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerButtonMaskTexture_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerButtonMaskTexture = { "ControllerButtonMaskTexture", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputBaseControllerData, ControllerButtonMaskTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerButtonMaskTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerButtonMaskTexture_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap_Inner = { "InputBrushDataMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCommonInputKeyBrushConfiguration, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "TitleProperty", "Key" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap = { "InputBrushDataMap", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputBaseControllerData, InputBrushDataMap), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets_Inner = { "InputBrushKeySets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCommonInputKeySetBrushConfiguration, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets_MetaData[] = {
		{ "Category", "Properties" },
		{ "ModuleRelativePath", "Public/CommonInputBaseTypes.h" },
		{ "TitleProperty", "Keys" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets = { "InputBrushKeySets", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonInputBaseControllerData, InputBrushKeySets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonInputBaseControllerData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_GamepadName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_ControllerButtonMaskTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushDataMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonInputBaseControllerData_Statics::NewProp_InputBrushKeySets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonInputBaseControllerData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonInputBaseControllerData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonInputBaseControllerData_Statics::ClassParams = {
		&UCommonInputBaseControllerData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonInputBaseControllerData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonInputBaseControllerData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonInputBaseControllerData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonInputBaseControllerData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonInputBaseControllerData, 3361932387);
	template<> COMMONINPUT_API UClass* StaticClass<UCommonInputBaseControllerData>()
	{
		return UCommonInputBaseControllerData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonInputBaseControllerData(Z_Construct_UClass_UCommonInputBaseControllerData, &UCommonInputBaseControllerData::StaticClass, TEXT("/Script/CommonInput"), TEXT("UCommonInputBaseControllerData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonInputBaseControllerData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
