// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonActionWidget.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonActionWidget() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActionWidget();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonActionWidget_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UWidget();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDataTableRowHandle();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics
	{
		struct CommonActionWidget_eventOnInputMethodChanged_Parms
		{
			bool bUsingGamepad;
		};
		static void NewProp_bUsingGamepad_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsingGamepad;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::NewProp_bUsingGamepad_SetBit(void* Obj)
	{
		((CommonActionWidget_eventOnInputMethodChanged_Parms*)Obj)->bUsingGamepad = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::NewProp_bUsingGamepad = { "bUsingGamepad", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonActionWidget_eventOnInputMethodChanged_Parms), &Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::NewProp_bUsingGamepad_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::NewProp_bUsingGamepad,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "OnInputMethodChanged__DelegateSignature", nullptr, nullptr, sizeof(CommonActionWidget_eventOnInputMethodChanged_Parms), Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UCommonActionWidget::execIsHeldAction)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsHeldAction();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActionWidget::execSetIconRimBrush)
	{
		P_GET_STRUCT(FSlateBrush,Z_Param_InIconRimBrush);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetIconRimBrush(Z_Param_InIconRimBrush);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActionWidget::execSetInputActions)
	{
		P_GET_TARRAY(FDataTableRowHandle,Z_Param_NewInputActions);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInputActions(Z_Param_NewInputActions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActionWidget::execSetInputAction)
	{
		P_GET_STRUCT(FDataTableRowHandle,Z_Param_InputActionRow);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInputAction(Z_Param_InputActionRow);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActionWidget::execGetDisplayText)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetDisplayText();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonActionWidget::execGetIcon)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSlateBrush*)Z_Param__Result=P_THIS->GetIcon();
		P_NATIVE_END;
	}
	void UCommonActionWidget::StaticRegisterNativesUCommonActionWidget()
	{
		UClass* Class = UCommonActionWidget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDisplayText", &UCommonActionWidget::execGetDisplayText },
			{ "GetIcon", &UCommonActionWidget::execGetIcon },
			{ "IsHeldAction", &UCommonActionWidget::execIsHeldAction },
			{ "SetIconRimBrush", &UCommonActionWidget::execSetIconRimBrush },
			{ "SetInputAction", &UCommonActionWidget::execSetInputAction },
			{ "SetInputActions", &UCommonActionWidget::execSetInputActions },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics
	{
		struct CommonActionWidget_eventGetDisplayText_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActionWidget_eventGetDisplayText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "GetDisplayText", nullptr, nullptr, sizeof(CommonActionWidget_eventGetDisplayText_Parms), Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActionWidget_GetDisplayText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActionWidget_GetDisplayText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics
	{
		struct CommonActionWidget_eventGetIcon_Parms
		{
			FSlateBrush ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActionWidget_eventGetIcon_Parms, ReturnValue), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonActionWidget" },
		{ "Comment", "/** End UWidet */" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
		{ "ToolTip", "End UWidet" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "GetIcon", nullptr, nullptr, sizeof(CommonActionWidget_eventGetIcon_Parms), Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActionWidget_GetIcon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActionWidget_GetIcon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics
	{
		struct CommonActionWidget_eventIsHeldAction_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CommonActionWidget_eventIsHeldAction_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonActionWidget_eventIsHeldAction_Parms), &Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "IsHeldAction", nullptr, nullptr, sizeof(CommonActionWidget_eventIsHeldAction_Parms), Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActionWidget_IsHeldAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActionWidget_IsHeldAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics
	{
		struct CommonActionWidget_eventSetIconRimBrush_Parms
		{
			FSlateBrush InIconRimBrush;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InIconRimBrush;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::NewProp_InIconRimBrush = { "InIconRimBrush", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActionWidget_eventSetIconRimBrush_Parms, InIconRimBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::NewProp_InIconRimBrush,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "SetIconRimBrush", nullptr, nullptr, sizeof(CommonActionWidget_eventSetIconRimBrush_Parms), Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics
	{
		struct CommonActionWidget_eventSetInputAction_Parms
		{
			FDataTableRowHandle InputActionRow;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActionRow;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::NewProp_InputActionRow = { "InputActionRow", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActionWidget_eventSetInputAction_Parms, InputActionRow), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::NewProp_InputActionRow,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "SetInputAction", nullptr, nullptr, sizeof(CommonActionWidget_eventSetInputAction_Parms), Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActionWidget_SetInputAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActionWidget_SetInputAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics
	{
		struct CommonActionWidget_eventSetInputActions_Parms
		{
			TArray<FDataTableRowHandle> NewInputActions;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewInputActions_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NewInputActions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::NewProp_NewInputActions_Inner = { "NewInputActions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::NewProp_NewInputActions = { "NewInputActions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonActionWidget_eventSetInputActions_Parms, NewInputActions), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::NewProp_NewInputActions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::NewProp_NewInputActions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonActionWidget, nullptr, "SetInputActions", nullptr, nullptr, sizeof(CommonActionWidget_eventSetInputActions_Parms), Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonActionWidget_SetInputActions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonActionWidget_SetInputActions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonActionWidget_NoRegister()
	{
		return UCommonActionWidget::StaticClass();
	}
	struct Z_Construct_UClass_UCommonActionWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInputMethodChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnInputMethodChanged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProgressMaterialBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProgressMaterialBrush;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProgressMaterialParam_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ProgressMaterialParam;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IconRimBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IconRimBrush;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputActions;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputActionDataRow_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputActionDataRow;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProgressDynamicMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProgressDynamicMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonActionWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonActionWidget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonActionWidget_GetDisplayText, "GetDisplayText" }, // 1729928015
		{ &Z_Construct_UFunction_UCommonActionWidget_GetIcon, "GetIcon" }, // 1677427534
		{ &Z_Construct_UFunction_UCommonActionWidget_IsHeldAction, "IsHeldAction" }, // 2893180600
		{ &Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature, "OnInputMethodChanged__DelegateSignature" }, // 4018309142
		{ &Z_Construct_UFunction_UCommonActionWidget_SetIconRimBrush, "SetIconRimBrush" }, // 798528876
		{ &Z_Construct_UFunction_UCommonActionWidget_SetInputAction, "SetInputAction" }, // 2907544303
		{ &Z_Construct_UFunction_UCommonActionWidget_SetInputActions, "SetInputActions" }, // 3211857248
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CommonActionWidget.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_OnInputMethodChanged_MetaData[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_OnInputMethodChanged = { "OnInputMethodChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, OnInputMethodChanged), Z_Construct_UDelegateFunction_UCommonActionWidget_OnInputMethodChanged__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_OnInputMethodChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_OnInputMethodChanged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialBrush_MetaData[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialBrush = { "ProgressMaterialBrush", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, ProgressMaterialBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialBrush_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialParam_MetaData[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialParam = { "ProgressMaterialParam", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, ProgressMaterialParam), METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialParam_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialParam_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_IconRimBrush_MetaData[] = {
		{ "Category", "CommonActionWidget" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_IconRimBrush = { "IconRimBrush", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, IconRimBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_IconRimBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_IconRimBrush_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions_Inner = { "InputActions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions_MetaData[] = {
		{ "Category", "CommonActionWidget" },
		{ "Comment", "/**\n\x09 * List all the input actions that this common action widget is intended to represent.  In some cases you might have multiple actions\n\x09 * that you need to represent as a single entry in the UI.  For example - zoom, might be mouse wheel up or down, but you just need to\n\x09 * show a single icon for Up & Down on the mouse, this solves that problem.\n\x09 */" },
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
		{ "RowType", "CommonInputActionDataBase" },
		{ "TitleProperty", "RowName" },
		{ "ToolTip", "List all the input actions that this common action widget is intended to represent.  In some cases you might have multiple actions\nthat you need to represent as a single entry in the UI.  For example - zoom, might be mouse wheel up or down, but you just need to\nshow a single icon for Up & Down on the mouse, this solves that problem." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions = { "InputActions", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, InputActions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActionDataRow_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActionDataRow = { "InputActionDataRow", nullptr, (EPropertyFlags)0x0020080820000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, InputActionDataRow_DEPRECATED), Z_Construct_UScriptStruct_FDataTableRowHandle, METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActionDataRow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActionDataRow_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressDynamicMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonActionWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressDynamicMaterial = { "ProgressDynamicMaterial", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonActionWidget, ProgressDynamicMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressDynamicMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressDynamicMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonActionWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_OnInputMethodChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialBrush,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressMaterialParam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_IconRimBrush,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActions,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_InputActionDataRow,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonActionWidget_Statics::NewProp_ProgressDynamicMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonActionWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonActionWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonActionWidget_Statics::ClassParams = {
		&UCommonActionWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonActionWidget_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonActionWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonActionWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonActionWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonActionWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonActionWidget, 1275858258);
	template<> COMMONUI_API UClass* StaticClass<UCommonActionWidget>()
	{
		return UCommonActionWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonActionWidget(Z_Construct_UClass_UCommonActionWidget, &UCommonActionWidget::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonActionWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonActionWidget);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UCommonActionWidget)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
