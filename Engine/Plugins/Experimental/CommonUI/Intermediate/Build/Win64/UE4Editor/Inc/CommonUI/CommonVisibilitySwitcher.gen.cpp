// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonVisibilitySwitcher.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonVisibilitySwitcher() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonVisibilitySwitcher_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonVisibilitySwitcher();
	UMG_API UClass* Z_Construct_UClass_UOverlay();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	UMG_API UClass* Z_Construct_UClass_UWidget_NoRegister();
	UMG_API UEnum* Z_Construct_UEnum_UMG_ESlateVisibility();
// End Cross Module References
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execDeactivateVisibleSlot)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeactivateVisibleSlot();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execActivateVisibleSlot)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ActivateVisibleSlot();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execDecrementActiveWidgetIndex)
	{
		P_GET_UBOOL(Z_Param_bAllowWrapping);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DecrementActiveWidgetIndex(Z_Param_bAllowWrapping);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execIncrementActiveWidgetIndex)
	{
		P_GET_UBOOL(Z_Param_bAllowWrapping);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncrementActiveWidgetIndex(Z_Param_bAllowWrapping);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execSetActiveWidget)
	{
		P_GET_OBJECT(UWidget,Z_Param_Widget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetActiveWidget(Z_Param_Widget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execGetActiveWidget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWidget**)Z_Param__Result=P_THIS->GetActiveWidget();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execGetActiveWidgetIndex)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetActiveWidgetIndex();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonVisibilitySwitcher::execSetActiveWidgetIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetActiveWidgetIndex(Z_Param_Index);
		P_NATIVE_END;
	}
	void UCommonVisibilitySwitcher::StaticRegisterNativesUCommonVisibilitySwitcher()
	{
		UClass* Class = UCommonVisibilitySwitcher::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ActivateVisibleSlot", &UCommonVisibilitySwitcher::execActivateVisibleSlot },
			{ "DeactivateVisibleSlot", &UCommonVisibilitySwitcher::execDeactivateVisibleSlot },
			{ "DecrementActiveWidgetIndex", &UCommonVisibilitySwitcher::execDecrementActiveWidgetIndex },
			{ "GetActiveWidget", &UCommonVisibilitySwitcher::execGetActiveWidget },
			{ "GetActiveWidgetIndex", &UCommonVisibilitySwitcher::execGetActiveWidgetIndex },
			{ "IncrementActiveWidgetIndex", &UCommonVisibilitySwitcher::execIncrementActiveWidgetIndex },
			{ "SetActiveWidget", &UCommonVisibilitySwitcher::execSetActiveWidget },
			{ "SetActiveWidgetIndex", &UCommonVisibilitySwitcher::execSetActiveWidgetIndex },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "ActivateVisibleSlot", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "DeactivateVisibleSlot", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics
	{
		struct CommonVisibilitySwitcher_eventDecrementActiveWidgetIndex_Parms
		{
			bool bAllowWrapping;
		};
		static void NewProp_bAllowWrapping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowWrapping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping_SetBit(void* Obj)
	{
		((CommonVisibilitySwitcher_eventDecrementActiveWidgetIndex_Parms*)Obj)->bAllowWrapping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping = { "bAllowWrapping", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonVisibilitySwitcher_eventDecrementActiveWidgetIndex_Parms), &Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "CPP_Default_bAllowWrapping", "true" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "DecrementActiveWidgetIndex", nullptr, nullptr, sizeof(CommonVisibilitySwitcher_eventDecrementActiveWidgetIndex_Parms), Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics
	{
		struct CommonVisibilitySwitcher_eventGetActiveWidget_Parms
		{
			UWidget* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonVisibilitySwitcher_eventGetActiveWidget_Parms, ReturnValue), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "GetActiveWidget", nullptr, nullptr, sizeof(CommonVisibilitySwitcher_eventGetActiveWidget_Parms), Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics
	{
		struct CommonVisibilitySwitcher_eventGetActiveWidgetIndex_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonVisibilitySwitcher_eventGetActiveWidgetIndex_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "GetActiveWidgetIndex", nullptr, nullptr, sizeof(CommonVisibilitySwitcher_eventGetActiveWidgetIndex_Parms), Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics
	{
		struct CommonVisibilitySwitcher_eventIncrementActiveWidgetIndex_Parms
		{
			bool bAllowWrapping;
		};
		static void NewProp_bAllowWrapping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowWrapping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping_SetBit(void* Obj)
	{
		((CommonVisibilitySwitcher_eventIncrementActiveWidgetIndex_Parms*)Obj)->bAllowWrapping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping = { "bAllowWrapping", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonVisibilitySwitcher_eventIncrementActiveWidgetIndex_Parms), &Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::NewProp_bAllowWrapping,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "CPP_Default_bAllowWrapping", "true" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "IncrementActiveWidgetIndex", nullptr, nullptr, sizeof(CommonVisibilitySwitcher_eventIncrementActiveWidgetIndex_Parms), Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics
	{
		struct CommonVisibilitySwitcher_eventSetActiveWidget_Parms
		{
			const UWidget* Widget;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Widget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Widget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::NewProp_Widget_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::NewProp_Widget = { "Widget", nullptr, (EPropertyFlags)0x0010000000080082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonVisibilitySwitcher_eventSetActiveWidget_Parms, Widget), Z_Construct_UClass_UWidget_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::NewProp_Widget_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::NewProp_Widget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::NewProp_Widget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "SetActiveWidget", nullptr, nullptr, sizeof(CommonVisibilitySwitcher_eventSetActiveWidget_Parms), Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics
	{
		struct CommonVisibilitySwitcher_eventSetActiveWidgetIndex_Parms
		{
			int32 Index;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonVisibilitySwitcher_eventSetActiveWidgetIndex_Parms, Index), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::NewProp_Index,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilitySwitcher, nullptr, "SetActiveWidgetIndex", nullptr, nullptr, sizeof(CommonVisibilitySwitcher_eventSetActiveWidgetIndex_Parms), Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonVisibilitySwitcher_NoRegister()
	{
		return UCommonVisibilitySwitcher::StaticClass();
	}
	struct Z_Construct_UClass_UCommonVisibilitySwitcher_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShownVisibility_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShownVisibility_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShownVisibility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveWidgetIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActiveWidgetIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoActivateSlot_MetaData[];
#endif
		static void NewProp_bAutoActivateSlot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoActivateSlot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bActivateFirstSlotOnAdding_MetaData[];
#endif
		static void NewProp_bActivateFirstSlotOnAdding_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActivateFirstSlotOnAdding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOverlay,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_ActivateVisibleSlot, "ActivateVisibleSlot" }, // 1782233437
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_DeactivateVisibleSlot, "DeactivateVisibleSlot" }, // 2299200774
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_DecrementActiveWidgetIndex, "DecrementActiveWidgetIndex" }, // 3106832620
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidget, "GetActiveWidget" }, // 297452728
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_GetActiveWidgetIndex, "GetActiveWidgetIndex" }, // 2793223297
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_IncrementActiveWidgetIndex, "IncrementActiveWidgetIndex" }, // 727285659
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidget, "SetActiveWidget" }, // 2631093759
		{ &Z_Construct_UFunction_UCommonVisibilitySwitcher_SetActiveWidgetIndex, "SetActiveWidgetIndex" }, // 4120456428
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Basic switcher that toggles visibility on its children to only show one widget at a time. Activates visible widget if possible.\n */" },
		{ "DisableNativeTick", "" },
		{ "IncludePath", "CommonVisibilitySwitcher.h" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
		{ "ToolTip", "Basic switcher that toggles visibility on its children to only show one widget at a time. Activates visible widget if possible." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility_MetaData[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility = { "ShownVisibility", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonVisibilitySwitcher, ShownVisibility), Z_Construct_UEnum_UMG_ESlateVisibility, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ActiveWidgetIndex_MetaData[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "ClampMin", "-1" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ActiveWidgetIndex = { "ActiveWidgetIndex", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonVisibilitySwitcher, ActiveWidgetIndex), METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ActiveWidgetIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ActiveWidgetIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot_MetaData[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "Comment", "// Whether or not to automatically activate a slot when it becomes visible\n" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
		{ "ToolTip", "Whether or not to automatically activate a slot when it becomes visible" },
	};
#endif
	void Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot_SetBit(void* Obj)
	{
		((UCommonVisibilitySwitcher*)Obj)->bAutoActivateSlot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot = { "bAutoActivateSlot", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonVisibilitySwitcher), &Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding_MetaData[] = {
		{ "Category", "CommonVisibilitySwitcher" },
		{ "Comment", "// Whether or not to activate the first slot if one is added dynamically\n" },
		{ "ModuleRelativePath", "Public/CommonVisibilitySwitcher.h" },
		{ "ToolTip", "Whether or not to activate the first slot if one is added dynamically" },
	};
#endif
	void Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding_SetBit(void* Obj)
	{
		((UCommonVisibilitySwitcher*)Obj)->bActivateFirstSlotOnAdding = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding = { "bActivateFirstSlotOnAdding", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonVisibilitySwitcher), &Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ShownVisibility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_ActiveWidgetIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bAutoActivateSlot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::NewProp_bActivateFirstSlotOnAdding,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonVisibilitySwitcher>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::ClassParams = {
		&UCommonVisibilitySwitcher::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonVisibilitySwitcher()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonVisibilitySwitcher_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonVisibilitySwitcher, 3285739094);
	template<> COMMONUI_API UClass* StaticClass<UCommonVisibilitySwitcher>()
	{
		return UCommonVisibilitySwitcher::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonVisibilitySwitcher(Z_Construct_UClass_UCommonVisibilitySwitcher, &UCommonVisibilitySwitcher::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonVisibilitySwitcher"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonVisibilitySwitcher);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
