// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonDateTimeTextBlock.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonDateTimeTextBlock() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonDateTimeTextBlock_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonDateTimeTextBlock();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonTextBlock();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimespan();
// End Cross Module References
	DEFINE_FUNCTION(UCommonDateTimeTextBlock::execGetDateTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDateTime*)Z_Param__Result=P_THIS->GetDateTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonDateTimeTextBlock::execSetCountDownCompletionText)
	{
		P_GET_PROPERTY(FTextProperty,Z_Param_InCompletionText);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCountDownCompletionText(Z_Param_InCompletionText);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonDateTimeTextBlock::execSetTimespanValue)
	{
		P_GET_STRUCT(FTimespan,Z_Param_InTimespan);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTimespanValue(Z_Param_InTimespan);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonDateTimeTextBlock::execSetDateTimeValue)
	{
		P_GET_STRUCT(FDateTime,Z_Param_InDateTime);
		P_GET_UBOOL(Z_Param_bShowAsCountdown);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InRefreshDelay);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDateTimeValue(Z_Param_InDateTime,Z_Param_bShowAsCountdown,Z_Param_InRefreshDelay);
		P_NATIVE_END;
	}
	void UCommonDateTimeTextBlock::StaticRegisterNativesUCommonDateTimeTextBlock()
	{
		UClass* Class = UCommonDateTimeTextBlock::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDateTime", &UCommonDateTimeTextBlock::execGetDateTime },
			{ "SetCountDownCompletionText", &UCommonDateTimeTextBlock::execSetCountDownCompletionText },
			{ "SetDateTimeValue", &UCommonDateTimeTextBlock::execSetDateTimeValue },
			{ "SetTimespanValue", &UCommonDateTimeTextBlock::execSetTimespanValue },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics
	{
		struct CommonDateTimeTextBlock_eventGetDateTime_Parms
		{
			FDateTime ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonDateTimeTextBlock_eventGetDateTime_Parms, ReturnValue), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "DateTime Text Block" },
		{ "ModuleRelativePath", "Public/CommonDateTimeTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonDateTimeTextBlock, nullptr, "GetDateTime", nullptr, nullptr, sizeof(CommonDateTimeTextBlock_eventGetDateTime_Parms), Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics
	{
		struct CommonDateTimeTextBlock_eventSetCountDownCompletionText_Parms
		{
			FText InCompletionText;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InCompletionText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_InCompletionText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::NewProp_InCompletionText_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::NewProp_InCompletionText = { "InCompletionText", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonDateTimeTextBlock_eventSetCountDownCompletionText_Parms, InCompletionText), METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::NewProp_InCompletionText_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::NewProp_InCompletionText_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::NewProp_InCompletionText,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::Function_MetaDataParams[] = {
		{ "Category", "DateTime Text Block" },
		{ "ModuleRelativePath", "Public/CommonDateTimeTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonDateTimeTextBlock, nullptr, "SetCountDownCompletionText", nullptr, nullptr, sizeof(CommonDateTimeTextBlock_eventSetCountDownCompletionText_Parms), Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics
	{
		struct CommonDateTimeTextBlock_eventSetDateTimeValue_Parms
		{
			FDateTime InDateTime;
			bool bShowAsCountdown;
			float InRefreshDelay;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InDateTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InDateTime;
		static void NewProp_bShowAsCountdown_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowAsCountdown;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InRefreshDelay;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InDateTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InDateTime = { "InDateTime", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonDateTimeTextBlock_eventSetDateTimeValue_Parms, InDateTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InDateTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InDateTime_MetaData)) };
	void Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_bShowAsCountdown_SetBit(void* Obj)
	{
		((CommonDateTimeTextBlock_eventSetDateTimeValue_Parms*)Obj)->bShowAsCountdown = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_bShowAsCountdown = { "bShowAsCountdown", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CommonDateTimeTextBlock_eventSetDateTimeValue_Parms), &Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_bShowAsCountdown_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InRefreshDelay = { "InRefreshDelay", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonDateTimeTextBlock_eventSetDateTimeValue_Parms, InRefreshDelay), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InDateTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_bShowAsCountdown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::NewProp_InRefreshDelay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DateTime Text Block" },
		{ "CPP_Default_InRefreshDelay", "1.000000" },
		{ "ModuleRelativePath", "Public/CommonDateTimeTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonDateTimeTextBlock, nullptr, "SetDateTimeValue", nullptr, nullptr, sizeof(CommonDateTimeTextBlock_eventSetDateTimeValue_Parms), Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics
	{
		struct CommonDateTimeTextBlock_eventSetTimespanValue_Parms
		{
			FTimespan InTimespan;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTimespan_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTimespan;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::NewProp_InTimespan_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::NewProp_InTimespan = { "InTimespan", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonDateTimeTextBlock_eventSetTimespanValue_Parms, InTimespan), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::NewProp_InTimespan_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::NewProp_InTimespan_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::NewProp_InTimespan,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DateTime Text Block" },
		{ "ModuleRelativePath", "Public/CommonDateTimeTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonDateTimeTextBlock, nullptr, "SetTimespanValue", nullptr, nullptr, sizeof(CommonDateTimeTextBlock_eventSetTimespanValue_Parms), Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonDateTimeTextBlock_NoRegister()
	{
		return UCommonDateTimeTextBlock::StaticClass();
	}
	struct Z_Construct_UClass_UCommonDateTimeTextBlock_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonTextBlock,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonDateTimeTextBlock_GetDateTime, "GetDateTime" }, // 3750134860
		{ &Z_Construct_UFunction_UCommonDateTimeTextBlock_SetCountDownCompletionText, "SetCountDownCompletionText" }, // 1012375887
		{ &Z_Construct_UFunction_UCommonDateTimeTextBlock_SetDateTimeValue, "SetDateTimeValue" }, // 1707043326
		{ &Z_Construct_UFunction_UCommonDateTimeTextBlock_SetTimespanValue, "SetTimespanValue" }, // 1671209553
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CommonDateTimeTextBlock.h" },
		{ "ModuleRelativePath", "Public/CommonDateTimeTextBlock.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonDateTimeTextBlock>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::ClassParams = {
		&UCommonDateTimeTextBlock::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A2u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonDateTimeTextBlock()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonDateTimeTextBlock_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonDateTimeTextBlock, 4221385153);
	template<> COMMONUI_API UClass* StaticClass<UCommonDateTimeTextBlock>()
	{
		return UCommonDateTimeTextBlock::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonDateTimeTextBlock(Z_Construct_UClass_UCommonDateTimeTextBlock, &UCommonDateTimeTextBlock::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonDateTimeTextBlock"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonDateTimeTextBlock);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
