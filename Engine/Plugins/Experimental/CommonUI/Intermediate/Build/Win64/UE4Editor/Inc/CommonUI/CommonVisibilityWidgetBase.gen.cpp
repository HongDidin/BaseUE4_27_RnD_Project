// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonVisibilityWidgetBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonVisibilityWidgetBase() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonVisibilityWidgetBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonVisibilityWidgetBase();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBorder();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	UMG_API UEnum* Z_Construct_UEnum_UMG_ESlateVisibility();
// End Cross Module References
	DEFINE_FUNCTION(UCommonVisibilityWidgetBase::execGetRegisteredPlatforms)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FName>*)Z_Param__Result=UCommonVisibilityWidgetBase::GetRegisteredPlatforms();
		P_NATIVE_END;
	}
	void UCommonVisibilityWidgetBase::StaticRegisterNativesUCommonVisibilityWidgetBase()
	{
		UClass* Class = UCommonVisibilityWidgetBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetRegisteredPlatforms", &UCommonVisibilityWidgetBase::execGetRegisteredPlatforms },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics
	{
		struct CommonVisibilityWidgetBase_eventGetRegisteredPlatforms_Parms
		{
			TArray<FName> ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000008000582, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonVisibilityWidgetBase_eventGetRegisteredPlatforms_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonVisibilityWidgetBase, nullptr, "GetRegisteredPlatforms", nullptr, nullptr, sizeof(CommonVisibilityWidgetBase_eventGetRegisteredPlatforms_Parms), Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00082401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonVisibilityWidgetBase_NoRegister()
	{
		return UCommonVisibilityWidgetBase::StaticClass();
	}
	struct Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_VisibilityControls_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_VisibilityControls_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisibilityControls_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VisibilityControls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowForGamepad_MetaData[];
#endif
		static void NewProp_bShowForGamepad_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowForGamepad;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowForMouseAndKeyboard_MetaData[];
#endif
		static void NewProp_bShowForMouseAndKeyboard_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowForMouseAndKeyboard;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowForTouch_MetaData[];
#endif
		static void NewProp_bShowForTouch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowForTouch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_VisibleType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisibleType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_VisibleType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_HiddenType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HiddenType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_HiddenType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonBorder,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonVisibilityWidgetBase_GetRegisteredPlatforms, "GetRegisteredPlatforms" }, // 3153040276
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A container that controls visibility based on Input type and Platform\n *\n */" },
		{ "IncludePath", "CommonVisibilityWidgetBase.h" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
		{ "ToolTip", "A container that controls visibility based on Input type and Platform" },
	};
#endif
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_ValueProp = { "VisibilityControls", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_Key_KeyProp = { "VisibilityControls_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_MetaData[] = {
		{ "Category", "Visibility" },
		{ "GetOptions", "GetRegisteredPlatforms" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls = { "VisibilityControls", nullptr, (EPropertyFlags)0x0010000000000041, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonVisibilityWidgetBase, VisibilityControls), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad_MetaData[] = {
		{ "Category", "Visibility" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	void Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad_SetBit(void* Obj)
	{
		((UCommonVisibilityWidgetBase*)Obj)->bShowForGamepad = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad = { "bShowForGamepad", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonVisibilityWidgetBase), &Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard_MetaData[] = {
		{ "Category", "Visibility" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	void Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard_SetBit(void* Obj)
	{
		((UCommonVisibilityWidgetBase*)Obj)->bShowForMouseAndKeyboard = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard = { "bShowForMouseAndKeyboard", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonVisibilityWidgetBase), &Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch_MetaData[] = {
		{ "Category", "Visibility" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	void Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch_SetBit(void* Obj)
	{
		((UCommonVisibilityWidgetBase*)Obj)->bShowForTouch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch = { "bShowForTouch", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonVisibilityWidgetBase), &Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType_MetaData[] = {
		{ "Category", "Visibility" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType = { "VisibleType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonVisibilityWidgetBase, VisibleType), Z_Construct_UEnum_UMG_ESlateVisibility, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType_MetaData[] = {
		{ "Category", "Visibility" },
		{ "ModuleRelativePath", "Public/CommonVisibilityWidgetBase.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType = { "HiddenType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonVisibilityWidgetBase, HiddenType), Z_Construct_UEnum_UMG_ESlateVisibility, METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibilityControls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForGamepad,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForMouseAndKeyboard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_bShowForTouch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_VisibleType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::NewProp_HiddenType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonVisibilityWidgetBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::ClassParams = {
		&UCommonVisibilityWidgetBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::PropPointers),
		0,
		0x00B000A2u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonVisibilityWidgetBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonVisibilityWidgetBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonVisibilityWidgetBase, 2480249894);
	template<> COMMONUI_API UClass* StaticClass<UCommonVisibilityWidgetBase>()
	{
		return UCommonVisibilityWidgetBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonVisibilityWidgetBase(Z_Construct_UClass_UCommonVisibilityWidgetBase, &UCommonVisibilityWidgetBase::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonVisibilityWidgetBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonVisibilityWidgetBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
