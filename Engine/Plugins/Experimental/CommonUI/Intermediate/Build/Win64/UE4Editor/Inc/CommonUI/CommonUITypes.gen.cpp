// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonUITypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonUITypes() {}
// Cross Module References
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	COMMONUI_API UFunction* Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature();
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_EInputActionState();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputActionDataBase();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FCommonInputTypeInfo();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventOnItemSelected_Parms
		{
			UUserWidget* Widget;
			bool Selected;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Widget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Widget;
		static void NewProp_Selected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Selected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Widget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Widget = { "Widget", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventOnItemSelected_Parms, Widget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Widget_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Widget_MetaData)) };
	void Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Selected_SetBit(void* Obj)
	{
		((_Script_CommonUI_eventOnItemSelected_Parms*)Obj)->Selected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Selected = { "Selected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_CommonUI_eventOnItemSelected_Parms), &Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Selected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Widget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::NewProp_Selected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "OnItemSelected__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventOnItemSelected_Parms), Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_OnItemSelected__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics
	{
		struct _Script_CommonUI_eventOnItemClicked_Parms
		{
			UUserWidget* Widget;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Widget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Widget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::NewProp_Widget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::NewProp_Widget = { "Widget", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonUI_eventOnItemClicked_Parms, Widget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::NewProp_Widget_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::NewProp_Widget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::NewProp_Widget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonUI, nullptr, "OnItemClicked__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonUI_eventOnItemClicked_Parms), Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonUI_OnItemClicked__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EInputActionState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonUI_EInputActionState, Z_Construct_UPackage__Script_CommonUI(), TEXT("EInputActionState"));
		}
		return Singleton;
	}
	template<> COMMONUI_API UEnum* StaticEnum<EInputActionState>()
	{
		return EInputActionState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EInputActionState(EInputActionState_StaticEnum, TEXT("/Script/CommonUI"), TEXT("EInputActionState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonUI_EInputActionState_Hash() { return 2574076326U; }
	UEnum* Z_Construct_UEnum_CommonUI_EInputActionState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EInputActionState"), 0, Get_Z_Construct_UEnum_CommonUI_EInputActionState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EInputActionState::Enabled", (int64)EInputActionState::Enabled },
				{ "EInputActionState::Disabled", (int64)EInputActionState::Disabled },
				{ "EInputActionState::Hidden", (int64)EInputActionState::Hidden },
				{ "EInputActionState::HiddenAndDisabled", (int64)EInputActionState::HiddenAndDisabled },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Disabled.Comment", "/** Disabled, will call all the disabled callback if specified otherwise do nothing */" },
				{ "Disabled.Name", "EInputActionState::Disabled" },
				{ "Disabled.ToolTip", "Disabled, will call all the disabled callback if specified otherwise do nothing" },
				{ "Enabled.Comment", "/** Enabled, will call all callbacks */" },
				{ "Enabled.Name", "EInputActionState::Enabled" },
				{ "Enabled.ToolTip", "Enabled, will call all callbacks" },
				{ "Hidden.Comment", "/** The common input reflector will not visualize this but still calls all callbacks. NOTE: Use this sparingly */" },
				{ "Hidden.Name", "EInputActionState::Hidden" },
				{ "Hidden.ToolTip", "The common input reflector will not visualize this but still calls all callbacks. NOTE: Use this sparingly" },
				{ "HiddenAndDisabled.Comment", "/** Hidden and disabled behaves as if it were never added with no callbacks being called */" },
				{ "HiddenAndDisabled.Name", "EInputActionState::HiddenAndDisabled" },
				{ "HiddenAndDisabled.ToolTip", "Hidden and disabled behaves as if it were never added with no callbacks being called" },
				{ "ModuleRelativePath", "Public/CommonUITypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonUI,
				nullptr,
				"EInputActionState",
				"EInputActionState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FCommonInputActionDataBase>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FCommonInputActionDataBase cannot be polymorphic unless super FTableRowBase is polymorphic");

class UScriptStruct* FCommonInputActionDataBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FCommonInputActionDataBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonInputActionDataBase, Z_Construct_UPackage__Script_CommonUI(), TEXT("CommonInputActionDataBase"), sizeof(FCommonInputActionDataBase), Get_Z_Construct_UScriptStruct_FCommonInputActionDataBase_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FCommonInputActionDataBase>()
{
	return FCommonInputActionDataBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonInputActionDataBase(FCommonInputActionDataBase::StaticStruct, TEXT("/Script/CommonUI"), TEXT("CommonInputActionDataBase"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFCommonInputActionDataBase
{
	FScriptStruct_CommonUI_StaticRegisterNativesFCommonInputActionDataBase()
	{
		UScriptStruct::DeferCppStructOps<FCommonInputActionDataBase>(FName(TEXT("CommonInputActionDataBase")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFCommonInputActionDataBase;
	struct Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoldDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_HoldDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyboardInputTypeInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyboardInputTypeInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultGamepadInputTypeInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultGamepadInputTypeInfo;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GamepadInputOverrides_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GamepadInputOverrides_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GamepadInputOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_GamepadInputOverrides;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TouchInputTypeInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TouchInputTypeInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonInputActionDataBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** User facing name (used when NOT a hold action) */" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "User facing name (used when NOT a hold action)" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionDataBase, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_HoldDisplayName_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** User facing name used when it IS a hold action */" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "User facing name used when it IS a hold action" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_HoldDisplayName = { "HoldDisplayName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionDataBase, HoldDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_HoldDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_HoldDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_KeyboardInputTypeInfo_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/**\n\x09* Key to bind to for each input method\n\x09*/" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Key to bind to for each input method" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_KeyboardInputTypeInfo = { "KeyboardInputTypeInfo", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionDataBase, KeyboardInputTypeInfo), Z_Construct_UScriptStruct_FCommonInputTypeInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_KeyboardInputTypeInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_KeyboardInputTypeInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DefaultGamepadInputTypeInfo_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/**\n\x09* Default input state for gamepads\n\x09*/" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Default input state for gamepads" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DefaultGamepadInputTypeInfo = { "DefaultGamepadInputTypeInfo", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionDataBase, DefaultGamepadInputTypeInfo), Z_Construct_UScriptStruct_FCommonInputTypeInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DefaultGamepadInputTypeInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DefaultGamepadInputTypeInfo_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_ValueProp = { "GamepadInputOverrides", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCommonInputTypeInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_Key_KeyProp = { "GamepadInputOverrides_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/**\n\x09* Override the input state for each input method\n\x09*/" },
		{ "GetOptions", "CommonInput.CommonInputBaseControllerData.GetRegisteredGamepads" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Override the input state for each input method" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides = { "GamepadInputOverrides", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionDataBase, GamepadInputOverrides), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_TouchInputTypeInfo_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/**\n\x09* Override the displayed brush for each input method\n\x09*/" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Override the displayed brush for each input method" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_TouchInputTypeInfo = { "TouchInputTypeInfo", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputActionDataBase, TouchInputTypeInfo), Z_Construct_UScriptStruct_FCommonInputTypeInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_TouchInputTypeInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_TouchInputTypeInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_HoldDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_KeyboardInputTypeInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_DefaultGamepadInputTypeInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_GamepadInputOverrides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::NewProp_TouchInputTypeInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"CommonInputActionDataBase",
		sizeof(FCommonInputActionDataBase),
		alignof(FCommonInputActionDataBase),
		Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonInputActionDataBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonInputActionDataBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonInputActionDataBase"), sizeof(FCommonInputActionDataBase), Get_Z_Construct_UScriptStruct_FCommonInputActionDataBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonInputActionDataBase_Hash() { return 462297485U; }
class UScriptStruct* FCommonInputTypeInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FCommonInputTypeInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCommonInputTypeInfo, Z_Construct_UPackage__Script_CommonUI(), TEXT("CommonInputTypeInfo"), sizeof(FCommonInputTypeInfo), Get_Z_Construct_UScriptStruct_FCommonInputTypeInfo_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FCommonInputTypeInfo>()
{
	return FCommonInputTypeInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCommonInputTypeInfo(FCommonInputTypeInfo::StaticStruct, TEXT("/Script/CommonUI"), TEXT("CommonInputTypeInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFCommonInputTypeInfo
{
	FScriptStruct_CommonUI_StaticRegisterNativesFCommonInputTypeInfo()
	{
		UScriptStruct::DeferCppStructOps<FCommonInputTypeInfo>(FName(TEXT("CommonInputTypeInfo")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFCommonInputTypeInfo;
	struct Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OverrrideState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrrideState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OverrrideState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bActionRequiresHold_MetaData[];
#endif
		static void NewProp_bActionRequiresHold_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActionRequiresHold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HoldTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HoldTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideBrush_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OverrideBrush;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCommonInputTypeInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** Key this action is bound to\x09*/" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Key this action is bound to" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputTypeInfo, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** EInputActionState::Enabled means that the state isn't overriden and the games dynamic control will work */" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "EInputActionState::Enabled means that the state isn't overriden and the games dynamic control will work" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState = { "OverrrideState", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputTypeInfo, OverrrideState), Z_Construct_UEnum_CommonUI_EInputActionState, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** Enables hold time if true */" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Enables hold time if true" },
	};
#endif
	void Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold_SetBit(void* Obj)
	{
		((FCommonInputTypeInfo*)Obj)->bActionRequiresHold = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold = { "bActionRequiresHold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FCommonInputTypeInfo), &Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_HoldTime_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** The hold time in seconds */" },
		{ "EditCondition", "bActionRequiresHold" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "The hold time in seconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_HoldTime = { "HoldTime", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputTypeInfo, HoldTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_HoldTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_HoldTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrideBrush_MetaData[] = {
		{ "Category", "CommonInput" },
		{ "Comment", "/** Override the brush specified by the Key Display Data  */" },
		{ "ModuleRelativePath", "Public/CommonUITypes.h" },
		{ "ToolTip", "Override the brush specified by the Key Display Data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrideBrush = { "OverrideBrush", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCommonInputTypeInfo, OverrideBrush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrideBrush_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrideBrush_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrrideState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_bActionRequiresHold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_HoldTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::NewProp_OverrideBrush,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		nullptr,
		&NewStructOps,
		"CommonInputTypeInfo",
		sizeof(FCommonInputTypeInfo),
		alignof(FCommonInputTypeInfo),
		Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCommonInputTypeInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCommonInputTypeInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CommonInputTypeInfo"), sizeof(FCommonInputTypeInfo), Get_Z_Construct_UScriptStruct_FCommonInputTypeInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCommonInputTypeInfo_Hash() { return 2046249104U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
