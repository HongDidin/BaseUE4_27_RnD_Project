// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUserWidget;
#ifdef COMMONUI_CommonUITypes_generated_h
#error "CommonUITypes.generated.h already included, missing '#pragma once' in CommonUITypes.h"
#endif
#define COMMONUI_CommonUITypes_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUITypes_h_77_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonInputActionDataBase_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__KeyboardInputTypeInfo() { return STRUCT_OFFSET(FCommonInputActionDataBase, KeyboardInputTypeInfo); } \
	FORCEINLINE static uint32 __PPO__DefaultGamepadInputTypeInfo() { return STRUCT_OFFSET(FCommonInputActionDataBase, DefaultGamepadInputTypeInfo); } \
	FORCEINLINE static uint32 __PPO__GamepadInputOverrides() { return STRUCT_OFFSET(FCommonInputActionDataBase, GamepadInputOverrides); } \
	FORCEINLINE static uint32 __PPO__TouchInputTypeInfo() { return STRUCT_OFFSET(FCommonInputActionDataBase, TouchInputTypeInfo); } \
	typedef FTableRowBase Super;


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonInputActionDataBase>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUITypes_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonInputTypeInfo_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Key() { return STRUCT_OFFSET(FCommonInputTypeInfo, Key); }


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonInputTypeInfo>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUITypes_h_158_DELEGATE \
struct _Script_CommonUI_eventOnItemSelected_Parms \
{ \
	UUserWidget* Widget; \
	bool Selected; \
}; \
static inline void FOnItemSelected_DelegateWrapper(const FScriptDelegate& OnItemSelected, UUserWidget* Widget, bool Selected) \
{ \
	_Script_CommonUI_eventOnItemSelected_Parms Parms; \
	Parms.Widget=Widget; \
	Parms.Selected=Selected ? true : false; \
	OnItemSelected.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUITypes_h_157_DELEGATE \
struct _Script_CommonUI_eventOnItemClicked_Parms \
{ \
	UUserWidget* Widget; \
}; \
static inline void FOnItemClicked_DelegateWrapper(const FScriptDelegate& OnItemClicked, UUserWidget* Widget) \
{ \
	_Script_CommonUI_eventOnItemClicked_Parms Parms; \
	Parms.Widget=Widget; \
	OnItemClicked.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUITypes_h


#define FOREACH_ENUM_EINPUTACTIONSTATE(op) \
	op(EInputActionState::Enabled) \
	op(EInputActionState::Disabled) \
	op(EInputActionState::Hidden) \
	op(EInputActionState::HiddenAndDisabled) 

enum class EInputActionState : uint8;
template<> COMMONUI_API UEnum* StaticEnum<EInputActionState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
