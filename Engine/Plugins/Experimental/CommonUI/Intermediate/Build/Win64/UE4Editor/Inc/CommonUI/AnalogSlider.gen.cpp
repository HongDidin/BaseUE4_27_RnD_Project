// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/AnalogSlider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnalogSlider() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UAnalogSlider_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UAnalogSlider();
	UMG_API UClass* Z_Construct_UClass_USlider();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	UMG_API UFunction* Z_Construct_UDelegateFunction_UMG_OnFloatValueChangedEvent__DelegateSignature();
// End Cross Module References
	void UAnalogSlider::StaticRegisterNativesUAnalogSlider()
	{
	}
	UClass* Z_Construct_UClass_UAnalogSlider_NoRegister()
	{
		return UAnalogSlider::StaticClass();
	}
	struct Z_Construct_UClass_UAnalogSlider_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAnalogCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAnalogCapture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnalogSlider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USlider,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnalogSlider_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A simple widget that shows a sliding bar with a handle that allows you to control the value between 0..1.\n *\n * * No Children\n */" },
		{ "IncludePath", "AnalogSlider.h" },
		{ "ModuleRelativePath", "Public/AnalogSlider.h" },
		{ "ToolTip", "A simple widget that shows a sliding bar with a handle that allows you to control the value between 0..1.\n\n* No Children" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnalogSlider_Statics::NewProp_OnAnalogCapture_MetaData[] = {
		{ "Category", "Widget Event" },
		{ "Comment", "/** Called when the value is changed by slider or typing. */" },
		{ "ModuleRelativePath", "Public/AnalogSlider.h" },
		{ "ToolTip", "Called when the value is changed by slider or typing." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAnalogSlider_Statics::NewProp_OnAnalogCapture = { "OnAnalogCapture", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAnalogSlider, OnAnalogCapture), Z_Construct_UDelegateFunction_UMG_OnFloatValueChangedEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAnalogSlider_Statics::NewProp_OnAnalogCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAnalogSlider_Statics::NewProp_OnAnalogCapture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAnalogSlider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnalogSlider_Statics::NewProp_OnAnalogCapture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnalogSlider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnalogSlider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnalogSlider_Statics::ClassParams = {
		&UAnalogSlider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAnalogSlider_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAnalogSlider_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAnalogSlider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAnalogSlider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnalogSlider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnalogSlider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnalogSlider, 2424514982);
	template<> COMMONUI_API UClass* StaticClass<UAnalogSlider>()
	{
		return UAnalogSlider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnalogSlider(Z_Construct_UClass_UAnalogSlider, &UAnalogSlider::StaticClass, TEXT("/Script/CommonUI"), TEXT("UAnalogSlider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnalogSlider);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
