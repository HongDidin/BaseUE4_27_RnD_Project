// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/UITag.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUITag() {}
// Cross Module References
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FUIActionTag();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UScriptStruct* Z_Construct_UScriptStruct_FUITag();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
// End Cross Module References

static_assert(std::is_polymorphic<FUIActionTag>() == std::is_polymorphic<FUITag>(), "USTRUCT FUIActionTag cannot be polymorphic unless super FUITag is polymorphic");

class UScriptStruct* FUIActionTag::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FUIActionTag_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUIActionTag, Z_Construct_UPackage__Script_CommonUI(), TEXT("UIActionTag"), sizeof(FUIActionTag), Get_Z_Construct_UScriptStruct_FUIActionTag_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FUIActionTag>()
{
	return FUIActionTag::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUIActionTag(FUIActionTag::StaticStruct, TEXT("/Script/CommonUI"), TEXT("UIActionTag"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFUIActionTag
{
	FScriptStruct_CommonUI_StaticRegisterNativesFUIActionTag()
	{
		UScriptStruct::DeferCppStructOps<FUIActionTag>(FName(TEXT("UIActionTag")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFUIActionTag;
	struct Z_Construct_UScriptStruct_FUIActionTag_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUIActionTag_Statics::Struct_MetaDataParams[] = {
		{ "Categories", "UI.Action" },
		{ "ModuleRelativePath", "Public/UITag.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUIActionTag_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUIActionTag>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUIActionTag_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		Z_Construct_UScriptStruct_FUITag,
		&NewStructOps,
		"UIActionTag",
		sizeof(FUIActionTag),
		alignof(FUIActionTag),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUIActionTag_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUIActionTag_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUIActionTag()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUIActionTag_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UIActionTag"), sizeof(FUIActionTag), Get_Z_Construct_UScriptStruct_FUIActionTag_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUIActionTag_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUIActionTag_Hash() { return 3672472021U; }

static_assert(std::is_polymorphic<FUITag>() == std::is_polymorphic<FGameplayTag>(), "USTRUCT FUITag cannot be polymorphic unless super FGameplayTag is polymorphic");

class UScriptStruct* FUITag::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONUI_API uint32 Get_Z_Construct_UScriptStruct_FUITag_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUITag, Z_Construct_UPackage__Script_CommonUI(), TEXT("UITag"), sizeof(FUITag), Get_Z_Construct_UScriptStruct_FUITag_Hash());
	}
	return Singleton;
}
template<> COMMONUI_API UScriptStruct* StaticStruct<FUITag>()
{
	return FUITag::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUITag(FUITag::StaticStruct, TEXT("/Script/CommonUI"), TEXT("UITag"), false, nullptr, nullptr);
static struct FScriptStruct_CommonUI_StaticRegisterNativesFUITag
{
	FScriptStruct_CommonUI_StaticRegisterNativesFUITag()
	{
		UScriptStruct::DeferCppStructOps<FUITag>(FName(TEXT("UITag")));
	}
} ScriptStruct_CommonUI_StaticRegisterNativesFUITag;
	struct Z_Construct_UScriptStruct_FUITag_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUITag_Statics::Struct_MetaDataParams[] = {
		{ "Categories", "UI" },
		{ "ModuleRelativePath", "Public/UITag.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUITag_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUITag>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUITag_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
		Z_Construct_UScriptStruct_FGameplayTag,
		&NewStructOps,
		"UITag",
		sizeof(FUITag),
		alignof(FUITag),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUITag_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUITag_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUITag()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUITag_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UITag"), sizeof(FUITag), Get_Z_Construct_UScriptStruct_FUITag_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUITag_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUITag_Hash() { return 21767750U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
