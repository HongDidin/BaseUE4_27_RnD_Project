// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonListView_generated_h
#error "CommonListView.generated.h already included, missing '#pragma once' in CommonListView.h"
#endif
#define COMMONUI_CommonListView_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetEntrySpacing);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetEntrySpacing);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonListView(); \
	friend struct Z_Construct_UClass_UCommonListView_Statics; \
public: \
	DECLARE_CLASS(UCommonListView, UListView, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonListView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_INCLASS \
private: \
	static void StaticRegisterNativesUCommonListView(); \
	friend struct Z_Construct_UClass_UCommonListView_Statics; \
public: \
	DECLARE_CLASS(UCommonListView, UListView, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonListView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonListView(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonListView) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonListView); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonListView); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonListView(UCommonListView&&); \
	NO_API UCommonListView(const UCommonListView&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonListView(UCommonListView&&); \
	NO_API UCommonListView(const UCommonListView&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonListView); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonListView); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonListView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_85_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h_88_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonListView>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonListView_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
