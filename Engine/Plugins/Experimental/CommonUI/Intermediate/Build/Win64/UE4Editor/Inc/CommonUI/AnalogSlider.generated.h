// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_AnalogSlider_generated_h
#error "AnalogSlider.generated.h already included, missing '#pragma once' in AnalogSlider.h"
#endif
#define COMMONUI_AnalogSlider_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnalogSlider(); \
	friend struct Z_Construct_UClass_UAnalogSlider_Statics; \
public: \
	DECLARE_CLASS(UAnalogSlider, USlider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UAnalogSlider)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUAnalogSlider(); \
	friend struct Z_Construct_UClass_UAnalogSlider_Statics; \
public: \
	DECLARE_CLASS(UAnalogSlider, USlider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UAnalogSlider)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnalogSlider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnalogSlider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnalogSlider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnalogSlider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnalogSlider(UAnalogSlider&&); \
	NO_API UAnalogSlider(const UAnalogSlider&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnalogSlider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnalogSlider(UAnalogSlider&&); \
	NO_API UAnalogSlider(const UAnalogSlider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnalogSlider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnalogSlider); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnalogSlider)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_20_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnalogSlider."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UAnalogSlider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_AnalogSlider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
