// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonTileView_generated_h
#error "CommonTileView.generated.h already included, missing '#pragma once' in CommonTileView.h"
#endif
#define COMMONUI_CommonTileView_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonTileView(); \
	friend struct Z_Construct_UClass_UCommonTileView_Statics; \
public: \
	DECLARE_CLASS(UCommonTileView, UTileView, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTileView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUCommonTileView(); \
	friend struct Z_Construct_UClass_UCommonTileView_Statics; \
public: \
	DECLARE_CLASS(UCommonTileView, UTileView, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonTileView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonTileView(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTileView) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTileView); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTileView); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTileView(UCommonTileView&&); \
	NO_API UCommonTileView(const UCommonTileView&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonTileView(UCommonTileView&&); \
	NO_API UCommonTileView(const UCommonTileView&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonTileView); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonTileView); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonTileView)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_8_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonTileView>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonTileView_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
