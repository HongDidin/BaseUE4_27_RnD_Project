// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonWidgetCarousel;
class UWidget;
#ifdef COMMONUI_CommonWidgetCarousel_generated_h
#error "CommonWidgetCarousel.generated.h already included, missing '#pragma once' in CommonWidgetCarousel.h"
#endif
#define COMMONUI_CommonWidgetCarousel_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_12_DELEGATE \
struct _Script_CommonUI_eventOnCurrentPageIndexChanged_Parms \
{ \
	UCommonWidgetCarousel* CarouselWidget; \
	int32 CurrentPageIndex; \
}; \
static inline void FOnCurrentPageIndexChanged_DelegateWrapper(const FMulticastScriptDelegate& OnCurrentPageIndexChanged, UCommonWidgetCarousel* CarouselWidget, int32 CurrentPageIndex) \
{ \
	_Script_CommonUI_eventOnCurrentPageIndexChanged_Parms Parms; \
	Parms.CarouselWidget=CarouselWidget; \
	Parms.CurrentPageIndex=CurrentPageIndex; \
	OnCurrentPageIndexChanged.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPreviousPage); \
	DECLARE_FUNCTION(execNextPage); \
	DECLARE_FUNCTION(execEndAutoScrolling); \
	DECLARE_FUNCTION(execBeginAutoScrolling); \
	DECLARE_FUNCTION(execGetWidgetAtIndex); \
	DECLARE_FUNCTION(execSetActiveWidget); \
	DECLARE_FUNCTION(execSetActiveWidgetIndex); \
	DECLARE_FUNCTION(execGetActiveWidgetIndex);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPreviousPage); \
	DECLARE_FUNCTION(execNextPage); \
	DECLARE_FUNCTION(execEndAutoScrolling); \
	DECLARE_FUNCTION(execBeginAutoScrolling); \
	DECLARE_FUNCTION(execGetWidgetAtIndex); \
	DECLARE_FUNCTION(execSetActiveWidget); \
	DECLARE_FUNCTION(execSetActiveWidgetIndex); \
	DECLARE_FUNCTION(execGetActiveWidgetIndex);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonWidgetCarousel(); \
	friend struct Z_Construct_UClass_UCommonWidgetCarousel_Statics; \
public: \
	DECLARE_CLASS(UCommonWidgetCarousel, UPanelWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonWidgetCarousel)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUCommonWidgetCarousel(); \
	friend struct Z_Construct_UClass_UCommonWidgetCarousel_Statics; \
public: \
	DECLARE_CLASS(UCommonWidgetCarousel, UPanelWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonWidgetCarousel)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonWidgetCarousel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonWidgetCarousel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonWidgetCarousel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonWidgetCarousel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonWidgetCarousel(UCommonWidgetCarousel&&); \
	NO_API UCommonWidgetCarousel(const UCommonWidgetCarousel&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonWidgetCarousel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonWidgetCarousel(UCommonWidgetCarousel&&); \
	NO_API UCommonWidgetCarousel(const UCommonWidgetCarousel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonWidgetCarousel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonWidgetCarousel); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonWidgetCarousel)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_17_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonWidgetCarousel."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonWidgetCarousel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonWidgetCarousel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
