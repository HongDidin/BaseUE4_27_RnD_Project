// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonRichTextBlock.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonRichTextBlock() {}
// Cross Module References
	COMMONUI_API UEnum* Z_Construct_UEnum_CommonUI_ERichTextInlineIconDisplayMode();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonRichTextBlock_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonRichTextBlock();
	UMG_API UClass* Z_Construct_UClass_URichTextBlock();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonTextStyle_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonTextScrollStyle_NoRegister();
// End Cross Module References
	static UEnum* ERichTextInlineIconDisplayMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonUI_ERichTextInlineIconDisplayMode, Z_Construct_UPackage__Script_CommonUI(), TEXT("ERichTextInlineIconDisplayMode"));
		}
		return Singleton;
	}
	template<> COMMONUI_API UEnum* StaticEnum<ERichTextInlineIconDisplayMode>()
	{
		return ERichTextInlineIconDisplayMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERichTextInlineIconDisplayMode(ERichTextInlineIconDisplayMode_StaticEnum, TEXT("/Script/CommonUI"), TEXT("ERichTextInlineIconDisplayMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonUI_ERichTextInlineIconDisplayMode_Hash() { return 148231850U; }
	UEnum* Z_Construct_UEnum_CommonUI_ERichTextInlineIconDisplayMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonUI();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERichTextInlineIconDisplayMode"), 0, Get_Z_Construct_UEnum_CommonUI_ERichTextInlineIconDisplayMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERichTextInlineIconDisplayMode::IconOnly", (int64)ERichTextInlineIconDisplayMode::IconOnly },
				{ "ERichTextInlineIconDisplayMode::TextOnly", (int64)ERichTextInlineIconDisplayMode::TextOnly },
				{ "ERichTextInlineIconDisplayMode::IconAndText", (int64)ERichTextInlineIconDisplayMode::IconAndText },
				{ "ERichTextInlineIconDisplayMode::MAX", (int64)ERichTextInlineIconDisplayMode::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "// Various ways that we display inline icon that have an icon-name association\n" },
				{ "IconAndText.Comment", "// Show both the icon and the text - use whenever there is space\n" },
				{ "IconAndText.Name", "ERichTextInlineIconDisplayMode::IconAndText" },
				{ "IconAndText.ToolTip", "Show both the icon and the text - use whenever there is space" },
				{ "IconOnly.Comment", "// Only show the icon - use when space is limited\n" },
				{ "IconOnly.Name", "ERichTextInlineIconDisplayMode::IconOnly" },
				{ "IconOnly.ToolTip", "Only show the icon - use when space is limited" },
				{ "MAX.Name", "ERichTextInlineIconDisplayMode::MAX" },
				{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
				{ "TextOnly.Comment", "// Only show the text - use seldom if ever\n" },
				{ "TextOnly.Name", "ERichTextInlineIconDisplayMode::TextOnly" },
				{ "TextOnly.ToolTip", "Only show the text - use seldom if ever" },
				{ "ToolTip", "Various ways that we display inline icon that have an icon-name association" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonUI,
				nullptr,
				"ERichTextInlineIconDisplayMode",
				"ERichTextInlineIconDisplayMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UCommonRichTextBlock::StaticRegisterNativesUCommonRichTextBlock()
	{
	}
	UClass* Z_Construct_UClass_UCommonRichTextBlock_NoRegister()
	{
		return UCommonRichTextBlock::StaticClass();
	}
	struct Z_Construct_UClass_UCommonRichTextBlock_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InlineIconDisplayMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InlineIconDisplayMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InlineIconDisplayMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTintInlineIcon_MetaData[];
#endif
		static void NewProp_bTintInlineIcon_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTintInlineIcon;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTextStyleOverrideClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultTextStyleOverrideClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MobileTextBlockScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MobileTextBlockScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScrollStyle_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScrollStyle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayAllCaps_MetaData[];
#endif
		static void NewProp_bDisplayAllCaps_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayAllCaps;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonRichTextBlock_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URichTextBlock,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CommonRichTextBlock.h" },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode_MetaData[] = {
		{ "Category", "InlineIcon" },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode = { "InlineIconDisplayMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonRichTextBlock, InlineIconDisplayMode), Z_Construct_UEnum_CommonUI_ERichTextInlineIconDisplayMode, METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon_MetaData[] = {
		{ "Category", "InlineIcon" },
		{ "Comment", "/** Toggle it on if the text color should also tint the inline icons. */" },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
		{ "ToolTip", "Toggle it on if the text color should also tint the inline icons." },
	};
#endif
	void Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon_SetBit(void* Obj)
	{
		((UCommonRichTextBlock*)Obj)->bTintInlineIcon = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon = { "bTintInlineIcon", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonRichTextBlock), &Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_DefaultTextStyleOverrideClass_MetaData[] = {
		{ "Category", "Appearance" },
		{ "EditCondition", "bOverrideDefaultStyle" },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_DefaultTextStyleOverrideClass = { "DefaultTextStyleOverrideClass", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonRichTextBlock, DefaultTextStyleOverrideClass), Z_Construct_UClass_UCommonTextStyle_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_DefaultTextStyleOverrideClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_DefaultTextStyleOverrideClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_MobileTextBlockScale_MetaData[] = {
		{ "Category", "Mobile" },
		{ "ClampMax", "5" },
		{ "ClampMin", "0.01" },
		{ "Comment", "/** Mobile font size multiplier. Activated by default on mobile. See CVar Mobile_PreviewFontSize */" },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
		{ "ToolTip", "Mobile font size multiplier. Activated by default on mobile. See CVar Mobile_PreviewFontSize" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_MobileTextBlockScale = { "MobileTextBlockScale", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonRichTextBlock, MobileTextBlockScale), METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_MobileTextBlockScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_MobileTextBlockScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_ScrollStyle_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** References the scroll style asset to use, no reference disables scrolling*/" },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
		{ "ToolTip", "References the scroll style asset to use, no reference disables scrolling" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_ScrollStyle = { "ScrollStyle", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonRichTextBlock, ScrollStyle), Z_Construct_UClass_UCommonTextScrollStyle_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_ScrollStyle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_ScrollStyle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps_MetaData[] = {
		{ "Comment", "/** True to always display text in ALL CAPS */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "bDisplayAllCaps is deprecated. Please use TextTransformPolicy instead." },
		{ "ModuleRelativePath", "Public/CommonRichTextBlock.h" },
		{ "ToolTip", "True to always display text in ALL CAPS" },
	};
#endif
	void Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps_SetBit(void* Obj)
	{
		((UCommonRichTextBlock*)Obj)->bDisplayAllCaps_DEPRECATED = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps = { "bDisplayAllCaps", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonRichTextBlock), &Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonRichTextBlock_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_InlineIconDisplayMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bTintInlineIcon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_DefaultTextStyleOverrideClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_MobileTextBlockScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_ScrollStyle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonRichTextBlock_Statics::NewProp_bDisplayAllCaps,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonRichTextBlock_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonRichTextBlock>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonRichTextBlock_Statics::ClassParams = {
		&UCommonRichTextBlock::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCommonRichTextBlock_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonRichTextBlock_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonRichTextBlock_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonRichTextBlock()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonRichTextBlock_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonRichTextBlock, 2219406524);
	template<> COMMONUI_API UClass* StaticClass<UCommonRichTextBlock>()
	{
		return UCommonRichTextBlock::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonRichTextBlock(Z_Construct_UClass_UCommonRichTextBlock, &UCommonRichTextBlock::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonRichTextBlock"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonRichTextBlock);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UCommonRichTextBlock)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
