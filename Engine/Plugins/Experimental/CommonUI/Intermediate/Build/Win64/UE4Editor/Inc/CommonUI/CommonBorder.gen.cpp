// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonBorder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonBorder() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBorderStyle_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBorderStyle();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBorder_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBorder();
	UMG_API UClass* Z_Construct_UClass_UBorder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMargin();
// End Cross Module References
	DEFINE_FUNCTION(UCommonBorderStyle::execGetBackgroundBrush)
	{
		P_GET_STRUCT_REF(FSlateBrush,Z_Param_Out_Brush);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetBackgroundBrush(Z_Param_Out_Brush);
		P_NATIVE_END;
	}
	void UCommonBorderStyle::StaticRegisterNativesUCommonBorderStyle()
	{
		UClass* Class = UCommonBorderStyle::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBackgroundBrush", &UCommonBorderStyle::execGetBackgroundBrush },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics
	{
		struct CommonBorderStyle_eventGetBackgroundBrush_Parms
		{
			FSlateBrush Brush;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Brush;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::NewProp_Brush = { "Brush", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonBorderStyle_eventGetBackgroundBrush_Parms, Brush), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::NewProp_Brush,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Border Style|Getters" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonBorderStyle, nullptr, "GetBackgroundBrush", nullptr, nullptr, sizeof(CommonBorderStyle_eventGetBackgroundBrush_Parms), Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonBorderStyle_NoRegister()
	{
		return UCommonBorderStyle::StaticClass();
	}
	struct Z_Construct_UClass_UCommonBorderStyle_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Background_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Background;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonBorderStyle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonBorderStyle_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonBorderStyle_GetBackgroundBrush, "GetBackgroundBrush" }, // 1487648229
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorderStyle_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "Common UI" },
		{ "ClassGroupNames", "UI" },
		{ "Comment", "/* \n * ---- All properties must be EditDefaultsOnly, BlueprintReadOnly !!! -----\n * We return the CDO to blueprints, so we cannot allow any changes (blueprint doesn't support const variables)\n */" },
		{ "IncludePath", "CommonBorder.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
		{ "ToolTip", "* ---- All properties must be EditDefaultsOnly, BlueprintReadOnly !!! -----\n* We return the CDO to blueprints, so we cannot allow any changes (blueprint doesn't support const variables)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorderStyle_Statics::NewProp_Background_MetaData[] = {
		{ "Category", "Properties" },
		{ "Comment", "/** The brush for the background of the border */" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
		{ "ToolTip", "The brush for the background of the border" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonBorderStyle_Statics::NewProp_Background = { "Background", nullptr, (EPropertyFlags)0x0010000000010015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonBorderStyle, Background), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UClass_UCommonBorderStyle_Statics::NewProp_Background_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorderStyle_Statics::NewProp_Background_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonBorderStyle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBorderStyle_Statics::NewProp_Background,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonBorderStyle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonBorderStyle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonBorderStyle_Statics::ClassParams = {
		&UCommonBorderStyle::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonBorderStyle_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorderStyle_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonBorderStyle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorderStyle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonBorderStyle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonBorderStyle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonBorderStyle, 1565468163);
	template<> COMMONUI_API UClass* StaticClass<UCommonBorderStyle>()
	{
		return UCommonBorderStyle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonBorderStyle(Z_Construct_UClass_UCommonBorderStyle, &UCommonBorderStyle::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonBorderStyle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonBorderStyle);
	DEFINE_FUNCTION(UCommonBorder::execSetStyle)
	{
		P_GET_OBJECT(UClass,Z_Param_InStyle);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStyle(Z_Param_InStyle);
		P_NATIVE_END;
	}
	void UCommonBorder::StaticRegisterNativesUCommonBorder()
	{
		UClass* Class = UCommonBorder::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetStyle", &UCommonBorder::execSetStyle },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonBorder_SetStyle_Statics
	{
		struct CommonBorder_eventSetStyle_Parms
		{
			TSubclassOf<UCommonBorderStyle>  InStyle;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InStyle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::NewProp_InStyle = { "InStyle", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonBorder_eventSetStyle_Parms, InStyle), Z_Construct_UClass_UCommonBorderStyle_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::NewProp_InStyle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Border" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonBorder, nullptr, "SetStyle", nullptr, nullptr, sizeof(CommonBorder_eventSetStyle_Parms), Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonBorder_SetStyle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonBorder_SetStyle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonBorder_NoRegister()
	{
		return UCommonBorder::StaticClass();
	}
	struct Z_Construct_UClass_UCommonBorder_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Style_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Style;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReducePaddingBySafezone_MetaData[];
#endif
		static void NewProp_bReducePaddingBySafezone_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReducePaddingBySafezone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumPadding_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MinimumPadding;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bStyleNoLongerNeedsConversion_MetaData[];
#endif
		static void NewProp_bStyleNoLongerNeedsConversion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bStyleNoLongerNeedsConversion;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonBorder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBorder,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonBorder_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonBorder_SetStyle, "SetStyle" }, // 1476955362
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorder_Statics::Class_MetaDataParams[] = {
		{ "Category", "Common UI" },
		{ "ClassGroupNames", "UI" },
		{ "DisplayName", "Common Border" },
		{ "IncludePath", "CommonBorder.h" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorder_Statics::NewProp_Style_MetaData[] = {
		{ "Category", "Common Border" },
		{ "Comment", "/** References the border style to use */" },
		{ "ExposeOnSpawn", "TRUE" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
		{ "ToolTip", "References the border style to use" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonBorder_Statics::NewProp_Style = { "Style", nullptr, (EPropertyFlags)0x0015000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonBorder, Style), Z_Construct_UClass_UCommonBorderStyle_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonBorder_Statics::NewProp_Style_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorder_Statics::NewProp_Style_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone_MetaData[] = {
		{ "Category", "Common Border" },
		{ "Comment", "/** Turning this on will cause the safe zone size to be removed from this borders content padding down to the minimum specified */" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
		{ "ToolTip", "Turning this on will cause the safe zone size to be removed from this borders content padding down to the minimum specified" },
	};
#endif
	void Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone_SetBit(void* Obj)
	{
		((UCommonBorder*)Obj)->bReducePaddingBySafezone = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone = { "bReducePaddingBySafezone", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonBorder), &Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorder_Statics::NewProp_MinimumPadding_MetaData[] = {
		{ "Category", "Common Border" },
		{ "Comment", "/** The minimum padding we will reduce to when the safezone grows */" },
		{ "EditCondition", "bReducePaddingBySafezone" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
		{ "ToolTip", "The minimum padding we will reduce to when the safezone grows" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonBorder_Statics::NewProp_MinimumPadding = { "MinimumPadding", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonBorder, MinimumPadding), Z_Construct_UScriptStruct_FMargin, METADATA_PARAMS(Z_Construct_UClass_UCommonBorder_Statics::NewProp_MinimumPadding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorder_Statics::NewProp_MinimumPadding_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion_MetaData[] = {
		{ "Comment", "/** Used to track widgets that were created before changing the default style pointer to null */" },
		{ "ModuleRelativePath", "Public/CommonBorder.h" },
		{ "ToolTip", "Used to track widgets that were created before changing the default style pointer to null" },
	};
#endif
	void Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion_SetBit(void* Obj)
	{
		((UCommonBorder*)Obj)->bStyleNoLongerNeedsConversion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion = { "bStyleNoLongerNeedsConversion", nullptr, (EPropertyFlags)0x0010000800000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCommonBorder), &Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonBorder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBorder_Statics::NewProp_Style,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBorder_Statics::NewProp_bReducePaddingBySafezone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBorder_Statics::NewProp_MinimumPadding,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBorder_Statics::NewProp_bStyleNoLongerNeedsConversion,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonBorder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonBorder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonBorder_Statics::ClassParams = {
		&UCommonBorder::StaticClass,
		"CommonUI",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonBorder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorder_Statics::PropPointers),
		0,
		0x00B000A2u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonBorder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBorder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonBorder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonBorder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonBorder, 190177046);
	template<> COMMONUI_API UClass* StaticClass<UCommonBorder>()
	{
		return UCommonBorder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonBorder(Z_Construct_UClass_UCommonBorder, &UCommonBorder::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonBorder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonBorder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
