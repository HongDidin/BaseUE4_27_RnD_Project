// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonUIEditorSettings_generated_h
#error "CommonUIEditorSettings.generated.h already included, missing '#pragma once' in CommonUIEditorSettings.h"
#endif
#define COMMONUI_CommonUIEditorSettings_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUIEditorSettings(); \
	friend struct Z_Construct_UClass_UCommonUIEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonUIEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUIEditorSettings(); \
	friend struct Z_Construct_UClass_UCommonUIEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UCommonUIEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUIEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIEditorSettings(UCommonUIEditorSettings&&); \
	NO_API UCommonUIEditorSettings(const UCommonUIEditorSettings&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIEditorSettings(UCommonUIEditorSettings&&); \
	NO_API UCommonUIEditorSettings(const UCommonUIEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIEditorSettings)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TemplateTextStyle() { return STRUCT_OFFSET(UCommonUIEditorSettings, TemplateTextStyle); } \
	FORCEINLINE static uint32 __PPO__TemplateButtonStyle() { return STRUCT_OFFSET(UCommonUIEditorSettings, TemplateButtonStyle); } \
	FORCEINLINE static uint32 __PPO__TemplateBorderStyle() { return STRUCT_OFFSET(UCommonUIEditorSettings, TemplateBorderStyle); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_20_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUIEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonUIEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
