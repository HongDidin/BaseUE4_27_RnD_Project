// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWidget;
#ifdef COMMONUI_CommonActivatableWidget_generated_h
#error "CommonActivatableWidget.generated.h already included, missing '#pragma once' in CommonActivatableWidget.h"
#endif
#define COMMONUI_CommonActivatableWidget_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_9_DELEGATE \
static inline void FOnWidgetActivationChanged_DelegateWrapper(const FMulticastScriptDelegate& OnWidgetActivationChanged) \
{ \
	OnWidgetActivationChanged.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDeactivateWidget); \
	DECLARE_FUNCTION(execActivateWidget); \
	DECLARE_FUNCTION(execIsActivated);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDeactivateWidget); \
	DECLARE_FUNCTION(execActivateWidget); \
	DECLARE_FUNCTION(execIsActivated);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_EVENT_PARMS \
	struct CommonActivatableWidget_eventBP_GetDesiredFocusTarget_Parms \
	{ \
		UWidget* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		CommonActivatableWidget_eventBP_GetDesiredFocusTarget_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	}; \
	struct CommonActivatableWidget_eventBP_OnHandleBackAction_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		CommonActivatableWidget_eventBP_OnHandleBackAction_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	};


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidget(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidget, UCommonUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUCommonActivatableWidget(); \
	friend struct Z_Construct_UClass_UCommonActivatableWidget_Statics; \
public: \
	DECLARE_CLASS(UCommonActivatableWidget, UCommonUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonActivatableWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidget(UCommonActivatableWidget&&); \
	NO_API UCommonActivatableWidget(const UCommonActivatableWidget&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonActivatableWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonActivatableWidget(UCommonActivatableWidget&&); \
	NO_API UCommonActivatableWidget(const UCommonActivatableWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonActivatableWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonActivatableWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonActivatableWidget)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAutoActivate() { return STRUCT_OFFSET(UCommonActivatableWidget, bAutoActivate); } \
	FORCEINLINE static uint32 __PPO__bIsBackHandler() { return STRUCT_OFFSET(UCommonActivatableWidget, bIsBackHandler); } \
	FORCEINLINE static uint32 __PPO__bSupportsActivationFocus() { return STRUCT_OFFSET(UCommonActivatableWidget, bSupportsActivationFocus); } \
	FORCEINLINE static uint32 __PPO__bIsModal() { return STRUCT_OFFSET(UCommonActivatableWidget, bIsModal); } \
	FORCEINLINE static uint32 __PPO__bAutoRestoreFocus() { return STRUCT_OFFSET(UCommonActivatableWidget, bAutoRestoreFocus); } \
	FORCEINLINE static uint32 __PPO__bSetVisibilityOnActivated() { return STRUCT_OFFSET(UCommonActivatableWidget, bSetVisibilityOnActivated); } \
	FORCEINLINE static uint32 __PPO__ActivatedVisibility() { return STRUCT_OFFSET(UCommonActivatableWidget, ActivatedVisibility); } \
	FORCEINLINE static uint32 __PPO__bSetVisibilityOnDeactivated() { return STRUCT_OFFSET(UCommonActivatableWidget, bSetVisibilityOnDeactivated); } \
	FORCEINLINE static uint32 __PPO__DeactivatedVisibility() { return STRUCT_OFFSET(UCommonActivatableWidget, DeactivatedVisibility); } \
	FORCEINLINE static uint32 __PPO__BP_OnWidgetActivated() { return STRUCT_OFFSET(UCommonActivatableWidget, BP_OnWidgetActivated); } \
	FORCEINLINE static uint32 __PPO__BP_OnWidgetDeactivated() { return STRUCT_OFFSET(UCommonActivatableWidget, BP_OnWidgetDeactivated); } \
	FORCEINLINE static uint32 __PPO__bIsActive() { return STRUCT_OFFSET(UCommonActivatableWidget, bIsActive); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_29_PROLOG \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonActivatableWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonActivatableWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
