// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWidget;
#ifdef COMMONUI_CommonVisibilitySwitcher_generated_h
#error "CommonVisibilitySwitcher.generated.h already included, missing '#pragma once' in CommonVisibilitySwitcher.h"
#endif
#define COMMONUI_CommonVisibilitySwitcher_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDeactivateVisibleSlot); \
	DECLARE_FUNCTION(execActivateVisibleSlot); \
	DECLARE_FUNCTION(execDecrementActiveWidgetIndex); \
	DECLARE_FUNCTION(execIncrementActiveWidgetIndex); \
	DECLARE_FUNCTION(execSetActiveWidget); \
	DECLARE_FUNCTION(execGetActiveWidget); \
	DECLARE_FUNCTION(execGetActiveWidgetIndex); \
	DECLARE_FUNCTION(execSetActiveWidgetIndex);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDeactivateVisibleSlot); \
	DECLARE_FUNCTION(execActivateVisibleSlot); \
	DECLARE_FUNCTION(execDecrementActiveWidgetIndex); \
	DECLARE_FUNCTION(execIncrementActiveWidgetIndex); \
	DECLARE_FUNCTION(execSetActiveWidget); \
	DECLARE_FUNCTION(execGetActiveWidget); \
	DECLARE_FUNCTION(execGetActiveWidgetIndex); \
	DECLARE_FUNCTION(execSetActiveWidgetIndex);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonVisibilitySwitcher(); \
	friend struct Z_Construct_UClass_UCommonVisibilitySwitcher_Statics; \
public: \
	DECLARE_CLASS(UCommonVisibilitySwitcher, UOverlay, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVisibilitySwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCommonVisibilitySwitcher(); \
	friend struct Z_Construct_UClass_UCommonVisibilitySwitcher_Statics; \
public: \
	DECLARE_CLASS(UCommonVisibilitySwitcher, UOverlay, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonVisibilitySwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonVisibilitySwitcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVisibilitySwitcher) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVisibilitySwitcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVisibilitySwitcher); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVisibilitySwitcher(UCommonVisibilitySwitcher&&); \
	NO_API UCommonVisibilitySwitcher(const UCommonVisibilitySwitcher&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonVisibilitySwitcher(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonVisibilitySwitcher(UCommonVisibilitySwitcher&&); \
	NO_API UCommonVisibilitySwitcher(const UCommonVisibilitySwitcher&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonVisibilitySwitcher); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonVisibilitySwitcher); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonVisibilitySwitcher)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ShownVisibility() { return STRUCT_OFFSET(UCommonVisibilitySwitcher, ShownVisibility); } \
	FORCEINLINE static uint32 __PPO__ActiveWidgetIndex() { return STRUCT_OFFSET(UCommonVisibilitySwitcher, ActiveWidgetIndex); } \
	FORCEINLINE static uint32 __PPO__bAutoActivateSlot() { return STRUCT_OFFSET(UCommonVisibilitySwitcher, bAutoActivateSlot); } \
	FORCEINLINE static uint32 __PPO__bActivateFirstSlotOnAdding() { return STRUCT_OFFSET(UCommonVisibilitySwitcher, bActivateFirstSlotOnAdding); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_12_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonVisibilitySwitcher>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonVisibilitySwitcher_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
