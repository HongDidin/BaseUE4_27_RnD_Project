// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCommonButtonBase;
class UCommonTextStyle;
struct FMargin;
struct FSlateBrush;
enum class ECommonInputType : uint8;
class USoundBase;
class UMaterialInstanceDynamic;
struct FDataTableRowHandle;
class UCommonButtonStyle;
#ifdef COMMONUI_CommonButtonBase_generated_h
#error "CommonButtonBase.generated.h already included, missing '#pragma once' in CommonButtonBase.h"
#endif
#define COMMONUI_CommonButtonBase_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonButtonStyleOptionalSlateSound_Statics; \
	COMMONUI_API static class UScriptStruct* StaticStruct();


template<> COMMONUI_API UScriptStruct* StaticStruct<struct FCommonButtonStyleOptionalSlateSound>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_272_DELEGATE \
struct _Script_CommonUI_eventCommonButtonBaseClicked_Parms \
{ \
	UCommonButtonBase* Button; \
}; \
static inline void FCommonButtonBaseClicked_DelegateWrapper(const FMulticastScriptDelegate& CommonButtonBaseClicked, UCommonButtonBase* Button) \
{ \
	_Script_CommonUI_eventCommonButtonBaseClicked_Parms Parms; \
	Parms.Button=Button; \
	CommonButtonBaseClicked.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_271_DELEGATE \
struct _Script_CommonUI_eventCommonSelectedStateChangedBase_Parms \
{ \
	UCommonButtonBase* Button; \
	bool Selected; \
}; \
static inline void FCommonSelectedStateChangedBase_DelegateWrapper(const FMulticastScriptDelegate& CommonSelectedStateChangedBase, UCommonButtonBase* Button, bool Selected) \
{ \
	_Script_CommonUI_eventCommonSelectedStateChangedBase_Parms Parms; \
	Parms.Button=Button; \
	Parms.Selected=Selected ? true : false; \
	CommonSelectedStateChangedBase.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDisabledTextStyle); \
	DECLARE_FUNCTION(execGetSelectedHoveredTextStyle); \
	DECLARE_FUNCTION(execGetSelectedTextStyle); \
	DECLARE_FUNCTION(execGetNormalHoveredTextStyle); \
	DECLARE_FUNCTION(execGetNormalTextStyle); \
	DECLARE_FUNCTION(execGetCustomPadding); \
	DECLARE_FUNCTION(execGetButtonPadding); \
	DECLARE_FUNCTION(execGetDisabledBrush); \
	DECLARE_FUNCTION(execGetSelectedPressedBrush); \
	DECLARE_FUNCTION(execGetSelectedHoveredBrush); \
	DECLARE_FUNCTION(execGetSelectedBaseBrush); \
	DECLARE_FUNCTION(execGetNormalPressedBrush); \
	DECLARE_FUNCTION(execGetNormalHoveredBrush); \
	DECLARE_FUNCTION(execGetNormalBaseBrush); \
	DECLARE_FUNCTION(execGetMaterialBrush);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDisabledTextStyle); \
	DECLARE_FUNCTION(execGetSelectedHoveredTextStyle); \
	DECLARE_FUNCTION(execGetSelectedTextStyle); \
	DECLARE_FUNCTION(execGetNormalHoveredTextStyle); \
	DECLARE_FUNCTION(execGetNormalTextStyle); \
	DECLARE_FUNCTION(execGetCustomPadding); \
	DECLARE_FUNCTION(execGetButtonPadding); \
	DECLARE_FUNCTION(execGetDisabledBrush); \
	DECLARE_FUNCTION(execGetSelectedPressedBrush); \
	DECLARE_FUNCTION(execGetSelectedHoveredBrush); \
	DECLARE_FUNCTION(execGetSelectedBaseBrush); \
	DECLARE_FUNCTION(execGetNormalPressedBrush); \
	DECLARE_FUNCTION(execGetNormalHoveredBrush); \
	DECLARE_FUNCTION(execGetNormalBaseBrush); \
	DECLARE_FUNCTION(execGetMaterialBrush);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonButtonStyle(); \
	friend struct Z_Construct_UClass_UCommonButtonStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonButtonStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonButtonStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUCommonButtonStyle(); \
	friend struct Z_Construct_UClass_UCommonButtonStyle_Statics; \
public: \
	DECLARE_CLASS(UCommonButtonStyle, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonButtonStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonButtonStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonButtonStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonButtonStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonButtonStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonButtonStyle(UCommonButtonStyle&&); \
	NO_API UCommonButtonStyle(const UCommonButtonStyle&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonButtonStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonButtonStyle(UCommonButtonStyle&&); \
	NO_API UCommonButtonStyle(const UCommonButtonStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonButtonStyle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonButtonStyle); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonButtonStyle)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_50_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_53_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonButtonStyle>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonButtonInternalBase(); \
	friend struct Z_Construct_UClass_UCommonButtonInternalBase_Statics; \
public: \
	DECLARE_CLASS(UCommonButtonInternalBase, UButton, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonButtonInternalBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_INCLASS \
private: \
	static void StaticRegisterNativesUCommonButtonInternalBase(); \
	friend struct Z_Construct_UClass_UCommonButtonInternalBase_Statics; \
public: \
	DECLARE_CLASS(UCommonButtonInternalBase, UButton, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonButtonInternalBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonButtonInternalBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonButtonInternalBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonButtonInternalBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonButtonInternalBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonButtonInternalBase(UCommonButtonInternalBase&&); \
	NO_API UCommonButtonInternalBase(const UCommonButtonInternalBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonButtonInternalBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonButtonInternalBase(UCommonButtonInternalBase&&); \
	NO_API UCommonButtonInternalBase(const UCommonButtonInternalBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonButtonInternalBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonButtonInternalBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonButtonInternalBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MinWidth() { return STRUCT_OFFSET(UCommonButtonInternalBase, MinWidth); } \
	FORCEINLINE static uint32 __PPO__MinHeight() { return STRUCT_OFFSET(UCommonButtonInternalBase, MinHeight); } \
	FORCEINLINE static uint32 __PPO__bButtonEnabled() { return STRUCT_OFFSET(UCommonButtonInternalBase, bButtonEnabled); } \
	FORCEINLINE static uint32 __PPO__bInteractionEnabled() { return STRUCT_OFFSET(UCommonButtonInternalBase, bInteractionEnabled); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_206_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_209_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonButtonInternalBase."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonButtonInternalBase>();

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execNativeOnActionComplete); \
	DECLARE_FUNCTION(execNativeOnActionProgress); \
	DECLARE_FUNCTION(execSetSelectedInternal); \
	DECLARE_FUNCTION(execStopDoubleClickPropagation); \
	DECLARE_FUNCTION(execHandleButtonReleased); \
	DECLARE_FUNCTION(execHandleButtonPressed); \
	DECLARE_FUNCTION(execHandleFocusReceived); \
	DECLARE_FUNCTION(execHandleButtonClicked); \
	DECLARE_FUNCTION(execHandleTriggeringActionCommited); \
	DECLARE_FUNCTION(execOnInputMethodChanged); \
	DECLARE_FUNCTION(execSetHoveredSoundOverride); \
	DECLARE_FUNCTION(execSetPressedSoundOverride); \
	DECLARE_FUNCTION(execSetInputActionProgressMaterial); \
	DECLARE_FUNCTION(execGetSingleMaterialStyleMID); \
	DECLARE_FUNCTION(execGetIsFocusable); \
	DECLARE_FUNCTION(execSetIsFocusable); \
	DECLARE_FUNCTION(execGetInputAction); \
	DECLARE_FUNCTION(execSetTriggeringInputAction); \
	DECLARE_FUNCTION(execSetTriggeredInputAction); \
	DECLARE_FUNCTION(execSetMinDimensions); \
	DECLARE_FUNCTION(execGetCurrentTextStyleClass); \
	DECLARE_FUNCTION(execGetCurrentTextStyle); \
	DECLARE_FUNCTION(execGetCurrentCustomPadding); \
	DECLARE_FUNCTION(execGetCurrentButtonPadding); \
	DECLARE_FUNCTION(execGetStyle); \
	DECLARE_FUNCTION(execSetStyle); \
	DECLARE_FUNCTION(execGetShouldSelectUponReceivingFocus); \
	DECLARE_FUNCTION(execSetShouldSelectUponReceivingFocus); \
	DECLARE_FUNCTION(execClearSelection); \
	DECLARE_FUNCTION(execGetSelected); \
	DECLARE_FUNCTION(execSetIsSelected); \
	DECLARE_FUNCTION(execSetShouldUseFallbackDefaultInputAction); \
	DECLARE_FUNCTION(execSetIsToggleable); \
	DECLARE_FUNCTION(execSetIsInteractableWhenSelected); \
	DECLARE_FUNCTION(execSetIsSelectable); \
	DECLARE_FUNCTION(execSetPressMethod); \
	DECLARE_FUNCTION(execSetTouchMethod); \
	DECLARE_FUNCTION(execSetClickMethod); \
	DECLARE_FUNCTION(execIsPressed); \
	DECLARE_FUNCTION(execIsInteractionEnabled); \
	DECLARE_FUNCTION(execSetIsInteractionEnabled); \
	DECLARE_FUNCTION(execDisableButtonWithReason);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execNativeOnActionComplete); \
	DECLARE_FUNCTION(execNativeOnActionProgress); \
	DECLARE_FUNCTION(execSetSelectedInternal); \
	DECLARE_FUNCTION(execStopDoubleClickPropagation); \
	DECLARE_FUNCTION(execHandleButtonReleased); \
	DECLARE_FUNCTION(execHandleButtonPressed); \
	DECLARE_FUNCTION(execHandleFocusReceived); \
	DECLARE_FUNCTION(execHandleButtonClicked); \
	DECLARE_FUNCTION(execHandleTriggeringActionCommited); \
	DECLARE_FUNCTION(execOnInputMethodChanged); \
	DECLARE_FUNCTION(execSetHoveredSoundOverride); \
	DECLARE_FUNCTION(execSetPressedSoundOverride); \
	DECLARE_FUNCTION(execSetInputActionProgressMaterial); \
	DECLARE_FUNCTION(execGetSingleMaterialStyleMID); \
	DECLARE_FUNCTION(execGetIsFocusable); \
	DECLARE_FUNCTION(execSetIsFocusable); \
	DECLARE_FUNCTION(execGetInputAction); \
	DECLARE_FUNCTION(execSetTriggeringInputAction); \
	DECLARE_FUNCTION(execSetTriggeredInputAction); \
	DECLARE_FUNCTION(execSetMinDimensions); \
	DECLARE_FUNCTION(execGetCurrentTextStyleClass); \
	DECLARE_FUNCTION(execGetCurrentTextStyle); \
	DECLARE_FUNCTION(execGetCurrentCustomPadding); \
	DECLARE_FUNCTION(execGetCurrentButtonPadding); \
	DECLARE_FUNCTION(execGetStyle); \
	DECLARE_FUNCTION(execSetStyle); \
	DECLARE_FUNCTION(execGetShouldSelectUponReceivingFocus); \
	DECLARE_FUNCTION(execSetShouldSelectUponReceivingFocus); \
	DECLARE_FUNCTION(execClearSelection); \
	DECLARE_FUNCTION(execGetSelected); \
	DECLARE_FUNCTION(execSetIsSelected); \
	DECLARE_FUNCTION(execSetShouldUseFallbackDefaultInputAction); \
	DECLARE_FUNCTION(execSetIsToggleable); \
	DECLARE_FUNCTION(execSetIsInteractableWhenSelected); \
	DECLARE_FUNCTION(execSetIsSelectable); \
	DECLARE_FUNCTION(execSetPressMethod); \
	DECLARE_FUNCTION(execSetTouchMethod); \
	DECLARE_FUNCTION(execSetClickMethod); \
	DECLARE_FUNCTION(execIsPressed); \
	DECLARE_FUNCTION(execIsInteractionEnabled); \
	DECLARE_FUNCTION(execSetIsInteractionEnabled); \
	DECLARE_FUNCTION(execDisableButtonWithReason);


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_EVENT_PARMS \
	struct CommonButtonBase_eventOnActionProgress_Parms \
	{ \
		float HeldPercent; \
	}; \
	struct CommonButtonBase_eventOnTriggeredInputActionChanged_Parms \
	{ \
		FDataTableRowHandle NewTriggeredAction; \
	};


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonButtonBase(); \
	friend struct Z_Construct_UClass_UCommonButtonBase_Statics; \
public: \
	DECLARE_CLASS(UCommonButtonBase, UCommonUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonButtonBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_INCLASS \
private: \
	static void StaticRegisterNativesUCommonButtonBase(); \
	friend struct Z_Construct_UClass_UCommonButtonBase_Statics; \
public: \
	DECLARE_CLASS(UCommonButtonBase, UCommonUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonButtonBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonButtonBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonButtonBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonButtonBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonButtonBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonButtonBase(UCommonButtonBase&&); \
	NO_API UCommonButtonBase(const UCommonButtonBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonButtonBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonButtonBase(UCommonButtonBase&&); \
	NO_API UCommonButtonBase(const UCommonButtonBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonButtonBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonButtonBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonButtonBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MinWidth() { return STRUCT_OFFSET(UCommonButtonBase, MinWidth); } \
	FORCEINLINE static uint32 __PPO__MinHeight() { return STRUCT_OFFSET(UCommonButtonBase, MinHeight); } \
	FORCEINLINE static uint32 __PPO__Style() { return STRUCT_OFFSET(UCommonButtonBase, Style); } \
	FORCEINLINE static uint32 __PPO__bHideInputAction() { return STRUCT_OFFSET(UCommonButtonBase, bHideInputAction); } \
	FORCEINLINE static uint32 __PPO__PressedSlateSoundOverride() { return STRUCT_OFFSET(UCommonButtonBase, PressedSlateSoundOverride); } \
	FORCEINLINE static uint32 __PPO__HoveredSlateSoundOverride() { return STRUCT_OFFSET(UCommonButtonBase, HoveredSlateSoundOverride); } \
	FORCEINLINE static uint32 __PPO__OnSelectedChangedBase() { return STRUCT_OFFSET(UCommonButtonBase, OnSelectedChangedBase); } \
	FORCEINLINE static uint32 __PPO__OnButtonBaseClicked() { return STRUCT_OFFSET(UCommonButtonBase, OnButtonBaseClicked); } \
	FORCEINLINE static uint32 __PPO__OnButtonBaseDoubleClicked() { return STRUCT_OFFSET(UCommonButtonBase, OnButtonBaseDoubleClicked); } \
	FORCEINLINE static uint32 __PPO__OnButtonBaseHovered() { return STRUCT_OFFSET(UCommonButtonBase, OnButtonBaseHovered); } \
	FORCEINLINE static uint32 __PPO__OnButtonBaseUnhovered() { return STRUCT_OFFSET(UCommonButtonBase, OnButtonBaseUnhovered); } \
	FORCEINLINE static uint32 __PPO__bIsPersistentBinding() { return STRUCT_OFFSET(UCommonButtonBase, bIsPersistentBinding); } \
	FORCEINLINE static uint32 __PPO__InputModeOverride() { return STRUCT_OFFSET(UCommonButtonBase, InputModeOverride); } \
	FORCEINLINE static uint32 __PPO__SingleMaterialStyleMID() { return STRUCT_OFFSET(UCommonButtonBase, SingleMaterialStyleMID); } \
	FORCEINLINE static uint32 __PPO__NormalStyle() { return STRUCT_OFFSET(UCommonButtonBase, NormalStyle); } \
	FORCEINLINE static uint32 __PPO__SelectedStyle() { return STRUCT_OFFSET(UCommonButtonBase, SelectedStyle); } \
	FORCEINLINE static uint32 __PPO__DisabledStyle() { return STRUCT_OFFSET(UCommonButtonBase, DisabledStyle); } \
	FORCEINLINE static uint32 __PPO__InputActionWidget() { return STRUCT_OFFSET(UCommonButtonBase, InputActionWidget); }


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_274_PROLOG \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h_277_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CommonButtonBase."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonButtonBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_CommonButtonBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
