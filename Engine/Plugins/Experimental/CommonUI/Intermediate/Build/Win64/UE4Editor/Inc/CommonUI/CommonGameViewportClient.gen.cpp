// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonGameViewportClient.h"
#include "Engine/Classes/Engine/Engine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonGameViewportClient() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonGameViewportClient_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonGameViewportClient();
	ENGINE_API UClass* Z_Construct_UClass_UGameViewportClient();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
// End Cross Module References
	void UCommonGameViewportClient::StaticRegisterNativesUCommonGameViewportClient()
	{
	}
	UClass* Z_Construct_UClass_UCommonGameViewportClient_NoRegister()
	{
		return UCommonGameViewportClient::StaticClass();
	}
	struct Z_Construct_UClass_UCommonGameViewportClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonGameViewportClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameViewportClient,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonGameViewportClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* CommonUI Viewport to reroute input to UI first. Needed to allow CommonUI to route / handle inputs.\n*/" },
		{ "IncludePath", "CommonGameViewportClient.h" },
		{ "ModuleRelativePath", "Public/CommonGameViewportClient.h" },
		{ "ToolTip", "CommonUI Viewport to reroute input to UI first. Needed to allow CommonUI to route / handle inputs." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonGameViewportClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonGameViewportClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonGameViewportClient_Statics::ClassParams = {
		&UCommonGameViewportClient::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UCommonGameViewportClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonGameViewportClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonGameViewportClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonGameViewportClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonGameViewportClient, 3623832812);
	template<> COMMONUI_API UClass* StaticClass<UCommonGameViewportClient>()
	{
		return UCommonGameViewportClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonGameViewportClient(Z_Construct_UClass_UCommonGameViewportClient, &UCommonGameViewportClient::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonGameViewportClient"), false, nullptr, nullptr, nullptr);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
