// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONUI_CommonUIActionRouterBase_generated_h
#error "CommonUIActionRouterBase.generated.h already included, missing '#pragma once' in CommonUIActionRouterBase.h"
#endif
#define COMMONUI_CommonUIActionRouterBase_generated_h

#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCommonUIActionRouterBase(); \
	friend struct Z_Construct_UClass_UCommonUIActionRouterBase_Statics; \
public: \
	DECLARE_CLASS(UCommonUIActionRouterBase, ULocalPlayerSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIActionRouterBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUCommonUIActionRouterBase(); \
	friend struct Z_Construct_UClass_UCommonUIActionRouterBase_Statics; \
public: \
	DECLARE_CLASS(UCommonUIActionRouterBase, ULocalPlayerSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonUI"), NO_API) \
	DECLARE_SERIALIZER(UCommonUIActionRouterBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCommonUIActionRouterBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCommonUIActionRouterBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIActionRouterBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIActionRouterBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIActionRouterBase(UCommonUIActionRouterBase&&); \
	NO_API UCommonUIActionRouterBase(const UCommonUIActionRouterBase&); \
public:


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCommonUIActionRouterBase(UCommonUIActionRouterBase&&); \
	NO_API UCommonUIActionRouterBase(const UCommonUIActionRouterBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCommonUIActionRouterBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCommonUIActionRouterBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCommonUIActionRouterBase)


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_46_PROLOG
#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_INCLASS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h_49_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONUI_API UClass* StaticClass<class UCommonUIActionRouterBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonUI_Source_CommonUI_Public_Input_CommonUIActionRouterBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
