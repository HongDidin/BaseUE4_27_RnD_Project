// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/CommonWidgetCarouselNavBar.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonWidgetCarouselNavBar() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonWidgetCarouselNavBar_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonWidgetCarouselNavBar();
	UMG_API UClass* Z_Construct_UClass_UWidget();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonBase_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonWidgetCarousel_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMargin();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonGroupBase_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UCommonWidgetCarouselNavBar::execHandleButtonClicked)
	{
		P_GET_OBJECT(UCommonButtonBase,Z_Param_AssociatedButton);
		P_GET_PROPERTY(FIntProperty,Z_Param_ButtonIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleButtonClicked(Z_Param_AssociatedButton,Z_Param_ButtonIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarouselNavBar::execHandlePageChanged)
	{
		P_GET_OBJECT(UCommonWidgetCarousel,Z_Param_CommonCarousel);
		P_GET_PROPERTY(FIntProperty,Z_Param_PageIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandlePageChanged(Z_Param_CommonCarousel,Z_Param_PageIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCommonWidgetCarouselNavBar::execSetLinkedCarousel)
	{
		P_GET_OBJECT(UCommonWidgetCarousel,Z_Param_CommonCarousel);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetLinkedCarousel(Z_Param_CommonCarousel);
		P_NATIVE_END;
	}
	void UCommonWidgetCarouselNavBar::StaticRegisterNativesUCommonWidgetCarouselNavBar()
	{
		UClass* Class = UCommonWidgetCarouselNavBar::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "HandleButtonClicked", &UCommonWidgetCarouselNavBar::execHandleButtonClicked },
			{ "HandlePageChanged", &UCommonWidgetCarouselNavBar::execHandlePageChanged },
			{ "SetLinkedCarousel", &UCommonWidgetCarouselNavBar::execSetLinkedCarousel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics
	{
		struct CommonWidgetCarouselNavBar_eventHandleButtonClicked_Parms
		{
			UCommonButtonBase* AssociatedButton;
			int32 ButtonIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssociatedButton_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssociatedButton;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ButtonIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_AssociatedButton_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_AssociatedButton = { "AssociatedButton", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarouselNavBar_eventHandleButtonClicked_Parms, AssociatedButton), Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_AssociatedButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_AssociatedButton_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_ButtonIndex = { "ButtonIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarouselNavBar_eventHandleButtonClicked_Parms, ButtonIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_AssociatedButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::NewProp_ButtonIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarouselNavBar, nullptr, "HandleButtonClicked", nullptr, nullptr, sizeof(CommonWidgetCarouselNavBar_eventHandleButtonClicked_Parms), Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics
	{
		struct CommonWidgetCarouselNavBar_eventHandlePageChanged_Parms
		{
			UCommonWidgetCarousel* CommonCarousel;
			int32 PageIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonCarousel_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CommonCarousel;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PageIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_CommonCarousel_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_CommonCarousel = { "CommonCarousel", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarouselNavBar_eventHandlePageChanged_Parms, CommonCarousel), Z_Construct_UClass_UCommonWidgetCarousel_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_CommonCarousel_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_CommonCarousel_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_PageIndex = { "PageIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarouselNavBar_eventHandlePageChanged_Parms, PageIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_CommonCarousel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::NewProp_PageIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarouselNavBar, nullptr, "HandlePageChanged", nullptr, nullptr, sizeof(CommonWidgetCarouselNavBar_eventHandlePageChanged_Parms), Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics
	{
		struct CommonWidgetCarouselNavBar_eventSetLinkedCarousel_Parms
		{
			UCommonWidgetCarousel* CommonCarousel;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommonCarousel_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CommonCarousel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::NewProp_CommonCarousel_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::NewProp_CommonCarousel = { "CommonCarousel", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CommonWidgetCarouselNavBar_eventSetLinkedCarousel_Parms, CommonCarousel), Z_Construct_UClass_UCommonWidgetCarousel_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::NewProp_CommonCarousel_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::NewProp_CommonCarousel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::NewProp_CommonCarousel,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::Function_MetaDataParams[] = {
		{ "Category", "CarouselNavBar" },
		{ "Comment", "/**\n\x09 * Establishes the Widget Carousel instance that this Nav Bar should interact with\n\x09 * @param CommonCarousel The carousel that this nav bar should be associated with and manipulate\n\x09 */" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
		{ "ToolTip", "Establishes the Widget Carousel instance that this Nav Bar should interact with\n@param CommonCarousel The carousel that this nav bar should be associated with and manipulate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonWidgetCarouselNavBar, nullptr, "SetLinkedCarousel", nullptr, nullptr, sizeof(CommonWidgetCarouselNavBar_eventSetLinkedCarousel_Parms), Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonWidgetCarouselNavBar_NoRegister()
	{
		return UCommonWidgetCarouselNavBar::StaticClass();
	}
	struct Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonWidgetType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ButtonWidgetType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonPadding_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ButtonPadding;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinkedCarousel_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LinkedCarousel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonGroup;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Buttons_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Buttons_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Buttons;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandleButtonClicked, "HandleButtonClicked" }, // 1790825734
		{ &Z_Construct_UFunction_UCommonWidgetCarouselNavBar_HandlePageChanged, "HandlePageChanged" }, // 3275878950
		{ &Z_Construct_UFunction_UCommonWidgetCarouselNavBar_SetLinkedCarousel, "SetLinkedCarousel" }, // 3935847162
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A Navigation control for a Carousel\n */" },
		{ "IncludePath", "CommonWidgetCarouselNavBar.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
		{ "ToolTip", "A Navigation control for a Carousel" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonWidgetType_MetaData[] = {
		{ "Category", "CarouselNavBar" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonWidgetType = { "ButtonWidgetType", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarouselNavBar, ButtonWidgetType), Z_Construct_UClass_UCommonButtonBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonWidgetType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonWidgetType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonPadding_MetaData[] = {
		{ "Category", "CarouselNavBar" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonPadding = { "ButtonPadding", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarouselNavBar, ButtonPadding), Z_Construct_UScriptStruct_FMargin, METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonPadding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonPadding_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_LinkedCarousel_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_LinkedCarousel = { "LinkedCarousel", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarouselNavBar, LinkedCarousel), Z_Construct_UClass_UCommonWidgetCarousel_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_LinkedCarousel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_LinkedCarousel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonGroup_MetaData[] = {
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonGroup = { "ButtonGroup", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarouselNavBar, ButtonGroup), Z_Construct_UClass_UCommonButtonGroupBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonGroup_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons_Inner = { "Buttons", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCommonButtonBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CommonWidgetCarouselNavBar.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons = { "Buttons", nullptr, (EPropertyFlags)0x0020088000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonWidgetCarouselNavBar, Buttons), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonWidgetType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonPadding,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_LinkedCarousel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_ButtonGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::NewProp_Buttons,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonWidgetCarouselNavBar>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::ClassParams = {
		&UCommonWidgetCarouselNavBar::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonWidgetCarouselNavBar()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonWidgetCarouselNavBar_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonWidgetCarouselNavBar, 769120642);
	template<> COMMONUI_API UClass* StaticClass<UCommonWidgetCarouselNavBar>()
	{
		return UCommonWidgetCarouselNavBar::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonWidgetCarouselNavBar(Z_Construct_UClass_UCommonWidgetCarouselNavBar, &UCommonWidgetCarouselNavBar::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonWidgetCarouselNavBar"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonWidgetCarouselNavBar);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
