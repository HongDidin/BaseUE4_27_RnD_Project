// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonUI/Public/Input/CommonBoundActionButton.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCommonBoundActionButton() {}
// Cross Module References
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBoundActionButton_NoRegister();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonBoundActionButton();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonButtonBase();
	UPackage* Z_Construct_UPackage__Script_CommonUI();
	COMMONUI_API UClass* Z_Construct_UClass_UCommonTextBlock_NoRegister();
// End Cross Module References
	static FName NAME_UCommonBoundActionButton_OnUpdateInputAction = FName(TEXT("OnUpdateInputAction"));
	void UCommonBoundActionButton::OnUpdateInputAction()
	{
		ProcessEvent(FindFunctionChecked(NAME_UCommonBoundActionButton_OnUpdateInputAction),NULL);
	}
	void UCommonBoundActionButton::StaticRegisterNativesUCommonBoundActionButton()
	{
	}
	struct Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction_Statics::Function_MetaDataParams[] = {
		{ "Category", "Common Bound Action" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionButton.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCommonBoundActionButton, nullptr, "OnUpdateInputAction", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCommonBoundActionButton_NoRegister()
	{
		return UCommonBoundActionButton::StaticClass();
	}
	struct Z_Construct_UClass_UCommonBoundActionButton_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Text_ActionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Text_ActionName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCommonBoundActionButton_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommonButtonBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonUI,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCommonBoundActionButton_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCommonBoundActionButton_OnUpdateInputAction, "OnUpdateInputAction" }, // 1620541402
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBoundActionButton_Statics::Class_MetaDataParams[] = {
		{ "DisableNativeTick", "" },
		{ "IncludePath", "Input/CommonBoundActionButton.h" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionButton.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCommonBoundActionButton_Statics::NewProp_Text_ActionName_MetaData[] = {
		{ "BindWidget", "" },
		{ "Category", "Text Block" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Input/CommonBoundActionButton.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCommonBoundActionButton_Statics::NewProp_Text_ActionName = { "Text_ActionName", nullptr, (EPropertyFlags)0x002008000008001c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCommonBoundActionButton, Text_ActionName), Z_Construct_UClass_UCommonTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCommonBoundActionButton_Statics::NewProp_Text_ActionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionButton_Statics::NewProp_Text_ActionName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCommonBoundActionButton_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCommonBoundActionButton_Statics::NewProp_Text_ActionName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCommonBoundActionButton_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCommonBoundActionButton>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCommonBoundActionButton_Statics::ClassParams = {
		&UCommonBoundActionButton::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCommonBoundActionButton_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionButton_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCommonBoundActionButton_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCommonBoundActionButton_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCommonBoundActionButton()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCommonBoundActionButton_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCommonBoundActionButton, 2472117826);
	template<> COMMONUI_API UClass* StaticClass<UCommonBoundActionButton>()
	{
		return UCommonBoundActionButton::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCommonBoundActionButton(Z_Construct_UClass_UCommonBoundActionButton, &UCommonBoundActionButton::StaticClass, TEXT("/Script/CommonUI"), TEXT("UCommonBoundActionButton"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCommonBoundActionButton);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
