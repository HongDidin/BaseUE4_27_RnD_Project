// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COLORCORRECTREGIONS_ColorCorrectRegion_generated_h
#error "ColorCorrectRegion.generated.h already included, missing '#pragma once' in ColorCorrectRegion.h"
#endif
#define COLORCORRECTREGIONS_ColorCorrectRegion_generated_h

#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAColorCorrectRegion(); \
	friend struct Z_Construct_UClass_AColorCorrectRegion_Statics; \
public: \
	DECLARE_CLASS(AColorCorrectRegion, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ColorCorrectRegions"), NO_API) \
	DECLARE_SERIALIZER(AColorCorrectRegion)


#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_INCLASS \
private: \
	static void StaticRegisterNativesAColorCorrectRegion(); \
	friend struct Z_Construct_UClass_AColorCorrectRegion_Statics; \
public: \
	DECLARE_CLASS(AColorCorrectRegion, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ColorCorrectRegions"), NO_API) \
	DECLARE_SERIALIZER(AColorCorrectRegion)


#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AColorCorrectRegion(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AColorCorrectRegion) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AColorCorrectRegion); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AColorCorrectRegion); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AColorCorrectRegion(AColorCorrectRegion&&); \
	NO_API AColorCorrectRegion(const AColorCorrectRegion&); \
public:


#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AColorCorrectRegion(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AColorCorrectRegion(AColorCorrectRegion&&); \
	NO_API AColorCorrectRegion(const AColorCorrectRegion&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AColorCorrectRegion); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AColorCorrectRegion); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AColorCorrectRegion)


#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_42_PROLOG
#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_INCLASS \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h_45_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ColorCorrectRegion."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COLORCORRECTREGIONS_API UClass* StaticClass<class AColorCorrectRegion>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ColorCorrectRegions_Source_ColorCorrectRegions_Public_ColorCorrectRegion_h


#define FOREACH_ENUM_ECOLORCORRECTREGIONTEMPERATURETYPE(op) \
	op(EColorCorrectRegionTemperatureType::LegacyTemperature) \
	op(EColorCorrectRegionTemperatureType::WhiteBalance) \
	op(EColorCorrectRegionTemperatureType::ColorTemperature) 

enum class EColorCorrectRegionTemperatureType : uint8;
template<> COLORCORRECTREGIONS_API UEnum* StaticEnum<EColorCorrectRegionTemperatureType>();

#define FOREACH_ENUM_ECOLORCORRECTREGIONSTYPE(op) \
	op(EColorCorrectRegionsType::Sphere) \
	op(EColorCorrectRegionsType::Box) \
	op(EColorCorrectRegionsType::Cylinder) \
	op(EColorCorrectRegionsType::Cone) 

enum class EColorCorrectRegionsType : uint8;
template<> COLORCORRECTREGIONS_API UEnum* StaticEnum<EColorCorrectRegionsType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
