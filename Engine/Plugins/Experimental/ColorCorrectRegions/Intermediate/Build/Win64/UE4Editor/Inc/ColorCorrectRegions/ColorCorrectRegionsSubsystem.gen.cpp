// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ColorCorrectRegions/Public/ColorCorrectRegionsSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeColorCorrectRegionsSubsystem() {}
// Cross Module References
	COLORCORRECTREGIONS_API UClass* Z_Construct_UClass_UColorCorrectRegionsSubsystem_NoRegister();
	COLORCORRECTREGIONS_API UClass* Z_Construct_UClass_UColorCorrectRegionsSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UWorldSubsystem();
	UPackage* Z_Construct_UPackage__Script_ColorCorrectRegions();
// End Cross Module References
	void UColorCorrectRegionsSubsystem::StaticRegisterNativesUColorCorrectRegionsSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UColorCorrectRegionsSubsystem_NoRegister()
	{
		return UColorCorrectRegionsSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWorldSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_ColorCorrectRegions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * World Subsystem responsible for managing AColorCorrectRegion classes in level.\n * This subsystem handles:\n *\x09\x09Level Loaded, Undo/Redo, Added to level, Removed from level events.\n * Unfortunately AActor class itself is not aware of when it is added/removed, Undo/Redo etc in the level.\n * \n * This is the only way (that we found) that was handling all region aggregation cases in more or less efficient way.\n *\x09\x09""Covered cases: Region added to a level, deleted from level, level loaded, undo, redo, level closed, editor closed:\n *\x09\x09World subsystem keeps track of all Regions in a level via three events OnLevelActorAdded, OnLevelActorDeleted, OnLevelActorListChanged.\n *\x09\x09""Actor classes are unaware of when they are added/deleted/undo/redo etc in the level, therefore this is the best place to manage this.\n * Alternative strategies (All tested):\n *\x09\x09World's AddOnActorSpawnedHandler. Flawed. Invoked in some cases we don't need, but does not get called during UNDO/REDO\n *\x09\x09""AActor's PostSpawnInitialize, PostActorCreated  and OnConstruction are also flawed.\n *\x09\x09""AActor does not have an internal event for when its deleted (EndPlay is the closest we have).\n */" },
		{ "IncludePath", "ColorCorrectRegionsSubsystem.h" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegionsSubsystem.h" },
		{ "ToolTip", "World Subsystem responsible for managing AColorCorrectRegion classes in level.\nThis subsystem handles:\n            Level Loaded, Undo/Redo, Added to level, Removed from level events.\nUnfortunately AActor class itself is not aware of when it is added/removed, Undo/Redo etc in the level.\n\nThis is the only way (that we found) that was handling all region aggregation cases in more or less efficient way.\n            Covered cases: Region added to a level, deleted from level, level loaded, undo, redo, level closed, editor closed:\n            World subsystem keeps track of all Regions in a level via three events OnLevelActorAdded, OnLevelActorDeleted, OnLevelActorListChanged.\n            Actor classes are unaware of when they are added/deleted/undo/redo etc in the level, therefore this is the best place to manage this.\nAlternative strategies (All tested):\n            World's AddOnActorSpawnedHandler. Flawed. Invoked in some cases we don't need, but does not get called during UNDO/REDO\n            AActor's PostSpawnInitialize, PostActorCreated  and OnConstruction are also flawed.\n            AActor does not have an internal event for when its deleted (EndPlay is the closest we have)." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UColorCorrectRegionsSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::ClassParams = {
		&UColorCorrectRegionsSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UColorCorrectRegionsSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UColorCorrectRegionsSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UColorCorrectRegionsSubsystem, 2736902243);
	template<> COLORCORRECTREGIONS_API UClass* StaticClass<UColorCorrectRegionsSubsystem>()
	{
		return UColorCorrectRegionsSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UColorCorrectRegionsSubsystem(Z_Construct_UClass_UColorCorrectRegionsSubsystem, &UColorCorrectRegionsSubsystem::StaticClass, TEXT("/Script/ColorCorrectRegions"), TEXT("UColorCorrectRegionsSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UColorCorrectRegionsSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
