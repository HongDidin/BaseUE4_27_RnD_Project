// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ColorCorrectRegions/Public/ColorCorrectRegion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeColorCorrectRegion() {}
// Cross Module References
	COLORCORRECTREGIONS_API UEnum* Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionTemperatureType();
	UPackage* Z_Construct_UPackage__Script_ColorCorrectRegions();
	COLORCORRECTREGIONS_API UEnum* Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionsType();
	COLORCORRECTREGIONS_API UClass* Z_Construct_UClass_AColorCorrectRegion_NoRegister();
	COLORCORRECTREGIONS_API UClass* Z_Construct_UClass_AColorCorrectRegion();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FColorGradingSettings();
// End Cross Module References
	static UEnum* EColorCorrectRegionTemperatureType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionTemperatureType, Z_Construct_UPackage__Script_ColorCorrectRegions(), TEXT("EColorCorrectRegionTemperatureType"));
		}
		return Singleton;
	}
	template<> COLORCORRECTREGIONS_API UEnum* StaticEnum<EColorCorrectRegionTemperatureType>()
	{
		return EColorCorrectRegionTemperatureType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EColorCorrectRegionTemperatureType(EColorCorrectRegionTemperatureType_StaticEnum, TEXT("/Script/ColorCorrectRegions"), TEXT("EColorCorrectRegionTemperatureType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionTemperatureType_Hash() { return 4201037796U; }
	UEnum* Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionTemperatureType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ColorCorrectRegions();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EColorCorrectRegionTemperatureType"), 0, Get_Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionTemperatureType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EColorCorrectRegionTemperatureType::LegacyTemperature", (int64)EColorCorrectRegionTemperatureType::LegacyTemperature },
				{ "EColorCorrectRegionTemperatureType::WhiteBalance", (int64)EColorCorrectRegionTemperatureType::WhiteBalance },
				{ "EColorCorrectRegionTemperatureType::ColorTemperature", (int64)EColorCorrectRegionTemperatureType::ColorTemperature },
				{ "EColorCorrectRegionTemperatureType::MAX", (int64)EColorCorrectRegionTemperatureType::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ColorTemperature.DisplayName", "Color Temperature" },
				{ "ColorTemperature.Name", "EColorCorrectRegionTemperatureType::ColorTemperature" },
				{ "LegacyTemperature.DisplayName", "Temperature (Legacy)" },
				{ "LegacyTemperature.Name", "EColorCorrectRegionTemperatureType::LegacyTemperature" },
				{ "MAX.Name", "EColorCorrectRegionTemperatureType::MAX" },
				{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
				{ "WhiteBalance.DisplayName", "White Balance" },
				{ "WhiteBalance.Name", "EColorCorrectRegionTemperatureType::WhiteBalance" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ColorCorrectRegions,
				nullptr,
				"EColorCorrectRegionTemperatureType",
				"EColorCorrectRegionTemperatureType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EColorCorrectRegionsType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionsType, Z_Construct_UPackage__Script_ColorCorrectRegions(), TEXT("EColorCorrectRegionsType"));
		}
		return Singleton;
	}
	template<> COLORCORRECTREGIONS_API UEnum* StaticEnum<EColorCorrectRegionsType>()
	{
		return EColorCorrectRegionsType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EColorCorrectRegionsType(EColorCorrectRegionsType_StaticEnum, TEXT("/Script/ColorCorrectRegions"), TEXT("EColorCorrectRegionsType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionsType_Hash() { return 3295725506U; }
	UEnum* Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionsType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ColorCorrectRegions();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EColorCorrectRegionsType"), 0, Get_Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionsType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EColorCorrectRegionsType::Sphere", (int64)EColorCorrectRegionsType::Sphere },
				{ "EColorCorrectRegionsType::Box", (int64)EColorCorrectRegionsType::Box },
				{ "EColorCorrectRegionsType::Cylinder", (int64)EColorCorrectRegionsType::Cylinder },
				{ "EColorCorrectRegionsType::Cone", (int64)EColorCorrectRegionsType::Cone },
				{ "EColorCorrectRegionsType::MAX", (int64)EColorCorrectRegionsType::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Box.DisplayName", "Box" },
				{ "Box.Name", "EColorCorrectRegionsType::Box" },
				{ "Cone.DisplayName", "Cone" },
				{ "Cone.Name", "EColorCorrectRegionsType::Cone" },
				{ "Cylinder.DisplayName", "Cylinder" },
				{ "Cylinder.Name", "EColorCorrectRegionsType::Cylinder" },
				{ "MAX.Name", "EColorCorrectRegionsType::MAX" },
				{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
				{ "Sphere.DisplayName", "Sphere" },
				{ "Sphere.Name", "EColorCorrectRegionsType::Sphere" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ColorCorrectRegions,
				nullptr,
				"EColorCorrectRegionsType",
				"EColorCorrectRegionsType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AColorCorrectRegion::StaticRegisterNativesAColorCorrectRegion()
	{
	}
	UClass* Z_Construct_UClass_AColorCorrectRegion_NoRegister()
	{
		return AColorCorrectRegion::StaticClass();
	}
	struct Z_Construct_UClass_AColorCorrectRegion_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Priority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Priority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Intensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Intensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Outer_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Outer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Invert_MetaData[];
#endif
		static void NewProp_Invert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Invert;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TemperatureType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TemperatureType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TemperatureType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Temperature_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Temperature;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorGradingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorGradingSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enabled_MetaData[];
#endif
		static void NewProp_Enabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Enabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludeStencil_MetaData[];
#endif
		static void NewProp_ExcludeStencil_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ExcludeStencil;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AColorCorrectRegion_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ColorCorrectRegions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * An instance of Color Correction Region. Used to aggregate all active regions.\n * This actor is aggregated by ColorCorrectRegionsSubsystem which handles:\n *   - Level Loaded, Undo/Redo, Added to level, Removed from level events. \n * AActor class itself is not aware of when it is added/removed, Undo/Redo etc in the Editor. \n * AColorCorrectRegion reaches out to UColorCorrectRegionsSubsystem when its priority is changed, requesting regions to be sorted \n * or during BeginPlay/EndPlay to register itself. \n * More information in ColorCorrectRegionsSubsytem.h\n */" },
		{ "IncludePath", "ColorCorrectRegion.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "An instance of Color Correction Region. Used to aggregate all active regions.\nThis actor is aggregated by ColorCorrectRegionsSubsystem which handles:\n  - Level Loaded, Undo/Redo, Added to level, Removed from level events.\nAActor class itself is not aware of when it is added/removed, Undo/Redo etc in the Editor.\nAColorCorrectRegion reaches out to UColorCorrectRegionsSubsystem when its priority is changed, requesting regions to be sorted\nor during BeginPlay/EndPlay to register itself.\nMore information in ColorCorrectRegionsSubsytem.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Region type. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Region type." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Type), Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionsType, METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Priority_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Render priority/order. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Render priority/order." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Priority), METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Priority_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Priority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Intensity_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Color correction intensity. Clamped to 0-1 range. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Color correction intensity. Clamped to 0-1 range." },
		{ "UIMax", "1.000000" },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Intensity = { "Intensity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Intensity), METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Intensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Intensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Inner_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Inner of the region. Swapped with Outer in case it is higher than Outer. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Inner of the region. Swapped with Outer in case it is higher than Outer." },
		{ "UIMax", "1.000000" },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Inner = { "Inner", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Inner), METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Outer_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Outer of the region. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Outer of the region." },
		{ "UIMax", "1.000000" },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Outer = { "Outer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Outer), METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Outer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Outer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Falloff. Softening the region. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Falloff. Softening the region." },
		{ "UIMin", "0.000000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Falloff), METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Invert region. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Invert region." },
	};
#endif
	void Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert_SetBit(void* Obj)
	{
		((AColorCorrectRegion*)Obj)->Invert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert = { "Invert", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AColorCorrectRegion), &Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert_SetBit, METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Type of algorithm to be used to control color temperature or white balance. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Type of algorithm to be used to control color temperature or white balance." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType = { "TemperatureType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, TemperatureType), Z_Construct_UEnum_ColorCorrectRegions_EColorCorrectRegionTemperatureType, METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Temperature_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Color correction temperature. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Color correction temperature." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Temperature = { "Temperature", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, Temperature), METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Temperature_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Temperature_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ColorGradingSettings_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Color correction settings. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Color correction settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ColorGradingSettings = { "ColorGradingSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AColorCorrectRegion, ColorGradingSettings), Z_Construct_UScriptStruct_FColorGradingSettings, METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ColorGradingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ColorGradingSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Enable/Disable color correction provided by this region. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Enable/Disable color correction provided by this region." },
	};
#endif
	void Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled_SetBit(void* Obj)
	{
		((AColorCorrectRegion*)Obj)->Enabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled = { "Enabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AColorCorrectRegion), &Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil_MetaData[] = {
		{ "Category", "Color Correction" },
		{ "Comment", "/** Enable stenciling. */" },
		{ "ModuleRelativePath", "Public/ColorCorrectRegion.h" },
		{ "ToolTip", "Enable stenciling." },
	};
#endif
	void Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil_SetBit(void* Obj)
	{
		((AColorCorrectRegion*)Obj)->ExcludeStencil = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil = { "ExcludeStencil", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AColorCorrectRegion), &Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil_SetBit, METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AColorCorrectRegion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Priority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Intensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Outer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Invert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_TemperatureType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Temperature,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ColorGradingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_Enabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AColorCorrectRegion_Statics::NewProp_ExcludeStencil,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AColorCorrectRegion_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AColorCorrectRegion>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AColorCorrectRegion_Statics::ClassParams = {
		&AColorCorrectRegion::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AColorCorrectRegion_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AColorCorrectRegion_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AColorCorrectRegion_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AColorCorrectRegion()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AColorCorrectRegion_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AColorCorrectRegion, 511007182);
	template<> COLORCORRECTREGIONS_API UClass* StaticClass<AColorCorrectRegion>()
	{
		return AColorCorrectRegion::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AColorCorrectRegion(Z_Construct_UClass_AColorCorrectRegion, &AColorCorrectRegion::StaticClass, TEXT("/Script/ColorCorrectRegions"), TEXT("AColorCorrectRegion"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AColorCorrectRegion);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
