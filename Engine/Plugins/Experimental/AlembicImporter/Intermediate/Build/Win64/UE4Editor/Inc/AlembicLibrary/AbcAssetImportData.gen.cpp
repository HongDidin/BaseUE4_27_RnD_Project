// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AlembicLibrary/Public/AbcAssetImportData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbcAssetImportData() {}
// Cross Module References
	ALEMBICLIBRARY_API UClass* Z_Construct_UClass_UAbcAssetImportData_NoRegister();
	ALEMBICLIBRARY_API UClass* Z_Construct_UClass_UAbcAssetImportData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData();
	UPackage* Z_Construct_UPackage__Script_AlembicLibrary();
	ALEMBICLIBRARY_API UScriptStruct* Z_Construct_UScriptStruct_FAbcSamplingSettings();
// End Cross Module References
	void UAbcAssetImportData::StaticRegisterNativesUAbcAssetImportData()
	{
	}
	UClass* Z_Construct_UClass_UAbcAssetImportData_NoRegister()
	{
		return UAbcAssetImportData::StaticClass();
	}
	struct Z_Construct_UClass_UAbcAssetImportData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TrackNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TrackNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SamplingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SamplingSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbcAssetImportData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetImportData,
		(UObject* (*)())Z_Construct_UPackage__Script_AlembicLibrary,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbcAssetImportData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Base class for import data and options used when importing any asset from Alembic\n*/" },
		{ "IncludePath", "AbcAssetImportData.h" },
		{ "ModuleRelativePath", "Public/AbcAssetImportData.h" },
		{ "ToolTip", "Base class for import data and options used when importing any asset from Alembic" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames_Inner = { "TrackNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/AbcAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames = { "TrackNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAbcAssetImportData, TrackNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_SamplingSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/AbcAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_SamplingSettings = { "SamplingSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAbcAssetImportData, SamplingSettings), Z_Construct_UScriptStruct_FAbcSamplingSettings, METADATA_PARAMS(Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_SamplingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_SamplingSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAbcAssetImportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_TrackNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbcAssetImportData_Statics::NewProp_SamplingSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbcAssetImportData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbcAssetImportData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAbcAssetImportData_Statics::ClassParams = {
		&UAbcAssetImportData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAbcAssetImportData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAbcAssetImportData_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAbcAssetImportData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbcAssetImportData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbcAssetImportData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAbcAssetImportData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAbcAssetImportData, 3884161698);
	template<> ALEMBICLIBRARY_API UClass* StaticClass<UAbcAssetImportData>()
	{
		return UAbcAssetImportData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAbcAssetImportData(Z_Construct_UClass_UAbcAssetImportData, &UAbcAssetImportData::StaticClass, TEXT("/Script/AlembicLibrary"), TEXT("UAbcAssetImportData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbcAssetImportData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
