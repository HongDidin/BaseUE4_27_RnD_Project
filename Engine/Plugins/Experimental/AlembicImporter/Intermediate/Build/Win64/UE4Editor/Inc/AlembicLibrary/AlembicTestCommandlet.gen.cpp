// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AlembicLibrary/Private/AlembicTestCommandlet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAlembicTestCommandlet() {}
// Cross Module References
	ALEMBICLIBRARY_API UClass* Z_Construct_UClass_UAlembicTestCommandlet_NoRegister();
	ALEMBICLIBRARY_API UClass* Z_Construct_UClass_UAlembicTestCommandlet();
	ENGINE_API UClass* Z_Construct_UClass_UCommandlet();
	UPackage* Z_Construct_UPackage__Script_AlembicLibrary();
// End Cross Module References
	void UAlembicTestCommandlet::StaticRegisterNativesUAlembicTestCommandlet()
	{
	}
	UClass* Z_Construct_UClass_UAlembicTestCommandlet_NoRegister()
	{
		return UAlembicTestCommandlet::StaticClass();
	}
	struct Z_Construct_UClass_UAlembicTestCommandlet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAlembicTestCommandlet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommandlet,
		(UObject* (*)())Z_Construct_UPackage__Script_AlembicLibrary,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlembicTestCommandlet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AlembicTestCommandlet.h" },
		{ "ModuleRelativePath", "Private/AlembicTestCommandlet.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAlembicTestCommandlet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAlembicTestCommandlet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAlembicTestCommandlet_Statics::ClassParams = {
		&UAlembicTestCommandlet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAlembicTestCommandlet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAlembicTestCommandlet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAlembicTestCommandlet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAlembicTestCommandlet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAlembicTestCommandlet, 3080920433);
	template<> ALEMBICLIBRARY_API UClass* StaticClass<UAlembicTestCommandlet>()
	{
		return UAlembicTestCommandlet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAlembicTestCommandlet(Z_Construct_UClass_UAlembicTestCommandlet, &UAlembicTestCommandlet::StaticClass, TEXT("/Script/AlembicLibrary"), TEXT("UAlembicTestCommandlet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAlembicTestCommandlet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
