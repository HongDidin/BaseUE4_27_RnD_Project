// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AlembicImporter/Classes/AlembicImportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAlembicImportFactory() {}
// Cross Module References
	ALEMBICIMPORTER_API UClass* Z_Construct_UClass_UAlembicImportFactory_NoRegister();
	ALEMBICIMPORTER_API UClass* Z_Construct_UClass_UAlembicImportFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AlembicImporter();
	ALEMBICLIBRARY_API UClass* Z_Construct_UClass_UAbcImportSettings_NoRegister();
// End Cross Module References
	void UAlembicImportFactory::StaticRegisterNativesUAlembicImportFactory()
	{
	}
	UClass* Z_Construct_UClass_UAlembicImportFactory_NoRegister()
	{
		return UAlembicImportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAlembicImportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowOption_MetaData[];
#endif
		static void NewProp_bShowOption_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowOption;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAlembicImportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AlembicImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlembicImportFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "AlembicImportFactory.h" },
		{ "ModuleRelativePath", "Classes/AlembicImportFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_ImportSettings_MetaData[] = {
		{ "Comment", "/** Object used to show import options for Alembic */" },
		{ "ModuleRelativePath", "Classes/AlembicImportFactory.h" },
		{ "ToolTip", "Object used to show import options for Alembic" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_ImportSettings = { "ImportSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlembicImportFactory, ImportSettings), Z_Construct_UClass_UAbcImportSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_ImportSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_ImportSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption_MetaData[] = {
		{ "ModuleRelativePath", "Classes/AlembicImportFactory.h" },
	};
#endif
	void Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption_SetBit(void* Obj)
	{
		((UAlembicImportFactory*)Obj)->bShowOption = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption = { "bShowOption", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAlembicImportFactory), &Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAlembicImportFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_ImportSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlembicImportFactory_Statics::NewProp_bShowOption,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAlembicImportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAlembicImportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAlembicImportFactory_Statics::ClassParams = {
		&UAlembicImportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAlembicImportFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAlembicImportFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAlembicImportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAlembicImportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAlembicImportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAlembicImportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAlembicImportFactory, 4272430075);
	template<> ALEMBICIMPORTER_API UClass* StaticClass<UAlembicImportFactory>()
	{
		return UAlembicImportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAlembicImportFactory(Z_Construct_UClass_UAlembicImportFactory, &UAlembicImportFactory::StaticClass, TEXT("/Script/AlembicImporter"), TEXT("UAlembicImportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAlembicImportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
