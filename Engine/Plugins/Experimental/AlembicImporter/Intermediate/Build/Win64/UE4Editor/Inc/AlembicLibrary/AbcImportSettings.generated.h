// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ALEMBICLIBRARY_AbcImportSettings_generated_h
#error "AbcImportSettings.generated.h already included, missing '#pragma once' in AbcImportSettings.h"
#endif
#define ALEMBICLIBRARY_AbcImportSettings_generated_h

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_261_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcGeometryCacheSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcGeometryCacheSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_216_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcConversionSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcConversionSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_181_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcStaticMeshSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcStaticMeshSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_162_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcMaterialSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcMaterialSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_127_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcNormalGenerationSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcNormalGenerationSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcSamplingSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcSamplingSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbcCompressionSettings_Statics; \
	ALEMBICLIBRARY_API static class UScriptStruct* StaticStruct();


template<> ALEMBICLIBRARY_API UScriptStruct* StaticStruct<struct FAbcCompressionSettings>();

#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_SPARSE_DATA
#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_RPC_WRAPPERS
#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAbcImportSettings(); \
	friend struct Z_Construct_UClass_UAbcImportSettings_Statics; \
public: \
	DECLARE_CLASS(UAbcImportSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AlembicLibrary"), NO_API) \
	DECLARE_SERIALIZER(UAbcImportSettings)


#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_INCLASS \
private: \
	static void StaticRegisterNativesUAbcImportSettings(); \
	friend struct Z_Construct_UClass_UAbcImportSettings_Statics; \
public: \
	DECLARE_CLASS(UAbcImportSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AlembicLibrary"), NO_API) \
	DECLARE_SERIALIZER(UAbcImportSettings)


#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAbcImportSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAbcImportSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbcImportSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbcImportSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbcImportSettings(UAbcImportSettings&&); \
	NO_API UAbcImportSettings(const UAbcImportSettings&); \
public:


#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAbcImportSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbcImportSettings(UAbcImportSettings&&); \
	NO_API UAbcImportSettings(const UAbcImportSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbcImportSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbcImportSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAbcImportSettings)


#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_303_PROLOG
#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_SPARSE_DATA \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_RPC_WRAPPERS \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_INCLASS \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_SPARSE_DATA \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h_306_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AbcImportSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ALEMBICLIBRARY_API UClass* StaticClass<class UAbcImportSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_AlembicImporter_Source_AlembicLibrary_Public_AbcImportSettings_h


#define FOREACH_ENUM_EABCGEOMETRYCACHEMOTIONVECTORSIMPORT(op) \
	op(EAbcGeometryCacheMotionVectorsImport::NoMotionVectors) \
	op(EAbcGeometryCacheMotionVectorsImport::ImportAbcVelocitiesAsMotionVectors) \
	op(EAbcGeometryCacheMotionVectorsImport::CalculateMotionVectorsDuringImport) 

enum class EAbcGeometryCacheMotionVectorsImport : uint8;
template<> ALEMBICLIBRARY_API UEnum* StaticEnum<EAbcGeometryCacheMotionVectorsImport>();

#define FOREACH_ENUM_EABCCONVERSIONPRESET(op) \
	op(EAbcConversionPreset::Maya) \
	op(EAbcConversionPreset::Max) \
	op(EAbcConversionPreset::Custom) 

enum class EAbcConversionPreset : uint8;
template<> ALEMBICLIBRARY_API UEnum* StaticEnum<EAbcConversionPreset>();

#define FOREACH_ENUM_EALEMBICSAMPLINGTYPE(op) \
	op(EAlembicSamplingType::PerFrame) \
	op(EAlembicSamplingType::PerXFrames) \
	op(EAlembicSamplingType::PerTimeStep) 

enum class EAlembicSamplingType : uint8;
template<> ALEMBICLIBRARY_API UEnum* StaticEnum<EAlembicSamplingType>();

#define FOREACH_ENUM_EBASECALCULATIONTYPE(op) \
	op(EBaseCalculationType::None) \
	op(EBaseCalculationType::PercentageBased) \
	op(EBaseCalculationType::FixedNumber) \
	op(EBaseCalculationType::NoCompression) 

enum class EBaseCalculationType : uint8;
template<> ALEMBICLIBRARY_API UEnum* StaticEnum<EBaseCalculationType>();

#define FOREACH_ENUM_EALEMBICIMPORTTYPE(op) \
	op(EAlembicImportType::StaticMesh) \
	op(EAlembicImportType::GeometryCache) \
	op(EAlembicImportType::Skeletal) 

enum class EAlembicImportType : uint8;
template<> ALEMBICLIBRARY_API UEnum* StaticEnum<EAlembicImportType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
