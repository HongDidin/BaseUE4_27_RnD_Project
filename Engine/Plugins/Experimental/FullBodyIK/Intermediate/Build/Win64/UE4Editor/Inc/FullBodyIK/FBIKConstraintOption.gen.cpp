// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FullBodyIK/Public/FBIKConstraintOption.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFBIKConstraintOption() {}
// Cross Module References
	FULLBODYIK_API UEnum* Z_Construct_UEnum_FullBodyIK_EPoleVectorOption();
	UPackage* Z_Construct_UPackage__Script_FullBodyIK();
	FULLBODYIK_API UEnum* Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FMotionProcessInput();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FFBIKConstraintOption();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FFBIKBoneLimit();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
// End Cross Module References
	static UEnum* EPoleVectorOption_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FullBodyIK_EPoleVectorOption, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("EPoleVectorOption"));
		}
		return Singleton;
	}
	template<> FULLBODYIK_API UEnum* StaticEnum<EPoleVectorOption>()
	{
		return EPoleVectorOption_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPoleVectorOption(EPoleVectorOption_StaticEnum, TEXT("/Script/FullBodyIK"), TEXT("EPoleVectorOption"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FullBodyIK_EPoleVectorOption_Hash() { return 3171119099U; }
	UEnum* Z_Construct_UEnum_FullBodyIK_EPoleVectorOption()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPoleVectorOption"), 0, Get_Z_Construct_UEnum_FullBodyIK_EPoleVectorOption_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPoleVectorOption::Direction", (int64)EPoleVectorOption::Direction },
				{ "EPoleVectorOption::Location", (int64)EPoleVectorOption::Location },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Direction.Name", "EPoleVectorOption::Direction" },
				{ "Location.Comment", "/* The polevector will indicate a direction vector in their local bone space */" },
				{ "Location.Name", "EPoleVectorOption::Location" },
				{ "Location.ToolTip", "The polevector will indicate a direction vector in their local bone space" },
				{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FullBodyIK,
				nullptr,
				"EPoleVectorOption",
				"EPoleVectorOption",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EFBIKBoneLimitType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("EFBIKBoneLimitType"));
		}
		return Singleton;
	}
	template<> FULLBODYIK_API UEnum* StaticEnum<EFBIKBoneLimitType>()
	{
		return EFBIKBoneLimitType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFBIKBoneLimitType(EFBIKBoneLimitType_StaticEnum, TEXT("/Script/FullBodyIK"), TEXT("EFBIKBoneLimitType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType_Hash() { return 4107802275U; }
	UEnum* Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFBIKBoneLimitType"), 0, Get_Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFBIKBoneLimitType::Free", (int64)EFBIKBoneLimitType::Free },
				{ "EFBIKBoneLimitType::Limit", (int64)EFBIKBoneLimitType::Limit },
				{ "EFBIKBoneLimitType::Locked", (int64)EFBIKBoneLimitType::Locked },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Free.Name", "EFBIKBoneLimitType::Free" },
				{ "Limit.Name", "EFBIKBoneLimitType::Limit" },
				{ "Locked.Comment", "// currently hard limit\n" },
				{ "Locked.Name", "EFBIKBoneLimitType::Locked" },
				{ "Locked.ToolTip", "currently hard limit" },
				{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FullBodyIK,
				nullptr,
				"EFBIKBoneLimitType",
				"EFBIKBoneLimitType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMotionProcessInput::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FMotionProcessInput_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMotionProcessInput, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("MotionProcessInput"), sizeof(FMotionProcessInput), Get_Z_Construct_UScriptStruct_FMotionProcessInput_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FMotionProcessInput>()
{
	return FMotionProcessInput::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMotionProcessInput(FMotionProcessInput::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("MotionProcessInput"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFMotionProcessInput
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFMotionProcessInput()
	{
		UScriptStruct::DeferCppStructOps<FMotionProcessInput>(FName(TEXT("MotionProcessInput")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFMotionProcessInput;
	struct Z_Construct_UScriptStruct_FMotionProcessInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceEffectorRotationTarget_MetaData[];
#endif
		static void NewProp_bForceEffectorRotationTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceEffectorRotationTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyApplyWhenReachedToTarget_MetaData[];
#endif
		static void NewProp_bOnlyApplyWhenReachedToTarget_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyApplyWhenReachedToTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMotionProcessInput_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMotionProcessInput>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget_MetaData[] = {
		{ "Category", "FMotionProcessInput" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget_SetBit(void* Obj)
	{
		((FMotionProcessInput*)Obj)->bForceEffectorRotationTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget = { "bForceEffectorRotationTarget", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMotionProcessInput), &Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget_MetaData[] = {
		{ "Category", "FMotionProcessInput" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget_SetBit(void* Obj)
	{
		((FMotionProcessInput*)Obj)->bOnlyApplyWhenReachedToTarget = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget = { "bOnlyApplyWhenReachedToTarget", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMotionProcessInput), &Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMotionProcessInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bForceEffectorRotationTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMotionProcessInput_Statics::NewProp_bOnlyApplyWhenReachedToTarget,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMotionProcessInput_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"MotionProcessInput",
		sizeof(FMotionProcessInput),
		alignof(FMotionProcessInput),
		Z_Construct_UScriptStruct_FMotionProcessInput_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMotionProcessInput_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMotionProcessInput()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMotionProcessInput_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MotionProcessInput"), sizeof(FMotionProcessInput), Get_Z_Construct_UScriptStruct_FMotionProcessInput_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMotionProcessInput_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMotionProcessInput_Hash() { return 1971781748U; }
class UScriptStruct* FFBIKConstraintOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FFBIKConstraintOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFBIKConstraintOption, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("FBIKConstraintOption"), sizeof(FFBIKConstraintOption), Get_Z_Construct_UScriptStruct_FFBIKConstraintOption_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FFBIKConstraintOption>()
{
	return FFBIKConstraintOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFBIKConstraintOption(FFBIKConstraintOption::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("FBIKConstraintOption"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKConstraintOption
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKConstraintOption()
	{
		UScriptStruct::DeferCppStructOps<FFBIKConstraintOption>(FName(TEXT("FBIKConstraintOption")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKConstraintOption;
	struct Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseStiffness_MetaData[];
#endif
		static void NewProp_bUseStiffness_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseStiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearStiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearStiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularStiffness_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AngularStiffness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseAngularLimit_MetaData[];
#endif
		static void NewProp_bUseAngularLimit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseAngularLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AngularLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsePoleVector_MetaData[];
#endif
		static void NewProp_bUsePoleVector_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePoleVector;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PoleVectorOption_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVectorOption_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PoleVectorOption;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoleVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PoleVector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OffsetRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFBIKConstraintOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_Item_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "/** Bone Name */" },
		{ "Constant", "" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "Bone Name" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FFBIKConstraintOption*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKConstraintOption), &Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness_SetBit(void* Obj)
	{
		((FFBIKConstraintOption*)Obj)->bUseStiffness = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness = { "bUseStiffness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKConstraintOption), &Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_LinearStiffness_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "/** Scale of [0, 1] and applied to linear motion strength - linear stiffness works on their local frame*/" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "Scale of [0, 1] and applied to linear motion strength - linear stiffness works on their local frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_LinearStiffness = { "LinearStiffness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, LinearStiffness), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_LinearStiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_LinearStiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularStiffness_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "/** Scale of [0, 1] and applied to angular motion strength, xyz is applied to twist, swing1, swing2 */" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "Scale of [0, 1] and applied to angular motion strength, xyz is applied to twist, swing1, swing2" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularStiffness = { "AngularStiffness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, AngularStiffness), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularStiffness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularStiffness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit_SetBit(void* Obj)
	{
		((FFBIKConstraintOption*)Obj)->bUseAngularLimit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit = { "bUseAngularLimit", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKConstraintOption), &Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularLimit_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "/** Angular delta limit of this joints local transform. [-Limit, Limit] */" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "Angular delta limit of this joints local transform. [-Limit, Limit]" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularLimit = { "AngularLimit", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, AngularLimit), Z_Construct_UScriptStruct_FFBIKBoneLimit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularLimit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector_SetBit(void* Obj)
	{
		((FFBIKConstraintOption*)Obj)->bUsePoleVector = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector = { "bUsePoleVector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKConstraintOption), &Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption = { "PoleVectorOption", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, PoleVectorOption), Z_Construct_UEnum_FullBodyIK_EPoleVectorOption, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVector_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "/** Pole Vector in their local space*/" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "Pole Vector in their local space" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVector = { "PoleVector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, PoleVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_OffsetRotation_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "// this is offset rotation applied when constructing the local frame\n" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "this is offset rotation applied when constructing the local frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_OffsetRotation = { "OffsetRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKConstraintOption, OffsetRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_OffsetRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_OffsetRotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseStiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_LinearStiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularStiffness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUseAngularLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_AngularLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_bUsePoleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVectorOption,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_PoleVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::NewProp_OffsetRotation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"FBIKConstraintOption",
		sizeof(FFBIKConstraintOption),
		alignof(FFBIKConstraintOption),
		Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFBIKConstraintOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFBIKConstraintOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FBIKConstraintOption"), sizeof(FFBIKConstraintOption), Get_Z_Construct_UScriptStruct_FFBIKConstraintOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFBIKConstraintOption_Hash() { return 482799378U; }
class UScriptStruct* FFBIKBoneLimit::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FFBIKBoneLimit_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFBIKBoneLimit, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("FBIKBoneLimit"), sizeof(FFBIKBoneLimit), Get_Z_Construct_UScriptStruct_FFBIKBoneLimit_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FFBIKBoneLimit>()
{
	return FFBIKBoneLimit::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFBIKBoneLimit(FFBIKBoneLimit::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("FBIKBoneLimit"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKBoneLimit
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKBoneLimit()
	{
		UScriptStruct::DeferCppStructOps<FFBIKBoneLimit>(FName(TEXT("FBIKBoneLimit")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKBoneLimit;
	struct Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LimitType_X_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LimitType_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LimitType_X;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LimitType_Y_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LimitType_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LimitType_Y;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LimitType_Z_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LimitType_Z_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LimitType_Z;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Limit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Limit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFBIKBoneLimit>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X = { "LimitType_X", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKBoneLimit, LimitType_X), Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y = { "LimitType_Y", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKBoneLimit, LimitType_Y), Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z = { "LimitType_Z", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKBoneLimit, LimitType_Z), Z_Construct_UEnum_FullBodyIK_EFBIKBoneLimitType, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_Limit_MetaData[] = {
		{ "Category", "FFBIKConstraintOption" },
		{ "Comment", "// currently no negative limit, for position we may need min/max\n" },
		{ "ModuleRelativePath", "Public/FBIKConstraintOption.h" },
		{ "ToolTip", "currently no negative limit, for position we may need min/max" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_Limit = { "Limit", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKBoneLimit, Limit), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_Limit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_Limit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_LimitType_Z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::NewProp_Limit,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"FBIKBoneLimit",
		sizeof(FFBIKBoneLimit),
		alignof(FFBIKBoneLimit),
		Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFBIKBoneLimit()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFBIKBoneLimit_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FBIKBoneLimit"), sizeof(FFBIKBoneLimit), Get_Z_Construct_UScriptStruct_FFBIKBoneLimit_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFBIKBoneLimit_Hash() { return 1479168416U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
