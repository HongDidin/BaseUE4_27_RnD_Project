// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FULLBODYIK_RigUnit_FullbodyIK_generated_h
#error "RigUnit_FullbodyIK.generated.h already included, missing '#pragma once' in RigUnit_FullbodyIK.h"
#endif
#define FULLBODYIK_RigUnit_FullbodyIK_generated_h


#define FRigUnit_FullbodyIK_Execute() \
	void FRigUnit_FullbodyIK::StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Root, \
		const FRigVMFixedArray<FFBIKEndEffector>& Effectors, \
		const FRigVMFixedArray<FFBIKConstraintOption>& Constraints, \
		const FSolverInput& SolverProperty, \
		const FMotionProcessInput& MotionProperty, \
		const bool bPropagateToChildren, \
		const FFBIKDebugOption& DebugOption, \
		FRigUnit_FullbodyIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	)

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Private_RigUnit_FullbodyIK_h_173_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct(); \
	static void StaticExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		const FRigElementKey& Root, \
		const FRigVMFixedArray<FFBIKEndEffector>& Effectors, \
		const FRigVMFixedArray<FFBIKConstraintOption>& Constraints, \
		const FSolverInput& SolverProperty, \
		const FMotionProcessInput& MotionProperty, \
		const bool bPropagateToChildren, \
		const FFBIKDebugOption& DebugOption, \
		FRigUnit_FullbodyIK_WorkData& WorkData, \
		FControlRigExecuteContext& ExecuteContext, \
		const FRigUnitContext & Context \
	); \
	FORCEINLINE_DEBUGGABLE static void RigVMExecute( \
		FRigVMExecuteContext& RigVMExecuteContext, \
		FRigVMMemoryHandleArray RigVMMemoryHandles \
	) \
	{ \
		const FRigUnitContext & Context = *(const FRigUnitContext *)RigVMExecuteContext.OpaqueArguments[0]; \
		 \
		const FRigElementKey& Root = *(FRigElementKey*)RigVMMemoryHandles[0].GetData(); \
		FRigVMFixedArray<FFBIKEndEffector> Effectors((FFBIKEndEffector*)RigVMMemoryHandles[1].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[2].GetData())); \
		FRigVMFixedArray<FFBIKConstraintOption> Constraints((FFBIKConstraintOption*)RigVMMemoryHandles[3].GetData(), reinterpret_cast<uint64>(RigVMMemoryHandles[4].GetData())); \
		const FSolverInput& SolverProperty = *(FSolverInput*)RigVMMemoryHandles[5].GetData(); \
		const FMotionProcessInput& MotionProperty = *(FMotionProcessInput*)RigVMMemoryHandles[6].GetData(); \
		const bool bPropagateToChildren = *(bool*)RigVMMemoryHandles[7].GetData(); \
		const FFBIKDebugOption& DebugOption = *(FFBIKDebugOption*)RigVMMemoryHandles[8].GetData(); \
		FRigVMDynamicArray<FRigUnit_FullbodyIK_WorkData> WorkData_9_Array(*((FRigVMByteArray*)RigVMMemoryHandles[9].GetData(0, false))); \
		WorkData_9_Array.EnsureMinimumSize(RigVMExecuteContext.GetSlice().TotalNum()); \
		FRigUnit_FullbodyIK_WorkData& WorkData = WorkData_9_Array[RigVMExecuteContext.GetSlice().GetIndex()]; \
		FControlRigExecuteContext& ExecuteContext = *(FControlRigExecuteContext*)RigVMMemoryHandles[10].GetData(); \
		 \
		StaticExecute( \
			RigVMExecuteContext, \
			Root, \
			Effectors, \
			Constraints, \
			SolverProperty, \
			MotionProperty, \
			bPropagateToChildren, \
			DebugOption, \
			WorkData, \
			ExecuteContext, \
			Context \
		); \
	} \
	typedef FRigUnit_HighlevelBaseMutable Super;


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FRigUnit_FullbodyIK>();

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Private_RigUnit_FullbodyIK_h_113_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSolverInput_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct();


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FSolverInput>();

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Private_RigUnit_FullbodyIK_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct();


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FRigUnit_FullbodyIK_WorkData>();

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Private_RigUnit_FullbodyIK_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFBIKEndEffector_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct();


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FFBIKEndEffector>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Private_RigUnit_FullbodyIK_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
