// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FULLBODYIK_FBIKConstraintOption_generated_h
#error "FBIKConstraintOption.generated.h already included, missing '#pragma once' in FBIKConstraintOption.h"
#endif
#define FULLBODYIK_FBIKConstraintOption_generated_h

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Public_FBIKConstraintOption_h_126_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMotionProcessInput_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct();


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FMotionProcessInput>();

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Public_FBIKConstraintOption_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFBIKConstraintOption_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct();


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FFBIKConstraintOption>();

#define Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Public_FBIKConstraintOption_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFBIKBoneLimit_Statics; \
	FULLBODYIK_API static class UScriptStruct* StaticStruct();


template<> FULLBODYIK_API UScriptStruct* StaticStruct<struct FFBIKBoneLimit>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_FullBodyIK_Source_FullBodyIK_Public_FBIKConstraintOption_h


#define FOREACH_ENUM_EPOLEVECTOROPTION(op) \
	op(EPoleVectorOption::Direction) \
	op(EPoleVectorOption::Location) 

enum class EPoleVectorOption : uint8;
template<> FULLBODYIK_API UEnum* StaticEnum<EPoleVectorOption>();

#define FOREACH_ENUM_EFBIKBONELIMITTYPE(op) \
	op(EFBIKBoneLimitType::Free) \
	op(EFBIKBoneLimitType::Limit) \
	op(EFBIKBoneLimitType::Locked) 

enum class EFBIKBoneLimitType : uint8;
template<> FULLBODYIK_API UEnum* StaticEnum<EFBIKBoneLimitType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
