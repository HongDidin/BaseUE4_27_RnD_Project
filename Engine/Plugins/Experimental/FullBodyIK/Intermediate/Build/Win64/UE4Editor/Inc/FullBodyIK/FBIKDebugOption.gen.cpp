// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FullBodyIK/Public/FBIKDebugOption.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFBIKDebugOption() {}
// Cross Module References
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FFBIKDebugOption();
	UPackage* Z_Construct_UPackage__Script_FullBodyIK();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
class UScriptStruct* FFBIKDebugOption::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FFBIKDebugOption_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFBIKDebugOption, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("FBIKDebugOption"), sizeof(FFBIKDebugOption), Get_Z_Construct_UScriptStruct_FFBIKDebugOption_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FFBIKDebugOption>()
{
	return FFBIKDebugOption::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFBIKDebugOption(FFBIKDebugOption::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("FBIKDebugOption"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKDebugOption
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKDebugOption()
	{
		UScriptStruct::DeferCppStructOps<FFBIKDebugOption>(FName(TEXT("FBIKDebugOption")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKDebugOption;
	struct Z_Construct_UScriptStruct_FFBIKDebugOption_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugHierarchy_MetaData[];
#endif
		static void NewProp_bDrawDebugHierarchy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bColorAngularMotionStrength_MetaData[];
#endif
		static void NewProp_bColorAngularMotionStrength_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bColorAngularMotionStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bColorLinearMotionStrength_MetaData[];
#endif
		static void NewProp_bColorLinearMotionStrength_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bColorLinearMotionStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugAxes_MetaData[];
#endif
		static void NewProp_bDrawDebugAxes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugAxes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugEffector_MetaData[];
#endif
		static void NewProp_bDrawDebugEffector_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugEffector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugConstraints_MetaData[];
#endif
		static void NewProp_bDrawDebugConstraints_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugConstraints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawWorldOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DrawWorldOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DrawSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFBIKDebugOption>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy_SetBit(void* Obj)
	{
		((FFBIKDebugOption*)Obj)->bDrawDebugHierarchy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy = { "bDrawDebugHierarchy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKDebugOption), &Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength_MetaData[] = {
		{ "Comment", "// use red channel\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
		{ "ToolTip", "use red channel" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength_SetBit(void* Obj)
	{
		((FFBIKDebugOption*)Obj)->bColorAngularMotionStrength = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength = { "bColorAngularMotionStrength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKDebugOption), &Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength_MetaData[] = {
		{ "Comment", "// use green channel\n" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
		{ "ToolTip", "use green channel" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength_SetBit(void* Obj)
	{
		((FFBIKDebugOption*)Obj)->bColorLinearMotionStrength = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength = { "bColorLinearMotionStrength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKDebugOption), &Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes_SetBit(void* Obj)
	{
		((FFBIKDebugOption*)Obj)->bDrawDebugAxes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes = { "bDrawDebugAxes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKDebugOption), &Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector_SetBit(void* Obj)
	{
		((FFBIKDebugOption*)Obj)->bDrawDebugEffector = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector = { "bDrawDebugEffector", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKDebugOption), &Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints_SetBit(void* Obj)
	{
		((FFBIKDebugOption*)Obj)->bDrawDebugConstraints = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints = { "bDrawDebugConstraints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFBIKDebugOption), &Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawWorldOffset_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawWorldOffset = { "DrawWorldOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKDebugOption, DrawWorldOffset), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawWorldOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawWorldOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawSize_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Public/FBIKDebugOption.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawSize = { "DrawSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKDebugOption, DrawSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorAngularMotionStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bColorLinearMotionStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugAxes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugEffector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_bDrawDebugConstraints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawWorldOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::NewProp_DrawSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"FBIKDebugOption",
		sizeof(FFBIKDebugOption),
		alignof(FFBIKDebugOption),
		Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFBIKDebugOption()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFBIKDebugOption_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FBIKDebugOption"), sizeof(FFBIKDebugOption), Get_Z_Construct_UScriptStruct_FFBIKDebugOption_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFBIKDebugOption_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFBIKDebugOption_Hash() { return 185129845U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
