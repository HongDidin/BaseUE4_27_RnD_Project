// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FullBodyIK/Private/RigUnit_FullbodyIK.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigUnit_FullbodyIK() {}
// Cross Module References
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_FullbodyIK();
	UPackage* Z_Construct_UPackage__Script_FullBodyIK();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable();
	CONTROLRIG_API UScriptStruct* Z_Construct_UScriptStruct_FRigElementKey();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FFBIKEndEffector();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FFBIKConstraintOption();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FSolverInput();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FMotionProcessInput();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FFBIKDebugOption();
	FULLBODYIK_API UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
// End Cross Module References

static_assert(std::is_polymorphic<FRigUnit_FullbodyIK>() == std::is_polymorphic<FRigUnit_HighlevelBaseMutable>(), "USTRUCT FRigUnit_FullbodyIK cannot be polymorphic unless super FRigUnit_HighlevelBaseMutable is polymorphic");

class UScriptStruct* FRigUnit_FullbodyIK::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("RigUnit_FullbodyIK"), sizeof(FRigUnit_FullbodyIK), Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Hash());
		FRigVMRegistry::Get().Register(TEXT("FRigUnit_FullbodyIK::Execute"), &FRigUnit_FullbodyIK::RigVMExecute, Singleton);
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FRigUnit_FullbodyIK>()
{
	return FRigUnit_FullbodyIK::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_FullbodyIK(FRigUnit_FullbodyIK::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("RigUnit_FullbodyIK"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFRigUnit_FullbodyIK
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFRigUnit_FullbodyIK()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_FullbodyIK>(FName(TEXT("RigUnit_FullbodyIK")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFRigUnit_FullbodyIK;
	struct Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Root_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Root;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effectors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effectors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Effectors;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Constraints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Constraints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Constraints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SolverProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SolverProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MotionProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MotionProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPropagateToChildren_MetaData[];
#endif
		static void NewProp_bPropagateToChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPropagateToChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugOption_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugOption;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::Struct_MetaDataParams[] = {
		{ "Category", "Hierarchy" },
		{ "Comment", "/**\n * Based on Jacobian solver at core, this can solve multi chains within a root using multi effectors\n */" },
		{ "DisplayName", "Fullbody IK" },
		{ "Keywords", "Multi, Effector, N-Chain, FB, IK" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "Based on Jacobian solver at core, this can solve multi chains within a root using multi effectors" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_FullbodyIK>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Root_MetaData[] = {
		{ "Comment", "/**\n\x09 * The first bone in the chain to solve\n\x09 */" },
		{ "Constant", "" },
		{ "CustomWidget", "BoneName" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "The first bone in the chain to solve" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Root = { "Root", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, Root), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Root_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Root_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors_Inner = { "Effectors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFBIKEndEffector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors = { "Effectors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, Effectors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints_Inner = { "Constraints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFBIKConstraintOption, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints_MetaData[] = {
		{ "Category", "FRigUnit_Jacobian" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints = { "Constraints", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, Constraints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_SolverProperty_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_SolverProperty = { "SolverProperty", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, SolverProperty), Z_Construct_UScriptStruct_FSolverInput, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_SolverProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_SolverProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_MotionProperty_MetaData[] = {
		{ "Constant", "" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_MotionProperty = { "MotionProperty", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, MotionProperty), Z_Construct_UScriptStruct_FMotionProcessInput, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_MotionProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_MotionProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren_MetaData[] = {
		{ "Comment", "/**\n\x09 * If set to true all of the global transforms of the children\n\x09 * of this bone will be recalculated based on their local transforms.\n\x09 * Note: This is computationally more expensive than turning it off.\n\x09 */" },
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "If set to true all of the global transforms of the children\nof this bone will be recalculated based on their local transforms.\nNote: This is computationally more expensive than turning it off." },
	};
#endif
	void Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren_SetBit(void* Obj)
	{
		((FRigUnit_FullbodyIK*)Obj)->bPropagateToChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren = { "bPropagateToChildren", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRigUnit_FullbodyIK), &Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_DebugOption_MetaData[] = {
		{ "Input", "" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_DebugOption = { "DebugOption", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, DebugOption), Z_Construct_UScriptStruct_FFBIKDebugOption, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_DebugOption_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_DebugOption_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_WorkData_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_WorkData = { "WorkData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRigUnit_FullbodyIK, WorkData), Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData, METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_WorkData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_WorkData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Root,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Effectors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_Constraints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_SolverProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_MotionProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_bPropagateToChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_DebugOption,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::NewProp_WorkData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		Z_Construct_UScriptStruct_FRigUnit_HighlevelBaseMutable,
		&NewStructOps,
		"RigUnit_FullbodyIK",
		sizeof(FRigUnit_FullbodyIK),
		alignof(FRigUnit_FullbodyIK),
		Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_FullbodyIK()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_FullbodyIK"), sizeof(FRigUnit_FullbodyIK), Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_Hash() { return 631560205U; }

void FRigUnit_FullbodyIK::Execute(const FRigUnitContext & Context)
{
	FRigVMExecuteContext RigVMExecuteContext;
	FRigVMFixedArray<FFBIKEndEffector> Effectors_1_Array(Effectors);
	FRigVMFixedArray<FFBIKConstraintOption> Constraints_2_Array(Constraints);
	
    StaticExecute(
		RigVMExecuteContext,
		Root,
		Effectors_1_Array,
		Constraints_2_Array,
		SolverProperty,
		MotionProperty,
		bPropagateToChildren,
		DebugOption,
		WorkData,
		ExecuteContext,
		Context
	);
}

class UScriptStruct* FSolverInput::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FSolverInput_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSolverInput, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("SolverInput"), sizeof(FSolverInput), Get_Z_Construct_UScriptStruct_FSolverInput_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FSolverInput>()
{
	return FSolverInput::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSolverInput(FSolverInput::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("SolverInput"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFSolverInput
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFSolverInput()
	{
		UScriptStruct::DeferCppStructOps<FSolverInput>(FName(TEXT("SolverInput")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFSolverInput;
	struct Z_Construct_UScriptStruct_FSolverInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearMotionStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LinearMotionStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinLinearMotionStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinLinearMotionStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularMotionStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngularMotionStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinAngularMotionStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinAngularMotionStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTargetClamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultTargetClamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Precision_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Precision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damping_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Damping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxIterations_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxIterations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseJacobianTranspose_MetaData[];
#endif
		static void NewProp_bUseJacobianTranspose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseJacobianTranspose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSolverInput_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSolverInput>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_LinearMotionStrength_MetaData[] = {
		{ "Comment", "/*\n\x09 * This value is applied to the target information for effectors, which influence back to \n\x09 * Joint's motion that are affected by the end effector\n\x09 * The reason min/max is used when we apply the depth through the chain that are affected\n\n\x09 */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "* This value is applied to the target information for effectors, which influence back to\n* Joint's motion that are affected by the end effector\n* The reason min/max is used when we apply the depth through the chain that are affected" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_LinearMotionStrength = { "LinearMotionStrength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, LinearMotionStrength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_LinearMotionStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_LinearMotionStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinLinearMotionStrength_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinLinearMotionStrength = { "MinLinearMotionStrength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, MinLinearMotionStrength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinLinearMotionStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinLinearMotionStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_AngularMotionStrength_MetaData[] = {
		{ "Comment", "/*\n\x09 * This value is applied to the target information for effectors, which influence back to \n\x09 * Joint's motion that are affected by the end effector\n\x09 * The reason min/max is used when we apply the depth through the chain that are affected\n\x09 */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "* This value is applied to the target information for effectors, which influence back to\n* Joint's motion that are affected by the end effector\n* The reason min/max is used when we apply the depth through the chain that are affected" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_AngularMotionStrength = { "AngularMotionStrength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, AngularMotionStrength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_AngularMotionStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_AngularMotionStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinAngularMotionStrength_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinAngularMotionStrength = { "MinAngularMotionStrength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, MinAngularMotionStrength), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinAngularMotionStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinAngularMotionStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_DefaultTargetClamp_MetaData[] = {
		{ "Comment", "/* This is a scale value (range from 0-0.7) that is used to stablize the target vector. If less, it's more stable, but it can reduce speed of converge. */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "This is a scale value (range from 0-0.7) that is used to stablize the target vector. If less, it's more stable, but it can reduce speed of converge." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_DefaultTargetClamp = { "DefaultTargetClamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, DefaultTargetClamp), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_DefaultTargetClamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_DefaultTargetClamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Precision_MetaData[] = {
		{ "Comment", "/**\n\x09 * The precision to use for the solver\n\x09 */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "The precision to use for the solver" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Precision = { "Precision", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, Precision), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Precision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Precision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Damping_MetaData[] = {
		{ "Comment", "/**\n\x09* The precision to use for the fabrik solver\n\x09*/" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "The precision to use for the fabrik solver" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Damping = { "Damping", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, Damping), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Damping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Damping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MaxIterations_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum number of iterations. Values between 4 and 16 are common.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "The maximum number of iterations. Values between 4 and 16 are common." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MaxIterations = { "MaxIterations", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSolverInput, MaxIterations), METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MaxIterations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MaxIterations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose_MetaData[] = {
		{ "Comment", "/**\n\x09 * Cheaper solution than default Jacobian Pseudo Inverse Damped Least Square\n\x09 */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "Cheaper solution than default Jacobian Pseudo Inverse Damped Least Square" },
	};
#endif
	void Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose_SetBit(void* Obj)
	{
		((FSolverInput*)Obj)->bUseJacobianTranspose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose = { "bUseJacobianTranspose", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSolverInput), &Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSolverInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_LinearMotionStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinLinearMotionStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_AngularMotionStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MinAngularMotionStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_DefaultTargetClamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Precision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_Damping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_MaxIterations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSolverInput_Statics::NewProp_bUseJacobianTranspose,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSolverInput_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"SolverInput",
		sizeof(FSolverInput),
		alignof(FSolverInput),
		Z_Construct_UScriptStruct_FSolverInput_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSolverInput_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSolverInput_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSolverInput()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSolverInput_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SolverInput"), sizeof(FSolverInput), Get_Z_Construct_UScriptStruct_FSolverInput_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSolverInput_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSolverInput_Hash() { return 2434606794U; }
class UScriptStruct* FRigUnit_FullbodyIK_WorkData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("RigUnit_FullbodyIK_WorkData"), sizeof(FRigUnit_FullbodyIK_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FRigUnit_FullbodyIK_WorkData>()
{
	return FRigUnit_FullbodyIK_WorkData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRigUnit_FullbodyIK_WorkData(FRigUnit_FullbodyIK_WorkData::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("RigUnit_FullbodyIK_WorkData"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFRigUnit_FullbodyIK_WorkData
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFRigUnit_FullbodyIK_WorkData()
	{
		UScriptStruct::DeferCppStructOps<FRigUnit_FullbodyIK_WorkData>(FName(TEXT("RigUnit_FullbodyIK_WorkData")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFRigUnit_FullbodyIK_WorkData;
	struct Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRigUnit_FullbodyIK_WorkData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"RigUnit_FullbodyIK_WorkData",
		sizeof(FRigUnit_FullbodyIK_WorkData),
		alignof(FRigUnit_FullbodyIK_WorkData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RigUnit_FullbodyIK_WorkData"), sizeof(FRigUnit_FullbodyIK_WorkData), Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRigUnit_FullbodyIK_WorkData_Hash() { return 206879340U; }
class UScriptStruct* FFBIKEndEffector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern FULLBODYIK_API uint32 Get_Z_Construct_UScriptStruct_FFBIKEndEffector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFBIKEndEffector, Z_Construct_UPackage__Script_FullBodyIK(), TEXT("FBIKEndEffector"), sizeof(FFBIKEndEffector), Get_Z_Construct_UScriptStruct_FFBIKEndEffector_Hash());
	}
	return Singleton;
}
template<> FULLBODYIK_API UScriptStruct* StaticStruct<FFBIKEndEffector>()
{
	return FFBIKEndEffector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFBIKEndEffector(FFBIKEndEffector::StaticStruct, TEXT("/Script/FullBodyIK"), TEXT("FBIKEndEffector"), false, nullptr, nullptr);
static struct FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKEndEffector
{
	FScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKEndEffector()
	{
		UScriptStruct::DeferCppStructOps<FFBIKEndEffector>(FName(TEXT("FBIKEndEffector")));
	}
} ScriptStruct_FullBodyIK_StaticRegisterNativesFFBIKEndEffector;
	struct Z_Construct_UScriptStruct_FFBIKEndEffector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionAlpha_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PositionAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PositionDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationAlpha_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RotationDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pull_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Pull;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFBIKEndEffector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Item_MetaData[] = {
		{ "Comment", "/**\n\x09 * The last item in the chain to solve - the effector\n\x09 */" },
		{ "Constant", "" },
		{ "CustomWidget", "BoneName" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "The last item in the chain to solve - the effector" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, Item), Z_Construct_UScriptStruct_FRigElementKey, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Position_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionAlpha_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionAlpha = { "PositionAlpha", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, PositionAlpha), METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionDepth_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionDepth = { "PositionDepth", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, PositionDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Rotation_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationAlpha_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationAlpha = { "RotationAlpha", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, RotationAlpha), METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationDepth_MetaData[] = {
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationDepth = { "RotationDepth", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, RotationDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Pull_MetaData[] = {
		{ "Comment", "/*\n\x09 * Clamps the total length to target by this scale for each iteration \n\x09 * This helps to stabilize solver to reduce singularity by avoiding to try to reach target too far. \n\x09 */" },
		{ "ModuleRelativePath", "Private/RigUnit_FullbodyIK.h" },
		{ "ToolTip", "* Clamps the total length to target by this scale for each iteration\n* This helps to stabilize solver to reduce singularity by avoiding to try to reach target too far." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Pull = { "Pull", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFBIKEndEffector, Pull), METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Pull_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Pull_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Item,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_PositionDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_RotationDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::NewProp_Pull,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_FullBodyIK,
		nullptr,
		&NewStructOps,
		"FBIKEndEffector",
		sizeof(FFBIKEndEffector),
		alignof(FFBIKEndEffector),
		Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFBIKEndEffector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFBIKEndEffector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_FullBodyIK();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FBIKEndEffector"), sizeof(FFBIKEndEffector), Get_Z_Construct_UScriptStruct_FFBIKEndEffector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFBIKEndEffector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFBIKEndEffector_Hash() { return 2391945459U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
