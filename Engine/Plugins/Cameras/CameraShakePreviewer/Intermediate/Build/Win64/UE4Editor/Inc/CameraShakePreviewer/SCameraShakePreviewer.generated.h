// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERASHAKEPREVIEWER_SCameraShakePreviewer_generated_h
#error "SCameraShakePreviewer.generated.h already included, missing '#pragma once' in SCameraShakePreviewer.h"
#endif
#define CAMERASHAKEPREVIEWER_SCameraShakePreviewer_generated_h

#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_SPARSE_DATA
#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_RPC_WRAPPERS
#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPreviewPlayerCameraManager(); \
	friend struct Z_Construct_UClass_APreviewPlayerCameraManager_Statics; \
public: \
	DECLARE_CLASS(APreviewPlayerCameraManager, APlayerCameraManager, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraShakePreviewer"), NO_API) \
	DECLARE_SERIALIZER(APreviewPlayerCameraManager)


#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_INCLASS \
private: \
	static void StaticRegisterNativesAPreviewPlayerCameraManager(); \
	friend struct Z_Construct_UClass_APreviewPlayerCameraManager_Statics; \
public: \
	DECLARE_CLASS(APreviewPlayerCameraManager, APlayerCameraManager, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraShakePreviewer"), NO_API) \
	DECLARE_SERIALIZER(APreviewPlayerCameraManager)


#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APreviewPlayerCameraManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APreviewPlayerCameraManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APreviewPlayerCameraManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APreviewPlayerCameraManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APreviewPlayerCameraManager(APreviewPlayerCameraManager&&); \
	NO_API APreviewPlayerCameraManager(const APreviewPlayerCameraManager&); \
public:


#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APreviewPlayerCameraManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APreviewPlayerCameraManager(APreviewPlayerCameraManager&&); \
	NO_API APreviewPlayerCameraManager(const APreviewPlayerCameraManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APreviewPlayerCameraManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APreviewPlayerCameraManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APreviewPlayerCameraManager)


#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_35_PROLOG
#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_SPARSE_DATA \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_RPC_WRAPPERS \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_INCLASS \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_SPARSE_DATA \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERASHAKEPREVIEWER_API UClass* StaticClass<class APreviewPlayerCameraManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Cameras_CameraShakePreviewer_Source_CameraShakePreviewer_Private_SCameraShakePreviewer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
