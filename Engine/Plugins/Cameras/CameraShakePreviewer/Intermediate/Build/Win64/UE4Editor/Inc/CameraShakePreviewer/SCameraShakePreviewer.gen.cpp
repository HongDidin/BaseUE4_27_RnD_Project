// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraShakePreviewer/Private/SCameraShakePreviewer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSCameraShakePreviewer() {}
// Cross Module References
	CAMERASHAKEPREVIEWER_API UClass* Z_Construct_UClass_APreviewPlayerCameraManager_NoRegister();
	CAMERASHAKEPREVIEWER_API UClass* Z_Construct_UClass_APreviewPlayerCameraManager();
	ENGINE_API UClass* Z_Construct_UClass_APlayerCameraManager();
	UPackage* Z_Construct_UPackage__Script_CameraShakePreviewer();
// End Cross Module References
	void APreviewPlayerCameraManager::StaticRegisterNativesAPreviewPlayerCameraManager()
	{
	}
	UClass* Z_Construct_UClass_APreviewPlayerCameraManager_NoRegister()
	{
		return APreviewPlayerCameraManager::StaticClass();
	}
	struct Z_Construct_UClass_APreviewPlayerCameraManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APreviewPlayerCameraManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerCameraManager,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraShakePreviewer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APreviewPlayerCameraManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SCameraShakePreviewer.h" },
		{ "ModuleRelativePath", "Private/SCameraShakePreviewer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APreviewPlayerCameraManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APreviewPlayerCameraManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APreviewPlayerCameraManager_Statics::ClassParams = {
		&APreviewPlayerCameraManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APreviewPlayerCameraManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APreviewPlayerCameraManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APreviewPlayerCameraManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APreviewPlayerCameraManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APreviewPlayerCameraManager, 662099559);
	template<> CAMERASHAKEPREVIEWER_API UClass* StaticClass<APreviewPlayerCameraManager>()
	{
		return APreviewPlayerCameraManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APreviewPlayerCameraManager(Z_Construct_UClass_APreviewPlayerCameraManager, &APreviewPlayerCameraManager::StaticClass, TEXT("/Script/CameraShakePreviewer"), TEXT("APreviewPlayerCameraManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APreviewPlayerCameraManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
