// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePixelStreaming_init() {}
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_PixelStreaming()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/PixelStreaming",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xE1F176E3,
				0x93EDFB3D,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
