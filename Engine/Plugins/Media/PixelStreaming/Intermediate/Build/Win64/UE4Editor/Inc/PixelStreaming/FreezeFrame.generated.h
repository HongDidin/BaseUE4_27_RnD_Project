// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture2D;
#ifdef PIXELSTREAMING_FreezeFrame_generated_h
#error "FreezeFrame.generated.h already included, missing '#pragma once' in FreezeFrame.h"
#endif
#define PIXELSTREAMING_FreezeFrame_generated_h

#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_SPARSE_DATA
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUnfreezeFrame); \
	DECLARE_FUNCTION(execFreezeFrame);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUnfreezeFrame); \
	DECLARE_FUNCTION(execFreezeFrame);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFreezeFrame(); \
	friend struct Z_Construct_UClass_UFreezeFrame_Statics; \
public: \
	DECLARE_CLASS(UFreezeFrame, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UFreezeFrame)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUFreezeFrame(); \
	friend struct Z_Construct_UClass_UFreezeFrame_Statics; \
public: \
	DECLARE_CLASS(UFreezeFrame, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UFreezeFrame)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFreezeFrame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFreezeFrame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFreezeFrame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFreezeFrame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFreezeFrame(UFreezeFrame&&); \
	NO_API UFreezeFrame(const UFreezeFrame&); \
public:


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFreezeFrame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFreezeFrame(UFreezeFrame&&); \
	NO_API UFreezeFrame(const UFreezeFrame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFreezeFrame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFreezeFrame); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFreezeFrame)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_15_PROLOG
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_INCLASS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIXELSTREAMING_API UClass* StaticClass<class UFreezeFrame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_FreezeFrame_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
