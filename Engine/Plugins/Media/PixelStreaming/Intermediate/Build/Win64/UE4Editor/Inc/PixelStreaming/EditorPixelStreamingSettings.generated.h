// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PIXELSTREAMING_EditorPixelStreamingSettings_generated_h
#error "EditorPixelStreamingSettings.generated.h already included, missing '#pragma once' in EditorPixelStreamingSettings.h"
#endif
#define PIXELSTREAMING_EditorPixelStreamingSettings_generated_h

#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPixelStreamingSettings(); \
	friend struct Z_Construct_UClass_UPixelStreamingSettings_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamingSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamingSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("PixelStreaming");} \



#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUPixelStreamingSettings(); \
	friend struct Z_Construct_UClass_UPixelStreamingSettings_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamingSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamingSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("PixelStreaming");} \



#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPixelStreamingSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamingSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamingSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamingSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamingSettings(UPixelStreamingSettings&&); \
	NO_API UPixelStreamingSettings(const UPixelStreamingSettings&); \
public:


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPixelStreamingSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamingSettings(UPixelStreamingSettings&&); \
	NO_API UPixelStreamingSettings(const UPixelStreamingSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamingSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamingSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamingSettings)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_11_PROLOG
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_INCLASS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PixelStreamingSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIXELSTREAMING_API UClass* StaticClass<class UPixelStreamingSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_EditorPixelStreamingSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
