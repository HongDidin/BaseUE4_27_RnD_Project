// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PixelStreaming/Public/PixelStreamerInputComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePixelStreamerInputComponent() {}
// Cross Module References
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamerInputComponent();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamerInputComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_PixelStreaming();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics
	{
		struct PixelStreamerInputComponent_eventOnInput_Parms
		{
			FString Descriptor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Descriptor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Descriptor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::NewProp_Descriptor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::NewProp_Descriptor = { "Descriptor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventOnInput_Parms, Descriptor), METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::NewProp_Descriptor_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::NewProp_Descriptor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::NewProp_Descriptor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// The delegate which will be notified about a UI interaction.\n" },
		{ "ModuleRelativePath", "Public/PixelStreamerInputComponent.h" },
		{ "ToolTip", "The delegate which will be notified about a UI interaction." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerInputComponent, nullptr, "OnInput__DelegateSignature", nullptr, nullptr, sizeof(PixelStreamerInputComponent_eventOnInput_Parms), Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UPixelStreamerInputComponent::execAddJsonStringValue)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Descriptor);
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_PROPERTY(FStrProperty,Z_Param_StringValue);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_NewDescriptor);
		P_GET_UBOOL_REF(Z_Param_Out_Success);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddJsonStringValue(Z_Param_Descriptor,Z_Param_FieldName,Z_Param_StringValue,Z_Param_Out_NewDescriptor,Z_Param_Out_Success);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPixelStreamerInputComponent::execGetJsonStringValue)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Descriptor);
		P_GET_PROPERTY(FStrProperty,Z_Param_FieldName);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_StringValue);
		P_GET_UBOOL_REF(Z_Param_Out_Success);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetJsonStringValue(Z_Param_Descriptor,Z_Param_FieldName,Z_Param_Out_StringValue,Z_Param_Out_Success);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPixelStreamerInputComponent::execSendPixelStreamingResponse)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Descriptor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendPixelStreamingResponse(Z_Param_Descriptor);
		P_NATIVE_END;
	}
	void UPixelStreamerInputComponent::StaticRegisterNativesUPixelStreamerInputComponent()
	{
		UClass* Class = UPixelStreamerInputComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddJsonStringValue", &UPixelStreamerInputComponent::execAddJsonStringValue },
			{ "GetJsonStringValue", &UPixelStreamerInputComponent::execGetJsonStringValue },
			{ "SendPixelStreamingResponse", &UPixelStreamerInputComponent::execSendPixelStreamingResponse },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics
	{
		struct PixelStreamerInputComponent_eventAddJsonStringValue_Parms
		{
			FString Descriptor;
			FString FieldName;
			FString StringValue;
			FString NewDescriptor;
			bool Success;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Descriptor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Descriptor;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewDescriptor;
		static void NewProp_Success_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Success;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Descriptor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Descriptor = { "Descriptor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventAddJsonStringValue_Parms, Descriptor), METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Descriptor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Descriptor_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventAddJsonStringValue_Parms, FieldName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_StringValue = { "StringValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventAddJsonStringValue_Parms, StringValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_NewDescriptor = { "NewDescriptor", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventAddJsonStringValue_Parms, NewDescriptor), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Success_SetBit(void* Obj)
	{
		((PixelStreamerInputComponent_eventAddJsonStringValue_Parms*)Obj)->Success = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Success = { "Success", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PixelStreamerInputComponent_eventAddJsonStringValue_Parms), &Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Success_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Descriptor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_StringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_NewDescriptor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::NewProp_Success,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "PixelStreamer Input" },
		{ "Comment", "/**\n\x09 * Helper function to add a string field to a JSON descriptor. This produces\n\x09 * a new descriptor which may then be chained to add further string fields.\n\x09 * @param Descriptor - The initial JSON descriptor which may be blank initially.\n\x09 * @param FieldName - The name of the field to add to the JSON.\n\x09 * @param StringValue - The string value associated with the field name.\n\x09 * @param NewDescriptor - The JSON descriptor with the string field added.\n\x09 * @param Success - True if the string field could be added successfully.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PixelStreamerInputComponent.h" },
		{ "ToolTip", "Helper function to add a string field to a JSON descriptor. This produces\na new descriptor which may then be chained to add further string fields.\n@param Descriptor - The initial JSON descriptor which may be blank initially.\n@param FieldName - The name of the field to add to the JSON.\n@param StringValue - The string value associated with the field name.\n@param NewDescriptor - The JSON descriptor with the string field added.\n@param Success - True if the string field could be added successfully." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerInputComponent, nullptr, "AddJsonStringValue", nullptr, nullptr, sizeof(PixelStreamerInputComponent_eventAddJsonStringValue_Parms), Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics
	{
		struct PixelStreamerInputComponent_eventGetJsonStringValue_Parms
		{
			FString Descriptor;
			FString FieldName;
			FString StringValue;
			bool Success;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Descriptor;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringValue;
		static void NewProp_Success_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Success;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_Descriptor = { "Descriptor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventGetJsonStringValue_Parms, Descriptor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventGetJsonStringValue_Parms, FieldName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_StringValue = { "StringValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventGetJsonStringValue_Parms, StringValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_Success_SetBit(void* Obj)
	{
		((PixelStreamerInputComponent_eventGetJsonStringValue_Parms*)Obj)->Success = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_Success = { "Success", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PixelStreamerInputComponent_eventGetJsonStringValue_Parms), &Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_Success_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_Descriptor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_StringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::NewProp_Success,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "PixelStreamer Input" },
		{ "Comment", "/**\n\x09 * Helper function to extract a string field from a JSON descriptor of a\n\x09 * UI interaction given its field name.\n\x09 * The field name may be hierarchical, delimited by a period. For example,\n\x09 * to access the Width value of a Resolution command above you should use\n\x09 * \"Resolution.Width\" to get the width value.\n\x09 * @param Descriptor - The UI interaction JSON descriptor.\n\x09 * @param FieldName - The name of the field to look for in the JSON.\n\x09 * @param StringValue - The string value associated with the field name.\n\x09 * @param Success - True if the field exists in the JSON data.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PixelStreamerInputComponent.h" },
		{ "ToolTip", "Helper function to extract a string field from a JSON descriptor of a\nUI interaction given its field name.\nThe field name may be hierarchical, delimited by a period. For example,\nto access the Width value of a Resolution command above you should use\n\"Resolution.Width\" to get the width value.\n@param Descriptor - The UI interaction JSON descriptor.\n@param FieldName - The name of the field to look for in the JSON.\n@param StringValue - The string value associated with the field name.\n@param Success - True if the field exists in the JSON data." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerInputComponent, nullptr, "GetJsonStringValue", nullptr, nullptr, sizeof(PixelStreamerInputComponent_eventGetJsonStringValue_Parms), Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics
	{
		struct PixelStreamerInputComponent_eventSendPixelStreamingResponse_Parms
		{
			FString Descriptor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Descriptor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Descriptor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::NewProp_Descriptor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::NewProp_Descriptor = { "Descriptor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerInputComponent_eventSendPixelStreamingResponse_Parms, Descriptor), METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::NewProp_Descriptor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::NewProp_Descriptor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::NewProp_Descriptor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::Function_MetaDataParams[] = {
		{ "Category", "PixelStreamer Input" },
		{ "Comment", "/**\n\x09 * Send a response back to the source of the UI interactions.\n\x09 * @param Descriptor - A generic descriptor string.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PixelStreamerInputComponent.h" },
		{ "ToolTip", "Send a response back to the source of the UI interactions.\n@param Descriptor - A generic descriptor string." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerInputComponent, nullptr, "SendPixelStreamingResponse", nullptr, nullptr, sizeof(PixelStreamerInputComponent_eventSendPixelStreamingResponse_Parms), Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPixelStreamerInputComponent_NoRegister()
	{
		return UPixelStreamerInputComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPixelStreamerInputComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInputEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnInputEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPixelStreamerInputComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PixelStreaming,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPixelStreamerInputComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPixelStreamerInputComponent_AddJsonStringValue, "AddJsonStringValue" }, // 1134388258
		{ &Z_Construct_UFunction_UPixelStreamerInputComponent_GetJsonStringValue, "GetJsonStringValue" }, // 3153017210
		{ &Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature, "OnInput__DelegateSignature" }, // 2417487051
		{ &Z_Construct_UFunction_UPixelStreamerInputComponent_SendPixelStreamingResponse, "SendPixelStreamingResponse" }, // 393181040
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerInputComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "PixelStreamer" },
		{ "Comment", "/**\n * This component may be attached to an actor to allow UI interactions to be\n * handled as the delegate will be notified about the interaction and will be\n * supplied with a generic descriptor string containing, for example, JSON data.\n * Responses back to the source of the UI interactions may also be sent.\n */" },
		{ "IncludePath", "PixelStreamerInputComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PixelStreamerInputComponent.h" },
		{ "ToolTip", "This component may be attached to an actor to allow UI interactions to be\nhandled as the delegate will be notified about the interaction and will be\nsupplied with a generic descriptor string containing, for example, JSON data.\nResponses back to the source of the UI interactions may also be sent." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerInputComponent_Statics::NewProp_OnInputEvent_MetaData[] = {
		{ "Category", "PixelStreamer Input" },
		{ "ModuleRelativePath", "Public/PixelStreamerInputComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPixelStreamerInputComponent_Statics::NewProp_OnInputEvent = { "OnInputEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamerInputComponent, OnInputEvent), Z_Construct_UDelegateFunction_UPixelStreamerInputComponent_OnInput__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerInputComponent_Statics::NewProp_OnInputEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerInputComponent_Statics::NewProp_OnInputEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPixelStreamerInputComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamerInputComponent_Statics::NewProp_OnInputEvent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPixelStreamerInputComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPixelStreamerInputComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPixelStreamerInputComponent_Statics::ClassParams = {
		&UPixelStreamerInputComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPixelStreamerInputComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerInputComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerInputComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerInputComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPixelStreamerInputComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPixelStreamerInputComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPixelStreamerInputComponent, 1117931764);
	template<> PIXELSTREAMING_API UClass* StaticClass<UPixelStreamerInputComponent>()
	{
		return UPixelStreamerInputComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPixelStreamerInputComponent(Z_Construct_UClass_UPixelStreamerInputComponent, &UPixelStreamerInputComponent::StaticClass, TEXT("/Script/PixelStreaming"), TEXT("UPixelStreamerInputComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPixelStreamerInputComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
