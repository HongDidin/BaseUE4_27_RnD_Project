// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PIXELSTREAMING_PixelStreamingAudioComponent_generated_h
#error "PixelStreamingAudioComponent.generated.h already included, missing '#pragma once' in PixelStreamingAudioComponent.h"
#endif
#define PIXELSTREAMING_PixelStreamingAudioComponent_generated_h

#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_SPARSE_DATA
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execIsListeningToPlayer); \
	DECLARE_FUNCTION(execListenTo);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execIsListeningToPlayer); \
	DECLARE_FUNCTION(execListenTo);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPixelStreamingAudioComponent(); \
	friend struct Z_Construct_UClass_UPixelStreamingAudioComponent_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamingAudioComponent, USynthComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamingAudioComponent)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUPixelStreamingAudioComponent(); \
	friend struct Z_Construct_UClass_UPixelStreamingAudioComponent_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamingAudioComponent, USynthComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamingAudioComponent)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPixelStreamingAudioComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamingAudioComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamingAudioComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamingAudioComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamingAudioComponent(UPixelStreamingAudioComponent&&); \
	NO_API UPixelStreamingAudioComponent(const UPixelStreamingAudioComponent&); \
public:


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamingAudioComponent(UPixelStreamingAudioComponent&&); \
	NO_API UPixelStreamingAudioComponent(const UPixelStreamingAudioComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamingAudioComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamingAudioComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamingAudioComponent)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_17_PROLOG
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_RPC_WRAPPERS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_INCLASS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIXELSTREAMING_API UClass* StaticClass<class UPixelStreamingAudioComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamingAudioComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
