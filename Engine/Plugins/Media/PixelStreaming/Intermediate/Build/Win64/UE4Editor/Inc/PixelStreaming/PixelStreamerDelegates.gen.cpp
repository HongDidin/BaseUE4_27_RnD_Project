// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PixelStreaming/Private/PixelStreamerDelegates.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePixelStreamerDelegates() {}
// Cross Module References
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamerDelegates();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature();
	PIXELSTREAMING_API UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamerDelegates_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_PixelStreaming();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * All connections have closed and nobody is viewing or interacting with\n\x09 * the app. This is an opportunity to reset the app.\n\x09 */" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
		{ "ToolTip", "All connections have closed and nobody is viewing or interacting with\nthe app. This is an opportunity to reset the app." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerDelegates, nullptr, "OnAllConnectionsClosed__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics
	{
		struct PixelStreamerDelegates_eventOnClosedConnection_Parms
		{
			FString PlayerId;
			bool WasQualityController;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerId;
		static void NewProp_WasQualityController_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WasQualityController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::NewProp_PlayerId = { "PlayerId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerDelegates_eventOnClosedConnection_Parms, PlayerId), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::NewProp_WasQualityController_SetBit(void* Obj)
	{
		((PixelStreamerDelegates_eventOnClosedConnection_Parms*)Obj)->WasQualityController = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::NewProp_WasQualityController = { "WasQualityController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PixelStreamerDelegates_eventOnClosedConnection_Parms), &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::NewProp_WasQualityController_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::NewProp_PlayerId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::NewProp_WasQualityController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * A connection to a player was lost.\n\x09 */" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
		{ "ToolTip", "A connection to a player was lost." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerDelegates, nullptr, "OnClosedConnection__DelegateSignature", nullptr, nullptr, sizeof(PixelStreamerDelegates_eventOnClosedConnection_Parms), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics
	{
		struct PixelStreamerDelegates_eventOnNewConnection_Parms
		{
			FString PlayerId;
			bool QualityController;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerId;
		static void NewProp_QualityController_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_QualityController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::NewProp_PlayerId = { "PlayerId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerDelegates_eventOnNewConnection_Parms, PlayerId), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::NewProp_QualityController_SetBit(void* Obj)
	{
		((PixelStreamerDelegates_eventOnNewConnection_Parms*)Obj)->QualityController = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::NewProp_QualityController = { "QualityController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PixelStreamerDelegates_eventOnNewConnection_Parms), &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::NewProp_QualityController_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::NewProp_PlayerId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::NewProp_QualityController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * A new connection has been made to the session.\n\x09 */" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
		{ "ToolTip", "A new connection has been made to the session." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerDelegates, nullptr, "OnNewConnection__DelegateSignature", nullptr, nullptr, sizeof(PixelStreamerDelegates_eventOnNewConnection_Parms), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * A connection to the signalling server was lost.\n\x09 */" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
		{ "ToolTip", "A connection to the signalling server was lost." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerDelegates, nullptr, "OnDisconnectedFromSignallingServer__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * A connection to the signalling server was made.\n\x09 */" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
		{ "ToolTip", "A connection to the signalling server was made." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerDelegates, nullptr, "OnConnecedToSignallingServer__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UPixelStreamerDelegates::execGetPixelStreamerDelegates)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UPixelStreamerDelegates**)Z_Param__Result=UPixelStreamerDelegates::GetPixelStreamerDelegates();
		P_NATIVE_END;
	}
	void UPixelStreamerDelegates::StaticRegisterNativesUPixelStreamerDelegates()
	{
		UClass* Class = UPixelStreamerDelegates::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPixelStreamerDelegates", &UPixelStreamerDelegates::execGetPixelStreamerDelegates },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics
	{
		struct PixelStreamerDelegates_eventGetPixelStreamerDelegates_Parms
		{
			UPixelStreamerDelegates* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamerDelegates_eventGetPixelStreamerDelegates_Parms, ReturnValue), Z_Construct_UClass_UPixelStreamerDelegates_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::Function_MetaDataParams[] = {
		{ "Category", "Pixel Streamer Delegates" },
		{ "Comment", "/**\n\x09 * Get the singleton. This allows application-specific blueprints to bind\n\x09 * to delegates of interest.\n\x09 */" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
		{ "ToolTip", "Get the singleton. This allows application-specific blueprints to bind\nto delegates of interest." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamerDelegates, nullptr, "GetPixelStreamerDelegates", nullptr, nullptr, sizeof(PixelStreamerDelegates_eventGetPixelStreamerDelegates_Parms), Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPixelStreamerDelegates_NoRegister()
	{
		return UPixelStreamerDelegates::StaticClass();
	}
	struct Z_Construct_UClass_UPixelStreamerDelegates_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnConnecedToSignallingServer_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnConnecedToSignallingServer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDisconnectedFromSignallingServer_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnDisconnectedFromSignallingServer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnNewConnection_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnNewConnection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnClosedConnection_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnClosedConnection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAllConnectionsClosed_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAllConnectionsClosed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPixelStreamerDelegates_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PixelStreaming,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPixelStreamerDelegates_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPixelStreamerDelegates_GetPixelStreamerDelegates, "GetPixelStreamerDelegates" }, // 1964776425
		{ &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature, "OnAllConnectionsClosed__DelegateSignature" }, // 3215889121
		{ &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnClosedConnection__DelegateSignature, "OnClosedConnection__DelegateSignature" }, // 3644452228
		{ &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature, "OnConnecedToSignallingServer__DelegateSignature" }, // 3539013694
		{ &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature, "OnDisconnectedFromSignallingServer__DelegateSignature" }, // 2444628534
		{ &Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature, "OnNewConnection__DelegateSignature" }, // 510637884
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerDelegates_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PixelStreamerDelegates.h" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnConnecedToSignallingServer_MetaData[] = {
		{ "Category", "Pixel Streamer Delegates" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnConnecedToSignallingServer = { "OnConnecedToSignallingServer", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamerDelegates, OnConnecedToSignallingServer), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnConnecedToSignallingServer__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnConnecedToSignallingServer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnConnecedToSignallingServer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnDisconnectedFromSignallingServer_MetaData[] = {
		{ "Category", "Pixel Streamer Delegates" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnDisconnectedFromSignallingServer = { "OnDisconnectedFromSignallingServer", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamerDelegates, OnDisconnectedFromSignallingServer), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnDisconnectedFromSignallingServer__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnDisconnectedFromSignallingServer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnDisconnectedFromSignallingServer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnNewConnection_MetaData[] = {
		{ "Category", "Pixel Streamer Delegates" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnNewConnection = { "OnNewConnection", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamerDelegates, OnNewConnection), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnNewConnection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnNewConnection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnClosedConnection_MetaData[] = {
		{ "Category", "Pixel Streamer Delegates" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnClosedConnection = { "OnClosedConnection", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamerDelegates, OnClosedConnection), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnNewConnection__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnClosedConnection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnClosedConnection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnAllConnectionsClosed_MetaData[] = {
		{ "Category", "Pixel Streamer Delegates" },
		{ "ModuleRelativePath", "Private/PixelStreamerDelegates.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnAllConnectionsClosed = { "OnAllConnectionsClosed", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamerDelegates, OnAllConnectionsClosed), Z_Construct_UDelegateFunction_UPixelStreamerDelegates_OnAllConnectionsClosed__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnAllConnectionsClosed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnAllConnectionsClosed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPixelStreamerDelegates_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnConnecedToSignallingServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnDisconnectedFromSignallingServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnNewConnection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnClosedConnection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamerDelegates_Statics::NewProp_OnAllConnectionsClosed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPixelStreamerDelegates_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPixelStreamerDelegates>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPixelStreamerDelegates_Statics::ClassParams = {
		&UPixelStreamerDelegates::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPixelStreamerDelegates_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPixelStreamerDelegates_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamerDelegates_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPixelStreamerDelegates()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPixelStreamerDelegates_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPixelStreamerDelegates, 4023349537);
	template<> PIXELSTREAMING_API UClass* StaticClass<UPixelStreamerDelegates>()
	{
		return UPixelStreamerDelegates::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPixelStreamerDelegates(Z_Construct_UClass_UPixelStreamerDelegates, &UPixelStreamerDelegates::StaticClass, TEXT("/Script/PixelStreaming"), TEXT("UPixelStreamerDelegates"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPixelStreamerDelegates);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
