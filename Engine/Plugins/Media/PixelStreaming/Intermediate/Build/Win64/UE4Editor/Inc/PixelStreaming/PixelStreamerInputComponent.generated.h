// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PIXELSTREAMING_PixelStreamerInputComponent_generated_h
#error "PixelStreamerInputComponent.generated.h already included, missing '#pragma once' in PixelStreamerInputComponent.h"
#endif
#define PIXELSTREAMING_PixelStreamerInputComponent_generated_h

#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_28_DELEGATE \
struct PixelStreamerInputComponent_eventOnInput_Parms \
{ \
	FString Descriptor; \
}; \
static inline void FOnInput_DelegateWrapper(const FMulticastScriptDelegate& OnInput, const FString& Descriptor) \
{ \
	PixelStreamerInputComponent_eventOnInput_Parms Parms; \
	Parms.Descriptor=Descriptor; \
	OnInput.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_SPARSE_DATA
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddJsonStringValue); \
	DECLARE_FUNCTION(execGetJsonStringValue); \
	DECLARE_FUNCTION(execSendPixelStreamingResponse);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddJsonStringValue); \
	DECLARE_FUNCTION(execGetJsonStringValue); \
	DECLARE_FUNCTION(execSendPixelStreamingResponse);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPixelStreamerInputComponent(); \
	friend struct Z_Construct_UClass_UPixelStreamerInputComponent_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamerInputComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamerInputComponent)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUPixelStreamerInputComponent(); \
	friend struct Z_Construct_UClass_UPixelStreamerInputComponent_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamerInputComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamerInputComponent)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPixelStreamerInputComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamerInputComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamerInputComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamerInputComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamerInputComponent(UPixelStreamerInputComponent&&); \
	NO_API UPixelStreamerInputComponent(const UPixelStreamerInputComponent&); \
public:


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamerInputComponent(UPixelStreamerInputComponent&&); \
	NO_API UPixelStreamerInputComponent(const UPixelStreamerInputComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamerInputComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamerInputComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPixelStreamerInputComponent)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_16_PROLOG
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_RPC_WRAPPERS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_INCLASS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIXELSTREAMING_API UClass* StaticClass<class UPixelStreamerInputComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Public_PixelStreamerInputComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
