// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPixelStreamerDelegates;
#ifdef PIXELSTREAMING_PixelStreamerDelegates_generated_h
#error "PixelStreamerDelegates.generated.h already included, missing '#pragma once' in PixelStreamerDelegates.h"
#endif
#define PIXELSTREAMING_PixelStreamerDelegates_generated_h

#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_46_DELEGATE \
static inline void FOnAllConnectionsClosed_DelegateWrapper(const FMulticastScriptDelegate& OnAllConnectionsClosed) \
{ \
	OnAllConnectionsClosed.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_38_DELEGATE \
struct PixelStreamerDelegates_eventOnClosedConnection_Parms \
{ \
	FString PlayerId; \
	bool WasQualityController; \
}; \
static inline void FOnClosedConnection_DelegateWrapper(const FMulticastScriptDelegate& OnClosedConnection, const FString& PlayerId, bool WasQualityController) \
{ \
	PixelStreamerDelegates_eventOnClosedConnection_Parms Parms; \
	Parms.PlayerId=PlayerId; \
	Parms.WasQualityController=WasQualityController ? true : false; \
	OnClosedConnection.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_31_DELEGATE \
struct PixelStreamerDelegates_eventOnNewConnection_Parms \
{ \
	FString PlayerId; \
	bool QualityController; \
}; \
static inline void FOnNewConnection_DelegateWrapper(const FMulticastScriptDelegate& OnNewConnection, const FString& PlayerId, bool QualityController) \
{ \
	PixelStreamerDelegates_eventOnNewConnection_Parms Parms; \
	Parms.PlayerId=PlayerId; \
	Parms.QualityController=QualityController ? true : false; \
	OnNewConnection.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_24_DELEGATE \
static inline void FOnDisconnectedFromSignallingServer_DelegateWrapper(const FMulticastScriptDelegate& OnDisconnectedFromSignallingServer) \
{ \
	OnDisconnectedFromSignallingServer.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_17_DELEGATE \
static inline void FOnConnecedToSignallingServer_DelegateWrapper(const FMulticastScriptDelegate& OnConnecedToSignallingServer) \
{ \
	OnConnecedToSignallingServer.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_SPARSE_DATA
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetPixelStreamerDelegates);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetPixelStreamerDelegates);


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPixelStreamerDelegates(); \
	friend struct Z_Construct_UClass_UPixelStreamerDelegates_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamerDelegates, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamerDelegates)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUPixelStreamerDelegates(); \
	friend struct Z_Construct_UClass_UPixelStreamerDelegates_Statics; \
public: \
	DECLARE_CLASS(UPixelStreamerDelegates, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PixelStreaming"), NO_API) \
	DECLARE_SERIALIZER(UPixelStreamerDelegates)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPixelStreamerDelegates(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamerDelegates) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamerDelegates); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamerDelegates); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamerDelegates(UPixelStreamerDelegates&&); \
	NO_API UPixelStreamerDelegates(const UPixelStreamerDelegates&); \
public:


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPixelStreamerDelegates(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPixelStreamerDelegates(UPixelStreamerDelegates&&); \
	NO_API UPixelStreamerDelegates(const UPixelStreamerDelegates&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPixelStreamerDelegates); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPixelStreamerDelegates); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPixelStreamerDelegates)


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_7_PROLOG
#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_RPC_WRAPPERS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_INCLASS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_SPARSE_DATA \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIXELSTREAMING_API UClass* StaticClass<class UPixelStreamerDelegates>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_PixelStreaming_Source_PixelStreaming_Private_PixelStreamerDelegates_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
