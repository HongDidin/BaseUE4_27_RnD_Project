// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PixelStreaming/Private/FreezeFrame.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFreezeFrame() {}
// Cross Module References
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UFreezeFrame_NoRegister();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UFreezeFrame();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_PixelStreaming();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UFreezeFrame::execUnfreezeFrame)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UFreezeFrame::UnfreezeFrame();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UFreezeFrame::execFreezeFrame)
	{
		P_GET_OBJECT(UTexture2D,Z_Param_Texture);
		P_FINISH;
		P_NATIVE_BEGIN;
		UFreezeFrame::FreezeFrame(Z_Param_Texture);
		P_NATIVE_END;
	}
	void UFreezeFrame::StaticRegisterNativesUFreezeFrame()
	{
		UClass* Class = UFreezeFrame::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FreezeFrame", &UFreezeFrame::execFreezeFrame },
			{ "UnfreezeFrame", &UFreezeFrame::execUnfreezeFrame },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics
	{
		struct FreezeFrame_eventFreezeFrame_Parms
		{
			UTexture2D* Texture;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FreezeFrame_eventFreezeFrame_Parms, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::NewProp_Texture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "Pixel Streaming Freeze Frame" },
		{ "Comment", "/**\n\x09 * Freeze Pixel Streaming.\n\x09 * @param Texture - The freeze frame to display. If null then the back buffer is captured.\n\x09 */" },
		{ "ModuleRelativePath", "Private/FreezeFrame.h" },
		{ "ToolTip", "Freeze Pixel Streaming.\n@param Texture - The freeze frame to display. If null then the back buffer is captured." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UFreezeFrame, nullptr, "FreezeFrame", nullptr, nullptr, sizeof(FreezeFrame_eventFreezeFrame_Parms), Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UFreezeFrame_FreezeFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UFreezeFrame_FreezeFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "Pixel Streaming Freeze Frame" },
		{ "Comment", "/**\n\x09 * Unfreeze Pixel Streaming. \n\x09 */" },
		{ "ModuleRelativePath", "Private/FreezeFrame.h" },
		{ "ToolTip", "Unfreeze Pixel Streaming." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UFreezeFrame, nullptr, "UnfreezeFrame", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UFreezeFrame_NoRegister()
	{
		return UFreezeFrame::StaticClass();
	}
	struct Z_Construct_UClass_UFreezeFrame_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFreezeFrame_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PixelStreaming,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UFreezeFrame_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UFreezeFrame_FreezeFrame, "FreezeFrame" }, // 3213583651
		{ &Z_Construct_UFunction_UFreezeFrame_UnfreezeFrame, "UnfreezeFrame" }, // 1550514847
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFreezeFrame_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * This singleton object allows Pixel Streaming to be frozen and unfrozen from\n * Blueprint. When frozen, a freeze frame (a still image) will be used by the\n * browser instead of the video stream.\n */" },
		{ "IncludePath", "FreezeFrame.h" },
		{ "ModuleRelativePath", "Private/FreezeFrame.h" },
		{ "ToolTip", "This singleton object allows Pixel Streaming to be frozen and unfrozen from\nBlueprint. When frozen, a freeze frame (a still image) will be used by the\nbrowser instead of the video stream." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFreezeFrame_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFreezeFrame>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFreezeFrame_Statics::ClassParams = {
		&UFreezeFrame::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFreezeFrame_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFreezeFrame_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFreezeFrame()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFreezeFrame_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFreezeFrame, 3127733912);
	template<> PIXELSTREAMING_API UClass* StaticClass<UFreezeFrame>()
	{
		return UFreezeFrame::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFreezeFrame(Z_Construct_UClass_UFreezeFrame, &UFreezeFrame::StaticClass, TEXT("/Script/PixelStreaming"), TEXT("UFreezeFrame"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFreezeFrame);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
