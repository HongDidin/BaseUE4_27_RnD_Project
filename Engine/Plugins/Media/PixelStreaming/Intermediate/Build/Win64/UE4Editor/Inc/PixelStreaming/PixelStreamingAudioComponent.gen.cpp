// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PixelStreaming/Public/PixelStreamingAudioComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePixelStreamingAudioComponent() {}
// Cross Module References
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamingAudioComponent_NoRegister();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamingAudioComponent();
	AUDIOMIXER_API UClass* Z_Construct_UClass_USynthComponent();
	UPackage* Z_Construct_UPackage__Script_PixelStreaming();
// End Cross Module References
	DEFINE_FUNCTION(UPixelStreamingAudioComponent::execReset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Reset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPixelStreamingAudioComponent::execIsListeningToPlayer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsListeningToPlayer();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPixelStreamingAudioComponent::execListenTo)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_PlayerToListenTo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ListenTo(Z_Param_PlayerToListenTo);
		P_NATIVE_END;
	}
	void UPixelStreamingAudioComponent::StaticRegisterNativesUPixelStreamingAudioComponent()
	{
		UClass* Class = UPixelStreamingAudioComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsListeningToPlayer", &UPixelStreamingAudioComponent::execIsListeningToPlayer },
			{ "ListenTo", &UPixelStreamingAudioComponent::execListenTo },
			{ "Reset", &UPixelStreamingAudioComponent::execReset },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics
	{
		struct PixelStreamingAudioComponent_eventIsListeningToPlayer_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PixelStreamingAudioComponent_eventIsListeningToPlayer_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PixelStreamingAudioComponent_eventIsListeningToPlayer_Parms), &Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::Function_MetaDataParams[] = {
		{ "Category", "Pixel Streaming Audio Component" },
		{ "Comment", "// True if listening to a connected WebRTC peer through Pixel Streaming.\n" },
		{ "ModuleRelativePath", "Public/PixelStreamingAudioComponent.h" },
		{ "ToolTip", "True if listening to a connected WebRTC peer through Pixel Streaming." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamingAudioComponent, nullptr, "IsListeningToPlayer", nullptr, nullptr, sizeof(PixelStreamingAudioComponent_eventIsListeningToPlayer_Parms), Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics
	{
		struct PixelStreamingAudioComponent_eventListenTo_Parms
		{
			FString PlayerToListenTo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerToListenTo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::NewProp_PlayerToListenTo = { "PlayerToListenTo", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PixelStreamingAudioComponent_eventListenTo_Parms, PlayerToListenTo), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PixelStreamingAudioComponent_eventListenTo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PixelStreamingAudioComponent_eventListenTo_Parms), &Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::NewProp_PlayerToListenTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Pixel Streaming Audio Component" },
		{ "Comment", "// Listen to a specific player. If the player is not found this component will be silent.\n" },
		{ "ModuleRelativePath", "Public/PixelStreamingAudioComponent.h" },
		{ "ToolTip", "Listen to a specific player. If the player is not found this component will be silent." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamingAudioComponent, nullptr, "ListenTo", nullptr, nullptr, sizeof(PixelStreamingAudioComponent_eventListenTo_Parms), Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Pixel Streaming Audio Component" },
		{ "Comment", "// Stops listening to any connected player/peer and resets internal state so component is ready to listen again.\n" },
		{ "ModuleRelativePath", "Public/PixelStreamingAudioComponent.h" },
		{ "ToolTip", "Stops listening to any connected player/peer and resets internal state so component is ready to listen again." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPixelStreamingAudioComponent, nullptr, "Reset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPixelStreamingAudioComponent_NoRegister()
	{
		return UPixelStreamingAudioComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPixelStreamingAudioComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerToHear_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerToHear;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoFindPeer_MetaData[];
#endif
		static void NewProp_bAutoFindPeer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoFindPeer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USynthComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PixelStreaming,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPixelStreamingAudioComponent_IsListeningToPlayer, "IsListeningToPlayer" }, // 1081981369
		{ &Z_Construct_UFunction_UPixelStreamingAudioComponent_ListenTo, "ListenTo" }, // 2290990944
		{ &Z_Construct_UFunction_UPixelStreamingAudioComponent_Reset, "Reset" }, // 2669514706
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "PixelStreamer" },
		{ "Comment", "/**\n * Allows in-engine playback of incoming WebRTC audio from a particular Pixel Streaming player/peer using their mic in the browser.\n * Note: Each audio component associates itself with a particular Pixel Streaming player/peer (using the the Pixel Streaming player id).\n */" },
		{ "HideCategories", "Object ActorComponent Physics Rendering Mobility LOD Trigger PhysicsVolume" },
		{ "IncludePath", "PixelStreamingAudioComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PixelStreamingAudioComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Allows in-engine playback of incoming WebRTC audio from a particular Pixel Streaming player/peer using their mic in the browser.\nNote: Each audio component associates itself with a particular Pixel Streaming player/peer (using the the Pixel Streaming player id)." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_PlayerToHear_MetaData[] = {
		{ "Category", "Pixel Streaming Audio Component" },
		{ "Comment", "/** \n        *   The Pixel Streaming player/peer whose audio we wish to listen to.\n        *   If this is left blank this component will listen to the first non-listened to peer that connects after this component is ready.\n        *   Note: that when the listened to peer disconnects this component is reset to blank and will once again listen to the next non-listened to peer that connects.\n        */" },
		{ "ModuleRelativePath", "Public/PixelStreamingAudioComponent.h" },
		{ "ToolTip", "The Pixel Streaming player/peer whose audio we wish to listen to.\nIf this is left blank this component will listen to the first non-listened to peer that connects after this component is ready.\nNote: that when the listened to peer disconnects this component is reset to blank and will once again listen to the next non-listened to peer that connects." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_PlayerToHear = { "PlayerToHear", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamingAudioComponent, PlayerToHear), METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_PlayerToHear_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_PlayerToHear_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer_MetaData[] = {
		{ "Category", "Pixel Streaming Audio Component" },
		{ "Comment", "/**\n         *  If not already listening to a player/peer will try to attach for listening to the \"PlayerToHear\" each tick.\n         */" },
		{ "ModuleRelativePath", "Public/PixelStreamingAudioComponent.h" },
		{ "ToolTip", "If not already listening to a player/peer will try to attach for listening to the \"PlayerToHear\" each tick." },
	};
#endif
	void Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer_SetBit(void* Obj)
	{
		((UPixelStreamingAudioComponent*)Obj)->bAutoFindPeer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer = { "bAutoFindPeer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPixelStreamingAudioComponent), &Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_PlayerToHear,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::NewProp_bAutoFindPeer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPixelStreamingAudioComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::ClassParams = {
		&UPixelStreamingAudioComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPixelStreamingAudioComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPixelStreamingAudioComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPixelStreamingAudioComponent, 741075713);
	template<> PIXELSTREAMING_API UClass* StaticClass<UPixelStreamingAudioComponent>()
	{
		return UPixelStreamingAudioComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPixelStreamingAudioComponent(Z_Construct_UClass_UPixelStreamingAudioComponent, &UPixelStreamingAudioComponent::StaticClass, TEXT("/Script/PixelStreaming"), TEXT("UPixelStreamingAudioComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPixelStreamingAudioComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
