// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PixelStreaming/Public/EditorPixelStreamingSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorPixelStreamingSettings() {}
// Cross Module References
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamingSettings_NoRegister();
	PIXELSTREAMING_API UClass* Z_Construct_UClass_UPixelStreamingSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_PixelStreaming();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	void UPixelStreamingSettings::StaticRegisterNativesUPixelStreamingSettings()
	{
	}
	UClass* Z_Construct_UClass_UPixelStreamingSettings_NoRegister()
	{
		return UPixelStreamingSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPixelStreamingSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelStreamerDefaultCursorClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PixelStreamerDefaultCursorClassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelStreamerTextEditBeamCursorClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PixelStreamerTextEditBeamCursorClassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelStreamerHiddenCursorClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PixelStreamerHiddenCursorClassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPixelStreamerMouseAlwaysAttached_MetaData[];
#endif
		static void NewProp_bPixelStreamerMouseAlwaysAttached_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPixelStreamerMouseAlwaysAttached;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPixelStreamingSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_PixelStreaming,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Config loaded/saved to an .ini file.\n// It is also exposed through the plugin settings page in editor.\n" },
		{ "DisplayName", "PixelStreaming" },
		{ "IncludePath", "EditorPixelStreamingSettings.h" },
		{ "ModuleRelativePath", "Public/EditorPixelStreamingSettings.h" },
		{ "ToolTip", "Config loaded/saved to an .ini file.\nIt is also exposed through the plugin settings page in editor." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerDefaultCursorClassName_MetaData[] = {
		{ "Category", "PixelStreaming" },
		{ "Comment", "/**\n\x09 * Pixel streaming always requires various software cursors so they will be\n\x09 * visible in the video stream sent to the browser to allow the user to\n\x09 * click and interact with UI elements.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorPixelStreamingSettings.h" },
		{ "ToolTip", "Pixel streaming always requires various software cursors so they will be\nvisible in the video stream sent to the browser to allow the user to\nclick and interact with UI elements." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerDefaultCursorClassName = { "PixelStreamerDefaultCursorClassName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamingSettings, PixelStreamerDefaultCursorClassName), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerDefaultCursorClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerDefaultCursorClassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerTextEditBeamCursorClassName_MetaData[] = {
		{ "Category", "PixelStreaming" },
		{ "ModuleRelativePath", "Public/EditorPixelStreamingSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerTextEditBeamCursorClassName = { "PixelStreamerTextEditBeamCursorClassName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamingSettings, PixelStreamerTextEditBeamCursorClassName), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerTextEditBeamCursorClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerTextEditBeamCursorClassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerHiddenCursorClassName_MetaData[] = {
		{ "Category", "PixelStreaming" },
		{ "Comment", "/**\n\x09 * Pixel Streaming can have a server-side cursor (where the cursor itself\n\x09 * is shown as part of the video), or a client-side cursor (where the cursor\n\x09 * is shown by the browser). In the latter case we need to turn the UE4\n\x09 * cursor invisible.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorPixelStreamingSettings.h" },
		{ "ToolTip", "Pixel Streaming can have a server-side cursor (where the cursor itself\nis shown as part of the video), or a client-side cursor (where the cursor\nis shown by the browser). In the latter case we need to turn the UE4\ncursor invisible." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerHiddenCursorClassName = { "PixelStreamerHiddenCursorClassName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPixelStreamingSettings, PixelStreamerHiddenCursorClassName), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerHiddenCursorClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerHiddenCursorClassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached_MetaData[] = {
		{ "Category", "PixelStreaming" },
		{ "Comment", "/**\n\x09 * Pixel Streaming may be running on a machine which has no physical mouse\n\x09 * attached, and yet the browser is sending mouse positions. As such, we\n\x09 * fake the presence of a mouse.\n\x09 */" },
		{ "ModuleRelativePath", "Public/EditorPixelStreamingSettings.h" },
		{ "ToolTip", "Pixel Streaming may be running on a machine which has no physical mouse\nattached, and yet the browser is sending mouse positions. As such, we\nfake the presence of a mouse." },
	};
#endif
	void Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached_SetBit(void* Obj)
	{
		((UPixelStreamingSettings*)Obj)->bPixelStreamerMouseAlwaysAttached = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached = { "bPixelStreamerMouseAlwaysAttached", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPixelStreamingSettings), &Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPixelStreamingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerDefaultCursorClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerTextEditBeamCursorClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_PixelStreamerHiddenCursorClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPixelStreamingSettings_Statics::NewProp_bPixelStreamerMouseAlwaysAttached,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPixelStreamingSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPixelStreamingSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPixelStreamingSettings_Statics::ClassParams = {
		&UPixelStreamingSettings::StaticClass,
		"PixelStreaming",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPixelStreamingSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UPixelStreamingSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPixelStreamingSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPixelStreamingSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPixelStreamingSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPixelStreamingSettings, 4056665257);
	template<> PIXELSTREAMING_API UClass* StaticClass<UPixelStreamingSettings>()
	{
		return UPixelStreamingSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPixelStreamingSettings(Z_Construct_UClass_UPixelStreamingSettings, &UPixelStreamingSettings::StaticClass, TEXT("/Script/PixelStreaming"), TEXT("UPixelStreamingSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPixelStreamingSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
