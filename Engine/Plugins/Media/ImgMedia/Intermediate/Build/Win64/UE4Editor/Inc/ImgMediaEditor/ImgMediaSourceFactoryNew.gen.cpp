// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImgMediaEditor/Private/Factories/ImgMediaSourceFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImgMediaSourceFactoryNew() {}
// Cross Module References
	IMGMEDIAEDITOR_API UClass* Z_Construct_UClass_UImgMediaSourceFactoryNew_NoRegister();
	IMGMEDIAEDITOR_API UClass* Z_Construct_UClass_UImgMediaSourceFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_ImgMediaEditor();
// End Cross Module References
	void UImgMediaSourceFactoryNew::StaticRegisterNativesUImgMediaSourceFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UImgMediaSourceFactoryNew_NoRegister()
	{
		return UImgMediaSourceFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ImgMediaEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UImgMediaSource objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/ImgMediaSourceFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/ImgMediaSourceFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UImgMediaSource objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImgMediaSourceFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::ClassParams = {
		&UImgMediaSourceFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImgMediaSourceFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImgMediaSourceFactoryNew, 2613119456);
	template<> IMGMEDIAEDITOR_API UClass* StaticClass<UImgMediaSourceFactoryNew>()
	{
		return UImgMediaSourceFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImgMediaSourceFactoryNew(Z_Construct_UClass_UImgMediaSourceFactoryNew, &UImgMediaSourceFactoryNew::StaticClass, TEXT("/Script/ImgMediaEditor"), TEXT("UImgMediaSourceFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImgMediaSourceFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
