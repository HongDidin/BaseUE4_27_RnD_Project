// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMGMEDIAEDITOR_ImgMediaSourceFactoryNew_generated_h
#error "ImgMediaSourceFactoryNew.generated.h already included, missing '#pragma once' in ImgMediaSourceFactoryNew.h"
#endif
#define IMGMEDIAEDITOR_ImgMediaSourceFactoryNew_generated_h

#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_SPARSE_DATA
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_RPC_WRAPPERS
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImgMediaSourceFactoryNew(); \
	friend struct Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UImgMediaSourceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImgMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UImgMediaSourceFactoryNew)


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUImgMediaSourceFactoryNew(); \
	friend struct Z_Construct_UClass_UImgMediaSourceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UImgMediaSourceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImgMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UImgMediaSourceFactoryNew)


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImgMediaSourceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImgMediaSourceFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImgMediaSourceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImgMediaSourceFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImgMediaSourceFactoryNew(UImgMediaSourceFactoryNew&&); \
	NO_API UImgMediaSourceFactoryNew(const UImgMediaSourceFactoryNew&); \
public:


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImgMediaSourceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImgMediaSourceFactoryNew(UImgMediaSourceFactoryNew&&); \
	NO_API UImgMediaSourceFactoryNew(const UImgMediaSourceFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImgMediaSourceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImgMediaSourceFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImgMediaSourceFactoryNew)


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_12_PROLOG
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_SPARSE_DATA \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_RPC_WRAPPERS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_INCLASS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_SPARSE_DATA \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ImgMediaSourceFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMGMEDIAEDITOR_API UClass* StaticClass<class UImgMediaSourceFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
