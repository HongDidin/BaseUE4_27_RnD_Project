// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMGMEDIAEDITOR_ImgMediaSourceFactory_generated_h
#error "ImgMediaSourceFactory.generated.h already included, missing '#pragma once' in ImgMediaSourceFactory.h"
#endif
#define IMGMEDIAEDITOR_ImgMediaSourceFactory_generated_h

#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_SPARSE_DATA
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImgMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UImgMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UImgMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImgMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UImgMediaSourceFactory)


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUImgMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UImgMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UImgMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImgMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UImgMediaSourceFactory)


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImgMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImgMediaSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImgMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImgMediaSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImgMediaSourceFactory(UImgMediaSourceFactory&&); \
	NO_API UImgMediaSourceFactory(const UImgMediaSourceFactory&); \
public:


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImgMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImgMediaSourceFactory(UImgMediaSourceFactory&&); \
	NO_API UImgMediaSourceFactory(const UImgMediaSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImgMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImgMediaSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImgMediaSourceFactory)


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_13_PROLOG
#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_INCLASS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ImgMediaSourceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMGMEDIAEDITOR_API UClass* StaticClass<class UImgMediaSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_ImgMedia_Source_ImgMediaEditor_Private_Factories_ImgMediaSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
