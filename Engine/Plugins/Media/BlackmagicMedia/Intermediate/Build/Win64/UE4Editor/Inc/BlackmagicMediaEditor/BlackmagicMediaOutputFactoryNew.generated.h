// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLACKMAGICMEDIAEDITOR_BlackmagicMediaOutputFactoryNew_generated_h
#error "BlackmagicMediaOutputFactoryNew.generated.h already included, missing '#pragma once' in BlackmagicMediaOutputFactoryNew.h"
#endif
#define BLACKMAGICMEDIAEDITOR_BlackmagicMediaOutputFactoryNew_generated_h

#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_SPARSE_DATA
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_RPC_WRAPPERS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaOutputFactoryNew(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaOutputFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaOutputFactoryNew)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaOutputFactoryNew(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaOutputFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaOutputFactoryNew)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaOutputFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaOutputFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaOutputFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaOutputFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaOutputFactoryNew(UBlackmagicMediaOutputFactoryNew&&); \
	NO_API UBlackmagicMediaOutputFactoryNew(const UBlackmagicMediaOutputFactoryNew&); \
public:


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaOutputFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaOutputFactoryNew(UBlackmagicMediaOutputFactoryNew&&); \
	NO_API UBlackmagicMediaOutputFactoryNew(const UBlackmagicMediaOutputFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaOutputFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaOutputFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaOutputFactoryNew)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_12_PROLOG
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_RPC_WRAPPERS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_INCLASS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BlackmagicMediaOutputFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLACKMAGICMEDIAEDITOR_API UClass* StaticClass<class UBlackmagicMediaOutputFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaEditor_Private_Factories_BlackmagicMediaOutputFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
