// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlackmagicMediaEditor/Private/Factories/BlackmagicMediaOutputFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlackmagicMediaOutputFactoryNew() {}
// Cross Module References
	BLACKMAGICMEDIAEDITOR_API UClass* Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_NoRegister();
	BLACKMAGICMEDIAEDITOR_API UClass* Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_BlackmagicMediaEditor();
// End Cross Module References
	void UBlackmagicMediaOutputFactoryNew::StaticRegisterNativesUBlackmagicMediaOutputFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_NoRegister()
	{
		return UBlackmagicMediaOutputFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_BlackmagicMediaEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/BlackmagicMediaOutputFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/BlackmagicMediaOutputFactoryNew.h" },
		{ "ToolTip", "Implements a factory for objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBlackmagicMediaOutputFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::ClassParams = {
		&UBlackmagicMediaOutputFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBlackmagicMediaOutputFactoryNew, 2340721114);
	template<> BLACKMAGICMEDIAEDITOR_API UClass* StaticClass<UBlackmagicMediaOutputFactoryNew>()
	{
		return UBlackmagicMediaOutputFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBlackmagicMediaOutputFactoryNew(Z_Construct_UClass_UBlackmagicMediaOutputFactoryNew, &UBlackmagicMediaOutputFactoryNew::StaticClass, TEXT("/Script/BlackmagicMediaEditor"), TEXT("UBlackmagicMediaOutputFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBlackmagicMediaOutputFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
