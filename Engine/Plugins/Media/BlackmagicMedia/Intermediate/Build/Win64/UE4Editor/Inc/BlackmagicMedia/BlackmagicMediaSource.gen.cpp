// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlackmagicMedia/Public/BlackmagicMediaSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlackmagicMediaSource() {}
// Cross Module References
	BLACKMAGICMEDIA_API UEnum* Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaAudioChannel();
	UPackage* Z_Construct_UPackage__Script_BlackmagicMedia();
	BLACKMAGICMEDIA_API UEnum* Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaSourceColorFormat();
	BLACKMAGICMEDIA_API UClass* Z_Construct_UClass_UBlackmagicMediaSource_NoRegister();
	BLACKMAGICMEDIA_API UClass* Z_Construct_UClass_UBlackmagicMediaSource();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UTimeSynchronizableMediaSource();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConfiguration();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat();
// End Cross Module References
	static UEnum* EBlackmagicMediaAudioChannel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaAudioChannel, Z_Construct_UPackage__Script_BlackmagicMedia(), TEXT("EBlackmagicMediaAudioChannel"));
		}
		return Singleton;
	}
	template<> BLACKMAGICMEDIA_API UEnum* StaticEnum<EBlackmagicMediaAudioChannel>()
	{
		return EBlackmagicMediaAudioChannel_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBlackmagicMediaAudioChannel(EBlackmagicMediaAudioChannel_StaticEnum, TEXT("/Script/BlackmagicMedia"), TEXT("EBlackmagicMediaAudioChannel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaAudioChannel_Hash() { return 3584846744U; }
	UEnum* Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaAudioChannel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlackmagicMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBlackmagicMediaAudioChannel"), 0, Get_Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaAudioChannel_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBlackmagicMediaAudioChannel::Stereo2", (int64)EBlackmagicMediaAudioChannel::Stereo2 },
				{ "EBlackmagicMediaAudioChannel::Surround8", (int64)EBlackmagicMediaAudioChannel::Surround8 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * Available number of audio channel supported by UE4 & Capture card.\n */" },
				{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
				{ "Stereo2.Name", "EBlackmagicMediaAudioChannel::Stereo2" },
				{ "Surround8.Name", "EBlackmagicMediaAudioChannel::Surround8" },
				{ "ToolTip", "Available number of audio channel supported by UE4 & Capture card." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlackmagicMedia,
				nullptr,
				"EBlackmagicMediaAudioChannel",
				"EBlackmagicMediaAudioChannel",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EBlackmagicMediaSourceColorFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaSourceColorFormat, Z_Construct_UPackage__Script_BlackmagicMedia(), TEXT("EBlackmagicMediaSourceColorFormat"));
		}
		return Singleton;
	}
	template<> BLACKMAGICMEDIA_API UEnum* StaticEnum<EBlackmagicMediaSourceColorFormat>()
	{
		return EBlackmagicMediaSourceColorFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBlackmagicMediaSourceColorFormat(EBlackmagicMediaSourceColorFormat_StaticEnum, TEXT("/Script/BlackmagicMedia"), TEXT("EBlackmagicMediaSourceColorFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaSourceColorFormat_Hash() { return 1323586443U; }
	UEnum* Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaSourceColorFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlackmagicMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBlackmagicMediaSourceColorFormat"), 0, Get_Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaSourceColorFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBlackmagicMediaSourceColorFormat::YUV8", (int64)EBlackmagicMediaSourceColorFormat::YUV8 },
				{ "EBlackmagicMediaSourceColorFormat::YUV10", (int64)EBlackmagicMediaSourceColorFormat::YUV10 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Native data format.\n */" },
				{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
				{ "ToolTip", "Native data format." },
				{ "YUV10.DisplayName", "10bit YUV" },
				{ "YUV10.Name", "EBlackmagicMediaSourceColorFormat::YUV10" },
				{ "YUV8.DisplayName", "8bit YUV" },
				{ "YUV8.Name", "EBlackmagicMediaSourceColorFormat::YUV8" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlackmagicMedia,
				nullptr,
				"EBlackmagicMediaSourceColorFormat",
				"EBlackmagicMediaSourceColorFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBlackmagicMediaSource::StaticRegisterNativesUBlackmagicMediaSource()
	{
	}
	UClass* Z_Construct_UClass_UBlackmagicMediaSource_NoRegister()
	{
		return UBlackmagicMediaSource::StaticClass();
	}
	struct Z_Construct_UClass_UBlackmagicMediaSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConfiguration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimecodeFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TimecodeFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureAudio_MetaData[];
#endif
		static void NewProp_bCaptureAudio_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureAudio;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AudioChannels_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AudioChannels;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumAudioFrameBuffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumAudioFrameBuffer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureVideo_MetaData[];
#endif
		static void NewProp_bCaptureVideo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureVideo;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSRGBInput_MetaData[];
#endif
		static void NewProp_bIsSRGBInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSRGBInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumVideoFrameBuffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumVideoFrameBuffer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLogDropFrame_MetaData[];
#endif
		static void NewProp_bLogDropFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLogDropFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncodeTimecodeInTexel_MetaData[];
#endif
		static void NewProp_bEncodeTimecodeInTexel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncodeTimecodeInTexel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBlackmagicMediaSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimeSynchronizableMediaSource,
		(UObject* (*)())Z_Construct_UPackage__Script_BlackmagicMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Media source description for Blackmagic.\n */" },
		{ "HideCategories", "Platforms Object Object Object" },
		{ "IncludePath", "BlackmagicMediaSource.h" },
		{ "MediaIOCustomLayout", "Blackmagic" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Media source description for Blackmagic." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MediaConfiguration_MetaData[] = {
		{ "Category", "Blackmagic" },
		{ "Comment", "/** The device, port and video settings that correspond to the input. */" },
		{ "DisplayName", "Configuration" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "The device, port and video settings that correspond to the input." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MediaConfiguration = { "MediaConfiguration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaSource, MediaConfiguration), Z_Construct_UScriptStruct_FMediaIOConfiguration, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MediaConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MediaConfiguration_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat_MetaData[] = {
		{ "Category", "Blackmagic" },
		{ "Comment", "/** Use the time code embedded in the input stream. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Use the time code embedded in the input stream." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat = { "TimecodeFormat", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaSource, TimecodeFormat), Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Capture Audio from the Blackmagic source. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Capture Audio from the Blackmagic source." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio_SetBit(void* Obj)
	{
		((UBlackmagicMediaSource*)Obj)->bCaptureAudio = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio = { "bCaptureAudio", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaSource), &Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Desired number of audio channel to capture. */" },
		{ "EditCondition", "bCaptureAudio" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Desired number of audio channel to capture." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels = { "AudioChannels", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaSource, AudioChannels), Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaAudioChannel, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer_MetaData[] = {
		{ "Category", "Audio" },
		{ "ClampMax", "32" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum number of audio frames to buffer. */" },
		{ "EditCondition", "bCaptureAudio" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Maximum number of audio frames to buffer." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer = { "MaxNumAudioFrameBuffer", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaSource, MaxNumAudioFrameBuffer), METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo_MetaData[] = {
		{ "Category", "Video" },
		{ "Comment", "/** Capture Video from the Blackmagic source. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Capture Video from the Blackmagic source." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo_SetBit(void* Obj)
	{
		((UBlackmagicMediaSource*)Obj)->bCaptureVideo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo = { "bCaptureVideo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaSource), &Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat_MetaData[] = {
		{ "Category", "Video" },
		{ "Comment", "/** Native data format internally used by the device after being converted from SDI/HDMI signal. */" },
		{ "EditCondition", "bCaptureVideo" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Native data format internally used by the device after being converted from SDI/HDMI signal." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat = { "ColorFormat", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaSource, ColorFormat), Z_Construct_UEnum_BlackmagicMedia_EBlackmagicMediaSourceColorFormat, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput_MetaData[] = {
		{ "Category", "Video" },
		{ "Comment", "/**\n\x09 * Whether the video input is in sRGB color space.\n\x09 * A sRGB to Linear conversion will be applied resulting in a texture in linear space.\n\x09 * @Note If the texture is not in linear space, it won't look correct in the editor. Another pass will be required either through Composure or other means.\n\x09 */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Whether the video input is in sRGB color space.\nA sRGB to Linear conversion will be applied resulting in a texture in linear space.\n@Note If the texture is not in linear space, it won't look correct in the editor. Another pass will be required either through Composure or other means." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput_SetBit(void* Obj)
	{
		((UBlackmagicMediaSource*)Obj)->bIsSRGBInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput = { "bIsSRGBInput", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaSource), &Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer_MetaData[] = {
		{ "Category", "Video" },
		{ "ClampMax", "32" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum number of video frames to buffer. */" },
		{ "EditCondition", "bCaptureVideo" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Maximum number of video frames to buffer." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer = { "MaxNumVideoFrameBuffer", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaSource, MaxNumVideoFrameBuffer), METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/** Log a warning when there's a drop frame. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Log a warning when there's a drop frame." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame_SetBit(void* Obj)
	{
		((UBlackmagicMediaSource*)Obj)->bLogDropFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame = { "bLogDropFrame", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaSource), &Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/**\n\x09 * Burn Frame Timecode in the input texture without any frame number clipping.\n\x09 * @Note Only supported in progressive format.\n\x09 */" },
		{ "DisplayName", "Burn Frame Timecode" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaSource.h" },
		{ "ToolTip", "Burn Frame Timecode in the input texture without any frame number clipping.\n@Note Only supported in progressive format." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_SetBit(void* Obj)
	{
		((UBlackmagicMediaSource*)Obj)->bEncodeTimecodeInTexel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel = { "bEncodeTimecodeInTexel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaSource), &Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBlackmagicMediaSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MediaConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_TimecodeFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureAudio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_AudioChannels,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bCaptureVideo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_ColorFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bIsSRGBInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bLogDropFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaSource_Statics::NewProp_bEncodeTimecodeInTexel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBlackmagicMediaSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBlackmagicMediaSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBlackmagicMediaSource_Statics::ClassParams = {
		&UBlackmagicMediaSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBlackmagicMediaSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBlackmagicMediaSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBlackmagicMediaSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBlackmagicMediaSource, 3689001028);
	template<> BLACKMAGICMEDIA_API UClass* StaticClass<UBlackmagicMediaSource>()
	{
		return UBlackmagicMediaSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBlackmagicMediaSource(Z_Construct_UClass_UBlackmagicMediaSource, &UBlackmagicMediaSource::StaticClass, TEXT("/Script/BlackmagicMedia"), TEXT("UBlackmagicMediaSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBlackmagicMediaSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
