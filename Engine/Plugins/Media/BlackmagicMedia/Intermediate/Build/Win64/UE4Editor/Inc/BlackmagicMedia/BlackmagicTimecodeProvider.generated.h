// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLACKMAGICMEDIA_BlackmagicTimecodeProvider_generated_h
#error "BlackmagicTimecodeProvider.generated.h already included, missing '#pragma once' in BlackmagicTimecodeProvider.h"
#endif
#define BLACKMAGICMEDIA_BlackmagicTimecodeProvider_generated_h

#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_SPARSE_DATA
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_RPC_WRAPPERS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlackmagicTimecodeProvider(); \
	friend struct Z_Construct_UClass_UBlackmagicTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicTimecodeProvider, UGenlockedTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMedia"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicTimecodeProvider)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUBlackmagicTimecodeProvider(); \
	friend struct Z_Construct_UClass_UBlackmagicTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicTimecodeProvider, UGenlockedTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMedia"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicTimecodeProvider)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicTimecodeProvider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicTimecodeProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicTimecodeProvider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicTimecodeProvider(UBlackmagicTimecodeProvider&&); \
	NO_API UBlackmagicTimecodeProvider(const UBlackmagicTimecodeProvider&); \
public:


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicTimecodeProvider(UBlackmagicTimecodeProvider&&); \
	NO_API UBlackmagicTimecodeProvider(const UBlackmagicTimecodeProvider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicTimecodeProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicTimecodeProvider); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicTimecodeProvider)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_19_PROLOG
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_RPC_WRAPPERS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_INCLASS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BlackmagicTimecodeProvider."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLACKMAGICMEDIA_API UClass* StaticClass<class UBlackmagicTimecodeProvider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicTimecodeProvider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
