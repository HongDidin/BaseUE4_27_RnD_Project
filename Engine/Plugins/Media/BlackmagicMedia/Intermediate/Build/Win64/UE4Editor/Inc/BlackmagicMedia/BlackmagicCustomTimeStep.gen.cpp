// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlackmagicMedia/Public/BlackmagicCustomTimeStep.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlackmagicCustomTimeStep() {}
// Cross Module References
	BLACKMAGICMEDIA_API UClass* Z_Construct_UClass_UBlackmagicCustomTimeStep_NoRegister();
	BLACKMAGICMEDIA_API UClass* Z_Construct_UClass_UBlackmagicCustomTimeStep();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UGenlockedCustomTimeStep();
	UPackage* Z_Construct_UPackage__Script_BlackmagicMedia();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConfiguration();
// End Cross Module References
	void UBlackmagicCustomTimeStep::StaticRegisterNativesUBlackmagicCustomTimeStep()
	{
	}
	UClass* Z_Construct_UClass_UBlackmagicCustomTimeStep_NoRegister()
	{
		return UBlackmagicCustomTimeStep::StaticClass();
	}
	struct Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConfiguration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableOverrunDetection_MetaData[];
#endif
		static void NewProp_bEnableOverrunDetection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableOverrunDetection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGenlockedCustomTimeStep,
		(UObject* (*)())Z_Construct_UPackage__Script_BlackmagicMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Control the Engine TimeStep via the Blackmagic Design card.\n */" },
		{ "DisplayName", "Blackmagic SDI Input" },
		{ "IncludePath", "BlackmagicCustomTimeStep.h" },
		{ "IsBlueprintBase", "true" },
		{ "MediaIOCustomLayout", "Blackmagic" },
		{ "ModuleRelativePath", "Public/BlackmagicCustomTimeStep.h" },
		{ "ToolTip", "Control the Engine TimeStep via the Blackmagic Design card." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_MediaConfiguration_MetaData[] = {
		{ "Category", "Genlock" },
		{ "Comment", "/** The device, port and video settings that correspond to where the Genlock signal will be coming from */" },
		{ "DisplayName", "Configuration" },
		{ "ModuleRelativePath", "Public/BlackmagicCustomTimeStep.h" },
		{ "ToolTip", "The device, port and video settings that correspond to where the Genlock signal will be coming from" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_MediaConfiguration = { "MediaConfiguration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicCustomTimeStep, MediaConfiguration), Z_Construct_UScriptStruct_FMediaIOConfiguration, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_MediaConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_MediaConfiguration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection_MetaData[] = {
		{ "Category", "Genlock options" },
		{ "Comment", "/** Enable mechanism to detect Engine loop overrunning the source */" },
		{ "DisplayName", "Display Dropped Frames Warning" },
		{ "ModuleRelativePath", "Public/BlackmagicCustomTimeStep.h" },
		{ "ToolTip", "Enable mechanism to detect Engine loop overrunning the source" },
	};
#endif
	void Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection_SetBit(void* Obj)
	{
		((UBlackmagicCustomTimeStep*)Obj)->bEnableOverrunDetection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection = { "bEnableOverrunDetection", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicCustomTimeStep), &Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_MediaConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::NewProp_bEnableOverrunDetection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBlackmagicCustomTimeStep>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::ClassParams = {
		&UBlackmagicCustomTimeStep::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBlackmagicCustomTimeStep()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBlackmagicCustomTimeStep, 3253700781);
	template<> BLACKMAGICMEDIA_API UClass* StaticClass<UBlackmagicCustomTimeStep>()
	{
		return UBlackmagicCustomTimeStep::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBlackmagicCustomTimeStep(Z_Construct_UClass_UBlackmagicCustomTimeStep, &UBlackmagicCustomTimeStep::StaticClass, TEXT("/Script/BlackmagicMedia"), TEXT("UBlackmagicCustomTimeStep"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBlackmagicCustomTimeStep);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
