// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLACKMAGICMEDIA_BlackmagicCustomTimeStep_generated_h
#error "BlackmagicCustomTimeStep.generated.h already included, missing '#pragma once' in BlackmagicCustomTimeStep.h"
#endif
#define BLACKMAGICMEDIA_BlackmagicCustomTimeStep_generated_h

#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_SPARSE_DATA
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_RPC_WRAPPERS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlackmagicCustomTimeStep(); \
	friend struct Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicCustomTimeStep, UGenlockedCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMedia"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicCustomTimeStep)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUBlackmagicCustomTimeStep(); \
	friend struct Z_Construct_UClass_UBlackmagicCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicCustomTimeStep, UGenlockedCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMedia"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicCustomTimeStep)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicCustomTimeStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicCustomTimeStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicCustomTimeStep(UBlackmagicCustomTimeStep&&); \
	NO_API UBlackmagicCustomTimeStep(const UBlackmagicCustomTimeStep&); \
public:


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicCustomTimeStep(UBlackmagicCustomTimeStep&&); \
	NO_API UBlackmagicCustomTimeStep(const UBlackmagicCustomTimeStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicCustomTimeStep); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicCustomTimeStep)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_20_PROLOG
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_RPC_WRAPPERS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_INCLASS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h_23_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BlackmagicCustomTimeStep."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLACKMAGICMEDIA_API UClass* StaticClass<class UBlackmagicCustomTimeStep>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicCustomTimeStep_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
