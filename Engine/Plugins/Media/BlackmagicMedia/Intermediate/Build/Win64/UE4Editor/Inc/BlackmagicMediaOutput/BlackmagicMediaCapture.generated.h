// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLACKMAGICMEDIAOUTPUT_BlackmagicMediaCapture_generated_h
#error "BlackmagicMediaCapture.generated.h already included, missing '#pragma once' in BlackmagicMediaCapture.h"
#endif
#define BLACKMAGICMEDIAOUTPUT_BlackmagicMediaCapture_generated_h

#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_SPARSE_DATA
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_RPC_WRAPPERS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaCapture(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaCapture_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaCapture)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaCapture(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaCapture_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaCapture)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaCapture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaCapture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaCapture(UBlackmagicMediaCapture&&); \
	NO_API UBlackmagicMediaCapture(const UBlackmagicMediaCapture&); \
public:


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaCapture(UBlackmagicMediaCapture&&); \
	NO_API UBlackmagicMediaCapture(const UBlackmagicMediaCapture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaCapture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaCapture)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_25_PROLOG
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_RPC_WRAPPERS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_INCLASS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h_28_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BlackmagicMediaCapture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLACKMAGICMEDIAOUTPUT_API UClass* StaticClass<class UBlackmagicMediaCapture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaCapture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
