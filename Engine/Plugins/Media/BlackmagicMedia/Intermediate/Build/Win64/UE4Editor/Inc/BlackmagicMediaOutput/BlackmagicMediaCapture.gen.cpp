// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlackmagicMediaOutput/Public/BlackmagicMediaCapture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlackmagicMediaCapture() {}
// Cross Module References
	BLACKMAGICMEDIAOUTPUT_API UClass* Z_Construct_UClass_UBlackmagicMediaCapture_NoRegister();
	BLACKMAGICMEDIAOUTPUT_API UClass* Z_Construct_UClass_UBlackmagicMediaCapture();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture();
	UPackage* Z_Construct_UPackage__Script_BlackmagicMediaOutput();
// End Cross Module References
	void UBlackmagicMediaCapture::StaticRegisterNativesUBlackmagicMediaCapture()
	{
	}
	UClass* Z_Construct_UClass_UBlackmagicMediaCapture_NoRegister()
	{
		return UBlackmagicMediaCapture::StaticClass();
	}
	struct Z_Construct_UClass_UBlackmagicMediaCapture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBlackmagicMediaCapture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaCapture,
		(UObject* (*)())Z_Construct_UPackage__Script_BlackmagicMediaOutput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaCapture_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Output Media for Blackmagic streams.\n * The output format could be any of EBlackmagicMediaOutputPixelFormat.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "BlackmagicMediaCapture.h" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaCapture.h" },
		{ "ToolTip", "Output Media for Blackmagic streams.\nThe output format could be any of EBlackmagicMediaOutputPixelFormat." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBlackmagicMediaCapture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBlackmagicMediaCapture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBlackmagicMediaCapture_Statics::ClassParams = {
		&UBlackmagicMediaCapture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaCapture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaCapture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBlackmagicMediaCapture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBlackmagicMediaCapture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBlackmagicMediaCapture, 298516313);
	template<> BLACKMAGICMEDIAOUTPUT_API UClass* StaticClass<UBlackmagicMediaCapture>()
	{
		return UBlackmagicMediaCapture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBlackmagicMediaCapture(Z_Construct_UClass_UBlackmagicMediaCapture, &UBlackmagicMediaCapture::StaticClass, TEXT("/Script/BlackmagicMediaOutput"), TEXT("UBlackmagicMediaCapture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBlackmagicMediaCapture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
