// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLACKMAGICMEDIA_BlackmagicMediaSource_generated_h
#error "BlackmagicMediaSource.generated.h already included, missing '#pragma once' in BlackmagicMediaSource.h"
#endif
#define BLACKMAGICMEDIA_BlackmagicMediaSource_generated_h

#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_SPARSE_DATA
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_RPC_WRAPPERS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaSource(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaSource_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaSource, UTimeSynchronizableMediaSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMedia"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaSource)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_INCLASS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaSource(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaSource_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaSource, UTimeSynchronizableMediaSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMedia"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaSource)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaSource(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaSource(UBlackmagicMediaSource&&); \
	NO_API UBlackmagicMediaSource(const UBlackmagicMediaSource&); \
public:


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaSource(UBlackmagicMediaSource&&); \
	NO_API UBlackmagicMediaSource(const UBlackmagicMediaSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaSource); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBlackmagicMediaSource)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_36_PROLOG
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_RPC_WRAPPERS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_INCLASS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLACKMAGICMEDIA_API UClass* StaticClass<class UBlackmagicMediaSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMedia_Public_BlackmagicMediaSource_h


#define FOREACH_ENUM_EBLACKMAGICMEDIAAUDIOCHANNEL(op) \
	op(EBlackmagicMediaAudioChannel::Stereo2) \
	op(EBlackmagicMediaAudioChannel::Surround8) 

enum class EBlackmagicMediaAudioChannel : uint8;
template<> BLACKMAGICMEDIA_API UEnum* StaticEnum<EBlackmagicMediaAudioChannel>();

#define FOREACH_ENUM_EBLACKMAGICMEDIASOURCECOLORFORMAT(op) \
	op(EBlackmagicMediaSourceColorFormat::YUV8) \
	op(EBlackmagicMediaSourceColorFormat::YUV10) 

enum class EBlackmagicMediaSourceColorFormat : uint8;
template<> BLACKMAGICMEDIA_API UEnum* StaticEnum<EBlackmagicMediaSourceColorFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
