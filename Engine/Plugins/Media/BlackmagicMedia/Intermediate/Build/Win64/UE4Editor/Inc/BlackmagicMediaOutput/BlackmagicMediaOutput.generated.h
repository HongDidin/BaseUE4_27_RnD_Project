// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BLACKMAGICMEDIAOUTPUT_BlackmagicMediaOutput_generated_h
#error "BlackmagicMediaOutput.generated.h already included, missing '#pragma once' in BlackmagicMediaOutput.h"
#endif
#define BLACKMAGICMEDIAOUTPUT_BlackmagicMediaOutput_generated_h

#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_SPARSE_DATA
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_RPC_WRAPPERS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaOutput(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaOutput)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUBlackmagicMediaOutput(); \
	friend struct Z_Construct_UClass_UBlackmagicMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UBlackmagicMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BlackmagicMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UBlackmagicMediaOutput)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaOutput(UBlackmagicMediaOutput&&); \
	NO_API UBlackmagicMediaOutput(const UBlackmagicMediaOutput&); \
public:


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlackmagicMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlackmagicMediaOutput(UBlackmagicMediaOutput&&); \
	NO_API UBlackmagicMediaOutput(const UBlackmagicMediaOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlackmagicMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlackmagicMediaOutput); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlackmagicMediaOutput)


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_27_PROLOG
#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_RPC_WRAPPERS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_INCLASS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_SPARSE_DATA \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h_30_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BlackmagicMediaOutput."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BLACKMAGICMEDIAOUTPUT_API UClass* StaticClass<class UBlackmagicMediaOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BlackmagicMedia_Source_BlackmagicMediaOutput_Public_BlackmagicMediaOutput_h


#define FOREACH_ENUM_EBLACKMAGICMEDIAOUTPUTPIXELFORMAT(op) \
	op(EBlackmagicMediaOutputPixelFormat::PF_8BIT_YUV) \
	op(EBlackmagicMediaOutputPixelFormat::PF_10BIT_YUV) 

enum class EBlackmagicMediaOutputPixelFormat : uint8;
template<> BLACKMAGICMEDIAOUTPUT_API UEnum* StaticEnum<EBlackmagicMediaOutputPixelFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
