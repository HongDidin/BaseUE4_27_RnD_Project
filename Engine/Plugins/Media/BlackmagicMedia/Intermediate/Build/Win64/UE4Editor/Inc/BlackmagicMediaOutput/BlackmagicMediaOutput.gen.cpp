// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BlackmagicMediaOutput/Public/BlackmagicMediaOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlackmagicMediaOutput() {}
// Cross Module References
	BLACKMAGICMEDIAOUTPUT_API UEnum* Z_Construct_UEnum_BlackmagicMediaOutput_EBlackmagicMediaOutputPixelFormat();
	UPackage* Z_Construct_UPackage__Script_BlackmagicMediaOutput();
	BLACKMAGICMEDIAOUTPUT_API UClass* Z_Construct_UClass_UBlackmagicMediaOutput_NoRegister();
	BLACKMAGICMEDIAOUTPUT_API UClass* Z_Construct_UClass_UBlackmagicMediaOutput();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOOutputConfiguration();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat();
// End Cross Module References
	static UEnum* EBlackmagicMediaOutputPixelFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_BlackmagicMediaOutput_EBlackmagicMediaOutputPixelFormat, Z_Construct_UPackage__Script_BlackmagicMediaOutput(), TEXT("EBlackmagicMediaOutputPixelFormat"));
		}
		return Singleton;
	}
	template<> BLACKMAGICMEDIAOUTPUT_API UEnum* StaticEnum<EBlackmagicMediaOutputPixelFormat>()
	{
		return EBlackmagicMediaOutputPixelFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBlackmagicMediaOutputPixelFormat(EBlackmagicMediaOutputPixelFormat_StaticEnum, TEXT("/Script/BlackmagicMediaOutput"), TEXT("EBlackmagicMediaOutputPixelFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_BlackmagicMediaOutput_EBlackmagicMediaOutputPixelFormat_Hash() { return 2607229372U; }
	UEnum* Z_Construct_UEnum_BlackmagicMediaOutput_EBlackmagicMediaOutputPixelFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_BlackmagicMediaOutput();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBlackmagicMediaOutputPixelFormat"), 0, Get_Z_Construct_UEnum_BlackmagicMediaOutput_EBlackmagicMediaOutputPixelFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBlackmagicMediaOutputPixelFormat::PF_8BIT_YUV", (int64)EBlackmagicMediaOutputPixelFormat::PF_8BIT_YUV },
				{ "EBlackmagicMediaOutputPixelFormat::PF_10BIT_YUV", (int64)EBlackmagicMediaOutputPixelFormat::PF_10BIT_YUV },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Native data format.\n */" },
				{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
				{ "PF_10BIT_YUV.DisplayName", "10bit YUV" },
				{ "PF_10BIT_YUV.Name", "EBlackmagicMediaOutputPixelFormat::PF_10BIT_YUV" },
				{ "PF_8BIT_YUV.DisplayName", "8bit YUV" },
				{ "PF_8BIT_YUV.Name", "EBlackmagicMediaOutputPixelFormat::PF_8BIT_YUV" },
				{ "ToolTip", "Native data format." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_BlackmagicMediaOutput,
				nullptr,
				"EBlackmagicMediaOutputPixelFormat",
				"EBlackmagicMediaOutputPixelFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UBlackmagicMediaOutput::StaticRegisterNativesUBlackmagicMediaOutput()
	{
	}
	UClass* Z_Construct_UClass_UBlackmagicMediaOutput_NoRegister()
	{
		return UBlackmagicMediaOutput::StaticClass();
	}
	struct Z_Construct_UClass_UBlackmagicMediaOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputConfiguration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimecodeFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TimecodeFormat;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PixelFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PixelFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertKeyOutput_MetaData[];
#endif
		static void NewProp_bInvertKeyOutput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertKeyOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfBlackmagicBuffers_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfBlackmagicBuffers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInterlacedFieldsTimecodeNeedToMatch_MetaData[];
#endif
		static void NewProp_bInterlacedFieldsTimecodeNeedToMatch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInterlacedFieldsTimecodeNeedToMatch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWaitForSyncEvent_MetaData[];
#endif
		static void NewProp_bWaitForSyncEvent_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWaitForSyncEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLogDropFrame_MetaData[];
#endif
		static void NewProp_bLogDropFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLogDropFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncodeTimecodeInTexel_MetaData[];
#endif
		static void NewProp_bEncodeTimecodeInTexel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncodeTimecodeInTexel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBlackmagicMediaOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_BlackmagicMediaOutput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Output information for a MediaCapture.\n * @note\x09'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha to enabled the Key.\n * @note\x09'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper' to enabled the Key.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "BlackmagicMediaOutput.h" },
		{ "MediaIOCustomLayout", "Blackmagic" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Output information for a MediaCapture.\n@note       'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha to enabled the Key.\n@note       'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper' to enabled the Key." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_OutputConfiguration_MetaData[] = {
		{ "Category", "Blackmagic" },
		{ "Comment", "/** The device, port and video settings that correspond to the output. */" },
		{ "DisplayName", "Configuration" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "The device, port and video settings that correspond to the output." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_OutputConfiguration = { "OutputConfiguration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaOutput, OutputConfiguration), Z_Construct_UScriptStruct_FMediaIOOutputConfiguration, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_OutputConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_OutputConfiguration_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "/** Whether to embed the Engine's timecode to the output frame. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Whether to embed the Engine's timecode to the output frame." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat = { "TimecodeFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaOutput, TimecodeFormat), Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "/** Native data format internally used by the device before being converted to SDI/HDMI signal. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Native data format internally used by the device before being converted to SDI/HDMI signal." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat = { "PixelFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaOutput, PixelFormat), Z_Construct_UEnum_BlackmagicMediaOutput_EBlackmagicMediaOutputPixelFormat, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "/** Invert Key Output */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Invert Key Output" },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput_SetBit(void* Obj)
	{
		((UBlackmagicMediaOutput*)Obj)->bInvertKeyOutput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput = { "bInvertKeyOutput", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaOutput), &Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_NumberOfBlackmagicBuffers_MetaData[] = {
		{ "Category", "Output" },
		{ "ClampMax", "4" },
		{ "ClampMin", "3" },
		{ "Comment", "/**\n\x09 * Number of frame used to transfer from the system memory to the Blackmagic card.\n\x09 * A smaller number is most likely to cause missed frame.\n\x09 * A bigger number is most likely to increase latency.\n\x09 */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Number of frame used to transfer from the system memory to the Blackmagic card.\nA smaller number is most likely to cause missed frame.\nA bigger number is most likely to increase latency." },
		{ "UIMax", "4" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_NumberOfBlackmagicBuffers = { "NumberOfBlackmagicBuffers", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBlackmagicMediaOutput, NumberOfBlackmagicBuffers), METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_NumberOfBlackmagicBuffers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_NumberOfBlackmagicBuffers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "/**\n\x09 * Only make sense in interlaced mode.\n\x09 * When creating a new Frame the 2 fields need to have the same timecode value.\n\x09 * The Engine's need a TimecodeProvider (or the default system clock) that is in sync with the generated fields.\n\x09 */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Only make sense in interlaced mode.\nWhen creating a new Frame the 2 fields need to have the same timecode value.\nThe Engine's need a TimecodeProvider (or the default system clock) that is in sync with the generated fields." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch_SetBit(void* Obj)
	{
		((UBlackmagicMediaOutput*)Obj)->bInterlacedFieldsTimecodeNeedToMatch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch = { "bInterlacedFieldsTimecodeNeedToMatch", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaOutput), &Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "Comment", "/** Try to maintain a the engine \"Genlock\" with the VSync signal. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Try to maintain a the engine \"Genlock\" with the VSync signal." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent_SetBit(void* Obj)
	{
		((UBlackmagicMediaOutput*)Obj)->bWaitForSyncEvent = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent = { "bWaitForSyncEvent", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaOutput), &Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/** Log a warning when there's a drop frame. */" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Log a warning when there's a drop frame." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame_SetBit(void* Obj)
	{
		((UBlackmagicMediaOutput*)Obj)->bLogDropFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame = { "bLogDropFrame", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaOutput), &Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/** Burn Frame Timecode on the output without any frame number clipping. */" },
		{ "DisplayName", "Burn Frame Timecode" },
		{ "ModuleRelativePath", "Public/BlackmagicMediaOutput.h" },
		{ "ToolTip", "Burn Frame Timecode on the output without any frame number clipping." },
	};
#endif
	void Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel_SetBit(void* Obj)
	{
		((UBlackmagicMediaOutput*)Obj)->bEncodeTimecodeInTexel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel = { "bEncodeTimecodeInTexel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBlackmagicMediaOutput), &Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBlackmagicMediaOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_OutputConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_TimecodeFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_PixelFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInvertKeyOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_NumberOfBlackmagicBuffers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bInterlacedFieldsTimecodeNeedToMatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bWaitForSyncEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bLogDropFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBlackmagicMediaOutput_Statics::NewProp_bEncodeTimecodeInTexel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBlackmagicMediaOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBlackmagicMediaOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBlackmagicMediaOutput_Statics::ClassParams = {
		&UBlackmagicMediaOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBlackmagicMediaOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBlackmagicMediaOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBlackmagicMediaOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBlackmagicMediaOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBlackmagicMediaOutput, 2827857007);
	template<> BLACKMAGICMEDIAOUTPUT_API UClass* StaticClass<UBlackmagicMediaOutput>()
	{
		return UBlackmagicMediaOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBlackmagicMediaOutput(Z_Construct_UClass_UBlackmagicMediaOutput, &UBlackmagicMediaOutput::StaticClass, TEXT("/Script/BlackmagicMediaOutput"), TEXT("UBlackmagicMediaOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBlackmagicMediaOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
