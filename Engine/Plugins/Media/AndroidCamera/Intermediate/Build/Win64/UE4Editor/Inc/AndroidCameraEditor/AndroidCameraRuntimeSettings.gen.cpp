// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AndroidCameraEditor/Public/AndroidCameraRuntimeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAndroidCameraRuntimeSettings() {}
// Cross Module References
	ANDROIDCAMERAEDITOR_API UClass* Z_Construct_UClass_UAndroidCameraRuntimeSettings_NoRegister();
	ANDROIDCAMERAEDITOR_API UClass* Z_Construct_UClass_UAndroidCameraRuntimeSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_AndroidCameraEditor();
// End Cross Module References
	void UAndroidCameraRuntimeSettings::StaticRegisterNativesUAndroidCameraRuntimeSettings()
	{
	}
	UClass* Z_Construct_UClass_UAndroidCameraRuntimeSettings_NoRegister()
	{
		return UAndroidCameraRuntimeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnablePermission_MetaData[];
#endif
		static void NewProp_bEnablePermission_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnablePermission;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRequiresAnyCamera_MetaData[];
#endif
		static void NewProp_bRequiresAnyCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequiresAnyCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRequiresBackFacingCamera_MetaData[];
#endif
		static void NewProp_bRequiresBackFacingCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequiresBackFacingCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRequiresFrontFacingCamera_MetaData[];
#endif
		static void NewProp_bRequiresFrontFacingCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequiresFrontFacingCamera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AndroidCameraEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for the AndroidCamera plugin.\n*/" },
		{ "IncludePath", "AndroidCameraRuntimeSettings.h" },
		{ "ModuleRelativePath", "Public/AndroidCameraRuntimeSettings.h" },
		{ "ToolTip", "Implements the settings for the AndroidCamera plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission_MetaData[] = {
		{ "Category", "ManifestSettings" },
		{ "Comment", "// Enable camera permission in AndroidManifest\n" },
		{ "ModuleRelativePath", "Public/AndroidCameraRuntimeSettings.h" },
		{ "ToolTip", "Enable camera permission in AndroidManifest" },
	};
#endif
	void Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission_SetBit(void* Obj)
	{
		((UAndroidCameraRuntimeSettings*)Obj)->bEnablePermission = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission = { "bEnablePermission", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAndroidCameraRuntimeSettings), &Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera_MetaData[] = {
		{ "Category", "ManifestSettings" },
		{ "Comment", "// Requires a camera to operate (if true and back-facing and front-facing are false, sets android.hardware.camera.any as required)\n" },
		{ "ModuleRelativePath", "Public/AndroidCameraRuntimeSettings.h" },
		{ "ToolTip", "Requires a camera to operate (if true and back-facing and front-facing are false, sets android.hardware.camera.any as required)" },
	};
#endif
	void Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera_SetBit(void* Obj)
	{
		((UAndroidCameraRuntimeSettings*)Obj)->bRequiresAnyCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera = { "bRequiresAnyCamera", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAndroidCameraRuntimeSettings), &Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera_MetaData[] = {
		{ "Category", "ManifestSettings" },
		{ "Comment", "// Requires back-facing camera in AndroidManifest (android.hardware.camera)\n" },
		{ "ModuleRelativePath", "Public/AndroidCameraRuntimeSettings.h" },
		{ "ToolTip", "Requires back-facing camera in AndroidManifest (android.hardware.camera)" },
	};
#endif
	void Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera_SetBit(void* Obj)
	{
		((UAndroidCameraRuntimeSettings*)Obj)->bRequiresBackFacingCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera = { "bRequiresBackFacingCamera", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAndroidCameraRuntimeSettings), &Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera_MetaData[] = {
		{ "Category", "ManifestSettings" },
		{ "Comment", "// Requires front-facing camera in AndroidManifest (android.hardware.camera.front)\n" },
		{ "ModuleRelativePath", "Public/AndroidCameraRuntimeSettings.h" },
		{ "ToolTip", "Requires front-facing camera in AndroidManifest (android.hardware.camera.front)" },
	};
#endif
	void Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera_SetBit(void* Obj)
	{
		((UAndroidCameraRuntimeSettings*)Obj)->bRequiresFrontFacingCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera = { "bRequiresFrontFacingCamera", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAndroidCameraRuntimeSettings), &Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bEnablePermission,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresAnyCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresBackFacingCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::NewProp_bRequiresFrontFacingCamera,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAndroidCameraRuntimeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::ClassParams = {
		&UAndroidCameraRuntimeSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAndroidCameraRuntimeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAndroidCameraRuntimeSettings, 503946716);
	template<> ANDROIDCAMERAEDITOR_API UClass* StaticClass<UAndroidCameraRuntimeSettings>()
	{
		return UAndroidCameraRuntimeSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAndroidCameraRuntimeSettings(Z_Construct_UClass_UAndroidCameraRuntimeSettings, &UAndroidCameraRuntimeSettings::StaticClass, TEXT("/Script/AndroidCameraEditor"), TEXT("UAndroidCameraRuntimeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAndroidCameraRuntimeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
