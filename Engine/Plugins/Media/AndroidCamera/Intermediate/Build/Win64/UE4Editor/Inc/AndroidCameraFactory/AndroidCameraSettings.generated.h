// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANDROIDCAMERAFACTORY_AndroidCameraSettings_generated_h
#error "AndroidCameraSettings.generated.h already included, missing '#pragma once' in AndroidCameraSettings.h"
#endif
#define ANDROIDCAMERAFACTORY_AndroidCameraSettings_generated_h

#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_SPARSE_DATA
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_RPC_WRAPPERS
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAndroidCameraSettings(); \
	friend struct Z_Construct_UClass_UAndroidCameraSettings_Statics; \
public: \
	DECLARE_CLASS(UAndroidCameraSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidCameraFactory"), NO_API) \
	DECLARE_SERIALIZER(UAndroidCameraSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUAndroidCameraSettings(); \
	friend struct Z_Construct_UClass_UAndroidCameraSettings_Statics; \
public: \
	DECLARE_CLASS(UAndroidCameraSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidCameraFactory"), NO_API) \
	DECLARE_SERIALIZER(UAndroidCameraSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAndroidCameraSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAndroidCameraSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidCameraSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidCameraSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidCameraSettings(UAndroidCameraSettings&&); \
	NO_API UAndroidCameraSettings(const UAndroidCameraSettings&); \
public:


#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidCameraSettings(UAndroidCameraSettings&&); \
	NO_API UAndroidCameraSettings(const UAndroidCameraSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidCameraSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidCameraSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAndroidCameraSettings)


#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_15_PROLOG
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_RPC_WRAPPERS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_INCLASS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANDROIDCAMERAFACTORY_API UClass* StaticClass<class UAndroidCameraSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraFactory_Public_AndroidCameraSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
