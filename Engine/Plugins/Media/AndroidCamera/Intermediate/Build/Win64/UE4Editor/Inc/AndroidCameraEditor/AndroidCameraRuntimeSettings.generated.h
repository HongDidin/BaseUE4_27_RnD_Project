// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANDROIDCAMERAEDITOR_AndroidCameraRuntimeSettings_generated_h
#error "AndroidCameraRuntimeSettings.generated.h already included, missing '#pragma once' in AndroidCameraRuntimeSettings.h"
#endif
#define ANDROIDCAMERAEDITOR_AndroidCameraRuntimeSettings_generated_h

#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_SPARSE_DATA
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_RPC_WRAPPERS
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAndroidCameraRuntimeSettings(); \
	friend struct Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UAndroidCameraRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidCameraEditor"), NO_API) \
	DECLARE_SERIALIZER(UAndroidCameraRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUAndroidCameraRuntimeSettings(); \
	friend struct Z_Construct_UClass_UAndroidCameraRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UAndroidCameraRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidCameraEditor"), NO_API) \
	DECLARE_SERIALIZER(UAndroidCameraRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAndroidCameraRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAndroidCameraRuntimeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidCameraRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidCameraRuntimeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidCameraRuntimeSettings(UAndroidCameraRuntimeSettings&&); \
	NO_API UAndroidCameraRuntimeSettings(const UAndroidCameraRuntimeSettings&); \
public:


#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAndroidCameraRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidCameraRuntimeSettings(UAndroidCameraRuntimeSettings&&); \
	NO_API UAndroidCameraRuntimeSettings(const UAndroidCameraRuntimeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidCameraRuntimeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidCameraRuntimeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAndroidCameraRuntimeSettings)


#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_14_PROLOG
#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_SPARSE_DATA \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_INCLASS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_SPARSE_DATA \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AndroidCameraRuntimeSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANDROIDCAMERAEDITOR_API UClass* StaticClass<class UAndroidCameraRuntimeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AndroidCamera_Source_AndroidCameraEditor_Public_AndroidCameraRuntimeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
