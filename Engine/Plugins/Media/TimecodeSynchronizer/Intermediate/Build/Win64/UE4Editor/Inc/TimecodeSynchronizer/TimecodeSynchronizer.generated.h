// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMECODESYNCHRONIZER_TimecodeSynchronizer_generated_h
#error "TimecodeSynchronizer.generated.h already included, missing '#pragma once' in TimecodeSynchronizer.h"
#endif
#define TIMECODESYNCHRONIZER_TimecodeSynchronizer_generated_h

#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_130_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics; \
	TIMECODESYNCHRONIZER_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bIsReady() { return STRUCT_OFFSET(FTimecodeSynchronizerActiveTimecodedInputSource, bIsReady); } \
	FORCEINLINE static uint32 __PPO__bCanBeSynchronized() { return STRUCT_OFFSET(FTimecodeSynchronizerActiveTimecodedInputSource, bCanBeSynchronized); } \
	FORCEINLINE static uint32 __PPO__TotalNumberOfSamples() { return STRUCT_OFFSET(FTimecodeSynchronizerActiveTimecodedInputSource, TotalNumberOfSamples); } \
	FORCEINLINE static uint32 __PPO__InputSource() { return STRUCT_OFFSET(FTimecodeSynchronizerActiveTimecodedInputSource, InputSource); }


template<> TIMECODESYNCHRONIZER_API UScriptStruct* StaticStruct<struct FTimecodeSynchronizerActiveTimecodedInputSource>();

#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_SPARSE_DATA
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_RPC_WRAPPERS
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimecodeSynchronizer(); \
	friend struct Z_Construct_UClass_UTimecodeSynchronizer_Statics; \
public: \
	DECLARE_CLASS(UTimecodeSynchronizer, UTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimecodeSynchronizer"), NO_API) \
	DECLARE_SERIALIZER(UTimecodeSynchronizer)


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_INCLASS \
private: \
	static void StaticRegisterNativesUTimecodeSynchronizer(); \
	friend struct Z_Construct_UClass_UTimecodeSynchronizer_Statics; \
public: \
	DECLARE_CLASS(UTimecodeSynchronizer, UTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TimecodeSynchronizer"), NO_API) \
	DECLARE_SERIALIZER(UTimecodeSynchronizer)


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimecodeSynchronizer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimecodeSynchronizer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimecodeSynchronizer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimecodeSynchronizer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimecodeSynchronizer(UTimecodeSynchronizer&&); \
	NO_API UTimecodeSynchronizer(const UTimecodeSynchronizer&); \
public:


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimecodeSynchronizer(UTimecodeSynchronizer&&); \
	NO_API UTimecodeSynchronizer(const UTimecodeSynchronizer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimecodeSynchronizer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimecodeSynchronizer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTimecodeSynchronizer)


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DynamicSources() { return STRUCT_OFFSET(UTimecodeSynchronizer, DynamicSources); } \
	FORCEINLINE static uint32 __PPO__SyncMode() { return STRUCT_OFFSET(UTimecodeSynchronizer, SyncMode); } \
	FORCEINLINE static uint32 __PPO__FrameOffset() { return STRUCT_OFFSET(UTimecodeSynchronizer, FrameOffset); } \
	FORCEINLINE static uint32 __PPO__AutoFrameOffset() { return STRUCT_OFFSET(UTimecodeSynchronizer, AutoFrameOffset); } \
	FORCEINLINE static uint32 __PPO__bWithRollover() { return STRUCT_OFFSET(UTimecodeSynchronizer, bWithRollover); } \
	FORCEINLINE static uint32 __PPO__SynchronizedSources() { return STRUCT_OFFSET(UTimecodeSynchronizer, SynchronizedSources); } \
	FORCEINLINE static uint32 __PPO__NonSynchronizedSources() { return STRUCT_OFFSET(UTimecodeSynchronizer, NonSynchronizedSources); } \
	FORCEINLINE static uint32 __PPO__RegisteredCustomTimeStep() { return STRUCT_OFFSET(UTimecodeSynchronizer, RegisteredCustomTimeStep); } \
	FORCEINLINE static uint32 __PPO__CachedPreviousTimecodeProvider() { return STRUCT_OFFSET(UTimecodeSynchronizer, CachedPreviousTimecodeProvider); } \
	FORCEINLINE static uint32 __PPO__CachedProxiedTimecodeProvider() { return STRUCT_OFFSET(UTimecodeSynchronizer, CachedProxiedTimecodeProvider); } \
	FORCEINLINE static uint32 __PPO__ActualFrameOffset() { return STRUCT_OFFSET(UTimecodeSynchronizer, ActualFrameOffset); }


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_250_PROLOG
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_SPARSE_DATA \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_RPC_WRAPPERS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_INCLASS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_SPARSE_DATA \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h_253_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMECODESYNCHRONIZER_API UClass* StaticClass<class UTimecodeSynchronizer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizer_h


#define FOREACH_ENUM_ETIMECODESYNCHRONIZATIONFRAMERATESOURCES(op) \
	op(ETimecodeSynchronizationFrameRateSources::EngineCustomTimeStepFrameRate) \
	op(ETimecodeSynchronizationFrameRateSources::CustomFrameRate) 

enum class ETimecodeSynchronizationFrameRateSources : uint8;
template<> TIMECODESYNCHRONIZER_API UEnum* StaticEnum<ETimecodeSynchronizationFrameRateSources>();

#define FOREACH_ENUM_ETIMECODESYNCHRONIZATIONTIMECODETYPE(op) \
	op(ETimecodeSynchronizationTimecodeType::DefaultProvider) \
	op(ETimecodeSynchronizationTimecodeType::TimecodeProvider) \
	op(ETimecodeSynchronizationTimecodeType::InputSource) 

enum class ETimecodeSynchronizationTimecodeType;
template<> TIMECODESYNCHRONIZER_API UEnum* StaticEnum<ETimecodeSynchronizationTimecodeType>();

#define FOREACH_ENUM_ETIMECODESYNCHRONIZATIONSYNCMODE(op) \
	op(ETimecodeSynchronizationSyncMode::UserDefinedOffset) \
	op(ETimecodeSynchronizationSyncMode::Auto) \
	op(ETimecodeSynchronizationSyncMode::AutoOldest) 

enum class ETimecodeSynchronizationSyncMode;
template<> TIMECODESYNCHRONIZER_API UEnum* StaticEnum<ETimecodeSynchronizationSyncMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
