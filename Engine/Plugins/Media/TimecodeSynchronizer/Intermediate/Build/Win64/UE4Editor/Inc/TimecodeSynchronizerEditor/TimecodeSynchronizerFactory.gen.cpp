// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimecodeSynchronizerEditor/Public/TimecodeSynchronizerFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimecodeSynchronizerFactory() {}
// Cross Module References
	TIMECODESYNCHRONIZEREDITOR_API UClass* Z_Construct_UClass_UTimecodeSynchronizerFactory_NoRegister();
	TIMECODESYNCHRONIZEREDITOR_API UClass* Z_Construct_UClass_UTimecodeSynchronizerFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_TimecodeSynchronizerEditor();
// End Cross Module References
	void UTimecodeSynchronizerFactory::StaticRegisterNativesUTimecodeSynchronizerFactory()
	{
	}
	UClass* Z_Construct_UClass_UTimecodeSynchronizerFactory_NoRegister()
	{
		return UTimecodeSynchronizerFactory::StaticClass();
	}
	struct Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_TimecodeSynchronizerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "TimecodeSynchronizerFactory.h" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizerFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimecodeSynchronizerFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::ClassParams = {
		&UTimecodeSynchronizerFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimecodeSynchronizerFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimecodeSynchronizerFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimecodeSynchronizerFactory, 1236888019);
	template<> TIMECODESYNCHRONIZEREDITOR_API UClass* StaticClass<UTimecodeSynchronizerFactory>()
	{
		return UTimecodeSynchronizerFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimecodeSynchronizerFactory(Z_Construct_UClass_UTimecodeSynchronizerFactory, &UTimecodeSynchronizerFactory::StaticClass, TEXT("/Script/TimecodeSynchronizerEditor"), TEXT("UTimecodeSynchronizerFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimecodeSynchronizerFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
