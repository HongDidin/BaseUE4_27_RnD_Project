// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimecodeSynchronizer/Public/TimecodeSynchronizerProjectSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimecodeSynchronizerProjectSettings() {}
// Cross Module References
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_NoRegister();
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizerProjectSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_TimecodeSynchronizer();
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizer_NoRegister();
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_NoRegister();
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizerEditorSettings();
// End Cross Module References
	void UTimecodeSynchronizerProjectSettings::StaticRegisterNativesUTimecodeSynchronizerProjectSettings()
	{
	}
	UClass* Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_NoRegister()
	{
		return UTimecodeSynchronizerProjectSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayInToolbar_MetaData[];
#endif
		static void NewProp_bDisplayInToolbar_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayInToolbar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTimecodeSynchronizer_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultTimecodeSynchronizer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Global settings for TimecodeSynchronizer\n */" },
		{ "IncludePath", "TimecodeSynchronizerProjectSettings.h" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizerProjectSettings.h" },
		{ "ToolTip", "Global settings for TimecodeSynchronizer" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar_MetaData[] = {
		{ "Category", "TimecodeSynchronizer" },
		{ "Comment", "/** Display the timecode synchronizer icon in the editor toolbar. */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizerProjectSettings.h" },
		{ "ToolTip", "Display the timecode synchronizer icon in the editor toolbar." },
	};
#endif
	void Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar_SetBit(void* Obj)
	{
		((UTimecodeSynchronizerProjectSettings*)Obj)->bDisplayInToolbar = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar = { "bDisplayInToolbar", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimecodeSynchronizerProjectSettings), &Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_DefaultTimecodeSynchronizer_MetaData[] = {
		{ "Category", "TimecodeSynchronizer" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizerProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_DefaultTimecodeSynchronizer = { "DefaultTimecodeSynchronizer", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizerProjectSettings, DefaultTimecodeSynchronizer), Z_Construct_UClass_UTimecodeSynchronizer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_DefaultTimecodeSynchronizer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_DefaultTimecodeSynchronizer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_bDisplayInToolbar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::NewProp_DefaultTimecodeSynchronizer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimecodeSynchronizerProjectSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::ClassParams = {
		&UTimecodeSynchronizerProjectSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimecodeSynchronizerProjectSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimecodeSynchronizerProjectSettings, 3725885288);
	template<> TIMECODESYNCHRONIZER_API UClass* StaticClass<UTimecodeSynchronizerProjectSettings>()
	{
		return UTimecodeSynchronizerProjectSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimecodeSynchronizerProjectSettings(Z_Construct_UClass_UTimecodeSynchronizerProjectSettings, &UTimecodeSynchronizerProjectSettings::StaticClass, TEXT("/Script/TimecodeSynchronizer"), TEXT("UTimecodeSynchronizerProjectSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimecodeSynchronizerProjectSettings);
	void UTimecodeSynchronizerEditorSettings::StaticRegisterNativesUTimecodeSynchronizerEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_NoRegister()
	{
		return UTimecodeSynchronizerEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserTimecodeSynchronizer_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_UserTimecodeSynchronizer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Editor settings for TimecodeSynchronizer\n */" },
		{ "IncludePath", "TimecodeSynchronizerProjectSettings.h" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizerProjectSettings.h" },
		{ "ToolTip", "Editor settings for TimecodeSynchronizer" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::NewProp_UserTimecodeSynchronizer_MetaData[] = {
		{ "Category", "TimecodeSynchronizer" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizerProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::NewProp_UserTimecodeSynchronizer = { "UserTimecodeSynchronizer", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizerEditorSettings, UserTimecodeSynchronizer), Z_Construct_UClass_UTimecodeSynchronizer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::NewProp_UserTimecodeSynchronizer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::NewProp_UserTimecodeSynchronizer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::NewProp_UserTimecodeSynchronizer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimecodeSynchronizerEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::ClassParams = {
		&UTimecodeSynchronizerEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimecodeSynchronizerEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimecodeSynchronizerEditorSettings, 850523456);
	template<> TIMECODESYNCHRONIZER_API UClass* StaticClass<UTimecodeSynchronizerEditorSettings>()
	{
		return UTimecodeSynchronizerEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimecodeSynchronizerEditorSettings(Z_Construct_UClass_UTimecodeSynchronizerEditorSettings, &UTimecodeSynchronizerEditorSettings::StaticClass, TEXT("/Script/TimecodeSynchronizer"), TEXT("UTimecodeSynchronizerEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimecodeSynchronizerEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
