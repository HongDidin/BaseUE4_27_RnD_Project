// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMECODESYNCHRONIZER_TimecodeSynchronizerProjectSettings_generated_h
#error "TimecodeSynchronizerProjectSettings.generated.h already included, missing '#pragma once' in TimecodeSynchronizerProjectSettings.h"
#endif
#define TIMECODESYNCHRONIZER_TimecodeSynchronizerProjectSettings_generated_h

#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_SPARSE_DATA
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_RPC_WRAPPERS
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimecodeSynchronizerProjectSettings(); \
	friend struct Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UTimecodeSynchronizerProjectSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimecodeSynchronizer"), NO_API) \
	DECLARE_SERIALIZER(UTimecodeSynchronizerProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUTimecodeSynchronizerProjectSettings(); \
	friend struct Z_Construct_UClass_UTimecodeSynchronizerProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UTimecodeSynchronizerProjectSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimecodeSynchronizer"), NO_API) \
	DECLARE_SERIALIZER(UTimecodeSynchronizerProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimecodeSynchronizerProjectSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimecodeSynchronizerProjectSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimecodeSynchronizerProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimecodeSynchronizerProjectSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimecodeSynchronizerProjectSettings(UTimecodeSynchronizerProjectSettings&&); \
	NO_API UTimecodeSynchronizerProjectSettings(const UTimecodeSynchronizerProjectSettings&); \
public:


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimecodeSynchronizerProjectSettings(UTimecodeSynchronizerProjectSettings&&); \
	NO_API UTimecodeSynchronizerProjectSettings(const UTimecodeSynchronizerProjectSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimecodeSynchronizerProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimecodeSynchronizerProjectSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTimecodeSynchronizerProjectSettings)


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_16_PROLOG
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_SPARSE_DATA \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_RPC_WRAPPERS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_INCLASS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_SPARSE_DATA \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_20_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMECODESYNCHRONIZER_API UClass* StaticClass<class UTimecodeSynchronizerProjectSettings>();

#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_SPARSE_DATA
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_RPC_WRAPPERS
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimecodeSynchronizerEditorSettings(); \
	friend struct Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTimecodeSynchronizerEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimecodeSynchronizer"), NO_API) \
	DECLARE_SERIALIZER(UTimecodeSynchronizerEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUTimecodeSynchronizerEditorSettings(); \
	friend struct Z_Construct_UClass_UTimecodeSynchronizerEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTimecodeSynchronizerEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimecodeSynchronizer"), NO_API) \
	DECLARE_SERIALIZER(UTimecodeSynchronizerEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimecodeSynchronizerEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimecodeSynchronizerEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimecodeSynchronizerEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimecodeSynchronizerEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimecodeSynchronizerEditorSettings(UTimecodeSynchronizerEditorSettings&&); \
	NO_API UTimecodeSynchronizerEditorSettings(const UTimecodeSynchronizerEditorSettings&); \
public:


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTimecodeSynchronizerEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTimecodeSynchronizerEditorSettings(UTimecodeSynchronizerEditorSettings&&); \
	NO_API UTimecodeSynchronizerEditorSettings(const UTimecodeSynchronizerEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTimecodeSynchronizerEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimecodeSynchronizerEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimecodeSynchronizerEditorSettings)


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_52_PROLOG
#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_SPARSE_DATA \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_RPC_WRAPPERS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_INCLASS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_SPARSE_DATA \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h_56_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMECODESYNCHRONIZER_API UClass* StaticClass<class UTimecodeSynchronizerEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_TimecodeSynchronizer_Source_TimecodeSynchronizer_Public_TimecodeSynchronizerProjectSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
