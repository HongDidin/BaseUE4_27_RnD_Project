// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimecodeSynchronizer/Public/TimecodeSynchronizer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimecodeSynchronizer() {}
// Cross Module References
	TIMECODESYNCHRONIZER_API UEnum* Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationFrameRateSources();
	UPackage* Z_Construct_UPackage__Script_TimecodeSynchronizer();
	TIMECODESYNCHRONIZER_API UEnum* Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationTimecodeType();
	TIMECODESYNCHRONIZER_API UEnum* Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationSyncMode();
	TIMECODESYNCHRONIZER_API UScriptStruct* Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UTimeSynchronizationSource_NoRegister();
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizer_NoRegister();
	TIMECODESYNCHRONIZER_API UClass* Z_Construct_UClass_UTimecodeSynchronizer();
	ENGINE_API UClass* Z_Construct_UClass_UTimecodeProvider();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	ENGINE_API UClass* Z_Construct_UClass_UTimecodeProvider_NoRegister();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UFixedFrameRateCustomTimeStep_NoRegister();
// End Cross Module References
	static UEnum* ETimecodeSynchronizationFrameRateSources_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationFrameRateSources, Z_Construct_UPackage__Script_TimecodeSynchronizer(), TEXT("ETimecodeSynchronizationFrameRateSources"));
		}
		return Singleton;
	}
	template<> TIMECODESYNCHRONIZER_API UEnum* StaticEnum<ETimecodeSynchronizationFrameRateSources>()
	{
		return ETimecodeSynchronizationFrameRateSources_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimecodeSynchronizationFrameRateSources(ETimecodeSynchronizationFrameRateSources_StaticEnum, TEXT("/Script/TimecodeSynchronizer"), TEXT("ETimecodeSynchronizationFrameRateSources"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationFrameRateSources_Hash() { return 3129545373U; }
	UEnum* Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationFrameRateSources()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimecodeSynchronizer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimecodeSynchronizationFrameRateSources"), 0, Get_Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationFrameRateSources_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimecodeSynchronizationFrameRateSources::EngineCustomTimeStepFrameRate", (int64)ETimecodeSynchronizationFrameRateSources::EngineCustomTimeStepFrameRate },
				{ "ETimecodeSynchronizationFrameRateSources::CustomFrameRate", (int64)ETimecodeSynchronizationFrameRateSources::CustomFrameRate },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Enumerates possible framerate source\n */" },
				{ "CustomFrameRate.Name", "ETimecodeSynchronizationFrameRateSources::CustomFrameRate" },
				{ "CustomFrameRate.ToolTip", "Custom Frame Rate selected by the user." },
				{ "EngineCustomTimeStepFrameRate.Name", "ETimecodeSynchronizationFrameRateSources::EngineCustomTimeStepFrameRate" },
				{ "EngineCustomTimeStepFrameRate.ToolTip", "Frame Rate of engine custom time step if it is of type UFixedFrameRateCustomTimeStep." },
				{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
				{ "ToolTip", "Enumerates possible framerate source" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
				nullptr,
				"ETimecodeSynchronizationFrameRateSources",
				"ETimecodeSynchronizationFrameRateSources",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimecodeSynchronizationTimecodeType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationTimecodeType, Z_Construct_UPackage__Script_TimecodeSynchronizer(), TEXT("ETimecodeSynchronizationTimecodeType"));
		}
		return Singleton;
	}
	template<> TIMECODESYNCHRONIZER_API UEnum* StaticEnum<ETimecodeSynchronizationTimecodeType>()
	{
		return ETimecodeSynchronizationTimecodeType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimecodeSynchronizationTimecodeType(ETimecodeSynchronizationTimecodeType_StaticEnum, TEXT("/Script/TimecodeSynchronizer"), TEXT("ETimecodeSynchronizationTimecodeType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationTimecodeType_Hash() { return 4080580026U; }
	UEnum* Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationTimecodeType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimecodeSynchronizer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimecodeSynchronizationTimecodeType"), 0, Get_Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationTimecodeType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimecodeSynchronizationTimecodeType::DefaultProvider", (int64)ETimecodeSynchronizationTimecodeType::DefaultProvider },
				{ "ETimecodeSynchronizationTimecodeType::TimecodeProvider", (int64)ETimecodeSynchronizationTimecodeType::TimecodeProvider },
				{ "ETimecodeSynchronizationTimecodeType::InputSource", (int64)ETimecodeSynchronizationTimecodeType::InputSource },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Enumerates Timecode source type.\n */" },
				{ "DefaultProvider.Comment", "/** Use the configured Engine Default Timecode provider. */" },
				{ "DefaultProvider.Name", "ETimecodeSynchronizationTimecodeType::DefaultProvider" },
				{ "DefaultProvider.ToolTip", "Use the configured Engine Default Timecode provider." },
				{ "InputSource.Comment", "/** Use one of the InputSource as the Timecode Provider. */" },
				{ "InputSource.Name", "ETimecodeSynchronizationTimecodeType::InputSource" },
				{ "InputSource.ToolTip", "Use one of the InputSource as the Timecode Provider." },
				{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
				{ "TimecodeProvider.Comment", "/** Use an external Timecode Provider to provide the timecode to follow. */" },
				{ "TimecodeProvider.Name", "ETimecodeSynchronizationTimecodeType::TimecodeProvider" },
				{ "TimecodeProvider.ToolTip", "Use an external Timecode Provider to provide the timecode to follow." },
				{ "ToolTip", "Enumerates Timecode source type." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
				nullptr,
				"ETimecodeSynchronizationTimecodeType",
				"ETimecodeSynchronizationTimecodeType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimecodeSynchronizationSyncMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationSyncMode, Z_Construct_UPackage__Script_TimecodeSynchronizer(), TEXT("ETimecodeSynchronizationSyncMode"));
		}
		return Singleton;
	}
	template<> TIMECODESYNCHRONIZER_API UEnum* StaticEnum<ETimecodeSynchronizationSyncMode>()
	{
		return ETimecodeSynchronizationSyncMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimecodeSynchronizationSyncMode(ETimecodeSynchronizationSyncMode_StaticEnum, TEXT("/Script/TimecodeSynchronizer"), TEXT("ETimecodeSynchronizationSyncMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationSyncMode_Hash() { return 3428265120U; }
	UEnum* Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationSyncMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimecodeSynchronizer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimecodeSynchronizationSyncMode"), 0, Get_Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationSyncMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimecodeSynchronizationSyncMode::UserDefinedOffset", (int64)ETimecodeSynchronizationSyncMode::UserDefinedOffset },
				{ "ETimecodeSynchronizationSyncMode::Auto", (int64)ETimecodeSynchronizationSyncMode::Auto },
				{ "ETimecodeSynchronizationSyncMode::AutoOldest", (int64)ETimecodeSynchronizationSyncMode::AutoOldest },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Auto.Comment", "/**\n\x09 * Engine will try and automatically determine an appropriate offset based on what frames are available\n\x09 * on the given sources.\n\x09 *\n\x09 * This is suitable for running a single UE4 instance that just wants to synchronize its inputs.\n\x09 */" },
				{ "Auto.Name", "ETimecodeSynchronizationSyncMode::Auto" },
				{ "Auto.ToolTip", "Engine will try and automatically determine an appropriate offset based on what frames are available\non the given sources.\n\nThis is suitable for running a single UE4 instance that just wants to synchronize its inputs." },
				{ "AutoOldest.Comment", "/**\n\x09 * The same as Auto except that instead of trying to find a suitable timecode nearest to the\n\x09 * newest common frame, we try to find a suitable timecode nearest to the oldest common frame.\n\x09 */" },
				{ "AutoOldest.Name", "ETimecodeSynchronizationSyncMode::AutoOldest" },
				{ "AutoOldest.ToolTip", "The same as Auto except that instead of trying to find a suitable timecode nearest to the\nnewest common frame, we try to find a suitable timecode nearest to the oldest common frame." },
				{ "Comment", "/**\n * Defines the various modes that the synchronizer can use to try and achieve synchronization.\n */" },
				{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
				{ "ToolTip", "Defines the various modes that the synchronizer can use to try and achieve synchronization." },
				{ "UserDefinedOffset.Comment", "/**\n\x09 * User will specify an offset (number of frames) from the Timecode Source (see ETimecodeSycnrhonizationTimecodeType).\n\x09 * This offset may be positive or negative depending on the latency of the source.\n\x09 * Synchronization will be achieved once the synchronizer detects all input sources have frames that correspond\n\x09 * with the offset timecode.\n\x09 *\n\x09 * This is suitable for applications trying to keep multiple UE4 instances in sync while using nDisplay / genlock.\n\x09 */" },
				{ "UserDefinedOffset.Name", "ETimecodeSynchronizationSyncMode::UserDefinedOffset" },
				{ "UserDefinedOffset.ToolTip", "User will specify an offset (number of frames) from the Timecode Source (see ETimecodeSycnrhonizationTimecodeType).\nThis offset may be positive or negative depending on the latency of the source.\nSynchronization will be achieved once the synchronizer detects all input sources have frames that correspond\nwith the offset timecode.\n\nThis is suitable for applications trying to keep multiple UE4 instances in sync while using nDisplay / genlock." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
				nullptr,
				"ETimecodeSynchronizationSyncMode",
				"ETimecodeSynchronizationSyncMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FTimecodeSynchronizerActiveTimecodedInputSource::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMECODESYNCHRONIZER_API uint32 Get_Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource, Z_Construct_UPackage__Script_TimecodeSynchronizer(), TEXT("TimecodeSynchronizerActiveTimecodedInputSource"), sizeof(FTimecodeSynchronizerActiveTimecodedInputSource), Get_Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Hash());
	}
	return Singleton;
}
template<> TIMECODESYNCHRONIZER_API UScriptStruct* StaticStruct<FTimecodeSynchronizerActiveTimecodedInputSource>()
{
	return FTimecodeSynchronizerActiveTimecodedInputSource::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource(FTimecodeSynchronizerActiveTimecodedInputSource::StaticStruct, TEXT("/Script/TimecodeSynchronizer"), TEXT("TimecodeSynchronizerActiveTimecodedInputSource"), false, nullptr, nullptr);
static struct FScriptStruct_TimecodeSynchronizer_StaticRegisterNativesFTimecodeSynchronizerActiveTimecodedInputSource
{
	FScriptStruct_TimecodeSynchronizer_StaticRegisterNativesFTimecodeSynchronizerActiveTimecodedInputSource()
	{
		UScriptStruct::DeferCppStructOps<FTimecodeSynchronizerActiveTimecodedInputSource>(FName(TEXT("TimecodeSynchronizerActiveTimecodedInputSource")));
	}
} ScriptStruct_TimecodeSynchronizer_StaticRegisterNativesFTimecodeSynchronizerActiveTimecodedInputSource;
	struct Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsReady_MetaData[];
#endif
		static void NewProp_bIsReady_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsReady;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCanBeSynchronized_MetaData[];
#endif
		static void NewProp_bCanBeSynchronized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanBeSynchronized;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalNumberOfSamples_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalNumberOfSamples;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputSource;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Provides a wrapper around a UTimeSynchronizerSource, and caches data necessary\n * to provide synchronization.\n *\n * The values are typically updated once per frame.\n */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Provides a wrapper around a UTimeSynchronizerSource, and caches data necessary\nto provide synchronization.\n\nThe values are typically updated once per frame." },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimecodeSynchronizerActiveTimecodedInputSource>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/* Flag stating if the source is ready */" },
		{ "DisplayName", "Is Ready" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Flag stating if the source is ready" },
	};
#endif
	void Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady_SetBit(void* Obj)
	{
		((FTimecodeSynchronizerActiveTimecodedInputSource*)Obj)->bIsReady = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady = { "bIsReady", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimecodeSynchronizerActiveTimecodedInputSource), &Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/* Flag stating if this source can be synchronized */" },
		{ "DisplayName", "Can Be Synchronized" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Flag stating if this source can be synchronized" },
	};
#endif
	void Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized_SetBit(void* Obj)
	{
		((FTimecodeSynchronizerActiveTimecodedInputSource*)Obj)->bCanBeSynchronized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized = { "bCanBeSynchronized", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimecodeSynchronizerActiveTimecodedInputSource), &Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_TotalNumberOfSamples_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_TotalNumberOfSamples = { "TotalNumberOfSamples", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimecodeSynchronizerActiveTimecodedInputSource, TotalNumberOfSamples), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_TotalNumberOfSamples_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_TotalNumberOfSamples_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_InputSource_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/* Associated source pointers */" },
		{ "DisplayName", "Input Source" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Associated source pointers" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_InputSource = { "InputSource", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimecodeSynchronizerActiveTimecodedInputSource, InputSource), Z_Construct_UClass_UTimeSynchronizationSource_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_InputSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_InputSource_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bIsReady,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_bCanBeSynchronized,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_TotalNumberOfSamples,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::NewProp_InputSource,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
		nullptr,
		&NewStructOps,
		"TimecodeSynchronizerActiveTimecodedInputSource",
		sizeof(FTimecodeSynchronizerActiveTimecodedInputSource),
		alignof(FTimecodeSynchronizerActiveTimecodedInputSource),
		Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimecodeSynchronizer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimecodeSynchronizerActiveTimecodedInputSource"), sizeof(FTimecodeSynchronizerActiveTimecodedInputSource), Get_Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource_Hash() { return 4265770777U; }
	void UTimecodeSynchronizer::StaticRegisterNativesUTimecodeSynchronizer()
	{
	}
	UClass* Z_Construct_UClass_UTimecodeSynchronizer_NoRegister()
	{
		return UTimecodeSynchronizer::StaticClass();
	}
	struct Z_Construct_UClass_UTimecodeSynchronizer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FrameRateSource_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameRateSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FrameRateSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixedFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixedFrameRate;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimecodeProviderType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeProviderType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TimecodeProviderType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeProvider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterSynchronizationSourceIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MasterSynchronizationSourceIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsePreRollingTimecodeMarginOfErrors_MetaData[];
#endif
		static void NewProp_bUsePreRollingTimecodeMarginOfErrors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePreRollingTimecodeMarginOfErrors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreRollingTimecodeMarginOfErrors_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PreRollingTimecodeMarginOfErrors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsePreRollingTimeout_MetaData[];
#endif
		static void NewProp_bUsePreRollingTimeout_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePreRollingTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreRollingTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreRollingTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeSynchronizationInputSources_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TimeSynchronizationInputSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeSynchronizationInputSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TimeSynchronizationInputSources;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynamicSources;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SyncMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SyncMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SyncMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutoFrameOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AutoFrameOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWithRollover_MetaData[];
#endif
		static void NewProp_bWithRollover_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWithRollover;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SynchronizedSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SynchronizedSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SynchronizedSources;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NonSynchronizedSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NonSynchronizedSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NonSynchronizedSources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegisteredCustomTimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RegisteredCustomTimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedPreviousTimecodeProvider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedPreviousTimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedProxiedTimecodeProvider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedProxiedTimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActualFrameOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActualFrameOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimecodeSynchronizer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimecodeProvider,
		(UObject* (*)())Z_Construct_UPackage__Script_TimecodeSynchronizer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Timecode Synchronizer is intended to correlate multiple timecode sources to help ensure\n * that all sources can produce data that is frame aligned.\n *\n * This typically works by having sources buffer data until we have enough frames that\n * such that we can find an overlap. Once that process is finished, the Synchronizer will\n * provide the appropriate timecode to the engine (which can be retrieved via FApp::GetTimecode\n * and FApp::GetTimecodeFrameRate).\n *\n * Note, the Synchronizer doesn't perform any buffering of data itself (that is left up to\n * TimeSynchronizationSources). Instead, the synchronizer simply acts as a coordinator\n * making sure all sources are ready, determining if sync is possible, etc.\n */" },
		{ "IncludePath", "TimecodeSynchronizer.h" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Timecode Synchronizer is intended to correlate multiple timecode sources to help ensure\nthat all sources can produce data that is frame aligned.\n\nThis typically works by having sources buffer data until we have enough frames that\nsuch that we can find an overlap. Once that process is finished, the Synchronizer will\nprovide the appropriate timecode to the engine (which can be retrieved via FApp::GetTimecode\nand FApp::GetTimecodeFrameRate).\n\nNote, the Synchronizer doesn't perform any buffering of data itself (that is left up to\nTimeSynchronizationSources). Instead, the synchronizer simply acts as a coordinator\nmaking sure all sources are ready, determining if sync is possible, etc." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource_MetaData[] = {
		{ "Category", "Frame Rate Settings" },
		{ "Comment", "/** Frame Rate Source. */" },
		{ "DisplayName", "Frame Rate Source" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Frame Rate Source." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource = { "FrameRateSource", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, FrameRateSource), Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationFrameRateSources, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FixedFrameRate_MetaData[] = {
		{ "Category", "Frame Rate Settings" },
		{ "ClampMin", "15.0" },
		{ "Comment", "/** The fixed framerate to use. */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "The fixed framerate to use." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FixedFrameRate = { "FixedFrameRate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, FixedFrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FixedFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FixedFrameRate_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType_MetaData[] = {
		{ "Category", "Timecode Provider" },
		{ "Comment", "/** Use a Timecode Provider. */" },
		{ "DisplayName", "Select" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Use a Timecode Provider." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType = { "TimecodeProviderType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, TimecodeProviderType), Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationTimecodeType, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProvider_MetaData[] = {
		{ "Category", "Timecode Provider" },
		{ "Comment", "/** Custom strategy to tick in a interval. */" },
		{ "DisplayName", "Timecode Source" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Custom strategy to tick in a interval." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProvider = { "TimecodeProvider", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, TimecodeProvider), Z_Construct_UClass_UTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_MasterSynchronizationSourceIndex_MetaData[] = {
		{ "Category", "Timecode Provider" },
		{ "Comment", "/**\n\x09 * Index of the source that drives the synchronized Timecode.\n\x09 * The source need to be timecoded and flag as bUseForSynchronization\n\x09 */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Index of the source that drives the synchronized Timecode.\nThe source need to be timecoded and flag as bUseForSynchronization" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_MasterSynchronizationSourceIndex = { "MasterSynchronizationSourceIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, MasterSynchronizationSourceIndex), METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_MasterSynchronizationSourceIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_MasterSynchronizationSourceIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors_MetaData[] = {
		{ "Comment", "/** Enable verification of margin between synchronized time and source time */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Enable verification of margin between synchronized time and source time" },
	};
#endif
	void Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors_SetBit(void* Obj)
	{
		((UTimecodeSynchronizer*)Obj)->bUsePreRollingTimecodeMarginOfErrors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors = { "bUsePreRollingTimecodeMarginOfErrors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimecodeSynchronizer), &Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimecodeMarginOfErrors_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Maximum gap size between synchronized time and source time */" },
		{ "EditCondition", "bUsePreRollingTimecodeMarginOfErrors" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Maximum gap size between synchronized time and source time" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimecodeMarginOfErrors = { "PreRollingTimecodeMarginOfErrors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, PreRollingTimecodeMarginOfErrors), METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimecodeMarginOfErrors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimecodeMarginOfErrors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout_MetaData[] = {
		{ "Comment", "/** Enable PreRoll timeout */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Enable PreRoll timeout" },
	};
#endif
	void Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout_SetBit(void* Obj)
	{
		((UTimecodeSynchronizer*)Obj)->bUsePreRollingTimeout = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout = { "bUsePreRollingTimeout", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimecodeSynchronizer), &Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimeout_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** How long to wait for all source to be ready */" },
		{ "EditCondition", "bUsePreRollingTimeout" },
		{ "ForceUnits", "s" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "How long to wait for all source to be ready" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimeout = { "PreRollingTimeout", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, PreRollingTimeout), METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_Inner_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "//! ONLY MODIFY THESE IN EDITOR\n//! TODO: Deprecate this and make it private.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "! ONLY MODIFY THESE IN EDITOR\n! TODO: Deprecate this and make it private." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_Inner = { "TimeSynchronizationInputSources", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTimeSynchronizationSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "//! ONLY MODIFY THESE IN EDITOR\n//! TODO: Deprecate this and make it private.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "! ONLY MODIFY THESE IN EDITOR\n! TODO: Deprecate this and make it private." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources = { "TimeSynchronizationInputSources", nullptr, (EPropertyFlags)0x0010008000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, TimeSynchronizationInputSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources_Inner = { "DynamicSources", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTimeSynchronizationSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources = { "DynamicSources", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, DynamicSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "Comment", "/** What mode will be used for synchronization. */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "What mode will be used for synchronization." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode = { "SyncMode", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, SyncMode), Z_Construct_UEnum_TimecodeSynchronizer_ETimecodeSynchronizationSyncMode, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameOffset_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "ClampMax", "640" },
		{ "ClampMin", "-640" },
		{ "Comment", "/**\n\x09 * When UserDefined mode is used, the number of frames delayed from the Provider's timecode.\n\x09 * Negative values indicate the used timecode will be ahead of the Provider's.\n\x09 */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "When UserDefined mode is used, the number of frames delayed from the Provider's timecode.\nNegative values indicate the used timecode will be ahead of the Provider's." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameOffset = { "FrameOffset", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, FrameOffset), METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_AutoFrameOffset_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "ClampMax", "640" },
		{ "ClampMin", "0" },
		{ "Comment", "/**\n\x09 * Similar to FrameOffset.\n\x09 * For Auto mode, this represents the number of frames behind the newest synced frame.\n\x09 * For AutoModeOldest, the is the of frames ahead of the last synced frame.\n\x09 */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Similar to FrameOffset.\nFor Auto mode, this represents the number of frames behind the newest synced frame.\nFor AutoModeOldest, the is the of frames ahead of the last synced frame." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_AutoFrameOffset = { "AutoFrameOffset", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, AutoFrameOffset), METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_AutoFrameOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_AutoFrameOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "Comment", "/** Whether or not the specified Provider's timecode rolls over. (Rollover is expected to occur at Timecode 24:00:00:00). */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Whether or not the specified Provider's timecode rolls over. (Rollover is expected to occur at Timecode 24:00:00:00)." },
	};
#endif
	void Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover_SetBit(void* Obj)
	{
		((UTimecodeSynchronizer*)Obj)->bWithRollover = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover = { "bWithRollover", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimecodeSynchronizer), &Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources_Inner = { "SynchronizedSources", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/** Sources used for synchronization */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Sources used for synchronization" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources = { "SynchronizedSources", nullptr, (EPropertyFlags)0x0040000000222001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, SynchronizedSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources_Inner = { "NonSynchronizedSources", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTimecodeSynchronizerActiveTimecodedInputSource, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/* Sources that wants to be synchronized */" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
		{ "ToolTip", "Sources that wants to be synchronized" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources = { "NonSynchronizedSources", nullptr, (EPropertyFlags)0x0040000000222001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, NonSynchronizedSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_RegisteredCustomTimeStep_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_RegisteredCustomTimeStep = { "RegisteredCustomTimeStep", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, RegisteredCustomTimeStep), Z_Construct_UClass_UFixedFrameRateCustomTimeStep_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_RegisteredCustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_RegisteredCustomTimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedPreviousTimecodeProvider_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedPreviousTimecodeProvider = { "CachedPreviousTimecodeProvider", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, CachedPreviousTimecodeProvider), Z_Construct_UClass_UTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedPreviousTimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedPreviousTimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedProxiedTimecodeProvider_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedProxiedTimecodeProvider = { "CachedProxiedTimecodeProvider", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, CachedProxiedTimecodeProvider), Z_Construct_UClass_UTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedProxiedTimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedProxiedTimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_ActualFrameOffset_MetaData[] = {
		{ "Category", "Synchronization" },
		{ "ModuleRelativePath", "Public/TimecodeSynchronizer.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_ActualFrameOffset = { "ActualFrameOffset", nullptr, (EPropertyFlags)0x0040000000222001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimecodeSynchronizer, ActualFrameOffset), METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_ActualFrameOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_ActualFrameOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimecodeSynchronizer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameRateSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FixedFrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProviderType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_MasterSynchronizationSourceIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimecodeMarginOfErrors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimecodeMarginOfErrors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bUsePreRollingTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_PreRollingTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_TimeSynchronizationInputSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_DynamicSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SyncMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_FrameOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_AutoFrameOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_bWithRollover,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_SynchronizedSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_NonSynchronizedSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_RegisteredCustomTimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedPreviousTimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_CachedProxiedTimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimecodeSynchronizer_Statics::NewProp_ActualFrameOffset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimecodeSynchronizer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimecodeSynchronizer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimecodeSynchronizer_Statics::ClassParams = {
		&UTimecodeSynchronizer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTimecodeSynchronizer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTimecodeSynchronizer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimecodeSynchronizer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimecodeSynchronizer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimecodeSynchronizer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimecodeSynchronizer, 3953336385);
	template<> TIMECODESYNCHRONIZER_API UClass* StaticClass<UTimecodeSynchronizer>()
	{
		return UTimecodeSynchronizer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimecodeSynchronizer(Z_Construct_UClass_UTimecodeSynchronizer, &UTimecodeSynchronizer::StaticClass, TEXT("/Script/TimecodeSynchronizer"), TEXT("UTimecodeSynchronizer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimecodeSynchronizer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
