// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIACOMPOSITINGEDITOR_MediaPlayerRecording_generated_h
#error "MediaPlayerRecording.generated.h already included, missing '#pragma once' in MediaPlayerRecording.h"
#endif
#define MEDIACOMPOSITINGEDITOR_MediaPlayerRecording_generated_h

#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics; \
	MEDIACOMPOSITINGEDITOR_API static class UScriptStruct* StaticStruct();


template<> MEDIACOMPOSITINGEDITOR_API UScriptStruct* StaticStruct<struct FMediaPlayerRecordingSettings>();

#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_SPARSE_DATA
#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaPlayerRecording(); \
	friend struct Z_Construct_UClass_UMediaPlayerRecording_Statics; \
public: \
	DECLARE_CLASS(UMediaPlayerRecording, USequenceRecordingBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaCompositingEditor"), MEDIACOMPOSITINGEDITOR_API) \
	DECLARE_SERIALIZER(UMediaPlayerRecording)


#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_INCLASS \
private: \
	static void StaticRegisterNativesUMediaPlayerRecording(); \
	friend struct Z_Construct_UClass_UMediaPlayerRecording_Statics; \
public: \
	DECLARE_CLASS(UMediaPlayerRecording, USequenceRecordingBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaCompositingEditor"), MEDIACOMPOSITINGEDITOR_API) \
	DECLARE_SERIALIZER(UMediaPlayerRecording)


#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIACOMPOSITINGEDITOR_API UMediaPlayerRecording(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaPlayerRecording) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIACOMPOSITINGEDITOR_API, UMediaPlayerRecording); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaPlayerRecording); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIACOMPOSITINGEDITOR_API UMediaPlayerRecording(UMediaPlayerRecording&&); \
	MEDIACOMPOSITINGEDITOR_API UMediaPlayerRecording(const UMediaPlayerRecording&); \
public:


#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIACOMPOSITINGEDITOR_API UMediaPlayerRecording(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIACOMPOSITINGEDITOR_API UMediaPlayerRecording(UMediaPlayerRecording&&); \
	MEDIACOMPOSITINGEDITOR_API UMediaPlayerRecording(const UMediaPlayerRecording&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIACOMPOSITINGEDITOR_API, UMediaPlayerRecording); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaPlayerRecording); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaPlayerRecording)


#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaPlayerToRecord() { return STRUCT_OFFSET(UMediaPlayerRecording, MediaPlayerToRecord); }


#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_85_PROLOG
#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_SPARSE_DATA \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_INCLASS \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_SPARSE_DATA \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h_88_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaPlayerRecording."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIACOMPOSITINGEDITOR_API UClass* StaticClass<class UMediaPlayerRecording>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaCompositing_Source_MediaCompositingEditor_Private_Sequencer_MediaPlayerRecording_h


#define FOREACH_ENUM_EMEDIAPLAYERRECORDINGIMAGEFORMAT(op) \
	op(EMediaPlayerRecordingImageFormat::PNG) \
	op(EMediaPlayerRecordingImageFormat::JPEG) \
	op(EMediaPlayerRecordingImageFormat::BMP) \
	op(EMediaPlayerRecordingImageFormat::EXR) 

enum class EMediaPlayerRecordingImageFormat : uint8;
template<> MEDIACOMPOSITINGEDITOR_API UEnum* StaticEnum<EMediaPlayerRecordingImageFormat>();

#define FOREACH_ENUM_EMEDIAPLAYERRECORDINGNUMERATIONSTYLE(op) \
	op(EMediaPlayerRecordingNumerationStyle::AppendFrameNumber) \
	op(EMediaPlayerRecordingNumerationStyle::AppendSampleTime) 

enum class EMediaPlayerRecordingNumerationStyle : uint8;
template<> MEDIACOMPOSITINGEDITOR_API UEnum* StaticEnum<EMediaPlayerRecordingNumerationStyle>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
