// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaCompositingEditor/Private/Sequencer/MediaPlayerRecording.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaPlayerRecording() {}
// Cross Module References
	MEDIACOMPOSITINGEDITOR_API UEnum* Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingImageFormat();
	UPackage* Z_Construct_UPackage__Script_MediaCompositingEditor();
	MEDIACOMPOSITINGEDITOR_API UEnum* Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingNumerationStyle();
	MEDIACOMPOSITINGEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings();
	MEDIACOMPOSITINGEDITOR_API UClass* Z_Construct_UClass_UMediaPlayerRecording_NoRegister();
	MEDIACOMPOSITINGEDITOR_API UClass* Z_Construct_UClass_UMediaPlayerRecording();
	SEQUENCERECORDER_API UClass* Z_Construct_UClass_USequenceRecordingBase();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaPlayer_NoRegister();
// End Cross Module References
	static UEnum* EMediaPlayerRecordingImageFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingImageFormat, Z_Construct_UPackage__Script_MediaCompositingEditor(), TEXT("EMediaPlayerRecordingImageFormat"));
		}
		return Singleton;
	}
	template<> MEDIACOMPOSITINGEDITOR_API UEnum* StaticEnum<EMediaPlayerRecordingImageFormat>()
	{
		return EMediaPlayerRecordingImageFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaPlayerRecordingImageFormat(EMediaPlayerRecordingImageFormat_StaticEnum, TEXT("/Script/MediaCompositingEditor"), TEXT("EMediaPlayerRecordingImageFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingImageFormat_Hash() { return 1282647955U; }
	UEnum* Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingImageFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaCompositingEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaPlayerRecordingImageFormat"), 0, Get_Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingImageFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaPlayerRecordingImageFormat::PNG", (int64)EMediaPlayerRecordingImageFormat::PNG },
				{ "EMediaPlayerRecordingImageFormat::JPEG", (int64)EMediaPlayerRecordingImageFormat::JPEG },
				{ "EMediaPlayerRecordingImageFormat::BMP", (int64)EMediaPlayerRecordingImageFormat::BMP },
				{ "EMediaPlayerRecordingImageFormat::EXR", (int64)EMediaPlayerRecordingImageFormat::EXR },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BMP.Name", "EMediaPlayerRecordingImageFormat::BMP" },
				{ "EXR.Name", "EMediaPlayerRecordingImageFormat::EXR" },
				{ "JPEG.Name", "EMediaPlayerRecordingImageFormat::JPEG" },
				{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
				{ "PNG.Name", "EMediaPlayerRecordingImageFormat::PNG" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaCompositingEditor,
				nullptr,
				"EMediaPlayerRecordingImageFormat",
				"EMediaPlayerRecordingImageFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaPlayerRecordingNumerationStyle_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingNumerationStyle, Z_Construct_UPackage__Script_MediaCompositingEditor(), TEXT("EMediaPlayerRecordingNumerationStyle"));
		}
		return Singleton;
	}
	template<> MEDIACOMPOSITINGEDITOR_API UEnum* StaticEnum<EMediaPlayerRecordingNumerationStyle>()
	{
		return EMediaPlayerRecordingNumerationStyle_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaPlayerRecordingNumerationStyle(EMediaPlayerRecordingNumerationStyle_StaticEnum, TEXT("/Script/MediaCompositingEditor"), TEXT("EMediaPlayerRecordingNumerationStyle"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingNumerationStyle_Hash() { return 854210614U; }
	UEnum* Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingNumerationStyle()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaCompositingEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaPlayerRecordingNumerationStyle"), 0, Get_Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingNumerationStyle_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaPlayerRecordingNumerationStyle::AppendFrameNumber", (int64)EMediaPlayerRecordingNumerationStyle::AppendFrameNumber },
				{ "EMediaPlayerRecordingNumerationStyle::AppendSampleTime", (int64)EMediaPlayerRecordingNumerationStyle::AppendSampleTime },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AppendFrameNumber.Name", "EMediaPlayerRecordingNumerationStyle::AppendFrameNumber" },
				{ "AppendSampleTime.Name", "EMediaPlayerRecordingNumerationStyle::AppendSampleTime" },
				{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaCompositingEditor,
				nullptr,
				"EMediaPlayerRecordingNumerationStyle",
				"EMediaPlayerRecordingNumerationStyle",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMediaPlayerRecordingSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIACOMPOSITINGEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings, Z_Construct_UPackage__Script_MediaCompositingEditor(), TEXT("MediaPlayerRecordingSettings"), sizeof(FMediaPlayerRecordingSettings), Get_Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Hash());
	}
	return Singleton;
}
template<> MEDIACOMPOSITINGEDITOR_API UScriptStruct* StaticStruct<FMediaPlayerRecordingSettings>()
{
	return FMediaPlayerRecordingSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaPlayerRecordingSettings(FMediaPlayerRecordingSettings::StaticStruct, TEXT("/Script/MediaCompositingEditor"), TEXT("MediaPlayerRecordingSettings"), false, nullptr, nullptr);
static struct FScriptStruct_MediaCompositingEditor_StaticRegisterNativesFMediaPlayerRecordingSettings
{
	FScriptStruct_MediaCompositingEditor_StaticRegisterNativesFMediaPlayerRecordingSettings()
	{
		UScriptStruct::DeferCppStructOps<FMediaPlayerRecordingSettings>(FName(TEXT("MediaPlayerRecordingSettings")));
	}
} ScriptStruct_MediaCompositingEditor_StaticRegisterNativesFMediaPlayerRecordingSettings;
	struct Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bActive_MetaData[];
#endif
		static void NewProp_bActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordMediaFrame_MetaData[];
#endif
		static void NewProp_bRecordMediaFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordMediaFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BaseFilename;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NumerationStyle_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumerationStyle_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NumerationStyle;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ImageFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ImageFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompressionQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CompressionQuality;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetAlpha_MetaData[];
#endif
		static void NewProp_bResetAlpha_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetAlpha;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaPlayerRecordingSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive_MetaData[] = {
		{ "Category", "MediaPlayer Event Recording" },
		{ "Comment", "/** Whether this MediaPlayer is active and his event will be recorded when the 'Record' button is pressed. */" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "Whether this MediaPlayer is active and his event will be recorded when the 'Record' button is pressed." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive_SetBit(void* Obj)
	{
		((FMediaPlayerRecordingSettings*)Obj)->bActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive = { "bActive", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaPlayerRecordingSettings), &Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame_MetaData[] = {
		{ "Category", "MediaPlayer Frame Recording" },
		{ "Comment", "/** Whether this MediaPlayer is active and the image frame will be recorded when the 'Record' button is pressed. */" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "Whether this MediaPlayer is active and the image frame will be recorded when the 'Record' button is pressed." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame_SetBit(void* Obj)
	{
		((FMediaPlayerRecordingSettings*)Obj)->bRecordMediaFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame = { "bRecordMediaFrame", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaPlayerRecordingSettings), &Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_BaseFilename_MetaData[] = {
		{ "Category", "MediaPlayer Frame Recording" },
		{ "Comment", "/** How to name each frame. */" },
		{ "EditCondition", "bRecordMediaFrame" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "How to name each frame." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_BaseFilename = { "BaseFilename", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaPlayerRecordingSettings, BaseFilename), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_BaseFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_BaseFilename_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle_MetaData[] = {
		{ "Category", "MediaPlayer Frame Recording" },
		{ "Comment", "/** How to numerate the filename. */" },
		{ "EditCondition", "bRecordMediaFrame" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "How to numerate the filename." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle = { "NumerationStyle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaPlayerRecordingSettings, NumerationStyle), Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingNumerationStyle, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat_MetaData[] = {
		{ "Category", "MediaPlayer Frame Recording" },
		{ "Comment", "/** The image format we wish to record to. */" },
		{ "EditCondition", "bRecordMediaFrame" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "The image format we wish to record to." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat = { "ImageFormat", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaPlayerRecordingSettings, ImageFormat), Z_Construct_UEnum_MediaCompositingEditor_EMediaPlayerRecordingImageFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_CompressionQuality_MetaData[] = {
		{ "Category", "MediaPlayer Frame Recording" },
		{ "Comment", "/**\n\x09 * An image format specific compression setting.\n\x09 * For EXRs, either 0 (Default) or 1 (Uncompressed)\n\x09 * For PNGs & JPEGs, 0 (Default) or a value between 1 (worst quality, best compression) and 100 (best quality, worst compression)\n\x09 */" },
		{ "EditCondition", "bRecordMediaFrame" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "An image format specific compression setting.\nFor EXRs, either 0 (Default) or 1 (Uncompressed)\nFor PNGs & JPEGs, 0 (Default) or a value between 1 (worst quality, best compression) and 100 (best quality, worst compression)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_CompressionQuality = { "CompressionQuality", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaPlayerRecordingSettings, CompressionQuality), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_CompressionQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_CompressionQuality_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha_MetaData[] = {
		{ "Category", "MediaPlayer Frame Recording" },
		{ "Comment", "/**\n\x09 * If the format support it, set the alpha to 1 (or 255).\n\x09 * @note Removing alpha increase the memory foot print.\n\x09 */" },
		{ "EditCondition", "bRecordMediaFrame" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "If the format support it, set the alpha to 1 (or 255).\n@note Removing alpha increase the memory foot print." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha_SetBit(void* Obj)
	{
		((FMediaPlayerRecordingSettings*)Obj)->bResetAlpha = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha = { "bResetAlpha", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaPlayerRecordingSettings), &Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bActive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bRecordMediaFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_BaseFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_NumerationStyle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_ImageFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_CompressionQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::NewProp_bResetAlpha,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaCompositingEditor,
		nullptr,
		&NewStructOps,
		"MediaPlayerRecordingSettings",
		sizeof(FMediaPlayerRecordingSettings),
		alignof(FMediaPlayerRecordingSettings),
		Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaCompositingEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaPlayerRecordingSettings"), sizeof(FMediaPlayerRecordingSettings), Get_Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings_Hash() { return 480893404U; }
	void UMediaPlayerRecording::StaticRegisterNativesUMediaPlayerRecording()
	{
	}
	UClass* Z_Construct_UClass_UMediaPlayerRecording_NoRegister()
	{
		return UMediaPlayerRecording::StaticClass();
	}
	struct Z_Construct_UClass_UMediaPlayerRecording_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecordingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RecordingSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaPlayerToRecord_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_MediaPlayerToRecord;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaPlayerRecording_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USequenceRecordingBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaCompositingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerRecording_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/MediaPlayerRecording.h" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_RecordingSettings_MetaData[] = {
		{ "Category", "MediaPlayer Recording" },
		{ "Comment", "/** Whether this MediaPlayer is active and his event will be recorded when the 'Record' button is pressed. */" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "Whether this MediaPlayer is active and his event will be recorded when the 'Record' button is pressed." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_RecordingSettings = { "RecordingSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerRecording, RecordingSettings), Z_Construct_UScriptStruct_FMediaPlayerRecordingSettings, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_RecordingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_RecordingSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_MediaPlayerToRecord_MetaData[] = {
		{ "Category", "MediaPlayer Recording" },
		{ "Comment", "/** The MediaPlayer we want to record */" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaPlayerRecording.h" },
		{ "ToolTip", "The MediaPlayer we want to record" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_MediaPlayerToRecord = { "MediaPlayerToRecord", nullptr, (EPropertyFlags)0x0044000000000001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerRecording, MediaPlayerToRecord), Z_Construct_UClass_UMediaPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_MediaPlayerToRecord_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_MediaPlayerToRecord_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaPlayerRecording_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_RecordingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerRecording_Statics::NewProp_MediaPlayerToRecord,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaPlayerRecording_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaPlayerRecording>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaPlayerRecording_Statics::ClassParams = {
		&UMediaPlayerRecording::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaPlayerRecording_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerRecording_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerRecording_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerRecording_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaPlayerRecording()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaPlayerRecording_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaPlayerRecording, 1501339712);
	template<> MEDIACOMPOSITINGEDITOR_API UClass* StaticClass<UMediaPlayerRecording>()
	{
		return UMediaPlayerRecording::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaPlayerRecording(Z_Construct_UClass_UMediaPlayerRecording, &UMediaPlayerRecording::StaticClass, TEXT("/Script/MediaCompositingEditor"), TEXT("UMediaPlayerRecording"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaPlayerRecording);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
