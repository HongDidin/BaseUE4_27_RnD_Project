// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaCompositingEditor/Private/Sequencer/MediaSequenceRecorderExtender.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaSequenceRecorderExtender() {}
// Cross Module References
	MEDIACOMPOSITINGEDITOR_API UClass* Z_Construct_UClass_UMediaSequenceRecorderSettings_NoRegister();
	MEDIACOMPOSITINGEDITOR_API UClass* Z_Construct_UClass_UMediaSequenceRecorderSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaCompositingEditor();
// End Cross Module References
	void UMediaSequenceRecorderSettings::StaticRegisterNativesUMediaSequenceRecorderSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaSequenceRecorderSettings_NoRegister()
	{
		return UMediaSequenceRecorderSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordMediaPlayerEnabled_MetaData[];
#endif
		static void NewProp_bRecordMediaPlayerEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordMediaPlayerEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaPlayerSubDirectory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MediaPlayerSubDirectory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaCompositingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/MediaSequenceRecorderExtender.h" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaSequenceRecorderExtender.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled_MetaData[] = {
		{ "Category", "MediaPlayer Recording" },
		{ "Comment", "/** Whether to enabled MediaPlayer recording into this sequence. */" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaSequenceRecorderExtender.h" },
		{ "ToolTip", "Whether to enabled MediaPlayer recording into this sequence." },
	};
#endif
	void Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled_SetBit(void* Obj)
	{
		((UMediaSequenceRecorderSettings*)Obj)->bRecordMediaPlayerEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled = { "bRecordMediaPlayerEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaSequenceRecorderSettings), &Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_MediaPlayerSubDirectory_MetaData[] = {
		{ "Category", "MediaPlayer Recording" },
		{ "Comment", "/** The name of the subdirectory MediaPlayer will be placed in. Leave this empty to place into the same directory as the sequence base path */" },
		{ "ModuleRelativePath", "Private/Sequencer/MediaSequenceRecorderExtender.h" },
		{ "ToolTip", "The name of the subdirectory MediaPlayer will be placed in. Leave this empty to place into the same directory as the sequence base path" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_MediaPlayerSubDirectory = { "MediaPlayerSubDirectory", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaSequenceRecorderSettings, MediaPlayerSubDirectory), METADATA_PARAMS(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_MediaPlayerSubDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_MediaPlayerSubDirectory_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_bRecordMediaPlayerEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::NewProp_MediaPlayerSubDirectory,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaSequenceRecorderSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::ClassParams = {
		&UMediaSequenceRecorderSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaSequenceRecorderSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaSequenceRecorderSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaSequenceRecorderSettings, 1744544986);
	template<> MEDIACOMPOSITINGEDITOR_API UClass* StaticClass<UMediaSequenceRecorderSettings>()
	{
		return UMediaSequenceRecorderSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaSequenceRecorderSettings(Z_Construct_UClass_UMediaSequenceRecorderSettings, &UMediaSequenceRecorderSettings::StaticClass, TEXT("/Script/MediaCompositingEditor"), TEXT("UMediaSequenceRecorderSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaSequenceRecorderSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
