// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioCaptureTimecodeProvider/Public/AudioCaptureTimecodeProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioCaptureTimecodeProvider() {}
// Cross Module References
	AUDIOCAPTURETIMECODEPROVIDER_API UClass* Z_Construct_UClass_UAudioCaptureTimecodeProvider_NoRegister();
	AUDIOCAPTURETIMECODEPROVIDER_API UClass* Z_Construct_UClass_UAudioCaptureTimecodeProvider();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UGenlockedTimecodeProvider();
	UPackage* Z_Construct_UPackage__Script_AudioCaptureTimecodeProvider();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
// End Cross Module References
	void UAudioCaptureTimecodeProvider::StaticRegisterNativesUAudioCaptureTimecodeProvider()
	{
	}
	UClass* Z_Construct_UClass_UAudioCaptureTimecodeProvider_NoRegister()
	{
		return UAudioCaptureTimecodeProvider::StaticClass();
	}
	struct Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDetectFrameRate_MetaData[];
#endif
		static void NewProp_bDetectFrameRate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDetectFrameRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAssumeDropFrameFormat_MetaData[];
#endif
		static void NewProp_bAssumeDropFrameFormat_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAssumeDropFrameFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrameRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AudioChannel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGenlockedTimecodeProvider,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioCaptureTimecodeProvider,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Read the LTC from the audio capture device.\n */" },
		{ "IncludePath", "AudioCaptureTimecodeProvider.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/AudioCaptureTimecodeProvider.h" },
		{ "ToolTip", "Read the LTC from the audio capture device." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate_MetaData[] = {
		{ "Category", "TimecodeProvider" },
		{ "Comment", "/**\n\x09 * Detect the frame rate from the audio source.\n\x09 * It may takes some extra time before the frame rate is properly detected.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AudioCaptureTimecodeProvider.h" },
		{ "ToolTip", "Detect the frame rate from the audio source.\nIt may takes some extra time before the frame rate is properly detected." },
	};
#endif
	void Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate_SetBit(void* Obj)
	{
		((UAudioCaptureTimecodeProvider*)Obj)->bDetectFrameRate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate = { "bDetectFrameRate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAudioCaptureTimecodeProvider), &Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat_MetaData[] = {
		{ "Category", "TimecodeProvider" },
		{ "Comment", "/** When detecting the frame rate. Assume the frame rate is a drop frame format. */" },
		{ "EditCondition", "bDetectFrameRate" },
		{ "ModuleRelativePath", "Public/AudioCaptureTimecodeProvider.h" },
		{ "ToolTip", "When detecting the frame rate. Assume the frame rate is a drop frame format." },
	};
#endif
	void Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat_SetBit(void* Obj)
	{
		((UAudioCaptureTimecodeProvider*)Obj)->bAssumeDropFrameFormat = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat = { "bAssumeDropFrameFormat", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAudioCaptureTimecodeProvider), &Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_FrameRate_MetaData[] = {
		{ "Category", "TimecodeProvider" },
		{ "Comment", "/**\n\x09 * Frame expected from the audio source.\n\x09 */" },
		{ "EditCondition", "!bDetectFrameRate" },
		{ "ModuleRelativePath", "Public/AudioCaptureTimecodeProvider.h" },
		{ "ToolTip", "Frame expected from the audio source." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_FrameRate = { "FrameRate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAudioCaptureTimecodeProvider, FrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_FrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_FrameRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_AudioChannel_MetaData[] = {
		{ "Category", "TimecodeProvider" },
		{ "ClampMin", "1" },
		{ "Comment", "/**\n\x09 * Index of the Channel to used for the capture.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AudioCaptureTimecodeProvider.h" },
		{ "ToolTip", "Index of the Channel to used for the capture." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_AudioChannel = { "AudioChannel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAudioCaptureTimecodeProvider, AudioChannel), METADATA_PARAMS(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_AudioChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_AudioChannel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bDetectFrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_bAssumeDropFrameFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_FrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::NewProp_AudioChannel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAudioCaptureTimecodeProvider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::ClassParams = {
		&UAudioCaptureTimecodeProvider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAudioCaptureTimecodeProvider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAudioCaptureTimecodeProvider, 312884940);
	template<> AUDIOCAPTURETIMECODEPROVIDER_API UClass* StaticClass<UAudioCaptureTimecodeProvider>()
	{
		return UAudioCaptureTimecodeProvider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAudioCaptureTimecodeProvider(Z_Construct_UClass_UAudioCaptureTimecodeProvider, &UAudioCaptureTimecodeProvider::StaticClass, TEXT("/Script/AudioCaptureTimecodeProvider"), TEXT("UAudioCaptureTimecodeProvider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAudioCaptureTimecodeProvider);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
