// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOCAPTURETIMECODEPROVIDER_AudioCaptureTimecodeProvider_generated_h
#error "AudioCaptureTimecodeProvider.generated.h already included, missing '#pragma once' in AudioCaptureTimecodeProvider.h"
#endif
#define AUDIOCAPTURETIMECODEPROVIDER_AudioCaptureTimecodeProvider_generated_h

#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_SPARSE_DATA
#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAudioCaptureTimecodeProvider(); \
	friend struct Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(UAudioCaptureTimecodeProvider, UGenlockedTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioCaptureTimecodeProvider"), NO_API) \
	DECLARE_SERIALIZER(UAudioCaptureTimecodeProvider)


#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAudioCaptureTimecodeProvider(); \
	friend struct Z_Construct_UClass_UAudioCaptureTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(UAudioCaptureTimecodeProvider, UGenlockedTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioCaptureTimecodeProvider"), NO_API) \
	DECLARE_SERIALIZER(UAudioCaptureTimecodeProvider)


#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAudioCaptureTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioCaptureTimecodeProvider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAudioCaptureTimecodeProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioCaptureTimecodeProvider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAudioCaptureTimecodeProvider(UAudioCaptureTimecodeProvider&&); \
	NO_API UAudioCaptureTimecodeProvider(const UAudioCaptureTimecodeProvider&); \
public:


#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAudioCaptureTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAudioCaptureTimecodeProvider(UAudioCaptureTimecodeProvider&&); \
	NO_API UAudioCaptureTimecodeProvider(const UAudioCaptureTimecodeProvider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAudioCaptureTimecodeProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioCaptureTimecodeProvider); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioCaptureTimecodeProvider)


#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_15_PROLOG
#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_INCLASS \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AudioCaptureTimecodeProvider."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOCAPTURETIMECODEPROVIDER_API UClass* StaticClass<class UAudioCaptureTimecodeProvider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AudioCaptureTimecodeProvider_Source_AudioCaptureTimecodeProvider_Public_AudioCaptureTimecodeProvider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
