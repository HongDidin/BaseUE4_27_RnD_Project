// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaPlayerEditor/Private/Shared/MediaPlayerEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaPlayerEditorSettings() {}
// Cross Module References
	MEDIAPLAYEREDITOR_API UEnum* Z_Construct_UEnum_MediaPlayerEditor_EMediaPlayerEditorScale();
	UPackage* Z_Construct_UPackage__Script_MediaPlayerEditor();
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlayerEditorSettings_NoRegister();
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlayerEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* EMediaPlayerEditorScale_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaPlayerEditor_EMediaPlayerEditorScale, Z_Construct_UPackage__Script_MediaPlayerEditor(), TEXT("EMediaPlayerEditorScale"));
		}
		return Singleton;
	}
	template<> MEDIAPLAYEREDITOR_API UEnum* StaticEnum<EMediaPlayerEditorScale>()
	{
		return EMediaPlayerEditorScale_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaPlayerEditorScale(EMediaPlayerEditorScale_StaticEnum, TEXT("/Script/MediaPlayerEditor"), TEXT("EMediaPlayerEditorScale"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaPlayerEditor_EMediaPlayerEditorScale_Hash() { return 761399014U; }
	UEnum* Z_Construct_UEnum_MediaPlayerEditor_EMediaPlayerEditorScale()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaPlayerEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaPlayerEditorScale"), 0, Get_Z_Construct_UEnum_MediaPlayerEditor_EMediaPlayerEditorScale_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaPlayerEditorScale::Fill", (int64)EMediaPlayerEditorScale::Fill },
				{ "EMediaPlayerEditorScale::Fit", (int64)EMediaPlayerEditorScale::Fit },
				{ "EMediaPlayerEditorScale::Original", (int64)EMediaPlayerEditorScale::Original },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Options for scaling the viewport's video texture. */" },
				{ "Fill.Comment", "/** Stretch non-uniformly to fill the viewport. */" },
				{ "Fill.Name", "EMediaPlayerEditorScale::Fill" },
				{ "Fill.ToolTip", "Stretch non-uniformly to fill the viewport." },
				{ "Fit.Comment", "/** Scale uniformly, preserving aspect ratio. */" },
				{ "Fit.Name", "EMediaPlayerEditorScale::Fit" },
				{ "Fit.ToolTip", "Scale uniformly, preserving aspect ratio." },
				{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorSettings.h" },
				{ "Original.Comment", "/** Do not stretch or scale. */" },
				{ "Original.Name", "EMediaPlayerEditorScale::Original" },
				{ "Original.ToolTip", "Do not stretch or scale." },
				{ "ToolTip", "Options for scaling the viewport's video texture." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaPlayerEditor,
				nullptr,
				"EMediaPlayerEditorScale",
				"EMediaPlayerEditorScale",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMediaPlayerEditorSettings::StaticRegisterNativesUMediaPlayerEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaPlayerEditorSettings_NoRegister()
	{
		return UMediaPlayerEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaPlayerEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesiredPlayerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DesiredPlayerName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowTextOverlays_MetaData[];
#endif
		static void NewProp_ShowTextOverlays_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ShowTextOverlays;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewportScale_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ViewportScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Shared/MediaPlayerEditorSettings.h" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_DesiredPlayerName_MetaData[] = {
		{ "Category", "Viewer" },
		{ "Comment", "/** The name of the desired native media player to use for playback. */" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorSettings.h" },
		{ "ToolTip", "The name of the desired native media player to use for playback." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_DesiredPlayerName = { "DesiredPlayerName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerEditorSettings, DesiredPlayerName), METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_DesiredPlayerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_DesiredPlayerName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays_MetaData[] = {
		{ "Category", "Viewer" },
		{ "Comment", "/** Whether to display overlay texts. */" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorSettings.h" },
		{ "ToolTip", "Whether to display overlay texts." },
	};
#endif
	void Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays_SetBit(void* Obj)
	{
		((UMediaPlayerEditorSettings*)Obj)->ShowTextOverlays = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays = { "ShowTextOverlays", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaPlayerEditorSettings), &Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale_MetaData[] = {
		{ "Category", "Viewer" },
		{ "Comment", "/** How the video viewport should be scaled. */" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorSettings.h" },
		{ "ToolTip", "How the video viewport should be scaled." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale = { "ViewportScale", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerEditorSettings, ViewportScale), Z_Construct_UEnum_MediaPlayerEditor_EMediaPlayerEditorScale, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_DesiredPlayerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ShowTextOverlays,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::NewProp_ViewportScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaPlayerEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::ClassParams = {
		&UMediaPlayerEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaPlayerEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaPlayerEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaPlayerEditorSettings, 1340616869);
	template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<UMediaPlayerEditorSettings>()
	{
		return UMediaPlayerEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaPlayerEditorSettings(Z_Construct_UClass_UMediaPlayerEditorSettings, &UMediaPlayerEditorSettings::StaticClass, TEXT("/Script/MediaPlayerEditor"), TEXT("UMediaPlayerEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaPlayerEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
