// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAPLAYEREDITOR_MediaPlayerEditorMediaContext_generated_h
#error "MediaPlayerEditorMediaContext.generated.h already included, missing '#pragma once' in MediaPlayerEditorMediaContext.h"
#endif
#define MEDIAPLAYEREDITOR_MediaPlayerEditorMediaContext_generated_h

#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_SPARSE_DATA
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaPlayerEditorMediaContext(); \
	friend struct Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics; \
public: \
	DECLARE_CLASS(UMediaPlayerEditorMediaContext, UToolMenuContextBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UMediaPlayerEditorMediaContext)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMediaPlayerEditorMediaContext(); \
	friend struct Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics; \
public: \
	DECLARE_CLASS(UMediaPlayerEditorMediaContext, UToolMenuContextBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UMediaPlayerEditorMediaContext)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaPlayerEditorMediaContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaPlayerEditorMediaContext) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaPlayerEditorMediaContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaPlayerEditorMediaContext); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaPlayerEditorMediaContext(UMediaPlayerEditorMediaContext&&); \
	NO_API UMediaPlayerEditorMediaContext(const UMediaPlayerEditorMediaContext&); \
public:


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaPlayerEditorMediaContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaPlayerEditorMediaContext(UMediaPlayerEditorMediaContext&&); \
	NO_API UMediaPlayerEditorMediaContext(const UMediaPlayerEditorMediaContext&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaPlayerEditorMediaContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaPlayerEditorMediaContext); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaPlayerEditorMediaContext)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_14_PROLOG
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_INCLASS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<class UMediaPlayerEditorMediaContext>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Shared_MediaPlayerEditorMediaContext_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
