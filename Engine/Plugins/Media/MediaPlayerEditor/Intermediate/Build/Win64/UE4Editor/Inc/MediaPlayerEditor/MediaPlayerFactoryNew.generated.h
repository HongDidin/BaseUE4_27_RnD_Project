// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAPLAYEREDITOR_MediaPlayerFactoryNew_generated_h
#error "MediaPlayerFactoryNew.generated.h already included, missing '#pragma once' in MediaPlayerFactoryNew.h"
#endif
#define MEDIAPLAYEREDITOR_MediaPlayerFactoryNew_generated_h

#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_SPARSE_DATA
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaPlayerFactoryNew(); \
	friend struct Z_Construct_UClass_UMediaPlayerFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UMediaPlayerFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UMediaPlayerFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUMediaPlayerFactoryNew(); \
	friend struct Z_Construct_UClass_UMediaPlayerFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UMediaPlayerFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UMediaPlayerFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaPlayerFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaPlayerFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaPlayerFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaPlayerFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaPlayerFactoryNew(UMediaPlayerFactoryNew&&); \
	NO_API UMediaPlayerFactoryNew(const UMediaPlayerFactoryNew&); \
public:


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaPlayerFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaPlayerFactoryNew(UMediaPlayerFactoryNew&&); \
	NO_API UMediaPlayerFactoryNew(const UMediaPlayerFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaPlayerFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaPlayerFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaPlayerFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_23_PROLOG
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_INCLASS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaPlayerFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<class UMediaPlayerFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaPlayerFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
