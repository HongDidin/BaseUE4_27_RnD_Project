// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaPlayerEditor/Private/Factories/MediaTextureFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaTextureFactoryNew() {}
// Cross Module References
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaTextureFactoryNew_NoRegister();
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaTextureFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MediaPlayerEditor();
// End Cross Module References
	void UMediaTextureFactoryNew::StaticRegisterNativesUMediaTextureFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UMediaTextureFactoryNew_NoRegister()
	{
		return UMediaTextureFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UMediaTextureFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaTextureFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaTextureFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UMediaTexture objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/MediaTextureFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/MediaTextureFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UMediaTexture objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaTextureFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaTextureFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaTextureFactoryNew_Statics::ClassParams = {
		&UMediaTextureFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaTextureFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaTextureFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaTextureFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaTextureFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaTextureFactoryNew, 498115006);
	template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<UMediaTextureFactoryNew>()
	{
		return UMediaTextureFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaTextureFactoryNew(Z_Construct_UClass_UMediaTextureFactoryNew, &UMediaTextureFactoryNew::StaticClass, TEXT("/Script/MediaPlayerEditor"), TEXT("UMediaTextureFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaTextureFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
