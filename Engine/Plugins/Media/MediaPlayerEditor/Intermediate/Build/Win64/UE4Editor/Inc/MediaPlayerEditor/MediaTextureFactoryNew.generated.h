// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAPLAYEREDITOR_MediaTextureFactoryNew_generated_h
#error "MediaTextureFactoryNew.generated.h already included, missing '#pragma once' in MediaTextureFactoryNew.h"
#endif
#define MEDIAPLAYEREDITOR_MediaTextureFactoryNew_generated_h

#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_SPARSE_DATA
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaTextureFactoryNew(); \
	friend struct Z_Construct_UClass_UMediaTextureFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UMediaTextureFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), MEDIAPLAYEREDITOR_API) \
	DECLARE_SERIALIZER(UMediaTextureFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMediaTextureFactoryNew(); \
	friend struct Z_Construct_UClass_UMediaTextureFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UMediaTextureFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), MEDIAPLAYEREDITOR_API) \
	DECLARE_SERIALIZER(UMediaTextureFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIAPLAYEREDITOR_API UMediaTextureFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaTextureFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIAPLAYEREDITOR_API, UMediaTextureFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaTextureFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIAPLAYEREDITOR_API UMediaTextureFactoryNew(UMediaTextureFactoryNew&&); \
	MEDIAPLAYEREDITOR_API UMediaTextureFactoryNew(const UMediaTextureFactoryNew&); \
public:


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIAPLAYEREDITOR_API UMediaTextureFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIAPLAYEREDITOR_API UMediaTextureFactoryNew(UMediaTextureFactoryNew&&); \
	MEDIAPLAYEREDITOR_API UMediaTextureFactoryNew(const UMediaTextureFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIAPLAYEREDITOR_API, UMediaTextureFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaTextureFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaTextureFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_13_PROLOG
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_INCLASS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaTextureFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<class UMediaTextureFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_MediaTextureFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
