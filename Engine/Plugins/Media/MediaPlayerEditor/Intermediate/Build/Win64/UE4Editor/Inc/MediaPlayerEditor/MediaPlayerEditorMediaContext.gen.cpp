// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaPlayerEditor/Private/Shared/MediaPlayerEditorMediaContext.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaPlayerEditorMediaContext() {}
// Cross Module References
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlayerEditorMediaContext_NoRegister();
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlayerEditorMediaContext();
	TOOLMENUS_API UClass* Z_Construct_UClass_UToolMenuContextBase();
	UPackage* Z_Construct_UPackage__Script_MediaPlayerEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UMediaPlayerEditorMediaContext::StaticRegisterNativesUMediaPlayerEditorMediaContext()
	{
	}
	UClass* Z_Construct_UClass_UMediaPlayerEditorMediaContext_NoRegister()
	{
		return UMediaPlayerEditorMediaContext::StaticClass();
	}
	struct Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StyleSetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_StyleSetName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UToolMenuContextBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Shared/MediaPlayerEditorMediaContext.h" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorMediaContext.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_SelectedAsset_MetaData[] = {
		{ "Category", "Editor Menus" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorMediaContext.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_SelectedAsset = { "SelectedAsset", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerEditorMediaContext, SelectedAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_SelectedAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_SelectedAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_StyleSetName_MetaData[] = {
		{ "Category", "Editor Menus" },
		{ "ModuleRelativePath", "Private/Shared/MediaPlayerEditorMediaContext.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_StyleSetName = { "StyleSetName", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerEditorMediaContext, StyleSetName), METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_StyleSetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_StyleSetName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_SelectedAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::NewProp_StyleSetName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaPlayerEditorMediaContext>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::ClassParams = {
		&UMediaPlayerEditorMediaContext::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaPlayerEditorMediaContext()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaPlayerEditorMediaContext_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaPlayerEditorMediaContext, 638043866);
	template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<UMediaPlayerEditorMediaContext>()
	{
		return UMediaPlayerEditorMediaContext::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaPlayerEditorMediaContext(Z_Construct_UClass_UMediaPlayerEditorMediaContext, &UMediaPlayerEditorMediaContext::StaticClass, TEXT("/Script/MediaPlayerEditor"), TEXT("UMediaPlayerEditorMediaContext"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaPlayerEditorMediaContext);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
