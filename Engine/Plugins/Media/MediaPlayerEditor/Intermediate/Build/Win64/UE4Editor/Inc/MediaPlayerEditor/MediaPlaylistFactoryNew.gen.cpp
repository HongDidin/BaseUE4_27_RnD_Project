// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaPlayerEditor/Private/Factories/MediaPlaylistFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaPlaylistFactoryNew() {}
// Cross Module References
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlaylistFactoryNew_NoRegister();
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlaylistFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MediaPlayerEditor();
// End Cross Module References
	void UMediaPlaylistFactoryNew::StaticRegisterNativesUMediaPlaylistFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UMediaPlaylistFactoryNew_NoRegister()
	{
		return UMediaPlaylistFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UMediaPlaylist objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/MediaPlaylistFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/MediaPlaylistFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UMediaPlaylist objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaPlaylistFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::ClassParams = {
		&UMediaPlaylistFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaPlaylistFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaPlaylistFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaPlaylistFactoryNew, 184708852);
	template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<UMediaPlaylistFactoryNew>()
	{
		return UMediaPlaylistFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaPlaylistFactoryNew(Z_Construct_UClass_UMediaPlaylistFactoryNew, &UMediaPlaylistFactoryNew::StaticClass, TEXT("/Script/MediaPlayerEditor"), TEXT("UMediaPlaylistFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaPlaylistFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
