// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAPLAYEREDITOR_StreamMediaSourceFactoryNew_generated_h
#error "StreamMediaSourceFactoryNew.generated.h already included, missing '#pragma once' in StreamMediaSourceFactoryNew.h"
#endif
#define MEDIAPLAYEREDITOR_StreamMediaSourceFactoryNew_generated_h

#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_SPARSE_DATA
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStreamMediaSourceFactoryNew(); \
	friend struct Z_Construct_UClass_UStreamMediaSourceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UStreamMediaSourceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UStreamMediaSourceFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUStreamMediaSourceFactoryNew(); \
	friend struct Z_Construct_UClass_UStreamMediaSourceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UStreamMediaSourceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UStreamMediaSourceFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStreamMediaSourceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStreamMediaSourceFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStreamMediaSourceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStreamMediaSourceFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStreamMediaSourceFactoryNew(UStreamMediaSourceFactoryNew&&); \
	NO_API UStreamMediaSourceFactoryNew(const UStreamMediaSourceFactoryNew&); \
public:


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStreamMediaSourceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStreamMediaSourceFactoryNew(UStreamMediaSourceFactoryNew&&); \
	NO_API UStreamMediaSourceFactoryNew(const UStreamMediaSourceFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStreamMediaSourceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStreamMediaSourceFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStreamMediaSourceFactoryNew)


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_13_PROLOG
#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_INCLASS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class StreamMediaSourceFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<class UStreamMediaSourceFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaPlayerEditor_Source_MediaPlayerEditor_Private_Factories_StreamMediaSourceFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
