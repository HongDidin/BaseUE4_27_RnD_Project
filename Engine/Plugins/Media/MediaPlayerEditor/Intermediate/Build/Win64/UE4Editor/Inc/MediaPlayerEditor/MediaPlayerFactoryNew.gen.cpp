// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaPlayerEditor/Private/Factories/MediaPlayerFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaPlayerFactoryNew() {}
// Cross Module References
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlayerFactoryNew_NoRegister();
	MEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UMediaPlayerFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MediaPlayerEditor();
// End Cross Module References
	void UMediaPlayerFactoryNew::StaticRegisterNativesUMediaPlayerFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UMediaPlayerFactoryNew_NoRegister()
	{
		return UMediaPlayerFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UMediaPlayerFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UMediaPlayer objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/MediaPlayerFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/MediaPlayerFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UMediaPlayer objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaPlayerFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::ClassParams = {
		&UMediaPlayerFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaPlayerFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaPlayerFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaPlayerFactoryNew, 1467149339);
	template<> MEDIAPLAYEREDITOR_API UClass* StaticClass<UMediaPlayerFactoryNew>()
	{
		return UMediaPlayerFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaPlayerFactoryNew(Z_Construct_UClass_UMediaPlayerFactoryNew, &UMediaPlayerFactoryNew::StaticClass, TEXT("/Script/MediaPlayerEditor"), TEXT("UMediaPlayerFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaPlayerFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
