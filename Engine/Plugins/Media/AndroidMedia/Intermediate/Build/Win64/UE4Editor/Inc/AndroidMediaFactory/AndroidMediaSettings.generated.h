// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANDROIDMEDIAFACTORY_AndroidMediaSettings_generated_h
#error "AndroidMediaSettings.generated.h already included, missing '#pragma once' in AndroidMediaSettings.h"
#endif
#define ANDROIDMEDIAFACTORY_AndroidMediaSettings_generated_h

#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_SPARSE_DATA
#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_RPC_WRAPPERS
#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAndroidMediaSettings(); \
	friend struct Z_Construct_UClass_UAndroidMediaSettings_Statics; \
public: \
	DECLARE_CLASS(UAndroidMediaSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidMediaFactory"), NO_API) \
	DECLARE_SERIALIZER(UAndroidMediaSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUAndroidMediaSettings(); \
	friend struct Z_Construct_UClass_UAndroidMediaSettings_Statics; \
public: \
	DECLARE_CLASS(UAndroidMediaSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AndroidMediaFactory"), NO_API) \
	DECLARE_SERIALIZER(UAndroidMediaSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAndroidMediaSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAndroidMediaSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidMediaSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidMediaSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidMediaSettings(UAndroidMediaSettings&&); \
	NO_API UAndroidMediaSettings(const UAndroidMediaSettings&); \
public:


#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAndroidMediaSettings(UAndroidMediaSettings&&); \
	NO_API UAndroidMediaSettings(const UAndroidMediaSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAndroidMediaSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAndroidMediaSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAndroidMediaSettings)


#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_15_PROLOG
#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_RPC_WRAPPERS \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_INCLASS \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_SPARSE_DATA \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANDROIDMEDIAFACTORY_API UClass* StaticClass<class UAndroidMediaSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AndroidMedia_Source_AndroidMediaFactory_Public_AndroidMediaSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
