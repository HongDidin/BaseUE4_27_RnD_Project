// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilities/Public/MediaPlayerTimeSynchronizationSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaPlayerTimeSynchronizationSource() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UTimeSynchronizationSource();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilities();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaSource_NoRegister();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaTexture_NoRegister();
// End Cross Module References
	void UMediaPlayerTimeSynchronizationSource::StaticRegisterNativesUMediaPlayerTimeSynchronizationSource()
	{
	}
	UClass* Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_NoRegister()
	{
		return UMediaPlayerTimeSynchronizationSource::StaticClass();
	}
	struct Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimeSynchronizationSource,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Synchronization Source using the Media Player framework\n */" },
		{ "IncludePath", "MediaPlayerTimeSynchronizationSource.h" },
		{ "ModuleRelativePath", "Public/MediaPlayerTimeSynchronizationSource.h" },
		{ "ToolTip", "Synchronization Source using the Media Player framework" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaSource_MetaData[] = {
		{ "Category", "Player" },
		{ "Comment", "/* Media source asset of this input*/" },
		{ "ModuleRelativePath", "Public/MediaPlayerTimeSynchronizationSource.h" },
		{ "ToolTip", "Media source asset of this input" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaSource = { "MediaSource", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerTimeSynchronizationSource, MediaSource), Z_Construct_UClass_UMediaSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaTexture_MetaData[] = {
		{ "Category", "Player" },
		{ "Comment", "/* Texture linked to the media player*/" },
		{ "ModuleRelativePath", "Public/MediaPlayerTimeSynchronizationSource.h" },
		{ "ToolTip", "Texture linked to the media player" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaTexture = { "MediaTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaPlayerTimeSynchronizationSource, MediaTexture), Z_Construct_UClass_UMediaTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::NewProp_MediaTexture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaPlayerTimeSynchronizationSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::ClassParams = {
		&UMediaPlayerTimeSynchronizationSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaPlayerTimeSynchronizationSource, 2508907672);
	template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<UMediaPlayerTimeSynchronizationSource>()
	{
		return UMediaPlayerTimeSynchronizationSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaPlayerTimeSynchronizationSource(Z_Construct_UClass_UMediaPlayerTimeSynchronizationSource, &UMediaPlayerTimeSynchronizationSource::StaticClass, TEXT("/Script/MediaFrameworkUtilities"), TEXT("UMediaPlayerTimeSynchronizationSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaPlayerTimeSynchronizationSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
