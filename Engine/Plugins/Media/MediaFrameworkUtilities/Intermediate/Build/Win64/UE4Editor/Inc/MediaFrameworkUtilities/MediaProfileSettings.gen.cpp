// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilities/Public/Profile/MediaProfileSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaProfileSettings() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfileSettings_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfileSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilities();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UProxyMediaSource_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UProxyMediaOutput_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfile_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfileEditorSettings_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfileEditorSettings();
// End Cross Module References
	void UMediaProfileSettings::StaticRegisterNativesUMediaProfileSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaProfileSettings_NoRegister()
	{
		return UMediaProfileSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaProfileSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyInCommandlet_MetaData[];
#endif
		static void NewProp_bApplyInCommandlet_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyInCommandlet;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MediaSourceProxy_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSourceProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MediaSourceProxy;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MediaOutputProxy_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutputProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MediaOutputProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartupMediaProfile_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_StartupMediaProfile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaProfileSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the media profile.\n */" },
		{ "IncludePath", "Profile/MediaProfileSettings.h" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "Settings for the media profile." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "Comment", "/**\n\x09 * Apply the startup media profile even when we are running a commandlet.\n\x09 * @note We always try to apply the user media profile before the startup media profile in the editor or standalone.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "Apply the startup media profile even when we are running a commandlet.\n@note We always try to apply the user media profile before the startup media profile in the editor or standalone." },
	};
#endif
	void Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet_SetBit(void* Obj)
	{
		((UMediaProfileSettings*)Obj)->bApplyInCommandlet = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet = { "bApplyInCommandlet", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaProfileSettings), &Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy_Inner = { "MediaSourceProxy", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UProxyMediaSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy = { "MediaSourceProxy", nullptr, (EPropertyFlags)0x0044040000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfileSettings, MediaSourceProxy), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy_Inner = { "MediaOutputProxy", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UProxyMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy = { "MediaOutputProxy", nullptr, (EPropertyFlags)0x0044040000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfileSettings, MediaOutputProxy), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_StartupMediaProfile_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "Comment", "/**\n\x09 * The media profile to use at startup.\n\x09 * @note The media profile can be overridden in the editor by user.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "The media profile to use at startup.\n@note The media profile can be overridden in the editor by user." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_StartupMediaProfile = { "StartupMediaProfile", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfileSettings, StartupMediaProfile), Z_Construct_UClass_UMediaProfile_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_StartupMediaProfile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_StartupMediaProfile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaProfileSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_bApplyInCommandlet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaSourceProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_MediaOutputProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileSettings_Statics::NewProp_StartupMediaProfile,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaProfileSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaProfileSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaProfileSettings_Statics::ClassParams = {
		&UMediaProfileSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaProfileSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaProfileSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaProfileSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaProfileSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaProfileSettings, 3107402303);
	template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<UMediaProfileSettings>()
	{
		return UMediaProfileSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaProfileSettings(Z_Construct_UClass_UMediaProfileSettings, &UMediaProfileSettings::StaticClass, TEXT("/Script/MediaFrameworkUtilities"), TEXT("UMediaProfileSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaProfileSettings);
	void UMediaProfileEditorSettings::StaticRegisterNativesUMediaProfileEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaProfileEditorSettings_NoRegister()
	{
		return UMediaProfileEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaProfileEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayInToolbar_MetaData[];
#endif
		static void NewProp_bDisplayInToolbar_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayInToolbar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayInMainEditor_MetaData[];
#endif
		static void NewProp_bDisplayInMainEditor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayInMainEditor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserMediaProfile_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_UserMediaProfile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaProfileEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the media profile in the editor or standalone.\n * @note Cooked games don't use this media profile setting.\n */" },
		{ "IncludePath", "Profile/MediaProfileSettings.h" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "Settings for the media profile in the editor or standalone.\n@note Cooked games don't use this media profile setting." },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "Comment", "/**\n\x09 * Display the media profile icon in the editor toolbar.\n\x09 */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "Display the media profile icon in the editor toolbar." },
	};
#endif
	void Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar_SetBit(void* Obj)
	{
		((UMediaProfileEditorSettings*)Obj)->bDisplayInToolbar = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar = { "bDisplayInToolbar", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaProfileEditorSettings), &Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "Comment", "/** When enabled, the media profile name will be displayed in the main editor UI. */" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "When enabled, the media profile name will be displayed in the main editor UI." },
	};
#endif
	void Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor_SetBit(void* Obj)
	{
		((UMediaProfileEditorSettings*)Obj)->bDisplayInMainEditor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor = { "bDisplayInMainEditor", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaProfileEditorSettings), &Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_UserMediaProfile_MetaData[] = {
		{ "Category", "MediaProfile" },
		{ "Comment", "/**\n\x09 * The media profile to use in standalone & editor.\n\x09 * @note The startup media profile in the project setting will be used when in cooked game.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfileSettings.h" },
		{ "ToolTip", "The media profile to use in standalone & editor.\n@note The startup media profile in the project setting will be used when in cooked game." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_UserMediaProfile = { "UserMediaProfile", nullptr, (EPropertyFlags)0x0044000800004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfileEditorSettings, UserMediaProfile), Z_Construct_UClass_UMediaProfile_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_UserMediaProfile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_UserMediaProfile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaProfileEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInToolbar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_bDisplayInMainEditor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfileEditorSettings_Statics::NewProp_UserMediaProfile,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaProfileEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaProfileEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaProfileEditorSettings_Statics::ClassParams = {
		&UMediaProfileEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::PropPointers), 0),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfileEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaProfileEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaProfileEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaProfileEditorSettings, 3269977251);
	template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<UMediaProfileEditorSettings>()
	{
		return UMediaProfileEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaProfileEditorSettings(Z_Construct_UClass_UMediaProfileEditorSettings, &UMediaProfileEditorSettings::StaticClass, TEXT("/Script/MediaFrameworkUtilities"), TEXT("UMediaProfileEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaProfileEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
