// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/MediaBundleFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaBundleFactoryNew() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaBundleFactoryNew_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaBundleFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UActorFactoryMediaBundle_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UActorFactoryMediaBundle();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
// End Cross Module References
	void UMediaBundleFactoryNew::StaticRegisterNativesUMediaBundleFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UMediaBundleFactoryNew_NoRegister()
	{
		return UMediaBundleFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UMediaBundleFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaBundleFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaBundleFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UMediaPlayer objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "MediaBundleFactoryNew.h" },
		{ "ModuleRelativePath", "Private/MediaBundleFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UMediaPlayer objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaBundleFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaBundleFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaBundleFactoryNew_Statics::ClassParams = {
		&UMediaBundleFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaBundleFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaBundleFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaBundleFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaBundleFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaBundleFactoryNew, 3980563163);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaBundleFactoryNew>()
	{
		return UMediaBundleFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaBundleFactoryNew(Z_Construct_UClass_UMediaBundleFactoryNew, &UMediaBundleFactoryNew::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaBundleFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaBundleFactoryNew);
	void UActorFactoryMediaBundle::StaticRegisterNativesUActorFactoryMediaBundle()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryMediaBundle_NoRegister()
	{
		return UActorFactoryMediaBundle::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryMediaBundle_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryMediaBundle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryMediaBundle_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "MediaBundleFactoryNew.h" },
		{ "ModuleRelativePath", "Private/MediaBundleFactoryNew.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryMediaBundle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryMediaBundle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryMediaBundle_Statics::ClassParams = {
		&UActorFactoryMediaBundle::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryMediaBundle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryMediaBundle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryMediaBundle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryMediaBundle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryMediaBundle, 4272157144);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UActorFactoryMediaBundle>()
	{
		return UActorFactoryMediaBundle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryMediaBundle(Z_Construct_UClass_UActorFactoryMediaBundle, &UActorFactoryMediaBundle::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UActorFactoryMediaBundle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryMediaBundle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
