// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilities/Public/MediaAssets/ProxyMediaSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProxyMediaSource() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UProxyMediaSource_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UProxyMediaSource();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaSource();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilities();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaSource_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UProxyMediaSource::execIsProxyValid)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsProxyValid();
		P_NATIVE_END;
	}
	void UProxyMediaSource::StaticRegisterNativesUProxyMediaSource()
	{
		UClass* Class = UProxyMediaSource::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsProxyValid", &UProxyMediaSource::execIsProxyValid },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics
	{
		struct ProxyMediaSource_eventIsProxyValid_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ProxyMediaSource_eventIsProxyValid_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ProxyMediaSource_eventIsProxyValid_Parms), &Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media Proxy" },
		{ "Comment", "/**\n\x09 * Is the media proxy has a valid proxy.\n\x09 *\n\x09 * @return true if the proxy is valid.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaAssets/ProxyMediaSource.h" },
		{ "ToolTip", "Is the media proxy has a valid proxy.\n\n@return true if the proxy is valid." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UProxyMediaSource, nullptr, "IsProxyValid", nullptr, nullptr, sizeof(ProxyMediaSource_eventIsProxyValid_Parms), Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UProxyMediaSource_IsProxyValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UProxyMediaSource_IsProxyValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UProxyMediaSource_NoRegister()
	{
		return UProxyMediaSource::StaticClass();
	}
	struct Z_Construct_UClass_UProxyMediaSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Proxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Proxy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProxyMediaSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaSource,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilities,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UProxyMediaSource_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UProxyMediaSource_IsProxyValid, "IsProxyValid" }, // 2026727273
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProxyMediaSource_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A media source that reditect to another media source.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "MediaAssets/ProxyMediaSource.h" },
		{ "ModuleRelativePath", "Public/MediaAssets/ProxyMediaSource.h" },
		{ "ToolTip", "A media source that reditect to another media source." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_DynamicProxy_MetaData[] = {
		{ "Comment", "/** Cached media source proxy. */" },
		{ "ModuleRelativePath", "Public/MediaAssets/ProxyMediaSource.h" },
		{ "ToolTip", "Cached media source proxy." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_DynamicProxy = { "DynamicProxy", nullptr, (EPropertyFlags)0x0040000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProxyMediaSource, DynamicProxy), Z_Construct_UClass_UMediaSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_DynamicProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_DynamicProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_Proxy_MetaData[] = {
		{ "Category", "Media Proxy" },
		{ "Comment", "/** Media source proxy. */" },
		{ "ModuleRelativePath", "Public/MediaAssets/ProxyMediaSource.h" },
		{ "ToolTip", "Media source proxy." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_Proxy = { "Proxy", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UProxyMediaSource, Proxy), Z_Construct_UClass_UMediaSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_Proxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_Proxy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UProxyMediaSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_DynamicProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UProxyMediaSource_Statics::NewProp_Proxy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProxyMediaSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProxyMediaSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProxyMediaSource_Statics::ClassParams = {
		&UProxyMediaSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UProxyMediaSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UProxyMediaSource_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UProxyMediaSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProxyMediaSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProxyMediaSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProxyMediaSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProxyMediaSource, 3098679989);
	template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<UProxyMediaSource>()
	{
		return UProxyMediaSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProxyMediaSource(Z_Construct_UClass_UProxyMediaSource, &UProxyMediaSource::StaticClass, TEXT("/Script/MediaFrameworkUtilities"), TEXT("UProxyMediaSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProxyMediaSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
