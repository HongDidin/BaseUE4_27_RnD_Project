// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/CaptureTab/SMediaFrameworkCapture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSMediaFrameworkCapture() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData();
// End Cross Module References
	void UMediaFrameworkMediaCaptureSettings::StaticRegisterNativesUMediaFrameworkMediaCaptureSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_NoRegister()
	{
		return UMediaFrameworkMediaCaptureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVerticalSplitterOrientation_MetaData[];
#endif
		static void NewProp_bIsVerticalSplitterOrientation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVerticalSplitterOrientation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the media capture tab.\n */" },
		{ "IncludePath", "CaptureTab/SMediaFrameworkCapture.h" },
		{ "ModuleRelativePath", "Private/CaptureTab/SMediaFrameworkCapture.h" },
		{ "ToolTip", "Settings for the media capture tab." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Private/CaptureTab/SMediaFrameworkCapture.h" },
	};
#endif
	void Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation_SetBit(void* Obj)
	{
		((UMediaFrameworkMediaCaptureSettings*)Obj)->bIsVerticalSplitterOrientation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation = { "bIsVerticalSplitterOrientation", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaFrameworkMediaCaptureSettings), &Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::NewProp_bIsVerticalSplitterOrientation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkMediaCaptureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::ClassParams = {
		&UMediaFrameworkMediaCaptureSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkMediaCaptureSettings, 453955153);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkMediaCaptureSettings>()
	{
		return UMediaFrameworkMediaCaptureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkMediaCaptureSettings(Z_Construct_UClass_UMediaFrameworkMediaCaptureSettings, &UMediaFrameworkMediaCaptureSettings::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkMediaCaptureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkMediaCaptureSettings);
	void UMediaFrameworkEditorCaptureSettings::StaticRegisterNativesUMediaFrameworkEditorCaptureSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_NoRegister()
	{
		return UMediaFrameworkEditorCaptureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSaveCaptureSetingsInWorld_MetaData[];
#endif
		static void NewProp_bSaveCaptureSetingsInWorld_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSaveCaptureSetingsInWorld;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the capture that are persistent per users.\n */" },
		{ "IncludePath", "CaptureTab/SMediaFrameworkCapture.h" },
		{ "ModuleRelativePath", "Private/CaptureTab/SMediaFrameworkCapture.h" },
		{ "ToolTip", "Settings for the capture that are persistent per users." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld_MetaData[] = {
		{ "Comment", "/** Should the Capture setting be saved with the level or should it be saved as a project settings. */" },
		{ "ModuleRelativePath", "Private/CaptureTab/SMediaFrameworkCapture.h" },
		{ "ToolTip", "Should the Capture setting be saved with the level or should it be saved as a project settings." },
	};
#endif
	void Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld_SetBit(void* Obj)
	{
		((UMediaFrameworkEditorCaptureSettings*)Obj)->bSaveCaptureSetingsInWorld = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld = { "bSaveCaptureSetingsInWorld", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaFrameworkEditorCaptureSettings), &Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::NewProp_bSaveCaptureSetingsInWorld,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkEditorCaptureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::ClassParams = {
		&UMediaFrameworkEditorCaptureSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::PropPointers),
		0,
		0x002810A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkEditorCaptureSettings, 4103525277);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkEditorCaptureSettings>()
	{
		return UMediaFrameworkEditorCaptureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkEditorCaptureSettings(Z_Construct_UClass_UMediaFrameworkEditorCaptureSettings, &UMediaFrameworkEditorCaptureSettings::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkEditorCaptureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkEditorCaptureSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
