// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/Profile/MediaProfileSettingsCustomizationOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaProfileSettingsCustomizationOptions() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
class UScriptStruct* FMediaProfileSettingsCustomizationOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAFRAMEWORKUTILITIESEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions, Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor(), TEXT("MediaProfileSettingsCustomizationOptions"), sizeof(FMediaProfileSettingsCustomizationOptions), Get_Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Hash());
	}
	return Singleton;
}
template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<FMediaProfileSettingsCustomizationOptions>()
{
	return FMediaProfileSettingsCustomizationOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaProfileSettingsCustomizationOptions(FMediaProfileSettingsCustomizationOptions::StaticStruct, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("MediaProfileSettingsCustomizationOptions"), false, nullptr, nullptr);
static struct FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaProfileSettingsCustomizationOptions
{
	FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaProfileSettingsCustomizationOptions()
	{
		UScriptStruct::DeferCppStructOps<FMediaProfileSettingsCustomizationOptions>(FName(TEXT("MediaProfileSettingsCustomizationOptions")));
	}
} ScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaProfileSettingsCustomizationOptions;
	struct Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProxiesLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProxiesLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfSourceProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfSourceProxies;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfOutputProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfOutputProxies;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldCreateBundle_MetaData[];
#endif
		static void NewProp_bShouldCreateBundle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldCreateBundle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BundlesLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BundlesLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Private/Profile/MediaProfileSettingsCustomizationOptions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaProfileSettingsCustomizationOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_ProxiesLocation_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** The location where the proxies should be created. */" },
		{ "ContentDir", "" },
		{ "ModuleRelativePath", "Private/Profile/MediaProfileSettingsCustomizationOptions.h" },
		{ "ToolTip", "The location where the proxies should be created." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_ProxiesLocation = { "ProxiesLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaProfileSettingsCustomizationOptions, ProxiesLocation), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_ProxiesLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_ProxiesLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfSourceProxies_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** The number of input source Unreal may capture. */" },
		{ "ModuleRelativePath", "Private/Profile/MediaProfileSettingsCustomizationOptions.h" },
		{ "ToolTip", "The number of input source Unreal may capture." },
		{ "UIMax", "8" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfSourceProxies = { "NumberOfSourceProxies", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaProfileSettingsCustomizationOptions, NumberOfSourceProxies), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfSourceProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfSourceProxies_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfOutputProxies_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** The number of output Unreal may generate. */" },
		{ "ModuleRelativePath", "Private/Profile/MediaProfileSettingsCustomizationOptions.h" },
		{ "ToolTip", "The number of output Unreal may generate." },
		{ "UIMax", "8" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfOutputProxies = { "NumberOfOutputProxies", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaProfileSettingsCustomizationOptions, NumberOfOutputProxies), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfOutputProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfOutputProxies_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Create 1 media bundle for every source proxy created. */" },
		{ "ModuleRelativePath", "Private/Profile/MediaProfileSettingsCustomizationOptions.h" },
		{ "ToolTip", "Create 1 media bundle for every source proxy created." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle_SetBit(void* Obj)
	{
		((FMediaProfileSettingsCustomizationOptions*)Obj)->bShouldCreateBundle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle = { "bShouldCreateBundle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaProfileSettingsCustomizationOptions), &Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_BundlesLocation_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** The location where the bundles should be created. */" },
		{ "ContentDir", "" },
		{ "EditCondition", "bShouldCreateBundle" },
		{ "ModuleRelativePath", "Private/Profile/MediaProfileSettingsCustomizationOptions.h" },
		{ "ToolTip", "The location where the bundles should be created." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_BundlesLocation = { "BundlesLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaProfileSettingsCustomizationOptions, BundlesLocation), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_BundlesLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_BundlesLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_ProxiesLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfSourceProxies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_NumberOfOutputProxies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_bShouldCreateBundle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::NewProp_BundlesLocation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
		nullptr,
		&NewStructOps,
		"MediaProfileSettingsCustomizationOptions",
		sizeof(FMediaProfileSettingsCustomizationOptions),
		alignof(FMediaProfileSettingsCustomizationOptions),
		Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaProfileSettingsCustomizationOptions"), sizeof(FMediaProfileSettingsCustomizationOptions), Get_Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Hash() { return 428988874U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
