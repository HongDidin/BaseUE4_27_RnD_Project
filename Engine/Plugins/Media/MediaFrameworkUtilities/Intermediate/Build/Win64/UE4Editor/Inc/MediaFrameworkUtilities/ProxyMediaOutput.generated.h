// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAFRAMEWORKUTILITIES_ProxyMediaOutput_generated_h
#error "ProxyMediaOutput.generated.h already included, missing '#pragma once' in ProxyMediaOutput.h"
#endif
#define MEDIAFRAMEWORKUTILITIES_ProxyMediaOutput_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsProxyValid);


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsProxyValid);


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUProxyMediaOutput(); \
	friend struct Z_Construct_UClass_UProxyMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UProxyMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UProxyMediaOutput)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUProxyMediaOutput(); \
	friend struct Z_Construct_UClass_UProxyMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UProxyMediaOutput, UMediaOutput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UProxyMediaOutput)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UProxyMediaOutput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UProxyMediaOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UProxyMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UProxyMediaOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UProxyMediaOutput(UProxyMediaOutput&&); \
	NO_API UProxyMediaOutput(const UProxyMediaOutput&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UProxyMediaOutput(UProxyMediaOutput&&); \
	NO_API UProxyMediaOutput(const UProxyMediaOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UProxyMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UProxyMediaOutput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UProxyMediaOutput)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DynamicProxy() { return STRUCT_OFFSET(UProxyMediaOutput, DynamicProxy); } \
	FORCEINLINE static uint32 __PPO__Proxy() { return STRUCT_OFFSET(UProxyMediaOutput, Proxy); }


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_13_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<class UProxyMediaOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaAssets_ProxyMediaOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
