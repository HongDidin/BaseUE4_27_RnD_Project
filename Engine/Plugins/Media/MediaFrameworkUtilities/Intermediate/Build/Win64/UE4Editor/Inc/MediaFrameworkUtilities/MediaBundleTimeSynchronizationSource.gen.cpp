// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilities/Public/MediaBundleTimeSynchronizationSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaBundleTimeSynchronizationSource() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaBundleTimeSynchronizationSource();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UTimeSynchronizationSource();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilities();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaBundle_NoRegister();
// End Cross Module References
	void UMediaBundleTimeSynchronizationSource::StaticRegisterNativesUMediaBundleTimeSynchronizationSource()
	{
	}
	UClass* Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_NoRegister()
	{
		return UMediaBundleTimeSynchronizationSource::StaticClass();
	}
	struct Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaBundle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaBundle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimeSynchronizationSource,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Synchronization Source using the Media Bundle\n */" },
		{ "IncludePath", "MediaBundleTimeSynchronizationSource.h" },
		{ "ModuleRelativePath", "Public/MediaBundleTimeSynchronizationSource.h" },
		{ "ToolTip", "Synchronization Source using the Media Bundle" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::NewProp_MediaBundle_MetaData[] = {
		{ "Category", "Player" },
		{ "Comment", "/* Media bundle asset of this input*/" },
		{ "ModuleRelativePath", "Public/MediaBundleTimeSynchronizationSource.h" },
		{ "ToolTip", "Media bundle asset of this input" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::NewProp_MediaBundle = { "MediaBundle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaBundleTimeSynchronizationSource, MediaBundle), Z_Construct_UClass_UMediaBundle_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::NewProp_MediaBundle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::NewProp_MediaBundle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::NewProp_MediaBundle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaBundleTimeSynchronizationSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::ClassParams = {
		&UMediaBundleTimeSynchronizationSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaBundleTimeSynchronizationSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaBundleTimeSynchronizationSource, 709908329);
	template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<UMediaBundleTimeSynchronizationSource>()
	{
		return UMediaBundleTimeSynchronizationSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaBundleTimeSynchronizationSource(Z_Construct_UClass_UMediaBundleTimeSynchronizationSource, &UMediaBundleTimeSynchronizationSource::StaticClass, TEXT("/Script/MediaFrameworkUtilities"), TEXT("UMediaBundleTimeSynchronizationSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaBundleTimeSynchronizationSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
