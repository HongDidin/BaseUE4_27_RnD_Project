// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/VideoInputTab/SMediaFrameworkVideoInputDisplay.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSMediaFrameworkVideoInputDisplay() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
// End Cross Module References
	DEFINE_FUNCTION(UMediaFrameworkVideoInputDisplayCallback::execOnMediaClosed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnMediaClosed();
		P_NATIVE_END;
	}
	void UMediaFrameworkVideoInputDisplayCallback::StaticRegisterNativesUMediaFrameworkVideoInputDisplayCallback()
	{
		UClass* Class = UMediaFrameworkVideoInputDisplayCallback::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnMediaClosed", &UMediaFrameworkVideoInputDisplayCallback::execOnMediaClosed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/VideoInputTab/SMediaFrameworkVideoInputDisplay.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback, nullptr, "OnMediaClosed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_NoRegister()
	{
		return UMediaFrameworkVideoInputDisplayCallback::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMediaFrameworkVideoInputDisplayCallback_OnMediaClosed, "OnMediaClosed" }, // 3709667805
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Callback object for SMediaFrameworkVideoInputDisplay\n */" },
		{ "IncludePath", "VideoInputTab/SMediaFrameworkVideoInputDisplay.h" },
		{ "ModuleRelativePath", "Private/VideoInputTab/SMediaFrameworkVideoInputDisplay.h" },
		{ "ToolTip", "Callback object for SMediaFrameworkVideoInputDisplay" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkVideoInputDisplayCallback>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::ClassParams = {
		&UMediaFrameworkVideoInputDisplayCallback::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkVideoInputDisplayCallback, 1516256281);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkVideoInputDisplayCallback>()
	{
		return UMediaFrameworkVideoInputDisplayCallback::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkVideoInputDisplayCallback(Z_Construct_UClass_UMediaFrameworkVideoInputDisplayCallback, &UMediaFrameworkVideoInputDisplayCallback::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkVideoInputDisplayCallback"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkVideoInputDisplayCallback);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
