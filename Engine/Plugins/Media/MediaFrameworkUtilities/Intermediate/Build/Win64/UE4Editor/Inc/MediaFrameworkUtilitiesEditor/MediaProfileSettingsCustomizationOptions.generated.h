// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAFRAMEWORKUTILITIESEDITOR_MediaProfileSettingsCustomizationOptions_generated_h
#error "MediaProfileSettingsCustomizationOptions.generated.h already included, missing '#pragma once' in MediaProfileSettingsCustomizationOptions.h"
#endif
#define MEDIAFRAMEWORKUTILITIESEDITOR_MediaProfileSettingsCustomizationOptions_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_Profile_MediaProfileSettingsCustomizationOptions_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaProfileSettingsCustomizationOptions_Statics; \
	MEDIAFRAMEWORKUTILITIESEDITOR_API static class UScriptStruct* StaticStruct();


template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<struct FMediaProfileSettingsCustomizationOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_Profile_MediaProfileSettingsCustomizationOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
