// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAFRAMEWORKUTILITIESEDITOR_MediaFrameworkVideoInputSettings_generated_h
#error "MediaFrameworkVideoInputSettings.generated.h already included, missing '#pragma once' in MediaFrameworkVideoInputSettings.h"
#endif
#define MEDIAFRAMEWORKUTILITIESEDITOR_MediaFrameworkVideoInputSettings_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics; \
	MEDIAFRAMEWORKUTILITIESEDITOR_API static class UScriptStruct* StaticStruct();


template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<struct FMediaFrameworkVideoInputSourceSettings>();

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaFrameworkVideoInputSettings(); \
	friend struct Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics; \
public: \
	DECLARE_CLASS(UMediaFrameworkVideoInputSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilitiesEditor"), MEDIAFRAMEWORKUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UMediaFrameworkVideoInputSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUMediaFrameworkVideoInputSettings(); \
	friend struct Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics; \
public: \
	DECLARE_CLASS(UMediaFrameworkVideoInputSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilitiesEditor"), MEDIAFRAMEWORKUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UMediaFrameworkVideoInputSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UMediaFrameworkVideoInputSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaFrameworkVideoInputSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIAFRAMEWORKUTILITIESEDITOR_API, UMediaFrameworkVideoInputSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaFrameworkVideoInputSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UMediaFrameworkVideoInputSettings(UMediaFrameworkVideoInputSettings&&); \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UMediaFrameworkVideoInputSettings(const UMediaFrameworkVideoInputSettings&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UMediaFrameworkVideoInputSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UMediaFrameworkVideoInputSettings(UMediaFrameworkVideoInputSettings&&); \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UMediaFrameworkVideoInputSettings(const UMediaFrameworkVideoInputSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIAFRAMEWORKUTILITIESEDITOR_API, UMediaFrameworkVideoInputSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaFrameworkVideoInputSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaFrameworkVideoInputSettings)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_28_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<class UMediaFrameworkVideoInputSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaFrameworkVideoInputSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
