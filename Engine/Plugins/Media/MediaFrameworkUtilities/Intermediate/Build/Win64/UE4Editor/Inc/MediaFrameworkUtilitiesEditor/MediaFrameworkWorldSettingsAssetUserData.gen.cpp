// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/MediaFrameworkWorldSettingsAssetUserData.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaFrameworkWorldSettingsAssetUserData() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaCaptureOptions();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EViewModeIndex();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData();
// End Cross Module References
class UScriptStruct* FMediaFrameworkCaptureRenderTargetCameraOutputInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAFRAMEWORKUTILITIESEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo, Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor(), TEXT("MediaFrameworkCaptureRenderTargetCameraOutputInfo"), sizeof(FMediaFrameworkCaptureRenderTargetCameraOutputInfo), Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Hash());
	}
	return Singleton;
}
template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<FMediaFrameworkCaptureRenderTargetCameraOutputInfo>()
{
	return FMediaFrameworkCaptureRenderTargetCameraOutputInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo(FMediaFrameworkCaptureRenderTargetCameraOutputInfo::StaticStruct, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("MediaFrameworkCaptureRenderTargetCameraOutputInfo"), false, nullptr, nullptr);
static struct FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureRenderTargetCameraOutputInfo
{
	FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureRenderTargetCameraOutputInfo()
	{
		UScriptStruct::DeferCppStructOps<FMediaFrameworkCaptureRenderTargetCameraOutputInfo>(FName(TEXT("MediaFrameworkCaptureRenderTargetCameraOutputInfo")));
	}
} ScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureRenderTargetCameraOutputInfo;
	struct Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * FMediaFrameworkCaptureRenderTargetCameraOutputInfo\n */" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ToolTip", "FMediaFrameworkCaptureRenderTargetCameraOutputInfo" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaFrameworkCaptureRenderTargetCameraOutputInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_RenderTarget_MetaData[] = {
		{ "Category", "MediaRenderTargetCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureRenderTargetCameraOutputInfo, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_RenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_RenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "Category", "MediaRenderTargetCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureRenderTargetCameraOutputInfo, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_CaptureOptions_MetaData[] = {
		{ "Category", "MediaRenderTargetCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureRenderTargetCameraOutputInfo, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_CaptureOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_CaptureOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::NewProp_CaptureOptions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
		nullptr,
		&NewStructOps,
		"MediaFrameworkCaptureRenderTargetCameraOutputInfo",
		sizeof(FMediaFrameworkCaptureRenderTargetCameraOutputInfo),
		alignof(FMediaFrameworkCaptureRenderTargetCameraOutputInfo),
		Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaFrameworkCaptureRenderTargetCameraOutputInfo"), sizeof(FMediaFrameworkCaptureRenderTargetCameraOutputInfo), Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo_Hash() { return 2914885411U; }
class UScriptStruct* FMediaFrameworkCaptureCameraViewportCameraOutputInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAFRAMEWORKUTILITIESEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo, Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor(), TEXT("MediaFrameworkCaptureCameraViewportCameraOutputInfo"), sizeof(FMediaFrameworkCaptureCameraViewportCameraOutputInfo), Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Hash());
	}
	return Singleton;
}
template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<FMediaFrameworkCaptureCameraViewportCameraOutputInfo>()
{
	return FMediaFrameworkCaptureCameraViewportCameraOutputInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo(FMediaFrameworkCaptureCameraViewportCameraOutputInfo::StaticStruct, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("MediaFrameworkCaptureCameraViewportCameraOutputInfo"), false, nullptr, nullptr);
static struct FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureCameraViewportCameraOutputInfo
{
	FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureCameraViewportCameraOutputInfo()
	{
		UScriptStruct::DeferCppStructOps<FMediaFrameworkCaptureCameraViewportCameraOutputInfo>(FName(TEXT("MediaFrameworkCaptureCameraViewportCameraOutputInfo")));
	}
} ScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureCameraViewportCameraOutputInfo;
	struct Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_LockedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LockedActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewMode;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LockedCameraActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockedCameraActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LockedCameraActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * FMediaFrameworkCaptureCameraViewportCameraOutputInfo\n */" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ToolTip", "FMediaFrameworkCaptureCameraViewportCameraOutputInfo" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaFrameworkCaptureCameraViewportCameraOutputInfo>();
	}
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors_Inner = { "LockedActors", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors_MetaData[] = {
		{ "Category", "MediaViewportCapture" },
		{ "DisplayName", "Cameras" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors = { "LockedActors", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCameraViewportCameraOutputInfo, LockedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "Category", "MediaViewportCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCameraViewportCameraOutputInfo, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_CaptureOptions_MetaData[] = {
		{ "Category", "MediaViewportCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCameraViewportCameraOutputInfo, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_CaptureOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_CaptureOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_ViewMode_MetaData[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_ViewMode = { "ViewMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCameraViewportCameraOutputInfo, ViewMode), Z_Construct_UEnum_Engine_EViewModeIndex, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_ViewMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_ViewMode_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors_Inner = { "LockedCameraActors", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors_MetaData[] = {
		{ "Comment", "//DEPRECATED 4.21 The type of LockedCameraActors has changed and will be removed from the code base in a future release. Use LockedActors.\n" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ToolTip", "DEPRECATED 4.21 The type of LockedCameraActors has changed and will be removed from the code base in a future release. Use LockedActors." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors = { "LockedCameraActors", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCameraViewportCameraOutputInfo, LockedCameraActors_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_CaptureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_ViewMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::NewProp_LockedCameraActors,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
		nullptr,
		&NewStructOps,
		"MediaFrameworkCaptureCameraViewportCameraOutputInfo",
		sizeof(FMediaFrameworkCaptureCameraViewportCameraOutputInfo),
		alignof(FMediaFrameworkCaptureCameraViewportCameraOutputInfo),
		Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaFrameworkCaptureCameraViewportCameraOutputInfo"), sizeof(FMediaFrameworkCaptureCameraViewportCameraOutputInfo), Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo_Hash() { return 3300856816U; }
class UScriptStruct* FMediaFrameworkCaptureCurrentViewportOutputInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAFRAMEWORKUTILITIESEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo, Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor(), TEXT("MediaFrameworkCaptureCurrentViewportOutputInfo"), sizeof(FMediaFrameworkCaptureCurrentViewportOutputInfo), Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Hash());
	}
	return Singleton;
}
template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<FMediaFrameworkCaptureCurrentViewportOutputInfo>()
{
	return FMediaFrameworkCaptureCurrentViewportOutputInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo(FMediaFrameworkCaptureCurrentViewportOutputInfo::StaticStruct, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("MediaFrameworkCaptureCurrentViewportOutputInfo"), false, nullptr, nullptr);
static struct FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureCurrentViewportOutputInfo
{
	FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureCurrentViewportOutputInfo()
	{
		UScriptStruct::DeferCppStructOps<FMediaFrameworkCaptureCurrentViewportOutputInfo>(FName(TEXT("MediaFrameworkCaptureCurrentViewportOutputInfo")));
	}
} ScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkCaptureCurrentViewportOutputInfo;
	struct Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * FMediaFrameworkCaptureCurrentViewportOutputInfo\n */" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ToolTip", "FMediaFrameworkCaptureCurrentViewportOutputInfo" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaFrameworkCaptureCurrentViewportOutputInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "Category", "MediaViewportCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCurrentViewportOutputInfo, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_CaptureOptions_MetaData[] = {
		{ "Category", "MediaViewportCapture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCurrentViewportOutputInfo, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_CaptureOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_CaptureOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_ViewMode_MetaData[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_ViewMode = { "ViewMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkCaptureCurrentViewportOutputInfo, ViewMode), Z_Construct_UEnum_Engine_EViewModeIndex, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_ViewMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_ViewMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_CaptureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::NewProp_ViewMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
		nullptr,
		&NewStructOps,
		"MediaFrameworkCaptureCurrentViewportOutputInfo",
		sizeof(FMediaFrameworkCaptureCurrentViewportOutputInfo),
		alignof(FMediaFrameworkCaptureCurrentViewportOutputInfo),
		Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaFrameworkCaptureCurrentViewportOutputInfo"), sizeof(FMediaFrameworkCaptureCurrentViewportOutputInfo), Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo_Hash() { return 3986773381U; }
	void UMediaFrameworkWorldSettingsAssetUserData::StaticRegisterNativesUMediaFrameworkWorldSettingsAssetUserData()
	{
	}
	UClass* Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_NoRegister()
	{
		return UMediaFrameworkWorldSettingsAssetUserData::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderTargetCaptures_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTargetCaptures_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RenderTargetCaptures;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportCaptures_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportCaptures_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportCaptures;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentViewportMediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentViewportMediaOutput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetUserData,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UMediaFrameworkCaptureCameraViewportAssetUserData\n */" },
		{ "IncludePath", "MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ToolTip", "UMediaFrameworkCaptureCameraViewportAssetUserData" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures_Inner = { "RenderTargetCaptures", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMediaFrameworkCaptureRenderTargetCameraOutputInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures_MetaData[] = {
		{ "Category", "Media Render Target Capture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures = { "RenderTargetCaptures", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaFrameworkWorldSettingsAssetUserData, RenderTargetCaptures), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures_Inner = { "ViewportCaptures", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMediaFrameworkCaptureCameraViewportCameraOutputInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures_MetaData[] = {
		{ "Category", "Media Viewport Capture" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures = { "ViewportCaptures", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaFrameworkWorldSettingsAssetUserData, ViewportCaptures), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_CurrentViewportMediaOutput_MetaData[] = {
		{ "Category", "Media Current Viewport Capture" },
		{ "Comment", "/**\n\x09 * Capture the current viewport. It may be the level editor active viewport or a PIE instance launch with \"New Editor Window PIE\".\n\x09 * @note The behavior is different from MediaCapture.CaptureActiveSceneViewport. Here we can capture the editor viewport (since we are in the editor).\n\x09 * @note If the viewport is the level editor active viewport, then all inputs will be disabled and the viewport will always rendered.\n\x09 */" },
		{ "DisplayName", "Current Viewport" },
		{ "ModuleRelativePath", "Private/MediaFrameworkWorldSettingsAssetUserData.h" },
		{ "ToolTip", "Capture the current viewport. It may be the level editor active viewport or a PIE instance launch with \"New Editor Window PIE\".\n@note The behavior is different from MediaCapture.CaptureActiveSceneViewport. Here we can capture the editor viewport (since we are in the editor).\n@note If the viewport is the level editor active viewport, then all inputs will be disabled and the viewport will always rendered." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_CurrentViewportMediaOutput = { "CurrentViewportMediaOutput", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaFrameworkWorldSettingsAssetUserData, CurrentViewportMediaOutput), Z_Construct_UScriptStruct_FMediaFrameworkCaptureCurrentViewportOutputInfo, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_CurrentViewportMediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_CurrentViewportMediaOutput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_RenderTargetCaptures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_ViewportCaptures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::NewProp_CurrentViewportMediaOutput,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkWorldSettingsAssetUserData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::ClassParams = {
		&UMediaFrameworkWorldSettingsAssetUserData::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::PropPointers),
		0,
		0x002810A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkWorldSettingsAssetUserData, 2352425248);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkWorldSettingsAssetUserData>()
	{
		return UMediaFrameworkWorldSettingsAssetUserData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkWorldSettingsAssetUserData(Z_Construct_UClass_UMediaFrameworkWorldSettingsAssetUserData, &UMediaFrameworkWorldSettingsAssetUserData::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkWorldSettingsAssetUserData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkWorldSettingsAssetUserData);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UMediaFrameworkWorldSettingsAssetUserData)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
