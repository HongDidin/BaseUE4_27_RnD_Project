// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/MediaFrameworkVideoInputSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaFrameworkVideoInputSettings() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaSource_NoRegister();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaTexture_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkVideoInputSettings_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkVideoInputSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaBundle_NoRegister();
// End Cross Module References
class UScriptStruct* FMediaFrameworkVideoInputSourceSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAFRAMEWORKUTILITIESEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings, Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor(), TEXT("MediaFrameworkVideoInputSourceSettings"), sizeof(FMediaFrameworkVideoInputSourceSettings), Get_Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Hash());
	}
	return Singleton;
}
template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UScriptStruct* StaticStruct<FMediaFrameworkVideoInputSourceSettings>()
{
	return FMediaFrameworkVideoInputSourceSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings(FMediaFrameworkVideoInputSourceSettings::StaticStruct, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("MediaFrameworkVideoInputSourceSettings"), false, nullptr, nullptr);
static struct FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkVideoInputSourceSettings
{
	FScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkVideoInputSourceSettings()
	{
		UScriptStruct::DeferCppStructOps<FMediaFrameworkVideoInputSourceSettings>(FName(TEXT("MediaFrameworkVideoInputSourceSettings")));
	}
} ScriptStruct_MediaFrameworkUtilitiesEditor_StaticRegisterNativesFMediaFrameworkVideoInputSourceSettings;
	struct Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MediaSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MediaTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaFrameworkVideoInputSourceSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaSource_MetaData[] = {
		{ "Category", "Media" },
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaSource = { "MediaSource", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkVideoInputSourceSettings, MediaSource), Z_Construct_UClass_UMediaSource_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaTexture_MetaData[] = {
		{ "Category", "Media" },
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaTexture = { "MediaTexture", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaFrameworkVideoInputSourceSettings, MediaTexture), Z_Construct_UClass_UMediaTexture_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::NewProp_MediaTexture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
		nullptr,
		&NewStructOps,
		"MediaFrameworkVideoInputSourceSettings",
		sizeof(FMediaFrameworkVideoInputSourceSettings),
		alignof(FMediaFrameworkVideoInputSourceSettings),
		Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaFrameworkVideoInputSourceSettings"), sizeof(FMediaFrameworkVideoInputSourceSettings), Get_Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings_Hash() { return 2341479779U; }
	void UMediaFrameworkVideoInputSettings::StaticRegisterNativesUMediaFrameworkVideoInputSettings()
	{
	}
	UClass* Z_Construct_UClass_UMediaFrameworkVideoInputSettings_NoRegister()
	{
		return UMediaFrameworkVideoInputSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MediaBundles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaBundles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MediaBundles;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MediaSources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReopenMediaBundles_MetaData[];
#endif
		static void NewProp_bReopenMediaBundles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReopenMediaBundles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReopenMediaSources_MetaData[];
#endif
		static void NewProp_bReopenMediaSources_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReopenMediaSources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReopenDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReopenDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVerticalSplitterOrientation_MetaData[];
#endif
		static void NewProp_bIsVerticalSplitterOrientation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVerticalSplitterOrientation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the video input tab.\n */" },
		{ "IncludePath", "MediaFrameworkVideoInputSettings.h" },
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
		{ "ToolTip", "Settings for the video input tab." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles_Inner = { "MediaBundles", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMediaBundle_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles_MetaData[] = {
		{ "Category", "Media Bundle" },
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles = { "MediaBundles", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaFrameworkVideoInputSettings, MediaBundles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources_Inner = { "MediaSources", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMediaFrameworkVideoInputSourceSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources_MetaData[] = {
		{ "Category", "Media Source" },
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources = { "MediaSources", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaFrameworkVideoInputSettings, MediaSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles_MetaData[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	void Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles_SetBit(void* Obj)
	{
		((UMediaFrameworkVideoInputSettings*)Obj)->bReopenMediaBundles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles = { "bReopenMediaBundles", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaFrameworkVideoInputSettings), &Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources_MetaData[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	void Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources_SetBit(void* Obj)
	{
		((UMediaFrameworkVideoInputSettings*)Obj)->bReopenMediaSources = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources = { "bReopenMediaSources", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaFrameworkVideoInputSettings), &Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_ReopenDelay_MetaData[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_ReopenDelay = { "ReopenDelay", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaFrameworkVideoInputSettings, ReopenDelay), METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_ReopenDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_ReopenDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Private/MediaFrameworkVideoInputSettings.h" },
	};
#endif
	void Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation_SetBit(void* Obj)
	{
		((UMediaFrameworkVideoInputSettings*)Obj)->bIsVerticalSplitterOrientation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation = { "bIsVerticalSplitterOrientation", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaFrameworkVideoInputSettings), &Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaBundles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_MediaSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaBundles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bReopenMediaSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_ReopenDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::NewProp_bIsVerticalSplitterOrientation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkVideoInputSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::ClassParams = {
		&UMediaFrameworkVideoInputSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkVideoInputSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkVideoInputSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkVideoInputSettings, 4057858515);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkVideoInputSettings>()
	{
		return UMediaFrameworkVideoInputSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkVideoInputSettings(Z_Construct_UClass_UMediaFrameworkVideoInputSettings, &UMediaFrameworkVideoInputSettings::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkVideoInputSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkVideoInputSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
