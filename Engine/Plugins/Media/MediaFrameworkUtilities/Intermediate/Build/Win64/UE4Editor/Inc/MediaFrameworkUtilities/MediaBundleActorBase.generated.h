// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class UMediaSoundComponent;
class UMediaBundle;
#ifdef MEDIAFRAMEWORKUTILITIES_MediaBundleActorBase_generated_h
#error "MediaBundleActorBase.generated.h already included, missing '#pragma once' in MediaBundleActorBase.h"
#endif
#define MEDIAFRAMEWORKUTILITIES_MediaBundleActorBase_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetComponent); \
	DECLARE_FUNCTION(execRequestCloseMediaSource); \
	DECLARE_FUNCTION(execRequestOpenMediaSource); \
	DECLARE_FUNCTION(execGetMediaBundle);


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetComponent); \
	DECLARE_FUNCTION(execRequestCloseMediaSource); \
	DECLARE_FUNCTION(execRequestOpenMediaSource); \
	DECLARE_FUNCTION(execGetMediaBundle);


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMediaBundleActorBase(); \
	friend struct Z_Construct_UClass_AMediaBundleActorBase_Statics; \
public: \
	DECLARE_CLASS(AMediaBundleActorBase, AActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(AMediaBundleActorBase)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_INCLASS \
private: \
	static void StaticRegisterNativesAMediaBundleActorBase(); \
	friend struct Z_Construct_UClass_AMediaBundleActorBase_Statics; \
public: \
	DECLARE_CLASS(AMediaBundleActorBase, AActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(AMediaBundleActorBase)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMediaBundleActorBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMediaBundleActorBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMediaBundleActorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMediaBundleActorBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMediaBundleActorBase(AMediaBundleActorBase&&); \
	NO_API AMediaBundleActorBase(const AMediaBundleActorBase&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMediaBundleActorBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMediaBundleActorBase(AMediaBundleActorBase&&); \
	NO_API AMediaBundleActorBase(const AMediaBundleActorBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMediaBundleActorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMediaBundleActorBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMediaBundleActorBase)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaBundle() { return STRUCT_OFFSET(AMediaBundleActorBase, MediaBundle); } \
	FORCEINLINE static uint32 __PPO__bAutoPlay() { return STRUCT_OFFSET(AMediaBundleActorBase, bAutoPlay); } \
	FORCEINLINE static uint32 __PPO__bPlayWhileEditing() { return STRUCT_OFFSET(AMediaBundleActorBase, bPlayWhileEditing); } \
	FORCEINLINE static uint32 __PPO__PrimitiveCmp() { return STRUCT_OFFSET(AMediaBundleActorBase, PrimitiveCmp); } \
	FORCEINLINE static uint32 __PPO__MediaSoundCmp() { return STRUCT_OFFSET(AMediaBundleActorBase, MediaSoundCmp); } \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(AMediaBundleActorBase, Material); } \
	FORCEINLINE static uint32 __PPO__PrimitiveMaterialIndex() { return STRUCT_OFFSET(AMediaBundleActorBase, PrimitiveMaterialIndex); }


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_20_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<class AMediaBundleActorBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleActorBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
