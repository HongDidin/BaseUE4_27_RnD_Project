// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilities/Public/Profile/MediaProfile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaProfile() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfile_NoRegister();
	MEDIAFRAMEWORKUTILITIES_API UClass* Z_Construct_UClass_UMediaProfile();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilities();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaSource_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTimecodeProvider_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UEngineCustomTimeStep_NoRegister();
// End Cross Module References
	void UMediaProfile::StaticRegisterNativesUMediaProfile()
	{
	}
	UClass* Z_Construct_UClass_UMediaProfile_NoRegister()
	{
		return UMediaProfile::StaticClass();
	}
	struct Z_Construct_UClass_UMediaProfile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSources_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MediaSources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutputs_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MediaOutputs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideTimecodeProvider_MetaData[];
#endif
		static void NewProp_bOverrideTimecodeProvider_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideTimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeProvider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideCustomTimeStep_MetaData[];
#endif
		static void NewProp_bOverrideCustomTimeStep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideCustomTimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomTimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CustomTimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppliedTimecodeProvider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AppliedTimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousTimecodeProvider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousTimecodeProvider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AppliedCustomTimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AppliedCustomTimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousCustomTimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousCustomTimeStep;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaProfile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A media profile that configures the inputs, outputs, timecode provider and custom time step.\n */" },
		{ "IncludePath", "Profile/MediaProfile.h" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "A media profile that configures the inputs, outputs, timecode provider and custom time step." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_Inner_MetaData[] = {
		{ "Category", "Inputs" },
		{ "Comment", "/** Media sources. */" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Media sources." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_Inner = { "MediaSources", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMediaSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_MetaData[] = {
		{ "Category", "Inputs" },
		{ "Comment", "/** Media sources. */" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Media sources." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources = { "MediaSources", nullptr, (EPropertyFlags)0x0020088000000049, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, MediaSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_Inner_MetaData[] = {
		{ "Category", "Outputs" },
		{ "Comment", "/** Media outputs. */" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Media outputs." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_Inner = { "MediaOutputs", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_MetaData[] = {
		{ "Category", "Outputs" },
		{ "Comment", "/** Media outputs. */" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Media outputs." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs = { "MediaOutputs", nullptr, (EPropertyFlags)0x0020088000000049, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, MediaOutputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider_MetaData[] = {
		{ "Category", "Timecode Provider" },
		{ "Comment", "/** Override the Engine's Timecode provider defined in the project settings. */" },
		{ "DisplayName", "Override Project Settings" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Override the Engine's Timecode provider defined in the project settings." },
	};
#endif
	void Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider_SetBit(void* Obj)
	{
		((UMediaProfile*)Obj)->bOverrideTimecodeProvider = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider = { "bOverrideTimecodeProvider", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaProfile), &Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_TimecodeProvider_MetaData[] = {
		{ "Category", "Timecode Provider" },
		{ "Comment", "/** Timecode provider. */" },
		{ "EditCondition", "bOverrideTimecodeProvider" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Timecode provider." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_TimecodeProvider = { "TimecodeProvider", nullptr, (EPropertyFlags)0x0022080000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, TimecodeProvider), Z_Construct_UClass_UTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_TimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_TimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep_MetaData[] = {
		{ "Category", "Genlock" },
		{ "Comment", "/** Override the Engine's Custom time step defined in the project settings. */" },
		{ "DisplayName", "Override Project Settings" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Override the Engine's Custom time step defined in the project settings." },
	};
#endif
	void Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep_SetBit(void* Obj)
	{
		((UMediaProfile*)Obj)->bOverrideCustomTimeStep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep = { "bOverrideCustomTimeStep", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMediaProfile), &Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_CustomTimeStep_MetaData[] = {
		{ "Category", "Genlock" },
		{ "Comment", "/** Custom time step */" },
		{ "EditCondition", "bOverrideCustomTimeStep" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
		{ "ToolTip", "Custom time step" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_CustomTimeStep = { "CustomTimeStep", nullptr, (EPropertyFlags)0x0022080000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, CustomTimeStep), Z_Construct_UClass_UEngineCustomTimeStep_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_CustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_CustomTimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedTimecodeProvider_MetaData[] = {
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedTimecodeProvider = { "AppliedTimecodeProvider", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, AppliedTimecodeProvider), Z_Construct_UClass_UTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedTimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedTimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousTimecodeProvider_MetaData[] = {
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousTimecodeProvider = { "PreviousTimecodeProvider", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, PreviousTimecodeProvider), Z_Construct_UClass_UTimecodeProvider_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousTimecodeProvider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousTimecodeProvider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedCustomTimeStep_MetaData[] = {
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedCustomTimeStep = { "AppliedCustomTimeStep", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, AppliedCustomTimeStep), Z_Construct_UClass_UEngineCustomTimeStep_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedCustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedCustomTimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousCustomTimeStep_MetaData[] = {
		{ "ModuleRelativePath", "Public/Profile/MediaProfile.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousCustomTimeStep = { "PreviousCustomTimeStep", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaProfile, PreviousCustomTimeStep), Z_Construct_UClass_UEngineCustomTimeStep_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousCustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousCustomTimeStep_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_MediaOutputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideTimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_TimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_bOverrideCustomTimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_CustomTimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedTimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousTimecodeProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_AppliedCustomTimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaProfile_Statics::NewProp_PreviousCustomTimeStep,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaProfile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaProfile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaProfile_Statics::ClassParams = {
		&UMediaProfile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaProfile_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaProfile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaProfile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaProfile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaProfile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaProfile, 633379324);
	template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<UMediaProfile>()
	{
		return UMediaProfile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaProfile(Z_Construct_UClass_UMediaProfile, &UMediaProfile::StaticClass, TEXT("/Script/MediaFrameworkUtilities"), TEXT("UMediaProfile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaProfile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
