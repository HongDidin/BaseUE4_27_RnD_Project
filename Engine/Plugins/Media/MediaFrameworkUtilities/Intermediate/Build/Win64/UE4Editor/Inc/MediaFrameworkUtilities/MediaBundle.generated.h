// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FOpenCVCameraViewInfo;
class UMediaSource;
class UTextureRenderTarget2D;
class UMediaTexture;
class UMediaPlayer;
class UMaterialInterface;
#ifdef MEDIAFRAMEWORKUTILITIES_MediaBundle_generated_h
#error "MediaBundle.generated.h already included, missing '#pragma once' in MediaBundle.h"
#endif
#define MEDIAFRAMEWORKUTILITIES_MediaBundle_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnMediaOpenFailed); \
	DECLARE_FUNCTION(execOnMediaOpenOpened); \
	DECLARE_FUNCTION(execOnMediaClosed); \
	DECLARE_FUNCTION(execGetUndistortedCameraViewInfo); \
	DECLARE_FUNCTION(execGetMediaSource); \
	DECLARE_FUNCTION(execGetLensDisplacementTexture); \
	DECLARE_FUNCTION(execGetMediaTexture); \
	DECLARE_FUNCTION(execGetMediaPlayer); \
	DECLARE_FUNCTION(execGetMaterial);


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnMediaOpenFailed); \
	DECLARE_FUNCTION(execOnMediaOpenOpened); \
	DECLARE_FUNCTION(execOnMediaClosed); \
	DECLARE_FUNCTION(execGetUndistortedCameraViewInfo); \
	DECLARE_FUNCTION(execGetMediaSource); \
	DECLARE_FUNCTION(execGetLensDisplacementTexture); \
	DECLARE_FUNCTION(execGetMediaTexture); \
	DECLARE_FUNCTION(execGetMediaPlayer); \
	DECLARE_FUNCTION(execGetMaterial);


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaBundle(); \
	friend struct Z_Construct_UClass_UMediaBundle_Statics; \
public: \
	DECLARE_CLASS(UMediaBundle, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UMediaBundle)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUMediaBundle(); \
	friend struct Z_Construct_UClass_UMediaBundle_Statics; \
public: \
	DECLARE_CLASS(UMediaBundle, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UMediaBundle)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaBundle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaBundle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaBundle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaBundle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaBundle(UMediaBundle&&); \
	NO_API UMediaBundle(const UMediaBundle&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaBundle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaBundle(UMediaBundle&&); \
	NO_API UMediaBundle(const UMediaBundle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaBundle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaBundle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaBundle)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaPlayer() { return STRUCT_OFFSET(UMediaBundle, MediaPlayer); } \
	FORCEINLINE static uint32 __PPO__MediaTexture() { return STRUCT_OFFSET(UMediaBundle, MediaTexture); } \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(UMediaBundle, Material); } \
	FORCEINLINE static uint32 __PPO__LensParameters() { return STRUCT_OFFSET(UMediaBundle, LensParameters); } \
	FORCEINLINE static uint32 __PPO__UndistortedCameraViewInfo() { return STRUCT_OFFSET(UMediaBundle, UndistortedCameraViewInfo); } \
	FORCEINLINE static uint32 __PPO__CurrentLensParameters() { return STRUCT_OFFSET(UMediaBundle, CurrentLensParameters); } \
	FORCEINLINE static uint32 __PPO__LensDisplacementMap() { return STRUCT_OFFSET(UMediaBundle, LensDisplacementMap); } \
	FORCEINLINE static uint32 __PPO__ReferenceCount() { return STRUCT_OFFSET(UMediaBundle, ReferenceCount); }


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_39_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h_42_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaBundle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<class UMediaBundle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
