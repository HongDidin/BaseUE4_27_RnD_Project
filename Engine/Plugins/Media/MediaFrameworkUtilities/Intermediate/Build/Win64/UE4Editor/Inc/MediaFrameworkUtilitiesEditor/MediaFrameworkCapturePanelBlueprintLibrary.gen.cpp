// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaFrameworkUtilitiesEditor/Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaFrameworkCapturePanelBlueprintLibrary() {}
// Cross Module References
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkCapturePanel_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkCapturePanel();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaCaptureOptions();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EViewModeIndex();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_NoRegister();
	MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execSetCurrentViewportCapture)
	{
		P_GET_OBJECT(UMediaOutput,Z_Param_MediaOutput);
		P_GET_STRUCT(FMediaCaptureOptions,Z_Param_CaptureOptions);
		P_GET_PROPERTY(FByteProperty,Z_Param_ViewMode);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCurrentViewportCapture(Z_Param_MediaOutput,Z_Param_CaptureOptions,EViewModeIndex(Z_Param_ViewMode));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execAddViewportCapture)
	{
		P_GET_OBJECT(UMediaOutput,Z_Param_MediaOutput);
		P_GET_OBJECT(AActor,Z_Param_Camera);
		P_GET_STRUCT(FMediaCaptureOptions,Z_Param_CaptureOptions);
		P_GET_PROPERTY(FByteProperty,Z_Param_ViewMode);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddViewportCapture(Z_Param_MediaOutput,Z_Param_Camera,Z_Param_CaptureOptions,EViewModeIndex(Z_Param_ViewMode));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execEmptyViewportCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EmptyViewportCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execAddRenderTargetCapture)
	{
		P_GET_OBJECT(UMediaOutput,Z_Param_MediaOutput);
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_RenderTarget);
		P_GET_STRUCT(FMediaCaptureOptions,Z_Param_CaptureOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddRenderTargetCapture(Z_Param_MediaOutput,Z_Param_RenderTarget,Z_Param_CaptureOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execEmptyRenderTargetCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EmptyRenderTargetCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execStopCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopCapture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaFrameworkCapturePanel::execStartCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartCapture();
		P_NATIVE_END;
	}
	void UMediaFrameworkCapturePanel::StaticRegisterNativesUMediaFrameworkCapturePanel()
	{
		UClass* Class = UMediaFrameworkCapturePanel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddRenderTargetCapture", &UMediaFrameworkCapturePanel::execAddRenderTargetCapture },
			{ "AddViewportCapture", &UMediaFrameworkCapturePanel::execAddViewportCapture },
			{ "EmptyRenderTargetCapture", &UMediaFrameworkCapturePanel::execEmptyRenderTargetCapture },
			{ "EmptyViewportCapture", &UMediaFrameworkCapturePanel::execEmptyViewportCapture },
			{ "SetCurrentViewportCapture", &UMediaFrameworkCapturePanel::execSetCurrentViewportCapture },
			{ "StartCapture", &UMediaFrameworkCapturePanel::execStartCapture },
			{ "StopCapture", &UMediaFrameworkCapturePanel::execStopCapture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics
	{
		struct MediaFrameworkCapturePanel_eventAddRenderTargetCapture_Parms
		{
			UMediaOutput* MediaOutput;
			UTextureRenderTarget2D* RenderTarget;
			FMediaCaptureOptions CaptureOptions;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddRenderTargetCapture_Parms, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddRenderTargetCapture_Parms, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddRenderTargetCapture_Parms, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::NewProp_CaptureOptions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Add a render target 2d to be captured.\n\x09 */" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Add a render target 2d to be captured." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "AddRenderTargetCapture", nullptr, nullptr, sizeof(MediaFrameworkCapturePanel_eventAddRenderTargetCapture_Parms), Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics
	{
		struct MediaFrameworkCapturePanel_eventAddViewportCapture_Parms
		{
			UMediaOutput* MediaOutput;
			AActor* Camera;
			FMediaCaptureOptions CaptureOptions;
			TEnumAsByte<EViewModeIndex> ViewMode;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddViewportCapture_Parms, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddViewportCapture_Parms, Camera), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddViewportCapture_Parms, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_ViewMode = { "ViewMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventAddViewportCapture_Parms, ViewMode), Z_Construct_UEnum_Engine_EViewModeIndex, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_CaptureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::NewProp_ViewMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Add a camera to be used when capturing the current viewport.\n\x09 */" },
		{ "CPP_Default_ViewMode", "VMI_Unknown" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Add a camera to be used when capturing the current viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "AddViewportCapture", nullptr, nullptr, sizeof(MediaFrameworkCapturePanel_eventAddViewportCapture_Parms), Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Clear all the render target captures.\n\x09 */" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Clear all the render target captures." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "EmptyRenderTargetCapture", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Clear all the viewport captures.\n\x09 */" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Clear all the viewport captures." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "EmptyViewportCapture", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics
	{
		struct MediaFrameworkCapturePanel_eventSetCurrentViewportCapture_Parms
		{
			UMediaOutput* MediaOutput;
			FMediaCaptureOptions CaptureOptions;
			TEnumAsByte<EViewModeIndex> ViewMode;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventSetCurrentViewportCapture_Parms, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventSetCurrentViewportCapture_Parms, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::NewProp_ViewMode = { "ViewMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanel_eventSetCurrentViewportCapture_Parms, ViewMode), Z_Construct_UEnum_Engine_EViewModeIndex, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::NewProp_CaptureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::NewProp_ViewMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Change the setting for capturing the current viewport.\n\x09 */" },
		{ "CPP_Default_ViewMode", "VMI_Unknown" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Change the setting for capturing the current viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "SetCurrentViewportCapture", nullptr, nullptr, sizeof(MediaFrameworkCapturePanel_eventSetCurrentViewportCapture_Parms), Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Capture the camera's viewport and the render target.\n\x09 */" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Capture the camera's viewport and the render target." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "StartCapture", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Stop the current capture.\n\x09 */" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Stop the current capture." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanel, nullptr, "StopCapture", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMediaFrameworkCapturePanel_NoRegister()
	{
		return UMediaFrameworkCapturePanel::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddRenderTargetCapture, "AddRenderTargetCapture" }, // 707241242
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_AddViewportCapture, "AddViewportCapture" }, // 3251172256
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyRenderTargetCapture, "EmptyRenderTargetCapture" }, // 3222568276
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_EmptyViewportCapture, "EmptyViewportCapture" }, // 1105565563
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_SetCurrentViewportCapture, "SetCurrentViewportCapture" }, // 2630660919
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_StartCapture, "StartCapture" }, // 518938999
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanel_StopCapture, "StopCapture" }, // 72313320
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkCapturePanel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::ClassParams = {
		&UMediaFrameworkCapturePanel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkCapturePanel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkCapturePanel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkCapturePanel, 3699974960);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkCapturePanel>()
	{
		return UMediaFrameworkCapturePanel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkCapturePanel(Z_Construct_UClass_UMediaFrameworkCapturePanel, &UMediaFrameworkCapturePanel::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkCapturePanel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkCapturePanel);
	DEFINE_FUNCTION(UMediaFrameworkCapturePanelBlueprintLibrary::execGetMediaCapturePanel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMediaFrameworkCapturePanel**)Z_Param__Result=UMediaFrameworkCapturePanelBlueprintLibrary::GetMediaCapturePanel();
		P_NATIVE_END;
	}
	void UMediaFrameworkCapturePanelBlueprintLibrary::StaticRegisterNativesUMediaFrameworkCapturePanelBlueprintLibrary()
	{
		UClass* Class = UMediaFrameworkCapturePanelBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetMediaCapturePanel", &UMediaFrameworkCapturePanelBlueprintLibrary::execGetMediaCapturePanel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics
	{
		struct MediaFrameworkCapturePanelBlueprintLibrary_eventGetMediaCapturePanel_Parms
		{
			UMediaFrameworkCapturePanel* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaFrameworkCapturePanelBlueprintLibrary_eventGetMediaCapturePanel_Parms, ReturnValue), Z_Construct_UClass_UMediaFrameworkCapturePanel_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Media Capture" },
		{ "Comment", "/**\n\x09 * Get Media Capture panel instance.\n\x09 */" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ToolTip", "Get Media Capture panel instance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary, nullptr, "GetMediaCapturePanel", nullptr, nullptr, sizeof(MediaFrameworkCapturePanelBlueprintLibrary_eventGetMediaCapturePanel_Parms), Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_NoRegister()
	{
		return UMediaFrameworkCapturePanelBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaFrameworkUtilitiesEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMediaFrameworkCapturePanelBlueprintLibrary_GetMediaCapturePanel, "GetMediaCapturePanel" }, // 2380201175
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Private/CaptureTab/MediaFrameworkCapturePanelBlueprintLibrary.h" },
		{ "ScriptName", "MediaFrameworkCapturePanelLibrary" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaFrameworkCapturePanelBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::ClassParams = {
		&UMediaFrameworkCapturePanelBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaFrameworkCapturePanelBlueprintLibrary, 4211606512);
	template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<UMediaFrameworkCapturePanelBlueprintLibrary>()
	{
		return UMediaFrameworkCapturePanelBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaFrameworkCapturePanelBlueprintLibrary(Z_Construct_UClass_UMediaFrameworkCapturePanelBlueprintLibrary, &UMediaFrameworkCapturePanelBlueprintLibrary::StaticClass, TEXT("/Script/MediaFrameworkUtilitiesEditor"), TEXT("UMediaFrameworkCapturePanelBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaFrameworkCapturePanelBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
