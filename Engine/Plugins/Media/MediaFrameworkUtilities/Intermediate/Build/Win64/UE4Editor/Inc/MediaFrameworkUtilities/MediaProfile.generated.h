// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAFRAMEWORKUTILITIES_MediaProfile_generated_h
#error "MediaProfile.generated.h already included, missing '#pragma once' in MediaProfile.h"
#endif
#define MEDIAFRAMEWORKUTILITIES_MediaProfile_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaProfile(); \
	friend struct Z_Construct_UClass_UMediaProfile_Statics; \
public: \
	DECLARE_CLASS(UMediaProfile, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UMediaProfile)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUMediaProfile(); \
	friend struct Z_Construct_UClass_UMediaProfile_Statics; \
public: \
	DECLARE_CLASS(UMediaProfile, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UMediaProfile)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaProfile(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaProfile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaProfile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaProfile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaProfile(UMediaProfile&&); \
	NO_API UMediaProfile(const UMediaProfile&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaProfile(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaProfile(UMediaProfile&&); \
	NO_API UMediaProfile(const UMediaProfile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaProfile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaProfile); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaProfile)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaSources() { return STRUCT_OFFSET(UMediaProfile, MediaSources); } \
	FORCEINLINE static uint32 __PPO__MediaOutputs() { return STRUCT_OFFSET(UMediaProfile, MediaOutputs); } \
	FORCEINLINE static uint32 __PPO__bOverrideTimecodeProvider() { return STRUCT_OFFSET(UMediaProfile, bOverrideTimecodeProvider); } \
	FORCEINLINE static uint32 __PPO__TimecodeProvider() { return STRUCT_OFFSET(UMediaProfile, TimecodeProvider); } \
	FORCEINLINE static uint32 __PPO__bOverrideCustomTimeStep() { return STRUCT_OFFSET(UMediaProfile, bOverrideCustomTimeStep); } \
	FORCEINLINE static uint32 __PPO__CustomTimeStep() { return STRUCT_OFFSET(UMediaProfile, CustomTimeStep); } \
	FORCEINLINE static uint32 __PPO__AppliedTimecodeProvider() { return STRUCT_OFFSET(UMediaProfile, AppliedTimecodeProvider); } \
	FORCEINLINE static uint32 __PPO__PreviousTimecodeProvider() { return STRUCT_OFFSET(UMediaProfile, PreviousTimecodeProvider); } \
	FORCEINLINE static uint32 __PPO__AppliedCustomTimeStep() { return STRUCT_OFFSET(UMediaProfile, AppliedCustomTimeStep); } \
	FORCEINLINE static uint32 __PPO__PreviousCustomTimeStep() { return STRUCT_OFFSET(UMediaProfile, PreviousCustomTimeStep); }


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_19_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<class UMediaProfile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_Profile_MediaProfile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
