// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAFRAMEWORKUTILITIES_MediaBundleTimeSynchronizationSource_generated_h
#error "MediaBundleTimeSynchronizationSource.generated.h already included, missing '#pragma once' in MediaBundleTimeSynchronizationSource.h"
#endif
#define MEDIAFRAMEWORKUTILITIES_MediaBundleTimeSynchronizationSource_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaBundleTimeSynchronizationSource(); \
	friend struct Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics; \
public: \
	DECLARE_CLASS(UMediaBundleTimeSynchronizationSource, UTimeSynchronizationSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UMediaBundleTimeSynchronizationSource)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMediaBundleTimeSynchronizationSource(); \
	friend struct Z_Construct_UClass_UMediaBundleTimeSynchronizationSource_Statics; \
public: \
	DECLARE_CLASS(UMediaBundleTimeSynchronizationSource, UTimeSynchronizationSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilities"), NO_API) \
	DECLARE_SERIALIZER(UMediaBundleTimeSynchronizationSource)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaBundleTimeSynchronizationSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaBundleTimeSynchronizationSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaBundleTimeSynchronizationSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaBundleTimeSynchronizationSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaBundleTimeSynchronizationSource(UMediaBundleTimeSynchronizationSource&&); \
	NO_API UMediaBundleTimeSynchronizationSource(const UMediaBundleTimeSynchronizationSource&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaBundleTimeSynchronizationSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaBundleTimeSynchronizationSource(UMediaBundleTimeSynchronizationSource&&); \
	NO_API UMediaBundleTimeSynchronizationSource(const UMediaBundleTimeSynchronizationSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaBundleTimeSynchronizationSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaBundleTimeSynchronizationSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaBundleTimeSynchronizationSource)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_15_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIES_API UClass* StaticClass<class UMediaBundleTimeSynchronizationSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilities_Public_MediaBundleTimeSynchronizationSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
