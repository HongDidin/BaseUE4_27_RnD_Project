// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAFRAMEWORKUTILITIESEDITOR_MediaBundleFactoryNew_generated_h
#error "MediaBundleFactoryNew.generated.h already included, missing '#pragma once' in MediaBundleFactoryNew.h"
#endif
#define MEDIAFRAMEWORKUTILITIESEDITOR_MediaBundleFactoryNew_generated_h

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaBundleFactoryNew(); \
	friend struct Z_Construct_UClass_UMediaBundleFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UMediaBundleFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UMediaBundleFactoryNew)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUMediaBundleFactoryNew(); \
	friend struct Z_Construct_UClass_UMediaBundleFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UMediaBundleFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilitiesEditor"), NO_API) \
	DECLARE_SERIALIZER(UMediaBundleFactoryNew)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaBundleFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaBundleFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaBundleFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaBundleFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaBundleFactoryNew(UMediaBundleFactoryNew&&); \
	NO_API UMediaBundleFactoryNew(const UMediaBundleFactoryNew&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaBundleFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaBundleFactoryNew(UMediaBundleFactoryNew&&); \
	NO_API UMediaBundleFactoryNew(const UMediaBundleFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaBundleFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaBundleFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaBundleFactoryNew)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_18_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaBundleFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<class UMediaBundleFactoryNew>();

#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_SPARSE_DATA
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryMediaBundle(); \
	friend struct Z_Construct_UClass_UActorFactoryMediaBundle_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryMediaBundle, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilitiesEditor"), MEDIAFRAMEWORKUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryMediaBundle)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryMediaBundle(); \
	friend struct Z_Construct_UClass_UActorFactoryMediaBundle_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryMediaBundle, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MediaFrameworkUtilitiesEditor"), MEDIAFRAMEWORKUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryMediaBundle)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UActorFactoryMediaBundle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryMediaBundle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIAFRAMEWORKUTILITIESEDITOR_API, UActorFactoryMediaBundle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryMediaBundle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UActorFactoryMediaBundle(UActorFactoryMediaBundle&&); \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UActorFactoryMediaBundle(const UActorFactoryMediaBundle&); \
public:


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UActorFactoryMediaBundle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UActorFactoryMediaBundle(UActorFactoryMediaBundle&&); \
	MEDIAFRAMEWORKUTILITIESEDITOR_API UActorFactoryMediaBundle(const UActorFactoryMediaBundle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MEDIAFRAMEWORKUTILITIESEDITOR_API, UActorFactoryMediaBundle); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryMediaBundle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryMediaBundle)


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_31_PROLOG
#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_INCLASS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_SPARSE_DATA \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h_34_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryMediaBundle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAFRAMEWORKUTILITIESEDITOR_API UClass* StaticClass<class UActorFactoryMediaBundle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaFrameworkUtilities_Source_MediaFrameworkUtilitiesEditor_Private_MediaBundleFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
