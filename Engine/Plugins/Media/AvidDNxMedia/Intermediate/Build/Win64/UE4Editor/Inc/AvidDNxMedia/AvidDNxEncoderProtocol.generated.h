// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AVIDDNXMEDIA_AvidDNxEncoderProtocol_generated_h
#error "AvidDNxEncoderProtocol.generated.h already included, missing '#pragma once' in AvidDNxEncoderProtocol.h"
#endif
#define AVIDDNXMEDIA_AvidDNxEncoderProtocol_generated_h

#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_SPARSE_DATA
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_RPC_WRAPPERS
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAvidDNxEncoderProtocol(); \
	friend struct Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics; \
public: \
	DECLARE_CLASS(UAvidDNxEncoderProtocol, UFrameGrabberProtocol, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AvidDNxMedia"), NO_API) \
	DECLARE_SERIALIZER(UAvidDNxEncoderProtocol)


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUAvidDNxEncoderProtocol(); \
	friend struct Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics; \
public: \
	DECLARE_CLASS(UAvidDNxEncoderProtocol, UFrameGrabberProtocol, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AvidDNxMedia"), NO_API) \
	DECLARE_SERIALIZER(UAvidDNxEncoderProtocol)


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAvidDNxEncoderProtocol(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAvidDNxEncoderProtocol) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAvidDNxEncoderProtocol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAvidDNxEncoderProtocol); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAvidDNxEncoderProtocol(UAvidDNxEncoderProtocol&&); \
	NO_API UAvidDNxEncoderProtocol(const UAvidDNxEncoderProtocol&); \
public:


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAvidDNxEncoderProtocol(UAvidDNxEncoderProtocol&&); \
	NO_API UAvidDNxEncoderProtocol(const UAvidDNxEncoderProtocol&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAvidDNxEncoderProtocol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAvidDNxEncoderProtocol); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAvidDNxEncoderProtocol)


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_20_PROLOG
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_SPARSE_DATA \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_RPC_WRAPPERS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_INCLASS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_SPARSE_DATA \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h_24_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AVIDDNXMEDIA_API UClass* StaticClass<class UAvidDNxEncoderProtocol>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_AvidDNxEncoderProtocol_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
