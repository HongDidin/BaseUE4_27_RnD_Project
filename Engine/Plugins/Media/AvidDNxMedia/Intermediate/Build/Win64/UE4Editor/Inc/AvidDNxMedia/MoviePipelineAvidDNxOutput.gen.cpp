// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/MoviePipelineAvidDNxOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineAvidDNxOutput() {}
// Cross Module References
	AVIDDNXMEDIA_API UClass* Z_Construct_UClass_UMoviePipelineAvidDNxOutput_NoRegister();
	AVIDDNXMEDIA_API UClass* Z_Construct_UClass_UMoviePipelineAvidDNxOutput();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineVideoOutputBase();
	UPackage* Z_Construct_UPackage__Script_AvidDNxMedia();
// End Cross Module References
	void UMoviePipelineAvidDNxOutput::StaticRegisterNativesUMoviePipelineAvidDNxOutput()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineAvidDNxOutput_NoRegister()
	{
		return UMoviePipelineAvidDNxOutput::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCompression_MetaData[];
#endif
		static void NewProp_bUseCompression_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCompression;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfEncodingThreads_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfEncodingThreads;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineVideoOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AvidDNxMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Forward Declare\n" },
		{ "IncludePath", "MoviePipelineAvidDNxOutput.h" },
		{ "ModuleRelativePath", "Private/MoviePipelineAvidDNxOutput.h" },
		{ "ToolTip", "Forward Declare" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Should we use a lossy compression for the output? */" },
		{ "ModuleRelativePath", "Private/MoviePipelineAvidDNxOutput.h" },
		{ "ToolTip", "Should we use a lossy compression for the output?" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression_SetBit(void* Obj)
	{
		((UMoviePipelineAvidDNxOutput*)Obj)->bUseCompression = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression = { "bUseCompression", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineAvidDNxOutput), &Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_NumberOfEncodingThreads_MetaData[] = {
		{ "Category", "Settings" },
		{ "ClampMin", "1" },
		{ "Comment", "/** How many threads should the AvidDNx Encoders use to encode frames? */" },
		{ "ModuleRelativePath", "Private/MoviePipelineAvidDNxOutput.h" },
		{ "ToolTip", "How many threads should the AvidDNx Encoders use to encode frames?" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_NumberOfEncodingThreads = { "NumberOfEncodingThreads", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineAvidDNxOutput, NumberOfEncodingThreads), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_NumberOfEncodingThreads_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_NumberOfEncodingThreads_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_bUseCompression,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::NewProp_NumberOfEncodingThreads,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineAvidDNxOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::ClassParams = {
		&UMoviePipelineAvidDNxOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineAvidDNxOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineAvidDNxOutput, 683159018);
	template<> AVIDDNXMEDIA_API UClass* StaticClass<UMoviePipelineAvidDNxOutput>()
	{
		return UMoviePipelineAvidDNxOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineAvidDNxOutput(Z_Construct_UClass_UMoviePipelineAvidDNxOutput, &UMoviePipelineAvidDNxOutput::StaticClass, TEXT("/Script/AvidDNxMedia"), TEXT("UMoviePipelineAvidDNxOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineAvidDNxOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
