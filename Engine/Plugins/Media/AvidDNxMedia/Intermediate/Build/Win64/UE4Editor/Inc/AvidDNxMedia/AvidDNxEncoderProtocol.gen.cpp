// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/AvidDNxEncoderProtocol.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAvidDNxEncoderProtocol() {}
// Cross Module References
	AVIDDNXMEDIA_API UClass* Z_Construct_UClass_UAvidDNxEncoderProtocol_NoRegister();
	AVIDDNXMEDIA_API UClass* Z_Construct_UClass_UAvidDNxEncoderProtocol();
	MOVIESCENECAPTURE_API UClass* Z_Construct_UClass_UFrameGrabberProtocol();
	UPackage* Z_Construct_UPackage__Script_AvidDNxMedia();
// End Cross Module References
	void UAvidDNxEncoderProtocol::StaticRegisterNativesUAvidDNxEncoderProtocol()
	{
	}
	UClass* Z_Construct_UClass_UAvidDNxEncoderProtocol_NoRegister()
	{
		return UAvidDNxEncoderProtocol::StaticClass();
	}
	struct Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUncompressed_MetaData[];
#endif
		static void NewProp_bUncompressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUncompressed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfEncodingThreads_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NumberOfEncodingThreads;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFrameGrabberProtocol,
		(UObject* (*)())Z_Construct_UPackage__Script_AvidDNxMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::Class_MetaDataParams[] = {
		{ "CommandLineID", "AvidDNx" },
		{ "DisplayName", "Avid DNx Encoder (mxf)" },
		{ "IncludePath", "AvidDNxEncoderProtocol.h" },
		{ "ModuleRelativePath", "Private/AvidDNxEncoderProtocol.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed_MetaData[] = {
		{ "Category", "Avid DNX Settings" },
		{ "ModuleRelativePath", "Private/AvidDNxEncoderProtocol.h" },
	};
#endif
	void Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed_SetBit(void* Obj)
	{
		((UAvidDNxEncoderProtocol*)Obj)->bUncompressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed = { "bUncompressed", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAvidDNxEncoderProtocol), &Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads_MetaData[] = {
		{ "Category", "Avid DNX Settings" },
		{ "ClampMax", "64" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Private/AvidDNxEncoderProtocol.h" },
		{ "UIMax", "64" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads = { "NumberOfEncodingThreads", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAvidDNxEncoderProtocol, NumberOfEncodingThreads), nullptr, METADATA_PARAMS(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_bUncompressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAvidDNxEncoderProtocol>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::ClassParams = {
		&UAvidDNxEncoderProtocol::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::PropPointers),
		0,
		0x001004A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAvidDNxEncoderProtocol()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAvidDNxEncoderProtocol_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAvidDNxEncoderProtocol, 2110956411);
	template<> AVIDDNXMEDIA_API UClass* StaticClass<UAvidDNxEncoderProtocol>()
	{
		return UAvidDNxEncoderProtocol::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAvidDNxEncoderProtocol(Z_Construct_UClass_UAvidDNxEncoderProtocol, &UAvidDNxEncoderProtocol::StaticClass, TEXT("/Script/AvidDNxMedia"), TEXT("UAvidDNxEncoderProtocol"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAvidDNxEncoderProtocol);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
