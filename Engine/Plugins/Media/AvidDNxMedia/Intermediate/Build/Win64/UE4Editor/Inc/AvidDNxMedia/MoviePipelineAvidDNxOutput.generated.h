// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AVIDDNXMEDIA_MoviePipelineAvidDNxOutput_generated_h
#error "MoviePipelineAvidDNxOutput.generated.h already included, missing '#pragma once' in MoviePipelineAvidDNxOutput.h"
#endif
#define AVIDDNXMEDIA_MoviePipelineAvidDNxOutput_generated_h

#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_SPARSE_DATA
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_RPC_WRAPPERS
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineAvidDNxOutput(); \
	friend struct Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineAvidDNxOutput, UMoviePipelineVideoOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AvidDNxMedia"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineAvidDNxOutput)


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineAvidDNxOutput(); \
	friend struct Z_Construct_UClass_UMoviePipelineAvidDNxOutput_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineAvidDNxOutput, UMoviePipelineVideoOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AvidDNxMedia"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineAvidDNxOutput)


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineAvidDNxOutput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineAvidDNxOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineAvidDNxOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineAvidDNxOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineAvidDNxOutput(UMoviePipelineAvidDNxOutput&&); \
	NO_API UMoviePipelineAvidDNxOutput(const UMoviePipelineAvidDNxOutput&); \
public:


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineAvidDNxOutput(UMoviePipelineAvidDNxOutput&&); \
	NO_API UMoviePipelineAvidDNxOutput(const UMoviePipelineAvidDNxOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineAvidDNxOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineAvidDNxOutput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineAvidDNxOutput)


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_10_PROLOG
#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_SPARSE_DATA \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_RPC_WRAPPERS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_INCLASS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_SPARSE_DATA \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AVIDDNXMEDIA_API UClass* StaticClass<class UMoviePipelineAvidDNxOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AvidDNxMedia_Source_Source_Private_MoviePipelineAvidDNxOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
