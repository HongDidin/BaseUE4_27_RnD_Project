// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleProResMedia/Public/AppleProResEncoder/AppleProResEncoder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleProResEncoder() {}
// Cross Module References
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanMode();
	UPackage* Z_Construct_UPackage__Script_AppleProResMedia();
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorPrimaries();
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec();
// End Cross Module References
	static UEnum* EAppleProResEncoderScanMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanMode, Z_Construct_UPackage__Script_AppleProResMedia(), TEXT("EAppleProResEncoderScanMode"));
		}
		return Singleton;
	}
	template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderScanMode>()
	{
		return EAppleProResEncoderScanMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAppleProResEncoderScanMode(EAppleProResEncoderScanMode_StaticEnum, TEXT("/Script/AppleProResMedia"), TEXT("EAppleProResEncoderScanMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanMode_Hash() { return 2599447325U; }
	UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleProResMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAppleProResEncoderScanMode"), 0, Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAppleProResEncoderScanMode::IM_PROGRESSIVE_SCAN", (int64)EAppleProResEncoderScanMode::IM_PROGRESSIVE_SCAN },
				{ "EAppleProResEncoderScanMode::IM_INTERLACED_TOP_FIELD_FIRST", (int64)EAppleProResEncoderScanMode::IM_INTERLACED_TOP_FIELD_FIRST },
				{ "EAppleProResEncoderScanMode::IM_INTERLATED_BOTTOM_FIRST_FIRST", (int64)EAppleProResEncoderScanMode::IM_INTERLATED_BOTTOM_FIRST_FIRST },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "IM_INTERLACED_TOP_FIELD_FIRST.DisplayName", "Interlaced; first (top) image line belongs to first temporal field" },
				{ "IM_INTERLACED_TOP_FIELD_FIRST.Name", "EAppleProResEncoderScanMode::IM_INTERLACED_TOP_FIELD_FIRST" },
				{ "IM_INTERLATED_BOTTOM_FIRST_FIRST.DisplayName", "Interlaced; second (bottom) image line belongs to first temporal field" },
				{ "IM_INTERLATED_BOTTOM_FIRST_FIRST.Name", "EAppleProResEncoderScanMode::IM_INTERLATED_BOTTOM_FIRST_FIRST" },
				{ "IM_PROGRESSIVE_SCAN.DisplayName", "Progressive" },
				{ "IM_PROGRESSIVE_SCAN.Name", "EAppleProResEncoderScanMode::IM_PROGRESSIVE_SCAN" },
				{ "ModuleRelativePath", "Public/AppleProResEncoder/AppleProResEncoder.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleProResMedia,
				nullptr,
				"EAppleProResEncoderScanMode",
				"EAppleProResEncoderScanMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAppleProResEncoderColorPrimaries_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorPrimaries, Z_Construct_UPackage__Script_AppleProResMedia(), TEXT("EAppleProResEncoderColorPrimaries"));
		}
		return Singleton;
	}
	template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderColorPrimaries>()
	{
		return EAppleProResEncoderColorPrimaries_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAppleProResEncoderColorPrimaries(EAppleProResEncoderColorPrimaries_StaticEnum, TEXT("/Script/AppleProResMedia"), TEXT("EAppleProResEncoderColorPrimaries"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorPrimaries_Hash() { return 1361482605U; }
	UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorPrimaries()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleProResMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAppleProResEncoderColorPrimaries"), 0, Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorPrimaries_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAppleProResEncoderColorPrimaries::CD_SDREC601_525_60HZ", (int64)EAppleProResEncoderColorPrimaries::CD_SDREC601_525_60HZ },
				{ "EAppleProResEncoderColorPrimaries::CD_SDREC601_625_50HZ", (int64)EAppleProResEncoderColorPrimaries::CD_SDREC601_625_50HZ },
				{ "EAppleProResEncoderColorPrimaries::CD_HDREC709", (int64)EAppleProResEncoderColorPrimaries::CD_HDREC709 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CD_HDREC709.DisplayName", "HD Rec. 709" },
				{ "CD_HDREC709.Name", "EAppleProResEncoderColorPrimaries::CD_HDREC709" },
				{ "CD_SDREC601_525_60HZ.DisplayName", "SD Rec. 601 525/60Hz" },
				{ "CD_SDREC601_525_60HZ.Name", "EAppleProResEncoderColorPrimaries::CD_SDREC601_525_60HZ" },
				{ "CD_SDREC601_625_50HZ.DisplayName", "SD Rec. 601 625/50Hz" },
				{ "CD_SDREC601_625_50HZ.Name", "EAppleProResEncoderColorPrimaries::CD_SDREC601_625_50HZ" },
				{ "ModuleRelativePath", "Public/AppleProResEncoder/AppleProResEncoder.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleProResMedia,
				nullptr,
				"EAppleProResEncoderColorPrimaries",
				"EAppleProResEncoderColorPrimaries",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAppleProResEncoderCodec_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec, Z_Construct_UPackage__Script_AppleProResMedia(), TEXT("EAppleProResEncoderCodec"));
		}
		return Singleton;
	}
	template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderCodec>()
	{
		return EAppleProResEncoderCodec_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAppleProResEncoderCodec(EAppleProResEncoderCodec_StaticEnum, TEXT("/Script/AppleProResMedia"), TEXT("EAppleProResEncoderCodec"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec_Hash() { return 4256470659U; }
	UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleProResMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAppleProResEncoderCodec"), 0, Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAppleProResEncoderCodec::ProRes_422Proxy", (int64)EAppleProResEncoderCodec::ProRes_422Proxy },
				{ "EAppleProResEncoderCodec::ProRes_422LT", (int64)EAppleProResEncoderCodec::ProRes_422LT },
				{ "EAppleProResEncoderCodec::ProRes_422", (int64)EAppleProResEncoderCodec::ProRes_422 },
				{ "EAppleProResEncoderCodec::ProRes_422HQ", (int64)EAppleProResEncoderCodec::ProRes_422HQ },
				{ "EAppleProResEncoderCodec::ProRes_4444", (int64)EAppleProResEncoderCodec::ProRes_4444 },
				{ "EAppleProResEncoderCodec::ProRes_4444XQ", (int64)EAppleProResEncoderCodec::ProRes_4444XQ },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/AppleProResEncoder/AppleProResEncoder.h" },
				{ "ProRes_422.Comment", "/** High Quality Compression for 422 RGB Sources. Approximately 150mbps @ 1920x1080@30fps*/" },
				{ "ProRes_422.DisplayName", "Apple ProRes 422" },
				{ "ProRes_422.Name", "EAppleProResEncoderCodec::ProRes_422" },
				{ "ProRes_422.ToolTip", "High Quality Compression for 422 RGB Sources. Approximately 150mbps @ 1920x1080@30fps" },
				{ "ProRes_422HQ.Comment", "/** A higher bit-rate version of Apple ProRes 422. Approximately 225mbps @ 1920x1080@30fps*/" },
				{ "ProRes_422HQ.DisplayName", "Apple ProRes 422 HQ" },
				{ "ProRes_422HQ.Name", "EAppleProResEncoderCodec::ProRes_422HQ" },
				{ "ProRes_422HQ.ToolTip", "A higher bit-rate version of Apple ProRes 422. Approximately 225mbps @ 1920x1080@30fps" },
				{ "ProRes_422LT.Comment", "/** High compression. Approximately 100mbps @ 1920x1080@30fps */" },
				{ "ProRes_422LT.DisplayName", "Apple ProRes 422 LT" },
				{ "ProRes_422LT.Name", "EAppleProResEncoderCodec::ProRes_422LT" },
				{ "ProRes_422LT.ToolTip", "High compression. Approximately 100mbps @ 1920x1080@30fps" },
				{ "ProRes_422Proxy.Comment", "/** Highest Compression. Approximately 45mbps @ 1920x1080@30fps */" },
				{ "ProRes_422Proxy.DisplayName", "Apple ProRes 422 Proxy" },
				{ "ProRes_422Proxy.Name", "EAppleProResEncoderCodec::ProRes_422Proxy" },
				{ "ProRes_422Proxy.ToolTip", "Highest Compression. Approximately 45mbps @ 1920x1080@30fps" },
				{ "ProRes_4444.Comment", "/** Extremely high quality and supports alpha channels. Can support both RGB and YCbCr formats. Very large file size. Approximately 330mbps @ 1920x1080@30fps */" },
				{ "ProRes_4444.DisplayName", "Apple ProRes 4444" },
				{ "ProRes_4444.Name", "EAppleProResEncoderCodec::ProRes_4444" },
				{ "ProRes_4444.ToolTip", "Extremely high quality and supports alpha channels. Can support both RGB and YCbCr formats. Very large file size. Approximately 330mbps @ 1920x1080@30fps" },
				{ "ProRes_4444XQ.Comment", "/** Highest quality storage with support for alpha channel with up to 12 bits precision for RGB and 16 bits for Alpha. Extremely large file size. Approximately 500mbps @ 1920x1080@30fps */" },
				{ "ProRes_4444XQ.DisplayName", "Apple ProRes 4444 XQ" },
				{ "ProRes_4444XQ.Name", "EAppleProResEncoderCodec::ProRes_4444XQ" },
				{ "ProRes_4444XQ.ToolTip", "Highest quality storage with support for alpha channel with up to 12 bits precision for RGB and 16 bits for Alpha. Extremely large file size. Approximately 500mbps @ 1920x1080@30fps" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleProResMedia,
				nullptr,
				"EAppleProResEncoderCodec",
				"EAppleProResEncoderCodec",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
