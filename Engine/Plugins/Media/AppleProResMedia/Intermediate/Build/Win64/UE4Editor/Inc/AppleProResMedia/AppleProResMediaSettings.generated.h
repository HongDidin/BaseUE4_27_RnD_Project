// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEPRORESMEDIA_AppleProResMediaSettings_generated_h
#error "AppleProResMediaSettings.generated.h already included, missing '#pragma once' in AppleProResMediaSettings.h"
#endif
#define APPLEPRORESMEDIA_AppleProResMediaSettings_generated_h

#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_SPARSE_DATA
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleProResMediaSettings(); \
	friend struct Z_Construct_UClass_UAppleProResMediaSettings_Statics; \
public: \
	DECLARE_CLASS(UAppleProResMediaSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/AppleProResMedia"), NO_API) \
	DECLARE_SERIALIZER(UAppleProResMediaSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAppleProResMediaSettings(); \
	friend struct Z_Construct_UClass_UAppleProResMediaSettings_Statics; \
public: \
	DECLARE_CLASS(UAppleProResMediaSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/AppleProResMedia"), NO_API) \
	DECLARE_SERIALIZER(UAppleProResMediaSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleProResMediaSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleProResMediaSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleProResMediaSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleProResMediaSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleProResMediaSettings(UAppleProResMediaSettings&&); \
	NO_API UAppleProResMediaSettings(const UAppleProResMediaSettings&); \
public:


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleProResMediaSettings(UAppleProResMediaSettings&&); \
	NO_API UAppleProResMediaSettings(const UAppleProResMediaSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleProResMediaSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleProResMediaSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAppleProResMediaSettings)


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_14_PROLOG
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_INCLASS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEPRORESMEDIA_API UClass* StaticClass<class UAppleProResMediaSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResMediaSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
