// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleProResMedia/Private/AppleProResMediaSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleProResMediaSettings() {}
// Cross Module References
	APPLEPRORESMEDIA_API UClass* Z_Construct_UClass_UAppleProResMediaSettings_NoRegister();
	APPLEPRORESMEDIA_API UClass* Z_Construct_UClass_UAppleProResMediaSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_AppleProResMedia();
// End Cross Module References
	void UAppleProResMediaSettings::StaticRegisterNativesUAppleProResMediaSettings()
	{
	}
	UClass* Z_Construct_UClass_UAppleProResMediaSettings_NoRegister()
	{
		return UAppleProResMediaSettings::StaticClass();
	}
	struct Z_Construct_UClass_UAppleProResMediaSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfCPUDecodingThreads_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfCPUDecodingThreads;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleProResMediaSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleProResMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResMediaSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the AppleProResMedia plug-in.\n */" },
		{ "IncludePath", "AppleProResMediaSettings.h" },
		{ "ModuleRelativePath", "Private/AppleProResMediaSettings.h" },
		{ "ToolTip", "Settings for the AppleProResMedia plug-in." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResMediaSettings_Statics::NewProp_NumberOfCPUDecodingThreads_MetaData[] = {
		{ "Category", "Media" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Number of CPU decoding threads (Set this to 0 to have the decoder spawn according to the number of processors detected in the system.). */" },
		{ "DisplayName", "Number Of CPU Decoding Threads" },
		{ "ModuleRelativePath", "Private/AppleProResMediaSettings.h" },
		{ "ToolTip", "Number of CPU decoding threads (Set this to 0 to have the decoder spawn according to the number of processors detected in the system.)." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UAppleProResMediaSettings_Statics::NewProp_NumberOfCPUDecodingThreads = { "NumberOfCPUDecodingThreads", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleProResMediaSettings, NumberOfCPUDecodingThreads), METADATA_PARAMS(Z_Construct_UClass_UAppleProResMediaSettings_Statics::NewProp_NumberOfCPUDecodingThreads_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResMediaSettings_Statics::NewProp_NumberOfCPUDecodingThreads_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAppleProResMediaSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResMediaSettings_Statics::NewProp_NumberOfCPUDecodingThreads,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleProResMediaSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleProResMediaSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleProResMediaSettings_Statics::ClassParams = {
		&UAppleProResMediaSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAppleProResMediaSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResMediaSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleProResMediaSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResMediaSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleProResMediaSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleProResMediaSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleProResMediaSettings, 1505791502);
	template<> APPLEPRORESMEDIA_API UClass* StaticClass<UAppleProResMediaSettings>()
	{
		return UAppleProResMediaSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleProResMediaSettings(Z_Construct_UClass_UAppleProResMediaSettings, &UAppleProResMediaSettings::StaticClass, TEXT("/Script/AppleProResMedia"), TEXT("UAppleProResMediaSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleProResMediaSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
