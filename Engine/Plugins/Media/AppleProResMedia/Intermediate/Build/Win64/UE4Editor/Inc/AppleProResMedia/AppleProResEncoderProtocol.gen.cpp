// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleProResMedia/Private/AppleProResEncoderProtocol.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAppleProResEncoderProtocol() {}
// Cross Module References
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanType();
	UPackage* Z_Construct_UPackage__Script_AppleProResMedia();
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorDescription();
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderFormats();
	APPLEPRORESMEDIA_API UClass* Z_Construct_UClass_UAppleProResEncoderProtocol_NoRegister();
	APPLEPRORESMEDIA_API UClass* Z_Construct_UClass_UAppleProResEncoderProtocol();
	MOVIESCENECAPTURE_API UClass* Z_Construct_UClass_UFrameGrabberProtocol();
// End Cross Module References
	static UEnum* EAppleProResEncoderScanType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanType, Z_Construct_UPackage__Script_AppleProResMedia(), TEXT("EAppleProResEncoderScanType"));
		}
		return Singleton;
	}
	template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderScanType>()
	{
		return EAppleProResEncoderScanType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAppleProResEncoderScanType(EAppleProResEncoderScanType_StaticEnum, TEXT("/Script/AppleProResMedia"), TEXT("EAppleProResEncoderScanType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanType_Hash() { return 2528983629U; }
	UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleProResMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAppleProResEncoderScanType"), 0, Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAppleProResEncoderScanType::IM_PROGRESSIVE_SCAN", (int64)EAppleProResEncoderScanType::IM_PROGRESSIVE_SCAN },
				{ "EAppleProResEncoderScanType::IM_INTERLACED_TOP_FIELD_FIRST", (int64)EAppleProResEncoderScanType::IM_INTERLACED_TOP_FIELD_FIRST },
				{ "EAppleProResEncoderScanType::IM_INTERLATED_BOTTOM_FIRST_FIRST", (int64)EAppleProResEncoderScanType::IM_INTERLATED_BOTTOM_FIRST_FIRST },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "IM_INTERLACED_TOP_FIELD_FIRST.DisplayName", "Interlaced mode; first (top) image line belongs to first temporal field" },
				{ "IM_INTERLACED_TOP_FIELD_FIRST.Name", "EAppleProResEncoderScanType::IM_INTERLACED_TOP_FIELD_FIRST" },
				{ "IM_INTERLATED_BOTTOM_FIRST_FIRST.DisplayName", "Interlaced mode; second (bottom) image line belongs to first temporal field" },
				{ "IM_INTERLATED_BOTTOM_FIRST_FIRST.Name", "EAppleProResEncoderScanType::IM_INTERLATED_BOTTOM_FIRST_FIRST" },
				{ "IM_PROGRESSIVE_SCAN.DisplayName", "Progressive encoding mode" },
				{ "IM_PROGRESSIVE_SCAN.Name", "EAppleProResEncoderScanType::IM_PROGRESSIVE_SCAN" },
				{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleProResMedia,
				nullptr,
				"EAppleProResEncoderScanType",
				"EAppleProResEncoderScanType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAppleProResEncoderColorDescription_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorDescription, Z_Construct_UPackage__Script_AppleProResMedia(), TEXT("EAppleProResEncoderColorDescription"));
		}
		return Singleton;
	}
	template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderColorDescription>()
	{
		return EAppleProResEncoderColorDescription_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAppleProResEncoderColorDescription(EAppleProResEncoderColorDescription_StaticEnum, TEXT("/Script/AppleProResMedia"), TEXT("EAppleProResEncoderColorDescription"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorDescription_Hash() { return 456009996U; }
	UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorDescription()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleProResMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAppleProResEncoderColorDescription"), 0, Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorDescription_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAppleProResEncoderColorDescription::CD_SDREC601_525_60HZ", (int64)EAppleProResEncoderColorDescription::CD_SDREC601_525_60HZ },
				{ "EAppleProResEncoderColorDescription::CD_SDREC601_625_50HZ", (int64)EAppleProResEncoderColorDescription::CD_SDREC601_625_50HZ },
				{ "EAppleProResEncoderColorDescription::CD_HDREC709", (int64)EAppleProResEncoderColorDescription::CD_HDREC709 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CD_HDREC709.DisplayName", "HD Rec. 709" },
				{ "CD_HDREC709.Name", "EAppleProResEncoderColorDescription::CD_HDREC709" },
				{ "CD_SDREC601_525_60HZ.DisplayName", "SD Rec. 601 525/60Hz" },
				{ "CD_SDREC601_525_60HZ.Name", "EAppleProResEncoderColorDescription::CD_SDREC601_525_60HZ" },
				{ "CD_SDREC601_625_50HZ.DisplayName", "SD Rec. 601 625/50Hz" },
				{ "CD_SDREC601_625_50HZ.Name", "EAppleProResEncoderColorDescription::CD_SDREC601_625_50HZ" },
				{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleProResMedia,
				nullptr,
				"EAppleProResEncoderColorDescription",
				"EAppleProResEncoderColorDescription",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAppleProResEncoderFormats_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderFormats, Z_Construct_UPackage__Script_AppleProResMedia(), TEXT("EAppleProResEncoderFormats"));
		}
		return Singleton;
	}
	template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderFormats>()
	{
		return EAppleProResEncoderFormats_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAppleProResEncoderFormats(EAppleProResEncoderFormats_StaticEnum, TEXT("/Script/AppleProResMedia"), TEXT("EAppleProResEncoderFormats"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderFormats_Hash() { return 1958195705U; }
	UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderFormats()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AppleProResMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAppleProResEncoderFormats"), 0, Get_Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderFormats_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAppleProResEncoderFormats::F_422HQ", (int64)EAppleProResEncoderFormats::F_422HQ },
				{ "EAppleProResEncoderFormats::F_422", (int64)EAppleProResEncoderFormats::F_422 },
				{ "EAppleProResEncoderFormats::F_422LT", (int64)EAppleProResEncoderFormats::F_422LT },
				{ "EAppleProResEncoderFormats::F_422Proxy", (int64)EAppleProResEncoderFormats::F_422Proxy },
				{ "EAppleProResEncoderFormats::F_4444", (int64)EAppleProResEncoderFormats::F_4444 },
				{ "EAppleProResEncoderFormats::F_4444XQ", (int64)EAppleProResEncoderFormats::F_4444XQ },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "F_422.DisplayName", "422" },
				{ "F_422.Name", "EAppleProResEncoderFormats::F_422" },
				{ "F_422HQ.DisplayName", "422 HQ" },
				{ "F_422HQ.Name", "EAppleProResEncoderFormats::F_422HQ" },
				{ "F_422LT.DisplayName", "422 LT" },
				{ "F_422LT.Name", "EAppleProResEncoderFormats::F_422LT" },
				{ "F_422Proxy.DisplayName", "422 Proxy" },
				{ "F_422Proxy.Name", "EAppleProResEncoderFormats::F_422Proxy" },
				{ "F_4444.DisplayName", "4444" },
				{ "F_4444.Name", "EAppleProResEncoderFormats::F_4444" },
				{ "F_4444XQ.DisplayName", "4444 XQ" },
				{ "F_4444XQ.Name", "EAppleProResEncoderFormats::F_4444XQ" },
				{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AppleProResMedia,
				nullptr,
				"EAppleProResEncoderFormats",
				"EAppleProResEncoderFormats",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UAppleProResEncoderProtocol::StaticRegisterNativesUAppleProResEncoderProtocol()
	{
	}
	UClass* Z_Construct_UClass_UAppleProResEncoderProtocol_NoRegister()
	{
		return UAppleProResEncoderProtocol::StaticClass();
	}
	struct Z_Construct_UClass_UAppleProResEncoderProtocol_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EncodingFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EncodingFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EncodingFormat;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorDescription_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorDescription;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ScanType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScanType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ScanType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfEncodingThreads_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfEncodingThreads;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEmbedTimecodeTrack_MetaData[];
#endif
		static void NewProp_bEmbedTimecodeTrack_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEmbedTimecodeTrack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDropFrameTimecode_MetaData[];
#endif
		static void NewProp_bDropFrameTimecode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDropFrameTimecode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFrameGrabberProtocol,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleProResMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::Class_MetaDataParams[] = {
		{ "CommandLineID", "AppleProRes" },
		{ "DisplayName", "Apple ProRes Encoder (mov)" },
		{ "IncludePath", "AppleProResEncoderProtocol.h" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat_MetaData[] = {
		{ "Category", "Apple ProRes Settings" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat = { "EncodingFormat", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleProResEncoderProtocol, EncodingFormat), Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderFormats, METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription_MetaData[] = {
		{ "Category", "Apple ProRes Settings" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription = { "ColorDescription", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleProResEncoderProtocol, ColorDescription), Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderColorDescription, METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType_MetaData[] = {
		{ "Category", "Apple ProRes Settings" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType = { "ScanType", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleProResEncoderProtocol, ScanType), Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderScanType, METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads_MetaData[] = {
		{ "Category", "Apple ProRes Settings" },
		{ "ClampMax", "64.0" },
		{ "ClampMin", "0.0" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
		{ "UIMax", "64.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads = { "NumberOfEncodingThreads", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAppleProResEncoderProtocol, NumberOfEncodingThreads), METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack_MetaData[] = {
		{ "Category", "Apple ProRes Settings" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
	};
#endif
	void Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack_SetBit(void* Obj)
	{
		((UAppleProResEncoderProtocol*)Obj)->bEmbedTimecodeTrack = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack = { "bEmbedTimecodeTrack", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAppleProResEncoderProtocol), &Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode_MetaData[] = {
		{ "Category", "Apple ProRes Settings" },
		{ "Comment", "/** Use Drop Frame Timecode when applicable (29.97p or 59.94i). */" },
		{ "ModuleRelativePath", "Private/AppleProResEncoderProtocol.h" },
		{ "ToolTip", "Use Drop Frame Timecode when applicable (29.97p or 59.94i)." },
	};
#endif
	void Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode_SetBit(void* Obj)
	{
		((UAppleProResEncoderProtocol*)Obj)->bDropFrameTimecode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode = { "bDropFrameTimecode", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAppleProResEncoderProtocol), &Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_EncodingFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ColorDescription,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_ScanType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_NumberOfEncodingThreads,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bEmbedTimecodeTrack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::NewProp_bDropFrameTimecode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAppleProResEncoderProtocol>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::ClassParams = {
		&UAppleProResEncoderProtocol::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::PropPointers),
		0,
		0x001004A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAppleProResEncoderProtocol()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAppleProResEncoderProtocol_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAppleProResEncoderProtocol, 3001948459);
	template<> APPLEPRORESMEDIA_API UClass* StaticClass<UAppleProResEncoderProtocol>()
	{
		return UAppleProResEncoderProtocol::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAppleProResEncoderProtocol(Z_Construct_UClass_UAppleProResEncoderProtocol, &UAppleProResEncoderProtocol::StaticClass, TEXT("/Script/AppleProResMedia"), TEXT("UAppleProResEncoderProtocol"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAppleProResEncoderProtocol);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
