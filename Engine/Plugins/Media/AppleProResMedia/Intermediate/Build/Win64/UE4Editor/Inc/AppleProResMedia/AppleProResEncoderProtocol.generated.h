// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APPLEPRORESMEDIA_AppleProResEncoderProtocol_generated_h
#error "AppleProResEncoderProtocol.generated.h already included, missing '#pragma once' in AppleProResEncoderProtocol.h"
#endif
#define APPLEPRORESMEDIA_AppleProResEncoderProtocol_generated_h

#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_SPARSE_DATA
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_RPC_WRAPPERS
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAppleProResEncoderProtocol(); \
	friend struct Z_Construct_UClass_UAppleProResEncoderProtocol_Statics; \
public: \
	DECLARE_CLASS(UAppleProResEncoderProtocol, UFrameGrabberProtocol, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AppleProResMedia"), NO_API) \
	DECLARE_SERIALIZER(UAppleProResEncoderProtocol)


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_INCLASS \
private: \
	static void StaticRegisterNativesUAppleProResEncoderProtocol(); \
	friend struct Z_Construct_UClass_UAppleProResEncoderProtocol_Statics; \
public: \
	DECLARE_CLASS(UAppleProResEncoderProtocol, UFrameGrabberProtocol, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AppleProResMedia"), NO_API) \
	DECLARE_SERIALIZER(UAppleProResEncoderProtocol)


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAppleProResEncoderProtocol(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleProResEncoderProtocol) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleProResEncoderProtocol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleProResEncoderProtocol); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleProResEncoderProtocol(UAppleProResEncoderProtocol&&); \
	NO_API UAppleProResEncoderProtocol(const UAppleProResEncoderProtocol&); \
public:


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAppleProResEncoderProtocol(UAppleProResEncoderProtocol&&); \
	NO_API UAppleProResEncoderProtocol(const UAppleProResEncoderProtocol&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAppleProResEncoderProtocol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAppleProResEncoderProtocol); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAppleProResEncoderProtocol)


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_48_PROLOG
#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_SPARSE_DATA \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_RPC_WRAPPERS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_INCLASS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_SPARSE_DATA \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h_52_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APPLEPRORESMEDIA_API UClass* StaticClass<class UAppleProResEncoderProtocol>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AppleProResMedia_Source_AppleProResMedia_Private_AppleProResEncoderProtocol_h


#define FOREACH_ENUM_EAPPLEPRORESENCODERSCANTYPE(op) \
	op(EAppleProResEncoderScanType::IM_PROGRESSIVE_SCAN) \
	op(EAppleProResEncoderScanType::IM_INTERLACED_TOP_FIELD_FIRST) \
	op(EAppleProResEncoderScanType::IM_INTERLATED_BOTTOM_FIRST_FIRST) 

enum class EAppleProResEncoderScanType : uint8;
template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderScanType>();

#define FOREACH_ENUM_EAPPLEPRORESENCODERCOLORDESCRIPTION(op) \
	op(EAppleProResEncoderColorDescription::CD_SDREC601_525_60HZ) \
	op(EAppleProResEncoderColorDescription::CD_SDREC601_625_50HZ) \
	op(EAppleProResEncoderColorDescription::CD_HDREC709) 

enum class EAppleProResEncoderColorDescription : uint8;
template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderColorDescription>();

#define FOREACH_ENUM_EAPPLEPRORESENCODERFORMATS(op) \
	op(EAppleProResEncoderFormats::F_422HQ) \
	op(EAppleProResEncoderFormats::F_422) \
	op(EAppleProResEncoderFormats::F_422LT) \
	op(EAppleProResEncoderFormats::F_422Proxy) \
	op(EAppleProResEncoderFormats::F_4444) \
	op(EAppleProResEncoderFormats::F_4444XQ) 

enum class EAppleProResEncoderFormats : uint8;
template<> APPLEPRORESMEDIA_API UEnum* StaticEnum<EAppleProResEncoderFormats>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
