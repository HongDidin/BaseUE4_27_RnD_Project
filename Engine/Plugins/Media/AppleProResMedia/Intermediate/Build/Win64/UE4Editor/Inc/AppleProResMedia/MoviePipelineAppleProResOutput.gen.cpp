// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AppleProResMedia/Private/MoviePipelineAppleProResOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineAppleProResOutput() {}
// Cross Module References
	APPLEPRORESMEDIA_API UClass* Z_Construct_UClass_UMoviePipelineAppleProResOutput_NoRegister();
	APPLEPRORESMEDIA_API UClass* Z_Construct_UClass_UMoviePipelineAppleProResOutput();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineVideoOutputBase();
	UPackage* Z_Construct_UPackage__Script_AppleProResMedia();
	APPLEPRORESMEDIA_API UEnum* Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec();
// End Cross Module References
	void UMoviePipelineAppleProResOutput::StaticRegisterNativesUMoviePipelineAppleProResOutput()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineAppleProResOutput_NoRegister()
	{
		return UMoviePipelineAppleProResOutput::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Codec_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Codec_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Codec;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDropFrameTimecode_MetaData[];
#endif
		static void NewProp_bDropFrameTimecode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDropFrameTimecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideMaximumEncodingThreads_MetaData[];
#endif
		static void NewProp_bOverrideMaximumEncodingThreads_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideMaximumEncodingThreads;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumberOfEncodingThreads_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumberOfEncodingThreads;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineVideoOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AppleProResMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Forward Declare\n" },
		{ "IncludePath", "MoviePipelineAppleProResOutput.h" },
		{ "ModuleRelativePath", "Private/MoviePipelineAppleProResOutput.h" },
		{ "ToolTip", "Forward Declare" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Which Apple ProRes codec should we use? See Apple documentation for more specifics. Uses Rec 709 color primaries. */" },
		{ "ModuleRelativePath", "Private/MoviePipelineAppleProResOutput.h" },
		{ "ToolTip", "Which Apple ProRes codec should we use? See Apple documentation for more specifics. Uses Rec 709 color primaries." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec = { "Codec", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineAppleProResOutput, Codec), Z_Construct_UEnum_AppleProResMedia_EAppleProResEncoderCodec, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Should the embedded timecode track be written using drop-frame format? Only applicable if the sequence framerate is 29.97 */" },
		{ "ModuleRelativePath", "Private/MoviePipelineAppleProResOutput.h" },
		{ "ToolTip", "Should the embedded timecode track be written using drop-frame format? Only applicable if the sequence framerate is 29.97" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode_SetBit(void* Obj)
	{
		((UMoviePipelineAppleProResOutput*)Obj)->bDropFrameTimecode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode = { "bDropFrameTimecode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineAppleProResOutput), &Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Should we override the maximum number of encoding threads? */" },
		{ "ModuleRelativePath", "Private/MoviePipelineAppleProResOutput.h" },
		{ "ToolTip", "Should we override the maximum number of encoding threads?" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads_SetBit(void* Obj)
	{
		((UMoviePipelineAppleProResOutput*)Obj)->bOverrideMaximumEncodingThreads = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads = { "bOverrideMaximumEncodingThreads", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineAppleProResOutput), &Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_MaxNumberOfEncodingThreads_MetaData[] = {
		{ "Category", "Settings" },
		{ "ClampMin", "0" },
		{ "Comment", "/** What is the maximum number of threads the encoder should use to encode frames with? Zero means auto-determine based on hardware. */" },
		{ "EditCondition", "bOverrideMaximumEncodingThreads" },
		{ "ModuleRelativePath", "Private/MoviePipelineAppleProResOutput.h" },
		{ "ToolTip", "What is the maximum number of threads the encoder should use to encode frames with? Zero means auto-determine based on hardware." },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_MaxNumberOfEncodingThreads = { "MaxNumberOfEncodingThreads", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineAppleProResOutput, MaxNumberOfEncodingThreads), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_MaxNumberOfEncodingThreads_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_MaxNumberOfEncodingThreads_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_Codec,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bDropFrameTimecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_bOverrideMaximumEncodingThreads,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::NewProp_MaxNumberOfEncodingThreads,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineAppleProResOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::ClassParams = {
		&UMoviePipelineAppleProResOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineAppleProResOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineAppleProResOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineAppleProResOutput, 3774051374);
	template<> APPLEPRORESMEDIA_API UClass* StaticClass<UMoviePipelineAppleProResOutput>()
	{
		return UMoviePipelineAppleProResOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineAppleProResOutput(Z_Construct_UClass_UMoviePipelineAppleProResOutput, &UMoviePipelineAppleProResOutput::StaticClass, TEXT("/Script/AppleProResMedia"), TEXT("UMoviePipelineAppleProResOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineAppleProResOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
