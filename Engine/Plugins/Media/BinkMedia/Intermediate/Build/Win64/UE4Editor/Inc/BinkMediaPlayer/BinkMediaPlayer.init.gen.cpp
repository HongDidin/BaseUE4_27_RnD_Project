// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBinkMediaPlayer_init() {}
	BINKMEDIAPLAYER_API UFunction* Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaClosed__DelegateSignature();
	BINKMEDIAPLAYER_API UFunction* Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaOpened__DelegateSignature();
	BINKMEDIAPLAYER_API UFunction* Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaReachedEnd__DelegateSignature();
	BINKMEDIAPLAYER_API UFunction* Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_BinkMediaPlayer()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaClosed__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaOpened__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaReachedEnd__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_BinkMediaPlayer_OnBinkMediaPlayerMediaEvent__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/BinkMediaPlayer",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x973EE8F7,
				0x8E7F6C13,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
