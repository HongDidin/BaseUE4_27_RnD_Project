// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BinkMediaPlayerEditor/Private/Factories/BinkMediaPlayerFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBinkMediaPlayerFactory() {}
// Cross Module References
	BINKMEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UBinkMediaPlayerFactory_NoRegister();
	BINKMEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UBinkMediaPlayerFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_BinkMediaPlayerEditor();
// End Cross Module References
	void UBinkMediaPlayerFactory::StaticRegisterNativesUBinkMediaPlayerFactory()
	{
	}
	UClass* Z_Construct_UClass_UBinkMediaPlayerFactory_NoRegister()
	{
		return UBinkMediaPlayerFactory::StaticClass();
	}
	struct Z_Construct_UClass_UBinkMediaPlayerFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_BinkMediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/BinkMediaPlayerFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/BinkMediaPlayerFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBinkMediaPlayerFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::ClassParams = {
		&UBinkMediaPlayerFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBinkMediaPlayerFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBinkMediaPlayerFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBinkMediaPlayerFactory, 421029257);
	template<> BINKMEDIAPLAYEREDITOR_API UClass* StaticClass<UBinkMediaPlayerFactory>()
	{
		return UBinkMediaPlayerFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBinkMediaPlayerFactory(Z_Construct_UClass_UBinkMediaPlayerFactory, &UBinkMediaPlayerFactory::StaticClass, TEXT("/Script/BinkMediaPlayerEditor"), TEXT("UBinkMediaPlayerFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBinkMediaPlayerFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
