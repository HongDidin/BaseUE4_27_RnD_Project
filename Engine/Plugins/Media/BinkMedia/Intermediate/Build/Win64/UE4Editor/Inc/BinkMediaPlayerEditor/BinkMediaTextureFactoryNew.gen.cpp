// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BinkMediaPlayerEditor/Private/Factories/BinkMediaTextureFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBinkMediaTextureFactoryNew() {}
// Cross Module References
	BINKMEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UBinkMediaTextureFactoryNew_NoRegister();
	BINKMEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UBinkMediaTextureFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_BinkMediaPlayerEditor();
	BINKMEDIAPLAYER_API UClass* Z_Construct_UClass_UBinkMediaPlayer_NoRegister();
// End Cross Module References
	void UBinkMediaTextureFactoryNew::StaticRegisterNativesUBinkMediaTextureFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UBinkMediaTextureFactoryNew_NoRegister()
	{
		return UBinkMediaTextureFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialMediaPlayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InitialMediaPlayer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_BinkMediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/BinkMediaTextureFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/BinkMediaTextureFactoryNew.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::NewProp_InitialMediaPlayer_MetaData[] = {
		{ "ModuleRelativePath", "Private/Factories/BinkMediaTextureFactoryNew.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::NewProp_InitialMediaPlayer = { "InitialMediaPlayer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBinkMediaTextureFactoryNew, InitialMediaPlayer), Z_Construct_UClass_UBinkMediaPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::NewProp_InitialMediaPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::NewProp_InitialMediaPlayer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::NewProp_InitialMediaPlayer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBinkMediaTextureFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::ClassParams = {
		&UBinkMediaTextureFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBinkMediaTextureFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBinkMediaTextureFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBinkMediaTextureFactoryNew, 3420423754);
	template<> BINKMEDIAPLAYEREDITOR_API UClass* StaticClass<UBinkMediaTextureFactoryNew>()
	{
		return UBinkMediaTextureFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBinkMediaTextureFactoryNew(Z_Construct_UClass_UBinkMediaTextureFactoryNew, &UBinkMediaTextureFactoryNew::StaticClass, TEXT("/Script/BinkMediaPlayerEditor"), TEXT("UBinkMediaTextureFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBinkMediaTextureFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
