// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BINKMEDIAPLAYEREDITOR_BinkMediaPlayerFactory_generated_h
#error "BinkMediaPlayerFactory.generated.h already included, missing '#pragma once' in BinkMediaPlayerFactory.h"
#endif
#define BINKMEDIAPLAYEREDITOR_BinkMediaPlayerFactory_generated_h

#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_SPARSE_DATA
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_RPC_WRAPPERS
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBinkMediaPlayerFactory(); \
	friend struct Z_Construct_UClass_UBinkMediaPlayerFactory_Statics; \
public: \
	DECLARE_CLASS(UBinkMediaPlayerFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BinkMediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UBinkMediaPlayerFactory)


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUBinkMediaPlayerFactory(); \
	friend struct Z_Construct_UClass_UBinkMediaPlayerFactory_Statics; \
public: \
	DECLARE_CLASS(UBinkMediaPlayerFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BinkMediaPlayerEditor"), NO_API) \
	DECLARE_SERIALIZER(UBinkMediaPlayerFactory)


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBinkMediaPlayerFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBinkMediaPlayerFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBinkMediaPlayerFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBinkMediaPlayerFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBinkMediaPlayerFactory(UBinkMediaPlayerFactory&&); \
	NO_API UBinkMediaPlayerFactory(const UBinkMediaPlayerFactory&); \
public:


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBinkMediaPlayerFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBinkMediaPlayerFactory(UBinkMediaPlayerFactory&&); \
	NO_API UBinkMediaPlayerFactory(const UBinkMediaPlayerFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBinkMediaPlayerFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBinkMediaPlayerFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBinkMediaPlayerFactory)


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_9_PROLOG
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_SPARSE_DATA \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_RPC_WRAPPERS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_INCLASS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_SPARSE_DATA \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BinkMediaPlayerFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BINKMEDIAPLAYEREDITOR_API UClass* StaticClass<class UBinkMediaPlayerFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayerEditor_Private_Factories_BinkMediaPlayerFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
