// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UBinkMediaPlayer;
#ifdef BINKMEDIAPLAYER_BinkMediaTexture_generated_h
#error "BinkMediaTexture.generated.h already included, missing '#pragma once' in BinkMediaTexture.h"
#endif
#define BINKMEDIAPLAYER_BinkMediaTexture_generated_h

#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_SPARSE_DATA
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execSetMediaPlayer);


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execSetMediaPlayer);


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBinkMediaTexture(); \
	friend struct Z_Construct_UClass_UBinkMediaTexture_Statics; \
public: \
	DECLARE_CLASS(UBinkMediaTexture, UTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BinkMediaPlayer"), NO_API) \
	DECLARE_SERIALIZER(UBinkMediaTexture)


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUBinkMediaTexture(); \
	friend struct Z_Construct_UClass_UBinkMediaTexture_Statics; \
public: \
	DECLARE_CLASS(UBinkMediaTexture, UTexture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BinkMediaPlayer"), NO_API) \
	DECLARE_SERIALIZER(UBinkMediaTexture)


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBinkMediaTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBinkMediaTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBinkMediaTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBinkMediaTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBinkMediaTexture(UBinkMediaTexture&&); \
	NO_API UBinkMediaTexture(const UBinkMediaTexture&); \
public:


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBinkMediaTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBinkMediaTexture(UBinkMediaTexture&&); \
	NO_API UBinkMediaTexture(const UBinkMediaTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBinkMediaTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBinkMediaTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBinkMediaTexture)


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_18_PROLOG
#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_SPARSE_DATA \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_RPC_WRAPPERS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_INCLASS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_SPARSE_DATA \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BinkMediaTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BINKMEDIAPLAYER_API UClass* StaticClass<class UBinkMediaTexture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_BinkMedia_Source_BinkMediaPlayer_Public_BinkMediaTexture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
