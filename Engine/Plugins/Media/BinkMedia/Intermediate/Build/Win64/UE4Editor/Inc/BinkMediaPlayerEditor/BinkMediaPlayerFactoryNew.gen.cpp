// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BinkMediaPlayerEditor/Private/Factories/BinkMediaPlayerFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBinkMediaPlayerFactoryNew() {}
// Cross Module References
	BINKMEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UBinkMediaPlayerFactoryNew_NoRegister();
	BINKMEDIAPLAYEREDITOR_API UClass* Z_Construct_UClass_UBinkMediaPlayerFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_BinkMediaPlayerEditor();
// End Cross Module References
	void UBinkMediaPlayerFactoryNew::StaticRegisterNativesUBinkMediaPlayerFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UBinkMediaPlayerFactoryNew_NoRegister()
	{
		return UBinkMediaPlayerFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_BinkMediaPlayerEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/BinkMediaPlayerFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/BinkMediaPlayerFactoryNew.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBinkMediaPlayerFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::ClassParams = {
		&UBinkMediaPlayerFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBinkMediaPlayerFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBinkMediaPlayerFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBinkMediaPlayerFactoryNew, 594370626);
	template<> BINKMEDIAPLAYEREDITOR_API UClass* StaticClass<UBinkMediaPlayerFactoryNew>()
	{
		return UBinkMediaPlayerFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBinkMediaPlayerFactoryNew(Z_Construct_UClass_UBinkMediaPlayerFactoryNew, &UBinkMediaPlayerFactoryNew::StaticClass, TEXT("/Script/BinkMediaPlayerEditor"), TEXT("UBinkMediaPlayerFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBinkMediaPlayerFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
