// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HLMEDIAEDITOR_HLMediaSourceFactory_generated_h
#error "HLMediaSourceFactory.generated.h already included, missing '#pragma once' in HLMediaSourceFactory.h"
#endif
#define HLMEDIAEDITOR_HLMediaSourceFactory_generated_h

#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_SPARSE_DATA
#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHLMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UHLMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UHLMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HLMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UHLMediaSourceFactory)


#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUHLMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UHLMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UHLMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HLMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UHLMediaSourceFactory)


#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHLMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHLMediaSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHLMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHLMediaSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHLMediaSourceFactory(UHLMediaSourceFactory&&); \
	NO_API UHLMediaSourceFactory(const UHLMediaSourceFactory&); \
public:


#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHLMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHLMediaSourceFactory(UHLMediaSourceFactory&&); \
	NO_API UHLMediaSourceFactory(const UHLMediaSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHLMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHLMediaSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHLMediaSourceFactory)


#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_14_PROLOG
#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_INCLASS \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class HLMediaSourceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HLMEDIAEDITOR_API UClass* StaticClass<class UHLMediaSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_HLMedia_Source_HLMediaEditor_Private_HLMediaSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
