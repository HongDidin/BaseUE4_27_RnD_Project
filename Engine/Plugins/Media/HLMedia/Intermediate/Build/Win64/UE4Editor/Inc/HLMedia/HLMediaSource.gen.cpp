// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HLMedia/Private/HLMediaSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHLMediaSource() {}
// Cross Module References
	HLMEDIA_API UClass* Z_Construct_UClass_UHLMediaSource_NoRegister();
	HLMEDIA_API UClass* Z_Construct_UClass_UHLMediaSource();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UBaseMediaSource();
	UPackage* Z_Construct_UPackage__Script_HLMedia();
// End Cross Module References
	void UHLMediaSource::StaticRegisterNativesUHLMediaSource()
	{
	}
	UClass* Z_Construct_UClass_UHLMediaSource_NoRegister()
	{
		return UHLMediaSource::StaticClass();
	}
	struct Z_Construct_UClass_UHLMediaSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsAdaptiveSource_MetaData[];
#endif
		static void NewProp_IsAdaptiveSource_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsAdaptiveSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StreamUrl_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StreamUrl;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHLMediaSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseMediaSource,
		(UObject* (*)())Z_Construct_UPackage__Script_HLMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHLMediaSource_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "HLMediaSource.h" },
		{ "ModuleRelativePath", "Private/HLMediaSource.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource_MetaData[] = {
		{ "Category", "Stream" },
		{ "Comment", "/** The URL property is an Adaptive Streaming playlist. */" },
		{ "ModuleRelativePath", "Private/HLMediaSource.h" },
		{ "ToolTip", "The URL property is an Adaptive Streaming playlist." },
	};
#endif
	void Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource_SetBit(void* Obj)
	{
		((UHLMediaSource*)Obj)->IsAdaptiveSource = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource = { "IsAdaptiveSource", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UHLMediaSource), &Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource_SetBit, METADATA_PARAMS(Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHLMediaSource_Statics::NewProp_StreamUrl_MetaData[] = {
		{ "Category", "Stream" },
		{ "Comment", "/** The URL to the media stream to be played. */" },
		{ "ModuleRelativePath", "Private/HLMediaSource.h" },
		{ "ToolTip", "The URL to the media stream to be played." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UHLMediaSource_Statics::NewProp_StreamUrl = { "StreamUrl", nullptr, (EPropertyFlags)0x0010010000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHLMediaSource, StreamUrl), METADATA_PARAMS(Z_Construct_UClass_UHLMediaSource_Statics::NewProp_StreamUrl_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHLMediaSource_Statics::NewProp_StreamUrl_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHLMediaSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHLMediaSource_Statics::NewProp_IsAdaptiveSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHLMediaSource_Statics::NewProp_StreamUrl,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHLMediaSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHLMediaSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHLMediaSource_Statics::ClassParams = {
		&UHLMediaSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHLMediaSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHLMediaSource_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHLMediaSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHLMediaSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHLMediaSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHLMediaSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHLMediaSource, 3260605582);
	template<> HLMEDIA_API UClass* StaticClass<UHLMediaSource>()
	{
		return UHLMediaSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHLMediaSource(Z_Construct_UClass_UHLMediaSource, &UHLMediaSource::StaticClass, TEXT("/Script/HLMedia"), TEXT("UHLMediaSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHLMediaSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
