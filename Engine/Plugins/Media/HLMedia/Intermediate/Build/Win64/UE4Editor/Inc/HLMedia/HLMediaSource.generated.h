// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HLMEDIA_HLMediaSource_generated_h
#error "HLMediaSource.generated.h already included, missing '#pragma once' in HLMediaSource.h"
#endif
#define HLMEDIA_HLMediaSource_generated_h

#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_SPARSE_DATA
#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_RPC_WRAPPERS
#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHLMediaSource(); \
	friend struct Z_Construct_UClass_UHLMediaSource_Statics; \
public: \
	DECLARE_CLASS(UHLMediaSource, UBaseMediaSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HLMedia"), NO_API) \
	DECLARE_SERIALIZER(UHLMediaSource)


#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUHLMediaSource(); \
	friend struct Z_Construct_UClass_UHLMediaSource_Statics; \
public: \
	DECLARE_CLASS(UHLMediaSource, UBaseMediaSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/HLMedia"), NO_API) \
	DECLARE_SERIALIZER(UHLMediaSource)


#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHLMediaSource(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHLMediaSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHLMediaSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHLMediaSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHLMediaSource(UHLMediaSource&&); \
	NO_API UHLMediaSource(const UHLMediaSource&); \
public:


#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHLMediaSource(UHLMediaSource&&); \
	NO_API UHLMediaSource(const UHLMediaSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHLMediaSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHLMediaSource); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHLMediaSource)


#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_11_PROLOG
#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_SPARSE_DATA \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_RPC_WRAPPERS \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_INCLASS \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_SPARSE_DATA \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HLMEDIA_API UClass* StaticClass<class UHLMediaSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_HLMedia_Source_HLMedia_Private_HLMediaSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
