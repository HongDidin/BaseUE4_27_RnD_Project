// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HLMediaEditor/Private/HLMediaSourceFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHLMediaSourceFactory() {}
// Cross Module References
	HLMEDIAEDITOR_API UClass* Z_Construct_UClass_UHLMediaSourceFactory_NoRegister();
	HLMEDIAEDITOR_API UClass* Z_Construct_UClass_UHLMediaSourceFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_HLMediaEditor();
// End Cross Module References
	void UHLMediaSourceFactory::StaticRegisterNativesUHLMediaSourceFactory()
	{
	}
	UClass* Z_Construct_UClass_UHLMediaSourceFactory_NoRegister()
	{
		return UHLMediaSourceFactory::StaticClass();
	}
	struct Z_Construct_UClass_UHLMediaSourceFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHLMediaSourceFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_HLMediaEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHLMediaSourceFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UHLMediaSource objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "HLMediaSourceFactory.h" },
		{ "ModuleRelativePath", "Private/HLMediaSourceFactory.h" },
		{ "ToolTip", "Implements a factory for UHLMediaSource objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHLMediaSourceFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHLMediaSourceFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHLMediaSourceFactory_Statics::ClassParams = {
		&UHLMediaSourceFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHLMediaSourceFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHLMediaSourceFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHLMediaSourceFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHLMediaSourceFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHLMediaSourceFactory, 1136552101);
	template<> HLMEDIAEDITOR_API UClass* StaticClass<UHLMediaSourceFactory>()
	{
		return UHLMediaSourceFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHLMediaSourceFactory(Z_Construct_UClass_UHLMediaSourceFactory, &UHLMediaSourceFactory::StaticClass, TEXT("/Script/HLMediaEditor"), TEXT("UHLMediaSourceFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHLMediaSourceFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
