// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AVFMEDIAEDITOR_AvfFileMediaSourceFactory_generated_h
#error "AvfFileMediaSourceFactory.generated.h already included, missing '#pragma once' in AvfFileMediaSourceFactory.h"
#endif
#define AVFMEDIAEDITOR_AvfFileMediaSourceFactory_generated_h

#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_SPARSE_DATA
#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAvfFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UAvfFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UAvfFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AvfMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UAvfFileMediaSourceFactory)


#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAvfFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UAvfFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UAvfFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AvfMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UAvfFileMediaSourceFactory)


#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAvfFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAvfFileMediaSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAvfFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAvfFileMediaSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAvfFileMediaSourceFactory(UAvfFileMediaSourceFactory&&); \
	NO_API UAvfFileMediaSourceFactory(const UAvfFileMediaSourceFactory&); \
public:


#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAvfFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAvfFileMediaSourceFactory(UAvfFileMediaSourceFactory&&); \
	NO_API UAvfFileMediaSourceFactory(const UAvfFileMediaSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAvfFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAvfFileMediaSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAvfFileMediaSourceFactory)


#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_14_PROLOG
#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_INCLASS \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AvfFileMediaSourceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AVFMEDIAEDITOR_API UClass* StaticClass<class UAvfFileMediaSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AvfMedia_Source_AvfMediaEditor_Private_AvfFileMediaSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
