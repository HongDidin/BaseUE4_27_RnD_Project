// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaMovieStreamer/Private/MediaMovieAssets.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaMovieAssets() {}
// Cross Module References
	MEDIAMOVIESTREAMER_API UClass* Z_Construct_UClass_UMediaMovieAssets_NoRegister();
	MEDIAMOVIESTREAMER_API UClass* Z_Construct_UClass_UMediaMovieAssets();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaMovieStreamer();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaPlayer_NoRegister();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaSource_NoRegister();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaTexture_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMediaMovieAssets::execOnMediaEnd)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnMediaEnd();
		P_NATIVE_END;
	}
	void UMediaMovieAssets::StaticRegisterNativesUMediaMovieAssets()
	{
		UClass* Class = UMediaMovieAssets::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnMediaEnd", &UMediaMovieAssets::execOnMediaEnd },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Called by the media player when the video ends.\n\x09 */" },
		{ "ModuleRelativePath", "Private/MediaMovieAssets.h" },
		{ "ToolTip", "Called by the media player when the video ends." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaMovieAssets, nullptr, "OnMediaEnd", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMediaMovieAssets_NoRegister()
	{
		return UMediaMovieAssets::StaticClass();
	}
	struct Z_Construct_UClass_UMediaMovieAssets_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaPlayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaPlayer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaMovieAssets_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaMovieStreamer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMediaMovieAssets_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMediaMovieAssets_OnMediaEnd, "OnMediaEnd" }, // 362173889
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaMovieAssets_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Keeps assets alive during level loading so they don't get garbage collected while we are using them.\n * Also handles other UObject functionality like hooking into the UMediaPlayer callbacks\n * which require a UObject.\n */" },
		{ "IncludePath", "MediaMovieAssets.h" },
		{ "ModuleRelativePath", "Private/MediaMovieAssets.h" },
		{ "ToolTip", "Keeps assets alive during level loading so they don't get garbage collected while we are using them.\nAlso handles other UObject functionality like hooking into the UMediaPlayer callbacks\nwhich require a UObject." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaPlayer_MetaData[] = {
		{ "Comment", "/** Holds the player we are using. */" },
		{ "ModuleRelativePath", "Private/MediaMovieAssets.h" },
		{ "ToolTip", "Holds the player we are using." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaPlayer = { "MediaPlayer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaMovieAssets, MediaPlayer), Z_Construct_UClass_UMediaPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaPlayer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaSource_MetaData[] = {
		{ "Comment", "/** Holds the media source we are using. */" },
		{ "ModuleRelativePath", "Private/MediaMovieAssets.h" },
		{ "ToolTip", "Holds the media source we are using." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaSource = { "MediaSource", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaMovieAssets, MediaSource), Z_Construct_UClass_UMediaSource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaTexture_MetaData[] = {
		{ "Comment", "/** Holds the media texture we are using. */" },
		{ "ModuleRelativePath", "Private/MediaMovieAssets.h" },
		{ "ToolTip", "Holds the media texture we are using." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaTexture = { "MediaTexture", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaMovieAssets, MediaTexture), Z_Construct_UClass_UMediaTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaMovieAssets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaPlayer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaMovieAssets_Statics::NewProp_MediaTexture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaMovieAssets_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaMovieAssets>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaMovieAssets_Statics::ClassParams = {
		&UMediaMovieAssets::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMediaMovieAssets_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaMovieAssets_Statics::PropPointers),
		0,
		0x000002A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaMovieAssets_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaMovieAssets_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaMovieAssets()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaMovieAssets_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaMovieAssets, 2215343957);
	template<> MEDIAMOVIESTREAMER_API UClass* StaticClass<UMediaMovieAssets>()
	{
		return UMediaMovieAssets::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaMovieAssets(Z_Construct_UClass_UMediaMovieAssets, &UMediaMovieAssets::StaticClass, TEXT("/Script/MediaMovieStreamer"), TEXT("UMediaMovieAssets"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaMovieAssets);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
