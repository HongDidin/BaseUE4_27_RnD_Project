// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAMOVIESTREAMER_MediaMovieAssets_generated_h
#error "MediaMovieAssets.generated.h already included, missing '#pragma once' in MediaMovieAssets.h"
#endif
#define MEDIAMOVIESTREAMER_MediaMovieAssets_generated_h

#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_SPARSE_DATA
#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnMediaEnd);


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnMediaEnd);


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaMovieAssets(); \
	friend struct Z_Construct_UClass_UMediaMovieAssets_Statics; \
public: \
	DECLARE_CLASS(UMediaMovieAssets, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MediaMovieStreamer"), NO_API) \
	DECLARE_SERIALIZER(UMediaMovieAssets)


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUMediaMovieAssets(); \
	friend struct Z_Construct_UClass_UMediaMovieAssets_Statics; \
public: \
	DECLARE_CLASS(UMediaMovieAssets, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MediaMovieStreamer"), NO_API) \
	DECLARE_SERIALIZER(UMediaMovieAssets)


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaMovieAssets(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaMovieAssets) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaMovieAssets); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaMovieAssets); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaMovieAssets(UMediaMovieAssets&&); \
	NO_API UMediaMovieAssets(const UMediaMovieAssets&); \
public:


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaMovieAssets(UMediaMovieAssets&&); \
	NO_API UMediaMovieAssets(const UMediaMovieAssets&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaMovieAssets); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaMovieAssets); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMediaMovieAssets)


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaPlayer() { return STRUCT_OFFSET(UMediaMovieAssets, MediaPlayer); } \
	FORCEINLINE static uint32 __PPO__MediaSource() { return STRUCT_OFFSET(UMediaMovieAssets, MediaSource); } \
	FORCEINLINE static uint32 __PPO__MediaTexture() { return STRUCT_OFFSET(UMediaMovieAssets, MediaTexture); }


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_21_PROLOG
#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_SPARSE_DATA \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_INCLASS \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_SPARSE_DATA \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAMOVIESTREAMER_API UClass* StaticClass<class UMediaMovieAssets>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaMovieStreamer_Source_MediaMovieStreamer_Private_MediaMovieAssets_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
