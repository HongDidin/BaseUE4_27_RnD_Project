// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMediaEditor/Private/Factories/AjaMediaOutputFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaMediaOutputFactoryNew() {}
// Cross Module References
	AJAMEDIAEDITOR_API UClass* Z_Construct_UClass_UAjaMediaOutputFactoryNew_NoRegister();
	AJAMEDIAEDITOR_API UClass* Z_Construct_UClass_UAjaMediaOutputFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AjaMediaEditor();
// End Cross Module References
	void UAjaMediaOutputFactoryNew::StaticRegisterNativesUAjaMediaOutputFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UAjaMediaOutputFactoryNew_NoRegister()
	{
		return UAjaMediaOutputFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMediaEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UAjaMediaOutput objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/AjaMediaOutputFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/AjaMediaOutputFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UAjaMediaOutput objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAjaMediaOutputFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::ClassParams = {
		&UAjaMediaOutputFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAjaMediaOutputFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAjaMediaOutputFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAjaMediaOutputFactoryNew, 3006780753);
	template<> AJAMEDIAEDITOR_API UClass* StaticClass<UAjaMediaOutputFactoryNew>()
	{
		return UAjaMediaOutputFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAjaMediaOutputFactoryNew(Z_Construct_UClass_UAjaMediaOutputFactoryNew, &UAjaMediaOutputFactoryNew::StaticClass, TEXT("/Script/AjaMediaEditor"), TEXT("UAjaMediaOutputFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAjaMediaOutputFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
