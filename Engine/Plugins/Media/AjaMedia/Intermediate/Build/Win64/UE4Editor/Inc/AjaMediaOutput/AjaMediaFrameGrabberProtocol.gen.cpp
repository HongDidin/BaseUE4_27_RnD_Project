// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMediaOutput/Public/AjaMediaFrameGrabberProtocol.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaMediaFrameGrabberProtocol() {}
// Cross Module References
	AJAMEDIAOUTPUT_API UClass* Z_Construct_UClass_UAjaFrameGrabberProtocol_NoRegister();
	AJAMEDIAOUTPUT_API UClass* Z_Construct_UClass_UAjaFrameGrabberProtocol();
	MOVIESCENECAPTURE_API UClass* Z_Construct_UClass_UMovieSceneImageCaptureProtocolBase();
	UPackage* Z_Construct_UPackage__Script_AjaMediaOutput();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	AJAMEDIAOUTPUT_API UClass* Z_Construct_UClass_UAjaMediaOutput_NoRegister();
	AJAMEDIAOUTPUT_API UClass* Z_Construct_UClass_UAjaMediaCapture_NoRegister();
// End Cross Module References
	void UAjaFrameGrabberProtocol::StaticRegisterNativesUAjaFrameGrabberProtocol()
	{
	}
	UClass* Z_Construct_UClass_UAjaFrameGrabberProtocol_NoRegister()
	{
		return UAjaFrameGrabberProtocol::StaticClass();
	}
	struct Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Information_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Information;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransientMediaOutputPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransientMediaOutputPtr;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransientMediaCapturePtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransientMediaCapturePtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneImageCaptureProtocolBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMediaOutput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::Class_MetaDataParams[] = {
		{ "CommandLineID", "AJAOutput" },
		{ "DisplayName", "AJA Output" },
		{ "IncludePath", "AjaMediaFrameGrabberProtocol.h" },
		{ "ModuleRelativePath", "Public/AjaMediaFrameGrabberProtocol.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "AllowedClasses", "AjaMediaOutput" },
		{ "Category", "AJA" },
		{ "Comment", "/** AJA Setting to use for the FrameGrabberProtocol */" },
		{ "ModuleRelativePath", "Public/AjaMediaFrameGrabberProtocol.h" },
		{ "ToolTip", "AJA Setting to use for the FrameGrabberProtocol" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaFrameGrabberProtocol, MediaOutput), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_Information_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** States unused options for AJAFrameGrabberProtocolSettings */" },
		{ "ModuleRelativePath", "Public/AjaMediaFrameGrabberProtocol.h" },
		{ "ToolTip", "States unused options for AJAFrameGrabberProtocolSettings" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_Information = { "Information", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaFrameGrabberProtocol, Information), METADATA_PARAMS(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_Information_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_Information_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaOutputPtr_MetaData[] = {
		{ "Comment", "/** Transient media output pointer to keep the media output alive while this protocol is in use */" },
		{ "ModuleRelativePath", "Public/AjaMediaFrameGrabberProtocol.h" },
		{ "ToolTip", "Transient media output pointer to keep the media output alive while this protocol is in use" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaOutputPtr = { "TransientMediaOutputPtr", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaFrameGrabberProtocol, TransientMediaOutputPtr), Z_Construct_UClass_UAjaMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaOutputPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaOutputPtr_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaCapturePtr_MetaData[] = {
		{ "Comment", "/** Transient media capture pointer that will capture the viewport */" },
		{ "ModuleRelativePath", "Public/AjaMediaFrameGrabberProtocol.h" },
		{ "ToolTip", "Transient media capture pointer that will capture the viewport" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaCapturePtr = { "TransientMediaCapturePtr", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaFrameGrabberProtocol, TransientMediaCapturePtr), Z_Construct_UClass_UAjaMediaCapture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaCapturePtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaCapturePtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_Information,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaOutputPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::NewProp_TransientMediaCapturePtr,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAjaFrameGrabberProtocol>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::ClassParams = {
		&UAjaFrameGrabberProtocol::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::PropPointers),
		0,
		0x001004A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAjaFrameGrabberProtocol()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAjaFrameGrabberProtocol, 478121641);
	template<> AJAMEDIAOUTPUT_API UClass* StaticClass<UAjaFrameGrabberProtocol>()
	{
		return UAjaFrameGrabberProtocol::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAjaFrameGrabberProtocol(Z_Construct_UClass_UAjaFrameGrabberProtocol, &UAjaFrameGrabberProtocol::StaticClass, TEXT("/Script/AjaMediaOutput"), TEXT("UAjaFrameGrabberProtocol"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAjaFrameGrabberProtocol);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
