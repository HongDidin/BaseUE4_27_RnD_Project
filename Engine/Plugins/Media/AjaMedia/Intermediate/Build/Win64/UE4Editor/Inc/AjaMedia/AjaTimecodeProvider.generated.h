// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AJAMEDIA_AjaTimecodeProvider_generated_h
#error "AjaTimecodeProvider.generated.h already included, missing '#pragma once' in AjaTimecodeProvider.h"
#endif
#define AJAMEDIA_AjaTimecodeProvider_generated_h

#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_SPARSE_DATA
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_RPC_WRAPPERS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UAjaTimecodeProvider, NO_API)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAjaTimecodeProvider(); \
	friend struct Z_Construct_UClass_UAjaTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(UAjaTimecodeProvider, UGenlockedTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMedia"), NO_API) \
	DECLARE_SERIALIZER(UAjaTimecodeProvider) \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_ARCHIVESERIALIZER


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUAjaTimecodeProvider(); \
	friend struct Z_Construct_UClass_UAjaTimecodeProvider_Statics; \
public: \
	DECLARE_CLASS(UAjaTimecodeProvider, UGenlockedTimecodeProvider, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMedia"), NO_API) \
	DECLARE_SERIALIZER(UAjaTimecodeProvider) \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_ARCHIVESERIALIZER


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaTimecodeProvider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaTimecodeProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaTimecodeProvider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaTimecodeProvider(UAjaTimecodeProvider&&); \
	NO_API UAjaTimecodeProvider(const UAjaTimecodeProvider&); \
public:


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaTimecodeProvider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaTimecodeProvider(UAjaTimecodeProvider&&); \
	NO_API UAjaTimecodeProvider(const UAjaTimecodeProvider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaTimecodeProvider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaTimecodeProvider); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaTimecodeProvider)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_24_PROLOG
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_RPC_WRAPPERS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_INCLASS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AjaTimecodeProvider."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AJAMEDIA_API UClass* StaticClass<class UAjaTimecodeProvider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaTimecodeProvider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
