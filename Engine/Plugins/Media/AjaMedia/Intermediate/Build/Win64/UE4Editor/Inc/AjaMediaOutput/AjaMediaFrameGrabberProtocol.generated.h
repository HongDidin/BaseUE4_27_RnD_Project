// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AJAMEDIAOUTPUT_AjaMediaFrameGrabberProtocol_generated_h
#error "AjaMediaFrameGrabberProtocol.generated.h already included, missing '#pragma once' in AjaMediaFrameGrabberProtocol.h"
#endif
#define AJAMEDIAOUTPUT_AjaMediaFrameGrabberProtocol_generated_h

#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_SPARSE_DATA
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAjaFrameGrabberProtocol(); \
	friend struct Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics; \
public: \
	DECLARE_CLASS(UAjaFrameGrabberProtocol, UMovieSceneImageCaptureProtocolBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AjaMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UAjaFrameGrabberProtocol)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAjaFrameGrabberProtocol(); \
	friend struct Z_Construct_UClass_UAjaFrameGrabberProtocol_Statics; \
public: \
	DECLARE_CLASS(UAjaFrameGrabberProtocol, UMovieSceneImageCaptureProtocolBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AjaMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UAjaFrameGrabberProtocol)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaFrameGrabberProtocol(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaFrameGrabberProtocol) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaFrameGrabberProtocol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaFrameGrabberProtocol); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaFrameGrabberProtocol(UAjaFrameGrabberProtocol&&); \
	NO_API UAjaFrameGrabberProtocol(const UAjaFrameGrabberProtocol&); \
public:


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaFrameGrabberProtocol(UAjaFrameGrabberProtocol&&); \
	NO_API UAjaFrameGrabberProtocol(const UAjaFrameGrabberProtocol&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaFrameGrabberProtocol); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaFrameGrabberProtocol); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaFrameGrabberProtocol)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TransientMediaOutputPtr() { return STRUCT_OFFSET(UAjaFrameGrabberProtocol, TransientMediaOutputPtr); } \
	FORCEINLINE static uint32 __PPO__TransientMediaCapturePtr() { return STRUCT_OFFSET(UAjaFrameGrabberProtocol, TransientMediaCapturePtr); }


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_14_PROLOG
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_INCLASS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AJAMEDIAOUTPUT_API UClass* StaticClass<class UAjaFrameGrabberProtocol>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaFrameGrabberProtocol_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
