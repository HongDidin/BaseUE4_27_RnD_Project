// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AJAMEDIA_AjaCustomTimeStep_generated_h
#error "AjaCustomTimeStep.generated.h already included, missing '#pragma once' in AjaCustomTimeStep.h"
#endif
#define AJAMEDIA_AjaCustomTimeStep_generated_h

#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_SPARSE_DATA
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_RPC_WRAPPERS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAjaCustomTimeStep(); \
	friend struct Z_Construct_UClass_UAjaCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UAjaCustomTimeStep, UGenlockedCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMedia"), NO_API) \
	DECLARE_SERIALIZER(UAjaCustomTimeStep)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUAjaCustomTimeStep(); \
	friend struct Z_Construct_UClass_UAjaCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UAjaCustomTimeStep, UGenlockedCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMedia"), NO_API) \
	DECLARE_SERIALIZER(UAjaCustomTimeStep)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaCustomTimeStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaCustomTimeStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaCustomTimeStep(UAjaCustomTimeStep&&); \
	NO_API UAjaCustomTimeStep(const UAjaCustomTimeStep&); \
public:


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaCustomTimeStep(UAjaCustomTimeStep&&); \
	NO_API UAjaCustomTimeStep(const UAjaCustomTimeStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaCustomTimeStep); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaCustomTimeStep)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_21_PROLOG
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_RPC_WRAPPERS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_INCLASS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h_24_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AjaCustomTimeStep."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AJAMEDIA_API UClass* StaticClass<class UAjaCustomTimeStep>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaCustomTimeStep_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
