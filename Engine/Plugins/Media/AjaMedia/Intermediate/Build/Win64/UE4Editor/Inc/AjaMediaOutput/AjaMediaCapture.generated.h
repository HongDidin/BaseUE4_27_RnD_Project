// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AJAMEDIAOUTPUT_AjaMediaCapture_generated_h
#error "AjaMediaCapture.generated.h already included, missing '#pragma once' in AjaMediaCapture.h"
#endif
#define AJAMEDIAOUTPUT_AjaMediaCapture_generated_h

#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_SPARSE_DATA
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_RPC_WRAPPERS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAjaMediaCapture(); \
	friend struct Z_Construct_UClass_UAjaMediaCapture_Statics; \
public: \
	DECLARE_CLASS(UAjaMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UAjaMediaCapture)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUAjaMediaCapture(); \
	friend struct Z_Construct_UClass_UAjaMediaCapture_Statics; \
public: \
	DECLARE_CLASS(UAjaMediaCapture, UMediaCapture, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMediaOutput"), NO_API) \
	DECLARE_SERIALIZER(UAjaMediaCapture)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaMediaCapture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaMediaCapture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaMediaCapture(UAjaMediaCapture&&); \
	NO_API UAjaMediaCapture(const UAjaMediaCapture&); \
public:


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaMediaCapture(UAjaMediaCapture&&); \
	NO_API UAjaMediaCapture(const UAjaMediaCapture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaMediaCapture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaMediaCapture)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_19_PROLOG
#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_RPC_WRAPPERS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_INCLASS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AjaMediaCapture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AJAMEDIAOUTPUT_API UClass* StaticClass<class UAjaMediaCapture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AjaMedia_Source_AjaMediaOutput_Public_AjaMediaCapture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
