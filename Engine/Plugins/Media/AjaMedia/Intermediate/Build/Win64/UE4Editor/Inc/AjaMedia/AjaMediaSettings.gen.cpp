// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMedia/Public/AjaMediaSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaMediaSettings() {}
// Cross Module References
	AJAMEDIA_API UClass* Z_Construct_UClass_UAjaMediaSettings_NoRegister();
	AJAMEDIA_API UClass* Z_Construct_UClass_UAjaMediaSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_AjaMedia();
// End Cross Module References
	void UAjaMediaSettings::StaticRegisterNativesUAjaMediaSettings()
	{
	}
	UClass* Z_Construct_UClass_UAjaMediaSettings_NoRegister()
	{
		return UAjaMediaSettings::StaticClass();
	}
	struct Z_Construct_UClass_UAjaMediaSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAjaMediaSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the AjaMedia plug-in.\n */" },
		{ "IncludePath", "AjaMediaSettings.h" },
		{ "ModuleRelativePath", "Public/AjaMediaSettings.h" },
		{ "ToolTip", "Settings for the AjaMedia plug-in." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAjaMediaSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAjaMediaSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAjaMediaSettings_Statics::ClassParams = {
		&UAjaMediaSettings::StaticClass,
		"AjaMedia",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAjaMediaSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAjaMediaSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAjaMediaSettings, 2222769769);
	template<> AJAMEDIA_API UClass* StaticClass<UAjaMediaSettings>()
	{
		return UAjaMediaSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAjaMediaSettings(Z_Construct_UClass_UAjaMediaSettings, &UAjaMediaSettings::StaticClass, TEXT("/Script/AjaMedia"), TEXT("UAjaMediaSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAjaMediaSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
