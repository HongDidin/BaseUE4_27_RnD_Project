// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMediaOutput/Public/AjaMediaCapture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaMediaCapture() {}
// Cross Module References
	AJAMEDIAOUTPUT_API UClass* Z_Construct_UClass_UAjaMediaCapture_NoRegister();
	AJAMEDIAOUTPUT_API UClass* Z_Construct_UClass_UAjaMediaCapture();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture();
	UPackage* Z_Construct_UPackage__Script_AjaMediaOutput();
// End Cross Module References
	void UAjaMediaCapture::StaticRegisterNativesUAjaMediaCapture()
	{
	}
	UClass* Z_Construct_UClass_UAjaMediaCapture_NoRegister()
	{
		return UAjaMediaCapture::StaticClass();
	}
	struct Z_Construct_UClass_UAjaMediaCapture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAjaMediaCapture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaCapture,
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMediaOutput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaCapture_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Output Media for AJA streams.\n * The output format could be any of EAjaMediaOutputPixelFormat.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "AjaMediaCapture.h" },
		{ "ModuleRelativePath", "Public/AjaMediaCapture.h" },
		{ "ToolTip", "Output Media for AJA streams.\nThe output format could be any of EAjaMediaOutputPixelFormat." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAjaMediaCapture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAjaMediaCapture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAjaMediaCapture_Statics::ClassParams = {
		&UAjaMediaCapture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAjaMediaCapture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaCapture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAjaMediaCapture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAjaMediaCapture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAjaMediaCapture, 3303135526);
	template<> AJAMEDIAOUTPUT_API UClass* StaticClass<UAjaMediaCapture>()
	{
		return UAjaMediaCapture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAjaMediaCapture(Z_Construct_UClass_UAjaMediaCapture, &UAjaMediaCapture::StaticClass, TEXT("/Script/AjaMediaOutput"), TEXT("UAjaMediaCapture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAjaMediaCapture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
