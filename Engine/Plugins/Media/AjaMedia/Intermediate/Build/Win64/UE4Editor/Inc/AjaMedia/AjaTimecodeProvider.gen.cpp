// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMedia/Public/AjaTimecodeProvider.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaTimecodeProvider() {}
// Cross Module References
	AJAMEDIA_API UClass* Z_Construct_UClass_UAjaTimecodeProvider_NoRegister();
	AJAMEDIA_API UClass* Z_Construct_UClass_UAjaTimecodeProvider();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UGenlockedTimecodeProvider();
	UPackage* Z_Construct_UPackage__Script_AjaMedia();
	AJAMEDIA_API UScriptStruct* Z_Construct_UScriptStruct_FAjaMediaTimecodeReference();
	AJAMEDIA_API UScriptStruct* Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration();
	ENGINE_API UClass* Z_Construct_UClass_UEngine_NoRegister();
// End Cross Module References
	void UAjaTimecodeProvider::StaticRegisterNativesUAjaTimecodeProvider()
	{
	}
	UClass* Z_Construct_UClass_UAjaTimecodeProvider_NoRegister()
	{
		return UAjaTimecodeProvider::StaticClass();
	}
	struct Z_Construct_UClass_UAjaTimecodeProvider_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseDedicatedPin_MetaData[];
#endif
		static void NewProp_bUseDedicatedPin_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseDedicatedPin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseReferenceIn_MetaData[];
#endif
		static void NewProp_bUseReferenceIn_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseReferenceIn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LTCConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LTCConfiguration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VideoConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VideoConfiguration;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitializedEngine_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InitializedEngine;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAjaTimecodeProvider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGenlockedTimecodeProvider,
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaTimecodeProvider_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Class to fetch a timecode via an AJA card.\n * When the signal is lost in the editor (not in PIE), the TimecodeProvider will try to re-synchronize every second.\n */" },
		{ "DisplayName", "AJA SDI Input" },
		{ "IncludePath", "AjaTimecodeProvider.h" },
		{ "IsBlueprintBase", "true" },
		{ "MediaIOCustomLayout", "AJA" },
		{ "ModuleRelativePath", "Public/AjaTimecodeProvider.h" },
		{ "ToolTip", "Class to fetch a timecode via an AJA card.\nWhen the signal is lost in the editor (not in PIE), the TimecodeProvider will try to re-synchronize every second." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/**\n\x09 * Should we read the timecode from a dedicated LTC pin or an SDI input.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AjaTimecodeProvider.h" },
		{ "ToolTip", "Should we read the timecode from a dedicated LTC pin or an SDI input." },
	};
#endif
	void Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin_SetBit(void* Obj)
	{
		((UAjaTimecodeProvider*)Obj)->bUseDedicatedPin = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin = { "bUseDedicatedPin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaTimecodeProvider), &Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/**\n\x09 * Read LTC timecode from reference pin. Will fail if device doesn't support that feature.\n\x09 */" },
		{ "EditCondition", "bUseDedicatedPin" },
		{ "ModuleRelativePath", "Public/AjaTimecodeProvider.h" },
		{ "ToolTip", "Read LTC timecode from reference pin. Will fail if device doesn't support that feature." },
	};
#endif
	void Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn_SetBit(void* Obj)
	{
		((UAjaTimecodeProvider*)Obj)->bUseReferenceIn = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn = { "bUseReferenceIn", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaTimecodeProvider), &Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_LTCConfiguration_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/**\n\x09 * Where to read LTC timecode from with which FrameRate expected\n\x09 */" },
		{ "EditCondition", "bUseDedicatedPin" },
		{ "ModuleRelativePath", "Public/AjaTimecodeProvider.h" },
		{ "ToolTip", "Where to read LTC timecode from with which FrameRate expected" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_LTCConfiguration = { "LTCConfiguration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaTimecodeProvider, LTCConfiguration), Z_Construct_UScriptStruct_FAjaMediaTimecodeReference, METADATA_PARAMS(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_LTCConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_LTCConfiguration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_VideoConfiguration_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/**\n     * It read the timecode from an input source.\n\x09 */" },
		{ "EditCondition", "!bUseDedicatedPin" },
		{ "ModuleRelativePath", "Public/AjaTimecodeProvider.h" },
		{ "ToolTip", "It read the timecode from an input source." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_VideoConfiguration = { "VideoConfiguration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaTimecodeProvider, VideoConfiguration), Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration, METADATA_PARAMS(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_VideoConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_VideoConfiguration_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_InitializedEngine_MetaData[] = {
		{ "Comment", "/** Engine used to initialize the Provider */" },
		{ "ModuleRelativePath", "Public/AjaTimecodeProvider.h" },
		{ "ToolTip", "Engine used to initialize the Provider" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_InitializedEngine = { "InitializedEngine", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaTimecodeProvider, InitializedEngine), Z_Construct_UClass_UEngine_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_InitializedEngine_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_InitializedEngine_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAjaTimecodeProvider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseDedicatedPin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_bUseReferenceIn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_LTCConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_VideoConfiguration,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaTimecodeProvider_Statics::NewProp_InitializedEngine,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAjaTimecodeProvider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAjaTimecodeProvider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAjaTimecodeProvider_Statics::ClassParams = {
		&UAjaTimecodeProvider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAjaTimecodeProvider_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAjaTimecodeProvider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaTimecodeProvider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAjaTimecodeProvider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAjaTimecodeProvider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAjaTimecodeProvider, 2308726981);
	template<> AJAMEDIA_API UClass* StaticClass<UAjaTimecodeProvider>()
	{
		return UAjaTimecodeProvider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAjaTimecodeProvider(Z_Construct_UClass_UAjaTimecodeProvider, &UAjaTimecodeProvider::StaticClass, TEXT("/Script/AjaMedia"), TEXT("UAjaTimecodeProvider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAjaTimecodeProvider);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UAjaTimecodeProvider)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
