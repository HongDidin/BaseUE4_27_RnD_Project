// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMedia/Public/AjaDeviceProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaDeviceProvider() {}
// Cross Module References
	AJAMEDIA_API UScriptStruct* Z_Construct_UScriptStruct_FAjaMediaTimecodeReference();
	UPackage* Z_Construct_UPackage__Script_AjaMedia();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIODevice();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	AJAMEDIA_API UScriptStruct* Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConfiguration();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat();
// End Cross Module References
class UScriptStruct* FAjaMediaTimecodeReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AJAMEDIA_API uint32 Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference, Z_Construct_UPackage__Script_AjaMedia(), TEXT("AjaMediaTimecodeReference"), sizeof(FAjaMediaTimecodeReference), Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Hash());
	}
	return Singleton;
}
template<> AJAMEDIA_API UScriptStruct* StaticStruct<FAjaMediaTimecodeReference>()
{
	return FAjaMediaTimecodeReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAjaMediaTimecodeReference(FAjaMediaTimecodeReference::StaticStruct, TEXT("/Script/AjaMedia"), TEXT("AjaMediaTimecodeReference"), false, nullptr, nullptr);
static struct FScriptStruct_AjaMedia_StaticRegisterNativesFAjaMediaTimecodeReference
{
	FScriptStruct_AjaMedia_StaticRegisterNativesFAjaMediaTimecodeReference()
	{
		UScriptStruct::DeferCppStructOps<FAjaMediaTimecodeReference>(FName(TEXT("AjaMediaTimecodeReference")));
	}
} ScriptStruct_AjaMedia_StaticRegisterNativesFAjaMediaTimecodeReference;
	struct Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Device_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Device;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LtcIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LtcIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LtcFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LtcFrameRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Configuration of an AJA timecode.\n */" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "Configuration of an AJA timecode." },
	};
#endif
	void* Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAjaMediaTimecodeReference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_Device_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** The frame rate of the LTC from the reference pin.*/" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "The frame rate of the LTC from the reference pin." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_Device = { "Device", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAjaMediaTimecodeReference, Device), Z_Construct_UScriptStruct_FMediaIODevice, METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_Device_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_Device_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcIndex_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** The LTC index to read from the reference pin. */" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "The LTC index to read from the reference pin." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcIndex = { "LtcIndex", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAjaMediaTimecodeReference, LtcIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcFrameRate_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** The frame rate of the LTC from the reference pin.*/" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "The frame rate of the LTC from the reference pin." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcFrameRate = { "LtcFrameRate", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAjaMediaTimecodeReference, LtcFrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcFrameRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_Device,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::NewProp_LtcFrameRate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMedia,
		nullptr,
		&NewStructOps,
		"AjaMediaTimecodeReference",
		sizeof(FAjaMediaTimecodeReference),
		alignof(FAjaMediaTimecodeReference),
		Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAjaMediaTimecodeReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AjaMedia();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AjaMediaTimecodeReference"), sizeof(FAjaMediaTimecodeReference), Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Hash() { return 3643812644U; }
class UScriptStruct* FAjaMediaTimecodeConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AJAMEDIA_API uint32 Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration, Z_Construct_UPackage__Script_AjaMedia(), TEXT("AjaMediaTimecodeConfiguration"), sizeof(FAjaMediaTimecodeConfiguration), Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Hash());
	}
	return Singleton;
}
template<> AJAMEDIA_API UScriptStruct* StaticStruct<FAjaMediaTimecodeConfiguration>()
{
	return FAjaMediaTimecodeConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAjaMediaTimecodeConfiguration(FAjaMediaTimecodeConfiguration::StaticStruct, TEXT("/Script/AjaMedia"), TEXT("AjaMediaTimecodeConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_AjaMedia_StaticRegisterNativesFAjaMediaTimecodeConfiguration
{
	FScriptStruct_AjaMedia_StaticRegisterNativesFAjaMediaTimecodeConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FAjaMediaTimecodeConfiguration>(FName(TEXT("AjaMediaTimecodeConfiguration")));
	}
} ScriptStruct_AjaMedia_StaticRegisterNativesFAjaMediaTimecodeConfiguration;
	struct Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConfiguration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimecodeFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TimecodeFormat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Configuration of an AJA timecode from Video\n */" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "Configuration of an AJA timecode from Video" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAjaMediaTimecodeConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_MediaConfiguration_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** Read the timecode from a video signal. */" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "Read the timecode from a video signal." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_MediaConfiguration = { "MediaConfiguration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAjaMediaTimecodeConfiguration, MediaConfiguration), Z_Construct_UScriptStruct_FMediaIOConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_MediaConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_MediaConfiguration_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** Timecode format to read from a video signal. */" },
		{ "ModuleRelativePath", "Public/AjaDeviceProvider.h" },
		{ "ToolTip", "Timecode format to read from a video signal." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat = { "TimecodeFormat", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAjaMediaTimecodeConfiguration, TimecodeFormat), Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_MediaConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::NewProp_TimecodeFormat,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMedia,
		nullptr,
		&NewStructOps,
		"AjaMediaTimecodeConfiguration",
		sizeof(FAjaMediaTimecodeConfiguration),
		alignof(FAjaMediaTimecodeConfiguration),
		Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AjaMedia();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AjaMediaTimecodeConfiguration"), sizeof(FAjaMediaTimecodeConfiguration), Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Hash() { return 2408004187U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
