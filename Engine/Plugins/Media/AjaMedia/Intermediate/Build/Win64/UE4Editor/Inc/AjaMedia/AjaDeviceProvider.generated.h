// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AJAMEDIA_AjaDeviceProvider_generated_h
#error "AjaDeviceProvider.generated.h already included, missing '#pragma once' in AjaDeviceProvider.h"
#endif
#define AJAMEDIA_AjaDeviceProvider_generated_h

#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaDeviceProvider_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAjaMediaTimecodeReference_Statics; \
	static class UScriptStruct* StaticStruct();


template<> AJAMEDIA_API UScriptStruct* StaticStruct<struct FAjaMediaTimecodeReference>();

#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaDeviceProvider_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAjaMediaTimecodeConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> AJAMEDIA_API UScriptStruct* StaticStruct<struct FAjaMediaTimecodeConfiguration>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaDeviceProvider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
