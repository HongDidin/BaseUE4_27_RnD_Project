// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AjaMedia/Public/AjaMediaSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAjaMediaSource() {}
// Cross Module References
	AJAMEDIA_API UEnum* Z_Construct_UEnum_AjaMedia_EAjaMediaAudioChannel();
	UPackage* Z_Construct_UPackage__Script_AjaMedia();
	AJAMEDIA_API UEnum* Z_Construct_UEnum_AjaMedia_EAjaMediaSourceColorFormat();
	AJAMEDIA_API UClass* Z_Construct_UClass_UAjaMediaSource_NoRegister();
	AJAMEDIA_API UClass* Z_Construct_UClass_UAjaMediaSource();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UTimeSynchronizableMediaSource();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConfiguration();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat();
// End Cross Module References
	static UEnum* EAjaMediaAudioChannel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AjaMedia_EAjaMediaAudioChannel, Z_Construct_UPackage__Script_AjaMedia(), TEXT("EAjaMediaAudioChannel"));
		}
		return Singleton;
	}
	template<> AJAMEDIA_API UEnum* StaticEnum<EAjaMediaAudioChannel>()
	{
		return EAjaMediaAudioChannel_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAjaMediaAudioChannel(EAjaMediaAudioChannel_StaticEnum, TEXT("/Script/AjaMedia"), TEXT("EAjaMediaAudioChannel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AjaMedia_EAjaMediaAudioChannel_Hash() { return 657262459U; }
	UEnum* Z_Construct_UEnum_AjaMedia_EAjaMediaAudioChannel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AjaMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAjaMediaAudioChannel"), 0, Get_Z_Construct_UEnum_AjaMedia_EAjaMediaAudioChannel_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAjaMediaAudioChannel::Channel6", (int64)EAjaMediaAudioChannel::Channel6 },
				{ "EAjaMediaAudioChannel::Channel8", (int64)EAjaMediaAudioChannel::Channel8 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Channel6.Name", "EAjaMediaAudioChannel::Channel6" },
				{ "Channel8.Name", "EAjaMediaAudioChannel::Channel8" },
				{ "Comment", "/**\n * Available number of audio channel supported by UE4 & AJA\n */" },
				{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
				{ "ToolTip", "Available number of audio channel supported by UE4 & AJA" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AjaMedia,
				nullptr,
				"EAjaMediaAudioChannel",
				"EAjaMediaAudioChannel",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EAjaMediaSourceColorFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AjaMedia_EAjaMediaSourceColorFormat, Z_Construct_UPackage__Script_AjaMedia(), TEXT("EAjaMediaSourceColorFormat"));
		}
		return Singleton;
	}
	template<> AJAMEDIA_API UEnum* StaticEnum<EAjaMediaSourceColorFormat>()
	{
		return EAjaMediaSourceColorFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAjaMediaSourceColorFormat(EAjaMediaSourceColorFormat_StaticEnum, TEXT("/Script/AjaMedia"), TEXT("EAjaMediaSourceColorFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AjaMedia_EAjaMediaSourceColorFormat_Hash() { return 3724818003U; }
	UEnum* Z_Construct_UEnum_AjaMedia_EAjaMediaSourceColorFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AjaMedia();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAjaMediaSourceColorFormat"), 0, Get_Z_Construct_UEnum_AjaMedia_EAjaMediaSourceColorFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAjaMediaSourceColorFormat::YUV2_8bit", (int64)EAjaMediaSourceColorFormat::YUV2_8bit },
				{ "EAjaMediaSourceColorFormat::YUV_10bit", (int64)EAjaMediaSourceColorFormat::YUV_10bit },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Native data format.\n */" },
				{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
				{ "ToolTip", "Native data format." },
				{ "YUV2_8bit.DisplayName", "8bit YUV" },
				{ "YUV2_8bit.Name", "EAjaMediaSourceColorFormat::YUV2_8bit" },
				{ "YUV_10bit.DisplayName", "10bit YUV" },
				{ "YUV_10bit.Name", "EAjaMediaSourceColorFormat::YUV_10bit" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AjaMedia,
				nullptr,
				"EAjaMediaSourceColorFormat",
				"EAjaMediaSourceColorFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UAjaMediaSource::StaticRegisterNativesUAjaMediaSource()
	{
	}
	UClass* Z_Construct_UClass_UAjaMediaSource_NoRegister()
	{
		return UAjaMediaSource::StaticClass();
	}
	struct Z_Construct_UClass_UAjaMediaSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConfiguration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TimecodeFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimecodeFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TimecodeFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureWithAutoCirculating_MetaData[];
#endif
		static void NewProp_bCaptureWithAutoCirculating_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureWithAutoCirculating;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureAncillary_MetaData[];
#endif
		static void NewProp_bCaptureAncillary_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureAncillary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumAncillaryFrameBuffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumAncillaryFrameBuffer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureAudio_MetaData[];
#endif
		static void NewProp_bCaptureAudio_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureAudio;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AudioChannel_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AudioChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumAudioFrameBuffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumAudioFrameBuffer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureVideo_MetaData[];
#endif
		static void NewProp_bCaptureVideo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureVideo;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSRGBInput_MetaData[];
#endif
		static void NewProp_bIsSRGBInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSRGBInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumVideoFrameBuffer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumVideoFrameBuffer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLogDropFrame_MetaData[];
#endif
		static void NewProp_bLogDropFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLogDropFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEncodeTimecodeInTexel_MetaData[];
#endif
		static void NewProp_bEncodeTimecodeInTexel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEncodeTimecodeInTexel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAjaMediaSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTimeSynchronizableMediaSource,
		(UObject* (*)())Z_Construct_UPackage__Script_AjaMedia,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Media source for AJA streams.\n */" },
		{ "HideCategories", "Platforms Object Object Object" },
		{ "IncludePath", "AjaMediaSource.h" },
		{ "MediaIOCustomLayout", "AJA" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Media source for AJA streams." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MediaConfiguration_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** The device, port and video settings that correspond to the input. */" },
		{ "DisplayName", "Configuration" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "The device, port and video settings that correspond to the input." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MediaConfiguration = { "MediaConfiguration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, MediaConfiguration), Z_Construct_UScriptStruct_FMediaIOConfiguration, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MediaConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MediaConfiguration_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/** Use the time code embedded in the input stream. */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Use the time code embedded in the input stream." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat = { "TimecodeFormat", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, TimecodeFormat), Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating_MetaData[] = {
		{ "Category", "AJA" },
		{ "Comment", "/**\n\x09 * Use a ring buffer to capture and transfer data.\n\x09 * This may decrease transfer latency but increase stability.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Use a ring buffer to capture and transfer data.\nThis may decrease transfer latency but increase stability." },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bCaptureWithAutoCirculating = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating = { "bCaptureWithAutoCirculating", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary_MetaData[] = {
		{ "Category", "Ancillary" },
		{ "Comment", "/**\n\x09 * Capture Ancillary from the AJA source.\n\x09 * It will decrease performance\n\x09 */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Capture Ancillary from the AJA source.\nIt will decrease performance" },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bCaptureAncillary = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary = { "bCaptureAncillary", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAncillaryFrameBuffer_MetaData[] = {
		{ "Category", "Ancillary" },
		{ "ClampMax", "32" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum number of ancillary data frames to buffer. */" },
		{ "EditCondition", "bCaptureAncillary" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Maximum number of ancillary data frames to buffer." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAncillaryFrameBuffer = { "MaxNumAncillaryFrameBuffer", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, MaxNumAncillaryFrameBuffer), METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAncillaryFrameBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAncillaryFrameBuffer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Capture Audio from the AJA source. */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Capture Audio from the AJA source." },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bCaptureAudio = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio = { "bCaptureAudio", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel_MetaData[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Desired number of audio channel to capture. */" },
		{ "EditCondition", "bCaptureAudio" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Desired number of audio channel to capture." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel = { "AudioChannel", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, AudioChannel), Z_Construct_UEnum_AjaMedia_EAjaMediaAudioChannel, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer_MetaData[] = {
		{ "Category", "Audio" },
		{ "ClampMax", "32" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum number of audio frames to buffer. */" },
		{ "EditCondition", "bCaptureAudio" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Maximum number of audio frames to buffer." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer = { "MaxNumAudioFrameBuffer", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, MaxNumAudioFrameBuffer), METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo_MetaData[] = {
		{ "Category", "Video" },
		{ "Comment", "/** Capture Video from the AJA source. */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Capture Video from the AJA source." },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bCaptureVideo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo = { "bCaptureVideo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat_MetaData[] = {
		{ "Category", "Video" },
		{ "Comment", "/** Native data format internally used by the device after being converted from SDI/HDMI signal. */" },
		{ "EditCondition", "bCaptureVideo" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Native data format internally used by the device after being converted from SDI/HDMI signal." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat = { "ColorFormat", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, ColorFormat), Z_Construct_UEnum_AjaMedia_EAjaMediaSourceColorFormat, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput_MetaData[] = {
		{ "Category", "Video" },
		{ "Comment", "/** \n\x09 * Whether the video input is in sRGB color space.\n\x09 * A sRGB to Linear conversion will be applied resulting in a texture in linear space.\n\x09 * @Note If the texture is not in linear space, it won't look correct in the editor. Another pass will be required either through Composure or other means.\n\x09 */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Whether the video input is in sRGB color space.\nA sRGB to Linear conversion will be applied resulting in a texture in linear space.\n@Note If the texture is not in linear space, it won't look correct in the editor. Another pass will be required either through Composure or other means." },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bIsSRGBInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput = { "bIsSRGBInput", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer_MetaData[] = {
		{ "Category", "Video" },
		{ "ClampMax", "32" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum number of video frames to buffer. */" },
		{ "EditCondition", "bCaptureVideo" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Maximum number of video frames to buffer." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer = { "MaxNumVideoFrameBuffer", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAjaMediaSource, MaxNumVideoFrameBuffer), METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/** Log a warning when there's a drop frame. */" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Log a warning when there's a drop frame." },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bLogDropFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame = { "bLogDropFrame", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/**\n\x09 * Burn Frame Timecode in the input texture without any frame number clipping.\n\x09 * @Note Only supported with progressive format.\n\x09 */" },
		{ "DisplayName", "Burn Frame Timecode" },
		{ "ModuleRelativePath", "Public/AjaMediaSource.h" },
		{ "ToolTip", "Burn Frame Timecode in the input texture without any frame number clipping.\n@Note Only supported with progressive format." },
	};
#endif
	void Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_SetBit(void* Obj)
	{
		((UAjaMediaSource*)Obj)->bEncodeTimecodeInTexel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel = { "bEncodeTimecodeInTexel", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAjaMediaSource), &Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAjaMediaSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MediaConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_TimecodeFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureWithAutoCirculating,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAncillary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAncillaryFrameBuffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureAudio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_AudioChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumAudioFrameBuffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bCaptureVideo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_ColorFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bIsSRGBInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_MaxNumVideoFrameBuffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bLogDropFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAjaMediaSource_Statics::NewProp_bEncodeTimecodeInTexel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAjaMediaSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAjaMediaSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAjaMediaSource_Statics::ClassParams = {
		&UAjaMediaSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAjaMediaSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAjaMediaSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAjaMediaSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAjaMediaSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAjaMediaSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAjaMediaSource, 3880240738);
	template<> AJAMEDIA_API UClass* StaticClass<UAjaMediaSource>()
	{
		return UAjaMediaSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAjaMediaSource(Z_Construct_UClass_UAjaMediaSource, &UAjaMediaSource::StaticClass, TEXT("/Script/AjaMedia"), TEXT("UAjaMediaSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAjaMediaSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
