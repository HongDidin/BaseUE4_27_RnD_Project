// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AJAMEDIA_AjaMediaSource_generated_h
#error "AjaMediaSource.generated.h already included, missing '#pragma once' in AjaMediaSource.h"
#endif
#define AJAMEDIA_AjaMediaSource_generated_h

#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_SPARSE_DATA
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_RPC_WRAPPERS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAjaMediaSource(); \
	friend struct Z_Construct_UClass_UAjaMediaSource_Statics; \
public: \
	DECLARE_CLASS(UAjaMediaSource, UTimeSynchronizableMediaSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMedia"), NO_API) \
	DECLARE_SERIALIZER(UAjaMediaSource)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUAjaMediaSource(); \
	friend struct Z_Construct_UClass_UAjaMediaSource_Statics; \
public: \
	DECLARE_CLASS(UAjaMediaSource, UTimeSynchronizableMediaSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AjaMedia"), NO_API) \
	DECLARE_SERIALIZER(UAjaMediaSource)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAjaMediaSource(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAjaMediaSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaMediaSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaMediaSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaMediaSource(UAjaMediaSource&&); \
	NO_API UAjaMediaSource(const UAjaMediaSource&); \
public:


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAjaMediaSource(UAjaMediaSource&&); \
	NO_API UAjaMediaSource(const UAjaMediaSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAjaMediaSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAjaMediaSource); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAjaMediaSource)


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_34_PROLOG
#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_RPC_WRAPPERS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_INCLASS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_SPARSE_DATA \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AJAMEDIA_API UClass* StaticClass<class UAjaMediaSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_AjaMedia_Source_AjaMedia_Public_AjaMediaSource_h


#define FOREACH_ENUM_EAJAMEDIAAUDIOCHANNEL(op) \
	op(EAjaMediaAudioChannel::Channel6) \
	op(EAjaMediaAudioChannel::Channel8) 

enum class EAjaMediaAudioChannel : uint8;
template<> AJAMEDIA_API UEnum* StaticEnum<EAjaMediaAudioChannel>();

#define FOREACH_ENUM_EAJAMEDIASOURCECOLORFORMAT(op) \
	op(EAjaMediaSourceColorFormat::YUV2_8bit) \
	op(EAjaMediaSourceColorFormat::YUV_10bit) 

enum class EAjaMediaSourceColorFormat : uint8;
template<> AJAMEDIA_API UEnum* StaticEnum<EAjaMediaSourceColorFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
