// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LinearTimecode/Public/LinearTimecodeComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLinearTimecodeComponent() {}
// Cross Module References
	LINEARTIMECODE_API UFunction* Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_LinearTimecode();
	LINEARTIMECODE_API UScriptStruct* Z_Construct_UScriptStruct_FDropTimecode();
	LINEARTIMECODE_API UClass* Z_Construct_UClass_ULinearTimecodeComponent_NoRegister();
	LINEARTIMECODE_API UClass* Z_Construct_UClass_ULinearTimecodeComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaPlayer_NoRegister();
	LINEARTIMECODE_API UClass* Z_Construct_UClass_UDropTimecodeToStringConversion_NoRegister();
	LINEARTIMECODE_API UClass* Z_Construct_UClass_UDropTimecodeToStringConversion();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics
	{
		struct _Script_LinearTimecode_eventOnTimecodeChange_Parms
		{
			FDropTimecode Timecode;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Timecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timecode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::NewProp_Timecode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::NewProp_Timecode = { "Timecode", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_LinearTimecode_eventOnTimecodeChange_Parms, Timecode), Z_Construct_UScriptStruct_FDropTimecode, METADATA_PARAMS(Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::NewProp_Timecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::NewProp_Timecode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::NewProp_Timecode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_LinearTimecode, nullptr, "OnTimecodeChange__DelegateSignature", nullptr, nullptr, sizeof(_Script_LinearTimecode_eventOnTimecodeChange_Parms), Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ULinearTimecodeComponent::execSetDropTimecodeFrameNumber)
	{
		P_GET_STRUCT_REF(FDropTimecode,Z_Param_Out_Timecode);
		P_GET_PROPERTY(FIntProperty,Z_Param_FrameNumber);
		P_GET_STRUCT_REF(FDropTimecode,Z_Param_Out_OutTimecode);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULinearTimecodeComponent::SetDropTimecodeFrameNumber(Z_Param_Out_Timecode,Z_Param_FrameNumber,Z_Param_Out_OutTimecode);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULinearTimecodeComponent::execGetDropTimeCodeFrameNumber)
	{
		P_GET_STRUCT_REF(FDropTimecode,Z_Param_Out_Timecode);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_FrameNumber);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULinearTimecodeComponent::GetDropTimeCodeFrameNumber(Z_Param_Out_Timecode,Z_Param_Out_FrameNumber);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULinearTimecodeComponent::execGetDropFrameNumber)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetDropFrameNumber();
		P_NATIVE_END;
	}
	void ULinearTimecodeComponent::StaticRegisterNativesULinearTimecodeComponent()
	{
		UClass* Class = ULinearTimecodeComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDropFrameNumber", &ULinearTimecodeComponent::execGetDropFrameNumber },
			{ "GetDropTimeCodeFrameNumber", &ULinearTimecodeComponent::execGetDropTimeCodeFrameNumber },
			{ "SetDropTimecodeFrameNumber", &ULinearTimecodeComponent::execSetDropTimecodeFrameNumber },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics
	{
		struct LinearTimecodeComponent_eventGetDropFrameNumber_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LinearTimecodeComponent_eventGetDropFrameNumber_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Get the Frame Number\n\x09 * @return Frame Number\n\x09 */" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Get the Frame Number\n@return Frame Number" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULinearTimecodeComponent, nullptr, "GetDropFrameNumber", nullptr, nullptr, sizeof(LinearTimecodeComponent_eventGetDropFrameNumber_Parms), Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics
	{
		struct LinearTimecodeComponent_eventGetDropTimeCodeFrameNumber_Parms
		{
			FDropTimecode Timecode;
			int32 FrameNumber;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Timecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timecode;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameNumber;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_Timecode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_Timecode = { "Timecode", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LinearTimecodeComponent_eventGetDropTimeCodeFrameNumber_Parms, Timecode), Z_Construct_UScriptStruct_FDropTimecode, METADATA_PARAMS(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_Timecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_Timecode_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_FrameNumber = { "FrameNumber", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LinearTimecodeComponent_eventGetDropTimeCodeFrameNumber_Parms, FrameNumber), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_Timecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::NewProp_FrameNumber,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Convert a drop timecode into a frame number\n\x09 * @param Timecode - Used to access the framerate, and drop flag\n\x09 * @param FrameNumber - returned calculated frame number\n\x09 */" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Convert a drop timecode into a frame number\n@param Timecode - Used to access the framerate, and drop flag\n@param FrameNumber - returned calculated frame number" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULinearTimecodeComponent, nullptr, "GetDropTimeCodeFrameNumber", nullptr, nullptr, sizeof(LinearTimecodeComponent_eventGetDropTimeCodeFrameNumber_Parms), Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics
	{
		struct LinearTimecodeComponent_eventSetDropTimecodeFrameNumber_Parms
		{
			FDropTimecode Timecode;
			int32 FrameNumber;
			FDropTimecode OutTimecode;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Timecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timecode;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameNumber;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutTimecode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_Timecode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_Timecode = { "Timecode", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LinearTimecodeComponent_eventSetDropTimecodeFrameNumber_Parms, Timecode), Z_Construct_UScriptStruct_FDropTimecode, METADATA_PARAMS(Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_Timecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_Timecode_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_FrameNumber = { "FrameNumber", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LinearTimecodeComponent_eventSetDropTimecodeFrameNumber_Parms, FrameNumber), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_OutTimecode = { "OutTimecode", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LinearTimecodeComponent_eventSetDropTimecodeFrameNumber_Parms, OutTimecode), Z_Construct_UScriptStruct_FDropTimecode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_Timecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_FrameNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::NewProp_OutTimecode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Convert frame number into a drop timecode\n\x09 * @param Timecode - used to access the framerate, and drop flag\n\x09 * @param FrameNumber - Frame number to set in the returned timecode\n\x09 * @param OutTimecode - Returned drop timecode\n\x09 */" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Convert frame number into a drop timecode\n@param Timecode - used to access the framerate, and drop flag\n@param FrameNumber - Frame number to set in the returned timecode\n@param OutTimecode - Returned drop timecode" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULinearTimecodeComponent, nullptr, "SetDropTimecodeFrameNumber", nullptr, nullptr, sizeof(LinearTimecodeComponent_eventSetDropTimecodeFrameNumber_Parms), Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULinearTimecodeComponent_NoRegister()
	{
		return ULinearTimecodeComponent::StaticClass();
	}
	struct Z_Construct_UClass_ULinearTimecodeComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaPlayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaPlayer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DropTimecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DropTimecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnTimecodeChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTimecodeChange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULinearTimecodeComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_LinearTimecode,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULinearTimecodeComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULinearTimecodeComponent_GetDropFrameNumber, "GetDropFrameNumber" }, // 3802594707
		{ &Z_Construct_UFunction_ULinearTimecodeComponent_GetDropTimeCodeFrameNumber, "GetDropTimeCodeFrameNumber" }, // 2337967511
		{ &Z_Construct_UFunction_ULinearTimecodeComponent_SetDropTimecodeFrameNumber, "SetDropTimecodeFrameNumber" }, // 2311767539
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULinearTimecodeComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Media" },
		{ "Comment", "/**\n * Implements a Linear timecode decoder.\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "LinearTimecodeComponent.h" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Implements a Linear timecode decoder." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_MediaPlayer_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** The media player asset associated with this component. */" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "The media player asset associated with this component." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_MediaPlayer = { "MediaPlayer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULinearTimecodeComponent, MediaPlayer), Z_Construct_UClass_UMediaPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_MediaPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_MediaPlayer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_DropTimecode_MetaData[] = {
		{ "Category", "Media" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_DropTimecode = { "DropTimecode", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULinearTimecodeComponent, DropTimecode), Z_Construct_UScriptStruct_FDropTimecode, METADATA_PARAMS(Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_DropTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_DropTimecode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_OnTimecodeChange_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Called when the timecode changes */" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Called when the timecode changes" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_OnTimecodeChange = { "OnTimecodeChange", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULinearTimecodeComponent, OnTimecodeChange), Z_Construct_UDelegateFunction_LinearTimecode_OnTimecodeChange__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_OnTimecodeChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_OnTimecodeChange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULinearTimecodeComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_MediaPlayer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_DropTimecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULinearTimecodeComponent_Statics::NewProp_OnTimecodeChange,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULinearTimecodeComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULinearTimecodeComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULinearTimecodeComponent_Statics::ClassParams = {
		&ULinearTimecodeComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULinearTimecodeComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULinearTimecodeComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULinearTimecodeComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULinearTimecodeComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULinearTimecodeComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULinearTimecodeComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULinearTimecodeComponent, 3578651466);
	template<> LINEARTIMECODE_API UClass* StaticClass<ULinearTimecodeComponent>()
	{
		return ULinearTimecodeComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULinearTimecodeComponent(Z_Construct_UClass_ULinearTimecodeComponent, &ULinearTimecodeComponent::StaticClass, TEXT("/Script/LinearTimecode"), TEXT("ULinearTimecodeComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULinearTimecodeComponent);
	DEFINE_FUNCTION(UDropTimecodeToStringConversion::execConv_DropTimecodeToString)
	{
		P_GET_STRUCT_REF(FDropTimecode,Z_Param_Out_InTimecode);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UDropTimecodeToStringConversion::Conv_DropTimecodeToString(Z_Param_Out_InTimecode);
		P_NATIVE_END;
	}
	void UDropTimecodeToStringConversion::StaticRegisterNativesUDropTimecodeToStringConversion()
	{
		UClass* Class = UDropTimecodeToStringConversion::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Conv_DropTimecodeToString", &UDropTimecodeToStringConversion::execConv_DropTimecodeToString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics
	{
		struct DropTimecodeToStringConversion_eventConv_DropTimecodeToString_Parms
		{
			FDropTimecode InTimecode;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTimecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InTimecode;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_InTimecode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_InTimecode = { "InTimecode", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DropTimecodeToStringConversion_eventConv_DropTimecodeToString_Parms, InTimecode), Z_Construct_UScriptStruct_FDropTimecode, METADATA_PARAMS(Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_InTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_InTimecode_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DropTimecodeToStringConversion_eventConv_DropTimecodeToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_InTimecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|String" },
		{ "Comment", "/** Convert a timecode structure into a string */" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToString (DropTimecode)" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Convert a timecode structure into a string" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDropTimecodeToStringConversion, nullptr, "Conv_DropTimecodeToString", nullptr, nullptr, sizeof(DropTimecodeToStringConversion_eventConv_DropTimecodeToString_Parms), Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDropTimecodeToStringConversion_NoRegister()
	{
		return UDropTimecodeToStringConversion::StaticClass();
	}
	struct Z_Construct_UClass_UDropTimecodeToStringConversion_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LinearTimecode,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDropTimecodeToStringConversion_Conv_DropTimecodeToString, "Conv_DropTimecodeToString" }, // 883574829
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::Class_MetaDataParams[] = {
		{ "BlueprintThreadSafe", "" },
		{ "Comment", "/**\n * Extend type conversions to handle FDropTimecode structure\n */" },
		{ "IncludePath", "LinearTimecodeComponent.h" },
		{ "ModuleRelativePath", "Public/LinearTimecodeComponent.h" },
		{ "ToolTip", "Extend type conversions to handle FDropTimecode structure" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDropTimecodeToStringConversion>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::ClassParams = {
		&UDropTimecodeToStringConversion::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDropTimecodeToStringConversion()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDropTimecodeToStringConversion_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDropTimecodeToStringConversion, 2674055401);
	template<> LINEARTIMECODE_API UClass* StaticClass<UDropTimecodeToStringConversion>()
	{
		return UDropTimecodeToStringConversion::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDropTimecodeToStringConversion(Z_Construct_UClass_UDropTimecodeToStringConversion, &UDropTimecodeToStringConversion::StaticClass, TEXT("/Script/LinearTimecode"), TEXT("UDropTimecodeToStringConversion"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDropTimecodeToStringConversion);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
