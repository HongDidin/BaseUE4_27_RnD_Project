// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LINEARTIMECODE_DropTimecode_generated_h
#error "DropTimecode.generated.h already included, missing '#pragma once' in DropTimecode.h"
#endif
#define LINEARTIMECODE_DropTimecode_generated_h

#define Engine_Plugins_Media_LinearTimecode_Source_LinearTimecode_Public_DropTimecode_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDropTimecode_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LINEARTIMECODE_API UScriptStruct* StaticStruct<struct FDropTimecode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_LinearTimecode_Source_LinearTimecode_Public_DropTimecode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
