// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LinearTimecode/Public/DropTimecode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDropTimecode() {}
// Cross Module References
	LINEARTIMECODE_API UScriptStruct* Z_Construct_UScriptStruct_FDropTimecode();
	UPackage* Z_Construct_UPackage__Script_LinearTimecode();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimecode();
// End Cross Module References
class UScriptStruct* FDropTimecode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LINEARTIMECODE_API uint32 Get_Z_Construct_UScriptStruct_FDropTimecode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDropTimecode, Z_Construct_UPackage__Script_LinearTimecode(), TEXT("DropTimecode"), sizeof(FDropTimecode), Get_Z_Construct_UScriptStruct_FDropTimecode_Hash());
	}
	return Singleton;
}
template<> LINEARTIMECODE_API UScriptStruct* StaticStruct<FDropTimecode>()
{
	return FDropTimecode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDropTimecode(FDropTimecode::StaticStruct, TEXT("/Script/LinearTimecode"), TEXT("DropTimecode"), false, nullptr, nullptr);
static struct FScriptStruct_LinearTimecode_StaticRegisterNativesFDropTimecode
{
	FScriptStruct_LinearTimecode_StaticRegisterNativesFDropTimecode()
	{
		UScriptStruct::DeferCppStructOps<FDropTimecode>(FName(TEXT("DropTimecode")));
	}
} ScriptStruct_LinearTimecode_StaticRegisterNativesFDropTimecode;
	struct Z_Construct_UScriptStruct_FDropTimecode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Timecode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bColorFraming_MetaData[];
#endif
		static void NewProp_bColorFraming_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bColorFraming;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRunningForward_MetaData[];
#endif
		static void NewProp_bRunningForward_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRunningForward;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNewFrame_MetaData[];
#endif
		static void NewProp_bNewFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDropTimecode_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Hold a frame of a Linear Timecode Frame */" },
		{ "ModuleRelativePath", "Public/DropTimecode.h" },
		{ "ToolTip", "Hold a frame of a Linear Timecode Frame" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDropTimecode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDropTimecode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_Timecode_MetaData[] = {
		{ "Category", "Time" },
		{ "Comment", "/** Decoded Timecode */" },
		{ "ModuleRelativePath", "Public/DropTimecode.h" },
		{ "ToolTip", "Decoded Timecode" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_Timecode = { "Timecode", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDropTimecode, Timecode), Z_Construct_UScriptStruct_FTimecode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_Timecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_Timecode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_FrameRate_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Guess at incoming frame rate */" },
		{ "ModuleRelativePath", "Public/DropTimecode.h" },
		{ "ToolTip", "Guess at incoming frame rate" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_FrameRate = { "FrameRate", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDropTimecode, FrameRate), METADATA_PARAMS(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_FrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_FrameRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming_MetaData[] = {
		{ "Category", "Time" },
		{ "Comment", "/** Sync is in phase with color burst */" },
		{ "ModuleRelativePath", "Public/DropTimecode.h" },
		{ "ToolTip", "Sync is in phase with color burst" },
	};
#endif
	void Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming_SetBit(void* Obj)
	{
		((FDropTimecode*)Obj)->bColorFraming = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming = { "bColorFraming", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDropTimecode), &Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward_MetaData[] = {
		{ "Category", "Time" },
		{ "Comment", "/** When timecode is reading forward */" },
		{ "ModuleRelativePath", "Public/DropTimecode.h" },
		{ "ToolTip", "When timecode is reading forward" },
	};
#endif
	void Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward_SetBit(void* Obj)
	{
		((FDropTimecode*)Obj)->bRunningForward = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward = { "bRunningForward", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDropTimecode), &Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame_MetaData[] = {
		{ "Category", "Time" },
		{ "Comment", "/** Is a new timecode frame */" },
		{ "ModuleRelativePath", "Public/DropTimecode.h" },
		{ "ToolTip", "Is a new timecode frame" },
	};
#endif
	void Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame_SetBit(void* Obj)
	{
		((FDropTimecode*)Obj)->bNewFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame = { "bNewFrame", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDropTimecode), &Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDropTimecode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_Timecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_FrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bColorFraming,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bRunningForward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDropTimecode_Statics::NewProp_bNewFrame,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDropTimecode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LinearTimecode,
		nullptr,
		&NewStructOps,
		"DropTimecode",
		sizeof(FDropTimecode),
		alignof(FDropTimecode),
		Z_Construct_UScriptStruct_FDropTimecode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDropTimecode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDropTimecode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDropTimecode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDropTimecode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LinearTimecode();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DropTimecode"), sizeof(FDropTimecode), Get_Z_Construct_UScriptStruct_FDropTimecode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDropTimecode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDropTimecode_Hash() { return 2399755546U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
