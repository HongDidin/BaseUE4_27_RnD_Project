// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaIOCore/Public/FileMediaOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFileMediaOutput() {}
// Cross Module References
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EFileMediaOutputPixelFormat();
	UPackage* Z_Construct_UPackage__Script_MediaIOCore();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UFileMediaOutput_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UFileMediaOutput();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput();
	IMAGEWRITEQUEUE_API UScriptStruct* Z_Construct_UScriptStruct_FImageWriteOptions();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
// End Cross Module References
	static UEnum* EFileMediaOutputPixelFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EFileMediaOutputPixelFormat, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EFileMediaOutputPixelFormat"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EFileMediaOutputPixelFormat>()
	{
		return EFileMediaOutputPixelFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFileMediaOutputPixelFormat(EFileMediaOutputPixelFormat_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EFileMediaOutputPixelFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EFileMediaOutputPixelFormat_Hash() { return 1591954913U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EFileMediaOutputPixelFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFileMediaOutputPixelFormat"), 0, Get_Z_Construct_UEnum_MediaIOCore_EFileMediaOutputPixelFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFileMediaOutputPixelFormat::B8G8R8A8", (int64)EFileMediaOutputPixelFormat::B8G8R8A8 },
				{ "EFileMediaOutputPixelFormat::FloatRGBA", (int64)EFileMediaOutputPixelFormat::FloatRGBA },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "B8G8R8A8.DisplayName", "8bit RGBA" },
				{ "B8G8R8A8.Name", "EFileMediaOutputPixelFormat::B8G8R8A8" },
				{ "Comment", "/** Texture format supported by UFileMediaOutput. */" },
				{ "FloatRGBA.DisplayName", "Float RGBA" },
				{ "FloatRGBA.Name", "EFileMediaOutputPixelFormat::FloatRGBA" },
				{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
				{ "ToolTip", "Texture format supported by UFileMediaOutput." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EFileMediaOutputPixelFormat",
				"EFileMediaOutputPixelFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UFileMediaOutput::StaticRegisterNativesUFileMediaOutput()
	{
	}
	UClass* Z_Construct_UClass_UFileMediaOutput_NoRegister()
	{
		return UFileMediaOutput::StaticClass();
	}
	struct Z_Construct_UClass_UFileMediaOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WriteOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WriteOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BaseFileName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideDesiredSize_MetaData[];
#endif
		static void NewProp_bOverrideDesiredSize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideDesiredSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesiredSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DesiredSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverridePixelFormat_MetaData[];
#endif
		static void NewProp_bOverridePixelFormat_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverridePixelFormat;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DesiredPixelFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesiredPixelFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DesiredPixelFormat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFileMediaOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Output information for a file media capture.\n * @note\x09'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha to enabled the Key.\n * @note\x09'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper' to enabled the Key.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "FileMediaOutput.h" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "ToolTip", "Output information for a file media capture.\n@note       'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha to enabled the Key.\n@note       'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper' to enabled the Key." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_WriteOptions_MetaData[] = {
		{ "Category", "File" },
		{ "Comment", "/** Options on how to save the images. */" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "ToolTip", "Options on how to save the images." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_WriteOptions = { "WriteOptions", nullptr, (EPropertyFlags)0x0010008000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFileMediaOutput, WriteOptions), Z_Construct_UScriptStruct_FImageWriteOptions, METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_WriteOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_WriteOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_FilePath_MetaData[] = {
		{ "Category", "File" },
		{ "Comment", "/** The file path for the images. */" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "RelativePath", "" },
		{ "ToolTip", "The file path for the images." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFileMediaOutput, FilePath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_FilePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_BaseFileName_MetaData[] = {
		{ "Category", "File" },
		{ "Comment", "/** The base file name of the images. The frame number will be append to the base file name. */" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "RelativePath", "" },
		{ "ToolTip", "The base file name of the images. The frame number will be append to the base file name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_BaseFileName = { "BaseFileName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFileMediaOutput, BaseFileName), METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_BaseFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_BaseFileName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Use the default back buffer size or specify a specific size to capture. */" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "ToolTip", "Use the default back buffer size or specify a specific size to capture." },
	};
#endif
	void Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize_SetBit(void* Obj)
	{
		((UFileMediaOutput*)Obj)->bOverrideDesiredSize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize = { "bOverrideDesiredSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFileMediaOutput), &Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredSize_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Use the default back buffer size or specify a specific size to capture. */" },
		{ "EditCondition", "bOverrideDesiredSize" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "ToolTip", "Use the default back buffer size or specify a specific size to capture." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredSize = { "DesiredSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFileMediaOutput, DesiredSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Use the default back buffer pixel format or specify a specific the pixel format to capture. */" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "ToolTip", "Use the default back buffer pixel format or specify a specific the pixel format to capture." },
	};
#endif
	void Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat_SetBit(void* Obj)
	{
		((UFileMediaOutput*)Obj)->bOverridePixelFormat = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat = { "bOverridePixelFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFileMediaOutput), &Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat_MetaData[] = {
		{ "Category", "Media" },
		{ "Comment", "/** Use the default back buffer pixel format or specify a specific the pixel format to capture. */" },
		{ "EditCondition", "bOverridePixelFormat" },
		{ "ModuleRelativePath", "Public/FileMediaOutput.h" },
		{ "ToolTip", "Use the default back buffer pixel format or specify a specific the pixel format to capture." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat = { "DesiredPixelFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFileMediaOutput, DesiredPixelFormat), Z_Construct_UEnum_MediaIOCore_EFileMediaOutputPixelFormat, METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFileMediaOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_WriteOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_FilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_BaseFileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverrideDesiredSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_bOverridePixelFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFileMediaOutput_Statics::NewProp_DesiredPixelFormat,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFileMediaOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFileMediaOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFileMediaOutput_Statics::ClassParams = {
		&UFileMediaOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFileMediaOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFileMediaOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFileMediaOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFileMediaOutput, 2239353834);
	template<> MEDIAIOCORE_API UClass* StaticClass<UFileMediaOutput>()
	{
		return UFileMediaOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFileMediaOutput(Z_Construct_UClass_UFileMediaOutput, &UFileMediaOutput::StaticClass, TEXT("/Script/MediaIOCore"), TEXT("UFileMediaOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFileMediaOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
