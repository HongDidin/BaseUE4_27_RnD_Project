// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaIOCore/Public/MediaIOCoreDefinitions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaIOCoreDefinitions() {}
// Cross Module References
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOReferenceType();
	UPackage* Z_Construct_UPackage__Script_MediaIOCore();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOOutputType();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOInputType();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOStandardType();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOQuadLinkTransportType();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTransportType();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOOutputConfiguration();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConfiguration();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOInputConfiguration();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConnection();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIOMode();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaIODevice();
// End Cross Module References
	static UEnum* EMediaIOReferenceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOReferenceType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOReferenceType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOReferenceType>()
	{
		return EMediaIOReferenceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOReferenceType(EMediaIOReferenceType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOReferenceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOReferenceType_Hash() { return 3481045048U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOReferenceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOReferenceType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOReferenceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOReferenceType::FreeRun", (int64)EMediaIOReferenceType::FreeRun },
				{ "EMediaIOReferenceType::External", (int64)EMediaIOReferenceType::External },
				{ "EMediaIOReferenceType::Input", (int64)EMediaIOReferenceType::Input },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * SDI Output type.\n */" },
				{ "External.Name", "EMediaIOReferenceType::External" },
				{ "FreeRun.Name", "EMediaIOReferenceType::FreeRun" },
				{ "Input.Name", "EMediaIOReferenceType::Input" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "ToolTip", "SDI Output type." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOReferenceType",
				"EMediaIOReferenceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaIOOutputType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOOutputType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOOutputType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOOutputType>()
	{
		return EMediaIOOutputType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOOutputType(EMediaIOOutputType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOOutputType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOOutputType_Hash() { return 935448805U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOOutputType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOOutputType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOOutputType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOOutputType::Fill", (int64)EMediaIOOutputType::Fill },
				{ "EMediaIOOutputType::FillAndKey", (int64)EMediaIOOutputType::FillAndKey },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * SDI Output type.\n */" },
				{ "Fill.DisplayName", "Fill" },
				{ "Fill.Name", "EMediaIOOutputType::Fill" },
				{ "FillAndKey.DisplayName", "Fill & Key" },
				{ "FillAndKey.Name", "EMediaIOOutputType::FillAndKey" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "ToolTip", "SDI Output type." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOOutputType",
				"EMediaIOOutputType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaIOInputType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOInputType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOInputType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOInputType>()
	{
		return EMediaIOInputType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOInputType(EMediaIOInputType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOInputType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOInputType_Hash() { return 104854644U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOInputType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOInputType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOInputType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOInputType::Fill", (int64)EMediaIOInputType::Fill },
				{ "EMediaIOInputType::FillAndKey", (int64)EMediaIOInputType::FillAndKey },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * SDI Input type.\n */" },
				{ "Fill.DisplayName", "Fill" },
				{ "Fill.Name", "EMediaIOInputType::Fill" },
				{ "FillAndKey.DisplayName", "Fill & Key" },
				{ "FillAndKey.Name", "EMediaIOInputType::FillAndKey" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "ToolTip", "SDI Input type." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOInputType",
				"EMediaIOInputType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaIOTimecodeFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOTimecodeFormat"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOTimecodeFormat>()
	{
		return EMediaIOTimecodeFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOTimecodeFormat(EMediaIOTimecodeFormat_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOTimecodeFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat_Hash() { return 1343546299U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOTimecodeFormat"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOTimecodeFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOTimecodeFormat::None", (int64)EMediaIOTimecodeFormat::None },
				{ "EMediaIOTimecodeFormat::LTC", (int64)EMediaIOTimecodeFormat::LTC },
				{ "EMediaIOTimecodeFormat::VITC", (int64)EMediaIOTimecodeFormat::VITC },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Timecode formats.\n */" },
				{ "LTC.Name", "EMediaIOTimecodeFormat::LTC" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "None.Name", "EMediaIOTimecodeFormat::None" },
				{ "ToolTip", "Timecode formats." },
				{ "VITC.Name", "EMediaIOTimecodeFormat::VITC" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOTimecodeFormat",
				"EMediaIOTimecodeFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaIOStandardType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOStandardType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOStandardType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOStandardType>()
	{
		return EMediaIOStandardType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOStandardType(EMediaIOStandardType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOStandardType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOStandardType_Hash() { return 2346200105U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOStandardType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOStandardType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOStandardType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOStandardType::Progressive", (int64)EMediaIOStandardType::Progressive },
				{ "EMediaIOStandardType::Interlaced", (int64)EMediaIOStandardType::Interlaced },
				{ "EMediaIOStandardType::ProgressiveSegmentedFrame", (int64)EMediaIOStandardType::ProgressiveSegmentedFrame },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * SDI transport type.\n */" },
				{ "Interlaced.Name", "EMediaIOStandardType::Interlaced" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "Progressive.Name", "EMediaIOStandardType::Progressive" },
				{ "ProgressiveSegmentedFrame.Name", "EMediaIOStandardType::ProgressiveSegmentedFrame" },
				{ "ToolTip", "SDI transport type." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOStandardType",
				"EMediaIOStandardType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaIOQuadLinkTransportType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOQuadLinkTransportType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOQuadLinkTransportType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOQuadLinkTransportType>()
	{
		return EMediaIOQuadLinkTransportType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOQuadLinkTransportType(EMediaIOQuadLinkTransportType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOQuadLinkTransportType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOQuadLinkTransportType_Hash() { return 1115326829U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOQuadLinkTransportType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOQuadLinkTransportType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOQuadLinkTransportType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOQuadLinkTransportType::SquareDivision", (int64)EMediaIOQuadLinkTransportType::SquareDivision },
				{ "EMediaIOQuadLinkTransportType::TwoSampleInterleave", (int64)EMediaIOQuadLinkTransportType::TwoSampleInterleave },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Quad link transport type.\n */" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "SquareDivision.Name", "EMediaIOQuadLinkTransportType::SquareDivision" },
				{ "ToolTip", "Quad link transport type." },
				{ "TwoSampleInterleave.Name", "EMediaIOQuadLinkTransportType::TwoSampleInterleave" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOQuadLinkTransportType",
				"EMediaIOQuadLinkTransportType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaIOTransportType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaIOTransportType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaIOTransportType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOTransportType>()
	{
		return EMediaIOTransportType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaIOTransportType(EMediaIOTransportType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaIOTransportType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaIOTransportType_Hash() { return 85122854U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaIOTransportType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaIOTransportType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaIOTransportType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaIOTransportType::SingleLink", (int64)EMediaIOTransportType::SingleLink },
				{ "EMediaIOTransportType::DualLink", (int64)EMediaIOTransportType::DualLink },
				{ "EMediaIOTransportType::QuadLink", (int64)EMediaIOTransportType::QuadLink },
				{ "EMediaIOTransportType::HDMI", (int64)EMediaIOTransportType::HDMI },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Media transport type.\n */" },
				{ "DualLink.Name", "EMediaIOTransportType::DualLink" },
				{ "HDMI.Name", "EMediaIOTransportType::HDMI" },
				{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
				{ "QuadLink.Name", "EMediaIOTransportType::QuadLink" },
				{ "SingleLink.Name", "EMediaIOTransportType::SingleLink" },
				{ "ToolTip", "Media transport type." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaIOTransportType",
				"EMediaIOTransportType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMediaIOOutputConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaIOOutputConfiguration"), sizeof(FMediaIOOutputConfiguration), Get_Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaIOOutputConfiguration>()
{
	return FMediaIOOutputConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaIOOutputConfiguration(FMediaIOOutputConfiguration::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaIOOutputConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOOutputConfiguration
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOOutputConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FMediaIOOutputConfiguration>(FName(TEXT("MediaIOOutputConfiguration")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOOutputConfiguration;
	struct Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConfiguration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_OutputType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OutputType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyPortIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_KeyPortIdentifier;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_OutputReference_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputReference_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OutputReference;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferencePortIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReferencePortIdentifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Configuration of a device output.\n */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Configuration of a device output." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaIOOutputConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_MediaConfiguration_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The signal output format. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The signal output format." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_MediaConfiguration = { "MediaConfiguration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOOutputConfiguration, MediaConfiguration), Z_Construct_UScriptStruct_FMediaIOConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_MediaConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_MediaConfiguration_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Whether to output the fill or the fill and key. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Whether to output the fill or the fill and key." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType = { "OutputType", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOOutputConfiguration, OutputType), Z_Construct_UEnum_MediaIOCore_EMediaIOOutputType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_KeyPortIdentifier_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/**\n\x09 * The port of the video channel on the device to output the key to.\n\x09 * @note\x09'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha.\n\x09 * @note\x09'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper'.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The port of the video channel on the device to output the key to.\n@note        'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha.\n@note        'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper'." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_KeyPortIdentifier = { "KeyPortIdentifier", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOOutputConfiguration, KeyPortIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_KeyPortIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_KeyPortIdentifier_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The Device output sync with either its internal clock, an external reference, or an other input. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The Device output sync with either its internal clock, an external reference, or an other input." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference = { "OutputReference", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOOutputConfiguration, OutputReference), Z_Construct_UEnum_MediaIOCore_EMediaIOReferenceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_ReferencePortIdentifier_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The port of the video channel on the device to output the synchronize to. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The port of the video channel on the device to output the synchronize to." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_ReferencePortIdentifier = { "ReferencePortIdentifier", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOOutputConfiguration, ReferencePortIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_ReferencePortIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_ReferencePortIdentifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_MediaConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_KeyPortIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_OutputReference,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::NewProp_ReferencePortIdentifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaIOOutputConfiguration",
		sizeof(FMediaIOOutputConfiguration),
		alignof(FMediaIOOutputConfiguration),
		Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaIOOutputConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaIOOutputConfiguration"), sizeof(FMediaIOOutputConfiguration), Get_Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Hash() { return 739635728U; }
class UScriptStruct* FMediaIOInputConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaIOInputConfiguration, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaIOInputConfiguration"), sizeof(FMediaIOInputConfiguration), Get_Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaIOInputConfiguration>()
{
	return FMediaIOInputConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaIOInputConfiguration(FMediaIOInputConfiguration::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaIOInputConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOInputConfiguration
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOInputConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FMediaIOInputConfiguration>(FName(TEXT("MediaIOInputConfiguration")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOInputConfiguration;
	struct Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConfiguration;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_InputType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InputType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyPortIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_KeyPortIdentifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Configuration of a device input.\n */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Configuration of a device input." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaIOInputConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_MediaConfiguration_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The signal input format. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The signal input format." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_MediaConfiguration = { "MediaConfiguration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOInputConfiguration, MediaConfiguration), Z_Construct_UScriptStruct_FMediaIOConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_MediaConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_MediaConfiguration_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Whether to input the fill or the fill and key. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Whether to input the fill or the fill and key." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType = { "InputType", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOInputConfiguration, InputType), Z_Construct_UEnum_MediaIOCore_EMediaIOInputType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_KeyPortIdentifier_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The port of the video channel on the device to input the key from. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The port of the video channel on the device to input the key from." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_KeyPortIdentifier = { "KeyPortIdentifier", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOInputConfiguration, KeyPortIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_KeyPortIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_KeyPortIdentifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_MediaConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_InputType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::NewProp_KeyPortIdentifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaIOInputConfiguration",
		sizeof(FMediaIOInputConfiguration),
		alignof(FMediaIOInputConfiguration),
		Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaIOInputConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaIOInputConfiguration"), sizeof(FMediaIOInputConfiguration), Get_Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Hash() { return 2718994958U; }
class UScriptStruct* FMediaIOConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaIOConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaIOConfiguration, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaIOConfiguration"), sizeof(FMediaIOConfiguration), Get_Z_Construct_UScriptStruct_FMediaIOConfiguration_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaIOConfiguration>()
{
	return FMediaIOConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaIOConfiguration(FMediaIOConfiguration::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaIOConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOConfiguration
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FMediaIOConfiguration>(FName(TEXT("MediaIOConfiguration")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOConfiguration;
	struct Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsInput_MetaData[];
#endif
		static void NewProp_bIsInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaConnection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaConnection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Configuration of a device input / output.\n */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Configuration of a device input / output." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaIOConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput_MetaData[] = {
		{ "Comment", "/** Configured as an input or output. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Configured as an input or output." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput_SetBit(void* Obj)
	{
		((FMediaIOConfiguration*)Obj)->bIsInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput = { "bIsInput", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaIOConfiguration), &Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaConnection_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The configuration's device and transport type. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The configuration's device and transport type." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaConnection = { "MediaConnection", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConfiguration, MediaConnection), Z_Construct_UScriptStruct_FMediaIOConnection, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaConnection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaConnection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaMode_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** The configuration's video mode. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The configuration's video mode." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaMode = { "MediaMode", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConfiguration, MediaMode), Z_Construct_UScriptStruct_FMediaIOMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_bIsInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaConnection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::NewProp_MediaMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaIOConfiguration",
		sizeof(FMediaIOConfiguration),
		alignof(FMediaIOConfiguration),
		Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaIOConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaIOConfiguration"), sizeof(FMediaIOConfiguration), Get_Z_Construct_UScriptStruct_FMediaIOConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaIOConfiguration_Hash() { return 4126347699U; }
class UScriptStruct* FMediaIOMode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaIOMode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaIOMode, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaIOMode"), sizeof(FMediaIOMode), Get_Z_Construct_UScriptStruct_FMediaIOMode_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaIOMode>()
{
	return FMediaIOMode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaIOMode(FMediaIOMode::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaIOMode"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOMode
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOMode()
	{
		UScriptStruct::DeferCppStructOps<FMediaIOMode>(FName(TEXT("MediaIOMode")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOMode;
	struct Z_Construct_UScriptStruct_FMediaIOMode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrameRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Resolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Resolution;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Standard_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Standard_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Standard;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceModeIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceModeIdentifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOMode_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Identifies a media mode.\n */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Identifies a media mode." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaIOMode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_FrameRate_MetaData[] = {
		{ "Category", "Mode" },
		{ "Comment", "/** The mode's frame rate. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The mode's frame rate." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_FrameRate = { "FrameRate", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOMode, FrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_FrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_FrameRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Resolution_MetaData[] = {
		{ "Category", "Mode" },
		{ "Comment", "/** The mode's image resolution. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The mode's image resolution." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Resolution = { "Resolution", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOMode, Resolution), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Resolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Resolution_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard_MetaData[] = {
		{ "Category", "Mode" },
		{ "Comment", "/** The mode's scanning type. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The mode's scanning type." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard = { "Standard", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOMode, Standard), Z_Construct_UEnum_MediaIOCore_EMediaIOStandardType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_DeviceModeIdentifier_MetaData[] = {
		{ "Category", "Mode" },
		{ "Comment", "/** The mode's identifier for the device. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The mode's identifier for the device." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_DeviceModeIdentifier = { "DeviceModeIdentifier", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOMode, DeviceModeIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_DeviceModeIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_DeviceModeIdentifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaIOMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_FrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Resolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_Standard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOMode_Statics::NewProp_DeviceModeIdentifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaIOMode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaIOMode",
		sizeof(FMediaIOMode),
		alignof(FMediaIOMode),
		Z_Construct_UScriptStruct_FMediaIOMode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOMode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOMode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOMode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaIOMode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaIOMode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaIOMode"), sizeof(FMediaIOMode), Get_Z_Construct_UScriptStruct_FMediaIOMode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaIOMode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaIOMode_Hash() { return 1141907424U; }
class UScriptStruct* FMediaIOConnection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaIOConnection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaIOConnection, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaIOConnection"), sizeof(FMediaIOConnection), Get_Z_Construct_UScriptStruct_FMediaIOConnection_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaIOConnection>()
{
	return FMediaIOConnection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaIOConnection(FMediaIOConnection::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaIOConnection"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOConnection
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOConnection()
	{
		UScriptStruct::DeferCppStructOps<FMediaIOConnection>(FName(TEXT("MediaIOConnection")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIOConnection;
	struct Z_Construct_UScriptStruct_FMediaIOConnection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Device_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Device;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Protocol_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Protocol;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TransportType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransportType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransportType;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_QuadTransportType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QuadTransportType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_QuadTransportType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PortIdentifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConnection_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Identifies an media connection.\n */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Identifies an media connection." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaIOConnection>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Device_MetaData[] = {
		{ "Category", "Connection" },
		{ "Comment", "/** The device identifier. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The device identifier." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Device = { "Device", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConnection, Device), Z_Construct_UScriptStruct_FMediaIODevice, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Device_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Device_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Protocol_MetaData[] = {
		{ "Category", "Connection" },
		{ "Comment", "/** The protocol used by the MediaFramework. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The protocol used by the MediaFramework." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Protocol = { "Protocol", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConnection, Protocol), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Protocol_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Protocol_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType_MetaData[] = {
		{ "Category", "Connection" },
		{ "Comment", "/** The type of cable link used for that configuration */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The type of cable link used for that configuration" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType = { "TransportType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConnection, TransportType), Z_Construct_UEnum_MediaIOCore_EMediaIOTransportType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType_MetaData[] = {
		{ "Category", "Connection" },
		{ "Comment", "/** The type of cable link used for that configuration */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The type of cable link used for that configuration" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType = { "QuadTransportType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConnection, QuadTransportType), Z_Construct_UEnum_MediaIOCore_EMediaIOQuadLinkTransportType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_PortIdentifier_MetaData[] = {
		{ "Category", "Connection" },
		{ "Comment", "/** The port of the video channel on the device. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The port of the video channel on the device." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_PortIdentifier = { "PortIdentifier", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIOConnection, PortIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_PortIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_PortIdentifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaIOConnection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Device,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_Protocol,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_TransportType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_QuadTransportType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIOConnection_Statics::NewProp_PortIdentifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaIOConnection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaIOConnection",
		sizeof(FMediaIOConnection),
		alignof(FMediaIOConnection),
		Z_Construct_UScriptStruct_FMediaIOConnection_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIOConnection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaIOConnection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaIOConnection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaIOConnection"), sizeof(FMediaIOConnection), Get_Z_Construct_UScriptStruct_FMediaIOConnection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaIOConnection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaIOConnection_Hash() { return 2023483879U; }
class UScriptStruct* FMediaIODevice::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaIODevice_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaIODevice, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaIODevice"), sizeof(FMediaIODevice), Get_Z_Construct_UScriptStruct_FMediaIODevice_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaIODevice>()
{
	return FMediaIODevice::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaIODevice(FMediaIODevice::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaIODevice"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIODevice
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIODevice()
	{
		UScriptStruct::DeferCppStructOps<FMediaIODevice>(FName(TEXT("MediaIODevice")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaIODevice;
	struct Z_Construct_UScriptStruct_FMediaIODevice_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DeviceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceIdentifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIODevice_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Identifies a device.\n */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "Identifies a device." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaIODevice>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Device" },
		{ "Comment", "/** The retail/display name of the Device. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The retail/display name of the Device." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIODevice, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceIdentifier_MetaData[] = {
		{ "Category", "Device" },
		{ "Comment", "/** The device identifier. */" },
		{ "ModuleRelativePath", "Public/MediaIOCoreDefinitions.h" },
		{ "ToolTip", "The device identifier." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceIdentifier = { "DeviceIdentifier", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaIODevice, DeviceIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceIdentifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaIODevice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaIODevice_Statics::NewProp_DeviceIdentifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaIODevice_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaIODevice",
		sizeof(FMediaIODevice),
		alignof(FMediaIODevice),
		Z_Construct_UScriptStruct_FMediaIODevice_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIODevice_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaIODevice_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaIODevice_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaIODevice()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaIODevice_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaIODevice"), sizeof(FMediaIODevice), Get_Z_Construct_UScriptStruct_FMediaIODevice_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaIODevice_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaIODevice_Hash() { return 4104641813U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
