// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaIOCore/Public/MediaOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaOutput() {}
// Cross Module References
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MediaIOCore();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMediaOutput::execValidate)
	{
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_OutFailureReason);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->Validate(Z_Param_Out_OutFailureReason);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaOutput::execCreateMediaCapture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMediaCapture**)Z_Param__Result=P_THIS->CreateMediaCapture();
		P_NATIVE_END;
	}
	void UMediaOutput::StaticRegisterNativesUMediaOutput()
	{
		UClass* Class = UMediaOutput::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateMediaCapture", &UMediaOutput::execCreateMediaCapture },
			{ "Validate", &UMediaOutput::execValidate },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics
	{
		struct MediaOutput_eventCreateMediaCapture_Parms
		{
			UMediaCapture* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaOutput_eventCreateMediaCapture_Parms, ReturnValue), Z_Construct_UClass_UMediaCapture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/** Creates the specific implementation of the MediaCapture for the MediaOutput. */" },
		{ "ModuleRelativePath", "Public/MediaOutput.h" },
		{ "ToolTip", "Creates the specific implementation of the MediaCapture for the MediaOutput." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaOutput, nullptr, "CreateMediaCapture", nullptr, nullptr, sizeof(MediaOutput_eventCreateMediaCapture_Parms), Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaOutput_CreateMediaCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaOutput_CreateMediaCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaOutput_Validate_Statics
	{
		struct MediaOutput_eventValidate_Parms
		{
			FString OutFailureReason;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutFailureReason;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMediaOutput_Validate_Statics::NewProp_OutFailureReason = { "OutFailureReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaOutput_eventValidate_Parms, OutFailureReason), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMediaOutput_Validate_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MediaOutput_eventValidate_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMediaOutput_Validate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MediaOutput_eventValidate_Parms), &Z_Construct_UFunction_UMediaOutput_Validate_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaOutput_Validate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaOutput_Validate_Statics::NewProp_OutFailureReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaOutput_Validate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaOutput_Validate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/**\n\x09 * Validate the media output settings (must be implemented in child classes).\n\x09 *\n\x09 * @return true if validation passed, false otherwise.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaOutput.h" },
		{ "ToolTip", "Validate the media output settings (must be implemented in child classes).\n\n@return true if validation passed, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaOutput_Validate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaOutput, nullptr, "Validate", nullptr, nullptr, sizeof(MediaOutput_eventValidate_Parms), Z_Construct_UFunction_UMediaOutput_Validate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaOutput_Validate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaOutput_Validate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaOutput_Validate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaOutput_Validate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaOutput_Validate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMediaOutput_NoRegister()
	{
		return UMediaOutput::StaticClass();
	}
	struct Z_Construct_UClass_UMediaOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfTextureBuffers_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfTextureBuffers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMediaOutput_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMediaOutput_CreateMediaCapture, "CreateMediaCapture" }, // 1774469954
		{ &Z_Construct_UFunction_UMediaOutput_Validate, "Validate" }, // 3038275238
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaOutput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Abstract base class for media output.\n *\n * Media output describe the location and/or settings of media objects that can\n * be used to output the content of UE4 to a target device via a MediaCapture.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "MediaOutput.h" },
		{ "ModuleRelativePath", "Public/MediaOutput.h" },
		{ "ToolTip", "Abstract base class for media output.\n\nMedia output describe the location and/or settings of media objects that can\nbe used to output the content of UE4 to a target device via a MediaCapture." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaOutput_Statics::NewProp_NumberOfTextureBuffers_MetaData[] = {
		{ "Category", "Output" },
		{ "ClampMax", "8" },
		{ "ClampMin", "1" },
		{ "Comment", "/**\n\x09 * Number of texture used to transfer the texture from the GPU to the system memory.\n\x09 * A smaller number is most likely to block the GPU (wait for the transfer to complete).\n\x09 * A bigger number is most likely to increase latency.\n\x09 * @note Some Capture are not are executed on the GPU. If it's the case then no buffer will be needed and no buffer will be created.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaOutput.h" },
		{ "ToolTip", "Number of texture used to transfer the texture from the GPU to the system memory.\nA smaller number is most likely to block the GPU (wait for the transfer to complete).\nA bigger number is most likely to increase latency.\n@note Some Capture are not are executed on the GPU. If it's the case then no buffer will be needed and no buffer will be created." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMediaOutput_Statics::NewProp_NumberOfTextureBuffers = { "NumberOfTextureBuffers", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaOutput, NumberOfTextureBuffers), METADATA_PARAMS(Z_Construct_UClass_UMediaOutput_Statics::NewProp_NumberOfTextureBuffers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaOutput_Statics::NewProp_NumberOfTextureBuffers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaOutput_Statics::NewProp_NumberOfTextureBuffers,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaOutput_Statics::ClassParams = {
		&UMediaOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMediaOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaOutput_Statics::PropPointers),
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaOutput, 1555678193);
	template<> MEDIAIOCORE_API UClass* StaticClass<UMediaOutput>()
	{
		return UMediaOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaOutput(Z_Construct_UClass_UMediaOutput, &UMediaOutput::StaticClass, TEXT("/Script/MediaIOCore"), TEXT("UMediaOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
