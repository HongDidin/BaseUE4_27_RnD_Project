// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaIOCore/Public/FileMediaCapture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFileMediaCapture() {}
// Cross Module References
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UFileMediaCapture_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UFileMediaCapture();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture();
	UPackage* Z_Construct_UPackage__Script_MediaIOCore();
// End Cross Module References
	void UFileMediaCapture::StaticRegisterNativesUFileMediaCapture()
	{
	}
	UClass* Z_Construct_UClass_UFileMediaCapture_NoRegister()
	{
		return UFileMediaCapture::StaticClass();
	}
	struct Z_Construct_UClass_UFileMediaCapture_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFileMediaCapture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMediaCapture,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaCapture_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "FileMediaCapture.h" },
		{ "ModuleRelativePath", "Public/FileMediaCapture.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFileMediaCapture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFileMediaCapture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFileMediaCapture_Statics::ClassParams = {
		&UFileMediaCapture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFileMediaCapture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaCapture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFileMediaCapture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFileMediaCapture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFileMediaCapture, 4154410827);
	template<> MEDIAIOCORE_API UClass* StaticClass<UFileMediaCapture>()
	{
		return UFileMediaCapture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFileMediaCapture(Z_Construct_UClass_UFileMediaCapture, &UFileMediaCapture::StaticClass, TEXT("/Script/MediaIOCore"), TEXT("UFileMediaCapture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFileMediaCapture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
