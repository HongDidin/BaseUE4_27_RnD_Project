// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaIOEditor/Private/FileMediaOutputFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFileMediaOutputFactory() {}
// Cross Module References
	MEDIAIOEDITOR_API UClass* Z_Construct_UClass_UFileMediaOutputFactory_NoRegister();
	MEDIAIOEDITOR_API UClass* Z_Construct_UClass_UFileMediaOutputFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_MediaIOEditor();
// End Cross Module References
	void UFileMediaOutputFactory::StaticRegisterNativesUFileMediaOutputFactory()
	{
	}
	UClass* Z_Construct_UClass_UFileMediaOutputFactory_NoRegister()
	{
		return UFileMediaOutputFactory::StaticClass();
	}
	struct Z_Construct_UClass_UFileMediaOutputFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFileMediaOutputFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFileMediaOutputFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UFileMediaOutput objects.\n */" },
		{ "IncludePath", "FileMediaOutputFactory.h" },
		{ "ModuleRelativePath", "Private/FileMediaOutputFactory.h" },
		{ "ToolTip", "Implements a factory for UFileMediaOutput objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFileMediaOutputFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFileMediaOutputFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFileMediaOutputFactory_Statics::ClassParams = {
		&UFileMediaOutputFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFileMediaOutputFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFileMediaOutputFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFileMediaOutputFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFileMediaOutputFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFileMediaOutputFactory, 3348615396);
	template<> MEDIAIOEDITOR_API UClass* StaticClass<UFileMediaOutputFactory>()
	{
		return UFileMediaOutputFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFileMediaOutputFactory(Z_Construct_UClass_UFileMediaOutputFactory, &UFileMediaOutputFactory::StaticClass, TEXT("/Script/MediaIOEditor"), TEXT("UFileMediaOutputFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFileMediaOutputFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
