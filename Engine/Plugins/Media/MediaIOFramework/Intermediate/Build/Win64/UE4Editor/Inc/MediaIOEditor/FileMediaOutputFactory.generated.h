// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAIOEDITOR_FileMediaOutputFactory_generated_h
#error "FileMediaOutputFactory.generated.h already included, missing '#pragma once' in FileMediaOutputFactory.h"
#endif
#define MEDIAIOEDITOR_FileMediaOutputFactory_generated_h

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_SPARSE_DATA
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_RPC_WRAPPERS
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFileMediaOutputFactory(); \
	friend struct Z_Construct_UClass_UFileMediaOutputFactory_Statics; \
public: \
	DECLARE_CLASS(UFileMediaOutputFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UFileMediaOutputFactory)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUFileMediaOutputFactory(); \
	friend struct Z_Construct_UClass_UFileMediaOutputFactory_Statics; \
public: \
	DECLARE_CLASS(UFileMediaOutputFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MediaIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UFileMediaOutputFactory)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFileMediaOutputFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFileMediaOutputFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFileMediaOutputFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFileMediaOutputFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFileMediaOutputFactory(UFileMediaOutputFactory&&); \
	NO_API UFileMediaOutputFactory(const UFileMediaOutputFactory&); \
public:


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFileMediaOutputFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFileMediaOutputFactory(UFileMediaOutputFactory&&); \
	NO_API UFileMediaOutputFactory(const UFileMediaOutputFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFileMediaOutputFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFileMediaOutputFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFileMediaOutputFactory)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_12_PROLOG
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_SPARSE_DATA \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_INCLASS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_SPARSE_DATA \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FileMediaOutputFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAIOEDITOR_API UClass* StaticClass<class UFileMediaOutputFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaIOFramework_Source_MediaIOEditor_Private_FileMediaOutputFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
