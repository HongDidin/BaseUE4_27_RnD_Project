// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MEDIAIOCORE_MediaIOCoreDefinitions_generated_h
#error "MediaIOCoreDefinitions.generated.h already included, missing '#pragma once' in MediaIOCoreDefinitions.h"
#endif
#define MEDIAIOCORE_MediaIOCoreDefinitions_generated_h

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h_269_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaIOOutputConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaIOOutputConfiguration>();

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h_239_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaIOInputConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaIOInputConfiguration>();

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h_208_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaIOConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaIOConfiguration>();

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h_169_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaIOMode_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaIOMode>();

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h_124_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaIOConnection_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaIOConnection>();

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h_97_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaIODevice_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaIODevice>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaIOCoreDefinitions_h


#define FOREACH_ENUM_EMEDIAIOREFERENCETYPE(op) \
	op(EMediaIOReferenceType::FreeRun) \
	op(EMediaIOReferenceType::External) \
	op(EMediaIOReferenceType::Input) 

enum class EMediaIOReferenceType;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOReferenceType>();

#define FOREACH_ENUM_EMEDIAIOOUTPUTTYPE(op) \
	op(EMediaIOOutputType::Fill) \
	op(EMediaIOOutputType::FillAndKey) 

enum class EMediaIOOutputType;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOOutputType>();

#define FOREACH_ENUM_EMEDIAIOINPUTTYPE(op) \
	op(EMediaIOInputType::Fill) \
	op(EMediaIOInputType::FillAndKey) 

enum class EMediaIOInputType;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOInputType>();

#define FOREACH_ENUM_EMEDIAIOTIMECODEFORMAT(op) \
	op(EMediaIOTimecodeFormat::None) \
	op(EMediaIOTimecodeFormat::LTC) \
	op(EMediaIOTimecodeFormat::VITC) 

enum class EMediaIOTimecodeFormat;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOTimecodeFormat>();

#define FOREACH_ENUM_EMEDIAIOSTANDARDTYPE(op) \
	op(EMediaIOStandardType::Progressive) \
	op(EMediaIOStandardType::Interlaced) \
	op(EMediaIOStandardType::ProgressiveSegmentedFrame) 

enum class EMediaIOStandardType;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOStandardType>();

#define FOREACH_ENUM_EMEDIAIOQUADLINKTRANSPORTTYPE(op) \
	op(EMediaIOQuadLinkTransportType::SquareDivision) \
	op(EMediaIOQuadLinkTransportType::TwoSampleInterleave) 

enum class EMediaIOQuadLinkTransportType;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOQuadLinkTransportType>();

#define FOREACH_ENUM_EMEDIAIOTRANSPORTTYPE(op) \
	op(EMediaIOTransportType::SingleLink) \
	op(EMediaIOTransportType::DualLink) \
	op(EMediaIOTransportType::QuadLink) \
	op(EMediaIOTransportType::HDMI) 

enum class EMediaIOTransportType;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaIOTransportType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
