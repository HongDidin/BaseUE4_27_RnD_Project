// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MediaIOCore/Public/MediaCapture.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaCapture() {}
// Cross Module References
	MEDIAIOCORE_API UFunction* Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MediaIOCore();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaCaptureCroppingType();
	MEDIAIOCORE_API UEnum* Z_Construct_UEnum_MediaIOCore_EMediaCaptureState();
	MEDIAIOCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMediaCaptureOptions();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EPixelFormat();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate signatures */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Delegate signatures" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore, nullptr, "MediaCaptureStateChangedSignature__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMediaCaptureCroppingType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaCaptureCroppingType, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaCaptureCroppingType"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaCaptureCroppingType>()
	{
		return EMediaCaptureCroppingType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaCaptureCroppingType(EMediaCaptureCroppingType_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaCaptureCroppingType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaCaptureCroppingType_Hash() { return 4192598012U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaCaptureCroppingType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaCaptureCroppingType"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaCaptureCroppingType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaCaptureCroppingType::None", (int64)EMediaCaptureCroppingType::None },
				{ "EMediaCaptureCroppingType::Center", (int64)EMediaCaptureCroppingType::Center },
				{ "EMediaCaptureCroppingType::TopLeft", (int64)EMediaCaptureCroppingType::TopLeft },
				{ "EMediaCaptureCroppingType::Custom", (int64)EMediaCaptureCroppingType::Custom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Center.Comment", "/** Keep the center of the captured image. */" },
				{ "Center.Name", "EMediaCaptureCroppingType::Center" },
				{ "Center.ToolTip", "Keep the center of the captured image." },
				{ "Comment", "/**\n * Type of cropping \n */" },
				{ "Custom.Comment", "/** Use the StartCapturePoint and the size of the MediaOutput to keep of the captured image. */" },
				{ "Custom.Name", "EMediaCaptureCroppingType::Custom" },
				{ "Custom.ToolTip", "Use the StartCapturePoint and the size of the MediaOutput to keep of the captured image." },
				{ "ModuleRelativePath", "Public/MediaCapture.h" },
				{ "None.Comment", "/** Do not crop the captured image. */" },
				{ "None.Name", "EMediaCaptureCroppingType::None" },
				{ "None.ToolTip", "Do not crop the captured image." },
				{ "ToolTip", "Type of cropping" },
				{ "TopLeft.Comment", "/** Keep the top left corner of the captured image. */" },
				{ "TopLeft.Name", "EMediaCaptureCroppingType::TopLeft" },
				{ "TopLeft.ToolTip", "Keep the top left corner of the captured image." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaCaptureCroppingType",
				"EMediaCaptureCroppingType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMediaCaptureState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MediaIOCore_EMediaCaptureState, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("EMediaCaptureState"));
		}
		return Singleton;
	}
	template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaCaptureState>()
	{
		return EMediaCaptureState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMediaCaptureState(EMediaCaptureState_StaticEnum, TEXT("/Script/MediaIOCore"), TEXT("EMediaCaptureState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MediaIOCore_EMediaCaptureState_Hash() { return 3578119412U; }
	UEnum* Z_Construct_UEnum_MediaIOCore_EMediaCaptureState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMediaCaptureState"), 0, Get_Z_Construct_UEnum_MediaIOCore_EMediaCaptureState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMediaCaptureState::Error", (int64)EMediaCaptureState::Error },
				{ "EMediaCaptureState::Capturing", (int64)EMediaCaptureState::Capturing },
				{ "EMediaCaptureState::Preparing", (int64)EMediaCaptureState::Preparing },
				{ "EMediaCaptureState::StopRequested", (int64)EMediaCaptureState::StopRequested },
				{ "EMediaCaptureState::Stopped", (int64)EMediaCaptureState::Stopped },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Capturing.Comment", "/** Media is currently capturing. */" },
				{ "Capturing.Name", "EMediaCaptureState::Capturing" },
				{ "Capturing.ToolTip", "Media is currently capturing." },
				{ "Comment", "/**\n * Possible states of media capture.\n */" },
				{ "Error.Comment", "/** Unrecoverable error occurred during capture. */" },
				{ "Error.Name", "EMediaCaptureState::Error" },
				{ "Error.ToolTip", "Unrecoverable error occurred during capture." },
				{ "ModuleRelativePath", "Public/MediaCapture.h" },
				{ "Preparing.Comment", "/** Media is being prepared for capturing. */" },
				{ "Preparing.Name", "EMediaCaptureState::Preparing" },
				{ "Preparing.ToolTip", "Media is being prepared for capturing." },
				{ "Stopped.Comment", "/** Capture has been stopped. */" },
				{ "Stopped.Name", "EMediaCaptureState::Stopped" },
				{ "Stopped.ToolTip", "Capture has been stopped." },
				{ "StopRequested.Comment", "/** Capture has been stopped but some frames may need to be process. */" },
				{ "StopRequested.Name", "EMediaCaptureState::StopRequested" },
				{ "StopRequested.ToolTip", "Capture has been stopped but some frames may need to be process." },
				{ "ToolTip", "Possible states of media capture." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MediaIOCore,
				nullptr,
				"EMediaCaptureState",
				"EMediaCaptureState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMediaCaptureOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MEDIAIOCORE_API uint32 Get_Z_Construct_UScriptStruct_FMediaCaptureOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMediaCaptureOptions, Z_Construct_UPackage__Script_MediaIOCore(), TEXT("MediaCaptureOptions"), sizeof(FMediaCaptureOptions), Get_Z_Construct_UScriptStruct_FMediaCaptureOptions_Hash());
	}
	return Singleton;
}
template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<FMediaCaptureOptions>()
{
	return FMediaCaptureOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMediaCaptureOptions(FMediaCaptureOptions::StaticStruct, TEXT("/Script/MediaIOCore"), TEXT("MediaCaptureOptions"), false, nullptr, nullptr);
static struct FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaCaptureOptions
{
	FScriptStruct_MediaIOCore_StaticRegisterNativesFMediaCaptureOptions()
	{
		UScriptStruct::DeferCppStructOps<FMediaCaptureOptions>(FName(TEXT("MediaCaptureOptions")));
	}
} ScriptStruct_MediaIOCore_StaticRegisterNativesFMediaCaptureOptions;
	struct Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Crop_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Crop_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Crop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomCapturePoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomCapturePoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResizeSourceBuffer_MetaData[];
#endif
		static void NewProp_bResizeSourceBuffer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResizeSourceBuffer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSkipFrameWhenRunningExpensiveTasks_MetaData[];
#endif
		static void NewProp_bSkipFrameWhenRunningExpensiveTasks_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSkipFrameWhenRunningExpensiveTasks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bConvertToDesiredPixelFormat_MetaData[];
#endif
		static void NewProp_bConvertToDesiredPixelFormat_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bConvertToDesiredPixelFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceAlphaToOneOnConversion_MetaData[];
#endif
		static void NewProp_bForceAlphaToOneOnConversion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceAlphaToOneOnConversion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base class of additional data that can be stored for each requested capture.\n */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Base class of additional data that can be stored for each requested capture." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMediaCaptureOptions>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop_MetaData[] = {
		{ "Category", "MediaCapture" },
		{ "Comment", "/** Crop the captured SceneViewport or TextureRenderTarget2D to the desired size. */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Crop the captured SceneViewport or TextureRenderTarget2D to the desired size." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop = { "Crop", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaCaptureOptions, Crop), Z_Construct_UEnum_MediaIOCore_EMediaCaptureCroppingType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_CustomCapturePoint_MetaData[] = {
		{ "Category", "MediaCapture" },
		{ "Comment", "/**\n\x09 * Crop the captured SceneViewport or TextureRenderTarget2D to the desired size.\n\x09 * @note Only valid when Crop is set to Custom.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Crop the captured SceneViewport or TextureRenderTarget2D to the desired size.\n@note Only valid when Crop is set to Custom." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_CustomCapturePoint = { "CustomCapturePoint", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMediaCaptureOptions, CustomCapturePoint), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_CustomCapturePoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_CustomCapturePoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer_MetaData[] = {
		{ "Category", "MediaCapture" },
		{ "Comment", "/**\n\x09 * When the capture start, resize the source buffer to the desired size.\n\x09 * @note Only valid when a size is specified by the MediaOutput.\n\x09 * @note For viewport, the window size will not change. Only the viewport will be resized.\n\x09 * @note For RenderTarget, the asset will be modified and resized to the desired size.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "When the capture start, resize the source buffer to the desired size.\n@note Only valid when a size is specified by the MediaOutput.\n@note For viewport, the window size will not change. Only the viewport will be resized.\n@note For RenderTarget, the asset will be modified and resized to the desired size." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer_SetBit(void* Obj)
	{
		((FMediaCaptureOptions*)Obj)->bResizeSourceBuffer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer = { "bResizeSourceBuffer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaCaptureOptions), &Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks_MetaData[] = {
		{ "Category", "MediaCapture" },
		{ "Comment", "/**\n\x09 * When the application enters responsive mode, skip the frame capture.\n\x09 * The application can enter responsive mode on mouse down, viewport resize, ...\n\x09 * That is to ensure responsiveness in low FPS situations.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "When the application enters responsive mode, skip the frame capture.\nThe application can enter responsive mode on mouse down, viewport resize, ...\nThat is to ensure responsiveness in low FPS situations." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks_SetBit(void* Obj)
	{
		((FMediaCaptureOptions*)Obj)->bSkipFrameWhenRunningExpensiveTasks = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks = { "bSkipFrameWhenRunningExpensiveTasks", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaCaptureOptions), &Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat_MetaData[] = {
		{ "Category", "MediaCapture" },
		{ "Comment", "/**\n\x09 * Allows to enable/disable pixel format conversion for the cases where render target is not of the desired pixel format. \n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Allows to enable/disable pixel format conversion for the cases where render target is not of the desired pixel format." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat_SetBit(void* Obj)
	{
		((FMediaCaptureOptions*)Obj)->bConvertToDesiredPixelFormat = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat = { "bConvertToDesiredPixelFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaCaptureOptions), &Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion_MetaData[] = {
		{ "Category", "MediaCapture" },
		{ "Comment", "/**\n\x09 * In some cases when we want to stream irregular render targets containing limited number\n\x09 * of channels (for example RG16f), we would like to force Alpha to 1.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "In some cases when we want to stream irregular render targets containing limited number\nof channels (for example RG16f), we would like to force Alpha to 1." },
	};
#endif
	void Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion_SetBit(void* Obj)
	{
		((FMediaCaptureOptions*)Obj)->bForceAlphaToOneOnConversion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion = { "bForceAlphaToOneOnConversion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMediaCaptureOptions), &Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_Crop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_CustomCapturePoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bResizeSourceBuffer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bSkipFrameWhenRunningExpensiveTasks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bConvertToDesiredPixelFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::NewProp_bForceAlphaToOneOnConversion,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
		nullptr,
		&NewStructOps,
		"MediaCaptureOptions",
		sizeof(FMediaCaptureOptions),
		alignof(FMediaCaptureOptions),
		Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMediaCaptureOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMediaCaptureOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MediaIOCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MediaCaptureOptions"), sizeof(FMediaCaptureOptions), Get_Z_Construct_UScriptStruct_FMediaCaptureOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMediaCaptureOptions_Hash() { return 776310354U; }
	DEFINE_FUNCTION(UMediaCapture::execGetDesiredPixelFormat)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<EPixelFormat>*)Z_Param__Result=P_THIS->GetDesiredPixelFormat();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execGetDesiredSize)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FIntPoint*)Z_Param__Result=P_THIS->GetDesiredSize();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execSetMediaOutput)
	{
		P_GET_OBJECT(UMediaOutput,Z_Param_InMediaOutput);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMediaOutput(Z_Param_InMediaOutput);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execGetState)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EMediaCaptureState*)Z_Param__Result=P_THIS->GetState();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execStopCapture)
	{
		P_GET_UBOOL(Z_Param_bAllowPendingFrameToBeProcess);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopCapture(Z_Param_bAllowPendingFrameToBeProcess);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execUpdateTextureRenderTarget2D)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_RenderTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->UpdateTextureRenderTarget2D(Z_Param_RenderTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execCaptureTextureRenderTarget2D)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_RenderTarget);
		P_GET_STRUCT(FMediaCaptureOptions,Z_Param_CaptureOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CaptureTextureRenderTarget2D(Z_Param_RenderTarget,Z_Param_CaptureOptions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMediaCapture::execCaptureActiveSceneViewport)
	{
		P_GET_STRUCT(FMediaCaptureOptions,Z_Param_CaptureOptions);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CaptureActiveSceneViewport(Z_Param_CaptureOptions);
		P_NATIVE_END;
	}
	void UMediaCapture::StaticRegisterNativesUMediaCapture()
	{
		UClass* Class = UMediaCapture::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CaptureActiveSceneViewport", &UMediaCapture::execCaptureActiveSceneViewport },
			{ "CaptureTextureRenderTarget2D", &UMediaCapture::execCaptureTextureRenderTarget2D },
			{ "GetDesiredPixelFormat", &UMediaCapture::execGetDesiredPixelFormat },
			{ "GetDesiredSize", &UMediaCapture::execGetDesiredSize },
			{ "GetState", &UMediaCapture::execGetState },
			{ "SetMediaOutput", &UMediaCapture::execSetMediaOutput },
			{ "StopCapture", &UMediaCapture::execStopCapture },
			{ "UpdateTextureRenderTarget2D", &UMediaCapture::execUpdateTextureRenderTarget2D },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics
	{
		struct MediaCapture_eventCaptureActiveSceneViewport_Parms
		{
			FMediaCaptureOptions CaptureOptions;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventCaptureActiveSceneViewport_Parms, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MediaCapture_eventCaptureActiveSceneViewport_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MediaCapture_eventCaptureActiveSceneViewport_Parms), &Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::NewProp_CaptureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/**\n\x09 * Stop the current capture if there is one.\n\x09 * Then find and capture every frame from active SceneViewport.\n\x09 * It can only find a SceneViewport when you play in Standalone or in \"New Editor Window PIE\".\n\x09 * If the active SceneViewport is destroyed, the capture will stop.\n\x09 * The SceneViewport needs to be of the same size and have the same pixel format as requested by the media output.\n\x09 * @return True if the capture was successfully started\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Stop the current capture if there is one.\nThen find and capture every frame from active SceneViewport.\nIt can only find a SceneViewport when you play in Standalone or in \"New Editor Window PIE\".\nIf the active SceneViewport is destroyed, the capture will stop.\nThe SceneViewport needs to be of the same size and have the same pixel format as requested by the media output.\n@return True if the capture was successfully started" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "CaptureActiveSceneViewport", nullptr, nullptr, sizeof(MediaCapture_eventCaptureActiveSceneViewport_Parms), Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics
	{
		struct MediaCapture_eventCaptureTextureRenderTarget2D_Parms
		{
			UTextureRenderTarget2D* RenderTarget;
			FMediaCaptureOptions CaptureOptions;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureOptions;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventCaptureTextureRenderTarget2D_Parms, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_CaptureOptions = { "CaptureOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventCaptureTextureRenderTarget2D_Parms, CaptureOptions), Z_Construct_UScriptStruct_FMediaCaptureOptions, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MediaCapture_eventCaptureTextureRenderTarget2D_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MediaCapture_eventCaptureTextureRenderTarget2D_Parms), &Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_CaptureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/**\n\x09 * Stop the actual capture if there is one.\n\x09 * Then capture every frame for a TextureRenderTarget2D.\n\x09 * The TextureRenderTarget2D needs to be of the same size and have the same pixel format as requested by the media output.\n\x09 * @return True if the capture was successfully started\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Stop the actual capture if there is one.\nThen capture every frame for a TextureRenderTarget2D.\nThe TextureRenderTarget2D needs to be of the same size and have the same pixel format as requested by the media output.\n@return True if the capture was successfully started" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "CaptureTextureRenderTarget2D", nullptr, nullptr, sizeof(MediaCapture_eventCaptureTextureRenderTarget2D_Parms), Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics
	{
		struct MediaCapture_eventGetDesiredPixelFormat_Parms
		{
			TEnumAsByte<EPixelFormat> ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventGetDesiredPixelFormat_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EPixelFormat, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/** Get the desired pixel format of the current capture. */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Get the desired pixel format of the current capture." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "GetDesiredPixelFormat", nullptr, nullptr, sizeof(MediaCapture_eventGetDesiredPixelFormat_Parms), Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics
	{
		struct MediaCapture_eventGetDesiredSize_Parms
		{
			FIntPoint ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventGetDesiredSize_Parms, ReturnValue), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/** Get the desired size of the current capture. */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Get the desired size of the current capture." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "GetDesiredSize", nullptr, nullptr, sizeof(MediaCapture_eventGetDesiredSize_Parms), Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_GetDesiredSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_GetDesiredSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_GetState_Statics
	{
		struct MediaCapture_eventGetState_Parms
		{
			EMediaCaptureState ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMediaCapture_GetState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMediaCapture_GetState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventGetState_Parms, ReturnValue), Z_Construct_UEnum_MediaIOCore_EMediaCaptureState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_GetState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_GetState_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_GetState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_GetState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/** Get the current state of the capture. */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Get the current state of the capture." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_GetState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "GetState", nullptr, nullptr, sizeof(MediaCapture_eventGetState_Parms), Z_Construct_UFunction_UMediaCapture_GetState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_GetState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_GetState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_GetState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_GetState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_GetState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics
	{
		struct MediaCapture_eventSetMediaOutput_Parms
		{
			UMediaOutput* InMediaOutput;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InMediaOutput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::NewProp_InMediaOutput = { "InMediaOutput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventSetMediaOutput_Parms, InMediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::NewProp_InMediaOutput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/** Set the media output. Can only be set when the capture is stopped. */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Set the media output. Can only be set when the capture is stopped." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "SetMediaOutput", nullptr, nullptr, sizeof(MediaCapture_eventSetMediaOutput_Parms), Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_SetMediaOutput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_SetMediaOutput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_StopCapture_Statics
	{
		struct MediaCapture_eventStopCapture_Parms
		{
			bool bAllowPendingFrameToBeProcess;
		};
		static void NewProp_bAllowPendingFrameToBeProcess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowPendingFrameToBeProcess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::NewProp_bAllowPendingFrameToBeProcess_SetBit(void* Obj)
	{
		((MediaCapture_eventStopCapture_Parms*)Obj)->bAllowPendingFrameToBeProcess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::NewProp_bAllowPendingFrameToBeProcess = { "bAllowPendingFrameToBeProcess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MediaCapture_eventStopCapture_Parms), &Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::NewProp_bAllowPendingFrameToBeProcess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::NewProp_bAllowPendingFrameToBeProcess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/**\n\x09 * Stop the previous requested capture.\n\x09 * @param bAllowPendingFrameToBeProcess\x09Keep copying the pending frames asynchronously or stop immediately without copying the pending frames.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Stop the previous requested capture.\n@param bAllowPendingFrameToBeProcess Keep copying the pending frames asynchronously or stop immediately without copying the pending frames." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "StopCapture", nullptr, nullptr, sizeof(MediaCapture_eventStopCapture_Parms), Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_StopCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_StopCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics
	{
		struct MediaCapture_eventUpdateTextureRenderTarget2D_Parms
		{
			UTextureRenderTarget2D* RenderTarget;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MediaCapture_eventUpdateTextureRenderTarget2D_Parms, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MediaCapture_eventUpdateTextureRenderTarget2D_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MediaCapture_eventUpdateTextureRenderTarget2D_Parms), &Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::Function_MetaDataParams[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/**\n\x09 * Update the current capture with every frame for a TextureRenderTarget2D.\n\x09 * The TextureRenderTarget2D needs to be of the same size and have the same pixel format as requested by the media output.\n\x09 * @return Return true if the capture was successfully updated. If false is returned, the capture was stopped.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Update the current capture with every frame for a TextureRenderTarget2D.\nThe TextureRenderTarget2D needs to be of the same size and have the same pixel format as requested by the media output.\n@return Return true if the capture was successfully updated. If false is returned, the capture was stopped." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMediaCapture, nullptr, "UpdateTextureRenderTarget2D", nullptr, nullptr, sizeof(MediaCapture_eventUpdateTextureRenderTarget2D_Parms), Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMediaCapture_NoRegister()
	{
		return UMediaCapture::StaticClass();
	}
	struct Z_Construct_UClass_UMediaCapture_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStateChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStateChanged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturingRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CapturingRenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaCapture_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MediaIOCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMediaCapture_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMediaCapture_CaptureActiveSceneViewport, "CaptureActiveSceneViewport" }, // 1337472030
		{ &Z_Construct_UFunction_UMediaCapture_CaptureTextureRenderTarget2D, "CaptureTextureRenderTarget2D" }, // 2497282931
		{ &Z_Construct_UFunction_UMediaCapture_GetDesiredPixelFormat, "GetDesiredPixelFormat" }, // 1991930169
		{ &Z_Construct_UFunction_UMediaCapture_GetDesiredSize, "GetDesiredSize" }, // 3188156674
		{ &Z_Construct_UFunction_UMediaCapture_GetState, "GetState" }, // 1339109557
		{ &Z_Construct_UFunction_UMediaCapture_SetMediaOutput, "SetMediaOutput" }, // 1617656937
		{ &Z_Construct_UFunction_UMediaCapture_StopCapture, "StopCapture" }, // 3399017581
		{ &Z_Construct_UFunction_UMediaCapture_UpdateTextureRenderTarget2D, "UpdateTextureRenderTarget2D" }, // 1389822801
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaCapture_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Abstract base class for media capture.\n *\n * MediaCapture capture the texture of the Render target or the SceneViewport and sends it to an external media device.\n * MediaCapture should be created by a MediaOutput.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "MediaCapture.h" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Abstract base class for media capture.\n\nMediaCapture capture the texture of the Render target or the SceneViewport and sends it to an external media device.\nMediaCapture should be created by a MediaOutput." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaCapture_Statics::NewProp_OnStateChanged_MetaData[] = {
		{ "Category", "Media|Output" },
		{ "Comment", "/** Called when the state of the capture changed. */" },
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
		{ "ToolTip", "Called when the state of the capture changed." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMediaCapture_Statics::NewProp_OnStateChanged = { "OnStateChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaCapture, OnStateChanged), Z_Construct_UDelegateFunction_MediaIOCore_MediaCaptureStateChangedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMediaCapture_Statics::NewProp_OnStateChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaCapture_Statics::NewProp_OnStateChanged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaCapture_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaCapture_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaCapture, MediaOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaCapture_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaCapture_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaCapture_Statics::NewProp_CapturingRenderTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/MediaCapture.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaCapture_Statics::NewProp_CapturingRenderTarget = { "CapturingRenderTarget", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaCapture, CapturingRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaCapture_Statics::NewProp_CapturingRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaCapture_Statics::NewProp_CapturingRenderTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaCapture_Statics::NewProp_OnStateChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaCapture_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaCapture_Statics::NewProp_CapturingRenderTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaCapture_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaCapture>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaCapture_Statics::ClassParams = {
		&UMediaCapture::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMediaCapture_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaCapture_Statics::PropPointers),
		0,
		0x009010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaCapture_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaCapture_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaCapture()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaCapture_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaCapture, 482171265);
	template<> MEDIAIOCORE_API UClass* StaticClass<UMediaCapture>()
	{
		return UMediaCapture::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaCapture(Z_Construct_UClass_UMediaCapture, &UMediaCapture::StaticClass, TEXT("/Script/MediaIOCore"), TEXT("UMediaCapture"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaCapture);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
