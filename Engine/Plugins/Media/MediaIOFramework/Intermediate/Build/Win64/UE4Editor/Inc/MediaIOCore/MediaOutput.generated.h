// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMediaCapture;
#ifdef MEDIAIOCORE_MediaOutput_generated_h
#error "MediaOutput.generated.h already included, missing '#pragma once' in MediaOutput.h"
#endif
#define MEDIAIOCORE_MediaOutput_generated_h

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_SPARSE_DATA
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execValidate); \
	DECLARE_FUNCTION(execCreateMediaCapture);


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execValidate); \
	DECLARE_FUNCTION(execCreateMediaCapture);


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaOutput(); \
	friend struct Z_Construct_UClass_UMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UMediaOutput, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MediaIOCore"), NO_API) \
	DECLARE_SERIALIZER(UMediaOutput)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUMediaOutput(); \
	friend struct Z_Construct_UClass_UMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UMediaOutput, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MediaIOCore"), NO_API) \
	DECLARE_SERIALIZER(UMediaOutput)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaOutput(UMediaOutput&&); \
	NO_API UMediaOutput(const UMediaOutput&); \
public:


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaOutput(UMediaOutput&&); \
	NO_API UMediaOutput(const UMediaOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaOutput); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaOutput)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_38_PROLOG
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_SPARSE_DATA \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_INCLASS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_SPARSE_DATA \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h_41_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaOutput."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAIOCORE_API UClass* StaticClass<class UMediaOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
