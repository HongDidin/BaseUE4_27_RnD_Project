// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FIntPoint;
class UMediaOutput;
enum class EMediaCaptureState : uint8;
class UTextureRenderTarget2D;
struct FMediaCaptureOptions;
#ifdef MEDIAIOCORE_MediaCapture_generated_h
#error "MediaCapture.generated.h already included, missing '#pragma once' in MediaCapture.h"
#endif
#define MEDIAIOCORE_MediaCapture_generated_h

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_74_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMediaCaptureOptions_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MEDIAIOCORE_API UScriptStruct* StaticStruct<struct FMediaCaptureOptions>();

#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_123_DELEGATE \
static inline void FMediaCaptureStateChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& MediaCaptureStateChangedSignature) \
{ \
	MediaCaptureStateChangedSignature.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_SPARSE_DATA
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDesiredPixelFormat); \
	DECLARE_FUNCTION(execGetDesiredSize); \
	DECLARE_FUNCTION(execSetMediaOutput); \
	DECLARE_FUNCTION(execGetState); \
	DECLARE_FUNCTION(execStopCapture); \
	DECLARE_FUNCTION(execUpdateTextureRenderTarget2D); \
	DECLARE_FUNCTION(execCaptureTextureRenderTarget2D); \
	DECLARE_FUNCTION(execCaptureActiveSceneViewport);


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDesiredPixelFormat); \
	DECLARE_FUNCTION(execGetDesiredSize); \
	DECLARE_FUNCTION(execSetMediaOutput); \
	DECLARE_FUNCTION(execGetState); \
	DECLARE_FUNCTION(execStopCapture); \
	DECLARE_FUNCTION(execUpdateTextureRenderTarget2D); \
	DECLARE_FUNCTION(execCaptureTextureRenderTarget2D); \
	DECLARE_FUNCTION(execCaptureActiveSceneViewport);


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaCapture(); \
	friend struct Z_Construct_UClass_UMediaCapture_Statics; \
public: \
	DECLARE_CLASS(UMediaCapture, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MediaIOCore"), NO_API) \
	DECLARE_SERIALIZER(UMediaCapture)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_INCLASS \
private: \
	static void StaticRegisterNativesUMediaCapture(); \
	friend struct Z_Construct_UClass_UMediaCapture_Statics; \
public: \
	DECLARE_CLASS(UMediaCapture, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MediaIOCore"), NO_API) \
	DECLARE_SERIALIZER(UMediaCapture)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaCapture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaCapture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaCapture(UMediaCapture&&); \
	NO_API UMediaCapture(const UMediaCapture&); \
public:


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaCapture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaCapture(UMediaCapture&&); \
	NO_API UMediaCapture(const UMediaCapture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaCapture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaCapture); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaCapture)


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaOutput() { return STRUCT_OFFSET(UMediaCapture, MediaOutput); } \
	FORCEINLINE static uint32 __PPO__CapturingRenderTarget() { return STRUCT_OFFSET(UMediaCapture, CapturingRenderTarget); }


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_133_PROLOG
#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_SPARSE_DATA \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_RPC_WRAPPERS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_INCLASS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_SPARSE_DATA \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h_136_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MediaCapture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MEDIAIOCORE_API UClass* StaticClass<class UMediaCapture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_MediaIOFramework_Source_MediaIOCore_Public_MediaCapture_h


#define FOREACH_ENUM_EMEDIACAPTURECROPPINGTYPE(op) \
	op(EMediaCaptureCroppingType::None) \
	op(EMediaCaptureCroppingType::Center) \
	op(EMediaCaptureCroppingType::TopLeft) \
	op(EMediaCaptureCroppingType::Custom) 

enum class EMediaCaptureCroppingType : uint8;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaCaptureCroppingType>();

#define FOREACH_ENUM_EMEDIACAPTURESTATE(op) \
	op(EMediaCaptureState::Error) \
	op(EMediaCaptureState::Capturing) \
	op(EMediaCaptureState::Preparing) \
	op(EMediaCaptureState::StopRequested) \
	op(EMediaCaptureState::Stopped) 

enum class EMediaCaptureState : uint8;
template<> MEDIAIOCORE_API UEnum* StaticEnum<EMediaCaptureState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
