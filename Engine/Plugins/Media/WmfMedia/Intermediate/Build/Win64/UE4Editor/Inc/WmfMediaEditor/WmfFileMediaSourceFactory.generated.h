// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WMFMEDIAEDITOR_WmfFileMediaSourceFactory_generated_h
#error "WmfFileMediaSourceFactory.generated.h already included, missing '#pragma once' in WmfFileMediaSourceFactory.h"
#endif
#define WMFMEDIAEDITOR_WmfFileMediaSourceFactory_generated_h

#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_SPARSE_DATA
#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWmfFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UWmfFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UWmfFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WmfMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UWmfFileMediaSourceFactory)


#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUWmfFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UWmfFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UWmfFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WmfMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UWmfFileMediaSourceFactory)


#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWmfFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWmfFileMediaSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWmfFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWmfFileMediaSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWmfFileMediaSourceFactory(UWmfFileMediaSourceFactory&&); \
	NO_API UWmfFileMediaSourceFactory(const UWmfFileMediaSourceFactory&); \
public:


#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWmfFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWmfFileMediaSourceFactory(UWmfFileMediaSourceFactory&&); \
	NO_API UWmfFileMediaSourceFactory(const UWmfFileMediaSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWmfFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWmfFileMediaSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWmfFileMediaSourceFactory)


#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_14_PROLOG
#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_INCLASS \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WmfFileMediaSourceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WMFMEDIAEDITOR_API UClass* StaticClass<class UWmfFileMediaSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_WmfMedia_Source_WmfMediaEditor_Private_WmfFileMediaSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
