// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBMMEDIAEDITOR_WebMFileMediaSourceFactory_generated_h
#error "WebMFileMediaSourceFactory.generated.h already included, missing '#pragma once' in WebMFileMediaSourceFactory.h"
#endif
#define WEBMMEDIAEDITOR_WebMFileMediaSourceFactory_generated_h

#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_SPARSE_DATA
#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWebMPlatFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UWebMPlatFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UWebMPlatFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WebMMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UWebMPlatFileMediaSourceFactory)


#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUWebMPlatFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UWebMPlatFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UWebMPlatFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WebMMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UWebMPlatFileMediaSourceFactory)


#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebMPlatFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebMPlatFileMediaSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebMPlatFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebMPlatFileMediaSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebMPlatFileMediaSourceFactory(UWebMPlatFileMediaSourceFactory&&); \
	NO_API UWebMPlatFileMediaSourceFactory(const UWebMPlatFileMediaSourceFactory&); \
public:


#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWebMPlatFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWebMPlatFileMediaSourceFactory(UWebMPlatFileMediaSourceFactory&&); \
	NO_API UWebMPlatFileMediaSourceFactory(const UWebMPlatFileMediaSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWebMPlatFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWebMPlatFileMediaSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWebMPlatFileMediaSourceFactory)


#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_13_PROLOG
#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_INCLASS \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_SPARSE_DATA \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class WebMPlatFileMediaSourceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WEBMMEDIAEDITOR_API UClass* StaticClass<class UWebMPlatFileMediaSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Media_WebMMedia_Source_WebMMediaEditor_Private_WebMFileMediaSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
