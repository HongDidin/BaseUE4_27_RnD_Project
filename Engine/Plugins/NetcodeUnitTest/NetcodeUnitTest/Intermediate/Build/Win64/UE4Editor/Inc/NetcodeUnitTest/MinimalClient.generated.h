// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETCODEUNITTEST_MinimalClient_generated_h
#error "MinimalClient.generated.h already included, missing '#pragma once' in MinimalClient.h"
#endif
#define NETCODEUNITTEST_MinimalClient_generated_h

#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_SPARSE_DATA
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_RPC_WRAPPERS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMinimalClient(); \
	friend struct Z_Construct_UClass_UMinimalClient_Statics; \
public: \
	DECLARE_CLASS(UMinimalClient, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(UMinimalClient)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_INCLASS \
private: \
	static void StaticRegisterNativesUMinimalClient(); \
	friend struct Z_Construct_UClass_UMinimalClient_Statics; \
public: \
	DECLARE_CLASS(UMinimalClient, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(UMinimalClient)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMinimalClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMinimalClient) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinimalClient); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinimalClient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinimalClient(UMinimalClient&&); \
	NO_API UMinimalClient(const UMinimalClient&); \
public:


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMinimalClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinimalClient(UMinimalClient&&); \
	NO_API UMinimalClient(const UMinimalClient&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinimalClient); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinimalClient); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMinimalClient)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_306_PROLOG
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_RPC_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_INCLASS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h_315_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MinimalClient."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETCODEUNITTEST_API UClass* StaticClass<class UMinimalClient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_MinimalClient_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
