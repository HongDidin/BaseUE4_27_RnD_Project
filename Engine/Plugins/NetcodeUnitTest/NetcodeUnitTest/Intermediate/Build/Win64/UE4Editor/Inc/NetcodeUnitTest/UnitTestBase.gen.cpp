// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTestBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnitTestBase() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestBase_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UUnitTestBase::StaticRegisterNativesUUnitTestBase()
	{
	}
	UClass* Z_Construct_UClass_UUnitTestBase_NoRegister()
	{
		return UUnitTestBase::StaticClass();
	}
	struct Z_Construct_UClass_UUnitTestBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnitTestBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTestBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for the unit test framework event implementation\n * (all engine/external-triggered events are wrapped, in order to hook logs triggered during their execution)\n *\n * NOTE: All wrapped functions/events, begin with 'UT'\n */" },
		{ "IncludePath", "UnitTestBase.h" },
		{ "ModuleRelativePath", "Classes/UnitTestBase.h" },
		{ "ToolTip", "Base class for the unit test framework event implementation\n(all engine/external-triggered events are wrapped, in order to hook logs triggered during their execution)\n\nNOTE: All wrapped functions/events, begin with 'UT'" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnitTestBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnitTestBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnitTestBase_Statics::ClassParams = {
		&UUnitTestBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UUnitTestBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTestBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnitTestBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnitTestBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitTestBase, 3678796397);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UUnitTestBase>()
	{
		return UUnitTestBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitTestBase(Z_Construct_UClass_UUnitTestBase, &UUnitTestBase::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UUnitTestBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitTestBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
