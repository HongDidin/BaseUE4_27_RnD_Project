// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTests/Engine/IPClient.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIPClient() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UIPClient_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UIPClient();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UClientUnitTest();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UIPClient::StaticRegisterNativesUIPClient()
	{
	}
	UClass* Z_Construct_UClass_UIPClient_NoRegister()
	{
		return UIPClient::StaticClass();
	}
	struct Z_Construct_UClass_UIPClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UIPClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UClientUnitTest,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIPClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Basic unit test for launching a server and connecting a client, while verifying that the correct net driver was used,\n * and that the client connected successfully.\n */" },
		{ "IncludePath", "UnitTests/Engine/IPClient.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/Engine/IPClient.h" },
		{ "ToolTip", "Basic unit test for launching a server and connecting a client, while verifying that the correct net driver was used,\nand that the client connected successfully." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UIPClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UIPClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UIPClient_Statics::ClassParams = {
		&UIPClient::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UIPClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UIPClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UIPClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UIPClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UIPClient, 93398618);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UIPClient>()
	{
		return UIPClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UIPClient(Z_Construct_UClass_UIPClient, &UIPClient::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UIPClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UIPClient);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
