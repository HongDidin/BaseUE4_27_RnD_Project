// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTests/Engine/WebSocketClient.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWebSocketClient() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UWebSocketClient_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UWebSocketClient();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UIPClient();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UWebSocketClient::StaticRegisterNativesUWebSocketClient()
	{
	}
	UClass* Z_Construct_UClass_UWebSocketClient_NoRegister()
	{
		return UWebSocketClient::StaticClass();
	}
	struct Z_Construct_UClass_UWebSocketClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWebSocketClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UIPClient,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWebSocketClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Basic unit test for verifying simple client connection to a server, using the WebSocket net driver.\n */" },
		{ "IncludePath", "UnitTests/Engine/WebSocketClient.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/Engine/WebSocketClient.h" },
		{ "ToolTip", "Basic unit test for verifying simple client connection to a server, using the WebSocket net driver." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWebSocketClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWebSocketClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWebSocketClient_Statics::ClassParams = {
		&UWebSocketClient::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWebSocketClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWebSocketClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWebSocketClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWebSocketClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWebSocketClient, 3433847421);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UWebSocketClient>()
	{
		return UWebSocketClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWebSocketClient(Z_Construct_UClass_UWebSocketClient, &UWebSocketClient::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UWebSocketClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWebSocketClient);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
