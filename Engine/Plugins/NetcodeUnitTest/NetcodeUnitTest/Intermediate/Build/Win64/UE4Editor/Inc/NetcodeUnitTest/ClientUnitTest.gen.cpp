// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/ClientUnitTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeClientUnitTest() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UClientUnitTest_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UClientUnitTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UProcessUnitTest();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UMinimalClient_NoRegister();
// End Cross Module References
	void UClientUnitTest::StaticRegisterNativesUClientUnitTest()
	{
	}
	UClass* Z_Construct_UClass_UClientUnitTest_NoRegister()
	{
		return UClientUnitTest::StaticClass();
	}
	struct Z_Construct_UClass_UClientUnitTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinClient_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MinClient;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UClientUnitTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UProcessUnitTest,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClientUnitTest_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for all unit tests depending upon a MinimalClient connecting to a server.\n * The MinimalClient handles creation/cleanup of an entire new UWorld, UNetDriver and UNetConnection, for fast unit testing.\n *\n * NOTE: See NUTEnum.h, for important flags for configuring unit tests and the minimal client.\n * \n * In subclasses, implement the unit test within the ExecuteClientUnitTest function (remembering to call parent)\n */" },
		{ "IncludePath", "ClientUnitTest.h" },
		{ "ModuleRelativePath", "Classes/ClientUnitTest.h" },
		{ "ToolTip", "Base class for all unit tests depending upon a MinimalClient connecting to a server.\nThe MinimalClient handles creation/cleanup of an entire new UWorld, UNetDriver and UNetConnection, for fast unit testing.\n\nNOTE: See NUTEnum.h, for important flags for configuring unit tests and the minimal client.\n\nIn subclasses, implement the unit test within the ExecuteClientUnitTest function (remembering to call parent)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UClientUnitTest_Statics::NewProp_MinClient_MetaData[] = {
		{ "Comment", "/** The object which handles implementation of the minimal client */" },
		{ "ModuleRelativePath", "Classes/ClientUnitTest.h" },
		{ "ToolTip", "The object which handles implementation of the minimal client" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UClientUnitTest_Statics::NewProp_MinClient = { "MinClient", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UClientUnitTest, MinClient), Z_Construct_UClass_UMinimalClient_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UClientUnitTest_Statics::NewProp_MinClient_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UClientUnitTest_Statics::NewProp_MinClient_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UClientUnitTest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UClientUnitTest_Statics::NewProp_MinClient,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UClientUnitTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UClientUnitTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UClientUnitTest_Statics::ClassParams = {
		&UClientUnitTest::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UClientUnitTest_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UClientUnitTest_Statics::PropPointers),
		0,
		0x001000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UClientUnitTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UClientUnitTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UClientUnitTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UClientUnitTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UClientUnitTest, 1918966734);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UClientUnitTest>()
	{
		return UClientUnitTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UClientUnitTest(Z_Construct_UClass_UClientUnitTest, &UClientUnitTest::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UClientUnitTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UClientUnitTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
