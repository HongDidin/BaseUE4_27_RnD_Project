// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETCODEUNITTEST_ClientUnitTest_generated_h
#error "ClientUnitTest.generated.h already included, missing '#pragma once' in ClientUnitTest.h"
#endif
#define NETCODEUNITTEST_ClientUnitTest_generated_h

#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_SPARSE_DATA
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_RPC_WRAPPERS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUClientUnitTest(); \
	friend struct Z_Construct_UClass_UClientUnitTest_Statics; \
public: \
	DECLARE_CLASS(UClientUnitTest, UProcessUnitTest, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(UClientUnitTest)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUClientUnitTest(); \
	friend struct Z_Construct_UClass_UClientUnitTest_Statics; \
public: \
	DECLARE_CLASS(UClientUnitTest, UProcessUnitTest, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(UClientUnitTest)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UClientUnitTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UClientUnitTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UClientUnitTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UClientUnitTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UClientUnitTest(UClientUnitTest&&); \
	NO_API UClientUnitTest(const UClientUnitTest&); \
public:


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UClientUnitTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UClientUnitTest(UClientUnitTest&&); \
	NO_API UClientUnitTest(const UClientUnitTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UClientUnitTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UClientUnitTest); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UClientUnitTest)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MinClient() { return STRUCT_OFFSET(UClientUnitTest, MinClient); }


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_43_PROLOG
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_RPC_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_INCLASS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h_46_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ClientUnitTest."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETCODEUNITTEST_API UClass* StaticClass<class UClientUnitTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_ClientUnitTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
