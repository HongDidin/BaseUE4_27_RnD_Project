// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/Net/UnitTestChannel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnitTestChannel() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestChannel_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestChannel();
	ENGINE_API UClass* Z_Construct_UClass_UChannel();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UUnitTestChannel::StaticRegisterNativesUUnitTestChannel()
	{
	}
	UClass* Z_Construct_UClass_UUnitTestChannel_NoRegister()
	{
		return UUnitTestChannel::StaticClass();
	}
	struct Z_Construct_UClass_UUnitTestChannel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnitTestChannel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UChannel,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTestChannel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A net channel for overriding the implementation of traditional net channels,\n * for e.g. blocking control channel messages, to enable minimal clients\n */" },
		{ "IncludePath", "Net/UnitTestChannel.h" },
		{ "ModuleRelativePath", "Classes/Net/UnitTestChannel.h" },
		{ "ToolTip", "A net channel for overriding the implementation of traditional net channels,\nfor e.g. blocking control channel messages, to enable minimal clients" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnitTestChannel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnitTestChannel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnitTestChannel_Statics::ClassParams = {
		&UUnitTestChannel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUnitTestChannel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTestChannel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnitTestChannel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnitTestChannel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitTestChannel, 2209823257);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UUnitTestChannel>()
	{
		return UUnitTestChannel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitTestChannel(Z_Construct_UClass_UUnitTestChannel, &UUnitTestChannel::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UUnitTestChannel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitTestChannel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
