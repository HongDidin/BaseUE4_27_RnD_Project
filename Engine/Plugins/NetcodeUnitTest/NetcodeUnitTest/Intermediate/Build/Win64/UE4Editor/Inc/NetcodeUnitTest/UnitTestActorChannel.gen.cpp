// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/Net/UnitTestActorChannel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnitTestActorChannel() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestActorChannel_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestActorChannel();
	ENGINE_API UClass* Z_Construct_UClass_UActorChannel();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UUnitTestActorChannel::StaticRegisterNativesUUnitTestActorChannel()
	{
	}
	UClass* Z_Construct_UClass_UUnitTestActorChannel_NoRegister()
	{
		return UUnitTestActorChannel::StaticClass();
	}
	struct Z_Construct_UClass_UUnitTestActorChannel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnitTestActorChannel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorChannel,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTestActorChannel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * An actor net channel override, for hooking ReceivedBunch, to aid in detecting/blocking of remote actors, of a specific class\n */" },
		{ "IncludePath", "Net/UnitTestActorChannel.h" },
		{ "ModuleRelativePath", "Classes/Net/UnitTestActorChannel.h" },
		{ "ToolTip", "An actor net channel override, for hooking ReceivedBunch, to aid in detecting/blocking of remote actors, of a specific class" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnitTestActorChannel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnitTestActorChannel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnitTestActorChannel_Statics::ClassParams = {
		&UUnitTestActorChannel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUnitTestActorChannel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTestActorChannel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnitTestActorChannel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnitTestActorChannel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitTestActorChannel, 235265093);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UUnitTestActorChannel>()
	{
		return UUnitTestActorChannel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitTestActorChannel(Z_Construct_UClass_UUnitTestActorChannel, &UUnitTestActorChannel::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UUnitTestActorChannel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitTestActorChannel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
