// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/MinimalClient.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinimalClient() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UMinimalClient_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UMinimalClient();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UMinimalClient::StaticRegisterNativesUMinimalClient()
	{
	}
	UClass* Z_Construct_UClass_UMinimalClient_NoRegister()
	{
		return UMinimalClient::StaticClass();
	}
	struct Z_Construct_UClass_UMinimalClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMinimalClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinimalClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for implementing a barebones/stripped-down game client, capable of connecting to a regular game server,\n * but stripped/locked-down so that the absolute minimum of client/server netcode functionality is executed, for connecting the client.\n */" },
		{ "IncludePath", "MinimalClient.h" },
		{ "ModuleRelativePath", "Classes/MinimalClient.h" },
		{ "ToolTip", "Base class for implementing a barebones/stripped-down game client, capable of connecting to a regular game server,\nbut stripped/locked-down so that the absolute minimum of client/server netcode functionality is executed, for connecting the client." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMinimalClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMinimalClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMinimalClient_Statics::ClassParams = {
		&UMinimalClient::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMinimalClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMinimalClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMinimalClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMinimalClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMinimalClient, 4032829762);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UMinimalClient>()
	{
		return UMinimalClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMinimalClient(Z_Construct_UClass_UMinimalClient, &UMinimalClient::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UMinimalClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMinimalClient);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
