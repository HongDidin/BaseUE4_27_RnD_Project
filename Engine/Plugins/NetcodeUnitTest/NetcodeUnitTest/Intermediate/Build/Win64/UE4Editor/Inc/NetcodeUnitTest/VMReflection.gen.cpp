// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTests/VMReflection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVMReflection() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UVMReflection_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UVMReflection();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTest();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UVMTestClassA_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UVMTestClassA();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_APawn_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UVMTestClassB_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UVMTestClassB();
// End Cross Module References
	void UVMReflection::StaticRegisterNativesUVMReflection()
	{
	}
	UClass* Z_Construct_UClass_UVMReflection_NoRegister()
	{
		return UVMReflection::StaticClass();
	}
	struct Z_Construct_UClass_UVMReflection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVMReflection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUnitTest,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMReflection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Internal unit test for verifying the functionality of the UScript/BP VM reflection helper\n */" },
		{ "IncludePath", "UnitTests/VMReflection.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
		{ "ToolTip", "Internal unit test for verifying the functionality of the UScript/BP VM reflection helper" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVMReflection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVMReflection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVMReflection_Statics::ClassParams = {
		&UVMReflection::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVMReflection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVMReflection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVMReflection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVMReflection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVMReflection, 972252248);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UVMReflection>()
	{
		return UVMReflection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVMReflection(Z_Construct_UClass_UVMReflection, &UVMReflection::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UVMReflection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVMReflection);
	void UVMTestClassA::StaticRegisterNativesUVMTestClassA()
	{
	}
	UClass* Z_Construct_UClass_UVMTestClassA_NoRegister()
	{
		return UVMTestClassA::StaticClass();
	}
	struct Z_Construct_UClass_UVMTestClassA_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AObjectRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AObjectRef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UInt16Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_UInt16Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UInt32Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_UInt32Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UInt64Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_UInt64Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int16Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Int16Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int64Prop_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Int64Prop;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DoubleProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoolPropA_MetaData[];
#endif
		static void NewProp_bBoolPropA_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoolPropA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoolPropB_MetaData[];
#endif
		static void NewProp_bBoolPropB_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoolPropB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoolPropC_MetaData[];
#endif
		static void NewProp_bBoolPropC_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoolPropC;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoolPropD_MetaData[];
#endif
		static void NewProp_bBoolPropD_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoolPropD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBoolPropE_MetaData[];
#endif
		static void NewProp_bBoolPropE_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBoolPropE;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BytePropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BytePropArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectPropArray;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DynBytePropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynBytePropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynBytePropArray;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DynBoolPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynBoolPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynBoolPropArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynObjectPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynObjectPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynObjectPropArray;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DynNamePropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynNamePropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynNamePropArray;
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DynDoublePropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynDoublePropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynDoublePropArray;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DynFloatPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynFloatPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynFloatPropArray;
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_DynInt16PropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynInt16PropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynInt16PropArray;
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_DynInt64PropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynInt64PropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynInt64PropArray;
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_DynInt8PropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynInt8PropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynInt8PropArray;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DynIntPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynIntPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynIntPropArray;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_DynUInt16PropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynUInt16PropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynUInt16PropArray;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_DynUIntPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynUIntPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynUIntPropArray;
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_DynUInt64PropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynUInt64PropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynUInt64PropArray;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DynStringPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynStringPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynStringPropArray;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DynTextPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynTextPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynTextPropArray;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DynClassPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynClassPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynClassPropArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynPawnPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynPawnPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynPawnPropArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructPropArray;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DynStructPropArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynStructPropArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DynStructPropArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVMTestClassA_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Test classes for testing different types/combinations of property reflection\n */" },
		{ "IncludePath", "UnitTests/VMReflection.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
		{ "ToolTip", "Test classes for testing different types/combinations of property reflection" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_AObjectRef_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_AObjectRef = { "AObjectRef", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, AObjectRef), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_AObjectRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_AObjectRef_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ByteProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ByteProp = { "ByteProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, ByteProp), nullptr, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ByteProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ByteProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt16Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt16Prop = { "UInt16Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, UInt16Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt16Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt16Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt32Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt32Prop = { "UInt32Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, UInt32Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt32Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt32Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt64Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt64Prop = { "UInt64Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, UInt64Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt64Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt64Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int8Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int8Prop = { "Int8Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, Int8Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int8Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int8Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int16Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int16Prop = { "Int16Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, Int16Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int16Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int16Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int32Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int32Prop = { "Int32Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, Int32Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int32Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int32Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int64Prop_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int64Prop = { "Int64Prop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, Int64Prop), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int64Prop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int64Prop_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_FloatProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_FloatProp = { "FloatProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, FloatProp), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_FloatProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_FloatProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DoubleProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DoubleProp = { "DoubleProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DoubleProp), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DoubleProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DoubleProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	void Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA_SetBit(void* Obj)
	{
		((UVMTestClassA*)Obj)->bBoolPropA = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA = { "bBoolPropA", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVMTestClassA), &Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	void Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB_SetBit(void* Obj)
	{
		((UVMTestClassA*)Obj)->bBoolPropB = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB = { "bBoolPropB", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVMTestClassA), &Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	void Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC_SetBit(void* Obj)
	{
		((UVMTestClassA*)Obj)->bBoolPropC = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC = { "bBoolPropC", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVMTestClassA), &Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	void Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD_SetBit(void* Obj)
	{
		((UVMTestClassA*)Obj)->bBoolPropD = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD = { "bBoolPropD", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVMTestClassA), &Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	void Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE_SetBit(void* Obj)
	{
		((UVMTestClassA*)Obj)->bBoolPropE = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE = { "bBoolPropE", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVMTestClassA), &Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_NameProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_NameProp = { "NameProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, NameProp), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_NameProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_NameProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StringProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StringProp = { "StringProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, StringProp), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StringProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StringProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_TextProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_TextProp = { "TextProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, TextProp), METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_TextProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_TextProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_BytePropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_BytePropArray = { "BytePropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(BytePropArray, UVMTestClassA), STRUCT_OFFSET(UVMTestClassA, BytePropArray), nullptr, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_BytePropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_BytePropArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ObjectPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ObjectPropArray = { "ObjectPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(ObjectPropArray, UVMTestClassA), STRUCT_OFFSET(UVMTestClassA, ObjectPropArray), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ObjectPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ObjectPropArray_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray_Inner = { "DynBytePropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray = { "DynBytePropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynBytePropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray_Inner = { "DynBoolPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray = { "DynBoolPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynBoolPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray_Inner = { "DynObjectPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray = { "DynObjectPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynObjectPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray_Inner = { "DynNamePropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray = { "DynNamePropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynNamePropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray_MetaData)) };
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray_Inner = { "DynDoublePropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray = { "DynDoublePropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynDoublePropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray_Inner = { "DynFloatPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray = { "DynFloatPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynFloatPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray_MetaData)) };
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray_Inner = { "DynInt16PropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray = { "DynInt16PropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynInt16PropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray_MetaData)) };
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray_Inner = { "DynInt64PropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray = { "DynInt64PropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynInt64PropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray_MetaData)) };
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray_Inner = { "DynInt8PropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray = { "DynInt8PropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynInt8PropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray_Inner = { "DynIntPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray = { "DynIntPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynIntPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray_Inner = { "DynUInt16PropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray = { "DynUInt16PropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynUInt16PropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray_Inner = { "DynUIntPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray = { "DynUIntPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynUIntPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray_MetaData)) };
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray_Inner = { "DynUInt64PropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray = { "DynUInt64PropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynUInt64PropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray_Inner = { "DynStringPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray = { "DynStringPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynStringPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray_Inner = { "DynTextPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray = { "DynTextPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynTextPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray_Inner = { "DynClassPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray = { "DynClassPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynClassPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray_Inner = { "DynPawnPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_APawn_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray = { "DynPawnPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynPawnPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructProp_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructProp = { "StructProp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, StructProp), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructProp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructPropArray = { "StructPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(StructPropArray, UVMTestClassA), STRUCT_OFFSET(UVMTestClassA, StructPropArray), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructPropArray_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray_Inner = { "DynStructPropArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray = { "DynStructPropArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassA, DynStructPropArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVMTestClassA_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_AObjectRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ByteProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt16Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt32Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_UInt64Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int8Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int16Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int32Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_Int64Prop,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_FloatProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DoubleProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropC,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_bBoolPropE,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_NameProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StringProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_TextProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_BytePropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_ObjectPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBytePropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynBoolPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynObjectPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynNamePropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynDoublePropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynFloatPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt16PropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt64PropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynInt8PropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynIntPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt16PropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUIntPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynUInt64PropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStringPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynTextPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynClassPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynPawnPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_StructPropArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassA_Statics::NewProp_DynStructPropArray,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVMTestClassA_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVMTestClassA>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVMTestClassA_Statics::ClassParams = {
		&UVMTestClassA::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVMTestClassA_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVMTestClassA_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassA_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVMTestClassA()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVMTestClassA_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVMTestClassA, 3803136941);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UVMTestClassA>()
	{
		return UVMTestClassA::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVMTestClassA(Z_Construct_UClass_UVMTestClassA, &UVMTestClassA::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UVMTestClassA"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVMTestClassA);
	void UVMTestClassB::StaticRegisterNativesUVMTestClassB()
	{
	}
	UClass* Z_Construct_UClass_UVMTestClassB_NoRegister()
	{
		return UVMTestClassB::StaticClass();
	}
	struct Z_Construct_UClass_UVMTestClassB_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BObjectRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BObjectRef;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVMTestClassB_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassB_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "UnitTests/VMReflection.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVMTestClassB_Statics::NewProp_BObjectRef_MetaData[] = {
		{ "ModuleRelativePath", "Classes/UnitTests/VMReflection.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVMTestClassB_Statics::NewProp_BObjectRef = { "BObjectRef", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVMTestClassB, BObjectRef), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVMTestClassB_Statics::NewProp_BObjectRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassB_Statics::NewProp_BObjectRef_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVMTestClassB_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVMTestClassB_Statics::NewProp_BObjectRef,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVMTestClassB_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVMTestClassB>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVMTestClassB_Statics::ClassParams = {
		&UVMTestClassB::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVMTestClassB_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassB_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVMTestClassB_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVMTestClassB_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVMTestClassB()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVMTestClassB_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVMTestClassB, 338200737);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UVMTestClassB>()
	{
		return UVMTestClassB::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVMTestClassB(Z_Construct_UClass_UVMTestClassB, &UVMTestClassB::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UVMTestClassB"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVMTestClassB);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
