// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NETCODEUNITTEST_UnitTestPackageMap_generated_h
#error "UnitTestPackageMap.generated.h already included, missing '#pragma once' in UnitTestPackageMap.h"
#endif
#define NETCODEUNITTEST_UnitTestPackageMap_generated_h

#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_SPARSE_DATA
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_RPC_WRAPPERS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUnitTestPackageMap(); \
	friend struct Z_Construct_UClass_UUnitTestPackageMap_Statics; \
public: \
	DECLARE_CLASS(UUnitTestPackageMap, UPackageMapClient, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(UUnitTestPackageMap)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUUnitTestPackageMap(); \
	friend struct Z_Construct_UClass_UUnitTestPackageMap_Statics; \
public: \
	DECLARE_CLASS(UUnitTestPackageMap, UPackageMapClient, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(UUnitTestPackageMap)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnitTestPackageMap(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnitTestPackageMap) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnitTestPackageMap); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnitTestPackageMap); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnitTestPackageMap(UUnitTestPackageMap&&); \
	NO_API UUnitTestPackageMap(const UUnitTestPackageMap&); \
public:


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnitTestPackageMap(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnitTestPackageMap(UUnitTestPackageMap&&); \
	NO_API UUnitTestPackageMap(const UUnitTestPackageMap&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnitTestPackageMap); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnitTestPackageMap); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnitTestPackageMap)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_34_PROLOG
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_RPC_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_INCLASS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h_37_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UnitTestPackageMap."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETCODEUNITTEST_API UClass* StaticClass<class UUnitTestPackageMap>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_Net_UnitTestPackageMap_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
