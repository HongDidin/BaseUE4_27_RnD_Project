// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/ProcessUnitTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProcessUnitTest() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UProcessUnitTest_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UProcessUnitTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTest();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UProcessUnitTest::StaticRegisterNativesUProcessUnitTest()
	{
	}
	UClass* Z_Construct_UClass_UProcessUnitTest_NoRegister()
	{
		return UProcessUnitTest::StaticClass();
	}
	struct Z_Construct_UClass_UProcessUnitTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UProcessUnitTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUnitTest,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UProcessUnitTest_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for all unit tests which launch child processes, whether they be UE4 child processes, or other arbitrary programs.\n *\n * Handles management of child processes, memory usage tracking, log/stdout output gathering/printing, and crash detection.\n */" },
		{ "IncludePath", "ProcessUnitTest.h" },
		{ "ModuleRelativePath", "Classes/ProcessUnitTest.h" },
		{ "ToolTip", "Base class for all unit tests which launch child processes, whether they be UE4 child processes, or other arbitrary programs.\n\nHandles management of child processes, memory usage tracking, log/stdout output gathering/printing, and crash detection." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UProcessUnitTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UProcessUnitTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UProcessUnitTest_Statics::ClassParams = {
		&UProcessUnitTest::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UProcessUnitTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UProcessUnitTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UProcessUnitTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UProcessUnitTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UProcessUnitTest, 2309349481);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UProcessUnitTest>()
	{
		return UProcessUnitTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UProcessUnitTest(Z_Construct_UClass_UProcessUnitTest, &UProcessUnitTest::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UProcessUnitTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UProcessUnitTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
