// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/Net/UnitTestPackageMap.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnitTestPackageMap() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestPackageMap_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestPackageMap();
	ENGINE_API UClass* Z_Construct_UClass_UPackageMapClient();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UUnitTestPackageMap::StaticRegisterNativesUUnitTestPackageMap()
	{
	}
	UClass* Z_Construct_UClass_UUnitTestPackageMap_NoRegister()
	{
		return UUnitTestPackageMap::StaticClass();
	}
	struct Z_Construct_UClass_UUnitTestPackageMap_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnitTestPackageMap_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPackageMapClient,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTestPackageMap_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Package map override, for blocking the creation of actor channels for specific actors (by detecting the actor class being created)\n */" },
		{ "IncludePath", "Net/UnitTestPackageMap.h" },
		{ "ModuleRelativePath", "Classes/Net/UnitTestPackageMap.h" },
		{ "ToolTip", "Package map override, for blocking the creation of actor channels for specific actors (by detecting the actor class being created)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnitTestPackageMap_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnitTestPackageMap>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnitTestPackageMap_Statics::ClassParams = {
		&UUnitTestPackageMap::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUnitTestPackageMap_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTestPackageMap_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnitTestPackageMap()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnitTestPackageMap_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitTestPackageMap, 1133917531);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UUnitTestPackageMap>()
	{
		return UUnitTestPackageMap::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitTestPackageMap(Z_Construct_UClass_UUnitTestPackageMap, &UUnitTestPackageMap::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UUnitTestPackageMap"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitTestPackageMap);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
