// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTask.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnitTask() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTask_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTask();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void UUnitTask::StaticRegisterNativesUUnitTask()
	{
	}
	UClass* Z_Construct_UClass_UUnitTask_NoRegister()
	{
		return UUnitTask::StaticClass();
	}
	struct Z_Construct_UClass_UUnitTask_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnitTask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTask_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UnitTask's are used to implement supporting code for UnitTest's, for handling complex behind-the-scenes setup prior to test execution\n * (e.g. primarily for implementing game-specific server/client environment setup), which is shared between many unit tests,\n * and which is better to abstract-away from visibility in unit tests themselves, for code clarity.\n *\n * For example, some games require authentication negotiation before the game client can connect to a server,\n * and this is the type of task that UnitTask's are designed for.\n *\n * Unit tasks that are added to a unit test, must complete execution before the unit test itself can execute.\n */" },
		{ "IncludePath", "UnitTask.h" },
		{ "ModuleRelativePath", "Classes/UnitTask.h" },
		{ "ToolTip", "UnitTask's are used to implement supporting code for UnitTest's, for handling complex behind-the-scenes setup prior to test execution\n(e.g. primarily for implementing game-specific server/client environment setup), which is shared between many unit tests,\nand which is better to abstract-away from visibility in unit tests themselves, for code clarity.\n\nFor example, some games require authentication negotiation before the game client can connect to a server,\nand this is the type of task that UnitTask's are designed for.\n\nUnit tasks that are added to a unit test, must complete execution before the unit test itself can execute." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnitTask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnitTask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnitTask_Statics::ClassParams = {
		&UUnitTask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UUnitTask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnitTask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnitTask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitTask, 3928594762);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UUnitTask>()
	{
		return UUnitTask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitTask(Z_Construct_UClass_UUnitTask, &UUnitTask::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UUnitTask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitTask);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
