// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTests/Engine/SteamClient.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSteamClient() {}
// Cross Module References
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_USteamClient_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_USteamClient();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UIPClient();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
// End Cross Module References
	void USteamClient::StaticRegisterNativesUSteamClient()
	{
	}
	UClass* Z_Construct_UClass_USteamClient_NoRegister()
	{
		return USteamClient::StaticClass();
	}
	struct Z_Construct_UClass_USteamClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USteamClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UIPClient,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USteamClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Basic unit test for verifying simple client connection to a server, using the Steam net driver.\n */" },
		{ "IncludePath", "UnitTests/Engine/SteamClient.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/Engine/SteamClient.h" },
		{ "ToolTip", "Basic unit test for verifying simple client connection to a server, using the Steam net driver." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USteamClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USteamClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USteamClient_Statics::ClassParams = {
		&USteamClient::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USteamClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USteamClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USteamClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USteamClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USteamClient, 23077880);
	template<> NETCODEUNITTEST_API UClass* StaticClass<USteamClient>()
	{
		return USteamClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USteamClient(Z_Construct_UClass_USteamClient, &USteamClient::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("USteamClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USteamClient);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
