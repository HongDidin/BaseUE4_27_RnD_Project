// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/NUTActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNUTActor() {}
// Cross Module References
	NETCODEUNITTEST_API UFunction* Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_ANUTActor_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_ANUTActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics
	{
		struct _Script_NetcodeUnitTest_eventExecuteOnServer_Parms
		{
			ANUTActor* InNUTActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InNUTActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::NewProp_InNUTActor = { "InNUTActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_NetcodeUnitTest_eventExecuteOnServer_Parms, InNUTActor), Z_Construct_UClass_ANUTActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::NewProp_InNUTActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * Delegate for executing a unit test function on the server\n *\n * @param InNUTActor\x09The serverside NUTActor triggering the delegate\n */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Delegate for executing a unit test function on the server\n\n@param InNUTActor   The serverside NUTActor triggering the delegate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_NetcodeUnitTest, nullptr, "ExecuteOnServer__DelegateSignature", nullptr, nullptr, sizeof(_Script_NetcodeUnitTest_eventExecuteOnServer_Parms), Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ANUTActor::execServerExecute)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->ServerExecute_Validate(Z_Param_InDelegate))
		{
			RPC_ValidateFailed(TEXT("ServerExecute_Validate"));
			return;
		}
		P_THIS->ServerExecute_Implementation(Z_Param_InDelegate);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execNetMulticastPing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NetMulticastPing_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execServerClientPing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->ServerClientPing_Validate())
		{
			RPC_ValidateFailed(TEXT("ServerClientPing_Validate"));
			return;
		}
		P_THIS->ServerClientPing_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execServerReceiveText)
	{
		P_GET_PROPERTY(FTextProperty,Z_Param_InText);
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->ServerReceiveText_Validate(Z_Param_InText))
		{
			RPC_ValidateFailed(TEXT("ServerReceiveText_Validate"));
			return;
		}
		P_THIS->ServerReceiveText_Implementation(Z_Param_InText);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execServerClientStillAlive)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->ServerClientStillAlive_Validate())
		{
			RPC_ValidateFailed(TEXT("ServerClientStillAlive_Validate"));
			return;
		}
		P_THIS->ServerClientStillAlive_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execWait)
	{
		P_GET_PROPERTY(FUInt16Property,Z_Param_Seconds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Wait(Z_Param_Seconds);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execNetFlush)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NetFlush();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execUnitTravel)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Dest);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UnitTravel(Z_Param_Dest);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execUnitSeamlessTravel)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Dest);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UnitSeamlessTravel(Z_Param_Dest);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execServerAdmin)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Command);
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->ServerAdmin_Validate(Z_Param_Command))
		{
			RPC_ValidateFailed(TEXT("ServerAdmin_Validate"));
			return;
		}
		P_THIS->ServerAdmin_Implementation(Z_Param_Command);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANUTActor::execAdmin)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Command);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Admin(Z_Param_Command);
		P_NATIVE_END;
	}
	static FName NAME_ANUTActor_NetMulticastPing = FName(TEXT("NetMulticastPing"));
	void ANUTActor::NetMulticastPing()
	{
		ProcessEvent(FindFunctionChecked(NAME_ANUTActor_NetMulticastPing),NULL);
	}
	static FName NAME_ANUTActor_ServerAdmin = FName(TEXT("ServerAdmin"));
	void ANUTActor::ServerAdmin(const FString& Command)
	{
		NUTActor_eventServerAdmin_Parms Parms;
		Parms.Command=Command;
		ProcessEvent(FindFunctionChecked(NAME_ANUTActor_ServerAdmin),&Parms);
	}
	static FName NAME_ANUTActor_ServerClientPing = FName(TEXT("ServerClientPing"));
	void ANUTActor::ServerClientPing()
	{
		ProcessEvent(FindFunctionChecked(NAME_ANUTActor_ServerClientPing),NULL);
	}
	static FName NAME_ANUTActor_ServerClientStillAlive = FName(TEXT("ServerClientStillAlive"));
	void ANUTActor::ServerClientStillAlive()
	{
		ProcessEvent(FindFunctionChecked(NAME_ANUTActor_ServerClientStillAlive),NULL);
	}
	static FName NAME_ANUTActor_ServerExecute = FName(TEXT("ServerExecute"));
	void ANUTActor::ServerExecute(const FString& InDelegate)
	{
		NUTActor_eventServerExecute_Parms Parms;
		Parms.InDelegate=InDelegate;
		ProcessEvent(FindFunctionChecked(NAME_ANUTActor_ServerExecute),&Parms);
	}
	static FName NAME_ANUTActor_ServerReceiveText = FName(TEXT("ServerReceiveText"));
	void ANUTActor::ServerReceiveText(FText const& InText)
	{
		NUTActor_eventServerReceiveText_Parms Parms;
		Parms.InText=InText;
		ProcessEvent(FindFunctionChecked(NAME_ANUTActor_ServerReceiveText),&Parms);
	}
	void ANUTActor::StaticRegisterNativesANUTActor()
	{
		UClass* Class = ANUTActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Admin", &ANUTActor::execAdmin },
			{ "NetFlush", &ANUTActor::execNetFlush },
			{ "NetMulticastPing", &ANUTActor::execNetMulticastPing },
			{ "ServerAdmin", &ANUTActor::execServerAdmin },
			{ "ServerClientPing", &ANUTActor::execServerClientPing },
			{ "ServerClientStillAlive", &ANUTActor::execServerClientStillAlive },
			{ "ServerExecute", &ANUTActor::execServerExecute },
			{ "ServerReceiveText", &ANUTActor::execServerReceiveText },
			{ "UnitSeamlessTravel", &ANUTActor::execUnitSeamlessTravel },
			{ "UnitTravel", &ANUTActor::execUnitTravel },
			{ "Wait", &ANUTActor::execWait },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANUTActor_Admin_Statics
	{
		struct NUTActor_eventAdmin_Parms
		{
			FString Command;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Command;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ANUTActor_Admin_Statics::NewProp_Command = { "Command", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventAdmin_Parms, Command), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_Admin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_Admin_Statics::NewProp_Command,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_Admin_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Executes a console command on the server\n\x09 *\n\x09 * @param Command\x09The command to be executed\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Executes a console command on the server\n\n@param Command       The command to be executed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_Admin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "Admin", nullptr, nullptr, sizeof(NUTActor_eventAdmin_Parms), Z_Construct_UFunction_ANUTActor_Admin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_Admin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_Admin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_Admin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_Admin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_Admin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_NetFlush_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_NetFlush_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Flushes all pending net connection packets\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Flushes all pending net connection packets" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_NetFlush_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "NetFlush", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_NetFlush_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_NetFlush_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_NetFlush()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_NetFlush_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_NetMulticastPing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_NetMulticastPing_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Received by all clients, emits a ping to log\n\x09 */// @todo #JohnBRefactor: When the VM reflection helper is finished, remove NETCODEUNITTEST_API from this, and use reflection instead\n" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Received by all clients, emits a ping to log\n        // @todo #JohnBRefactor: When the VM reflection helper is finished, remove NETCODEUNITTEST_API from this, and use reflection instead" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_NetMulticastPing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "NetMulticastPing", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00024CC2, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_NetMulticastPing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_NetMulticastPing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_NetMulticastPing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_NetMulticastPing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Command_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Command;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::NewProp_Command_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::NewProp_Command = { "Command", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventServerAdmin_Parms, Command), METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::NewProp_Command_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::NewProp_Command_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::NewProp_Command,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "ServerAdmin", nullptr, nullptr, sizeof(NUTActor_eventServerAdmin_Parms), Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_ServerAdmin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_ServerAdmin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_ServerClientPing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerClientPing_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Send a 'ping' RPC to all clients, to make them log a ping, which unit tests then use to verify the presence of a client process\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Send a 'ping' RPC to all clients, to make them log a ping, which unit tests then use to verify the presence of a client process" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_ServerClientPing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "ServerClientPing", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80220CC2, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerClientPing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerClientPing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_ServerClientPing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_ServerClientPing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_ServerClientStillAlive_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerClientStillAlive_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Notifies the server that the client is still around\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Notifies the server that the client is still around" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_ServerClientStillAlive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "ServerClientStillAlive", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerClientStillAlive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerClientStillAlive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_ServerClientStillAlive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_ServerClientStillAlive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_ServerExecute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::NewProp_InDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::NewProp_InDelegate = { "InDelegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventServerExecute_Parms, InDelegate), METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::NewProp_InDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::NewProp_InDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::NewProp_InDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Takes a string representing a delegate (can't replicate delegate parameters), and executes that delegate on the server\n\x09 *\n\x09 * @param InDelegate\x09""A string representing the delegate to be executed\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Takes a string representing a delegate (can't replicate delegate parameters), and executes that delegate on the server\n\n@param InDelegate    A string representing the delegate to be executed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "ServerExecute", nullptr, nullptr, sizeof(NUTActor_eventServerExecute_Parms), Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80220CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_ServerExecute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_ServerExecute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_InText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::NewProp_InText_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::NewProp_InText = { "InText", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventServerReceiveText_Parms, InText), METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::NewProp_InText_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::NewProp_InText_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::NewProp_InText,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Test log function\n\x09 *\n\x09 * @param InText\x09The FText passed from the client\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Test log function\n\n@param InText        The FText passed from the client" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "ServerReceiveText", nullptr, nullptr, sizeof(NUTActor_eventServerReceiveText_Parms), Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x80220CC2, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_ServerReceiveText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_ServerReceiveText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics
	{
		struct NUTActor_eventUnitSeamlessTravel_Parms
		{
			FString Dest;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Dest;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::NewProp_Dest = { "Dest", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventUnitSeamlessTravel_Parms, Dest), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::NewProp_Dest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Triggers seamless travel\n\x09 *\n\x09 * @param Dest\x09The travel destination\n\x09 */" },
		{ "CPP_Default_Dest", " " },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Triggers seamless travel\n\n@param Dest  The travel destination" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "UnitSeamlessTravel", nullptr, nullptr, sizeof(NUTActor_eventUnitSeamlessTravel_Parms), Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_UnitTravel_Statics
	{
		struct NUTActor_eventUnitTravel_Parms
		{
			FString Dest;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Dest;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::NewProp_Dest = { "Dest", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventUnitTravel_Parms, Dest), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::NewProp_Dest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Triggers normal travel\n\x09 *\n\x09 * @param Dest\x09The travel destination\n\x09 */" },
		{ "CPP_Default_Dest", " " },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Triggers normal travel\n\n@param Dest  The travel destination" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "UnitTravel", nullptr, nullptr, sizeof(NUTActor_eventUnitTravel_Parms), Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_UnitTravel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_UnitTravel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANUTActor_Wait_Statics
	{
		struct NUTActor_eventWait_Parms
		{
			uint16 Seconds;
		};
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Seconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UFunction_ANUTActor_Wait_Statics::NewProp_Seconds = { "Seconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NUTActor_eventWait_Parms, Seconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANUTActor_Wait_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANUTActor_Wait_Statics::NewProp_Seconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANUTActor_Wait_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Makes the game thread wait for the specified number of seconds\n\x09 */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "Makes the game thread wait for the specified number of seconds" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANUTActor_Wait_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANUTActor, nullptr, "Wait", nullptr, nullptr, sizeof(NUTActor_eventWait_Parms), Z_Construct_UFunction_ANUTActor_Wait_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_Wait_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANUTActor_Wait_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANUTActor_Wait_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANUTActor_Wait()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANUTActor_Wait_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANUTActor_NoRegister()
	{
		return ANUTActor::StaticClass();
	}
	struct Z_Construct_UClass_ANUTActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TempDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_TempDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANUTActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANUTActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANUTActor_Admin, "Admin" }, // 3529064211
		{ &Z_Construct_UFunction_ANUTActor_NetFlush, "NetFlush" }, // 1741918587
		{ &Z_Construct_UFunction_ANUTActor_NetMulticastPing, "NetMulticastPing" }, // 2273161561
		{ &Z_Construct_UFunction_ANUTActor_ServerAdmin, "ServerAdmin" }, // 2163599443
		{ &Z_Construct_UFunction_ANUTActor_ServerClientPing, "ServerClientPing" }, // 675673184
		{ &Z_Construct_UFunction_ANUTActor_ServerClientStillAlive, "ServerClientStillAlive" }, // 3602866292
		{ &Z_Construct_UFunction_ANUTActor_ServerExecute, "ServerExecute" }, // 1969216099
		{ &Z_Construct_UFunction_ANUTActor_ServerReceiveText, "ServerReceiveText" }, // 2564775447
		{ &Z_Construct_UFunction_ANUTActor_UnitSeamlessTravel, "UnitSeamlessTravel" }, // 2728049423
		{ &Z_Construct_UFunction_ANUTActor_UnitTravel, "UnitTravel" }, // 2539714710
		{ &Z_Construct_UFunction_ANUTActor_Wait, "Wait" }, // 1409332044
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANUTActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NUTActor.h" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANUTActor_Statics::NewProp_TempDelegate_MetaData[] = {
		{ "Comment", "/** A delegate property, used solely for converting strings to delegates */" },
		{ "ModuleRelativePath", "Classes/NUTActor.h" },
		{ "ToolTip", "A delegate property, used solely for converting strings to delegates" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_ANUTActor_Statics::NewProp_TempDelegate = { "TempDelegate", nullptr, (EPropertyFlags)0x0010000000080000, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANUTActor, TempDelegate), Z_Construct_UDelegateFunction_NetcodeUnitTest_ExecuteOnServer__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ANUTActor_Statics::NewProp_TempDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANUTActor_Statics::NewProp_TempDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANUTActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANUTActor_Statics::NewProp_TempDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANUTActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANUTActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANUTActor_Statics::ClassParams = {
		&ANUTActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ANUTActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ANUTActor_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANUTActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANUTActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANUTActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANUTActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANUTActor, 466799467);
	template<> NETCODEUNITTEST_API UClass* StaticClass<ANUTActor>()
	{
		return ANUTActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANUTActor(Z_Construct_UClass_ANUTActor, &ANUTActor::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("ANUTActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANUTActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
