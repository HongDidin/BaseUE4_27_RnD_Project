// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ANUTActor;
#ifdef NETCODEUNITTEST_NUTActor_generated_h
#error "NUTActor.generated.h already included, missing '#pragma once' in NUTActor.h"
#endif
#define NETCODEUNITTEST_NUTActor_generated_h

#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_44_DELEGATE \
struct _Script_NetcodeUnitTest_eventExecuteOnServer_Parms \
{ \
	ANUTActor* InNUTActor; \
}; \
static inline void FExecuteOnServer_DelegateWrapper(const FScriptDelegate& ExecuteOnServer, ANUTActor* InNUTActor) \
{ \
	_Script_NetcodeUnitTest_eventExecuteOnServer_Parms Parms; \
	Parms.InNUTActor=InNUTActor; \
	ExecuteOnServer.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_SPARSE_DATA
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_RPC_WRAPPERS \
	virtual bool ServerExecute_Validate(const FString& ); \
	virtual void ServerExecute_Implementation(const FString& InDelegate); \
	NETCODEUNITTEST_API virtual void NetMulticastPing_Implementation(); \
	virtual bool ServerClientPing_Validate(); \
	NETCODEUNITTEST_API virtual void ServerClientPing_Implementation(); \
	virtual bool ServerReceiveText_Validate(FText const& ); \
	NETCODEUNITTEST_API virtual void ServerReceiveText_Implementation(FText const& InText); \
	virtual bool ServerClientStillAlive_Validate(); \
	virtual void ServerClientStillAlive_Implementation(); \
	virtual bool ServerAdmin_Validate(const FString& ); \
	virtual void ServerAdmin_Implementation(const FString& Command); \
 \
	DECLARE_FUNCTION(execServerExecute); \
	DECLARE_FUNCTION(execNetMulticastPing); \
	DECLARE_FUNCTION(execServerClientPing); \
	DECLARE_FUNCTION(execServerReceiveText); \
	DECLARE_FUNCTION(execServerClientStillAlive); \
	DECLARE_FUNCTION(execWait); \
	DECLARE_FUNCTION(execNetFlush); \
	DECLARE_FUNCTION(execUnitTravel); \
	DECLARE_FUNCTION(execUnitSeamlessTravel); \
	DECLARE_FUNCTION(execServerAdmin); \
	DECLARE_FUNCTION(execAdmin);


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual bool ServerExecute_Validate(const FString& ); \
	virtual void ServerExecute_Implementation(const FString& InDelegate); \
	NETCODEUNITTEST_API virtual void NetMulticastPing_Implementation(); \
	virtual bool ServerClientPing_Validate(); \
	NETCODEUNITTEST_API virtual void ServerClientPing_Implementation(); \
	virtual bool ServerReceiveText_Validate(FText const& ); \
	NETCODEUNITTEST_API virtual void ServerReceiveText_Implementation(FText const& InText); \
	virtual bool ServerClientStillAlive_Validate(); \
	virtual void ServerClientStillAlive_Implementation(); \
	virtual bool ServerAdmin_Validate(const FString& ); \
	virtual void ServerAdmin_Implementation(const FString& Command); \
 \
	DECLARE_FUNCTION(execServerExecute); \
	DECLARE_FUNCTION(execNetMulticastPing); \
	DECLARE_FUNCTION(execServerClientPing); \
	DECLARE_FUNCTION(execServerReceiveText); \
	DECLARE_FUNCTION(execServerClientStillAlive); \
	DECLARE_FUNCTION(execWait); \
	DECLARE_FUNCTION(execNetFlush); \
	DECLARE_FUNCTION(execUnitTravel); \
	DECLARE_FUNCTION(execUnitSeamlessTravel); \
	DECLARE_FUNCTION(execServerAdmin); \
	DECLARE_FUNCTION(execAdmin);


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_EVENT_PARMS \
	struct NUTActor_eventServerAdmin_Parms \
	{ \
		FString Command; \
	}; \
	struct NUTActor_eventServerExecute_Parms \
	{ \
		FString InDelegate; \
	}; \
	struct NUTActor_eventServerReceiveText_Parms \
	{ \
		FText InText; \
	};


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_CALLBACK_WRAPPERS
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANUTActor(); \
	friend struct Z_Construct_UClass_ANUTActor_Statics; \
public: \
	DECLARE_CLASS(ANUTActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(ANUTActor)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_INCLASS \
private: \
	static void StaticRegisterNativesANUTActor(); \
	friend struct Z_Construct_UClass_ANUTActor_Statics; \
public: \
	DECLARE_CLASS(ANUTActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NetcodeUnitTest"), NO_API) \
	DECLARE_SERIALIZER(ANUTActor)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANUTActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANUTActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANUTActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANUTActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANUTActor(ANUTActor&&); \
	NO_API ANUTActor(const ANUTActor&); \
public:


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANUTActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANUTActor(ANUTActor&&); \
	NO_API ANUTActor(const ANUTActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANUTActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANUTActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANUTActor)


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_47_PROLOG \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_EVENT_PARMS


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_RPC_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_CALLBACK_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_INCLASS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_CALLBACK_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h_50_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NUTActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NETCODEUNITTEST_API UClass* StaticClass<class ANUTActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_NetcodeUnitTest_NetcodeUnitTest_Source_NetcodeUnitTest_Classes_NUTActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
