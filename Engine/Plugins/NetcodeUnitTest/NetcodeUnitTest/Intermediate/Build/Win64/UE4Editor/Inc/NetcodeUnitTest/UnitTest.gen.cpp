// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NetcodeUnitTest/Classes/UnitTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnitTest() {}
// Cross Module References
	NETCODEUNITTEST_API UEnum* Z_Construct_UEnum_NetcodeUnitTest_EUnitTestResetStage();
	UPackage* Z_Construct_UPackage__Script_NetcodeUnitTest();
	NETCODEUNITTEST_API UEnum* Z_Construct_UEnum_NetcodeUnitTest_EUnitTestVerification();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTest_NoRegister();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTestBase();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTask_NoRegister();
// End Cross Module References
	static UEnum* EUnitTestResetStage_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetcodeUnitTest_EUnitTestResetStage, Z_Construct_UPackage__Script_NetcodeUnitTest(), TEXT("EUnitTestResetStage"));
		}
		return Singleton;
	}
	template<> NETCODEUNITTEST_API UEnum* StaticEnum<EUnitTestResetStage>()
	{
		return EUnitTestResetStage_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUnitTestResetStage(EUnitTestResetStage_StaticEnum, TEXT("/Script/NetcodeUnitTest"), TEXT("EUnitTestResetStage"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetcodeUnitTest_EUnitTestResetStage_Hash() { return 3231163651U; }
	UEnum* Z_Construct_UEnum_NetcodeUnitTest_EUnitTestResetStage()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetcodeUnitTest();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUnitTestResetStage"), 0, Get_Z_Construct_UEnum_NetcodeUnitTest_EUnitTestResetStage_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUnitTestResetStage::None", (int64)EUnitTestResetStage::None },
				{ "EUnitTestResetStage::FullReset", (int64)EUnitTestResetStage::FullReset },
				{ "EUnitTestResetStage::ResetConnection", (int64)EUnitTestResetStage::ResetConnection },
				{ "EUnitTestResetStage::ResetExecute", (int64)EUnitTestResetStage::ResetExecute },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * The different stages that unit tests can be reset to - a global/non-locally-customizable list, for now\n * NOTE: Stages MUST be sequential! (e.g. ResetConnection implies ResetExecute, FullReset implies both ResetConnection and ResetExecute)\n * NOTE: Apart from checking for 'None', all comparisons should be either <= or >=, to support potential enum additions\n */" },
				{ "FullReset.Comment", "/** Resets the entire unit test, allowing restart from the beginning */" },
				{ "FullReset.Name", "EUnitTestResetStage::FullReset" },
				{ "FullReset.ToolTip", "Resets the entire unit test, allowing restart from the beginning" },
				{ "ModuleRelativePath", "Classes/UnitTest.h" },
				{ "None.Comment", "/** No reset stage */" },
				{ "None.Name", "EUnitTestResetStage::None" },
				{ "None.ToolTip", "No reset stage" },
				{ "ResetConnection.Comment", "/** For ClientUnitTest's, resets the net connection and minimal client - but not the server - allowing a restart from connecting */" },
				{ "ResetConnection.Name", "EUnitTestResetStage::ResetConnection" },
				{ "ResetConnection.ToolTip", "For ClientUnitTest's, resets the net connection and minimal client - but not the server - allowing a restart from connecting" },
				{ "ResetExecute.Comment", "/** Resets unit tests to the point prior to 'ExecuteUnitTest' - usually implemented individually per unit test */" },
				{ "ResetExecute.Name", "EUnitTestResetStage::ResetExecute" },
				{ "ResetExecute.ToolTip", "Resets unit tests to the point prior to 'ExecuteUnitTest' - usually implemented individually per unit test" },
				{ "ToolTip", "The different stages that unit tests can be reset to - a global/non-locally-customizable list, for now\nNOTE: Stages MUST be sequential! (e.g. ResetConnection implies ResetExecute, FullReset implies both ResetConnection and ResetExecute)\nNOTE: Apart from checking for 'None', all comparisons should be either <= or >=, to support potential enum additions" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
				nullptr,
				"EUnitTestResetStage",
				"EUnitTestResetStage",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EUnitTestVerification_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NetcodeUnitTest_EUnitTestVerification, Z_Construct_UPackage__Script_NetcodeUnitTest(), TEXT("EUnitTestVerification"));
		}
		return Singleton;
	}
	template<> NETCODEUNITTEST_API UEnum* StaticEnum<EUnitTestVerification>()
	{
		return EUnitTestVerification_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUnitTestVerification(EUnitTestVerification_StaticEnum, TEXT("/Script/NetcodeUnitTest"), TEXT("EUnitTestVerification"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NetcodeUnitTest_EUnitTestVerification_Hash() { return 1314938196U; }
	UEnum* Z_Construct_UEnum_NetcodeUnitTest_EUnitTestVerification()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NetcodeUnitTest();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUnitTestVerification"), 0, Get_Z_Construct_UEnum_NetcodeUnitTest_EUnitTestVerification_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUnitTestVerification::Unverified", (int64)EUnitTestVerification::Unverified },
				{ "EUnitTestVerification::VerifiedNotFixed", (int64)EUnitTestVerification::VerifiedNotFixed },
				{ "EUnitTestVerification::VerifiedFixed", (int64)EUnitTestVerification::VerifiedFixed },
				{ "EUnitTestVerification::VerifiedNeedsUpdate", (int64)EUnitTestVerification::VerifiedNeedsUpdate },
				{ "EUnitTestVerification::VerifiedUnreliable", (int64)EUnitTestVerification::VerifiedUnreliable },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * The verification status of the current unit test - normally its execution completes immediately after positive/negative verification\n */" },
				{ "ModuleRelativePath", "Classes/UnitTest.h" },
				{ "ToolTip", "The verification status of the current unit test - normally its execution completes immediately after positive/negative verification" },
				{ "Unverified.Comment", "/** Unit test is not yet verified */" },
				{ "Unverified.Name", "EUnitTestVerification::Unverified" },
				{ "Unverified.ToolTip", "Unit test is not yet verified" },
				{ "VerifiedFixed.Comment", "/** Unit test is verified as fixed */" },
				{ "VerifiedFixed.Name", "EUnitTestVerification::VerifiedFixed" },
				{ "VerifiedFixed.ToolTip", "Unit test is verified as fixed" },
				{ "VerifiedNeedsUpdate.Comment", "/** Unit test is no longer functioning, needs manual check/update (issue may be fixed, or unit test broken) */" },
				{ "VerifiedNeedsUpdate.Name", "EUnitTestVerification::VerifiedNeedsUpdate" },
				{ "VerifiedNeedsUpdate.ToolTip", "Unit test is no longer functioning, needs manual check/update (issue may be fixed, or unit test broken)" },
				{ "VerifiedNotFixed.Comment", "/** Unit test is verified as not fixed */" },
				{ "VerifiedNotFixed.Name", "EUnitTestVerification::VerifiedNotFixed" },
				{ "VerifiedNotFixed.ToolTip", "Unit test is verified as not fixed" },
				{ "VerifiedUnreliable.Comment", "/** Unit test is verified as having executed unreliably */" },
				{ "VerifiedUnreliable.Name", "EUnitTestVerification::VerifiedUnreliable" },
				{ "VerifiedUnreliable.ToolTip", "Unit test is verified as having executed unreliably" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
				nullptr,
				"EUnitTestVerification",
				"EUnitTestVerification",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUnitTest::StaticRegisterNativesUUnitTest()
	{
	}
	UClass* Z_Construct_UClass_UUnitTest_NoRegister()
	{
		return UUnitTest::StaticClass();
	}
	struct Z_Construct_UClass_UUnitTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PeakMemoryUsage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_PeakMemoryUsage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeToPeakMem_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeToPeakMem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastExecutionTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LastExecutionTime;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UnitTasks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitTasks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnitTasks;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_VerificationState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VerificationState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_VerificationState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUnitTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUnitTestBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NetcodeUnitTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTest_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for all unit tests\n */" },
		{ "IncludePath", "UnitTest.h" },
		{ "ModuleRelativePath", "Classes/UnitTest.h" },
		{ "ToolTip", "Base class for all unit tests" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTest_Statics::NewProp_PeakMemoryUsage_MetaData[] = {
		{ "Comment", "/** Stores stats on the highest-ever reported memory usage, for this unit test - for estimating memory usage */" },
		{ "ModuleRelativePath", "Classes/UnitTest.h" },
		{ "ToolTip", "Stores stats on the highest-ever reported memory usage, for this unit test - for estimating memory usage" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_PeakMemoryUsage = { "PeakMemoryUsage", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUnitTest, PeakMemoryUsage), METADATA_PARAMS(Z_Construct_UClass_UUnitTest_Statics::NewProp_PeakMemoryUsage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::NewProp_PeakMemoryUsage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTest_Statics::NewProp_TimeToPeakMem_MetaData[] = {
		{ "Comment", "/** The amount of time it takes to reach 'PeakMemoryUsage' (or within 90% of its value) */" },
		{ "ModuleRelativePath", "Classes/UnitTest.h" },
		{ "ToolTip", "The amount of time it takes to reach 'PeakMemoryUsage' (or within 90% of its value)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_TimeToPeakMem = { "TimeToPeakMem", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUnitTest, TimeToPeakMem), METADATA_PARAMS(Z_Construct_UClass_UUnitTest_Statics::NewProp_TimeToPeakMem_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::NewProp_TimeToPeakMem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTest_Statics::NewProp_LastExecutionTime_MetaData[] = {
		{ "Comment", "/** The amount of time it took to execute the unit test the last time it was run */" },
		{ "ModuleRelativePath", "Classes/UnitTest.h" },
		{ "ToolTip", "The amount of time it took to execute the unit test the last time it was run" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_LastExecutionTime = { "LastExecutionTime", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUnitTest, LastExecutionTime), METADATA_PARAMS(Z_Construct_UClass_UUnitTest_Statics::NewProp_LastExecutionTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::NewProp_LastExecutionTime_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks_Inner = { "UnitTasks", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UUnitTask_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks_MetaData[] = {
		{ "Comment", "/** UnitTask's which must be run before different stages of the unit test can execute */" },
		{ "ModuleRelativePath", "Classes/UnitTest.h" },
		{ "ToolTip", "UnitTask's which must be run before different stages of the unit test can execute" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks = { "UnitTasks", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUnitTest, UnitTasks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState_MetaData[] = {
		{ "Comment", "/** Whether or not the success or failure of the current unit test has been verified */" },
		{ "ModuleRelativePath", "Classes/UnitTest.h" },
		{ "ToolTip", "Whether or not the success or failure of the current unit test has been verified" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState = { "VerificationState", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUnitTest, VerificationState), Z_Construct_UEnum_NetcodeUnitTest_EUnitTestVerification, METADATA_PARAMS(Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUnitTest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_PeakMemoryUsage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_TimeToPeakMem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_LastExecutionTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_UnitTasks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUnitTest_Statics::NewProp_VerificationState,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUnitTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUnitTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUnitTest_Statics::ClassParams = {
		&UUnitTest::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUnitTest_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::PropPointers),
		0,
		0x001000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UUnitTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUnitTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUnitTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUnitTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitTest, 1709578825);
	template<> NETCODEUNITTEST_API UClass* StaticClass<UUnitTest>()
	{
		return UUnitTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitTest(Z_Construct_UClass_UUnitTest, &UUnitTest::StaticClass, TEXT("/Script/NetcodeUnitTest"), TEXT("UUnitTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
