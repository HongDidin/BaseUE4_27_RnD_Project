// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NUTUnrealEngine4/Classes/UnitTests/Obsolete/UTT61_DebugReplicateData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUTT61_DebugReplicateData() {}
// Cross Module References
	NUTUNREALENGINE4_API UClass* Z_Construct_UClass_UUTT61_DebugReplicateData_NoRegister();
	NUTUNREALENGINE4_API UClass* Z_Construct_UClass_UUTT61_DebugReplicateData();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UClientUnitTest();
	UPackage* Z_Construct_UPackage__Script_NUTUnrealEngine4();
// End Cross Module References
	void UUTT61_DebugReplicateData::StaticRegisterNativesUUTT61_DebugReplicateData()
	{
	}
	UClass* Z_Construct_UClass_UUTT61_DebugReplicateData_NoRegister()
	{
		return UUTT61_DebugReplicateData::StaticClass();
	}
	struct Z_Construct_UClass_UUTT61_DebugReplicateData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UClientUnitTest,
		(UObject* (*)())Z_Construct_UPackage__Script_NUTUnrealEngine4,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements UTT GameplayDebuggingComponent/ServerReplicateData crash\n *\n * Command:\n *\x09UTT -b 61 127.0.0.1\n *\n * Documentation from Luigi:\n * - TEST VULNERABILITY: ServerReplicateData array overflow\n * ####\n * 07 May 2014\n * ServerReplicateData array overflow\n * \n * The ServerReplicateMessageToAIDebugView function used for some\n * debugging features is affected by an array overflow with the\n * InMessages: ActivateDataView and DeactivateDataView.\n * \n * This is the same function that was affected by the bug 55 reported\n * the 27 March, anyway remember that it's NOT used in shipping builds.\n */" },
		{ "IncludePath", "UnitTests/Obsolete/UTT61_DebugReplicateData.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/Obsolete/UTT61_DebugReplicateData.h" },
		{ "ToolTip", "Implements UTT GameplayDebuggingComponent/ServerReplicateData crash\n\nCommand:\n    UTT -b 61 127.0.0.1\n\nDocumentation from Luigi:\n- TEST VULNERABILITY: ServerReplicateData array overflow\n####\n07 May 2014\nServerReplicateData array overflow\n\nThe ServerReplicateMessageToAIDebugView function used for some\ndebugging features is affected by an array overflow with the\nInMessages: ActivateDataView and DeactivateDataView.\n\nThis is the same function that was affected by the bug 55 reported\nthe 27 March, anyway remember that it's NOT used in shipping builds." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUTT61_DebugReplicateData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::ClassParams = {
		&UUTT61_DebugReplicateData::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUTT61_DebugReplicateData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUTT61_DebugReplicateData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUTT61_DebugReplicateData, 61651889);
	template<> NUTUNREALENGINE4_API UClass* StaticClass<UUTT61_DebugReplicateData>()
	{
		return UUTT61_DebugReplicateData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUTT61_DebugReplicateData(Z_Construct_UClass_UUTT61_DebugReplicateData, &UUTT61_DebugReplicateData::StaticClass, TEXT("/Script/NUTUnrealEngine4"), TEXT("UUTT61_DebugReplicateData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUTT61_DebugReplicateData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
