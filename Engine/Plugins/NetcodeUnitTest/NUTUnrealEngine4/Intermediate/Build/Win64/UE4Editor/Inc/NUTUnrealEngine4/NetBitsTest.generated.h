// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NUTUNREALENGINE4_NetBitsTest_generated_h
#error "NetBitsTest.generated.h already included, missing '#pragma once' in NetBitsTest.h"
#endif
#define NUTUNREALENGINE4_NetBitsTest_generated_h

#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_SPARSE_DATA
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_RPC_WRAPPERS
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNetBitsTest(); \
	friend struct Z_Construct_UClass_UNetBitsTest_Statics; \
public: \
	DECLARE_CLASS(UNetBitsTest, UUnitTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NUTUnrealEngine4"), NO_API) \
	DECLARE_SERIALIZER(UNetBitsTest)


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUNetBitsTest(); \
	friend struct Z_Construct_UClass_UNetBitsTest_Statics; \
public: \
	DECLARE_CLASS(UNetBitsTest, UUnitTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NUTUnrealEngine4"), NO_API) \
	DECLARE_SERIALIZER(UNetBitsTest)


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetBitsTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetBitsTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetBitsTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetBitsTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetBitsTest(UNetBitsTest&&); \
	NO_API UNetBitsTest(const UNetBitsTest&); \
public:


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNetBitsTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNetBitsTest(UNetBitsTest&&); \
	NO_API UNetBitsTest(const UNetBitsTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNetBitsTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNetBitsTest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNetBitsTest)


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_15_PROLOG
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_RPC_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_INCLASS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class NetBitsTest."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NUTUNREALENGINE4_API UClass* StaticClass<class UNetBitsTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_NetBitsTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
