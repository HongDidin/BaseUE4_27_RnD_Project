// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NUTUNREALENGINE4_UTT61_DebugReplicateData_generated_h
#error "UTT61_DebugReplicateData.generated.h already included, missing '#pragma once' in UTT61_DebugReplicateData.h"
#endif
#define NUTUNREALENGINE4_UTT61_DebugReplicateData_generated_h

#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_SPARSE_DATA
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_RPC_WRAPPERS
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUTT61_DebugReplicateData(); \
	friend struct Z_Construct_UClass_UUTT61_DebugReplicateData_Statics; \
public: \
	DECLARE_CLASS(UUTT61_DebugReplicateData, UClientUnitTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NUTUnrealEngine4"), NO_API) \
	DECLARE_SERIALIZER(UUTT61_DebugReplicateData)


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUUTT61_DebugReplicateData(); \
	friend struct Z_Construct_UClass_UUTT61_DebugReplicateData_Statics; \
public: \
	DECLARE_CLASS(UUTT61_DebugReplicateData, UClientUnitTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NUTUnrealEngine4"), NO_API) \
	DECLARE_SERIALIZER(UUTT61_DebugReplicateData)


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUTT61_DebugReplicateData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUTT61_DebugReplicateData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUTT61_DebugReplicateData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUTT61_DebugReplicateData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUTT61_DebugReplicateData(UUTT61_DebugReplicateData&&); \
	NO_API UUTT61_DebugReplicateData(const UUTT61_DebugReplicateData&); \
public:


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUTT61_DebugReplicateData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUTT61_DebugReplicateData(UUTT61_DebugReplicateData&&); \
	NO_API UUTT61_DebugReplicateData(const UUTT61_DebugReplicateData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUTT61_DebugReplicateData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUTT61_DebugReplicateData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUTT61_DebugReplicateData)


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_33_PROLOG
#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_RPC_WRAPPERS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_INCLASS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_SPARSE_DATA \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h_36_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UTT61_DebugReplicateData."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NUTUNREALENGINE4_API UClass* StaticClass<class UUTT61_DebugReplicateData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_NetcodeUnitTest_NUTUnrealEngine4_Source_NUTUnrealEngine4_Classes_UnitTests_Obsolete_UTT61_DebugReplicateData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
