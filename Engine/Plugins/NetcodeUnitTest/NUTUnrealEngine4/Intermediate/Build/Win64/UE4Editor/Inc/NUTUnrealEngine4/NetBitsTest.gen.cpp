// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NUTUnrealEngine4/Classes/UnitTests/NetBitsTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNetBitsTest() {}
// Cross Module References
	NUTUNREALENGINE4_API UClass* Z_Construct_UClass_UNetBitsTest_NoRegister();
	NUTUNREALENGINE4_API UClass* Z_Construct_UClass_UNetBitsTest();
	NETCODEUNITTEST_API UClass* Z_Construct_UClass_UUnitTest();
	UPackage* Z_Construct_UPackage__Script_NUTUnrealEngine4();
// End Cross Module References
	void UNetBitsTest::StaticRegisterNativesUNetBitsTest()
	{
	}
	UClass* Z_Construct_UClass_UNetBitsTest_NoRegister()
	{
		return UNetBitsTest::StaticClass();
	}
	struct Z_Construct_UClass_UNetBitsTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNetBitsTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUnitTest,
		(UObject* (*)())Z_Construct_UPackage__Script_NUTUnrealEngine4,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNetBitsTest_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * @todo JohnB\n */" },
		{ "IncludePath", "UnitTests/NetBitsTest.h" },
		{ "ModuleRelativePath", "Classes/UnitTests/NetBitsTest.h" },
		{ "ToolTip", "@todo JohnB" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNetBitsTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNetBitsTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNetBitsTest_Statics::ClassParams = {
		&UNetBitsTest::StaticClass,
		"UnitTestStats",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNetBitsTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNetBitsTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNetBitsTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNetBitsTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNetBitsTest, 3798330803);
	template<> NUTUNREALENGINE4_API UClass* StaticClass<UNetBitsTest>()
	{
		return UNetBitsTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNetBitsTest(Z_Construct_UClass_UNetBitsTest, &UNetBitsTest::StaticClass, TEXT("/Script/NUTUnrealEngine4"), TEXT("UNetBitsTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNetBitsTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
