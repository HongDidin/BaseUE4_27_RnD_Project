// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapBluetoothGattConnectionPriority : uint8;
struct FMagicLeapBluetoothGattCharacteristic;
struct FMagicLeapBluetoothGattDescriptor;
#ifdef MAGICLEAPBLE_MagicLeapBLEComponent_generated_h
#error "MagicLeapBLEComponent.generated.h already included, missing '#pragma once' in MagicLeapBLEComponent.h"
#endif
#define MAGICLEAPBLE_MagicLeapBLEComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRegisterForAdapterStateChangeNotifications); \
	DECLARE_FUNCTION(execAdapterGetStateAsync); \
	DECLARE_FUNCTION(execAdapterGetNameAsync); \
	DECLARE_FUNCTION(execGattRequestMTUAsync); \
	DECLARE_FUNCTION(execGattRequestConnectionPriorityAsync); \
	DECLARE_FUNCTION(execGattSetCharacteristicNotificationAsync); \
	DECLARE_FUNCTION(execGattWriteDescriptorAsync); \
	DECLARE_FUNCTION(execGattReadDescriptorAsync); \
	DECLARE_FUNCTION(execGattWriteCharacteristicAsync); \
	DECLARE_FUNCTION(execGattReadCharacteristicAsync); \
	DECLARE_FUNCTION(execGattGetAvailableServicesAsync); \
	DECLARE_FUNCTION(execGattReadRemoteRSSIAsync); \
	DECLARE_FUNCTION(execGattDisconnectAsync); \
	DECLARE_FUNCTION(execGattConnectAsync); \
	DECLARE_FUNCTION(execStopScan); \
	DECLARE_FUNCTION(execStartScanAsync);


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRegisterForAdapterStateChangeNotifications); \
	DECLARE_FUNCTION(execAdapterGetStateAsync); \
	DECLARE_FUNCTION(execAdapterGetNameAsync); \
	DECLARE_FUNCTION(execGattRequestMTUAsync); \
	DECLARE_FUNCTION(execGattRequestConnectionPriorityAsync); \
	DECLARE_FUNCTION(execGattSetCharacteristicNotificationAsync); \
	DECLARE_FUNCTION(execGattWriteDescriptorAsync); \
	DECLARE_FUNCTION(execGattReadDescriptorAsync); \
	DECLARE_FUNCTION(execGattWriteCharacteristicAsync); \
	DECLARE_FUNCTION(execGattReadCharacteristicAsync); \
	DECLARE_FUNCTION(execGattGetAvailableServicesAsync); \
	DECLARE_FUNCTION(execGattReadRemoteRSSIAsync); \
	DECLARE_FUNCTION(execGattDisconnectAsync); \
	DECLARE_FUNCTION(execGattConnectAsync); \
	DECLARE_FUNCTION(execStopScan); \
	DECLARE_FUNCTION(execStartScanAsync);


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapBLEComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapBLEComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapBLEComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapBLE"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapBLEComponent)


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapBLEComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapBLEComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapBLEComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapBLE"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapBLEComponent)


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapBLEComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapBLEComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapBLEComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapBLEComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapBLEComponent(UMagicLeapBLEComponent&&); \
	NO_API UMagicLeapBLEComponent(const UMagicLeapBLEComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapBLEComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapBLEComponent(UMagicLeapBLEComponent&&); \
	NO_API UMagicLeapBLEComponent(const UMagicLeapBLEComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapBLEComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapBLEComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapBLEComponent)


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnLogDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnLogDelegate); } \
	FORCEINLINE static uint32 __PPO__OnFoundDeviceDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnFoundDeviceDelegate); } \
	FORCEINLINE static uint32 __PPO__OnConnStateChangedDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnConnStateChangedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnReadRemoteRSSIDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnReadRemoteRSSIDelegate); } \
	FORCEINLINE static uint32 __PPO__OnGotAvailableServicesDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnGotAvailableServicesDelegate); } \
	FORCEINLINE static uint32 __PPO__OnReadCharacteristicDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnReadCharacteristicDelegate); } \
	FORCEINLINE static uint32 __PPO__OnWriteCharacteristicDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnWriteCharacteristicDelegate); } \
	FORCEINLINE static uint32 __PPO__OnReadDescriptorDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnReadDescriptorDelegate); } \
	FORCEINLINE static uint32 __PPO__OnWriteDescriptorDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnWriteDescriptorDelegate); } \
	FORCEINLINE static uint32 __PPO__OnCharacteristicChangedDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnCharacteristicChangedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnConnPriorityChangedDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnConnPriorityChangedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnMTUChangedDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnMTUChangedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnGotAdapterNameDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnGotAdapterNameDelegate); } \
	FORCEINLINE static uint32 __PPO__OnGotAdapterStateDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnGotAdapterStateDelegate); } \
	FORCEINLINE static uint32 __PPO__OnAdapterStateChangedDelegate() { return STRUCT_OFFSET(UMagicLeapBLEComponent, OnAdapterStateChangedDelegate); }


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPBLE_API UClass* StaticClass<class UMagicLeapBLEComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLEComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
