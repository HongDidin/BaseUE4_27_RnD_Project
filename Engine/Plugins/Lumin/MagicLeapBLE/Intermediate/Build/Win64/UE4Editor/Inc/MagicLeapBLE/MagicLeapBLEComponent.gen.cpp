// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapBLEComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapBLEComponent() {}
// Cross Module References
	MAGICLEAPBLE_API UClass* Z_Construct_UClass_UMagicLeapBLEComponent_NoRegister();
	MAGICLEAPBLE_API UClass* Z_Construct_UClass_UMagicLeapBLEComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapBLE();
	MAGICLEAPBLE_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapBluetoothGattCharacteristic();
	MAGICLEAPBLE_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapBluetoothGattDescriptor();
	MAGICLEAPBLE_API UEnum* Z_Construct_UEnum_MagicLeapBLE_EMagicLeapBluetoothGattConnectionPriority();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnLogDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnFoundDeviceDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnStateChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadRemoteRSSIDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAvailableServicesDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadCharacteristicDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteCharacteristicDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadDescriptorDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteDescriptorDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnCharacteristicChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnPriorityChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnMTUChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterNameDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterStateDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnAdapterStateChangedDelegateMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execRegisterForAdapterStateChangeNotifications)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RegisterForAdapterStateChangeNotifications();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execAdapterGetStateAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AdapterGetStateAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execAdapterGetNameAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AdapterGetNameAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattRequestMTUAsync)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_MTU);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattRequestMTUAsync(Z_Param_MTU);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattRequestConnectionPriorityAsync)
	{
		P_GET_ENUM(EMagicLeapBluetoothGattConnectionPriority,Z_Param_InPriority);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattRequestConnectionPriorityAsync(EMagicLeapBluetoothGattConnectionPriority(Z_Param_InPriority));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattSetCharacteristicNotificationAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapBluetoothGattCharacteristic,Z_Param_Out_InCharacteristic);
		P_GET_UBOOL(Z_Param_bEnable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattSetCharacteristicNotificationAsync(Z_Param_Out_InCharacteristic,Z_Param_bEnable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattWriteDescriptorAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapBluetoothGattDescriptor,Z_Param_Out_InDescriptor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattWriteDescriptorAsync(Z_Param_Out_InDescriptor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattReadDescriptorAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapBluetoothGattDescriptor,Z_Param_Out_InDescriptor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattReadDescriptorAsync(Z_Param_Out_InDescriptor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattWriteCharacteristicAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapBluetoothGattCharacteristic,Z_Param_Out_InCharacteristic);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattWriteCharacteristicAsync(Z_Param_Out_InCharacteristic);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattReadCharacteristicAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapBluetoothGattCharacteristic,Z_Param_Out_InCharacteristic);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattReadCharacteristicAsync(Z_Param_Out_InCharacteristic);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattGetAvailableServicesAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattGetAvailableServicesAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattReadRemoteRSSIAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattReadRemoteRSSIAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattDisconnectAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattDisconnectAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execGattConnectAsync)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InAddress);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GattConnectAsync(Z_Param_InAddress);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execStopScan)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopScan();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapBLEComponent::execStartScanAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartScanAsync();
		P_NATIVE_END;
	}
	void UMagicLeapBLEComponent::StaticRegisterNativesUMagicLeapBLEComponent()
	{
		UClass* Class = UMagicLeapBLEComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AdapterGetNameAsync", &UMagicLeapBLEComponent::execAdapterGetNameAsync },
			{ "AdapterGetStateAsync", &UMagicLeapBLEComponent::execAdapterGetStateAsync },
			{ "GattConnectAsync", &UMagicLeapBLEComponent::execGattConnectAsync },
			{ "GattDisconnectAsync", &UMagicLeapBLEComponent::execGattDisconnectAsync },
			{ "GattGetAvailableServicesAsync", &UMagicLeapBLEComponent::execGattGetAvailableServicesAsync },
			{ "GattReadCharacteristicAsync", &UMagicLeapBLEComponent::execGattReadCharacteristicAsync },
			{ "GattReadDescriptorAsync", &UMagicLeapBLEComponent::execGattReadDescriptorAsync },
			{ "GattReadRemoteRSSIAsync", &UMagicLeapBLEComponent::execGattReadRemoteRSSIAsync },
			{ "GattRequestConnectionPriorityAsync", &UMagicLeapBLEComponent::execGattRequestConnectionPriorityAsync },
			{ "GattRequestMTUAsync", &UMagicLeapBLEComponent::execGattRequestMTUAsync },
			{ "GattSetCharacteristicNotificationAsync", &UMagicLeapBLEComponent::execGattSetCharacteristicNotificationAsync },
			{ "GattWriteCharacteristicAsync", &UMagicLeapBLEComponent::execGattWriteCharacteristicAsync },
			{ "GattWriteDescriptorAsync", &UMagicLeapBLEComponent::execGattWriteDescriptorAsync },
			{ "RegisterForAdapterStateChangeNotifications", &UMagicLeapBLEComponent::execRegisterForAdapterStateChangeNotifications },
			{ "StartScanAsync", &UMagicLeapBLEComponent::execStartScanAsync },
			{ "StopScan", &UMagicLeapBLEComponent::execStopScan },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Requests the name of the local Bluetooth adapter.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Requests the name of the local Bluetooth adapter." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "AdapterGetNameAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Requests the state of the local Bluetooth adpater.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Requests the state of the local Bluetooth adpater." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "AdapterGetStateAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattConnectAsync_Parms
		{
			FString InAddress;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InAddress;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::NewProp_InAddress_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::NewProp_InAddress = { "InAddress", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattConnectAsync_Parms, InAddress), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::NewProp_InAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::NewProp_InAddress_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::NewProp_InAddress,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/** \n\x09\x09Initiates a connection to a Bluetooth GATT capable device. \n\x09\x09@param InAddress The address to connect to.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Initiates a connection to a Bluetooth GATT capable device.\n@param InAddress The address to connect to." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattConnectAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattConnectAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Disconnects an established connection, or cancels a connection attempt.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Disconnects an established connection, or cancels a connection attempt." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattDisconnectAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/** \n\x09\x09Gets a list of GATT services offered by the remote devices.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Gets a list of GATT services offered by the remote devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattGetAvailableServicesAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattReadCharacteristicAsync_Parms
		{
			FMagicLeapBluetoothGattCharacteristic InCharacteristic;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InCharacteristic_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InCharacteristic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::NewProp_InCharacteristic_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::NewProp_InCharacteristic = { "InCharacteristic", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattReadCharacteristicAsync_Parms, InCharacteristic), Z_Construct_UScriptStruct_FMagicLeapBluetoothGattCharacteristic, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::NewProp_InCharacteristic_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::NewProp_InCharacteristic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::NewProp_InCharacteristic,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/** \n\x09\x09Reads the requested characteristic from the connected remote device.\n\x09\x09@param InCharacteristic The characteristic to read from the connected device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Reads the requested characteristic from the connected remote device.\n@param InCharacteristic The characteristic to read from the connected device." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattReadCharacteristicAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattReadCharacteristicAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattReadDescriptorAsync_Parms
		{
			FMagicLeapBluetoothGattDescriptor InDescriptor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InDescriptor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InDescriptor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::NewProp_InDescriptor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::NewProp_InDescriptor = { "InDescriptor", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattReadDescriptorAsync_Parms, InDescriptor), Z_Construct_UScriptStruct_FMagicLeapBluetoothGattDescriptor, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::NewProp_InDescriptor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::NewProp_InDescriptor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::NewProp_InDescriptor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Reads the requested descriptor from the connected remote device.\n\x09\x09@param InDescriptor The descriptor to be read from the connected device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Reads the requested descriptor from the connected remote device.\n@param InDescriptor The descriptor to be read from the connected device." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattReadDescriptorAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattReadDescriptorAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Reads the RSSI for a connected remote device.  The on_gatt_read_remote_rssi callback will be invoked when the RSSI value\n\x09\x09has been read.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Reads the RSSI for a connected remote device.  The on_gatt_read_remote_rssi callback will be invoked when the RSSI value\nhas been read." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattReadRemoteRSSIAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattRequestConnectionPriorityAsync_Parms
		{
			EMagicLeapBluetoothGattConnectionPriority InPriority;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InPriority_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InPriority;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::NewProp_InPriority_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::NewProp_InPriority = { "InPriority", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattRequestConnectionPriorityAsync_Parms, InPriority), Z_Construct_UEnum_MagicLeapBLE_EMagicLeapBluetoothGattConnectionPriority, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::NewProp_InPriority_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::NewProp_InPriority,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Requests a connection parameter update.\n\x09\x09@param InPriority The new connection priority for the connected device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Requests a connection parameter update.\n@param InPriority The new connection priority for the connected device." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattRequestConnectionPriorityAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattRequestConnectionPriorityAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattRequestMTUAsync_Parms
		{
			int32 MTU;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MTU;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::NewProp_MTU = { "MTU", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattRequestMTUAsync_Parms, MTU), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::NewProp_MTU,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Requests to change MTU size.\n\x09\x09@param MTU The new MTU for the connected device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Requests to change MTU size.\n@param MTU The new MTU for the connected device." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattRequestMTUAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattRequestMTUAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattSetCharacteristicNotificationAsync_Parms
		{
			FMagicLeapBluetoothGattCharacteristic InCharacteristic;
			bool bEnable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InCharacteristic_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InCharacteristic;
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_InCharacteristic_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_InCharacteristic = { "InCharacteristic", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattSetCharacteristicNotificationAsync_Parms, InCharacteristic), Z_Construct_UScriptStruct_FMagicLeapBluetoothGattCharacteristic, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_InCharacteristic_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_InCharacteristic_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((MagicLeapBLEComponent_eventGattSetCharacteristicNotificationAsync_Parms*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapBLEComponent_eventGattSetCharacteristicNotificationAsync_Parms), &Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_InCharacteristic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::NewProp_bEnable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Enables or disables notifications/indications for a given chracteristic.\n\x09\x09@param InCharacteristic The characteristic to receive change notifications from on the remote device.\n\x09\x09@param bEnable Enables/Disables the notifications for InCharacteristic.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Enables or disables notifications/indications for a given chracteristic.\n@param InCharacteristic The characteristic to receive change notifications from on the remote device.\n@param bEnable Enables/Disables the notifications for InCharacteristic." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattSetCharacteristicNotificationAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattSetCharacteristicNotificationAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattWriteCharacteristicAsync_Parms
		{
			FMagicLeapBluetoothGattCharacteristic InCharacteristic;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InCharacteristic_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InCharacteristic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::NewProp_InCharacteristic_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::NewProp_InCharacteristic = { "InCharacteristic", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattWriteCharacteristicAsync_Parms, InCharacteristic), Z_Construct_UScriptStruct_FMagicLeapBluetoothGattCharacteristic, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::NewProp_InCharacteristic_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::NewProp_InCharacteristic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::NewProp_InCharacteristic,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Writes a given characteristic and its value to the connected remote device.\n\x09\x09@param InCharacteristic The characteristic to be written to the connected device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Writes a given characteristic and its value to the connected remote device.\n@param InCharacteristic The characteristic to be written to the connected device." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattWriteCharacteristicAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattWriteCharacteristicAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics
	{
		struct MagicLeapBLEComponent_eventGattWriteDescriptorAsync_Parms
		{
			FMagicLeapBluetoothGattDescriptor InDescriptor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InDescriptor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InDescriptor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::NewProp_InDescriptor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::NewProp_InDescriptor = { "InDescriptor", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapBLEComponent_eventGattWriteDescriptorAsync_Parms, InDescriptor), Z_Construct_UScriptStruct_FMagicLeapBluetoothGattDescriptor, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::NewProp_InDescriptor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::NewProp_InDescriptor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::NewProp_InDescriptor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Writes the value of a given descriptor to the connected device.\n\x09\x09@param InDescriptor The descriptor to be written to the connected device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Writes the value of a given descriptor to the connected device.\n@param InDescriptor The descriptor to be written to the connected device." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "GattWriteDescriptorAsync", nullptr, nullptr, sizeof(MagicLeapBLEComponent_eventGattWriteDescriptorAsync_Parms), Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Requests adapter state changes to be relayed to the calling app.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Requests adapter state changes to be relayed to the calling app." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "RegisterForAdapterStateChangeNotifications", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Starts Bluetooth LE scan.  The results will be delivered through scanner callback.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Starts Bluetooth LE scan.  The results will be delivered through scanner callback." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "StartScanAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan_Statics::Function_MetaDataParams[] = {
		{ "Category", "BLE | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Stops Bluetooth LE scan in progress.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Stops Bluetooth LE scan in progress." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapBLEComponent, nullptr, "StopScan", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapBLEComponent_NoRegister()
	{
		return UMagicLeapBLEComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapBLEComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnLogDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnLogDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFoundDeviceDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFoundDeviceDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnConnStateChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnConnStateChangedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnReadRemoteRSSIDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnReadRemoteRSSIDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnGotAvailableServicesDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGotAvailableServicesDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnReadCharacteristicDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnReadCharacteristicDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnWriteCharacteristicDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnWriteCharacteristicDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnReadDescriptorDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnReadDescriptorDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnWriteDescriptorDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnWriteDescriptorDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCharacteristicChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCharacteristicChangedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnConnPriorityChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnConnPriorityChangedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMTUChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMTUChangedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnGotAdapterNameDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGotAdapterNameDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnGotAdapterStateDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGotAdapterStateDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAdapterStateChangedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAdapterStateChangedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapBLEComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapBLE,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapBLEComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetNameAsync, "AdapterGetNameAsync" }, // 3460791094
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_AdapterGetStateAsync, "AdapterGetStateAsync" }, // 1547556077
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattConnectAsync, "GattConnectAsync" }, // 2448253247
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattDisconnectAsync, "GattDisconnectAsync" }, // 4108321594
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattGetAvailableServicesAsync, "GattGetAvailableServicesAsync" }, // 897131543
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadCharacteristicAsync, "GattReadCharacteristicAsync" }, // 2866174887
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadDescriptorAsync, "GattReadDescriptorAsync" }, // 4044577836
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattReadRemoteRSSIAsync, "GattReadRemoteRSSIAsync" }, // 2039228215
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestConnectionPriorityAsync, "GattRequestConnectionPriorityAsync" }, // 3069858275
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattRequestMTUAsync, "GattRequestMTUAsync" }, // 2148600232
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattSetCharacteristicNotificationAsync, "GattSetCharacteristicNotificationAsync" }, // 1270239950
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteCharacteristicAsync, "GattWriteCharacteristicAsync" }, // 2596579406
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_GattWriteDescriptorAsync, "GattWriteDescriptorAsync" }, // 3670223233
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_RegisterForAdapterStateChangeNotifications, "RegisterForAdapterStateChangeNotifications" }, // 2815762808
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_StartScanAsync, "StartScanAsync" }, // 471316940
		{ &Z_Construct_UFunction_UMagicLeapBLEComponent_StopScan, "StopScan" }, // 3625083967
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09""Component that provides access to the BLE API functionality.\n*/" },
		{ "IncludePath", "MagicLeapBLEComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
		{ "ToolTip", "Component that provides access to the BLE API functionality." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnLogDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnLogDelegate = { "OnLogDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnLogDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnLogDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnLogDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnLogDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnFoundDeviceDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnFoundDeviceDelegate = { "OnFoundDeviceDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnFoundDeviceDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnFoundDeviceDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnFoundDeviceDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnFoundDeviceDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnStateChangedDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnStateChangedDelegate = { "OnConnStateChangedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnConnStateChangedDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnStateChangedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnStateChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnStateChangedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadRemoteRSSIDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadRemoteRSSIDelegate = { "OnReadRemoteRSSIDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnReadRemoteRSSIDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadRemoteRSSIDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadRemoteRSSIDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadRemoteRSSIDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAvailableServicesDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAvailableServicesDelegate = { "OnGotAvailableServicesDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnGotAvailableServicesDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAvailableServicesDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAvailableServicesDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAvailableServicesDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadCharacteristicDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadCharacteristicDelegate = { "OnReadCharacteristicDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnReadCharacteristicDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadCharacteristicDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadCharacteristicDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadCharacteristicDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteCharacteristicDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteCharacteristicDelegate = { "OnWriteCharacteristicDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnWriteCharacteristicDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteCharacteristicDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteCharacteristicDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteCharacteristicDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadDescriptorDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadDescriptorDelegate = { "OnReadDescriptorDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnReadDescriptorDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadDescriptorDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadDescriptorDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadDescriptorDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteDescriptorDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteDescriptorDelegate = { "OnWriteDescriptorDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnWriteDescriptorDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteDescriptorDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteDescriptorDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteDescriptorDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnCharacteristicChangedDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnCharacteristicChangedDelegate = { "OnCharacteristicChangedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnCharacteristicChangedDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnCharacteristicChangedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnCharacteristicChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnCharacteristicChangedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnPriorityChangedDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnPriorityChangedDelegate = { "OnConnPriorityChangedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnConnPriorityChangedDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnPriorityChangedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnPriorityChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnPriorityChangedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnMTUChangedDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnMTUChangedDelegate = { "OnMTUChangedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnMTUChangedDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnMTUChangedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnMTUChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnMTUChangedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterNameDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterNameDelegate = { "OnGotAdapterNameDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnGotAdapterNameDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterNameDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterNameDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterNameDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterStateDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterStateDelegate = { "OnGotAdapterStateDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnGotAdapterStateDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterStateDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterStateDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterStateDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnAdapterStateChangedDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "BLE | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapBLEComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnAdapterStateChangedDelegate = { "OnAdapterStateChangedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapBLEComponent, OnAdapterStateChangedDelegate), Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnAdapterStateChangedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnAdapterStateChangedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnAdapterStateChangedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapBLEComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnLogDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnFoundDeviceDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnStateChangedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadRemoteRSSIDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAvailableServicesDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadCharacteristicDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteCharacteristicDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnReadDescriptorDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnWriteDescriptorDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnCharacteristicChangedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnConnPriorityChangedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnMTUChangedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterNameDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnGotAdapterStateDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapBLEComponent_Statics::NewProp_OnAdapterStateChangedDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapBLEComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapBLEComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapBLEComponent_Statics::ClassParams = {
		&UMagicLeapBLEComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapBLEComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapBLEComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapBLEComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapBLEComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapBLEComponent, 2151743606);
	template<> MAGICLEAPBLE_API UClass* StaticClass<UMagicLeapBLEComponent>()
	{
		return UMagicLeapBLEComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapBLEComponent(Z_Construct_UClass_UMagicLeapBLEComponent, &UMagicLeapBLEComponent::StaticClass, TEXT("/Script/MagicLeapBLE"), TEXT("UMagicLeapBLEComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapBLEComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
