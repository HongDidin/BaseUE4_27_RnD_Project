// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapBluetoothAdapterState : uint8;
struct FMagicLeapResult;
enum class EMagicLeapBluetoothGattStatus : uint8;
struct FMagicLeapBluetoothGattCharacteristic;
struct FMagicLeapBluetoothGattDescriptor;
struct FMagicLeapBluetoothGattService;
enum class EMagicLeapBluetoothGattConnectionState : uint8;
struct FMagicLeapBluetoothDevice;
#ifdef MAGICLEAPBLE_MagicLeapBLETypes_generated_h
#error "MagicLeapBLETypes.generated.h already included, missing '#pragma once' in MagicLeapBLETypes.h"
#endif
#define MAGICLEAPBLE_MagicLeapBLETypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_280_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapBluetoothGattService_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPBLE_API UScriptStruct* StaticStruct<struct FMagicLeapBluetoothGattService>();

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_259_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapBluetoothGattIncludedService_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPBLE_API UScriptStruct* StaticStruct<struct FMagicLeapBluetoothGattIncludedService>();

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_218_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapBluetoothGattCharacteristic_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPBLE_API UScriptStruct* StaticStruct<struct FMagicLeapBluetoothGattCharacteristic>();

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_189_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapBluetoothGattDescriptor_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPBLE_API UScriptStruct* StaticStruct<struct FMagicLeapBluetoothGattDescriptor>();

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_164_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapBluetoothDevice_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPBLE_API UScriptStruct* StaticStruct<struct FMagicLeapBluetoothDevice>();

#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_361_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnAdapterStateChangedDelegateMulti_Parms \
{ \
	EMagicLeapBluetoothAdapterState NewState; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnAdapterStateChangedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnAdapterStateChangedDelegateMulti, EMagicLeapBluetoothAdapterState NewState, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnAdapterStateChangedDelegateMulti_Parms Parms; \
	Parms.NewState=NewState; \
	Parms.Result=Result; \
	MagicLeapBLEOnAdapterStateChangedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_360_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnAdapterStateChangedDelegateDynamic_Parms \
{ \
	EMagicLeapBluetoothAdapterState NewState; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnAdapterStateChangedDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnAdapterStateChangedDelegateDynamic, EMagicLeapBluetoothAdapterState NewState, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnAdapterStateChangedDelegateDynamic_Parms Parms; \
	Parms.NewState=NewState; \
	Parms.Result=Result; \
	MagicLeapBLEOnAdapterStateChangedDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_357_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterStateDelegateMulti_Parms \
{ \
	EMagicLeapBluetoothAdapterState NewState; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnGotAdapterStateDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnGotAdapterStateDelegateMulti, EMagicLeapBluetoothAdapterState NewState, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterStateDelegateMulti_Parms Parms; \
	Parms.NewState=NewState; \
	Parms.Result=Result; \
	MagicLeapBLEOnGotAdapterStateDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_356_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterStateDelegateDynamic_Parms \
{ \
	EMagicLeapBluetoothAdapterState NewState; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnGotAdapterStateDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnGotAdapterStateDelegateDynamic, EMagicLeapBluetoothAdapterState NewState, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterStateDelegateDynamic_Parms Parms; \
	Parms.NewState=NewState; \
	Parms.Result=Result; \
	MagicLeapBLEOnGotAdapterStateDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_353_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterNameDelegateMulti_Parms \
{ \
	FString Name; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnGotAdapterNameDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnGotAdapterNameDelegateMulti, const FString& Name, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterNameDelegateMulti_Parms Parms; \
	Parms.Name=Name; \
	Parms.Result=Result; \
	MagicLeapBLEOnGotAdapterNameDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_352_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterNameDelegateDynamic_Parms \
{ \
	FString Name; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnGotAdapterNameDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnGotAdapterNameDelegateDynamic, const FString& Name, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnGotAdapterNameDelegateDynamic_Parms Parms; \
	Parms.Name=Name; \
	Parms.Result=Result; \
	MagicLeapBLEOnGotAdapterNameDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_349_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnMTUChangedDelegateMulti_Parms \
{ \
	int32 NewMTU; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnMTUChangedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnMTUChangedDelegateMulti, int32 NewMTU, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnMTUChangedDelegateMulti_Parms Parms; \
	Parms.NewMTU=NewMTU; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnMTUChangedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_348_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnMTUChangedDelegateDynamic_Parms \
{ \
	int32 NewMTU; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnMTUChangedDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnMTUChangedDelegateDynamic, int32 NewMTU, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnMTUChangedDelegateDynamic_Parms Parms; \
	Parms.NewMTU=NewMTU; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnMTUChangedDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_345_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnConnPriorityChangedDelegateMulti_Parms \
{ \
	int32 Interval; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnConnPriorityChangedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnConnPriorityChangedDelegateMulti, int32 Interval, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnConnPriorityChangedDelegateMulti_Parms Parms; \
	Parms.Interval=Interval; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnConnPriorityChangedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_344_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnConnPriorityChangedDelegateDynamic_Parms \
{ \
	int32 Interval; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnConnPriorityChangedDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnConnPriorityChangedDelegateDynamic, int32 Interval, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnConnPriorityChangedDelegateDynamic_Parms Parms; \
	Parms.Interval=Interval; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnConnPriorityChangedDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_341_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnCharacteristicChangedDelegateMulti_Parms \
{ \
	FMagicLeapBluetoothGattCharacteristic Characteristic; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnCharacteristicChangedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnCharacteristicChangedDelegateMulti, FMagicLeapBluetoothGattCharacteristic const& Characteristic, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnCharacteristicChangedDelegateMulti_Parms Parms; \
	Parms.Characteristic=Characteristic; \
	Parms.Result=Result; \
	MagicLeapBLEOnCharacteristicChangedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_340_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnCharacteristicChangedDelegateDynamic_Parms \
{ \
	FMagicLeapBluetoothGattCharacteristic Characteristic; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnCharacteristicChangedDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnCharacteristicChangedDelegateDynamic, FMagicLeapBluetoothGattCharacteristic const& Characteristic, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnCharacteristicChangedDelegateDynamic_Parms Parms; \
	Parms.Characteristic=Characteristic; \
	Parms.Result=Result; \
	MagicLeapBLEOnCharacteristicChangedDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_337_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnWriteDescriptorDelegateMulti_Parms \
{ \
	FMagicLeapBluetoothGattDescriptor Descriptor; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnWriteDescriptorDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnWriteDescriptorDelegateMulti, FMagicLeapBluetoothGattDescriptor const& Descriptor, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnWriteDescriptorDelegateMulti_Parms Parms; \
	Parms.Descriptor=Descriptor; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnWriteDescriptorDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_336_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnWriteDescriptorDelegateDynamic_Parms \
{ \
	FMagicLeapBluetoothGattDescriptor Descriptor; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnWriteDescriptorDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnWriteDescriptorDelegateDynamic, FMagicLeapBluetoothGattDescriptor const& Descriptor, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnWriteDescriptorDelegateDynamic_Parms Parms; \
	Parms.Descriptor=Descriptor; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnWriteDescriptorDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_333_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnReadDescriptorDelegateMulti_Parms \
{ \
	FMagicLeapBluetoothGattDescriptor Descriptor; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnReadDescriptorDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnReadDescriptorDelegateMulti, FMagicLeapBluetoothGattDescriptor const& Descriptor, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnReadDescriptorDelegateMulti_Parms Parms; \
	Parms.Descriptor=Descriptor; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnReadDescriptorDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_332_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnReadDescriptorDelegateDynamic_Parms \
{ \
	FMagicLeapBluetoothGattDescriptor Descriptor; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnReadDescriptorDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnReadDescriptorDelegateDynamic, FMagicLeapBluetoothGattDescriptor const& Descriptor, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnReadDescriptorDelegateDynamic_Parms Parms; \
	Parms.Descriptor=Descriptor; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnReadDescriptorDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_329_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnWriteCharacteristicDelegateMulti_Parms \
{ \
	FMagicLeapBluetoothGattCharacteristic Characteristic; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnWriteCharacteristicDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnWriteCharacteristicDelegateMulti, FMagicLeapBluetoothGattCharacteristic const& Characteristic, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnWriteCharacteristicDelegateMulti_Parms Parms; \
	Parms.Characteristic=Characteristic; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnWriteCharacteristicDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_328_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnWriteCharacteristicDelegateDynamic_Parms \
{ \
	FMagicLeapBluetoothGattCharacteristic Characteristic; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnWriteCharacteristicDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnWriteCharacteristicDelegateDynamic, FMagicLeapBluetoothGattCharacteristic const& Characteristic, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnWriteCharacteristicDelegateDynamic_Parms Parms; \
	Parms.Characteristic=Characteristic; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnWriteCharacteristicDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_325_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnReadCharacteristicDelegateMulti_Parms \
{ \
	FMagicLeapBluetoothGattCharacteristic Characteristic; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnReadCharacteristicDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnReadCharacteristicDelegateMulti, FMagicLeapBluetoothGattCharacteristic const& Characteristic, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnReadCharacteristicDelegateMulti_Parms Parms; \
	Parms.Characteristic=Characteristic; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnReadCharacteristicDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_324_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnReadCharacteristicDelegateDynamic_Parms \
{ \
	FMagicLeapBluetoothGattCharacteristic Characteristic; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnReadCharacteristicDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnReadCharacteristicDelegateDynamic, FMagicLeapBluetoothGattCharacteristic const& Characteristic, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnReadCharacteristicDelegateDynamic_Parms Parms; \
	Parms.Characteristic=Characteristic; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnReadCharacteristicDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_321_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnGotAvailableServicesDelegateMulti_Parms \
{ \
	TArray<FMagicLeapBluetoothGattService> AvailableServices; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnGotAvailableServicesDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnGotAvailableServicesDelegateMulti, TArray<FMagicLeapBluetoothGattService> const& AvailableServices, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnGotAvailableServicesDelegateMulti_Parms Parms; \
	Parms.AvailableServices=AvailableServices; \
	Parms.Result=Result; \
	MagicLeapBLEOnGotAvailableServicesDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_320_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnGotAvailableServicesDelegateDynamic_Parms \
{ \
	TArray<FMagicLeapBluetoothGattService> AvailableServices; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnGotAvailableServicesDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnGotAvailableServicesDelegateDynamic, TArray<FMagicLeapBluetoothGattService> const& AvailableServices, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnGotAvailableServicesDelegateDynamic_Parms Parms; \
	Parms.AvailableServices=AvailableServices; \
	Parms.Result=Result; \
	MagicLeapBLEOnGotAvailableServicesDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_317_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnReadRemoteRSSIDelegateMulti_Parms \
{ \
	int32 RSSI; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnReadRemoteRSSIDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnReadRemoteRSSIDelegateMulti, int32 RSSI, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnReadRemoteRSSIDelegateMulti_Parms Parms; \
	Parms.RSSI=RSSI; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnReadRemoteRSSIDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_316_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnReadRemoteRSSIDelegateDynamic_Parms \
{ \
	int32 RSSI; \
	EMagicLeapBluetoothGattStatus Status; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnReadRemoteRSSIDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnReadRemoteRSSIDelegateDynamic, int32 RSSI, EMagicLeapBluetoothGattStatus Status, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnReadRemoteRSSIDelegateDynamic_Parms Parms; \
	Parms.RSSI=RSSI; \
	Parms.Status=Status; \
	Parms.Result=Result; \
	MagicLeapBLEOnReadRemoteRSSIDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_313_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnConnStateChangedDelegateMulti_Parms \
{ \
	EMagicLeapBluetoothGattConnectionState ConnectionState; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnConnStateChangedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnConnStateChangedDelegateMulti, EMagicLeapBluetoothGattConnectionState ConnectionState, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnConnStateChangedDelegateMulti_Parms Parms; \
	Parms.ConnectionState=ConnectionState; \
	Parms.Result=Result; \
	MagicLeapBLEOnConnStateChangedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_312_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnConnStateChangedDelegateDynamic_Parms \
{ \
	EMagicLeapBluetoothGattConnectionState ConnectionState; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnConnStateChangedDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnConnStateChangedDelegateDynamic, EMagicLeapBluetoothGattConnectionState ConnectionState, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnConnStateChangedDelegateDynamic_Parms Parms; \
	Parms.ConnectionState=ConnectionState; \
	Parms.Result=Result; \
	MagicLeapBLEOnConnStateChangedDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_309_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnFoundDeviceDelegateMulti_Parms \
{ \
	FMagicLeapBluetoothDevice BLEDevice; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnFoundDeviceDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnFoundDeviceDelegateMulti, FMagicLeapBluetoothDevice const& BLEDevice, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnFoundDeviceDelegateMulti_Parms Parms; \
	Parms.BLEDevice=BLEDevice; \
	Parms.Result=Result; \
	MagicLeapBLEOnFoundDeviceDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_308_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnFoundDeviceDelegateDynamic_Parms \
{ \
	FMagicLeapBluetoothDevice BLEDevice; \
	FMagicLeapResult Result; \
}; \
static inline void FMagicLeapBLEOnFoundDeviceDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnFoundDeviceDelegateDynamic, FMagicLeapBluetoothDevice const& BLEDevice, FMagicLeapResult const& Result) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnFoundDeviceDelegateDynamic_Parms Parms; \
	Parms.BLEDevice=BLEDevice; \
	Parms.Result=Result; \
	MagicLeapBLEOnFoundDeviceDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_305_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnLogDelegateMulti_Parms \
{ \
	FString LogMessage; \
}; \
static inline void FMagicLeapBLEOnLogDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapBLEOnLogDelegateMulti, const FString& LogMessage) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnLogDelegateMulti_Parms Parms; \
	Parms.LogMessage=LogMessage; \
	MagicLeapBLEOnLogDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h_304_DELEGATE \
struct _Script_MagicLeapBLE_eventMagicLeapBLEOnLogDelegateDynamic_Parms \
{ \
	FString LogMessage; \
}; \
static inline void FMagicLeapBLEOnLogDelegateDynamic_DelegateWrapper(const FScriptDelegate& MagicLeapBLEOnLogDelegateDynamic, const FString& LogMessage) \
{ \
	_Script_MagicLeapBLE_eventMagicLeapBLEOnLogDelegateDynamic_Parms Parms; \
	Parms.LogMessage=LogMessage; \
	MagicLeapBLEOnLogDelegateDynamic.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapBLE_Source_Public_MagicLeapBLETypes_h


#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHDEVICETYPE(op) \
	op(EMagicLeapBluetoothDeviceType::Unknown) \
	op(EMagicLeapBluetoothDeviceType::LE) 

enum class EMagicLeapBluetoothDeviceType : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothDeviceType>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHACLSTATE(op) \
	op(EMagicLeapBluetoothACLState::Connected) \
	op(EMagicLeapBluetoothACLState::Disconnected) 

enum class EMagicLeapBluetoothACLState : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothACLState>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHBONDTYPE(op) \
	op(EMagicLeapBluetoothBondType::ClassicPin) \
	op(EMagicLeapBluetoothBondType::SspJustWorks) \
	op(EMagicLeapBluetoothBondType::SspNumericalComparison) \
	op(EMagicLeapBluetoothBondType::SspPasskeyEntry) \
	op(EMagicLeapBluetoothBondType::SspPasskeyNotification) 

enum class EMagicLeapBluetoothBondType : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothBondType>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHBONDSTATE(op) \
	op(EMagicLeapBluetoothBondState::None) \
	op(EMagicLeapBluetoothBondState::Bonding) \
	op(EMagicLeapBluetoothBondState::Bonded) 

enum class EMagicLeapBluetoothBondState : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothBondState>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHADAPTERSTATE(op) \
	op(EMagicLeapBluetoothAdapterState::Off) \
	op(EMagicLeapBluetoothAdapterState::On) 

enum class EMagicLeapBluetoothAdapterState : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothAdapterState>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHGATTCONNECTIONPRIORITY(op) \
	op(EMagicLeapBluetoothGattConnectionPriority::Balanced) \
	op(EMagicLeapBluetoothGattConnectionPriority::High) \
	op(EMagicLeapBluetoothGattConnectionPriority::LowPower) 

enum class EMagicLeapBluetoothGattConnectionPriority : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothGattConnectionPriority>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHGATTCONNECTIONSTATE(op) \
	op(EMagicLeapBluetoothGattConnectionState::NotConnected) \
	op(EMagicLeapBluetoothGattConnectionState::Connecting) \
	op(EMagicLeapBluetoothGattConnectionState::Connected) \
	op(EMagicLeapBluetoothGattConnectionState::Disconnecting) 

enum class EMagicLeapBluetoothGattConnectionState : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothGattConnectionState>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHGATTCHARACTERISTICWRITETYPES(op) \
	op(EMagicLeapBluetoothGattCharacteristicWriteTypes::NoResponse) \
	op(EMagicLeapBluetoothGattCharacteristicWriteTypes::Default) \
	op(EMagicLeapBluetoothGattCharacteristicWriteTypes::Signed) 

enum class EMagicLeapBluetoothGattCharacteristicWriteTypes : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothGattCharacteristicWriteTypes>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHGATTCHARACTERISTICPROPERTIES(op) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::Broadcast) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::Read) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::WriteNoRes) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::Write) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::Notify) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::Indicate) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::SignedWrite) \
	op(EMagicLeapBluetoothGattCharacteristicProperties::ExtProps) 

enum class EMagicLeapBluetoothGattCharacteristicProperties : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothGattCharacteristicProperties>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHGATTCHARACTERISTICPERMISSIONS(op) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::Read) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::ReadEncrypted) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::ReadEncryptedMITM) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::Write) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::WriteEncrypted) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::WriteEncryptedMITM) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::WriteSigned) \
	op(EMagicLeapBluetoothGattCharacteristicPermissions::WriteSignedMITM) 

enum class EMagicLeapBluetoothGattCharacteristicPermissions : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothGattCharacteristicPermissions>();

#define FOREACH_ENUM_EMAGICLEAPBLUETOOTHGATTSTATUS(op) \
	op(EMagicLeapBluetoothGattStatus::Success) \
	op(EMagicLeapBluetoothGattStatus::ReadNotPermitted) \
	op(EMagicLeapBluetoothGattStatus::WriteNotPermitted) \
	op(EMagicLeapBluetoothGattStatus::InsufficientAuthentication) \
	op(EMagicLeapBluetoothGattStatus::RequestNot_Supported) \
	op(EMagicLeapBluetoothGattStatus::InvalidOffset) \
	op(EMagicLeapBluetoothGattStatus::InvalidAttributeLength) \
	op(EMagicLeapBluetoothGattStatus::InsufficientEncryption) \
	op(EMagicLeapBluetoothGattStatus::ConnectionCongested) \
	op(EMagicLeapBluetoothGattStatus::Error) \
	op(EMagicLeapBluetoothGattStatus::Failure) 

enum class EMagicLeapBluetoothGattStatus : uint8;
template<> MAGICLEAPBLE_API UEnum* StaticEnum<EMagicLeapBluetoothGattStatus>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
