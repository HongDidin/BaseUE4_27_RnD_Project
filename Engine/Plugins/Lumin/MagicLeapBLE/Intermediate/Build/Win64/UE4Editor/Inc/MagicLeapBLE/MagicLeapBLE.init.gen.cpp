// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapBLE_init() {}
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnLogDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnLogDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnFoundDeviceDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnFoundDeviceDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnStateChangedDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnStateChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadRemoteRSSIDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadRemoteRSSIDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAvailableServicesDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAvailableServicesDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadCharacteristicDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadCharacteristicDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteCharacteristicDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteCharacteristicDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadDescriptorDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadDescriptorDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteDescriptorDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteDescriptorDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnCharacteristicChangedDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnCharacteristicChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnPriorityChangedDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnPriorityChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnMTUChangedDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnMTUChangedDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterNameDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterNameDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterStateDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterStateDelegateMulti__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnAdapterStateChangedDelegateDynamic__DelegateSignature();
	MAGICLEAPBLE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnAdapterStateChangedDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapBLE()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnLogDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnLogDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnFoundDeviceDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnFoundDeviceDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnStateChangedDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnStateChangedDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadRemoteRSSIDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadRemoteRSSIDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAvailableServicesDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAvailableServicesDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadCharacteristicDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadCharacteristicDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteCharacteristicDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteCharacteristicDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadDescriptorDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnReadDescriptorDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteDescriptorDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnWriteDescriptorDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnCharacteristicChangedDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnCharacteristicChangedDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnPriorityChangedDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnConnPriorityChangedDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnMTUChangedDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnMTUChangedDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterNameDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterNameDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterStateDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnGotAdapterStateDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnAdapterStateChangedDelegateDynamic__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapBLE_MagicLeapBLEOnAdapterStateChangedDelegateMulti__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapBLE",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x10C3CF33,
				0xD358E0FD,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
