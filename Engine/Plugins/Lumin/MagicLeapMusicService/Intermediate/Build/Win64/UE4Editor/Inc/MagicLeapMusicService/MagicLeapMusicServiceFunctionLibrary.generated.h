// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMagicLeapMusicServiceTrackMetadata;
enum class EMagicLeapMusicServiceStatus : uint8;
struct FTimespan;
enum class EMagicLeapMusicServicePlaybackRepeatState : uint8;
enum class EMagicLeapMusicServicePlaybackShuffleState : uint8;
enum class EMagicLeapMusicServicePlaybackState : uint8;
enum class EMagicLeapMusicServiceProviderError : uint8;
struct FMagicLeapMusicServiceCallbacks;
#ifdef MAGICLEAPMUSICSERVICE_MagicLeapMusicServiceFunctionLibrary_generated_h
#error "MagicLeapMusicServiceFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapMusicServiceFunctionLibrary.h"
#endif
#define MAGICLEAPMUSICSERVICE_MagicLeapMusicServiceFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentTrackMetadata); \
	DECLARE_FUNCTION(execGetServiceStatus); \
	DECLARE_FUNCTION(execGetCurrentPosition); \
	DECLARE_FUNCTION(execGetTrackLength); \
	DECLARE_FUNCTION(execGetPlaybackVolume); \
	DECLARE_FUNCTION(execSetPlaybackVolume); \
	DECLARE_FUNCTION(execGetPlaybackRepeatState); \
	DECLARE_FUNCTION(execSetPlaybackRepeatState); \
	DECLARE_FUNCTION(execGetPlaybackShuffleState); \
	DECLARE_FUNCTION(execSetPlaybackShuffleState); \
	DECLARE_FUNCTION(execGetPlaybackState); \
	DECLARE_FUNCTION(execGetServiceProviderError); \
	DECLARE_FUNCTION(execPreviousTrack); \
	DECLARE_FUNCTION(execNextTrack); \
	DECLARE_FUNCTION(execSeekTo); \
	DECLARE_FUNCTION(execResumePlayback); \
	DECLARE_FUNCTION(execPausePlayback); \
	DECLARE_FUNCTION(execStopPlayback); \
	DECLARE_FUNCTION(execStartPlayback); \
	DECLARE_FUNCTION(execSetPlaylist); \
	DECLARE_FUNCTION(execSetPlaybackURL); \
	DECLARE_FUNCTION(execSetAuthenticationString); \
	DECLARE_FUNCTION(execSetCallbacks); \
	DECLARE_FUNCTION(execDisconnect); \
	DECLARE_FUNCTION(execConnect);


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentTrackMetadata); \
	DECLARE_FUNCTION(execGetServiceStatus); \
	DECLARE_FUNCTION(execGetCurrentPosition); \
	DECLARE_FUNCTION(execGetTrackLength); \
	DECLARE_FUNCTION(execGetPlaybackVolume); \
	DECLARE_FUNCTION(execSetPlaybackVolume); \
	DECLARE_FUNCTION(execGetPlaybackRepeatState); \
	DECLARE_FUNCTION(execSetPlaybackRepeatState); \
	DECLARE_FUNCTION(execGetPlaybackShuffleState); \
	DECLARE_FUNCTION(execSetPlaybackShuffleState); \
	DECLARE_FUNCTION(execGetPlaybackState); \
	DECLARE_FUNCTION(execGetServiceProviderError); \
	DECLARE_FUNCTION(execPreviousTrack); \
	DECLARE_FUNCTION(execNextTrack); \
	DECLARE_FUNCTION(execSeekTo); \
	DECLARE_FUNCTION(execResumePlayback); \
	DECLARE_FUNCTION(execPausePlayback); \
	DECLARE_FUNCTION(execStopPlayback); \
	DECLARE_FUNCTION(execStartPlayback); \
	DECLARE_FUNCTION(execSetPlaylist); \
	DECLARE_FUNCTION(execSetPlaybackURL); \
	DECLARE_FUNCTION(execSetAuthenticationString); \
	DECLARE_FUNCTION(execSetCallbacks); \
	DECLARE_FUNCTION(execDisconnect); \
	DECLARE_FUNCTION(execConnect);


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapMusicServiceFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapMusicServiceFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapMusicService"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapMusicServiceFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapMusicServiceFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapMusicServiceFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapMusicService"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapMusicServiceFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapMusicServiceFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapMusicServiceFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapMusicServiceFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapMusicServiceFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapMusicServiceFunctionLibrary(UMagicLeapMusicServiceFunctionLibrary&&); \
	NO_API UMagicLeapMusicServiceFunctionLibrary(const UMagicLeapMusicServiceFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapMusicServiceFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapMusicServiceFunctionLibrary(UMagicLeapMusicServiceFunctionLibrary&&); \
	NO_API UMagicLeapMusicServiceFunctionLibrary(const UMagicLeapMusicServiceFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapMusicServiceFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapMusicServiceFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapMusicServiceFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_14_PROLOG
#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_INCLASS \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPMUSICSERVICE_API UClass* StaticClass<class UMagicLeapMusicServiceFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
