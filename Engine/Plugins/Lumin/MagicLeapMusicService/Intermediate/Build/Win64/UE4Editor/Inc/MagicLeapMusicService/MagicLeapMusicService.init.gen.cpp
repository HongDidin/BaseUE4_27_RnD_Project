// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapMusicService_init() {}
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapMusicService()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapMusicService",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xA1505432,
				0x2670AD39,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
