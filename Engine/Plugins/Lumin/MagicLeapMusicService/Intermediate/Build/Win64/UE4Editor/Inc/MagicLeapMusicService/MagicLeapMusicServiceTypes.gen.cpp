// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapMusicService/Public/MagicLeapMusicServiceTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapMusicServiceTypes() {}
// Cross Module References
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapMusicService();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimespan();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState();
	MAGICLEAPMUSICSERVICE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState();
	MAGICLEAPMUSICSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceVolumeDelegate_Parms
		{
			float Volume;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Volume_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Volume;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::NewProp_Volume_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::NewProp_Volume = { "Volume", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceVolumeDelegate_Parms, Volume), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::NewProp_Volume_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::NewProp_Volume_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::NewProp_Volume,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServiceVolumeDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceVolumeDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceStatusDelegate_Parms
		{
			EMagicLeapMusicServiceStatus Status;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Status_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Status_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status = { "Status", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceStatusDelegate_Parms, Status), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::NewProp_Status,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServiceStatusDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceStatusDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceErrorDelegate_Parms
		{
			EMagicLeapMusicServiceProviderError Error;
			int32 ErrorCode;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Error_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Error_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Error;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ErrorCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ErrorCode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error = { "Error", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceErrorDelegate_Parms, Error), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_ErrorCode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_ErrorCode = { "ErrorCode", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceErrorDelegate_Parms, ErrorCode), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_ErrorCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_ErrorCode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_Error,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::NewProp_ErrorCode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServiceErrorDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceErrorDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServicePositionDelegate_Parms
		{
			FTimespan Position;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::NewProp_Position_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServicePositionDelegate_Parms, Position), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::NewProp_Position_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::NewProp_Position,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServicePositionDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServicePositionDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceMetadataDelegate_Parms
		{
			FMagicLeapMusicServiceTrackMetadata Metadata;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::NewProp_Metadata_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceMetadataDelegate_Parms, Metadata), Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::NewProp_Metadata,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServiceMetadataDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceMetadataDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceShuffleStateDelegate_Parms
		{
			EMagicLeapMusicServicePlaybackShuffleState ShuffleState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShuffleState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShuffleState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShuffleState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState = { "ShuffleState", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceShuffleStateDelegate_Parms, ShuffleState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::NewProp_ShuffleState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceShuffleStateDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceRepeatStateDelegate_Parms
		{
			EMagicLeapMusicServicePlaybackRepeatState RepeatState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RepeatState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepeatState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RepeatState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState = { "RepeatState", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceRepeatStateDelegate_Parms, RepeatState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::NewProp_RepeatState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServiceRepeatStateDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapMusicService_eventMagicLeapMusicServicePlaybackStateDelegate_Parms
		{
			EMagicLeapMusicServicePlaybackState PlaybackState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlaybackState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PlaybackState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState = { "PlaybackState", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapMusicService_eventMagicLeapMusicServicePlaybackStateDelegate_Parms, PlaybackState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::NewProp_PlaybackState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegates used for music service callbacks. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "Delegates used for music service callbacks." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService, nullptr, "MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapMusicService_eventMagicLeapMusicServicePlaybackStateDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMagicLeapMusicServiceStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("EMagicLeapMusicServiceStatus"));
		}
		return Singleton;
	}
	template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServiceStatus>()
	{
		return EMagicLeapMusicServiceStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapMusicServiceStatus(EMagicLeapMusicServiceStatus_StaticEnum, TEXT("/Script/MagicLeapMusicService"), TEXT("EMagicLeapMusicServiceStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus_Hash() { return 1600988691U; }
	UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapMusicServiceStatus"), 0, Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapMusicServiceStatus::ContextChanged", (int64)EMagicLeapMusicServiceStatus::ContextChanged },
				{ "EMagicLeapMusicServiceStatus::Created", (int64)EMagicLeapMusicServiceStatus::Created },
				{ "EMagicLeapMusicServiceStatus::LoggedIn", (int64)EMagicLeapMusicServiceStatus::LoggedIn },
				{ "EMagicLeapMusicServiceStatus::LoggedOut", (int64)EMagicLeapMusicServiceStatus::LoggedOut },
				{ "EMagicLeapMusicServiceStatus::NextTrack", (int64)EMagicLeapMusicServiceStatus::NextTrack },
				{ "EMagicLeapMusicServiceStatus::PreviousTrack", (int64)EMagicLeapMusicServiceStatus::PreviousTrack },
				{ "EMagicLeapMusicServiceStatus::TrackChanged", (int64)EMagicLeapMusicServiceStatus::TrackChanged },
				{ "EMagicLeapMusicServiceStatus::Unknown", (int64)EMagicLeapMusicServiceStatus::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ContextChanged.Comment", "/** The music service provider's context has changed */" },
				{ "ContextChanged.Name", "EMagicLeapMusicServiceStatus::ContextChanged" },
				{ "ContextChanged.ToolTip", "The music service provider's context has changed" },
				{ "Created.Comment", "/** The music service provider has successfully been created */" },
				{ "Created.Name", "EMagicLeapMusicServiceStatus::Created" },
				{ "Created.ToolTip", "The music service provider has successfully been created" },
				{ "LoggedIn.Comment", "/** Client has successfully logged into the connected music service provider */" },
				{ "LoggedIn.Name", "EMagicLeapMusicServiceStatus::LoggedIn" },
				{ "LoggedIn.ToolTip", "Client has successfully logged into the connected music service provider" },
				{ "LoggedOut.Comment", "/** Client has successfully logged out of the connected music service provider */" },
				{ "LoggedOut.Name", "EMagicLeapMusicServiceStatus::LoggedOut" },
				{ "LoggedOut.ToolTip", "Client has successfully logged out of the connected music service provider" },
				{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
				{ "NextTrack.Comment", "/** Music service provider has advanced the current track */" },
				{ "NextTrack.Name", "EMagicLeapMusicServiceStatus::NextTrack" },
				{ "NextTrack.ToolTip", "Music service provider has advanced the current track" },
				{ "PreviousTrack.Comment", "/** Music service provider has rewinded the current track */" },
				{ "PreviousTrack.Name", "EMagicLeapMusicServiceStatus::PreviousTrack" },
				{ "PreviousTrack.ToolTip", "Music service provider has rewinded the current track" },
				{ "TrackChanged.Comment", "/** Music service provider has changed the track */" },
				{ "TrackChanged.Name", "EMagicLeapMusicServiceStatus::TrackChanged" },
				{ "TrackChanged.ToolTip", "Music service provider has changed the track" },
				{ "Unknown.Comment", "/** The current status of the music service provider is not known */" },
				{ "Unknown.Name", "EMagicLeapMusicServiceStatus::Unknown" },
				{ "Unknown.ToolTip", "The current status of the music service provider is not known" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
				nullptr,
				"EMagicLeapMusicServiceStatus",
				"EMagicLeapMusicServiceStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapMusicServicePlaybackRepeatState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("EMagicLeapMusicServicePlaybackRepeatState"));
		}
		return Singleton;
	}
	template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServicePlaybackRepeatState>()
	{
		return EMagicLeapMusicServicePlaybackRepeatState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapMusicServicePlaybackRepeatState(EMagicLeapMusicServicePlaybackRepeatState_StaticEnum, TEXT("/Script/MagicLeapMusicService"), TEXT("EMagicLeapMusicServicePlaybackRepeatState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState_Hash() { return 4100582238U; }
	UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapMusicServicePlaybackRepeatState"), 0, Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapMusicServicePlaybackRepeatState::Off", (int64)EMagicLeapMusicServicePlaybackRepeatState::Off },
				{ "EMagicLeapMusicServicePlaybackRepeatState::Song", (int64)EMagicLeapMusicServicePlaybackRepeatState::Song },
				{ "EMagicLeapMusicServicePlaybackRepeatState::Album", (int64)EMagicLeapMusicServicePlaybackRepeatState::Album },
				{ "EMagicLeapMusicServicePlaybackRepeatState::Unkown", (int64)EMagicLeapMusicServicePlaybackRepeatState::Unkown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Album.Comment", "/** Enable playlist playback repetition */" },
				{ "Album.Name", "EMagicLeapMusicServicePlaybackRepeatState::Album" },
				{ "Album.ToolTip", "Enable playlist playback repetition" },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
				{ "Off.Comment", "/** Disable playback repetition */" },
				{ "Off.Name", "EMagicLeapMusicServicePlaybackRepeatState::Off" },
				{ "Off.ToolTip", "Disable playback repetition" },
				{ "Song.Comment", "/** Enable single track playback repetition */" },
				{ "Song.Name", "EMagicLeapMusicServicePlaybackRepeatState::Song" },
				{ "Song.ToolTip", "Enable single track playback repetition" },
				{ "Unkown.Comment", "/** Repeat state of music service is not known */" },
				{ "Unkown.Name", "EMagicLeapMusicServicePlaybackRepeatState::Unkown" },
				{ "Unkown.ToolTip", "Repeat state of music service is not known" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
				nullptr,
				"EMagicLeapMusicServicePlaybackRepeatState",
				"EMagicLeapMusicServicePlaybackRepeatState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapMusicServicePlaybackShuffleState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("EMagicLeapMusicServicePlaybackShuffleState"));
		}
		return Singleton;
	}
	template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServicePlaybackShuffleState>()
	{
		return EMagicLeapMusicServicePlaybackShuffleState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapMusicServicePlaybackShuffleState(EMagicLeapMusicServicePlaybackShuffleState_StaticEnum, TEXT("/Script/MagicLeapMusicService"), TEXT("EMagicLeapMusicServicePlaybackShuffleState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState_Hash() { return 4059900448U; }
	UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapMusicServicePlaybackShuffleState"), 0, Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapMusicServicePlaybackShuffleState::On", (int64)EMagicLeapMusicServicePlaybackShuffleState::On },
				{ "EMagicLeapMusicServicePlaybackShuffleState::Off", (int64)EMagicLeapMusicServicePlaybackShuffleState::Off },
				{ "EMagicLeapMusicServicePlaybackShuffleState::Unknown", (int64)EMagicLeapMusicServicePlaybackShuffleState::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
				{ "Off.Comment", "/** Disable shuffling */" },
				{ "Off.Name", "EMagicLeapMusicServicePlaybackShuffleState::Off" },
				{ "Off.ToolTip", "Disable shuffling" },
				{ "On.Comment", "/** Enable shuffling */" },
				{ "On.Name", "EMagicLeapMusicServicePlaybackShuffleState::On" },
				{ "On.ToolTip", "Enable shuffling" },
				{ "Unknown.Comment", "/** Shuffle state of music service is not known */" },
				{ "Unknown.Name", "EMagicLeapMusicServicePlaybackShuffleState::Unknown" },
				{ "Unknown.ToolTip", "Shuffle state of music service is not known" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
				nullptr,
				"EMagicLeapMusicServicePlaybackShuffleState",
				"EMagicLeapMusicServicePlaybackShuffleState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapMusicServicePlaybackState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("EMagicLeapMusicServicePlaybackState"));
		}
		return Singleton;
	}
	template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServicePlaybackState>()
	{
		return EMagicLeapMusicServicePlaybackState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapMusicServicePlaybackState(EMagicLeapMusicServicePlaybackState_StaticEnum, TEXT("/Script/MagicLeapMusicService"), TEXT("EMagicLeapMusicServicePlaybackState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState_Hash() { return 1881612300U; }
	UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapMusicServicePlaybackState"), 0, Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapMusicServicePlaybackState::Playing", (int64)EMagicLeapMusicServicePlaybackState::Playing },
				{ "EMagicLeapMusicServicePlaybackState::Paused", (int64)EMagicLeapMusicServicePlaybackState::Paused },
				{ "EMagicLeapMusicServicePlaybackState::Stopped", (int64)EMagicLeapMusicServicePlaybackState::Stopped },
				{ "EMagicLeapMusicServicePlaybackState::Error", (int64)EMagicLeapMusicServicePlaybackState::Error },
				{ "EMagicLeapMusicServicePlaybackState::Unknown", (int64)EMagicLeapMusicServicePlaybackState::Unknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Error.Comment", "/** Music service has errored */" },
				{ "Error.Name", "EMagicLeapMusicServicePlaybackState::Error" },
				{ "Error.ToolTip", "Music service has errored" },
				{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
				{ "Paused.Comment", "/** Music service is currently paused */" },
				{ "Paused.Name", "EMagicLeapMusicServicePlaybackState::Paused" },
				{ "Paused.ToolTip", "Music service is currently paused" },
				{ "Playing.Comment", "/** Music service is currently playing */" },
				{ "Playing.Name", "EMagicLeapMusicServicePlaybackState::Playing" },
				{ "Playing.ToolTip", "Music service is currently playing" },
				{ "Stopped.Comment", "/** Music service is currently stopped */" },
				{ "Stopped.Name", "EMagicLeapMusicServicePlaybackState::Stopped" },
				{ "Stopped.ToolTip", "Music service is currently stopped" },
				{ "Unknown.Comment", "/** Playback state of the music service is not known */" },
				{ "Unknown.Name", "EMagicLeapMusicServicePlaybackState::Unknown" },
				{ "Unknown.ToolTip", "Playback state of the music service is not known" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
				nullptr,
				"EMagicLeapMusicServicePlaybackState",
				"EMagicLeapMusicServicePlaybackState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapMusicServiceProviderError_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("EMagicLeapMusicServiceProviderError"));
		}
		return Singleton;
	}
	template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServiceProviderError>()
	{
		return EMagicLeapMusicServiceProviderError_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapMusicServiceProviderError(EMagicLeapMusicServiceProviderError_StaticEnum, TEXT("/Script/MagicLeapMusicService"), TEXT("EMagicLeapMusicServiceProviderError"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError_Hash() { return 1944396926U; }
	UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapMusicServiceProviderError"), 0, Get_Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapMusicServiceProviderError::None", (int64)EMagicLeapMusicServiceProviderError::None },
				{ "EMagicLeapMusicServiceProviderError::Connectivity", (int64)EMagicLeapMusicServiceProviderError::Connectivity },
				{ "EMagicLeapMusicServiceProviderError::Timeout", (int64)EMagicLeapMusicServiceProviderError::Timeout },
				{ "EMagicLeapMusicServiceProviderError::GeneralPlayback", (int64)EMagicLeapMusicServiceProviderError::GeneralPlayback },
				{ "EMagicLeapMusicServiceProviderError::Privilege", (int64)EMagicLeapMusicServiceProviderError::Privilege },
				{ "EMagicLeapMusicServiceProviderError::ServiceSpecific", (int64)EMagicLeapMusicServiceProviderError::ServiceSpecific },
				{ "EMagicLeapMusicServiceProviderError::NoMemory", (int64)EMagicLeapMusicServiceProviderError::NoMemory },
				{ "EMagicLeapMusicServiceProviderError::Unspecified", (int64)EMagicLeapMusicServiceProviderError::Unspecified },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Connectivity.Comment", "/** Music service has reported a connectivity error */" },
				{ "Connectivity.Name", "EMagicLeapMusicServiceProviderError::Connectivity" },
				{ "Connectivity.ToolTip", "Music service has reported a connectivity error" },
				{ "GeneralPlayback.Comment", "/** Music service has reported a general playback error */" },
				{ "GeneralPlayback.Name", "EMagicLeapMusicServiceProviderError::GeneralPlayback" },
				{ "GeneralPlayback.ToolTip", "Music service has reported a general playback error" },
				{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
				{ "NoMemory.Comment", "/** Music service has reported it has no available memory */" },
				{ "NoMemory.Name", "EMagicLeapMusicServiceProviderError::NoMemory" },
				{ "NoMemory.ToolTip", "Music service has reported it has no available memory" },
				{ "None.Comment", "/** Music service has not reported any errors */" },
				{ "None.Name", "EMagicLeapMusicServiceProviderError::None" },
				{ "None.ToolTip", "Music service has not reported any errors" },
				{ "Privilege.Comment", "/** Music service has reported an app privilege error */" },
				{ "Privilege.Name", "EMagicLeapMusicServiceProviderError::Privilege" },
				{ "Privilege.ToolTip", "Music service has reported an app privilege error" },
				{ "ServiceSpecific.Comment", "/** Music service has reported an error specific to it's implementation */" },
				{ "ServiceSpecific.Name", "EMagicLeapMusicServiceProviderError::ServiceSpecific" },
				{ "ServiceSpecific.ToolTip", "Music service has reported an error specific to it's implementation" },
				{ "Timeout.Comment", "/** Music service has reported a timeout */" },
				{ "Timeout.Name", "EMagicLeapMusicServiceProviderError::Timeout" },
				{ "Timeout.ToolTip", "Music service has reported a timeout" },
				{ "Unspecified.Comment", "/** Music service has reported an unspecified error */" },
				{ "Unspecified.Name", "EMagicLeapMusicServiceProviderError::Unspecified" },
				{ "Unspecified.ToolTip", "Music service has reported an unspecified error" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
				nullptr,
				"EMagicLeapMusicServiceProviderError",
				"EMagicLeapMusicServiceProviderError",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMagicLeapMusicServiceCallbacks::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPMUSICSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("MagicLeapMusicServiceCallbacks"), sizeof(FMagicLeapMusicServiceCallbacks), Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPMUSICSERVICE_API UScriptStruct* StaticStruct<FMagicLeapMusicServiceCallbacks>()
{
	return FMagicLeapMusicServiceCallbacks::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapMusicServiceCallbacks(FMagicLeapMusicServiceCallbacks::StaticStruct, TEXT("/Script/MagicLeapMusicService"), TEXT("MagicLeapMusicServiceCallbacks"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapMusicService_StaticRegisterNativesFMagicLeapMusicServiceCallbacks
{
	FScriptStruct_MagicLeapMusicService_StaticRegisterNativesFMagicLeapMusicServiceCallbacks()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapMusicServiceCallbacks>(FName(TEXT("MagicLeapMusicServiceCallbacks")));
	}
} ScriptStruct_MagicLeapMusicService_StaticRegisterNativesFMagicLeapMusicServiceCallbacks;
	struct Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackStateDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_PlaybackStateDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepeatStateDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_RepeatStateDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShuffleStateDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ShuffleStateDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetadataDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_MetadataDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_PositionDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ErrorDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ErrorDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StatusDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_StatusDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_VolumeDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapMusicServiceCallbacks>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PlaybackStateDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PlaybackStateDelegate = { "PlaybackStateDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, PlaybackStateDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePlaybackStateDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PlaybackStateDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PlaybackStateDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_RepeatStateDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_RepeatStateDelegate = { "RepeatStateDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, RepeatStateDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceRepeatStateDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_RepeatStateDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_RepeatStateDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ShuffleStateDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ShuffleStateDelegate = { "ShuffleStateDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, ShuffleStateDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceShuffleStateDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ShuffleStateDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ShuffleStateDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_MetadataDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_MetadataDelegate = { "MetadataDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, MetadataDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceMetadataDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_MetadataDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_MetadataDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PositionDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PositionDelegate = { "PositionDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, PositionDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServicePositionDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PositionDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PositionDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ErrorDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ErrorDelegate = { "ErrorDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, ErrorDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceErrorDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ErrorDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ErrorDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_StatusDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_StatusDelegate = { "StatusDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, StatusDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceStatusDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_StatusDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_StatusDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_VolumeDelegate_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_VolumeDelegate = { "VolumeDelegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceCallbacks, VolumeDelegate), Z_Construct_UDelegateFunction_MagicLeapMusicService_MagicLeapMusicServiceVolumeDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_VolumeDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_VolumeDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PlaybackStateDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_RepeatStateDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ShuffleStateDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_MetadataDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_PositionDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_ErrorDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_StatusDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::NewProp_VolumeDelegate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
		nullptr,
		&NewStructOps,
		"MagicLeapMusicServiceCallbacks",
		sizeof(FMagicLeapMusicServiceCallbacks),
		alignof(FMagicLeapMusicServiceCallbacks),
		Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapMusicServiceCallbacks"), sizeof(FMagicLeapMusicServiceCallbacks), Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Hash() { return 58309260U; }
class UScriptStruct* FMagicLeapMusicServiceTrackMetadata::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPMUSICSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata, Z_Construct_UPackage__Script_MagicLeapMusicService(), TEXT("MagicLeapMusicServiceTrackMetadata"), sizeof(FMagicLeapMusicServiceTrackMetadata), Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPMUSICSERVICE_API UScriptStruct* StaticStruct<FMagicLeapMusicServiceTrackMetadata>()
{
	return FMagicLeapMusicServiceTrackMetadata::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata(FMagicLeapMusicServiceTrackMetadata::StaticStruct, TEXT("/Script/MagicLeapMusicService"), TEXT("MagicLeapMusicServiceTrackMetadata"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapMusicService_StaticRegisterNativesFMagicLeapMusicServiceTrackMetadata
{
	FScriptStruct_MagicLeapMusicService_StaticRegisterNativesFMagicLeapMusicServiceTrackMetadata()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapMusicServiceTrackMetadata>(FName(TEXT("MagicLeapMusicServiceTrackMetadata")));
	}
} ScriptStruct_MagicLeapMusicService_StaticRegisterNativesFMagicLeapMusicServiceTrackMetadata;
	struct Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackTitle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TrackTitle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlbumName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AlbumName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlbumURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AlbumURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlbumCoverURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AlbumCoverURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArtistName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ArtistName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArtistURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ArtistURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TrackLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapMusicServiceTrackMetadata>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackTitle_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** Title of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "Title of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackTitle = { "TrackTitle", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, TrackTitle), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackTitle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackTitle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumName_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** Name of the album of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "Name of the album of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumName = { "AlbumName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, AlbumName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumURL_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** URL to the album of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "URL to the album of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumURL = { "AlbumURL", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, AlbumURL), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumCoverURL_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** URL to the album cover of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "URL to the album cover of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumCoverURL = { "AlbumCoverURL", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, AlbumCoverURL), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumCoverURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumCoverURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistName_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** Artist name of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "Artist name of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistName = { "ArtistName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, ArtistName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistURL_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** Artist URL of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "Artist URL of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistURL = { "ArtistURL", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, ArtistURL), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackLength_MetaData[] = {
		{ "Category", "Music Service|MagicLeap" },
		{ "Comment", "/** Runtime length of the currently selected track */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceTypes.h" },
		{ "ToolTip", "Runtime length of the currently selected track" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackLength = { "TrackLength", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMusicServiceTrackMetadata, TrackLength), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackTitle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_AlbumCoverURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_ArtistURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::NewProp_TrackLength,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
		nullptr,
		&NewStructOps,
		"MagicLeapMusicServiceTrackMetadata",
		sizeof(FMagicLeapMusicServiceTrackMetadata),
		alignof(FMagicLeapMusicServiceTrackMetadata),
		Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMusicService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapMusicServiceTrackMetadata"), sizeof(FMagicLeapMusicServiceTrackMetadata), Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Hash() { return 575047735U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
