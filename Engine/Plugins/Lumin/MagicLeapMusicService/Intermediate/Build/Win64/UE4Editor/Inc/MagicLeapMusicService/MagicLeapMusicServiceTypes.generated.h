// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapMusicServiceStatus : uint8;
enum class EMagicLeapMusicServiceProviderError : uint8;
struct FTimespan;
struct FMagicLeapMusicServiceTrackMetadata;
enum class EMagicLeapMusicServicePlaybackShuffleState : uint8;
enum class EMagicLeapMusicServicePlaybackRepeatState : uint8;
enum class EMagicLeapMusicServicePlaybackState : uint8;
#ifdef MAGICLEAPMUSICSERVICE_MagicLeapMusicServiceTypes_generated_h
#error "MagicLeapMusicServiceTypes.generated.h already included, missing '#pragma once' in MagicLeapMusicServiceTypes.h"
#endif
#define MAGICLEAPMUSICSERVICE_MagicLeapMusicServiceTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_139_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPMUSICSERVICE_API UScriptStruct* StaticStruct<struct FMagicLeapMusicServiceCallbacks>();

#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_92_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPMUSICSERVICE_API UScriptStruct* StaticStruct<struct FMagicLeapMusicServiceTrackMetadata>();

#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_133_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceVolumeDelegate_Parms \
{ \
	float Volume; \
}; \
static inline void FMagicLeapMusicServiceVolumeDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServiceVolumeDelegate, const float Volume) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServiceVolumeDelegate_Parms Parms; \
	Parms.Volume=Volume; \
	MagicLeapMusicServiceVolumeDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_132_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceStatusDelegate_Parms \
{ \
	EMagicLeapMusicServiceStatus Status; \
}; \
static inline void FMagicLeapMusicServiceStatusDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServiceStatusDelegate, const EMagicLeapMusicServiceStatus Status) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServiceStatusDelegate_Parms Parms; \
	Parms.Status=Status; \
	MagicLeapMusicServiceStatusDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_131_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceErrorDelegate_Parms \
{ \
	EMagicLeapMusicServiceProviderError Error; \
	int32 ErrorCode; \
}; \
static inline void FMagicLeapMusicServiceErrorDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServiceErrorDelegate, const EMagicLeapMusicServiceProviderError Error, const int32 ErrorCode) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServiceErrorDelegate_Parms Parms; \
	Parms.Error=Error; \
	Parms.ErrorCode=ErrorCode; \
	MagicLeapMusicServiceErrorDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_130_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServicePositionDelegate_Parms \
{ \
	FTimespan Position; \
}; \
static inline void FMagicLeapMusicServicePositionDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServicePositionDelegate, const FTimespan Position) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServicePositionDelegate_Parms Parms; \
	Parms.Position=Position; \
	MagicLeapMusicServicePositionDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_129_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceMetadataDelegate_Parms \
{ \
	FMagicLeapMusicServiceTrackMetadata Metadata; \
}; \
static inline void FMagicLeapMusicServiceMetadataDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServiceMetadataDelegate, FMagicLeapMusicServiceTrackMetadata const& Metadata) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServiceMetadataDelegate_Parms Parms; \
	Parms.Metadata=Metadata; \
	MagicLeapMusicServiceMetadataDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_128_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceShuffleStateDelegate_Parms \
{ \
	EMagicLeapMusicServicePlaybackShuffleState ShuffleState; \
}; \
static inline void FMagicLeapMusicServiceShuffleStateDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServiceShuffleStateDelegate, const EMagicLeapMusicServicePlaybackShuffleState ShuffleState) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServiceShuffleStateDelegate_Parms Parms; \
	Parms.ShuffleState=ShuffleState; \
	MagicLeapMusicServiceShuffleStateDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_127_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServiceRepeatStateDelegate_Parms \
{ \
	EMagicLeapMusicServicePlaybackRepeatState RepeatState; \
}; \
static inline void FMagicLeapMusicServiceRepeatStateDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServiceRepeatStateDelegate, const EMagicLeapMusicServicePlaybackRepeatState RepeatState) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServiceRepeatStateDelegate_Parms Parms; \
	Parms.RepeatState=RepeatState; \
	MagicLeapMusicServiceRepeatStateDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h_126_DELEGATE \
struct _Script_MagicLeapMusicService_eventMagicLeapMusicServicePlaybackStateDelegate_Parms \
{ \
	EMagicLeapMusicServicePlaybackState PlaybackState; \
}; \
static inline void FMagicLeapMusicServicePlaybackStateDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMusicServicePlaybackStateDelegate, const EMagicLeapMusicServicePlaybackState PlaybackState) \
{ \
	_Script_MagicLeapMusicService_eventMagicLeapMusicServicePlaybackStateDelegate_Parms Parms; \
	Parms.PlaybackState=PlaybackState; \
	MagicLeapMusicServicePlaybackStateDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapMusicService_Source_MagicLeapMusicService_Public_MagicLeapMusicServiceTypes_h


#define FOREACH_ENUM_EMAGICLEAPMUSICSERVICESTATUS(op) \
	op(EMagicLeapMusicServiceStatus::ContextChanged) \
	op(EMagicLeapMusicServiceStatus::Created) \
	op(EMagicLeapMusicServiceStatus::LoggedIn) \
	op(EMagicLeapMusicServiceStatus::LoggedOut) \
	op(EMagicLeapMusicServiceStatus::NextTrack) \
	op(EMagicLeapMusicServiceStatus::PreviousTrack) \
	op(EMagicLeapMusicServiceStatus::TrackChanged) \
	op(EMagicLeapMusicServiceStatus::Unknown) 

enum class EMagicLeapMusicServiceStatus : uint8;
template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServiceStatus>();

#define FOREACH_ENUM_EMAGICLEAPMUSICSERVICEPLAYBACKREPEATSTATE(op) \
	op(EMagicLeapMusicServicePlaybackRepeatState::Off) \
	op(EMagicLeapMusicServicePlaybackRepeatState::Song) \
	op(EMagicLeapMusicServicePlaybackRepeatState::Album) \
	op(EMagicLeapMusicServicePlaybackRepeatState::Unkown) 

enum class EMagicLeapMusicServicePlaybackRepeatState : uint8;
template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServicePlaybackRepeatState>();

#define FOREACH_ENUM_EMAGICLEAPMUSICSERVICEPLAYBACKSHUFFLESTATE(op) \
	op(EMagicLeapMusicServicePlaybackShuffleState::On) \
	op(EMagicLeapMusicServicePlaybackShuffleState::Off) \
	op(EMagicLeapMusicServicePlaybackShuffleState::Unknown) 

enum class EMagicLeapMusicServicePlaybackShuffleState : uint8;
template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServicePlaybackShuffleState>();

#define FOREACH_ENUM_EMAGICLEAPMUSICSERVICEPLAYBACKSTATE(op) \
	op(EMagicLeapMusicServicePlaybackState::Playing) \
	op(EMagicLeapMusicServicePlaybackState::Paused) \
	op(EMagicLeapMusicServicePlaybackState::Stopped) \
	op(EMagicLeapMusicServicePlaybackState::Error) \
	op(EMagicLeapMusicServicePlaybackState::Unknown) 

enum class EMagicLeapMusicServicePlaybackState : uint8;
template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServicePlaybackState>();

#define FOREACH_ENUM_EMAGICLEAPMUSICSERVICEPROVIDERERROR(op) \
	op(EMagicLeapMusicServiceProviderError::None) \
	op(EMagicLeapMusicServiceProviderError::Connectivity) \
	op(EMagicLeapMusicServiceProviderError::Timeout) \
	op(EMagicLeapMusicServiceProviderError::GeneralPlayback) \
	op(EMagicLeapMusicServiceProviderError::Privilege) \
	op(EMagicLeapMusicServiceProviderError::ServiceSpecific) \
	op(EMagicLeapMusicServiceProviderError::NoMemory) \
	op(EMagicLeapMusicServiceProviderError::Unspecified) 

enum class EMagicLeapMusicServiceProviderError : uint8;
template<> MAGICLEAPMUSICSERVICE_API UEnum* StaticEnum<EMagicLeapMusicServiceProviderError>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
