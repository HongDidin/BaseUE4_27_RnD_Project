// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapMusicService/Public/MagicLeapMusicServiceFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapMusicServiceFunctionLibrary() {}
// Cross Module References
	MAGICLEAPMUSICSERVICE_API UClass* Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_NoRegister();
	MAGICLEAPMUSICSERVICE_API UClass* Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapMusicService();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimespan();
	MAGICLEAPMUSICSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError();
	MAGICLEAPMUSICSERVICE_API UEnum* Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus();
	MAGICLEAPMUSICSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetCurrentTrackMetadata)
	{
		P_GET_STRUCT_REF(FMagicLeapMusicServiceTrackMetadata,Z_Param_Out_Metadata);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetCurrentTrackMetadata(Z_Param_Out_Metadata);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetServiceStatus)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServiceStatus,Z_Param_Out_Status);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetServiceStatus((EMagicLeapMusicServiceStatus&)(Z_Param_Out_Status));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetCurrentPosition)
	{
		P_GET_STRUCT_REF(FTimespan,Z_Param_Out_Position);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetCurrentPosition(Z_Param_Out_Position);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetTrackLength)
	{
		P_GET_STRUCT_REF(FTimespan,Z_Param_Out_Length);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetTrackLength(Z_Param_Out_Length);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackVolume)
	{
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_Volume);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetPlaybackVolume(Z_Param_Out_Volume);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackVolume)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Volume);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetPlaybackVolume(Z_Param_Volume);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackRepeatState)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServicePlaybackRepeatState,Z_Param_Out_RepeatState);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetPlaybackRepeatState((EMagicLeapMusicServicePlaybackRepeatState&)(Z_Param_Out_RepeatState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackRepeatState)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServicePlaybackRepeatState,Z_Param_Out_RepeatState);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetPlaybackRepeatState((EMagicLeapMusicServicePlaybackRepeatState&)(Z_Param_Out_RepeatState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackShuffleState)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServicePlaybackShuffleState,Z_Param_Out_ShuffleState);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetPlaybackShuffleState((EMagicLeapMusicServicePlaybackShuffleState&)(Z_Param_Out_ShuffleState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackShuffleState)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServicePlaybackShuffleState,Z_Param_Out_ShuffleState);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetPlaybackShuffleState((EMagicLeapMusicServicePlaybackShuffleState&)(Z_Param_Out_ShuffleState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackState)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServicePlaybackState,Z_Param_Out_PlaybackState);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetPlaybackState((EMagicLeapMusicServicePlaybackState&)(Z_Param_Out_PlaybackState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execGetServiceProviderError)
	{
		P_GET_ENUM_REF(EMagicLeapMusicServiceProviderError,Z_Param_Out_ErrorType);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_ErrorCode);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::GetServiceProviderError((EMagicLeapMusicServiceProviderError&)(Z_Param_Out_ErrorType),Z_Param_Out_ErrorCode);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execPreviousTrack)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::PreviousTrack();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execNextTrack)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::NextTrack();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSeekTo)
	{
		P_GET_STRUCT_REF(FTimespan,Z_Param_Out_Position);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SeekTo(Z_Param_Out_Position);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execResumePlayback)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::ResumePlayback();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execPausePlayback)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::PausePlayback();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execStopPlayback)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::StopPlayback();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execStartPlayback)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::StartPlayback();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetPlaylist)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_Playlist);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetPlaylist(Z_Param_Out_Playlist);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackURL)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_PlaybackURL);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetPlaybackURL(Z_Param_PlaybackURL);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetAuthenticationString)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AuthenticationString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetAuthenticationString(Z_Param_AuthenticationString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execSetCallbacks)
	{
		P_GET_STRUCT_REF(FMagicLeapMusicServiceCallbacks,Z_Param_Out_Callbacks);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::SetCallbacks(Z_Param_Out_Callbacks);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execDisconnect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::Disconnect();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMusicServiceFunctionLibrary::execConnect)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Name);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapMusicServiceFunctionLibrary::Connect(Z_Param_Name);
		P_NATIVE_END;
	}
	void UMagicLeapMusicServiceFunctionLibrary::StaticRegisterNativesUMagicLeapMusicServiceFunctionLibrary()
	{
		UClass* Class = UMagicLeapMusicServiceFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Connect", &UMagicLeapMusicServiceFunctionLibrary::execConnect },
			{ "Disconnect", &UMagicLeapMusicServiceFunctionLibrary::execDisconnect },
			{ "GetCurrentPosition", &UMagicLeapMusicServiceFunctionLibrary::execGetCurrentPosition },
			{ "GetCurrentTrackMetadata", &UMagicLeapMusicServiceFunctionLibrary::execGetCurrentTrackMetadata },
			{ "GetPlaybackRepeatState", &UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackRepeatState },
			{ "GetPlaybackShuffleState", &UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackShuffleState },
			{ "GetPlaybackState", &UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackState },
			{ "GetPlaybackVolume", &UMagicLeapMusicServiceFunctionLibrary::execGetPlaybackVolume },
			{ "GetServiceProviderError", &UMagicLeapMusicServiceFunctionLibrary::execGetServiceProviderError },
			{ "GetServiceStatus", &UMagicLeapMusicServiceFunctionLibrary::execGetServiceStatus },
			{ "GetTrackLength", &UMagicLeapMusicServiceFunctionLibrary::execGetTrackLength },
			{ "NextTrack", &UMagicLeapMusicServiceFunctionLibrary::execNextTrack },
			{ "PausePlayback", &UMagicLeapMusicServiceFunctionLibrary::execPausePlayback },
			{ "PreviousTrack", &UMagicLeapMusicServiceFunctionLibrary::execPreviousTrack },
			{ "ResumePlayback", &UMagicLeapMusicServiceFunctionLibrary::execResumePlayback },
			{ "SeekTo", &UMagicLeapMusicServiceFunctionLibrary::execSeekTo },
			{ "SetAuthenticationString", &UMagicLeapMusicServiceFunctionLibrary::execSetAuthenticationString },
			{ "SetCallbacks", &UMagicLeapMusicServiceFunctionLibrary::execSetCallbacks },
			{ "SetPlaybackRepeatState", &UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackRepeatState },
			{ "SetPlaybackShuffleState", &UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackShuffleState },
			{ "SetPlaybackURL", &UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackURL },
			{ "SetPlaybackVolume", &UMagicLeapMusicServiceFunctionLibrary::execSetPlaybackVolume },
			{ "SetPlaylist", &UMagicLeapMusicServiceFunctionLibrary::execSetPlaylist },
			{ "StartPlayback", &UMagicLeapMusicServiceFunctionLibrary::execStartPlayback },
			{ "StopPlayback", &UMagicLeapMusicServiceFunctionLibrary::execStopPlayback },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventConnect_Parms
		{
			FString Name;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventConnect_Parms, Name), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_Name_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventConnect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventConnect_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Connects to a music service provider\n\n\x09\x09@param Name  Name of the music service provider to connect to.\n\x09\x09@return True if successfully connected to the music provider with name 'Name', false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Connects to a music service provider\n\n@param Name  Name of the music service provider to connect to.\n@return True if successfully connected to the music provider with name 'Name', false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "Connect", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventConnect_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventDisconnect_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventDisconnect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventDisconnect_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Disconnects from the currently connected service provider.\n\n\x09\x09@return True if successfully disconnected to the currently connected music provider, false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Disconnects from the currently connected service provider.\n\n@return True if successfully disconnected to the currently connected music provider, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "Disconnect", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventDisconnect_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetCurrentPosition_Parms
		{
			FTimespan Position;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetCurrentPosition_Parms, Position), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetCurrentPosition_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetCurrentPosition_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the current playback position of the currently selected track\n\n\x09\x09@param Position Position of playback of the current track\n\x09\x09@return True if retrieving the current track position succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the current playback position of the currently selected track\n\n@param Position Position of playback of the current track\n@return True if retrieving the current track position succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetCurrentPosition", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetCurrentPosition_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetCurrentTrackMetadata_Parms
		{
			FMagicLeapMusicServiceTrackMetadata Metadata;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Metadata;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetCurrentTrackMetadata_Parms, Metadata), Z_Construct_UScriptStruct_FMagicLeapMusicServiceTrackMetadata, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetCurrentTrackMetadata_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetCurrentTrackMetadata_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::NewProp_Metadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the currently selected track's metadata\n\n\x09\x09@param Metadata  Metadata of the currently selected track\n\x09\x09@return True if retrieving the current track metadata succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the currently selected track's metadata\n\n@param Metadata  Metadata of the currently selected track\n@return True if retrieving the current track metadata succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetCurrentTrackMetadata", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetCurrentTrackMetadata_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackRepeatState_Parms
		{
			EMagicLeapMusicServicePlaybackRepeatState RepeatState;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RepeatState_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RepeatState;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_RepeatState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_RepeatState = { "RepeatState", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackRepeatState_Parms, RepeatState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackRepeatState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackRepeatState_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_RepeatState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_RepeatState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the current repeat state of the music service provider\n\n\x09\x09@param RepeatState  The repeat state the music service provider is currently set to\n\x09\x09@return True if retrieving the repeat state succeeds\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the current repeat state of the music service provider\n\n@param RepeatState  The repeat state the music service provider is currently set to\n@return True if retrieving the repeat state succeeds" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetPlaybackRepeatState", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackRepeatState_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackShuffleState_Parms
		{
			EMagicLeapMusicServicePlaybackShuffleState ShuffleState;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShuffleState_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShuffleState;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ShuffleState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ShuffleState = { "ShuffleState", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackShuffleState_Parms, ShuffleState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackShuffleState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackShuffleState_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ShuffleState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ShuffleState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the current playback shuffle state\n\n\x09\x09@param ShuffleState  The current playback shuffle state the music provider is set to.\n\x09\x09@return True if retrieving the shuffle state succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the current playback shuffle state\n\n@param ShuffleState  The current playback shuffle state the music provider is set to.\n@return True if retrieving the shuffle state succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetPlaybackShuffleState", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackShuffleState_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackState_Parms
		{
			EMagicLeapMusicServicePlaybackState PlaybackState;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlaybackState_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PlaybackState;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_PlaybackState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_PlaybackState = { "PlaybackState", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackState_Parms, PlaybackState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackState, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackState_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_PlaybackState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_PlaybackState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the current playback state of the music service provider\n\n\x09\x09@return True if retreiving the playback state succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the current playback state of the music service provider\n\n@return True if retreiving the playback state succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetPlaybackState", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackState_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackVolume_Parms
		{
			float Volume;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Volume;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::NewProp_Volume = { "Volume", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackVolume_Parms, Volume), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackVolume_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackVolume_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::NewProp_Volume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the volume of playback the music service provider is currently set to\n\n\x09\x09@param Volume  Volume of current playback\n\x09\x09@return True if the playback volume has sucessfully been retrieved\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the volume of playback the music service provider is currently set to\n\n@param Volume  Volume of current playback\n@return True if the playback volume has sucessfully been retrieved" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetPlaybackVolume", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetPlaybackVolume_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetServiceProviderError_Parms
		{
			EMagicLeapMusicServiceProviderError ErrorType;
			int32 ErrorCode;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ErrorType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ErrorType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ErrorCode;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ErrorType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ErrorType = { "ErrorType", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetServiceProviderError_Parms, ErrorType), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceProviderError, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ErrorCode = { "ErrorCode", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetServiceProviderError_Parms, ErrorCode), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetServiceProviderError_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetServiceProviderError_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ErrorType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ErrorType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ErrorCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the current error status of the music service provider.\n\x09\x09If no error has occurred then this will return the error type 'None'\n\n\x09\x09@return True if retreiving the providers error has succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the current error status of the music service provider.\nIf no error has occurred then this will return the error type 'None'\n\n@return True if retreiving the providers error has succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetServiceProviderError", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetServiceProviderError_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetServiceStatus_Parms
		{
			EMagicLeapMusicServiceStatus Status;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Status_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Status;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_Status_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_Status = { "Status", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetServiceStatus_Parms, Status), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServiceStatus, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetServiceStatus_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetServiceStatus_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_Status_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_Status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the current service status of the music service provider\n\n\x09\x09@param Status Current service status of the music service provider\n\x09\x09@return True if retrieving the service status of the current music service provider succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the current service status of the music service provider\n\n@param Status Current service status of the music service provider\n@return True if retrieving the service status of the current music service provider succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetServiceStatus", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetServiceStatus_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventGetTrackLength_Parms
		{
			FTimespan Length;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Length;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::NewProp_Length = { "Length", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventGetTrackLength_Parms, Length), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventGetTrackLength_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetTrackLength_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::NewProp_Length,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the length of the currently selected track\n\n\x09\x09@param Length  Length of the currently selected track\n\x09\x09@return True if the length of the current track has been sucessfully retrieved\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Gets the length of the currently selected track\n\n@param Length  Length of the currently selected track\n@return True if the length of the current track has been sucessfully retrieved" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "GetTrackLength", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventGetTrackLength_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventNextTrack_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventNextTrack_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventNextTrack_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Advances to the next track on the music service provider's playlist.\n\n\x09\x09@return True if advancing to the next track succeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Advances to the next track on the music service provider's playlist.\n\n@return True if advancing to the next track succeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "NextTrack", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventNextTrack_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventPausePlayback_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventPausePlayback_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventPausePlayback_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Pauses playback of the currently selected track.\n\n\x09\x09@return True if playback paused successfully\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Pauses playback of the currently selected track.\n\n@return True if playback paused successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "PausePlayback", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventPausePlayback_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventPreviousTrack_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventPreviousTrack_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventPreviousTrack_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Rewinds to the previous track on the music provider's playlist\n\n\x09\x09@return True if rewind to the previous track succeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Rewinds to the previous track on the music provider's playlist\n\n@return True if rewind to the previous track succeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "PreviousTrack", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventPreviousTrack_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventResumePlayback_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventResumePlayback_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventResumePlayback_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Resumes playback of the currently selected track.\n\n\x09\x09@return True if playback resumed successfully\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Resumes playback of the currently selected track.\n\n@return True if playback resumed successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "ResumePlayback", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventResumePlayback_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSeekTo_Parms
		{
			FTimespan Position;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_Position_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSeekTo_Parms, Position), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_Position_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSeekTo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSeekTo_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Seeks to a position on the currently selected track.\n\n\x09\x09@param Position Position on the current track to seek to.\n\x09\x09@return True if the seeking 'Position' succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Seeks to a position on the currently selected track.\n\n@param Position Position on the current track to seek to.\n@return True if the seeking 'Position' succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SeekTo", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSeekTo_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetAuthenticationString_Parms
		{
			FString AuthenticationString;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AuthenticationString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AuthenticationString;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_AuthenticationString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_AuthenticationString = { "AuthenticationString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetAuthenticationString_Parms, AuthenticationString), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_AuthenticationString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_AuthenticationString_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetAuthenticationString_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetAuthenticationString_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_AuthenticationString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the authentication string used to connect to the music service provider\n\n\x09\x09@param AuthenticationString  String used to connect to the music service provider\n\x09\x09@return True if successfully set the authentication string, false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets the authentication string used to connect to the music service provider\n\n@param AuthenticationString  String used to connect to the music service provider\n@return True if successfully set the authentication string, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetAuthenticationString", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetAuthenticationString_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetCallbacks_Parms
		{
			FMagicLeapMusicServiceCallbacks Callbacks;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Callbacks_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Callbacks;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_Callbacks_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_Callbacks = { "Callbacks", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetCallbacks_Parms, Callbacks), Z_Construct_UScriptStruct_FMagicLeapMusicServiceCallbacks, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_Callbacks_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_Callbacks_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetCallbacks_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetCallbacks_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_Callbacks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets callbacks the music service provider will utilize.\n\n\x09\x09@return True if successful in setting the music service callbacks, false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets callbacks the music service provider will utilize.\n\n@return True if successful in setting the music service callbacks, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetCallbacks", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetCallbacks_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackRepeatState_Parms
		{
			EMagicLeapMusicServicePlaybackRepeatState RepeatState;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RepeatState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepeatState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RepeatState;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState = { "RepeatState", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackRepeatState_Parms, RepeatState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackRepeatState, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackRepeatState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackRepeatState_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_RepeatState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the repeat state of the music service provider\n\n\x09\x09@param RepeatState  Repeat state to set the music service provider to\n\x09\x09@return True if setting the repeat state to 'RepeatState' succeeded\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets the repeat state of the music service provider\n\n@param RepeatState  Repeat state to set the music service provider to\n@return True if setting the repeat state to 'RepeatState' succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetPlaybackRepeatState", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackRepeatState_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackShuffleState_Parms
		{
			EMagicLeapMusicServicePlaybackShuffleState ShuffleState;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShuffleState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShuffleState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShuffleState;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState = { "ShuffleState", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackShuffleState_Parms, ShuffleState), Z_Construct_UEnum_MagicLeapMusicService_EMagicLeapMusicServicePlaybackShuffleState, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackShuffleState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackShuffleState_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ShuffleState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the current playback shuffle state\n\n\x09\x09@param ShuffleState  The shuffle type to set the current shuffle state to.\n\x09\x09@return True if setting the shuffle state to 'ShuffleState' succeeds\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets the current playback shuffle state\n\n@param ShuffleState  The shuffle type to set the current shuffle state to.\n@return True if setting the shuffle state to 'ShuffleState' succeeds" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetPlaybackShuffleState", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackShuffleState_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackURL_Parms
		{
			FString PlaybackURL;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlaybackURL;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_PlaybackURL_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_PlaybackURL = { "PlaybackURL", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackURL_Parms, PlaybackURL), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_PlaybackURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_PlaybackURL_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackURL_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackURL_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_PlaybackURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the URL to playback using the music service provider\n\n\x09\x09@param PlaybackURL  URL to play via the music service provider\n\x09\x09@return True if successfully set the playback URL, false otherwise\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets the URL to playback using the music service provider\n\n@param PlaybackURL  URL to play via the music service provider\n@return True if successfully set the playback URL, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetPlaybackURL", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackURL_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackVolume_Parms
		{
			float Volume;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Volume_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Volume;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_Volume_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_Volume = { "Volume", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackVolume_Parms, Volume), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_Volume_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_Volume_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackVolume_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackVolume_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_Volume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the volume of playback by the music service provider\n\n\x09\x09@param Volume  Volume to set playback to, should range between 0.0 and 1.0\n\x09\x09@return True if playback volume has successfully been changed to 'Volume'\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets the volume of playback by the music service provider\n\n@param Volume  Volume to set playback to, should range between 0.0 and 1.0\n@return True if playback volume has successfully been changed to 'Volume'" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetPlaybackVolume", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaybackVolume_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventSetPlaylist_Parms
		{
			TArray<FString> Playlist;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Playlist_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Playlist_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Playlist;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist_Inner = { "Playlist", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist = { "Playlist", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMusicServiceFunctionLibrary_eventSetPlaylist_Parms, Playlist), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventSetPlaylist_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaylist_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_Playlist,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the playlist for the music service provider\n\n\x09\x09@param Playlist  Array of track names to be played by the music service provider\n\x09\x09@return True if successfully set the playlist\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Sets the playlist for the music service provider\n\n@param Playlist  Array of track names to be played by the music service provider\n@return True if successfully set the playlist" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "SetPlaylist", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventSetPlaylist_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventStartPlayback_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventStartPlayback_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventStartPlayback_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Starts playback of the currently selected track.\n\n\x09\x09@return True if playback started successfully\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Starts playback of the currently selected track.\n\n@return True if playback started successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "StartPlayback", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventStartPlayback_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics
	{
		struct MagicLeapMusicServiceFunctionLibrary_eventStopPlayback_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMusicServiceFunctionLibrary_eventStopPlayback_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMusicServiceFunctionLibrary_eventStopPlayback_Parms), &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::Function_MetaDataParams[] = {
		{ "Category", "Music Service Function Library|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Stops playback of the currently selected track.\n\n\x09\x09@return True if playback stopped successfully\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Stops playback of the currently selected track.\n\n@return True if playback stopped successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, nullptr, "StopPlayback", nullptr, nullptr, sizeof(MagicLeapMusicServiceFunctionLibrary_eventStopPlayback_Parms), Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_NoRegister()
	{
		return UMagicLeapMusicServiceFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMusicService,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Connect, "Connect" }, // 387280038
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_Disconnect, "Disconnect" }, // 2743498017
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentPosition, "GetCurrentPosition" }, // 2381520360
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetCurrentTrackMetadata, "GetCurrentTrackMetadata" }, // 1971842978
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackRepeatState, "GetPlaybackRepeatState" }, // 3818104234
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackShuffleState, "GetPlaybackShuffleState" }, // 3597934671
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackState, "GetPlaybackState" }, // 607500474
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetPlaybackVolume, "GetPlaybackVolume" }, // 790442174
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceProviderError, "GetServiceProviderError" }, // 2715732990
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetServiceStatus, "GetServiceStatus" }, // 4053422851
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_GetTrackLength, "GetTrackLength" }, // 1265264393
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_NextTrack, "NextTrack" }, // 107182694
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PausePlayback, "PausePlayback" }, // 575028300
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_PreviousTrack, "PreviousTrack" }, // 2131598530
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_ResumePlayback, "ResumePlayback" }, // 1705247226
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SeekTo, "SeekTo" }, // 1810010002
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetAuthenticationString, "SetAuthenticationString" }, // 750333337
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetCallbacks, "SetCallbacks" }, // 2239846675
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackRepeatState, "SetPlaybackRepeatState" }, // 963518463
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackShuffleState, "SetPlaybackShuffleState" }, // 1780104887
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackURL, "SetPlaybackURL" }, // 2555226052
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaybackVolume, "SetPlaybackVolume" }, // 3060879531
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_SetPlaylist, "SetPlaylist" }, // 2909843411
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StartPlayback, "StartPlayback" }, // 3925754959
		{ &Z_Construct_UFunction_UMagicLeapMusicServiceFunctionLibrary_StopPlayback, "StopPlayback" }, // 3134728249
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n   Provides music playback control of a Music Service Provider\n */" },
		{ "IncludePath", "MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapMusicServiceFunctionLibrary.h" },
		{ "ToolTip", "Provides music playback control of a Music Service Provider" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapMusicServiceFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapMusicServiceFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapMusicServiceFunctionLibrary, 68381473);
	template<> MAGICLEAPMUSICSERVICE_API UClass* StaticClass<UMagicLeapMusicServiceFunctionLibrary>()
	{
		return UMagicLeapMusicServiceFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapMusicServiceFunctionLibrary(Z_Construct_UClass_UMagicLeapMusicServiceFunctionLibrary, &UMagicLeapMusicServiceFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapMusicService"), TEXT("UMagicLeapMusicServiceFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapMusicServiceFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
