// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapContactsTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapContactsTypes() {}
// Cross Module References
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapContacts();
	MAGICLEAPCONTACTS_API UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature();
	MAGICLEAPCONTACTS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapContact();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature();
	MAGICLEAPCONTACTS_API UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField();
	MAGICLEAPCONTACTS_API UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsResult();
	MAGICLEAPCONTACTS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapContacts_eventMagicLeapContactsLogMessageMulti_Parms
		{
			FString LogMessage;
			EMagicLeapContactsOperationStatus OpStatus;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LogMessage;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OpStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OpStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage = { "LogMessage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapContactsLogMessageMulti_Parms, LogMessage), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_OpStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_OpStatus = { "OpStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapContactsLogMessageMulti_Parms, OpStatus), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_OpStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::NewProp_OpStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts, nullptr, "MagicLeapContactsLogMessageMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapContacts_eventMagicLeapContactsLogMessageMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics
	{
		struct _Script_MagicLeapContacts_eventMagicLeapContactsLogMessage_Parms
		{
			FString LogMessage;
			EMagicLeapContactsOperationStatus OpStatus;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LogMessage;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OpStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OpStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_LogMessage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_LogMessage = { "LogMessage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapContactsLogMessage_Parms, LogMessage), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_LogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_LogMessage_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_OpStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_OpStatus = { "OpStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapContactsLogMessage_Parms, OpStatus), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_LogMessage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_OpStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::NewProp_OpStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09""Delegate used to pass log messages from the contacts plugin to the initiating blueprint.\n\x09@note This is useful if the user wishes to have log messages in 3D space.\n\x09@param LogMessage A string containing the log message.\n\x09@param OpStatus The status of the operation associated with the log message.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Delegate used to pass log messages from the contacts plugin to the initiating blueprint.\n@note This is useful if the user wishes to have log messages in 3D space.\n@param LogMessage A string containing the log message.\n@param OpStatus The status of the operation associated with the log message." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts, nullptr, "MagicLeapContactsLogMessage__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapContacts_eventMagicLeapContactsLogMessage_Parms), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegateMulti_Parms
		{
			TArray<FMagicLeapContact> Contacts;
			EMagicLeapContactsOperationStatus OpStatus;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contacts_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contacts_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Contacts;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OpStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OpStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts_Inner = { "Contacts", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts = { "Contacts", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegateMulti_Parms, Contacts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus = { "OpStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegateMulti_Parms, OpStatus), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_Contacts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts, nullptr, "MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegate_Parms
		{
			TArray<FMagicLeapContact> Contacts;
			EMagicLeapContactsOperationStatus OpStatus;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contacts_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contacts_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Contacts;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OpStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OpStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts_Inner = { "Contacts", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts = { "Contacts", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegate_Parms, Contacts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_OpStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_OpStatus = { "OpStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegate_Parms, OpStatus), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_Contacts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_OpStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::NewProp_OpStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to convey the result of a multiple contacts operation. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of a multiple contacts operation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts, nullptr, "MagicLeapMultipleContactsResultDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegateMulti_Parms
		{
			EMagicLeapContactsOperationStatus OpStatus;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OpStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OpStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus = { "OpStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegateMulti_Parms, OpStatus), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::NewProp_OpStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts, nullptr, "MagicLeapSingleContactResultDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegate_Parms
		{
			EMagicLeapContactsOperationStatus OpStatus;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OpStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OpStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::NewProp_OpStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::NewProp_OpStatus = { "OpStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegate_Parms, OpStatus), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::NewProp_OpStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::NewProp_OpStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to convey the result of a single contact operation. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of a single contact operation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts, nullptr, "MagicLeapSingleContactResultDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMagicLeapContactsOperationStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus, Z_Construct_UPackage__Script_MagicLeapContacts(), TEXT("EMagicLeapContactsOperationStatus"));
		}
		return Singleton;
	}
	template<> MAGICLEAPCONTACTS_API UEnum* StaticEnum<EMagicLeapContactsOperationStatus>()
	{
		return EMagicLeapContactsOperationStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapContactsOperationStatus(EMagicLeapContactsOperationStatus_StaticEnum, TEXT("/Script/MagicLeapContacts"), TEXT("EMagicLeapContactsOperationStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus_Hash() { return 217575329U; }
	UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapContacts();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapContactsOperationStatus"), 0, Get_Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsOperationStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapContactsOperationStatus::Success", (int64)EMagicLeapContactsOperationStatus::Success },
				{ "EMagicLeapContactsOperationStatus::Fail", (int64)EMagicLeapContactsOperationStatus::Fail },
				{ "EMagicLeapContactsOperationStatus::Duplicate", (int64)EMagicLeapContactsOperationStatus::Duplicate },
				{ "EMagicLeapContactsOperationStatus::NotFound", (int64)EMagicLeapContactsOperationStatus::NotFound },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Result types for contacts operations. */" },
				{ "Duplicate.Comment", "/** MagicLeapContact with the details specified for an insert already exists. */" },
				{ "Duplicate.Name", "EMagicLeapContactsOperationStatus::Duplicate" },
				{ "Duplicate.ToolTip", "MagicLeapContact with the details specified for an insert already exists." },
				{ "Fail.Comment", "/** Operation failed. */" },
				{ "Fail.Name", "EMagicLeapContactsOperationStatus::Fail" },
				{ "Fail.ToolTip", "Operation failed." },
				{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
				{ "NotFound.Comment", "/** MagicLeapContact to be deleted/updated doesn't exist. */" },
				{ "NotFound.Name", "EMagicLeapContactsOperationStatus::NotFound" },
				{ "NotFound.ToolTip", "MagicLeapContact to be deleted/updated doesn't exist." },
				{ "Success.Comment", "/** Operation succeeded. */" },
				{ "Success.Name", "EMagicLeapContactsOperationStatus::Success" },
				{ "Success.ToolTip", "Operation succeeded." },
				{ "ToolTip", "Result types for contacts operations." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts,
				nullptr,
				"EMagicLeapContactsOperationStatus",
				"EMagicLeapContactsOperationStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapContactsSearchField_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField, Z_Construct_UPackage__Script_MagicLeapContacts(), TEXT("EMagicLeapContactsSearchField"));
		}
		return Singleton;
	}
	template<> MAGICLEAPCONTACTS_API UEnum* StaticEnum<EMagicLeapContactsSearchField>()
	{
		return EMagicLeapContactsSearchField_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapContactsSearchField(EMagicLeapContactsSearchField_StaticEnum, TEXT("/Script/MagicLeapContacts"), TEXT("EMagicLeapContactsSearchField"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField_Hash() { return 4081537985U; }
	UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapContacts();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapContactsSearchField"), 0, Get_Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapContactsSearchField::Name", (int64)EMagicLeapContactsSearchField::Name },
				{ "EMagicLeapContactsSearchField::Phone", (int64)EMagicLeapContactsSearchField::Phone },
				{ "EMagicLeapContactsSearchField::Email", (int64)EMagicLeapContactsSearchField::Email },
				{ "EMagicLeapContactsSearchField::All", (int64)EMagicLeapContactsSearchField::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.Comment", "/** Search across all fields. */" },
				{ "All.Name", "EMagicLeapContactsSearchField::All" },
				{ "All.ToolTip", "Search across all fields." },
				{ "BlueprintType", "true" },
				{ "Comment", "/** Search query field types. */" },
				{ "Email.Comment", "/** Search field for email. */" },
				{ "Email.Name", "EMagicLeapContactsSearchField::Email" },
				{ "Email.ToolTip", "Search field for email." },
				{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
				{ "Name.Comment", "/** Search field for nickname. */" },
				{ "Name.Name", "EMagicLeapContactsSearchField::Name" },
				{ "Name.ToolTip", "Search field for nickname." },
				{ "Phone.Comment", "/** Search field for phone. */" },
				{ "Phone.Name", "EMagicLeapContactsSearchField::Phone" },
				{ "Phone.ToolTip", "Search field for phone." },
				{ "ToolTip", "Search query field types." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts,
				nullptr,
				"EMagicLeapContactsSearchField",
				"EMagicLeapContactsSearchField",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapContactsResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsResult, Z_Construct_UPackage__Script_MagicLeapContacts(), TEXT("EMagicLeapContactsResult"));
		}
		return Singleton;
	}
	template<> MAGICLEAPCONTACTS_API UEnum* StaticEnum<EMagicLeapContactsResult>()
	{
		return EMagicLeapContactsResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapContactsResult(EMagicLeapContactsResult_StaticEnum, TEXT("/Script/MagicLeapContacts"), TEXT("EMagicLeapContactsResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsResult_Hash() { return 4025681437U; }
	UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapContacts();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapContactsResult"), 0, Get_Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapContactsResult::HandleNotFound", (int64)EMagicLeapContactsResult::HandleNotFound },
				{ "EMagicLeapContactsResult::Completed", (int64)EMagicLeapContactsResult::Completed },
				{ "EMagicLeapContactsResult::IllegalState", (int64)EMagicLeapContactsResult::IllegalState },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Result types for contacts requests. */" },
				{ "Completed.Comment", "/** Request is completed, its corresponding result has been returned, and its related resources are marked for deletion. */" },
				{ "Completed.Name", "EMagicLeapContactsResult::Completed" },
				{ "Completed.ToolTip", "Request is completed, its corresponding result has been returned, and its related resources are marked for deletion." },
				{ "HandleNotFound.Comment", "/** This handle is not yet recognized. */" },
				{ "HandleNotFound.Name", "EMagicLeapContactsResult::HandleNotFound" },
				{ "HandleNotFound.ToolTip", "This handle is not yet recognized." },
				{ "IllegalState.Comment", "/** Request failed due to system being in an illegal state, for e.g., when the user hasn't successfully logged-in. */" },
				{ "IllegalState.Name", "EMagicLeapContactsResult::IllegalState" },
				{ "IllegalState.ToolTip", "Request failed due to system being in an illegal state, for e.g., when the user hasn't successfully logged-in." },
				{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
				{ "ToolTip", "Result types for contacts requests." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapContacts,
				nullptr,
				"EMagicLeapContactsResult",
				"EMagicLeapContactsResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMagicLeapContact::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPCONTACTS_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapContact_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapContact, Z_Construct_UPackage__Script_MagicLeapContacts(), TEXT("MagicLeapContact"), sizeof(FMagicLeapContact), Get_Z_Construct_UScriptStruct_FMagicLeapContact_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPCONTACTS_API UScriptStruct* StaticStruct<FMagicLeapContact>()
{
	return FMagicLeapContact::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapContact(FMagicLeapContact::StaticStruct, TEXT("/Script/MagicLeapContacts"), TEXT("MagicLeapContact"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapContacts_StaticRegisterNativesFMagicLeapContact
{
	FScriptStruct_MagicLeapContacts_StaticRegisterNativesFMagicLeapContact()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapContact>(FName(TEXT("MagicLeapContact")));
	}
} ScriptStruct_MagicLeapContacts_StaticRegisterNativesFMagicLeapContact;
	struct Z_Construct_UScriptStruct_FMagicLeapContact_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PhoneNumbers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhoneNumbers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PhoneNumbers;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EmailAddresses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmailAddresses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EmailAddresses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapContact_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Representation of available information for a single contact in an address book. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Representation of available information for a single contact in an address book." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapContact>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "Contacts|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Locally-unique contact identifier.  Generated by the system.  May change across reboots.\n\x09\x09@note Please do not edit this value.  It is exposed as read/write merely so that it can copied via the make/break functionality.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Locally-unique contact identifier.  Generated by the system.  May change across reboots.\n@note Please do not edit this value.  It is exposed as read/write merely so that it can copied via the make/break functionality." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapContact, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Contacts|MagicLeap" },
		{ "Comment", "/** Contacts's name. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Contacts's name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapContact, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers_Inner = { "PhoneNumbers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers_MetaData[] = {
		{ "Category", "Contacts|MagicLeap" },
		{ "Comment", "/** Contacts's phone numbers. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Contacts's phone numbers." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers = { "PhoneNumbers", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapContact, PhoneNumbers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses_Inner = { "EmailAddresses", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses_MetaData[] = {
		{ "Category", "Contacts|MagicLeap" },
		{ "Comment", "/** Contacts's email addresses. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Contacts's email addresses." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses = { "EmailAddresses", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapContact, EmailAddresses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapContact_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_PhoneNumbers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapContact_Statics::NewProp_EmailAddresses,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapContact_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapContacts,
		nullptr,
		&NewStructOps,
		"MagicLeapContact",
		sizeof(FMagicLeapContact),
		alignof(FMagicLeapContact),
		Z_Construct_UScriptStruct_FMagicLeapContact_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapContact_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapContact()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapContact_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapContacts();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapContact"), sizeof(FMagicLeapContact), Get_Z_Construct_UScriptStruct_FMagicLeapContact_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapContact_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapContact_Hash() { return 1188102344U; }
class UScriptStruct* FMagicLeapTaggedAttribute::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPCONTACTS_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute, Z_Construct_UPackage__Script_MagicLeapContacts(), TEXT("MagicLeapTaggedAttribute"), sizeof(FMagicLeapTaggedAttribute), Get_Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPCONTACTS_API UScriptStruct* StaticStruct<FMagicLeapTaggedAttribute>()
{
	return FMagicLeapTaggedAttribute::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapTaggedAttribute(FMagicLeapTaggedAttribute::StaticStruct, TEXT("/Script/MagicLeapContacts"), TEXT("MagicLeapTaggedAttribute"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapContacts_StaticRegisterNativesFMagicLeapTaggedAttribute
{
	FScriptStruct_MagicLeapContacts_StaticRegisterNativesFMagicLeapTaggedAttribute()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapTaggedAttribute>(FName(TEXT("MagicLeapTaggedAttribute")));
	}
} ScriptStruct_MagicLeapContacts_StaticRegisterNativesFMagicLeapTaggedAttribute;
	struct Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tag_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Tag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n\x09Stores a tagged value, such as phone number or email address.  Optional tag indicates what type\n\x09of value is stored, e.g. \"home\", \"work\", etc.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Stores a tagged value, such as phone number or email address.  Optional tag indicates what type\nof value is stored, e.g. \"home\", \"work\", etc." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapTaggedAttribute>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Tag_MetaData[] = {
		{ "Category", "Contacts|MagicLeap" },
		{ "Comment", "/** Name of the Tag. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Name of the Tag." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Tag = { "Tag", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapTaggedAttribute, Tag), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Tag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Tag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "Contacts|MagicLeap" },
		{ "Comment", "/** Value of this attribute. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsTypes.h" },
		{ "ToolTip", "Value of this attribute." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapTaggedAttribute, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Tag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapContacts,
		nullptr,
		&NewStructOps,
		"MagicLeapTaggedAttribute",
		sizeof(FMagicLeapTaggedAttribute),
		alignof(FMagicLeapTaggedAttribute),
		Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapContacts();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapTaggedAttribute"), sizeof(FMagicLeapTaggedAttribute), Get_Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Hash() { return 1833358021U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
