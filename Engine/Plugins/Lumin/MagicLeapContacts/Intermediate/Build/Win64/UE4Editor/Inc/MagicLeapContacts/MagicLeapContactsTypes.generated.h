// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapContactsOperationStatus : uint8;
struct FMagicLeapContact;
#ifdef MAGICLEAPCONTACTS_MagicLeapContactsTypes_generated_h
#error "MagicLeapContactsTypes.generated.h already included, missing '#pragma once' in MagicLeapContactsTypes.h"
#endif
#define MAGICLEAPCONTACTS_MagicLeapContactsTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_69_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapContact_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPCONTACTS_API UScriptStruct* StaticStruct<struct FMagicLeapContact>();

#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_54_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapTaggedAttribute_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPCONTACTS_API UScriptStruct* StaticStruct<struct FMagicLeapTaggedAttribute>();

#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_106_DELEGATE \
struct _Script_MagicLeapContacts_eventMagicLeapContactsLogMessageMulti_Parms \
{ \
	FString LogMessage; \
	EMagicLeapContactsOperationStatus OpStatus; \
}; \
static inline void FMagicLeapContactsLogMessageMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapContactsLogMessageMulti, const FString& LogMessage, EMagicLeapContactsOperationStatus OpStatus) \
{ \
	_Script_MagicLeapContacts_eventMagicLeapContactsLogMessageMulti_Parms Parms; \
	Parms.LogMessage=LogMessage; \
	Parms.OpStatus=OpStatus; \
	MagicLeapContactsLogMessageMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_105_DELEGATE \
struct _Script_MagicLeapContacts_eventMagicLeapContactsLogMessage_Parms \
{ \
	FString LogMessage; \
	EMagicLeapContactsOperationStatus OpStatus; \
}; \
static inline void FMagicLeapContactsLogMessage_DelegateWrapper(const FScriptDelegate& MagicLeapContactsLogMessage, const FString& LogMessage, EMagicLeapContactsOperationStatus OpStatus) \
{ \
	_Script_MagicLeapContacts_eventMagicLeapContactsLogMessage_Parms Parms; \
	Parms.LogMessage=LogMessage; \
	Parms.OpStatus=OpStatus; \
	MagicLeapContactsLogMessage.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_97_DELEGATE \
struct _Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegateMulti_Parms \
{ \
	TArray<FMagicLeapContact> Contacts; \
	EMagicLeapContactsOperationStatus OpStatus; \
}; \
static inline void FMagicLeapMultipleContactsResultDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapMultipleContactsResultDelegateMulti, TArray<FMagicLeapContact> const& Contacts, EMagicLeapContactsOperationStatus OpStatus) \
{ \
	_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegateMulti_Parms Parms; \
	Parms.Contacts=Contacts; \
	Parms.OpStatus=OpStatus; \
	MagicLeapMultipleContactsResultDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_96_DELEGATE \
struct _Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegate_Parms \
{ \
	TArray<FMagicLeapContact> Contacts; \
	EMagicLeapContactsOperationStatus OpStatus; \
}; \
static inline void FMagicLeapMultipleContactsResultDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapMultipleContactsResultDelegate, TArray<FMagicLeapContact> const& Contacts, EMagicLeapContactsOperationStatus OpStatus) \
{ \
	_Script_MagicLeapContacts_eventMagicLeapMultipleContactsResultDelegate_Parms Parms; \
	Parms.Contacts=Contacts; \
	Parms.OpStatus=OpStatus; \
	MagicLeapMultipleContactsResultDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_93_DELEGATE \
struct _Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegateMulti_Parms \
{ \
	EMagicLeapContactsOperationStatus OpStatus; \
}; \
static inline void FMagicLeapSingleContactResultDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapSingleContactResultDelegateMulti, EMagicLeapContactsOperationStatus OpStatus) \
{ \
	_Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegateMulti_Parms Parms; \
	Parms.OpStatus=OpStatus; \
	MagicLeapSingleContactResultDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h_92_DELEGATE \
struct _Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegate_Parms \
{ \
	EMagicLeapContactsOperationStatus OpStatus; \
}; \
static inline void FMagicLeapSingleContactResultDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapSingleContactResultDelegate, EMagicLeapContactsOperationStatus OpStatus) \
{ \
	_Script_MagicLeapContacts_eventMagicLeapSingleContactResultDelegate_Parms Parms; \
	Parms.OpStatus=OpStatus; \
	MagicLeapSingleContactResultDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsTypes_h


#define FOREACH_ENUM_EMAGICLEAPCONTACTSOPERATIONSTATUS(op) \
	op(EMagicLeapContactsOperationStatus::Success) \
	op(EMagicLeapContactsOperationStatus::Fail) \
	op(EMagicLeapContactsOperationStatus::Duplicate) \
	op(EMagicLeapContactsOperationStatus::NotFound) 

enum class EMagicLeapContactsOperationStatus : uint8;
template<> MAGICLEAPCONTACTS_API UEnum* StaticEnum<EMagicLeapContactsOperationStatus>();

#define FOREACH_ENUM_EMAGICLEAPCONTACTSSEARCHFIELD(op) \
	op(EMagicLeapContactsSearchField::Name) \
	op(EMagicLeapContactsSearchField::Phone) \
	op(EMagicLeapContactsSearchField::Email) \
	op(EMagicLeapContactsSearchField::All) 

enum class EMagicLeapContactsSearchField : uint8;
template<> MAGICLEAPCONTACTS_API UEnum* StaticEnum<EMagicLeapContactsSearchField>();

#define FOREACH_ENUM_EMAGICLEAPCONTACTSRESULT(op) \
	op(EMagicLeapContactsResult::HandleNotFound) \
	op(EMagicLeapContactsResult::Completed) \
	op(EMagicLeapContactsResult::IllegalState) 

enum class EMagicLeapContactsResult : uint8;
template<> MAGICLEAPCONTACTS_API UEnum* StaticEnum<EMagicLeapContactsResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
