// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapContactsComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapContactsComponent() {}
// Cross Module References
	MAGICLEAPCONTACTS_API UClass* Z_Construct_UClass_UMagicLeapContactsComponent_NoRegister();
	MAGICLEAPCONTACTS_API UClass* Z_Construct_UClass_UMagicLeapContactsComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapContacts();
	MAGICLEAPCONTACTS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapContact();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MAGICLEAPCONTACTS_API UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapContactsComponent::execSearchContactsAsync)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Query);
		P_GET_ENUM(EMagicLeapContactsSearchField,Z_Param_SearchField);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=P_THIS->SearchContactsAsync(Z_Param_Query,EMagicLeapContactsSearchField(Z_Param_SearchField));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsComponent::execSelectContactsAsync)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxNumResults);
		P_GET_ENUM(EMagicLeapContactsSearchField,Z_Param_SearchField);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=P_THIS->SelectContactsAsync(Z_Param_MaxNumResults,EMagicLeapContactsSearchField(Z_Param_SearchField));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsComponent::execRequestContactsAsync)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxNumResults);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=P_THIS->RequestContactsAsync(Z_Param_MaxNumResults);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsComponent::execDeleteContactAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapContact,Z_Param_Out_Contact);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=P_THIS->DeleteContactAsync(Z_Param_Out_Contact);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsComponent::execEditContactAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapContact,Z_Param_Out_Contact);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=P_THIS->EditContactAsync(Z_Param_Out_Contact);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsComponent::execAddContactAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapContact,Z_Param_Out_Contact);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=P_THIS->AddContactAsync(Z_Param_Out_Contact);
		P_NATIVE_END;
	}
	void UMagicLeapContactsComponent::StaticRegisterNativesUMagicLeapContactsComponent()
	{
		UClass* Class = UMagicLeapContactsComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddContactAsync", &UMagicLeapContactsComponent::execAddContactAsync },
			{ "DeleteContactAsync", &UMagicLeapContactsComponent::execDeleteContactAsync },
			{ "EditContactAsync", &UMagicLeapContactsComponent::execEditContactAsync },
			{ "RequestContactsAsync", &UMagicLeapContactsComponent::execRequestContactsAsync },
			{ "SearchContactsAsync", &UMagicLeapContactsComponent::execSearchContactsAsync },
			{ "SelectContactsAsync", &UMagicLeapContactsComponent::execSelectContactsAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics
	{
		struct MagicLeapContactsComponent_eventAddContactAsync_Parms
		{
			FMagicLeapContact Contact;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contact_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contact;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_Contact_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_Contact = { "Contact", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventAddContactAsync_Parms, Contact), Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_Contact_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_Contact_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventAddContactAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_Contact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the creation of a new contact.\n\x09\x09@param Contact The contact to be created.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Initiates the creation of a new contact.\n@param Contact The contact to be created.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsComponent, nullptr, "AddContactAsync", nullptr, nullptr, sizeof(MagicLeapContactsComponent_eventAddContactAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics
	{
		struct MagicLeapContactsComponent_eventDeleteContactAsync_Parms
		{
			FMagicLeapContact Contact;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contact_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contact;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_Contact_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_Contact = { "Contact", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventDeleteContactAsync_Parms, Contact), Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_Contact_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_Contact_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventDeleteContactAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_Contact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the deletion of an existing contact.\n\x09\x09@param Contact The contact to be deleted.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Initiates the deletion of an existing contact.\n@param Contact The contact to be deleted.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsComponent, nullptr, "DeleteContactAsync", nullptr, nullptr, sizeof(MagicLeapContactsComponent_eventDeleteContactAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics
	{
		struct MagicLeapContactsComponent_eventEditContactAsync_Parms
		{
			FMagicLeapContact Contact;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contact_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contact;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_Contact_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_Contact = { "Contact", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventEditContactAsync_Parms, Contact), Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_Contact_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_Contact_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventEditContactAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_Contact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the update of an existing contact.\n\x09\x09@param Contact The contact to be updated.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Initiates the update of an existing contact.\n@param Contact The contact to be updated.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsComponent, nullptr, "EditContactAsync", nullptr, nullptr, sizeof(MagicLeapContactsComponent_eventEditContactAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics
	{
		struct MagicLeapContactsComponent_eventRequestContactsAsync_Parms
		{
			int32 MaxNumResults;
			FGuid ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumResults;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::NewProp_MaxNumResults = { "MaxNumResults", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventRequestContactsAsync_Parms, MaxNumResults), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventRequestContactsAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::NewProp_MaxNumResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the retrieval of the entire contacts list from the cloud.\n\x09\x09@param MaxNumResults The maximum number of results to return.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Initiates the retrieval of the entire contacts list from the cloud.\n@param MaxNumResults The maximum number of results to return.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsComponent, nullptr, "RequestContactsAsync", nullptr, nullptr, sizeof(MagicLeapContactsComponent_eventRequestContactsAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics
	{
		struct MagicLeapContactsComponent_eventSearchContactsAsync_Parms
		{
			FString Query;
			EMagicLeapContactsSearchField SearchField;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Query_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Query;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SearchField_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SearchField;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_Query_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_Query = { "Query", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventSearchContactsAsync_Parms, Query), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_Query_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_Query_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_SearchField_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_SearchField = { "SearchField", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventSearchContactsAsync_Parms, SearchField), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventSearchContactsAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_Query,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_SearchField_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_SearchField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates a search for contacts with a given query across specified fields.\n\x09\x09@param Query The search string to look for instances of.\n\x09\x09@param SearchField The field within the contact to match the query against.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Initiates a search for contacts with a given query across specified fields.\n@param Query The search string to look for instances of.\n@param SearchField The field within the contact to match the query against.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsComponent, nullptr, "SearchContactsAsync", nullptr, nullptr, sizeof(MagicLeapContactsComponent_eventSearchContactsAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics
	{
		struct MagicLeapContactsComponent_eventSelectContactsAsync_Parms
		{
			int32 MaxNumResults;
			EMagicLeapContactsSearchField SearchField;
			FGuid ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumResults;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SearchField_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SearchField;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_MaxNumResults = { "MaxNumResults", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventSelectContactsAsync_Parms, MaxNumResults), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_SearchField_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_SearchField = { "SearchField", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventSelectContactsAsync_Parms, SearchField), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsComponent_eventSelectContactsAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_MaxNumResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_SearchField_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_SearchField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Pops up a dialog allowing the user to manually select the contacts they wish to query.\n\x09\x09@param MaxNumResults The maximum number of contacts to display (values greater than number of contacts will result in an invalid param error).\n\x09\x09@param SearchField Specifies which field(s) to retrieve for each selected contact.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Pops up a dialog allowing the user to manually select the contacts they wish to query.\n@param MaxNumResults The maximum number of contacts to display (values greater than number of contacts will result in an invalid param error).\n@param SearchField Specifies which field(s) to retrieve for each selected contact.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsComponent, nullptr, "SelectContactsAsync", nullptr, nullptr, sizeof(MagicLeapContactsComponent_eventSelectContactsAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapContactsComponent_NoRegister()
	{
		return UMagicLeapContactsComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapContactsComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAddContactResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAddContactResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnEditContactResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnEditContactResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDeleteContactResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnDeleteContactResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnRequestContactsResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnRequestContactsResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSelectContactsResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSelectContactsResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSearchContactsResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSearchContactsResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnLogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnLogMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapContactsComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapContacts,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapContactsComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapContactsComponent_AddContactAsync, "AddContactAsync" }, // 3219135781
		{ &Z_Construct_UFunction_UMagicLeapContactsComponent_DeleteContactAsync, "DeleteContactAsync" }, // 237330236
		{ &Z_Construct_UFunction_UMagicLeapContactsComponent_EditContactAsync, "EditContactAsync" }, // 2198593443
		{ &Z_Construct_UFunction_UMagicLeapContactsComponent_RequestContactsAsync, "RequestContactsAsync" }, // 2946366560
		{ &Z_Construct_UFunction_UMagicLeapContactsComponent_SearchContactsAsync, "SearchContactsAsync" }, // 1876341240
		{ &Z_Construct_UFunction_UMagicLeapContactsComponent_SelectContactsAsync, "SelectContactsAsync" }, // 3767586034
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09""Component that provides access to the Contacts API functionality.\n*/" },
		{ "IncludePath", "MagicLeapContactsComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Component that provides access to the Contacts API functionality." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnAddContactResult_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "// Delegate instances\n" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
		{ "ToolTip", "Delegate instances" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnAddContactResult = { "OnAddContactResult", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnAddContactResult), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnAddContactResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnAddContactResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnEditContactResult_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnEditContactResult = { "OnEditContactResult", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnEditContactResult), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnEditContactResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnEditContactResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnDeleteContactResult_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnDeleteContactResult = { "OnDeleteContactResult", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnDeleteContactResult), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnDeleteContactResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnDeleteContactResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnRequestContactsResult_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnRequestContactsResult = { "OnRequestContactsResult", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnRequestContactsResult), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnRequestContactsResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnRequestContactsResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSelectContactsResult_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSelectContactsResult = { "OnSelectContactsResult", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnSelectContactsResult), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSelectContactsResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSelectContactsResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSearchContactsResult_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSearchContactsResult = { "OnSearchContactsResult", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnSearchContactsResult), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSearchContactsResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSearchContactsResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnLogMessage_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Contacts | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnLogMessage = { "OnLogMessage", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapContactsComponent, OnLogMessage), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnLogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnLogMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapContactsComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnAddContactResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnEditContactResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnDeleteContactResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnRequestContactsResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSelectContactsResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnSearchContactsResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapContactsComponent_Statics::NewProp_OnLogMessage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapContactsComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapContactsComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapContactsComponent_Statics::ClassParams = {
		&UMagicLeapContactsComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapContactsComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapContactsComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapContactsComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapContactsComponent, 1469510586);
	template<> MAGICLEAPCONTACTS_API UClass* StaticClass<UMagicLeapContactsComponent>()
	{
		return UMagicLeapContactsComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapContactsComponent(Z_Construct_UClass_UMagicLeapContactsComponent, &UMagicLeapContactsComponent::StaticClass, TEXT("/Script/MagicLeapContacts"), TEXT("UMagicLeapContactsComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapContactsComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
