// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapContactsSearchField : uint8;
struct FGuid;
struct FMagicLeapContact;
#ifdef MAGICLEAPCONTACTS_MagicLeapContactsComponent_generated_h
#error "MagicLeapContactsComponent.generated.h already included, missing '#pragma once' in MagicLeapContactsComponent.h"
#endif
#define MAGICLEAPCONTACTS_MagicLeapContactsComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSearchContactsAsync); \
	DECLARE_FUNCTION(execSelectContactsAsync); \
	DECLARE_FUNCTION(execRequestContactsAsync); \
	DECLARE_FUNCTION(execDeleteContactAsync); \
	DECLARE_FUNCTION(execEditContactAsync); \
	DECLARE_FUNCTION(execAddContactAsync);


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSearchContactsAsync); \
	DECLARE_FUNCTION(execSelectContactsAsync); \
	DECLARE_FUNCTION(execRequestContactsAsync); \
	DECLARE_FUNCTION(execDeleteContactAsync); \
	DECLARE_FUNCTION(execEditContactAsync); \
	DECLARE_FUNCTION(execAddContactAsync);


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapContactsComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapContactsComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapContactsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapContacts"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapContactsComponent)


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapContactsComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapContactsComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapContactsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapContacts"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapContactsComponent)


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapContactsComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapContactsComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapContactsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapContactsComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapContactsComponent(UMagicLeapContactsComponent&&); \
	NO_API UMagicLeapContactsComponent(const UMagicLeapContactsComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapContactsComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapContactsComponent(UMagicLeapContactsComponent&&); \
	NO_API UMagicLeapContactsComponent(const UMagicLeapContactsComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapContactsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapContactsComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapContactsComponent)


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnAddContactResult() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnAddContactResult); } \
	FORCEINLINE static uint32 __PPO__OnEditContactResult() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnEditContactResult); } \
	FORCEINLINE static uint32 __PPO__OnDeleteContactResult() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnDeleteContactResult); } \
	FORCEINLINE static uint32 __PPO__OnRequestContactsResult() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnRequestContactsResult); } \
	FORCEINLINE static uint32 __PPO__OnSelectContactsResult() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnSelectContactsResult); } \
	FORCEINLINE static uint32 __PPO__OnSearchContactsResult() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnSearchContactsResult); } \
	FORCEINLINE static uint32 __PPO__OnLogMessage() { return STRUCT_OFFSET(UMagicLeapContactsComponent, OnLogMessage); }


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCONTACTS_API UClass* StaticClass<class UMagicLeapContactsComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
