// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapContacts_init() {}
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapContacts()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessageMulti__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapContacts",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x6C028774,
				0xDD3D366B,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
