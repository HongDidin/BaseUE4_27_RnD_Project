// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapContactsSearchField : uint8;
struct FGuid;
struct FMagicLeapContact;
#ifdef MAGICLEAPCONTACTS_MagicLeapContactsFunctionLibrary_generated_h
#error "MagicLeapContactsFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapContactsFunctionLibrary.h"
#endif
#define MAGICLEAPCONTACTS_MagicLeapContactsFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetLogDelegate); \
	DECLARE_FUNCTION(execSearchContactsAsync); \
	DECLARE_FUNCTION(execSelectContactsAsync); \
	DECLARE_FUNCTION(execRequestContactsAsync); \
	DECLARE_FUNCTION(execDeleteContactAsync); \
	DECLARE_FUNCTION(execEditContactAsync); \
	DECLARE_FUNCTION(execAddContactAsync); \
	DECLARE_FUNCTION(execShutdown); \
	DECLARE_FUNCTION(execStartup);


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetLogDelegate); \
	DECLARE_FUNCTION(execSearchContactsAsync); \
	DECLARE_FUNCTION(execSelectContactsAsync); \
	DECLARE_FUNCTION(execRequestContactsAsync); \
	DECLARE_FUNCTION(execDeleteContactAsync); \
	DECLARE_FUNCTION(execEditContactAsync); \
	DECLARE_FUNCTION(execAddContactAsync); \
	DECLARE_FUNCTION(execShutdown); \
	DECLARE_FUNCTION(execStartup);


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapContactsFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapContactsFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapContacts"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapContactsFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapContactsFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapContactsFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapContacts"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapContactsFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapContactsFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapContactsFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapContactsFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapContactsFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapContactsFunctionLibrary(UMagicLeapContactsFunctionLibrary&&); \
	NO_API UMagicLeapContactsFunctionLibrary(const UMagicLeapContactsFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapContactsFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapContactsFunctionLibrary(UMagicLeapContactsFunctionLibrary&&); \
	NO_API UMagicLeapContactsFunctionLibrary(const UMagicLeapContactsFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapContactsFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapContactsFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapContactsFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_9_PROLOG
#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_INCLASS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCONTACTS_API UClass* StaticClass<class UMagicLeapContactsFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapContacts_Source_Public_MagicLeapContactsFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
