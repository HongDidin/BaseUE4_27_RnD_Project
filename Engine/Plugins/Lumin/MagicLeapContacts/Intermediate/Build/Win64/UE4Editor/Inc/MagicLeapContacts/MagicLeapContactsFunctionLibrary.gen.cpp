// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapContactsFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapContactsFunctionLibrary() {}
// Cross Module References
	MAGICLEAPCONTACTS_API UClass* Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_NoRegister();
	MAGICLEAPCONTACTS_API UClass* Z_Construct_UClass_UMagicLeapContactsFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapContacts();
	MAGICLEAPCONTACTS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapContact();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature();
	MAGICLEAPCONTACTS_API UEnum* Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField();
	MAGICLEAPCONTACTS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execSetLogDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_LogDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::SetLogDelegate(FMagicLeapContactsLogMessage(Z_Param_Out_LogDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execSearchContactsAsync)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Query);
		P_GET_ENUM(EMagicLeapContactsSearchField,Z_Param_SearchField);
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::SearchContactsAsync(Z_Param_Query,EMagicLeapContactsSearchField(Z_Param_SearchField),FMagicLeapMultipleContactsResultDelegate(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execSelectContactsAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxNumResults);
		P_GET_ENUM(EMagicLeapContactsSearchField,Z_Param_SearchField);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::SelectContactsAsync(FMagicLeapMultipleContactsResultDelegate(Z_Param_Out_ResultDelegate),Z_Param_MaxNumResults,EMagicLeapContactsSearchField(Z_Param_SearchField));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execRequestContactsAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_GET_PROPERTY(FIntProperty,Z_Param_MaxNumResults);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::RequestContactsAsync(FMagicLeapMultipleContactsResultDelegate(Z_Param_Out_ResultDelegate),Z_Param_MaxNumResults);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execDeleteContactAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapContact,Z_Param_Out_Contact);
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::DeleteContactAsync(Z_Param_Out_Contact,FMagicLeapSingleContactResultDelegate(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execEditContactAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapContact,Z_Param_Out_Contact);
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::EditContactAsync(Z_Param_Out_Contact,FMagicLeapSingleContactResultDelegate(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execAddContactAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapContact,Z_Param_Out_Contact);
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGuid*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::AddContactAsync(Z_Param_Out_Contact,FMagicLeapSingleContactResultDelegate(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execShutdown)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::Shutdown();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapContactsFunctionLibrary::execStartup)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapContactsFunctionLibrary::Startup();
		P_NATIVE_END;
	}
	void UMagicLeapContactsFunctionLibrary::StaticRegisterNativesUMagicLeapContactsFunctionLibrary()
	{
		UClass* Class = UMagicLeapContactsFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddContactAsync", &UMagicLeapContactsFunctionLibrary::execAddContactAsync },
			{ "DeleteContactAsync", &UMagicLeapContactsFunctionLibrary::execDeleteContactAsync },
			{ "EditContactAsync", &UMagicLeapContactsFunctionLibrary::execEditContactAsync },
			{ "RequestContactsAsync", &UMagicLeapContactsFunctionLibrary::execRequestContactsAsync },
			{ "SearchContactsAsync", &UMagicLeapContactsFunctionLibrary::execSearchContactsAsync },
			{ "SelectContactsAsync", &UMagicLeapContactsFunctionLibrary::execSelectContactsAsync },
			{ "SetLogDelegate", &UMagicLeapContactsFunctionLibrary::execSetLogDelegate },
			{ "Shutdown", &UMagicLeapContactsFunctionLibrary::execShutdown },
			{ "Startup", &UMagicLeapContactsFunctionLibrary::execStartup },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventAddContactAsync_Parms
		{
			FMagicLeapContact Contact;
			FScriptDelegate ResultDelegate;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contact_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contact;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_Contact_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_Contact = { "Contact", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventAddContactAsync_Parms, Contact), Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_Contact_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_Contact_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventAddContactAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventAddContactAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_Contact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the creation of a new contact.\n\x09\x09@param Contact The contact to be created.\n\x09\x09@param ResultDelegate The delegate to be notified upon creation of the contact.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Initiates the creation of a new contact.\n@param Contact The contact to be created.\n@param ResultDelegate The delegate to be notified upon creation of the contact.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "AddContactAsync", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventAddContactAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventDeleteContactAsync_Parms
		{
			FMagicLeapContact Contact;
			FScriptDelegate ResultDelegate;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contact_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contact;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_Contact_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_Contact = { "Contact", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventDeleteContactAsync_Parms, Contact), Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_Contact_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_Contact_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventDeleteContactAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventDeleteContactAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_Contact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/** \n\x09\x09Initiates the deletion of an existing contact.\n\x09\x09@param Contact The contact to be deleted.\n\x09\x09@param ResultDelegate The delegate to be notified upon deletion of the contact.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Initiates the deletion of an existing contact.\n@param Contact The contact to be deleted.\n@param ResultDelegate The delegate to be notified upon deletion of the contact.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "DeleteContactAsync", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventDeleteContactAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventEditContactAsync_Parms
		{
			FMagicLeapContact Contact;
			FScriptDelegate ResultDelegate;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Contact_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Contact;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_Contact_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_Contact = { "Contact", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventEditContactAsync_Parms, Contact), Z_Construct_UScriptStruct_FMagicLeapContact, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_Contact_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_Contact_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventEditContactAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapSingleContactResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventEditContactAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_Contact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the update of an existing contact.\n\x09\x09@param Contact The contact to be updated.\n\x09\x09@param ResultDelegate The delegate to be notified upon update of the contact.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Initiates the update of an existing contact.\n@param Contact The contact to be updated.\n@param ResultDelegate The delegate to be notified upon update of the contact.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "EditContactAsync", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventEditContactAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventRequestContactsAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			int32 MaxNumResults;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumResults;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventRequestContactsAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_MaxNumResults = { "MaxNumResults", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventRequestContactsAsync_Parms, MaxNumResults), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventRequestContactsAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_MaxNumResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/** \n\x09\x09Initiates the retrieval of the entire contacts list from the cloud.\n\x09\x09@param MaxNumResults The maximum number of results to return.\n\x09\x09@param ResultDelegate The delegate to be notified once the contacts list has been retrieved from the cloud.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Initiates the retrieval of the entire contacts list from the cloud.\n@param MaxNumResults The maximum number of results to return.\n@param ResultDelegate The delegate to be notified once the contacts list has been retrieved from the cloud.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "RequestContactsAsync", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventRequestContactsAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventSearchContactsAsync_Parms
		{
			FString Query;
			EMagicLeapContactsSearchField SearchField;
			FScriptDelegate ResultDelegate;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Query_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Query;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SearchField_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SearchField;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_Query_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_Query = { "Query", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSearchContactsAsync_Parms, Query), METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_Query_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_Query_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_SearchField_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_SearchField = { "SearchField", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSearchContactsAsync_Parms, SearchField), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSearchContactsAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSearchContactsAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_Query,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_SearchField_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_SearchField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates a search for contacts with a given query across specified fields.\n\x09\x09@param Query The search string to look for instances of.\n\x09\x09@param SearchField The field within the contact to match the query against.\n\x09\x09@param ResultDelegate The delegate to be notified upon completion of the query.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Initiates a search for contacts with a given query across specified fields.\n@param Query The search string to look for instances of.\n@param SearchField The field within the contact to match the query against.\n@param ResultDelegate The delegate to be notified upon completion of the query.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "SearchContactsAsync", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventSearchContactsAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventSelectContactsAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			int32 MaxNumResults;
			EMagicLeapContactsSearchField SearchField;
			FGuid ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumResults;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SearchField_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SearchField;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSelectContactsAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapMultipleContactsResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_MaxNumResults = { "MaxNumResults", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSelectContactsAsync_Parms, MaxNumResults), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_SearchField_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_SearchField = { "SearchField", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSelectContactsAsync_Parms, SearchField), Z_Construct_UEnum_MagicLeapContacts_EMagicLeapContactsSearchField, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSelectContactsAsync_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_MaxNumResults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_SearchField_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_SearchField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Pops up a dialog allowing the user to manually select the contacts they wish to query.\n\x09\x09@param MaxNumResults The maximum number of contacts to display (values greater than number of contacts will result in an invalid param error).\n\x09\x09@param SearchField Specifies which field(s) to retrieve for each selected contact.\n\x09\x09@return A unique identifier for this request.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Pops up a dialog allowing the user to manually select the contacts they wish to query.\n@param MaxNumResults The maximum number of contacts to display (values greater than number of contacts will result in an invalid param error).\n@param SearchField Specifies which field(s) to retrieve for each selected contact.\n@return A unique identifier for this request." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "SelectContactsAsync", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventSelectContactsAsync_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventSetLogDelegate_Parms
		{
			FScriptDelegate LogDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_LogDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate = { "LogDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapContactsFunctionLibrary_eventSetLogDelegate_Parms, LogDelegate), Z_Construct_UDelegateFunction_MagicLeapContacts_MagicLeapContactsLogMessage__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapContactsFunctionLibrary_eventSetLogDelegate_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapContactsFunctionLibrary_eventSetLogDelegate_Parms), &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the delegate by which the system can pass log messages back to the calling blueprint.\n\x09\x09@param LogDelegate The delegate by which the system will return log messages to the calling blueprint.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Sets the delegate by which the system can pass log messages back to the calling blueprint.\n@param LogDelegate The delegate by which the system will return log messages to the calling blueprint.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "SetLogDelegate", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventSetLogDelegate_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventShutdown_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapContactsFunctionLibrary_eventShutdown_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapContactsFunctionLibrary_eventShutdown_Parms), &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/** Deinitialize all resources for this API. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Deinitialize all resources for this API." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "Shutdown", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventShutdown_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics
	{
		struct MagicLeapContactsFunctionLibrary_eventStartup_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapContactsFunctionLibrary_eventStartup_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapContactsFunctionLibrary_eventStartup_Parms), &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::Function_MetaDataParams[] = {
		{ "Category", "Contacts Function Library | MagicLeap" },
		{ "Comment", "/** Initialize all necessary resources for using the Contacts API. */" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
		{ "ToolTip", "Initialize all necessary resources for using the Contacts API." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, nullptr, "Startup", nullptr, nullptr, sizeof(MagicLeapContactsFunctionLibrary_eventStartup_Parms), Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_NoRegister()
	{
		return UMagicLeapContactsFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapContacts,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_AddContactAsync, "AddContactAsync" }, // 145107679
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_DeleteContactAsync, "DeleteContactAsync" }, // 1856123440
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_EditContactAsync, "EditContactAsync" }, // 698019019
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_RequestContactsAsync, "RequestContactsAsync" }, // 3697671181
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SearchContactsAsync, "SearchContactsAsync" }, // 3844323904
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SelectContactsAsync, "SelectContactsAsync" }, // 493864505
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_SetLogDelegate, "SetLogDelegate" }, // 1636959817
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Shutdown, "Shutdown" }, // 348155221
		{ &Z_Construct_UFunction_UMagicLeapContactsFunctionLibrary_Startup, "Startup" }, // 1879539588
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "IncludePath", "MagicLeapContactsFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapContactsFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapContactsFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapContactsFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapContactsFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapContactsFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapContactsFunctionLibrary, 1887604484);
	template<> MAGICLEAPCONTACTS_API UClass* StaticClass<UMagicLeapContactsFunctionLibrary>()
	{
		return UMagicLeapContactsFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapContactsFunctionLibrary(Z_Construct_UClass_UMagicLeapContactsFunctionLibrary, &UMagicLeapContactsFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapContacts"), TEXT("UMagicLeapContactsFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapContactsFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
