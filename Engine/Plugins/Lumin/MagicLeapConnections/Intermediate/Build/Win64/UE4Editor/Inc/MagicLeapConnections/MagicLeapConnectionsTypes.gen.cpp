// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapConnectionsTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapConnectionsTypes() {}
// Cross Module References
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapConnections();
	MAGICLEAPCONNECTIONS_API UEnum* Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature();
	MAGICLEAPCONNECTIONS_API UEnum* Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteeFilter();
	MAGICLEAPCONNECTIONS_API UEnum* Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsResult();
	MAGICLEAPCONNECTIONS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapConnections_eventMagicLeapInviteSentDelegateMulti_Parms
		{
			EMagicLeapConnectionsInviteStatus InviteStatus;
			FGuid InviteHandle;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InviteStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InviteStatus;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InviteHandle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::NewProp_InviteStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::NewProp_InviteStatus = { "InviteStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegateMulti_Parms, InviteStatus), Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::NewProp_InviteHandle = { "InviteHandle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegateMulti_Parms, InviteHandle), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::NewProp_InviteStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::NewProp_InviteStatus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::NewProp_InviteHandle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections, nullptr, "MagicLeapInviteSentDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapConnections_eventMagicLeapInviteSentDelegate_Parms
		{
			EMagicLeapConnectionsInviteStatus InviteStatus;
			FGuid InviteHandle;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InviteStatus_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InviteStatus;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InviteHandle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::NewProp_InviteStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::NewProp_InviteStatus = { "InviteStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegate_Parms, InviteStatus), Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::NewProp_InviteHandle = { "InviteHandle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegate_Parms, InviteHandle), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::NewProp_InviteStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::NewProp_InviteStatus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::NewProp_InviteHandle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to notify the application when a send invite operation has completed. */" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Delegate used to notify the application when a send invite operation has completed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections, nullptr, "MagicLeapInviteSentDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms
		{
			bool bUserAccepted;
			FString Payload;
		};
		static void NewProp_bUserAccepted_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUserAccepted;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Payload;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::NewProp_bUserAccepted_SetBit(void* Obj)
	{
		((_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms*)Obj)->bUserAccepted = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::NewProp_bUserAccepted = { "bUserAccepted", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::NewProp_bUserAccepted_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::NewProp_Payload = { "Payload", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms, Payload), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::NewProp_bUserAccepted,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::NewProp_Payload,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections, nullptr, "MagicLeapInviteReceivedDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms
		{
			bool bUserAccepted;
			FString Payload;
		};
		static void NewProp_bUserAccepted_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUserAccepted;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Payload;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::NewProp_bUserAccepted_SetBit(void* Obj)
	{
		((_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms*)Obj)->bUserAccepted = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::NewProp_bUserAccepted = { "bUserAccepted", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms), &Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::NewProp_bUserAccepted_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::NewProp_Payload = { "Payload", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms, Payload), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::NewProp_bUserAccepted,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::NewProp_Payload,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to notify the application when a receive invite operation has completed. */" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Delegate used to notify the application when a receive invite operation has completed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections, nullptr, "MagicLeapInviteReceivedDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMagicLeapConnectionsInviteeFilter_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteeFilter, Z_Construct_UPackage__Script_MagicLeapConnections(), TEXT("EMagicLeapConnectionsInviteeFilter"));
		}
		return Singleton;
	}
	template<> MAGICLEAPCONNECTIONS_API UEnum* StaticEnum<EMagicLeapConnectionsInviteeFilter>()
	{
		return EMagicLeapConnectionsInviteeFilter_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapConnectionsInviteeFilter(EMagicLeapConnectionsInviteeFilter_StaticEnum, TEXT("/Script/MagicLeapConnections"), TEXT("EMagicLeapConnectionsInviteeFilter"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteeFilter_Hash() { return 3144719604U; }
	UEnum* Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteeFilter()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapConnections();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapConnectionsInviteeFilter"), 0, Get_Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteeFilter_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapConnectionsInviteeFilter::Following", (int64)EMagicLeapConnectionsInviteeFilter::Following },
				{ "EMagicLeapConnectionsInviteeFilter::Followers", (int64)EMagicLeapConnectionsInviteeFilter::Followers },
				{ "EMagicLeapConnectionsInviteeFilter::Mutual", (int64)EMagicLeapConnectionsInviteeFilter::Mutual },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Followers.Comment", "/** Show Magic Leap connections who are online and follow current user. */" },
				{ "Followers.Name", "EMagicLeapConnectionsInviteeFilter::Followers" },
				{ "Followers.ToolTip", "Show Magic Leap connections who are online and follow current user." },
				{ "Following.Comment", "/** Show Magic Leap connections who are online and followed by current user. */" },
				{ "Following.Name", "EMagicLeapConnectionsInviteeFilter::Following" },
				{ "Following.ToolTip", "Show Magic Leap connections who are online and followed by current user." },
				{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
				{ "Mutual.Comment", "/** Show Magic Leap connections who are online and are mutual followers for current user. */" },
				{ "Mutual.Name", "EMagicLeapConnectionsInviteeFilter::Mutual" },
				{ "Mutual.ToolTip", "Show Magic Leap connections who are online and are mutual followers for current user." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections,
				nullptr,
				"EMagicLeapConnectionsInviteeFilter",
				"EMagicLeapConnectionsInviteeFilter",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapConnectionsInviteStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus, Z_Construct_UPackage__Script_MagicLeapConnections(), TEXT("EMagicLeapConnectionsInviteStatus"));
		}
		return Singleton;
	}
	template<> MAGICLEAPCONNECTIONS_API UEnum* StaticEnum<EMagicLeapConnectionsInviteStatus>()
	{
		return EMagicLeapConnectionsInviteStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapConnectionsInviteStatus(EMagicLeapConnectionsInviteStatus_StaticEnum, TEXT("/Script/MagicLeapConnections"), TEXT("EMagicLeapConnectionsInviteStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus_Hash() { return 2605824802U; }
	UEnum* Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapConnections();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapConnectionsInviteStatus"), 0, Get_Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapConnectionsInviteStatus::SubmittingRequest", (int64)EMagicLeapConnectionsInviteStatus::SubmittingRequest },
				{ "EMagicLeapConnectionsInviteStatus::Pending", (int64)EMagicLeapConnectionsInviteStatus::Pending },
				{ "EMagicLeapConnectionsInviteStatus::Dispatched", (int64)EMagicLeapConnectionsInviteStatus::Dispatched },
				{ "EMagicLeapConnectionsInviteStatus::DispatchFailed", (int64)EMagicLeapConnectionsInviteStatus::DispatchFailed },
				{ "EMagicLeapConnectionsInviteStatus::Cancelled", (int64)EMagicLeapConnectionsInviteStatus::Cancelled },
				{ "EMagicLeapConnectionsInviteStatus::InvalidHandle", (int64)EMagicLeapConnectionsInviteStatus::InvalidHandle },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Cancelled.Comment", "/** Indicates sending process was cancelled either by user, system or \\see MLConnectionsCancelInvite. */" },
				{ "Cancelled.Name", "EMagicLeapConnectionsInviteStatus::Cancelled" },
				{ "Cancelled.ToolTip", "Indicates sending process was cancelled either by user, system or \\see MLConnectionsCancelInvite." },
				{ "Dispatched.Comment", "/** Indicates invite dialog has been completed by the user the invite was successfully sent.\n      Invite request resources ready to be freed \\see MLContactsReleaseRequestResources. */" },
				{ "Dispatched.Name", "EMagicLeapConnectionsInviteStatus::Dispatched" },
				{ "Dispatched.ToolTip", "Indicates invite dialog has been completed by the user the invite was successfully sent.\n     Invite request resources ready to be freed \\see MLContactsReleaseRequestResources." },
				{ "DispatchFailed.Comment", "/** Indicates that the user has completed the invite dialog but the system was unable to send the invite. */" },
				{ "DispatchFailed.Name", "EMagicLeapConnectionsInviteStatus::DispatchFailed" },
				{ "DispatchFailed.ToolTip", "Indicates that the user has completed the invite dialog but the system was unable to send the invite." },
				{ "InvalidHandle.Comment", "/** Unable to determine invite status, because provided handle is invalid. */" },
				{ "InvalidHandle.Name", "EMagicLeapConnectionsInviteStatus::InvalidHandle" },
				{ "InvalidHandle.ToolTip", "Unable to determine invite status, because provided handle is invalid." },
				{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
				{ "Pending.Comment", "/** Indicates sending process has successfully initiated and invite dialog is being displayed to the user. */" },
				{ "Pending.Name", "EMagicLeapConnectionsInviteStatus::Pending" },
				{ "Pending.ToolTip", "Indicates sending process has successfully initiated and invite dialog is being displayed to the user." },
				{ "SubmittingRequest.Comment", "/** Indicates the request to start the sending process is being submitted to the system. */" },
				{ "SubmittingRequest.Name", "EMagicLeapConnectionsInviteStatus::SubmittingRequest" },
				{ "SubmittingRequest.ToolTip", "Indicates the request to start the sending process is being submitted to the system." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections,
				nullptr,
				"EMagicLeapConnectionsInviteStatus",
				"EMagicLeapConnectionsInviteStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapConnectionsResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsResult, Z_Construct_UPackage__Script_MagicLeapConnections(), TEXT("EMagicLeapConnectionsResult"));
		}
		return Singleton;
	}
	template<> MAGICLEAPCONNECTIONS_API UEnum* StaticEnum<EMagicLeapConnectionsResult>()
	{
		return EMagicLeapConnectionsResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapConnectionsResult(EMagicLeapConnectionsResult_StaticEnum, TEXT("/Script/MagicLeapConnections"), TEXT("EMagicLeapConnectionsResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsResult_Hash() { return 1373318140U; }
	UEnum* Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapConnections();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapConnectionsResult"), 0, Get_Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapConnectionsResult::InvalidHandle", (int64)EMagicLeapConnectionsResult::InvalidHandle },
				{ "EMagicLeapConnectionsResult::InvalidInviteeCount", (int64)EMagicLeapConnectionsResult::InvalidInviteeCount },
				{ "EMagicLeapConnectionsResult::CancellationPending", (int64)EMagicLeapConnectionsResult::CancellationPending },
				{ "EMagicLeapConnectionsResult::IllegalState", (int64)EMagicLeapConnectionsResult::IllegalState },
				{ "EMagicLeapConnectionsResult::NetworkFailure", (int64)EMagicLeapConnectionsResult::NetworkFailure },
				{ "EMagicLeapConnectionsResult::AlreadyRegistered", (int64)EMagicLeapConnectionsResult::AlreadyRegistered },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlreadyRegistered.Comment", "/** \\see MLConnectionsRegisterForInvite failed because the application is\n      already registered to handle invite requests. */" },
				{ "AlreadyRegistered.Name", "EMagicLeapConnectionsResult::AlreadyRegistered" },
				{ "AlreadyRegistered.ToolTip", "\\see MLConnectionsRegisterForInvite failed because the application is\n     already registered to handle invite requests." },
				{ "BlueprintType", "true" },
				{ "CancellationPending.Comment", "/** Indicates invite request has been found and the system is attempting to cancel the process. */" },
				{ "CancellationPending.Name", "EMagicLeapConnectionsResult::CancellationPending" },
				{ "CancellationPending.ToolTip", "Indicates invite request has been found and the system is attempting to cancel the process." },
				{ "IllegalState.Comment", "/** Request failed due to system being in an illegal state,\n      e.g. user has not successfully logged-in. */" },
				{ "IllegalState.Name", "EMagicLeapConnectionsResult::IllegalState" },
				{ "IllegalState.ToolTip", "Request failed due to system being in an illegal state,\n     e.g. user has not successfully logged-in." },
				{ "InvalidHandle.Comment", "/** This MLHandle is not recognized. */" },
				{ "InvalidHandle.Name", "EMagicLeapConnectionsResult::InvalidHandle" },
				{ "InvalidHandle.ToolTip", "This MLHandle is not recognized." },
				{ "InvalidInviteeCount.Comment", "/** Indicates number of invitees is invalid. */" },
				{ "InvalidInviteeCount.Name", "EMagicLeapConnectionsResult::InvalidInviteeCount" },
				{ "InvalidInviteeCount.ToolTip", "Indicates number of invitees is invalid." },
				{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
				{ "NetworkFailure.Comment", "/** \\see MLConnectionsRegisterForInvite failed because the system had an error\n      with network connectivity, or the servers could not be reached. */" },
				{ "NetworkFailure.Name", "EMagicLeapConnectionsResult::NetworkFailure" },
				{ "NetworkFailure.ToolTip", "\\see MLConnectionsRegisterForInvite failed because the system had an error\n     with network connectivity, or the servers could not be reached." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapConnections,
				nullptr,
				"EMagicLeapConnectionsResult",
				"EMagicLeapConnectionsResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMagicLeapConnectionsInviteArgs::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPCONNECTIONS_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs, Z_Construct_UPackage__Script_MagicLeapConnections(), TEXT("MagicLeapConnectionsInviteArgs"), sizeof(FMagicLeapConnectionsInviteArgs), Get_Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPCONNECTIONS_API UScriptStruct* StaticStruct<FMagicLeapConnectionsInviteArgs>()
{
	return FMagicLeapConnectionsInviteArgs::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapConnectionsInviteArgs(FMagicLeapConnectionsInviteArgs::StaticStruct, TEXT("/Script/MagicLeapConnections"), TEXT("MagicLeapConnectionsInviteArgs"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapConnections_StaticRegisterNativesFMagicLeapConnectionsInviteArgs
{
	FScriptStruct_MagicLeapConnections_StaticRegisterNativesFMagicLeapConnectionsInviteArgs()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapConnectionsInviteArgs>(FName(TEXT("MagicLeapConnectionsInviteArgs")));
	}
} ScriptStruct_MagicLeapConnections_StaticRegisterNativesFMagicLeapConnectionsInviteArgs;
	struct Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InviteeCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InviteeCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InviteUserPrompt_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InviteUserPrompt;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InvitePayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InvitePayload;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultInviteeFilter_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultInviteeFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DefaultInviteeFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Stores arguments for the sending invite process */" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Stores arguments for the sending invite process" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapConnectionsInviteArgs>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteeCount_MetaData[] = {
		{ "Category", "Connections|MagicLeap" },
		{ "Comment", "/** Max number of connections to be invited.  Min limit is 1. */" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Max number of connections to be invited.  Min limit is 1." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteeCount = { "InviteeCount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapConnectionsInviteArgs, InviteeCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteeCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteeCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteUserPrompt_MetaData[] = {
		{ "Category", "Connections|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Text prompt to be displayed to the user with invitee selection dialog.  Caller should allocate memory for this.\n\x09\x09""Encoding should be in UTF8.  This will be copied internally.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Text prompt to be displayed to the user with invitee selection dialog.  Caller should allocate memory for this.\nEncoding should be in UTF8.  This will be copied internally." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteUserPrompt = { "InviteUserPrompt", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapConnectionsInviteArgs, InviteUserPrompt), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteUserPrompt_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteUserPrompt_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InvitePayload_MetaData[] = {
		{ "Category", "Connections|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Payload message to be delivered to remote copy of the application with invite in serialized text form.  Caller should\n\x09\x09""allocate memory for this.  Encoding should be in UTF8.  This will be copied internally.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Payload message to be delivered to remote copy of the application with invite in serialized text form.  Caller should\nallocate memory for this.  Encoding should be in UTF8.  This will be copied internally." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InvitePayload = { "InvitePayload", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapConnectionsInviteArgs, InvitePayload), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InvitePayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InvitePayload_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter_MetaData[] = {
		{ "Category", "Connections|MagicLeap" },
		{ "Comment", "/** Type of filter applied by default to ML connections list in invitee selection dialog. */" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsTypes.h" },
		{ "ToolTip", "Type of filter applied by default to ML connections list in invitee selection dialog." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter = { "DefaultInviteeFilter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapConnectionsInviteArgs, DefaultInviteeFilter), Z_Construct_UEnum_MagicLeapConnections_EMagicLeapConnectionsInviteeFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteeCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InviteUserPrompt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_InvitePayload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::NewProp_DefaultInviteeFilter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapConnections,
		nullptr,
		&NewStructOps,
		"MagicLeapConnectionsInviteArgs",
		sizeof(FMagicLeapConnectionsInviteArgs),
		alignof(FMagicLeapConnectionsInviteArgs),
		Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapConnections();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapConnectionsInviteArgs"), sizeof(FMagicLeapConnectionsInviteArgs), Get_Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Hash() { return 3030703846U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
