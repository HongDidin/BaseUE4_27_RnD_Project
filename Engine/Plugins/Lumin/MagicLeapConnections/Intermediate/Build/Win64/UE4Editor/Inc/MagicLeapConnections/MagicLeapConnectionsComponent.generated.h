// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FGuid;
struct FMagicLeapConnectionsInviteArgs;
#ifdef MAGICLEAPCONNECTIONS_MagicLeapConnectionsComponent_generated_h
#error "MagicLeapConnectionsComponent.generated.h already included, missing '#pragma once' in MagicLeapConnectionsComponent.h"
#endif
#define MAGICLEAPCONNECTIONS_MagicLeapConnectionsComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCancelInvite); \
	DECLARE_FUNCTION(execSendInviteAsync); \
	DECLARE_FUNCTION(execIsInvitesEnabled); \
	DECLARE_FUNCTION(execDisableInvites); \
	DECLARE_FUNCTION(execEnableInvitesAsync);


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCancelInvite); \
	DECLARE_FUNCTION(execSendInviteAsync); \
	DECLARE_FUNCTION(execIsInvitesEnabled); \
	DECLARE_FUNCTION(execDisableInvites); \
	DECLARE_FUNCTION(execEnableInvitesAsync);


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapConnectionsComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapConnectionsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapConnections"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapConnectionsComponent)


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapConnectionsComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapConnectionsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapConnections"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapConnectionsComponent)


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapConnectionsComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapConnectionsComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapConnectionsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapConnectionsComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapConnectionsComponent(UMagicLeapConnectionsComponent&&); \
	NO_API UMagicLeapConnectionsComponent(const UMagicLeapConnectionsComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapConnectionsComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapConnectionsComponent(UMagicLeapConnectionsComponent&&); \
	NO_API UMagicLeapConnectionsComponent(const UMagicLeapConnectionsComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapConnectionsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapConnectionsComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapConnectionsComponent)


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnInviteReceived() { return STRUCT_OFFSET(UMagicLeapConnectionsComponent, OnInviteReceived); } \
	FORCEINLINE static uint32 __PPO__OnInviteSent() { return STRUCT_OFFSET(UMagicLeapConnectionsComponent, OnInviteSent); }


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCONNECTIONS_API UClass* StaticClass<class UMagicLeapConnectionsComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
