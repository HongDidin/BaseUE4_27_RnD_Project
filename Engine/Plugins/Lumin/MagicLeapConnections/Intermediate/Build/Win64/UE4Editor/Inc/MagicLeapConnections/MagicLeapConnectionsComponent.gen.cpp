// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapConnectionsComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapConnectionsComponent() {}
// Cross Module References
	MAGICLEAPCONNECTIONS_API UClass* Z_Construct_UClass_UMagicLeapConnectionsComponent_NoRegister();
	MAGICLEAPCONNECTIONS_API UClass* Z_Construct_UClass_UMagicLeapConnectionsComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapConnections();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MAGICLEAPCONNECTIONS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapConnectionsComponent::execCancelInvite)
	{
		P_GET_STRUCT_REF(FGuid,Z_Param_Out_InviteRequestHandle);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CancelInvite(Z_Param_Out_InviteRequestHandle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsComponent::execSendInviteAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapConnectionsInviteArgs,Z_Param_Out_Args);
		P_GET_STRUCT_REF(FGuid,Z_Param_Out_OutInviteHandle);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SendInviteAsync(Z_Param_Out_Args,Z_Param_Out_OutInviteHandle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsComponent::execIsInvitesEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsInvitesEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsComponent::execDisableInvites)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DisableInvites();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsComponent::execEnableInvitesAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EnableInvitesAsync();
		P_NATIVE_END;
	}
	void UMagicLeapConnectionsComponent::StaticRegisterNativesUMagicLeapConnectionsComponent()
	{
		UClass* Class = UMagicLeapConnectionsComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CancelInvite", &UMagicLeapConnectionsComponent::execCancelInvite },
			{ "DisableInvites", &UMagicLeapConnectionsComponent::execDisableInvites },
			{ "EnableInvitesAsync", &UMagicLeapConnectionsComponent::execEnableInvitesAsync },
			{ "IsInvitesEnabled", &UMagicLeapConnectionsComponent::execIsInvitesEnabled },
			{ "SendInviteAsync", &UMagicLeapConnectionsComponent::execSendInviteAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics
	{
		struct MagicLeapConnectionsComponent_eventCancelInvite_Parms
		{
			FGuid InviteRequestHandle;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InviteRequestHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InviteRequestHandle;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_InviteRequestHandle_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_InviteRequestHandle = { "InviteRequestHandle", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsComponent_eventCancelInvite_Parms, InviteRequestHandle), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_InviteRequestHandle_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_InviteRequestHandle_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapConnectionsComponent_eventCancelInvite_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapConnectionsComponent_eventCancelInvite_Parms), &Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_InviteRequestHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to cancel a previously requested invite sending process.  If invite dialog has not yet been completed by the\n\x09\x09user, this request will dismiss the dialog and cancel the invite sending process.  Otherwise this operation will return an\n\x09\x09""error.\n\x09\x09@param InviteRequestHandle The handle to the invite to be cancelled.\n\x09\x09@return True if the invite was cancelled, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Attempts to cancel a previously requested invite sending process.  If invite dialog has not yet been completed by the\nuser, this request will dismiss the dialog and cancel the invite sending process.  Otherwise this operation will return an\nerror.\n@param InviteRequestHandle The handle to the invite to be cancelled.\n@return True if the invite was cancelled, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsComponent, nullptr, "CancelInvite", nullptr, nullptr, sizeof(MagicLeapConnectionsComponent_eventCancelInvite_Parms), Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Disables the sending and receiving of invites.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Disables the sending and receiving of invites." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsComponent, nullptr, "DisableInvites", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Enables the sending and receiving of invites.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Enables the sending and receiving of invites." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsComponent, nullptr, "EnableInvitesAsync", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics
	{
		struct MagicLeapConnectionsComponent_eventIsInvitesEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapConnectionsComponent_eventIsInvitesEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapConnectionsComponent_eventIsInvitesEnabled_Parms), &Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Indicates the current status of the invite send/receive servers.\n\x09\x09@return True if sending and receiving invites are enabled, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Indicates the current status of the invite send/receive servers.\n@return True if sending and receiving invites are enabled, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsComponent, nullptr, "IsInvitesEnabled", nullptr, nullptr, sizeof(MagicLeapConnectionsComponent_eventIsInvitesEnabled_Parms), Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics
	{
		struct MagicLeapConnectionsComponent_eventSendInviteAsync_Parms
		{
			FMagicLeapConnectionsInviteArgs Args;
			FGuid OutInviteHandle;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Args_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Args;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutInviteHandle;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_Args_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_Args = { "Args", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsComponent_eventSendInviteAsync_Parms, Args), Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_Args_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_Args_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_OutInviteHandle = { "OutInviteHandle", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsComponent_eventSendInviteAsync_Parms, OutInviteHandle), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapConnectionsComponent_eventSendInviteAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapConnectionsComponent_eventSendInviteAsync_Parms), &Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_Args,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_OutInviteHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Submits a request to start the invite sending process.  Request an invite to be sent for other connections to join a\n\x09\x09multi-user experience.  This call will trigger a connections invite dialog requesting the user to select up to the\n\x09\x09specified number of online users to be invited.  The system will then initiate a push notification to other online devices,\n\x09\x09start a copy of the application requesting the invite and deliver the given payload.  If the requesting application is not\n\x09\x09installed on the receiving device, the system will prompt the user to install via Magic Leap World app.\n\x09\x09@param Args The arguments for the sending invite process.\n\x09\x09@param OutInviteHandle A valid FGuid to the invite request if the function call succeeds.\n\x09\x09@return True if the invite is successfully created, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Submits a request to start the invite sending process.  Request an invite to be sent for other connections to join a\nmulti-user experience.  This call will trigger a connections invite dialog requesting the user to select up to the\nspecified number of online users to be invited.  The system will then initiate a push notification to other online devices,\nstart a copy of the application requesting the invite and deliver the given payload.  If the requesting application is not\ninstalled on the receiving device, the system will prompt the user to install via Magic Leap World app.\n@param Args The arguments for the sending invite process.\n@param OutInviteHandle A valid FGuid to the invite request if the function call succeeds.\n@return True if the invite is successfully created, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsComponent, nullptr, "SendInviteAsync", nullptr, nullptr, sizeof(MagicLeapConnectionsComponent_eventSendInviteAsync_Parms), Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapConnectionsComponent_NoRegister()
	{
		return UMagicLeapConnectionsComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInviteReceived_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnInviteReceived;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInviteSent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnInviteSent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapConnections,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapConnectionsComponent_CancelInvite, "CancelInvite" }, // 3015417940
		{ &Z_Construct_UFunction_UMagicLeapConnectionsComponent_DisableInvites, "DisableInvites" }, // 4227922863
		{ &Z_Construct_UFunction_UMagicLeapConnectionsComponent_EnableInvitesAsync, "EnableInvitesAsync" }, // 309664061
		{ &Z_Construct_UFunction_UMagicLeapConnectionsComponent_IsInvitesEnabled, "IsInvitesEnabled" }, // 2099638433
		{ &Z_Construct_UFunction_UMagicLeapConnectionsComponent_SendInviteAsync, "SendInviteAsync" }, // 3553054991
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09""Component that provides access to the Connections API functionality.\n*/" },
		{ "IncludePath", "MagicLeapConnectionsComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Component that provides access to the Connections API functionality." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteReceived_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Connections | MagicLeap" },
		{ "Comment", "// Delegate instances\n" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
		{ "ToolTip", "Delegate instances" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteReceived = { "OnInviteReceived", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapConnectionsComponent, OnInviteReceived), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteReceived_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteReceived_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteSent_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Connections | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteSent = { "OnInviteSent", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapConnectionsComponent, OnInviteSent), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteSent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteSent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteReceived,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::NewProp_OnInviteSent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapConnectionsComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::ClassParams = {
		&UMagicLeapConnectionsComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapConnectionsComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapConnectionsComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapConnectionsComponent, 2431824218);
	template<> MAGICLEAPCONNECTIONS_API UClass* StaticClass<UMagicLeapConnectionsComponent>()
	{
		return UMagicLeapConnectionsComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapConnectionsComponent(Z_Construct_UClass_UMagicLeapConnectionsComponent, &UMagicLeapConnectionsComponent::StaticClass, TEXT("/Script/MagicLeapConnections"), TEXT("UMagicLeapConnectionsComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapConnectionsComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
