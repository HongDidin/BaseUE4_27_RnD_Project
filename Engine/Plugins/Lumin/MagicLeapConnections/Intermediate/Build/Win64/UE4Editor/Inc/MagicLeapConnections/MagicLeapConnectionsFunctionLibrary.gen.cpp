// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapConnectionsFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapConnectionsFunctionLibrary() {}
// Cross Module References
	MAGICLEAPCONNECTIONS_API UClass* Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_NoRegister();
	MAGICLEAPCONNECTIONS_API UClass* Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapConnections();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature();
	MAGICLEAPCONNECTIONS_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs();
	MAGICLEAPCONNECTIONS_API UFunction* Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapConnectionsFunctionLibrary::execCancelInvite)
	{
		P_GET_STRUCT_REF(FGuid,Z_Param_Out_InviteRequestHandle);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapConnectionsFunctionLibrary::CancelInvite(Z_Param_Out_InviteRequestHandle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsFunctionLibrary::execSendInviteAsync)
	{
		P_GET_STRUCT_REF(FMagicLeapConnectionsInviteArgs,Z_Param_Out_Args);
		P_GET_STRUCT_REF(FGuid,Z_Param_Out_OutInviteHandle);
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_InviteSentDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapConnectionsFunctionLibrary::SendInviteAsync(Z_Param_Out_Args,Z_Param_Out_OutInviteHandle,FMagicLeapInviteSentDelegate(Z_Param_Out_InviteSentDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsFunctionLibrary::execIsInvitesEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapConnectionsFunctionLibrary::IsInvitesEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsFunctionLibrary::execDisableInvites)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapConnectionsFunctionLibrary::DisableInvites();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapConnectionsFunctionLibrary::execEnableInvitesAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_InviteReceivedDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapConnectionsFunctionLibrary::EnableInvitesAsync(FMagicLeapInviteReceivedDelegate(Z_Param_Out_InviteReceivedDelegate));
		P_NATIVE_END;
	}
	void UMagicLeapConnectionsFunctionLibrary::StaticRegisterNativesUMagicLeapConnectionsFunctionLibrary()
	{
		UClass* Class = UMagicLeapConnectionsFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CancelInvite", &UMagicLeapConnectionsFunctionLibrary::execCancelInvite },
			{ "DisableInvites", &UMagicLeapConnectionsFunctionLibrary::execDisableInvites },
			{ "EnableInvitesAsync", &UMagicLeapConnectionsFunctionLibrary::execEnableInvitesAsync },
			{ "IsInvitesEnabled", &UMagicLeapConnectionsFunctionLibrary::execIsInvitesEnabled },
			{ "SendInviteAsync", &UMagicLeapConnectionsFunctionLibrary::execSendInviteAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics
	{
		struct MagicLeapConnectionsFunctionLibrary_eventCancelInvite_Parms
		{
			FGuid InviteRequestHandle;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InviteRequestHandle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InviteRequestHandle;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_InviteRequestHandle_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_InviteRequestHandle = { "InviteRequestHandle", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsFunctionLibrary_eventCancelInvite_Parms, InviteRequestHandle), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_InviteRequestHandle_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_InviteRequestHandle_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapConnectionsFunctionLibrary_eventCancelInvite_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapConnectionsFunctionLibrary_eventCancelInvite_Parms), &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_InviteRequestHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to cancel a previously requested invite sending process.  If invite dialog has not yet been completed by the\n\x09\x09user, this request will dismiss the dialog and cancel the invite sending process.  Otherwise this operation will return an\n\x09\x09""error.\n\x09\x09@param InviteRequestHandle The handle to the invite to be cancelled.\n\x09\x09@return True if the invite was cancelled, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsFunctionLibrary.h" },
		{ "ToolTip", "Attempts to cancel a previously requested invite sending process.  If invite dialog has not yet been completed by the\nuser, this request will dismiss the dialog and cancel the invite sending process.  Otherwise this operation will return an\nerror.\n@param InviteRequestHandle The handle to the invite to be cancelled.\n@return True if the invite was cancelled, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary, nullptr, "CancelInvite", nullptr, nullptr, sizeof(MagicLeapConnectionsFunctionLibrary_eventCancelInvite_Parms), Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Disables the sending and receiving of invites.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsFunctionLibrary.h" },
		{ "ToolTip", "Disables the sending and receiving of invites." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary, nullptr, "DisableInvites", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics
	{
		struct MagicLeapConnectionsFunctionLibrary_eventEnableInvitesAsync_Parms
		{
			FScriptDelegate InviteReceivedDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InviteReceivedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_InviteReceivedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::NewProp_InviteReceivedDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::NewProp_InviteReceivedDelegate = { "InviteReceivedDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsFunctionLibrary_eventEnableInvitesAsync_Parms, InviteReceivedDelegate), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteReceivedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::NewProp_InviteReceivedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::NewProp_InviteReceivedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::NewProp_InviteReceivedDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Enables the sending and receiving of invites.\n\x09\x09@param InviteReceivedDelegate User delegate to be notified when an invite is received.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsFunctionLibrary.h" },
		{ "ToolTip", "Enables the sending and receiving of invites.\n@param InviteReceivedDelegate User delegate to be notified when an invite is received." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary, nullptr, "EnableInvitesAsync", nullptr, nullptr, sizeof(MagicLeapConnectionsFunctionLibrary_eventEnableInvitesAsync_Parms), Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics
	{
		struct MagicLeapConnectionsFunctionLibrary_eventIsInvitesEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapConnectionsFunctionLibrary_eventIsInvitesEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapConnectionsFunctionLibrary_eventIsInvitesEnabled_Parms), &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Indicates the current status of the invite send/receive servers.\n\x09\x09@return True if sending and receiving invites are enabled, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsFunctionLibrary.h" },
		{ "ToolTip", "Indicates the current status of the invite send/receive servers.\n@return True if sending and receiving invites are enabled, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary, nullptr, "IsInvitesEnabled", nullptr, nullptr, sizeof(MagicLeapConnectionsFunctionLibrary_eventIsInvitesEnabled_Parms), Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics
	{
		struct MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms
		{
			FMagicLeapConnectionsInviteArgs Args;
			FGuid OutInviteHandle;
			FScriptDelegate InviteSentDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Args_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Args;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutInviteHandle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InviteSentDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_InviteSentDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_Args_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_Args = { "Args", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms, Args), Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_Args_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_Args_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_OutInviteHandle = { "OutInviteHandle", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms, OutInviteHandle), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_InviteSentDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_InviteSentDelegate = { "InviteSentDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms, InviteSentDelegate), Z_Construct_UDelegateFunction_MagicLeapConnections_MagicLeapInviteSentDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_InviteSentDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_InviteSentDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms), &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_Args,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_OutInviteHandle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_InviteSentDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Connections Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Submits a request to start the invite sending process.  Request an invite to be sent for other connections to join a\n\x09\x09multi-user experience.  This call will trigger a connections invite dialog requesting the user to select up to the\n\x09\x09specified number of online users to be invited.  The system will then initiate a push notification to other online devices,\n\x09\x09start a copy of the application requesting the invite and deliver the given payload.  If the requesting application is not\n\x09\x09installed on the receiving device, the system will prompt the user to install via Magic Leap World app.\n\x09\x09@param Args The arguments for the sending invite process.\n\x09\x09@param OutInviteHandle A valid FGuid to the invite request if the function call succeeds.\n\x09\x09@param InviteSentDelegate User delegate to be notified when the sending process has completed.\n\x09\x09@return True if the invite is successfully created, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsFunctionLibrary.h" },
		{ "ToolTip", "Submits a request to start the invite sending process.  Request an invite to be sent for other connections to join a\nmulti-user experience.  This call will trigger a connections invite dialog requesting the user to select up to the\nspecified number of online users to be invited.  The system will then initiate a push notification to other online devices,\nstart a copy of the application requesting the invite and deliver the given payload.  If the requesting application is not\ninstalled on the receiving device, the system will prompt the user to install via Magic Leap World app.\n@param Args The arguments for the sending invite process.\n@param OutInviteHandle A valid FGuid to the invite request if the function call succeeds.\n@param InviteSentDelegate User delegate to be notified when the sending process has completed.\n@return True if the invite is successfully created, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary, nullptr, "SendInviteAsync", nullptr, nullptr, sizeof(MagicLeapConnectionsFunctionLibrary_eventSendInviteAsync_Parms), Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_NoRegister()
	{
		return UMagicLeapConnectionsFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapConnections,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_CancelInvite, "CancelInvite" }, // 3791891271
		{ &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_DisableInvites, "DisableInvites" }, // 2265243928
		{ &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_EnableInvitesAsync, "EnableInvitesAsync" }, // 1175566234
		{ &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_IsInvitesEnabled, "IsInvitesEnabled" }, // 4071428033
		{ &Z_Construct_UFunction_UMagicLeapConnectionsFunctionLibrary_SendInviteAsync, "SendInviteAsync" }, // 3561092288
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "IncludePath", "MagicLeapConnectionsFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapConnectionsFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapConnectionsFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapConnectionsFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapConnectionsFunctionLibrary, 3678789170);
	template<> MAGICLEAPCONNECTIONS_API UClass* StaticClass<UMagicLeapConnectionsFunctionLibrary>()
	{
		return UMagicLeapConnectionsFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapConnectionsFunctionLibrary(Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary, &UMagicLeapConnectionsFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapConnections"), TEXT("UMagicLeapConnectionsFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapConnectionsFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
