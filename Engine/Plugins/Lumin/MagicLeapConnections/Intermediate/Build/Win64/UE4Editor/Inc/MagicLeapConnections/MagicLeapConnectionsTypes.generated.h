// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapConnectionsInviteStatus : uint8;
struct FGuid;
#ifdef MAGICLEAPCONNECTIONS_MagicLeapConnectionsTypes_generated_h
#error "MagicLeapConnectionsTypes.generated.h already included, missing '#pragma once' in MagicLeapConnectionsTypes.h"
#endif
#define MAGICLEAPCONNECTIONS_MagicLeapConnectionsTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsTypes_h_60_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapConnectionsInviteArgs_Statics; \
	MAGICLEAPCONNECTIONS_API static class UScriptStruct* StaticStruct();


template<> MAGICLEAPCONNECTIONS_API UScriptStruct* StaticStruct<struct FMagicLeapConnectionsInviteArgs>();

#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsTypes_h_94_DELEGATE \
struct _Script_MagicLeapConnections_eventMagicLeapInviteSentDelegateMulti_Parms \
{ \
	EMagicLeapConnectionsInviteStatus InviteStatus; \
	FGuid InviteHandle; \
}; \
static inline void FMagicLeapInviteSentDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapInviteSentDelegateMulti, EMagicLeapConnectionsInviteStatus InviteStatus, FGuid InviteHandle) \
{ \
	_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegateMulti_Parms Parms; \
	Parms.InviteStatus=InviteStatus; \
	Parms.InviteHandle=InviteHandle; \
	MagicLeapInviteSentDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsTypes_h_93_DELEGATE \
struct _Script_MagicLeapConnections_eventMagicLeapInviteSentDelegate_Parms \
{ \
	EMagicLeapConnectionsInviteStatus InviteStatus; \
	FGuid InviteHandle; \
}; \
static inline void FMagicLeapInviteSentDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapInviteSentDelegate, EMagicLeapConnectionsInviteStatus InviteStatus, FGuid InviteHandle) \
{ \
	_Script_MagicLeapConnections_eventMagicLeapInviteSentDelegate_Parms Parms; \
	Parms.InviteStatus=InviteStatus; \
	Parms.InviteHandle=InviteHandle; \
	MagicLeapInviteSentDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsTypes_h_90_DELEGATE \
struct _Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms \
{ \
	bool bUserAccepted; \
	FString Payload; \
}; \
static inline void FMagicLeapInviteReceivedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapInviteReceivedDelegateMulti, bool bUserAccepted, const FString& Payload) \
{ \
	_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegateMulti_Parms Parms; \
	Parms.bUserAccepted=bUserAccepted ? true : false; \
	Parms.Payload=Payload; \
	MagicLeapInviteReceivedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsTypes_h_89_DELEGATE \
struct _Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms \
{ \
	bool bUserAccepted; \
	FString Payload; \
}; \
static inline void FMagicLeapInviteReceivedDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapInviteReceivedDelegate, bool bUserAccepted, const FString& Payload) \
{ \
	_Script_MagicLeapConnections_eventMagicLeapInviteReceivedDelegate_Parms Parms; \
	Parms.bUserAccepted=bUserAccepted ? true : false; \
	Parms.Payload=Payload; \
	MagicLeapInviteReceivedDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsTypes_h


#define FOREACH_ENUM_EMAGICLEAPCONNECTIONSINVITEEFILTER(op) \
	op(EMagicLeapConnectionsInviteeFilter::Following) \
	op(EMagicLeapConnectionsInviteeFilter::Followers) \
	op(EMagicLeapConnectionsInviteeFilter::Mutual) 

enum class EMagicLeapConnectionsInviteeFilter : uint8;
template<> MAGICLEAPCONNECTIONS_API UEnum* StaticEnum<EMagicLeapConnectionsInviteeFilter>();

#define FOREACH_ENUM_EMAGICLEAPCONNECTIONSINVITESTATUS(op) \
	op(EMagicLeapConnectionsInviteStatus::SubmittingRequest) \
	op(EMagicLeapConnectionsInviteStatus::Pending) \
	op(EMagicLeapConnectionsInviteStatus::Dispatched) \
	op(EMagicLeapConnectionsInviteStatus::DispatchFailed) \
	op(EMagicLeapConnectionsInviteStatus::Cancelled) \
	op(EMagicLeapConnectionsInviteStatus::InvalidHandle) 

enum class EMagicLeapConnectionsInviteStatus : uint8;
template<> MAGICLEAPCONNECTIONS_API UEnum* StaticEnum<EMagicLeapConnectionsInviteStatus>();

#define FOREACH_ENUM_EMAGICLEAPCONNECTIONSRESULT(op) \
	op(EMagicLeapConnectionsResult::InvalidHandle) \
	op(EMagicLeapConnectionsResult::InvalidInviteeCount) \
	op(EMagicLeapConnectionsResult::CancellationPending) \
	op(EMagicLeapConnectionsResult::IllegalState) \
	op(EMagicLeapConnectionsResult::NetworkFailure) \
	op(EMagicLeapConnectionsResult::AlreadyRegistered) 

enum class EMagicLeapConnectionsResult : uint8;
template<> MAGICLEAPCONNECTIONS_API UEnum* StaticEnum<EMagicLeapConnectionsResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
