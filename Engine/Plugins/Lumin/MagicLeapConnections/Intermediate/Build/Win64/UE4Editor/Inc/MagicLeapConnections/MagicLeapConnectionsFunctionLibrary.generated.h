// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FGuid;
struct FMagicLeapConnectionsInviteArgs;
#ifdef MAGICLEAPCONNECTIONS_MagicLeapConnectionsFunctionLibrary_generated_h
#error "MagicLeapConnectionsFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapConnectionsFunctionLibrary.h"
#endif
#define MAGICLEAPCONNECTIONS_MagicLeapConnectionsFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCancelInvite); \
	DECLARE_FUNCTION(execSendInviteAsync); \
	DECLARE_FUNCTION(execIsInvitesEnabled); \
	DECLARE_FUNCTION(execDisableInvites); \
	DECLARE_FUNCTION(execEnableInvitesAsync);


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCancelInvite); \
	DECLARE_FUNCTION(execSendInviteAsync); \
	DECLARE_FUNCTION(execIsInvitesEnabled); \
	DECLARE_FUNCTION(execDisableInvites); \
	DECLARE_FUNCTION(execEnableInvitesAsync);


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapConnectionsFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapConnectionsFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapConnections"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapConnectionsFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapConnectionsFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapConnectionsFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapConnectionsFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapConnections"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapConnectionsFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapConnectionsFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapConnectionsFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapConnectionsFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapConnectionsFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapConnectionsFunctionLibrary(UMagicLeapConnectionsFunctionLibrary&&); \
	NO_API UMagicLeapConnectionsFunctionLibrary(const UMagicLeapConnectionsFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapConnectionsFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapConnectionsFunctionLibrary(UMagicLeapConnectionsFunctionLibrary&&); \
	NO_API UMagicLeapConnectionsFunctionLibrary(const UMagicLeapConnectionsFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapConnectionsFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapConnectionsFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapConnectionsFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_9_PROLOG
#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_INCLASS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCONNECTIONS_API UClass* StaticClass<class UMagicLeapConnectionsFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapConnections_Source_Public_MagicLeapConnectionsFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
