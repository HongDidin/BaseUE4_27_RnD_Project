// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMagicLeapInputTabletDeviceButton : uint8;
struct FMagicLeapInputTabletDeviceState;
#ifdef MAGICLEAPTABLET_MagicLeapTabletTypes_generated_h
#error "MagicLeapTabletTypes.generated.h already included, missing '#pragma once' in MagicLeapTabletTypes.h"
#endif
#define MAGICLEAPTABLET_MagicLeapTabletTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_56_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPTABLET_API UScriptStruct* StaticStruct<struct FMagicLeapInputTabletDeviceState>();

#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_124_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegateMulti_Parms \
{ \
	uint8 DeviceId; \
	EMagicLeapInputTabletDeviceButton Button; \
}; \
static inline void FMagicLeapTabletOnButtonUpDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapTabletOnButtonUpDelegateMulti, uint8 DeviceId, EMagicLeapInputTabletDeviceButton const& Button) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegateMulti_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.Button=Button; \
	MagicLeapTabletOnButtonUpDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_123_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegate_Parms \
{ \
	uint8 DeviceId; \
	EMagicLeapInputTabletDeviceButton Button; \
}; \
static inline void FMagicLeapTabletOnButtonUpDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapTabletOnButtonUpDelegate, uint8 DeviceId, EMagicLeapInputTabletDeviceButton const& Button) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegate_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.Button=Button; \
	MagicLeapTabletOnButtonUpDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_121_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegateMulti_Parms \
{ \
	uint8 DeviceId; \
	EMagicLeapInputTabletDeviceButton Button; \
}; \
static inline void FMagicLeapTabletOnButtonDownDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapTabletOnButtonDownDelegateMulti, uint8 DeviceId, EMagicLeapInputTabletDeviceButton const& Button) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegateMulti_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.Button=Button; \
	MagicLeapTabletOnButtonDownDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_120_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegate_Parms \
{ \
	uint8 DeviceId; \
	EMagicLeapInputTabletDeviceButton Button; \
}; \
static inline void FMagicLeapTabletOnButtonDownDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapTabletOnButtonDownDelegate, uint8 DeviceId, EMagicLeapInputTabletDeviceButton const& Button) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegate_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.Button=Button; \
	MagicLeapTabletOnButtonDownDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_118_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegateMulti_Parms \
{ \
	uint8 DeviceId; \
	int32 TouchValue; \
}; \
static inline void FMagicLeapTabletOnRingTouchDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapTabletOnRingTouchDelegateMulti, uint8 DeviceId, int32 const& TouchValue) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegateMulti_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.TouchValue=TouchValue; \
	MagicLeapTabletOnRingTouchDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_117_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegate_Parms \
{ \
	uint8 DeviceId; \
	int32 TouchValue; \
}; \
static inline void FMagicLeapTabletOnRingTouchDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapTabletOnRingTouchDelegate, uint8 DeviceId, int32 const& TouchValue) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegate_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.TouchValue=TouchValue; \
	MagicLeapTabletOnRingTouchDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_115_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegateMulti_Parms \
{ \
	uint8 DeviceId; \
	FMagicLeapInputTabletDeviceState DeviceState; \
}; \
static inline void FMagicLeapTabletOnPenTouchDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapTabletOnPenTouchDelegateMulti, uint8 DeviceId, FMagicLeapInputTabletDeviceState const& DeviceState) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegateMulti_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.DeviceState=DeviceState; \
	MagicLeapTabletOnPenTouchDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_114_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegate_Parms \
{ \
	uint8 DeviceId; \
	FMagicLeapInputTabletDeviceState DeviceState; \
}; \
static inline void FMagicLeapTabletOnPenTouchDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapTabletOnPenTouchDelegate, uint8 DeviceId, FMagicLeapInputTabletDeviceState const& DeviceState) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegate_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	Parms.DeviceState=DeviceState; \
	MagicLeapTabletOnPenTouchDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_112_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegateMulti_Parms \
{ \
	uint8 DeviceId; \
}; \
static inline void FMagicLeapTabletOnDisconnectedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapTabletOnDisconnectedDelegateMulti, uint8 DeviceId) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegateMulti_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	MagicLeapTabletOnDisconnectedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_111_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegate_Parms \
{ \
	uint8 DeviceId; \
}; \
static inline void FMagicLeapTabletOnDisconnectedDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapTabletOnDisconnectedDelegate, uint8 DeviceId) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegate_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	MagicLeapTabletOnDisconnectedDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_109_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegateMulti_Parms \
{ \
	uint8 DeviceId; \
}; \
static inline void FMagicLeapTabletOnConnectedDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapTabletOnConnectedDelegateMulti, uint8 DeviceId) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegateMulti_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	MagicLeapTabletOnConnectedDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h_108_DELEGATE \
struct _Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegate_Parms \
{ \
	uint8 DeviceId; \
}; \
static inline void FMagicLeapTabletOnConnectedDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapTabletOnConnectedDelegate, uint8 DeviceId) \
{ \
	_Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegate_Parms Parms; \
	Parms.DeviceId=DeviceId; \
	MagicLeapTabletOnConnectedDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletTypes_h


#define FOREACH_ENUM_EMAGICLEAPINPUTTABLETDEVICEBUTTON(op) \
	op(EMagicLeapInputTabletDeviceButton::Unknown) \
	op(EMagicLeapInputTabletDeviceButton::Button1) \
	op(EMagicLeapInputTabletDeviceButton::Button2) \
	op(EMagicLeapInputTabletDeviceButton::Button3) \
	op(EMagicLeapInputTabletDeviceButton::Button4) \
	op(EMagicLeapInputTabletDeviceButton::Button5) \
	op(EMagicLeapInputTabletDeviceButton::Button6) \
	op(EMagicLeapInputTabletDeviceButton::Button7) \
	op(EMagicLeapInputTabletDeviceButton::Button8) \
	op(EMagicLeapInputTabletDeviceButton::Button9) \
	op(EMagicLeapInputTabletDeviceButton::Button10) \
	op(EMagicLeapInputTabletDeviceButton::Button11) \
	op(EMagicLeapInputTabletDeviceButton::Button12) 

enum class EMagicLeapInputTabletDeviceButton : uint8;
template<> MAGICLEAPTABLET_API UEnum* StaticEnum<EMagicLeapInputTabletDeviceButton>();

#define FOREACH_ENUM_EMAGICLEAPINPUTTABLETDEVICETOOLTYPE(op) \
	op(EMagicLeapInputTabletDeviceToolType::Unknown) \
	op(EMagicLeapInputTabletDeviceToolType::Pen) \
	op(EMagicLeapInputTabletDeviceToolType::Eraser) 

enum class EMagicLeapInputTabletDeviceToolType : uint8;
template<> MAGICLEAPTABLET_API UEnum* StaticEnum<EMagicLeapInputTabletDeviceToolType>();

#define FOREACH_ENUM_EMAGICLEAPINPUTTABLETDEVICETYPE(op) \
	op(EMagicLeapInputTabletDeviceType::Unknown) \
	op(EMagicLeapInputTabletDeviceType::Wacom) 

enum class EMagicLeapInputTabletDeviceType : uint8;
template<> MAGICLEAPTABLET_API UEnum* StaticEnum<EMagicLeapInputTabletDeviceType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
