// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapTablet_init() {}
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapTablet()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapTablet",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x18252BEE,
				0x800049FF,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
