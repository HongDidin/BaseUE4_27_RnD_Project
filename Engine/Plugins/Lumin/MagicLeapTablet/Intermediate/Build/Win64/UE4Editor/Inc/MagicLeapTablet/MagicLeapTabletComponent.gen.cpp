// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapTabletComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapTabletComponent() {}
// Cross Module References
	MAGICLEAPTABLET_API UClass* Z_Construct_UClass_UMagicLeapTabletComponent_NoRegister();
	MAGICLEAPTABLET_API UClass* Z_Construct_UClass_UMagicLeapTabletComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapTablet();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature();
// End Cross Module References
	void UMagicLeapTabletComponent::StaticRegisterNativesUMagicLeapTabletComponent()
	{
	}
	UClass* Z_Construct_UClass_UMagicLeapTabletComponent_NoRegister()
	{
		return UMagicLeapTabletComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapTabletComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnConnected_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnConnected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDisconnected_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnDisconnected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPenTouch_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPenTouch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnRingTouch_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnRingTouch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnButtonDown_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnButtonDown;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnButtonUp_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnButtonUp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapTabletComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapTablet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09""Component that provides access to the Tablet API functionality.\n*/" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "MagicLeapTabletComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
		{ "ToolTip", "Component that provides access to the Tablet API functionality." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnConnected_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "// Delegate instances\n" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
		{ "ToolTip", "Delegate instances" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnConnected = { "OnConnected", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapTabletComponent, OnConnected), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnConnected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnConnected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnDisconnected_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Tablet | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnDisconnected = { "OnDisconnected", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapTabletComponent, OnDisconnected), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnDisconnected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnDisconnected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnPenTouch_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Tablet | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnPenTouch = { "OnPenTouch", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapTabletComponent, OnPenTouch), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnPenTouch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnPenTouch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnRingTouch_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Tablet | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnRingTouch = { "OnRingTouch", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapTabletComponent, OnRingTouch), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnRingTouch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnRingTouch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonDown_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Tablet | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonDown = { "OnButtonDown", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapTabletComponent, OnButtonDown), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonDown_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonDown_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonUp_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Tablet | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonUp = { "OnButtonUp", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapTabletComponent, OnButtonUp), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonUp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonUp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapTabletComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnConnected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnDisconnected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnPenTouch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnRingTouch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonDown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapTabletComponent_Statics::NewProp_OnButtonUp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapTabletComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapTabletComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapTabletComponent_Statics::ClassParams = {
		&UMagicLeapTabletComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMagicLeapTabletComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapTabletComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapTabletComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapTabletComponent, 3389057737);
	template<> MAGICLEAPTABLET_API UClass* StaticClass<UMagicLeapTabletComponent>()
	{
		return UMagicLeapTabletComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapTabletComponent(Z_Construct_UClass_UMagicLeapTabletComponent, &UMagicLeapTabletComponent::StaticClass, TEXT("/Script/MagicLeapTablet"), TEXT("UMagicLeapTabletComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapTabletComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
