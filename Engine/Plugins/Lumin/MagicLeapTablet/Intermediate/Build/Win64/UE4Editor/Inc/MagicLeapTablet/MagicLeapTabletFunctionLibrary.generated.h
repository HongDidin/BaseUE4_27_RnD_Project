// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPTABLET_MagicLeapTabletFunctionLibrary_generated_h
#error "MagicLeapTabletFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapTabletFunctionLibrary.h"
#endif
#define MAGICLEAPTABLET_MagicLeapTabletFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetButtonUpDelegate); \
	DECLARE_FUNCTION(execSetButtonDownDelegate); \
	DECLARE_FUNCTION(execSetRingTouchDelegate); \
	DECLARE_FUNCTION(execSetPenTouchDelegate); \
	DECLARE_FUNCTION(execSetTabletDisconnectedDelegate); \
	DECLARE_FUNCTION(execSetTabletConnectedDelegate);


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetButtonUpDelegate); \
	DECLARE_FUNCTION(execSetButtonDownDelegate); \
	DECLARE_FUNCTION(execSetRingTouchDelegate); \
	DECLARE_FUNCTION(execSetPenTouchDelegate); \
	DECLARE_FUNCTION(execSetTabletDisconnectedDelegate); \
	DECLARE_FUNCTION(execSetTabletConnectedDelegate);


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapTabletFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapTabletFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapTablet"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapTabletFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapTabletFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapTabletFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapTablet"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapTabletFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapTabletFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapTabletFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapTabletFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapTabletFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapTabletFunctionLibrary(UMagicLeapTabletFunctionLibrary&&); \
	NO_API UMagicLeapTabletFunctionLibrary(const UMagicLeapTabletFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapTabletFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapTabletFunctionLibrary(UMagicLeapTabletFunctionLibrary&&); \
	NO_API UMagicLeapTabletFunctionLibrary(const UMagicLeapTabletFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapTabletFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapTabletFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapTabletFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_9_PROLOG
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_INCLASS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPTABLET_API UClass* StaticClass<class UMagicLeapTabletFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
