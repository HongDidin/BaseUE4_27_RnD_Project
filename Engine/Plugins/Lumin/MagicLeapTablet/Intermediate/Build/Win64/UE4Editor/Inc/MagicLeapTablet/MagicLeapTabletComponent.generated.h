// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPTABLET_MagicLeapTabletComponent_generated_h
#error "MagicLeapTabletComponent.generated.h already included, missing '#pragma once' in MagicLeapTabletComponent.h"
#endif
#define MAGICLEAPTABLET_MagicLeapTabletComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_RPC_WRAPPERS
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapTabletComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapTabletComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapTabletComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapTablet"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapTabletComponent)


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapTabletComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapTabletComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapTabletComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapTablet"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapTabletComponent)


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapTabletComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapTabletComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapTabletComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapTabletComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapTabletComponent(UMagicLeapTabletComponent&&); \
	NO_API UMagicLeapTabletComponent(const UMagicLeapTabletComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapTabletComponent(UMagicLeapTabletComponent&&); \
	NO_API UMagicLeapTabletComponent(const UMagicLeapTabletComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapTabletComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapTabletComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMagicLeapTabletComponent)


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnConnected() { return STRUCT_OFFSET(UMagicLeapTabletComponent, OnConnected); } \
	FORCEINLINE static uint32 __PPO__OnDisconnected() { return STRUCT_OFFSET(UMagicLeapTabletComponent, OnDisconnected); } \
	FORCEINLINE static uint32 __PPO__OnPenTouch() { return STRUCT_OFFSET(UMagicLeapTabletComponent, OnPenTouch); } \
	FORCEINLINE static uint32 __PPO__OnRingTouch() { return STRUCT_OFFSET(UMagicLeapTabletComponent, OnRingTouch); } \
	FORCEINLINE static uint32 __PPO__OnButtonDown() { return STRUCT_OFFSET(UMagicLeapTabletComponent, OnButtonDown); } \
	FORCEINLINE static uint32 __PPO__OnButtonUp() { return STRUCT_OFFSET(UMagicLeapTabletComponent, OnButtonUp); }


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPTABLET_API UClass* StaticClass<class UMagicLeapTabletComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapTablet_Source_Public_MagicLeapTabletComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
