// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapTabletFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapTabletFunctionLibrary() {}
// Cross Module References
	MAGICLEAPTABLET_API UClass* Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_NoRegister();
	MAGICLEAPTABLET_API UClass* Z_Construct_UClass_UMagicLeapTabletFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapTablet();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapTabletFunctionLibrary::execSetButtonUpDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ButtonDownDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapTabletFunctionLibrary::SetButtonUpDelegate(FMagicLeapTabletOnButtonUpDelegate(Z_Param_Out_ButtonDownDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapTabletFunctionLibrary::execSetButtonDownDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ButtonDownDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapTabletFunctionLibrary::SetButtonDownDelegate(FMagicLeapTabletOnButtonDownDelegate(Z_Param_Out_ButtonDownDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapTabletFunctionLibrary::execSetRingTouchDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_RingTouchDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapTabletFunctionLibrary::SetRingTouchDelegate(FMagicLeapTabletOnRingTouchDelegate(Z_Param_Out_RingTouchDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapTabletFunctionLibrary::execSetPenTouchDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_PenTouchDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapTabletFunctionLibrary::SetPenTouchDelegate(FMagicLeapTabletOnPenTouchDelegate(Z_Param_Out_PenTouchDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapTabletFunctionLibrary::execSetTabletDisconnectedDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_DisconnectedDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapTabletFunctionLibrary::SetTabletDisconnectedDelegate(FMagicLeapTabletOnDisconnectedDelegate(Z_Param_Out_DisconnectedDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapTabletFunctionLibrary::execSetTabletConnectedDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ConnectedDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMagicLeapTabletFunctionLibrary::SetTabletConnectedDelegate(FMagicLeapTabletOnConnectedDelegate(Z_Param_Out_ConnectedDelegate));
		P_NATIVE_END;
	}
	void UMagicLeapTabletFunctionLibrary::StaticRegisterNativesUMagicLeapTabletFunctionLibrary()
	{
		UClass* Class = UMagicLeapTabletFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetButtonDownDelegate", &UMagicLeapTabletFunctionLibrary::execSetButtonDownDelegate },
			{ "SetButtonUpDelegate", &UMagicLeapTabletFunctionLibrary::execSetButtonUpDelegate },
			{ "SetPenTouchDelegate", &UMagicLeapTabletFunctionLibrary::execSetPenTouchDelegate },
			{ "SetRingTouchDelegate", &UMagicLeapTabletFunctionLibrary::execSetRingTouchDelegate },
			{ "SetTabletConnectedDelegate", &UMagicLeapTabletFunctionLibrary::execSetTabletConnectedDelegate },
			{ "SetTabletDisconnectedDelegate", &UMagicLeapTabletFunctionLibrary::execSetTabletDisconnectedDelegate },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics
	{
		struct MagicLeapTabletFunctionLibrary_eventSetButtonDownDelegate_Parms
		{
			FScriptDelegate ButtonDownDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonDownDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ButtonDownDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::NewProp_ButtonDownDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::NewProp_ButtonDownDelegate = { "ButtonDownDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapTabletFunctionLibrary_eventSetButtonDownDelegate_Parms, ButtonDownDelegate), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::NewProp_ButtonDownDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::NewProp_ButtonDownDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::NewProp_ButtonDownDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tablet Function Library | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, nullptr, "SetButtonDownDelegate", nullptr, nullptr, sizeof(MagicLeapTabletFunctionLibrary_eventSetButtonDownDelegate_Parms), Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics
	{
		struct MagicLeapTabletFunctionLibrary_eventSetButtonUpDelegate_Parms
		{
			FScriptDelegate ButtonDownDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonDownDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ButtonDownDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::NewProp_ButtonDownDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::NewProp_ButtonDownDelegate = { "ButtonDownDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapTabletFunctionLibrary_eventSetButtonUpDelegate_Parms, ButtonDownDelegate), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::NewProp_ButtonDownDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::NewProp_ButtonDownDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::NewProp_ButtonDownDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tablet Function Library | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, nullptr, "SetButtonUpDelegate", nullptr, nullptr, sizeof(MagicLeapTabletFunctionLibrary_eventSetButtonUpDelegate_Parms), Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics
	{
		struct MagicLeapTabletFunctionLibrary_eventSetPenTouchDelegate_Parms
		{
			FScriptDelegate PenTouchDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PenTouchDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_PenTouchDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::NewProp_PenTouchDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::NewProp_PenTouchDelegate = { "PenTouchDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapTabletFunctionLibrary_eventSetPenTouchDelegate_Parms, PenTouchDelegate), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::NewProp_PenTouchDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::NewProp_PenTouchDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::NewProp_PenTouchDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tablet Function Library | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, nullptr, "SetPenTouchDelegate", nullptr, nullptr, sizeof(MagicLeapTabletFunctionLibrary_eventSetPenTouchDelegate_Parms), Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics
	{
		struct MagicLeapTabletFunctionLibrary_eventSetRingTouchDelegate_Parms
		{
			FScriptDelegate RingTouchDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RingTouchDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_RingTouchDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::NewProp_RingTouchDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::NewProp_RingTouchDelegate = { "RingTouchDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapTabletFunctionLibrary_eventSetRingTouchDelegate_Parms, RingTouchDelegate), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::NewProp_RingTouchDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::NewProp_RingTouchDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::NewProp_RingTouchDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tablet Function Library | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, nullptr, "SetRingTouchDelegate", nullptr, nullptr, sizeof(MagicLeapTabletFunctionLibrary_eventSetRingTouchDelegate_Parms), Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics
	{
		struct MagicLeapTabletFunctionLibrary_eventSetTabletConnectedDelegate_Parms
		{
			FScriptDelegate ConnectedDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ConnectedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::NewProp_ConnectedDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::NewProp_ConnectedDelegate = { "ConnectedDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapTabletFunctionLibrary_eventSetTabletConnectedDelegate_Parms, ConnectedDelegate), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::NewProp_ConnectedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::NewProp_ConnectedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::NewProp_ConnectedDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tablet Function Library | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, nullptr, "SetTabletConnectedDelegate", nullptr, nullptr, sizeof(MagicLeapTabletFunctionLibrary_eventSetTabletConnectedDelegate_Parms), Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics
	{
		struct MagicLeapTabletFunctionLibrary_eventSetTabletDisconnectedDelegate_Parms
		{
			FScriptDelegate DisconnectedDelegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisconnectedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_DisconnectedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::NewProp_DisconnectedDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::NewProp_DisconnectedDelegate = { "DisconnectedDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapTabletFunctionLibrary_eventSetTabletDisconnectedDelegate_Parms, DisconnectedDelegate), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::NewProp_DisconnectedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::NewProp_DisconnectedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::NewProp_DisconnectedDelegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tablet Function Library | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, nullptr, "SetTabletDisconnectedDelegate", nullptr, nullptr, sizeof(MagicLeapTabletFunctionLibrary_eventSetTabletDisconnectedDelegate_Parms), Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_NoRegister()
	{
		return UMagicLeapTabletFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapTablet,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonDownDelegate, "SetButtonDownDelegate" }, // 864216690
		{ &Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetButtonUpDelegate, "SetButtonUpDelegate" }, // 4178870151
		{ &Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetPenTouchDelegate, "SetPenTouchDelegate" }, // 418209244
		{ &Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetRingTouchDelegate, "SetRingTouchDelegate" }, // 240540324
		{ &Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletConnectedDelegate, "SetTabletConnectedDelegate" }, // 3690233955
		{ &Z_Construct_UFunction_UMagicLeapTabletFunctionLibrary_SetTabletDisconnectedDelegate, "SetTabletDisconnectedDelegate" }, // 1747687912
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "IncludePath", "MagicLeapTabletFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapTabletFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapTabletFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapTabletFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapTabletFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapTabletFunctionLibrary, 3522639993);
	template<> MAGICLEAPTABLET_API UClass* StaticClass<UMagicLeapTabletFunctionLibrary>()
	{
		return UMagicLeapTabletFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapTabletFunctionLibrary(Z_Construct_UClass_UMagicLeapTabletFunctionLibrary, &UMagicLeapTabletFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapTablet"), TEXT("UMagicLeapTabletFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapTabletFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
