// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapTabletTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapTabletTypes() {}
// Cross Module References
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapTablet();
	MAGICLEAPTABLET_API UEnum* Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature();
	MAGICLEAPTABLET_API UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature();
	MAGICLEAPTABLET_API UEnum* Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceToolType();
	MAGICLEAPTABLET_API UEnum* Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceType();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimespan();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegateMulti_Parms
		{
			uint8 DeviceId;
			EMagicLeapInputTabletDeviceButton Button;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Button_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Button_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Button;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegateMulti_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button = { "Button", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegateMulti_Parms, Button), Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::NewProp_Button,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegate_Parms
		{
			uint8 DeviceId;
			EMagicLeapInputTabletDeviceButton Button;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Button_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Button_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Button;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegate_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button = { "Button", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegate_Parms, Button), Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::NewProp_Button,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to signal a button release from a connected tablet. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Delegate used to signal a button release from a connected tablet." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnButtonUpDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonUpDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonUpDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegateMulti_Parms
		{
			uint8 DeviceId;
			EMagicLeapInputTabletDeviceButton Button;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Button_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Button_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Button;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegateMulti_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button = { "Button", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegateMulti_Parms, Button), Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::NewProp_Button,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegate_Parms
		{
			uint8 DeviceId;
			EMagicLeapInputTabletDeviceButton Button;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Button_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Button_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Button;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegate_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button = { "Button", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegate_Parms, Button), Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::NewProp_Button,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to signal a button press from a connected tablet. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Delegate used to signal a button press from a connected tablet." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnButtonDownDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnButtonDownDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnButtonDownDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegateMulti_Parms
		{
			uint8 DeviceId;
			int32 TouchValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TouchValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TouchValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegateMulti_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_TouchValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_TouchValue = { "TouchValue", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegateMulti_Parms, TouchValue), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_TouchValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_TouchValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::NewProp_TouchValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegate_Parms
		{
			uint8 DeviceId;
			int32 TouchValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TouchValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TouchValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegate_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_TouchValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_TouchValue = { "TouchValue", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegate_Parms, TouchValue), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_TouchValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_TouchValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::NewProp_TouchValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to signal the ring touch state of a connected tablet. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Delegate used to signal the ring touch state of a connected tablet." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnRingTouchDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnRingTouchDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnRingTouchDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegateMulti_Parms
		{
			uint8 DeviceId;
			FMagicLeapInputTabletDeviceState DeviceState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeviceState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegateMulti_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceState = { "DeviceState", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegateMulti_Parms, DeviceState), Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceState_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::NewProp_DeviceState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegate_Parms
		{
			uint8 DeviceId;
			FMagicLeapInputTabletDeviceState DeviceState;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeviceState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegate_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceState = { "DeviceState", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegate_Parms, DeviceState), Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceState_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::NewProp_DeviceState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to signal the pen touch state of a connected tablet. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Delegate used to signal the pen touch state of a connected tablet." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnPenTouchDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnPenTouchDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnPenTouchDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegateMulti_Parms
		{
			uint8 DeviceId;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegateMulti_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegate_Parms
		{
			uint8 DeviceId;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegate_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::NewProp_DeviceId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to signal when a tablet device has disconnected from the application. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Delegate used to signal when a tablet device has disconnected from the application." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnDisconnectedDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnDisconnectedDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnDisconnectedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegateMulti_Parms
		{
			uint8 DeviceId;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegateMulti_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::NewProp_DeviceId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegate_Parms
		{
			uint8 DeviceId;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegate_Parms, DeviceId), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::NewProp_DeviceId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to signal when a tablet device has connected to the application. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Delegate used to signal when a tablet device has connected to the application." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet, nullptr, "MagicLeapTabletOnConnectedDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapTablet_eventMagicLeapTabletOnConnectedDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapTablet_MagicLeapTabletOnConnectedDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMagicLeapInputTabletDeviceButton_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton, Z_Construct_UPackage__Script_MagicLeapTablet(), TEXT("EMagicLeapInputTabletDeviceButton"));
		}
		return Singleton;
	}
	template<> MAGICLEAPTABLET_API UEnum* StaticEnum<EMagicLeapInputTabletDeviceButton>()
	{
		return EMagicLeapInputTabletDeviceButton_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapInputTabletDeviceButton(EMagicLeapInputTabletDeviceButton_StaticEnum, TEXT("/Script/MagicLeapTablet"), TEXT("EMagicLeapInputTabletDeviceButton"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton_Hash() { return 2199445121U; }
	UEnum* Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapTablet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapInputTabletDeviceButton"), 0, Get_Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceButton_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapInputTabletDeviceButton::Unknown", (int64)EMagicLeapInputTabletDeviceButton::Unknown },
				{ "EMagicLeapInputTabletDeviceButton::Button1", (int64)EMagicLeapInputTabletDeviceButton::Button1 },
				{ "EMagicLeapInputTabletDeviceButton::Button2", (int64)EMagicLeapInputTabletDeviceButton::Button2 },
				{ "EMagicLeapInputTabletDeviceButton::Button3", (int64)EMagicLeapInputTabletDeviceButton::Button3 },
				{ "EMagicLeapInputTabletDeviceButton::Button4", (int64)EMagicLeapInputTabletDeviceButton::Button4 },
				{ "EMagicLeapInputTabletDeviceButton::Button5", (int64)EMagicLeapInputTabletDeviceButton::Button5 },
				{ "EMagicLeapInputTabletDeviceButton::Button6", (int64)EMagicLeapInputTabletDeviceButton::Button6 },
				{ "EMagicLeapInputTabletDeviceButton::Button7", (int64)EMagicLeapInputTabletDeviceButton::Button7 },
				{ "EMagicLeapInputTabletDeviceButton::Button8", (int64)EMagicLeapInputTabletDeviceButton::Button8 },
				{ "EMagicLeapInputTabletDeviceButton::Button9", (int64)EMagicLeapInputTabletDeviceButton::Button9 },
				{ "EMagicLeapInputTabletDeviceButton::Button10", (int64)EMagicLeapInputTabletDeviceButton::Button10 },
				{ "EMagicLeapInputTabletDeviceButton::Button11", (int64)EMagicLeapInputTabletDeviceButton::Button11 },
				{ "EMagicLeapInputTabletDeviceButton::Button12", (int64)EMagicLeapInputTabletDeviceButton::Button12 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Button1.Name", "EMagicLeapInputTabletDeviceButton::Button1" },
				{ "Button10.Name", "EMagicLeapInputTabletDeviceButton::Button10" },
				{ "Button11.Name", "EMagicLeapInputTabletDeviceButton::Button11" },
				{ "Button12.Name", "EMagicLeapInputTabletDeviceButton::Button12" },
				{ "Button2.Name", "EMagicLeapInputTabletDeviceButton::Button2" },
				{ "Button3.Name", "EMagicLeapInputTabletDeviceButton::Button3" },
				{ "Button4.Name", "EMagicLeapInputTabletDeviceButton::Button4" },
				{ "Button5.Name", "EMagicLeapInputTabletDeviceButton::Button5" },
				{ "Button6.Name", "EMagicLeapInputTabletDeviceButton::Button6" },
				{ "Button7.Name", "EMagicLeapInputTabletDeviceButton::Button7" },
				{ "Button8.Name", "EMagicLeapInputTabletDeviceButton::Button8" },
				{ "Button9.Name", "EMagicLeapInputTabletDeviceButton::Button9" },
				{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
				{ "Unknown.Name", "EMagicLeapInputTabletDeviceButton::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet,
				nullptr,
				"EMagicLeapInputTabletDeviceButton",
				"EMagicLeapInputTabletDeviceButton",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapInputTabletDeviceToolType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceToolType, Z_Construct_UPackage__Script_MagicLeapTablet(), TEXT("EMagicLeapInputTabletDeviceToolType"));
		}
		return Singleton;
	}
	template<> MAGICLEAPTABLET_API UEnum* StaticEnum<EMagicLeapInputTabletDeviceToolType>()
	{
		return EMagicLeapInputTabletDeviceToolType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapInputTabletDeviceToolType(EMagicLeapInputTabletDeviceToolType_StaticEnum, TEXT("/Script/MagicLeapTablet"), TEXT("EMagicLeapInputTabletDeviceToolType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceToolType_Hash() { return 514591891U; }
	UEnum* Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceToolType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapTablet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapInputTabletDeviceToolType"), 0, Get_Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceToolType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapInputTabletDeviceToolType::Unknown", (int64)EMagicLeapInputTabletDeviceToolType::Unknown },
				{ "EMagicLeapInputTabletDeviceToolType::Pen", (int64)EMagicLeapInputTabletDeviceToolType::Pen },
				{ "EMagicLeapInputTabletDeviceToolType::Eraser", (int64)EMagicLeapInputTabletDeviceToolType::Eraser },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Eraser.Name", "EMagicLeapInputTabletDeviceToolType::Eraser" },
				{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
				{ "Pen.Name", "EMagicLeapInputTabletDeviceToolType::Pen" },
				{ "Unknown.Name", "EMagicLeapInputTabletDeviceToolType::Unknown" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet,
				nullptr,
				"EMagicLeapInputTabletDeviceToolType",
				"EMagicLeapInputTabletDeviceToolType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMagicLeapInputTabletDeviceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceType, Z_Construct_UPackage__Script_MagicLeapTablet(), TEXT("EMagicLeapInputTabletDeviceType"));
		}
		return Singleton;
	}
	template<> MAGICLEAPTABLET_API UEnum* StaticEnum<EMagicLeapInputTabletDeviceType>()
	{
		return EMagicLeapInputTabletDeviceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapInputTabletDeviceType(EMagicLeapInputTabletDeviceType_StaticEnum, TEXT("/Script/MagicLeapTablet"), TEXT("EMagicLeapInputTabletDeviceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceType_Hash() { return 191658201U; }
	UEnum* Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapTablet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapInputTabletDeviceType"), 0, Get_Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapInputTabletDeviceType::Unknown", (int64)EMagicLeapInputTabletDeviceType::Unknown },
				{ "EMagicLeapInputTabletDeviceType::Wacom", (int64)EMagicLeapInputTabletDeviceType::Wacom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
				{ "Unknown.Name", "EMagicLeapInputTabletDeviceType::Unknown" },
				{ "Wacom.Name", "EMagicLeapInputTabletDeviceType::Wacom" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapTablet,
				nullptr,
				"EMagicLeapInputTabletDeviceType",
				"EMagicLeapInputTabletDeviceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMagicLeapInputTabletDeviceState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPTABLET_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState, Z_Construct_UPackage__Script_MagicLeapTablet(), TEXT("MagicLeapInputTabletDeviceState"), sizeof(FMagicLeapInputTabletDeviceState), Get_Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPTABLET_API UScriptStruct* StaticStruct<FMagicLeapInputTabletDeviceState>()
{
	return FMagicLeapInputTabletDeviceState::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapInputTabletDeviceState(FMagicLeapInputTabletDeviceState::StaticStruct, TEXT("/Script/MagicLeapTablet"), TEXT("MagicLeapInputTabletDeviceState"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapTablet_StaticRegisterNativesFMagicLeapInputTabletDeviceState
{
	FScriptStruct_MagicLeapTablet_StaticRegisterNativesFMagicLeapInputTabletDeviceState()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapInputTabletDeviceState>(FName(TEXT("MagicLeapInputTabletDeviceState")));
	}
} ScriptStruct_MagicLeapTablet_StaticRegisterNativesFMagicLeapInputTabletDeviceState;
	struct Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ToolType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ToolType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ToolType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PenTouchPosAndForce_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PenTouchPosAndForce;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AdditionalPenTouchData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalPenTouchData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionalPenTouchData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPenTouchActive_MetaData[];
#endif
		static void NewProp_bIsPenTouchActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPenTouchActive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsConnected_MetaData[];
#endif
		static void NewProp_bIsConnected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsConnected;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PenDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PenDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Timestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timestamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValidFieldsFlag_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ValidFieldsFlag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapInputTabletDeviceState>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/** Type of this tablet device. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Type of this tablet device." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, Type), Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/** Type of tool used with the tablet. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Type of tool used with the tablet." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType = { "ToolType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, ToolType), Z_Construct_UEnum_MagicLeapTablet_EMagicLeapInputTabletDeviceToolType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenTouchPosAndForce_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/**\n\x09  The current touch position (x,y) and force (z).\n\x09  Position is in the [-1.0,1.0] range and force is in the [0.0,1.0] range.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "The current touch position (x,y) and force (z).\nPosition is in the [-1.0,1.0] range and force is in the [0.0,1.0] range." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenTouchPosAndForce = { "PenTouchPosAndForce", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, PenTouchPosAndForce), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenTouchPosAndForce_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenTouchPosAndForce_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData_Inner = { "AdditionalPenTouchData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/**\n\x09  The Additional coordinate values (x, y, z).\n\x09  It could contain data specific to the device type.\n\x09  For example, it could hold tilt values while using pen.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "The Additional coordinate values (x, y, z).\nIt could contain data specific to the device type.\nFor example, it could hold tilt values while using pen." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData = { "AdditionalPenTouchData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, AdditionalPenTouchData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/** Is touch active. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Is touch active." },
	};
#endif
	void Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive_SetBit(void* Obj)
	{
		((FMagicLeapInputTabletDeviceState*)Obj)->bIsPenTouchActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive = { "bIsPenTouchActive", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMagicLeapInputTabletDeviceState), &Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/** If this tablet is connected. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "If this tablet is connected." },
	};
#endif
	void Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected_SetBit(void* Obj)
	{
		((FMagicLeapInputTabletDeviceState*)Obj)->bIsConnected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected = { "bIsConnected", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMagicLeapInputTabletDeviceState), &Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenDistance_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/** Distance between pen and tablet. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Distance between pen and tablet." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenDistance = { "PenDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, PenDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Timestamp_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/** Time since device bootup. */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Time since device bootup." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Timestamp = { "Timestamp", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, Timestamp), Z_Construct_UScriptStruct_FTimespan, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Timestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Timestamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ValidFieldsFlag_MetaData[] = {
		{ "Category", "Tablet | MagicLeap" },
		{ "Comment", "/**\n\x09  Flags to denote which of the above fields are valid.\n\x09  #EMagicLeapInputTabletDeviceStateMask defines the bitmap.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapTabletTypes.h" },
		{ "ToolTip", "Flags to denote which of the above fields are valid.\n#EMagicLeapInputTabletDeviceStateMask defines the bitmap." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ValidFieldsFlag = { "ValidFieldsFlag", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapInputTabletDeviceState, ValidFieldsFlag), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ValidFieldsFlag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ValidFieldsFlag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ToolType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenTouchPosAndForce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_AdditionalPenTouchData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsPenTouchActive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_bIsConnected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_PenDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_Timestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::NewProp_ValidFieldsFlag,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapTablet,
		nullptr,
		&NewStructOps,
		"MagicLeapInputTabletDeviceState",
		sizeof(FMagicLeapInputTabletDeviceState),
		alignof(FMagicLeapInputTabletDeviceState),
		Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapTablet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapInputTabletDeviceState"), sizeof(FMagicLeapInputTabletDeviceState), Get_Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapInputTabletDeviceState_Hash() { return 3002141166U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
