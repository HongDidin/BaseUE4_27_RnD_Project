// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPMEDIAEDITOR_MagicLeapFileMediaSourceFactory_generated_h
#error "MagicLeapFileMediaSourceFactory.generated.h already included, missing '#pragma once' in MagicLeapFileMediaSourceFactory.h"
#endif
#define MAGICLEAPMEDIAEDITOR_MagicLeapFileMediaSourceFactory_generated_h

#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UMagicLeapFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapFileMediaSourceFactory)


#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapFileMediaSourceFactory(); \
	friend struct Z_Construct_UClass_UMagicLeapFileMediaSourceFactory_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapFileMediaSourceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapMediaEditor"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapFileMediaSourceFactory)


#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapFileMediaSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapFileMediaSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapFileMediaSourceFactory(UMagicLeapFileMediaSourceFactory&&); \
	NO_API UMagicLeapFileMediaSourceFactory(const UMagicLeapFileMediaSourceFactory&); \
public:


#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapFileMediaSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapFileMediaSourceFactory(UMagicLeapFileMediaSourceFactory&&); \
	NO_API UMagicLeapFileMediaSourceFactory(const UMagicLeapFileMediaSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapFileMediaSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapFileMediaSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapFileMediaSourceFactory)


#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MagicLeapFileMediaSourceFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPMEDIAEDITOR_API UClass* StaticClass<class UMagicLeapFileMediaSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapMedia_Source_MagicLeapMediaEditor_Private_MagicLeapFileMediaSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
