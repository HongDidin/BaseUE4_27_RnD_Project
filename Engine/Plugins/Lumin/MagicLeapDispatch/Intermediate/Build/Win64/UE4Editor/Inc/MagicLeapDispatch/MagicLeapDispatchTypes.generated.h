// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPDISPATCH_MagicLeapDispatchTypes_generated_h
#error "MagicLeapDispatchTypes.generated.h already included, missing '#pragma once' in MagicLeapDispatchTypes.h"
#endif
#define MAGICLEAPDISPATCH_MagicLeapDispatchTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapDispatch_Source_Public_MagicLeapDispatchTypes_h_27_DELEGATE \
struct _Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandlerMulti_Parms \
{ \
	FString Response; \
}; \
static inline void FMagicLeapOAuthSchemaHandlerMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapOAuthSchemaHandlerMulti, const FString& Response) \
{ \
	_Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandlerMulti_Parms Parms; \
	Parms.Response=Response; \
	MagicLeapOAuthSchemaHandlerMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapDispatch_Source_Public_MagicLeapDispatchTypes_h_26_DELEGATE \
struct _Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandler_Parms \
{ \
	FString Response; \
}; \
static inline void FMagicLeapOAuthSchemaHandler_DelegateWrapper(const FScriptDelegate& MagicLeapOAuthSchemaHandler, const FString& Response) \
{ \
	_Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandler_Parms Parms; \
	Parms.Response=Response; \
	MagicLeapOAuthSchemaHandler.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapDispatch_Source_Public_MagicLeapDispatchTypes_h


#define FOREACH_ENUM_EMAGICLEAPDISPATCHRESULT(op) \
	op(EMagicLeapDispatchResult::Ok) \
	op(EMagicLeapDispatchResult::CannotStartApp) \
	op(EMagicLeapDispatchResult::InvalidPacket) \
	op(EMagicLeapDispatchResult::NoAppFound) \
	op(EMagicLeapDispatchResult::AppPickerDialogFailure) \
	op(EMagicLeapDispatchResult::AllocFailed) \
	op(EMagicLeapDispatchResult::InvalidParam) \
	op(EMagicLeapDispatchResult::UnspecifiedFailure) \
	op(EMagicLeapDispatchResult::NotImplemented) 

enum class EMagicLeapDispatchResult : uint8;
template<> MAGICLEAPDISPATCH_API UEnum* StaticEnum<EMagicLeapDispatchResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
