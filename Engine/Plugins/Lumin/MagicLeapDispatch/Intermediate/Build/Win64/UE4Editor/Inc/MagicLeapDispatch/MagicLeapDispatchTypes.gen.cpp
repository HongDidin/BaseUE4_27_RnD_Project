// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapDispatchTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapDispatchTypes() {}
// Cross Module References
	MAGICLEAPDISPATCH_API UFunction* Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapDispatch();
	MAGICLEAPDISPATCH_API UFunction* Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature();
	MAGICLEAPDISPATCH_API UEnum* Z_Construct_UEnum_MagicLeapDispatch_EMagicLeapDispatchResult();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandlerMulti_Parms
		{
			FString Response;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Response_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Response;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::NewProp_Response_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::NewProp_Response = { "Response", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandlerMulti_Parms, Response), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::NewProp_Response_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::NewProp_Response_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::NewProp_Response,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapDispatchTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapDispatch, nullptr, "MagicLeapOAuthSchemaHandlerMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandlerMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandlerMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics
	{
		struct _Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandler_Parms
		{
			FString Response;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Response_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Response;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::NewProp_Response_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::NewProp_Response = { "Response", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandler_Parms, Response), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::NewProp_Response_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::NewProp_Response_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::NewProp_Response,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapDispatchTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapDispatch, nullptr, "MagicLeapOAuthSchemaHandler__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapDispatch_eventMagicLeapOAuthSchemaHandler_Parms), Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapDispatch_MagicLeapOAuthSchemaHandler__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMagicLeapDispatchResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapDispatch_EMagicLeapDispatchResult, Z_Construct_UPackage__Script_MagicLeapDispatch(), TEXT("EMagicLeapDispatchResult"));
		}
		return Singleton;
	}
	template<> MAGICLEAPDISPATCH_API UEnum* StaticEnum<EMagicLeapDispatchResult>()
	{
		return EMagicLeapDispatchResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapDispatchResult(EMagicLeapDispatchResult_StaticEnum, TEXT("/Script/MagicLeapDispatch"), TEXT("EMagicLeapDispatchResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapDispatch_EMagicLeapDispatchResult_Hash() { return 4292987481U; }
	UEnum* Z_Construct_UEnum_MagicLeapDispatch_EMagicLeapDispatchResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapDispatch();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapDispatchResult"), 0, Get_Z_Construct_UEnum_MagicLeapDispatch_EMagicLeapDispatchResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapDispatchResult::Ok", (int64)EMagicLeapDispatchResult::Ok },
				{ "EMagicLeapDispatchResult::CannotStartApp", (int64)EMagicLeapDispatchResult::CannotStartApp },
				{ "EMagicLeapDispatchResult::InvalidPacket", (int64)EMagicLeapDispatchResult::InvalidPacket },
				{ "EMagicLeapDispatchResult::NoAppFound", (int64)EMagicLeapDispatchResult::NoAppFound },
				{ "EMagicLeapDispatchResult::AppPickerDialogFailure", (int64)EMagicLeapDispatchResult::AppPickerDialogFailure },
				{ "EMagicLeapDispatchResult::AllocFailed", (int64)EMagicLeapDispatchResult::AllocFailed },
				{ "EMagicLeapDispatchResult::InvalidParam", (int64)EMagicLeapDispatchResult::InvalidParam },
				{ "EMagicLeapDispatchResult::UnspecifiedFailure", (int64)EMagicLeapDispatchResult::UnspecifiedFailure },
				{ "EMagicLeapDispatchResult::NotImplemented", (int64)EMagicLeapDispatchResult::NotImplemented },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AllocFailed.Name", "EMagicLeapDispatchResult::AllocFailed" },
				{ "AppPickerDialogFailure.Name", "EMagicLeapDispatchResult::AppPickerDialogFailure" },
				{ "BlueprintType", "true" },
				{ "CannotStartApp.Name", "EMagicLeapDispatchResult::CannotStartApp" },
				{ "InvalidPacket.Name", "EMagicLeapDispatchResult::InvalidPacket" },
				{ "InvalidParam.Name", "EMagicLeapDispatchResult::InvalidParam" },
				{ "ModuleRelativePath", "Public/MagicLeapDispatchTypes.h" },
				{ "NoAppFound.Name", "EMagicLeapDispatchResult::NoAppFound" },
				{ "NotImplemented.Name", "EMagicLeapDispatchResult::NotImplemented" },
				{ "Ok.Name", "EMagicLeapDispatchResult::Ok" },
				{ "UnspecifiedFailure.Name", "EMagicLeapDispatchResult::UnspecifiedFailure" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapDispatch,
				nullptr,
				"EMagicLeapDispatchResult",
				"EMagicLeapDispatchResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
