// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPNETWORKING_MagicLeapNetworkingFunctionLibrary_generated_h
#error "MagicLeapNetworkingFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapNetworkingFunctionLibrary.h"
#endif
#define MAGICLEAPNETWORKING_MagicLeapNetworkingFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWiFiDataAsync); \
	DECLARE_FUNCTION(execIsInternetConnectedAsync);


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWiFiDataAsync); \
	DECLARE_FUNCTION(execIsInternetConnectedAsync);


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapNetworkingFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapNetworkingFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapNetworking"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapNetworkingFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapNetworkingFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapNetworkingFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapNetworking"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapNetworkingFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapNetworkingFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapNetworkingFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapNetworkingFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapNetworkingFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapNetworkingFunctionLibrary(UMagicLeapNetworkingFunctionLibrary&&); \
	NO_API UMagicLeapNetworkingFunctionLibrary(const UMagicLeapNetworkingFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapNetworkingFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapNetworkingFunctionLibrary(UMagicLeapNetworkingFunctionLibrary&&); \
	NO_API UMagicLeapNetworkingFunctionLibrary(const UMagicLeapNetworkingFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapNetworkingFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapNetworkingFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapNetworkingFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_10_PROLOG
#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_INCLASS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPNETWORKING_API UClass* StaticClass<class UMagicLeapNetworkingFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
