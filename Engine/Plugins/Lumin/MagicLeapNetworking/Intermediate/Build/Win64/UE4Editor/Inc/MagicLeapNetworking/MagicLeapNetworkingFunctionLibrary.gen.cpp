// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapNetworkingFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapNetworkingFunctionLibrary() {}
// Cross Module References
	MAGICLEAPNETWORKING_API UClass* Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_NoRegister();
	MAGICLEAPNETWORKING_API UClass* Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapNetworking();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapNetworkingFunctionLibrary::execGetWiFiDataAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapNetworkingFunctionLibrary::GetWiFiDataAsync(FMagicLeapWifiStatusDelegate(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapNetworkingFunctionLibrary::execIsInternetConnectedAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapNetworkingFunctionLibrary::IsInternetConnectedAsync(FMagicLeapInternetConnectionStatusDelegate(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	void UMagicLeapNetworkingFunctionLibrary::StaticRegisterNativesUMagicLeapNetworkingFunctionLibrary()
	{
		UClass* Class = UMagicLeapNetworkingFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWiFiDataAsync", &UMagicLeapNetworkingFunctionLibrary::execGetWiFiDataAsync },
			{ "IsInternetConnectedAsync", &UMagicLeapNetworkingFunctionLibrary::execIsInternetConnectedAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics
	{
		struct MagicLeapNetworkingFunctionLibrary_eventGetWiFiDataAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapNetworkingFunctionLibrary_eventGetWiFiDataAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapNetworkingFunctionLibrary_eventGetWiFiDataAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapNetworkingFunctionLibrary_eventGetWiFiDataAsync_Parms), &Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Networking Function Library | MagicLeap" },
		{ "Comment", "/** \n\x09\x09""Asynchronously queries the device's WiFi related data.\n\x09\x09@param ResultDelegate The delegate that will convey the wifi data.\n\x09\x09@return True if the async task is successfully created, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingFunctionLibrary.h" },
		{ "ToolTip", "Asynchronously queries the device's WiFi related data.\n@param ResultDelegate The delegate that will convey the wifi data.\n@return True if the async task is successfully created, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary, nullptr, "GetWiFiDataAsync", nullptr, nullptr, sizeof(MagicLeapNetworkingFunctionLibrary_eventGetWiFiDataAsync_Parms), Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics
	{
		struct MagicLeapNetworkingFunctionLibrary_eventIsInternetConnectedAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapNetworkingFunctionLibrary_eventIsInternetConnectedAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapNetworkingFunctionLibrary_eventIsInternetConnectedAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapNetworkingFunctionLibrary_eventIsInternetConnectedAsync_Parms), &Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Networking Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Asynchronously queries whether or not the internet is connected.  If the delegate broadcasts that the connection is active,\n\x09\x09it means link layer is up, internet is accessible, and DNS is good.\n\x09\x09@param ResultDelegate The delegate that will convey the result of the connection query.\n\x09\x09@return True if the async task is successfully created, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingFunctionLibrary.h" },
		{ "ToolTip", "Asynchronously queries whether or not the internet is connected.  If the delegate broadcasts that the connection is active,\nit means link layer is up, internet is accessible, and DNS is good.\n@param ResultDelegate The delegate that will convey the result of the connection query.\n@return True if the async task is successfully created, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary, nullptr, "IsInternetConnectedAsync", nullptr, nullptr, sizeof(MagicLeapNetworkingFunctionLibrary_eventIsInternetConnectedAsync_Parms), Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_NoRegister()
	{
		return UMagicLeapNetworkingFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapNetworking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_GetWiFiDataAsync, "GetWiFiDataAsync" }, // 150635980
		{ &Z_Construct_UFunction_UMagicLeapNetworkingFunctionLibrary_IsInternetConnectedAsync, "IsInternetConnectedAsync" }, // 3804339835
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "IncludePath", "MagicLeapNetworkingFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapNetworkingFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapNetworkingFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapNetworkingFunctionLibrary, 485548117);
	template<> MAGICLEAPNETWORKING_API UClass* StaticClass<UMagicLeapNetworkingFunctionLibrary>()
	{
		return UMagicLeapNetworkingFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapNetworkingFunctionLibrary(Z_Construct_UClass_UMagicLeapNetworkingFunctionLibrary, &UMagicLeapNetworkingFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapNetworking"), TEXT("UMagicLeapNetworkingFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapNetworkingFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
