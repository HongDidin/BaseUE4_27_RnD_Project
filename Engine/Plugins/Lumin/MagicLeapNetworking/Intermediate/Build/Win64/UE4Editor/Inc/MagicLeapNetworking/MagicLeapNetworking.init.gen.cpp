// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapNetworking_init() {}
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapNetworking()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapNetworking",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xCB13E7CD,
				0x27D26B55,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
