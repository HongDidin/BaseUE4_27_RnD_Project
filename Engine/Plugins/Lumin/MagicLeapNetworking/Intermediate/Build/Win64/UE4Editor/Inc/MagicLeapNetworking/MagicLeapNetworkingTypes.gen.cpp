// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapNetworkingTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapNetworkingTypes() {}
// Cross Module References
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapNetworking();
	MAGICLEAPNETWORKING_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapNetworking_eventWifiStatusDelegateMulti_Parms
		{
			FMagicLeapNetworkingWiFiData WiFiData;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WiFiData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::NewProp_WiFiData = { "WiFiData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapNetworking_eventWifiStatusDelegateMulti_Parms, WiFiData), Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::NewProp_WiFiData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapNetworking, nullptr, "WifiStatusDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapNetworking_eventWifiStatusDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapNetworking_eventMagicLeapWifiStatusDelegate_Parms
		{
			FMagicLeapNetworkingWiFiData WiFiData;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WiFiData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::NewProp_WiFiData = { "WiFiData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapNetworking_eventMagicLeapWifiStatusDelegate_Parms, WiFiData), Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::NewProp_WiFiData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to convey the result of wifi data query. */" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of wifi data query." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapNetworking, nullptr, "MagicLeapWifiStatusDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapNetworking_eventMagicLeapWifiStatusDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapWifiStatusDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapNetworking_eventInternetConnectionStatusDelegateMulti_Parms
		{
			bool bIsConnected;
		};
		static void NewProp_bIsConnected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsConnected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::NewProp_bIsConnected_SetBit(void* Obj)
	{
		((_Script_MagicLeapNetworking_eventInternetConnectionStatusDelegateMulti_Parms*)Obj)->bIsConnected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::NewProp_bIsConnected = { "bIsConnected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapNetworking_eventInternetConnectionStatusDelegateMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::NewProp_bIsConnected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::NewProp_bIsConnected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapNetworking, nullptr, "InternetConnectionStatusDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapNetworking_eventInternetConnectionStatusDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapNetworking_eventMagicLeapInternetConnectionStatusDelegate_Parms
		{
			bool bIsConnected;
		};
		static void NewProp_bIsConnected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsConnected;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::NewProp_bIsConnected_SetBit(void* Obj)
	{
		((_Script_MagicLeapNetworking_eventMagicLeapInternetConnectionStatusDelegate_Parms*)Obj)->bIsConnected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::NewProp_bIsConnected = { "bIsConnected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapNetworking_eventMagicLeapInternetConnectionStatusDelegate_Parms), &Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::NewProp_bIsConnected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::NewProp_bIsConnected,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to convey the result of an internet connection query. */" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of an internet connection query." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapNetworking, nullptr, "MagicLeapInternetConnectionStatusDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapNetworking_eventMagicLeapInternetConnectionStatusDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapNetworking_MagicLeapInternetConnectionStatusDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FMagicLeapNetworkingWiFiData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPNETWORKING_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData, Z_Construct_UPackage__Script_MagicLeapNetworking(), TEXT("MagicLeapNetworkingWiFiData"), sizeof(FMagicLeapNetworkingWiFiData), Get_Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPNETWORKING_API UScriptStruct* StaticStruct<FMagicLeapNetworkingWiFiData>()
{
	return FMagicLeapNetworkingWiFiData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapNetworkingWiFiData(FMagicLeapNetworkingWiFiData::StaticStruct, TEXT("/Script/MagicLeapNetworking"), TEXT("MagicLeapNetworkingWiFiData"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapNetworking_StaticRegisterNativesFMagicLeapNetworkingWiFiData
{
	FScriptStruct_MagicLeapNetworking_StaticRegisterNativesFMagicLeapNetworkingWiFiData()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapNetworkingWiFiData>(FName(TEXT("MagicLeapNetworkingWiFiData")));
	}
} ScriptStruct_MagicLeapNetworking_StaticRegisterNativesFMagicLeapNetworkingWiFiData;
	struct Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RSSI_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RSSI;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Linkspeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Linkspeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** WiFi related data. */" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
		{ "ToolTip", "WiFi related data." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapNetworkingWiFiData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_RSSI_MetaData[] = {
		{ "Category", "Networking|MagicLeap" },
		{ "Comment", "/** WiFi RSSI in dbM. */" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
		{ "ToolTip", "WiFi RSSI in dbM." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_RSSI = { "RSSI", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapNetworkingWiFiData, RSSI), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_RSSI_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_RSSI_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Linkspeed_MetaData[] = {
		{ "Category", "Networking|MagicLeap" },
		{ "Comment", "/** WiFi link speed in Mb/s. */" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
		{ "ToolTip", "WiFi link speed in Mb/s." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Linkspeed = { "Linkspeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapNetworkingWiFiData, Linkspeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Linkspeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Linkspeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "Networking|MagicLeap" },
		{ "Comment", "/** WiFi frequency in MHz. */" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingTypes.h" },
		{ "ToolTip", "WiFi frequency in MHz." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapNetworkingWiFiData, Frequency), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Frequency_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_RSSI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Linkspeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::NewProp_Frequency,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapNetworking,
		nullptr,
		&NewStructOps,
		"MagicLeapNetworkingWiFiData",
		sizeof(FMagicLeapNetworkingWiFiData),
		alignof(FMagicLeapNetworkingWiFiData),
		Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapNetworking();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapNetworkingWiFiData"), sizeof(FMagicLeapNetworkingWiFiData), Get_Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Hash() { return 4177228847U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
