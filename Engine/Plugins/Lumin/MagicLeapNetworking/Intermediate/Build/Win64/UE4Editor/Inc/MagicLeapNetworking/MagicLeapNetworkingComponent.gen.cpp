// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapNetworkingComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapNetworkingComponent() {}
// Cross Module References
	MAGICLEAPNETWORKING_API UClass* Z_Construct_UClass_UMagicLeapNetworkingComponent_NoRegister();
	MAGICLEAPNETWORKING_API UClass* Z_Construct_UClass_UMagicLeapNetworkingComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapNetworking();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature();
	MAGICLEAPNETWORKING_API UFunction* Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapNetworkingComponent::execGetWiFiDataAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetWiFiDataAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapNetworkingComponent::execIsInternetConnectedAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsInternetConnectedAsync();
		P_NATIVE_END;
	}
	void UMagicLeapNetworkingComponent::StaticRegisterNativesUMagicLeapNetworkingComponent()
	{
		UClass* Class = UMagicLeapNetworkingComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWiFiDataAsync", &UMagicLeapNetworkingComponent::execGetWiFiDataAsync },
			{ "IsInternetConnectedAsync", &UMagicLeapNetworkingComponent::execIsInternetConnectedAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics
	{
		struct MagicLeapNetworkingComponent_eventGetWiFiDataAsync_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapNetworkingComponent_eventGetWiFiDataAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapNetworkingComponent_eventGetWiFiDataAsync_Parms), &Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Networking | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Asynchronously queries the device's WiFi related data.\n\x09\x09@param ResultDelegate The delegate that will convey the wifi data.\n\x09\x09@return True if the async task is successfully created, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingComponent.h" },
		{ "ToolTip", "Asynchronously queries the device's WiFi related data.\n@param ResultDelegate The delegate that will convey the wifi data.\n@return True if the async task is successfully created, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapNetworkingComponent, nullptr, "GetWiFiDataAsync", nullptr, nullptr, sizeof(MagicLeapNetworkingComponent_eventGetWiFiDataAsync_Parms), Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics
	{
		struct MagicLeapNetworkingComponent_eventIsInternetConnectedAsync_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapNetworkingComponent_eventIsInternetConnectedAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapNetworkingComponent_eventIsInternetConnectedAsync_Parms), &Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Networking | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Asynchronously queries whether or not the internet is connected.  If the delegate broadcasts that the connection is active,\n\x09\x09it means link layer is up, internet is accessible, and DNS is good.\n\x09\x09@param ResultDelegate The delegate that will convey the result of the connection query.\n\x09\x09@return True if the async task is successfully created, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingComponent.h" },
		{ "ToolTip", "Asynchronously queries whether or not the internet is connected.  If the delegate broadcasts that the connection is active,\nit means link layer is up, internet is accessible, and DNS is good.\n@param ResultDelegate The delegate that will convey the result of the connection query.\n@return True if the async task is successfully created, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapNetworkingComponent, nullptr, "IsInternetConnectedAsync", nullptr, nullptr, sizeof(MagicLeapNetworkingComponent_eventIsInternetConnectedAsync_Parms), Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapNetworkingComponent_NoRegister()
	{
		return UMagicLeapNetworkingComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionQueryResultDeleage_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_ConnectionQueryResultDeleage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WifiDataQueryResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_WifiDataQueryResultDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapNetworking,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapNetworkingComponent_GetWiFiDataAsync, "GetWiFiDataAsync" }, // 2166594374
		{ &Z_Construct_UFunction_UMagicLeapNetworkingComponent_IsInternetConnectedAsync, "IsInternetConnectedAsync" }, // 1414575094
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09""Component that provides access to the Networking API functionality.\n*/" },
		{ "IncludePath", "MagicLeapNetworkingComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingComponent.h" },
		{ "ToolTip", "Component that provides access to the Networking API functionality." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_ConnectionQueryResultDeleage_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Networking | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_ConnectionQueryResultDeleage = { "ConnectionQueryResultDeleage", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapNetworkingComponent, ConnectionQueryResultDeleage), Z_Construct_UDelegateFunction_MagicLeapNetworking_InternetConnectionStatusDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_ConnectionQueryResultDeleage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_ConnectionQueryResultDeleage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_WifiDataQueryResultDelegate_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Networking | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapNetworkingComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_WifiDataQueryResultDelegate = { "WifiDataQueryResultDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapNetworkingComponent, WifiDataQueryResultDelegate), Z_Construct_UDelegateFunction_MagicLeapNetworking_WifiStatusDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_WifiDataQueryResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_WifiDataQueryResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_ConnectionQueryResultDeleage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::NewProp_WifiDataQueryResultDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapNetworkingComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::ClassParams = {
		&UMagicLeapNetworkingComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapNetworkingComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapNetworkingComponent, 1792943563);
	template<> MAGICLEAPNETWORKING_API UClass* StaticClass<UMagicLeapNetworkingComponent>()
	{
		return UMagicLeapNetworkingComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapNetworkingComponent(Z_Construct_UClass_UMagicLeapNetworkingComponent, &UMagicLeapNetworkingComponent::StaticClass, TEXT("/Script/MagicLeapNetworking"), TEXT("UMagicLeapNetworkingComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapNetworkingComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
