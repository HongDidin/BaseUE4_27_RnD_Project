// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMagicLeapNetworkingWiFiData;
#ifdef MAGICLEAPNETWORKING_MagicLeapNetworkingTypes_generated_h
#error "MagicLeapNetworkingTypes.generated.h already included, missing '#pragma once' in MagicLeapNetworkingTypes.h"
#endif
#define MAGICLEAPNETWORKING_MagicLeapNetworkingTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingTypes_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapNetworkingWiFiData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPNETWORKING_API UScriptStruct* StaticStruct<struct FMagicLeapNetworkingWiFiData>();

#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingTypes_h_33_DELEGATE \
struct _Script_MagicLeapNetworking_eventWifiStatusDelegateMulti_Parms \
{ \
	FMagicLeapNetworkingWiFiData WiFiData; \
}; \
static inline void FWifiStatusDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& WifiStatusDelegateMulti, FMagicLeapNetworkingWiFiData WiFiData) \
{ \
	_Script_MagicLeapNetworking_eventWifiStatusDelegateMulti_Parms Parms; \
	Parms.WiFiData=WiFiData; \
	WifiStatusDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingTypes_h_32_DELEGATE \
struct _Script_MagicLeapNetworking_eventMagicLeapWifiStatusDelegate_Parms \
{ \
	FMagicLeapNetworkingWiFiData WiFiData; \
}; \
static inline void FMagicLeapWifiStatusDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapWifiStatusDelegate, FMagicLeapNetworkingWiFiData WiFiData) \
{ \
	_Script_MagicLeapNetworking_eventMagicLeapWifiStatusDelegate_Parms Parms; \
	Parms.WiFiData=WiFiData; \
	MagicLeapWifiStatusDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingTypes_h_29_DELEGATE \
struct _Script_MagicLeapNetworking_eventInternetConnectionStatusDelegateMulti_Parms \
{ \
	bool bIsConnected; \
}; \
static inline void FInternetConnectionStatusDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& InternetConnectionStatusDelegateMulti, bool bIsConnected) \
{ \
	_Script_MagicLeapNetworking_eventInternetConnectionStatusDelegateMulti_Parms Parms; \
	Parms.bIsConnected=bIsConnected ? true : false; \
	InternetConnectionStatusDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingTypes_h_28_DELEGATE \
struct _Script_MagicLeapNetworking_eventMagicLeapInternetConnectionStatusDelegate_Parms \
{ \
	bool bIsConnected; \
}; \
static inline void FMagicLeapInternetConnectionStatusDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapInternetConnectionStatusDelegate, bool bIsConnected) \
{ \
	_Script_MagicLeapNetworking_eventMagicLeapInternetConnectionStatusDelegate_Parms Parms; \
	Parms.bIsConnected=bIsConnected ? true : false; \
	MagicLeapInternetConnectionStatusDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
