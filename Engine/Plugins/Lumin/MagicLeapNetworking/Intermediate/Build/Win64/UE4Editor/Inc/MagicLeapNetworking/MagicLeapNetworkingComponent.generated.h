// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPNETWORKING_MagicLeapNetworkingComponent_generated_h
#error "MagicLeapNetworkingComponent.generated.h already included, missing '#pragma once' in MagicLeapNetworkingComponent.h"
#endif
#define MAGICLEAPNETWORKING_MagicLeapNetworkingComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWiFiDataAsync); \
	DECLARE_FUNCTION(execIsInternetConnectedAsync);


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWiFiDataAsync); \
	DECLARE_FUNCTION(execIsInternetConnectedAsync);


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapNetworkingComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapNetworkingComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapNetworking"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapNetworkingComponent)


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapNetworkingComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapNetworkingComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapNetworkingComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapNetworking"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapNetworkingComponent)


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapNetworkingComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapNetworkingComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapNetworkingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapNetworkingComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapNetworkingComponent(UMagicLeapNetworkingComponent&&); \
	NO_API UMagicLeapNetworkingComponent(const UMagicLeapNetworkingComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapNetworkingComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapNetworkingComponent(UMagicLeapNetworkingComponent&&); \
	NO_API UMagicLeapNetworkingComponent(const UMagicLeapNetworkingComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapNetworkingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapNetworkingComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapNetworkingComponent)


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ConnectionQueryResultDeleage() { return STRUCT_OFFSET(UMagicLeapNetworkingComponent, ConnectionQueryResultDeleage); } \
	FORCEINLINE static uint32 __PPO__WifiDataQueryResultDelegate() { return STRUCT_OFFSET(UMagicLeapNetworkingComponent, WifiDataQueryResultDelegate); }


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPNETWORKING_API UClass* StaticClass<class UMagicLeapNetworkingComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapNetworking_Source_Public_MagicLeapNetworkingComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
