// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture2D;
#ifdef MAGICLEAPCAMERA_MagicLeapCameraTypes_generated_h
#error "MagicLeapCameraTypes.generated.h already included, missing '#pragma once' in MagicLeapCameraTypes.h"
#endif
#define MAGICLEAPCAMERA_MagicLeapCameraTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPCAMERA_API UScriptStruct* StaticStruct<struct FMagicLeapCameraOutput>();

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_10_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPCAMERA_API UScriptStruct* StaticStruct<struct FMagicLeapCameraPlaneInfo>();

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_91_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraLogMessageMulti_Parms \
{ \
	FString LogMessage; \
}; \
static inline void FMagicLeapCameraLogMessageMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCameraLogMessageMulti, const FString& LogMessage) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraLogMessageMulti_Parms Parms; \
	Parms.LogMessage=LogMessage; \
	MagicLeapCameraLogMessageMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_90_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraLogMessage_Parms \
{ \
	FString LogMessage; \
}; \
static inline void FMagicLeapCameraLogMessage_DelegateWrapper(const FScriptDelegate& MagicLeapCameraLogMessage, const FString& LogMessage) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraLogMessage_Parms Parms; \
	Parms.LogMessage=LogMessage; \
	MagicLeapCameraLogMessage.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_83_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms \
{ \
	bool bSuccess; \
	FString FilePath; \
}; \
static inline void FMagicLeapCameraStopRecordingMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCameraStopRecordingMulti, bool bSuccess, const FString& FilePath) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	Parms.FilePath=FilePath; \
	MagicLeapCameraStopRecordingMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_82_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms \
{ \
	bool bSuccess; \
	FString FilePath; \
}; \
static inline void FMagicLeapCameraStopRecording_DelegateWrapper(const FScriptDelegate& MagicLeapCameraStopRecording, bool bSuccess, const FString& FilePath) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	Parms.FilePath=FilePath; \
	MagicLeapCameraStopRecording.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_74_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraStartRecordingMulti_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCameraStartRecordingMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCameraStartRecordingMulti, bool bSuccess) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraStartRecordingMulti_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCameraStartRecordingMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_73_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraStartRecording_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCameraStartRecording_DelegateWrapper(const FScriptDelegate& MagicLeapCameraStartRecording, bool bSuccess) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraStartRecording_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCameraStartRecording.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_66_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms \
{ \
	bool bSuccess; \
	UTexture2D* CaptureTexture; \
}; \
static inline void FMagicLeapCameraCaptureImgToTextureMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCameraCaptureImgToTextureMulti, bool bSuccess, UTexture2D* CaptureTexture) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	Parms.CaptureTexture=CaptureTexture; \
	MagicLeapCameraCaptureImgToTextureMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_65_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms \
{ \
	bool bSuccess; \
	UTexture2D* CaptureTexture; \
}; \
static inline void FMagicLeapCameraCaptureImgToTexture_DelegateWrapper(const FScriptDelegate& MagicLeapCameraCaptureImgToTexture, bool bSuccess, UTexture2D* CaptureTexture) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	Parms.CaptureTexture=CaptureTexture; \
	MagicLeapCameraCaptureImgToTexture.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_57_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms \
{ \
	bool bSuccess; \
	FString FilePath; \
}; \
static inline void FMagicLeapCameraCaptureImgToFileMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCameraCaptureImgToFileMulti, bool bSuccess, const FString& FilePath) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	Parms.FilePath=FilePath; \
	MagicLeapCameraCaptureImgToFileMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_56_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms \
{ \
	bool bSuccess; \
	FString FilePath; \
}; \
static inline void FMagicLeapCameraCaptureImgToFile_DelegateWrapper(const FScriptDelegate& MagicLeapCameraCaptureImgToFile, bool bSuccess, const FString& FilePath) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	Parms.FilePath=FilePath; \
	MagicLeapCameraCaptureImgToFile.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_48_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraDisconnect_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCameraDisconnect_DelegateWrapper(const FScriptDelegate& MagicLeapCameraDisconnect, bool bSuccess) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraDisconnect_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCameraDisconnect.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h_45_DELEGATE \
struct _Script_MagicLeapCamera_eventMagicLeapCameraConnect_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCameraConnect_DelegateWrapper(const FScriptDelegate& MagicLeapCameraConnect, bool bSuccess) \
{ \
	_Script_MagicLeapCamera_eventMagicLeapCameraConnect_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCameraConnect.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
