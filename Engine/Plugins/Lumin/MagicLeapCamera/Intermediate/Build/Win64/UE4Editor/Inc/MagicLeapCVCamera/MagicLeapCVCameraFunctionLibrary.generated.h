// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMagicLeapCameraOutput;
struct FTransform;
struct FMagicLeapCVCameraIntrinsicCalibrationParameters;
#ifdef MAGICLEAPCVCAMERA_MagicLeapCVCameraFunctionLibrary_generated_h
#error "MagicLeapCVCameraFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapCVCameraFunctionLibrary.h"
#endif
#define MAGICLEAPCVCAMERA_MagicLeapCVCameraFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCameraOutput); \
	DECLARE_FUNCTION(execGetFramePose); \
	DECLARE_FUNCTION(execGetIntrinsicCalibrationParameters); \
	DECLARE_FUNCTION(execDisableAsync); \
	DECLARE_FUNCTION(execEnableAsync);


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCameraOutput); \
	DECLARE_FUNCTION(execGetFramePose); \
	DECLARE_FUNCTION(execGetIntrinsicCalibrationParameters); \
	DECLARE_FUNCTION(execDisableAsync); \
	DECLARE_FUNCTION(execEnableAsync);


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapCVCameraFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapCVCameraFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapCVCamera"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapCVCameraFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapCVCameraFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapCVCameraFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapCVCamera"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapCVCameraFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapCVCameraFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapCVCameraFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapCVCameraFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapCVCameraFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapCVCameraFunctionLibrary(UMagicLeapCVCameraFunctionLibrary&&); \
	NO_API UMagicLeapCVCameraFunctionLibrary(const UMagicLeapCVCameraFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapCVCameraFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapCVCameraFunctionLibrary(UMagicLeapCVCameraFunctionLibrary&&); \
	NO_API UMagicLeapCVCameraFunctionLibrary(const UMagicLeapCVCameraFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapCVCameraFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapCVCameraFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapCVCameraFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_15_PROLOG
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_INCLASS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCVCAMERA_API UClass* StaticClass<class UMagicLeapCVCameraFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
