// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCamera_init() {}
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCamera()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapCamera",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x9BF07E3D,
				0xC97A0C29,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
