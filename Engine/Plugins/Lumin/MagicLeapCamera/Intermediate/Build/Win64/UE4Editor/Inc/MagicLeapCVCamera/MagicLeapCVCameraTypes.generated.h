// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPCVCAMERA_MagicLeapCVCameraTypes_generated_h
#error "MagicLeapCVCameraTypes.generated.h already included, missing '#pragma once' in MagicLeapCVCameraTypes.h"
#endif
#define MAGICLEAPCVCAMERA_MagicLeapCVCameraTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraTypes_h_10_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPCVCAMERA_API UScriptStruct* StaticStruct<struct FMagicLeapCVCameraIntrinsicCalibrationParameters>();

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraTypes_h_51_DELEGATE \
struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisableMulti_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCVCameraDisableMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCVCameraDisableMulti, bool bSuccess) \
{ \
	_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisableMulti_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCVCameraDisableMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraTypes_h_50_DELEGATE \
struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisable_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCVCameraDisable_DelegateWrapper(const FScriptDelegate& MagicLeapCVCameraDisable, bool bSuccess) \
{ \
	_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisable_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCVCameraDisable.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraTypes_h_41_DELEGATE \
struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnableMulti_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCVCameraEnableMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapCVCameraEnableMulti, bool bSuccess) \
{ \
	_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnableMulti_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCVCameraEnableMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraTypes_h_40_DELEGATE \
struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnable_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FMagicLeapCVCameraEnable_DelegateWrapper(const FScriptDelegate& MagicLeapCVCameraEnable, bool bSuccess) \
{ \
	_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnable_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapCVCameraEnable.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
