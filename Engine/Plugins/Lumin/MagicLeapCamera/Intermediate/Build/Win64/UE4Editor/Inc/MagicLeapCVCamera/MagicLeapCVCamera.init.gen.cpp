// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCVCamera_init() {}
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCVCamera()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MagicLeapCVCamera",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xD4512AF1,
				0x4A79AFF4,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
