// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapCVCamera/Public/MagicLeapCVCameraComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCVCameraComponent() {}
// Cross Module References
	MAGICLEAPCVCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCVCameraComponent_NoRegister();
	MAGICLEAPCVCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCVCameraComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCVCamera();
	MAGICLEAPCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCameraOutput();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	MAGICLEAPCVCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapCVCameraComponent::execGetCameraOutput)
	{
		P_GET_STRUCT_REF(FMagicLeapCameraOutput,Z_Param_Out_OutCameraOutput);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetCameraOutput(Z_Param_Out_OutCameraOutput);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCVCameraComponent::execGetFramePose)
	{
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_OutFramePose);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetFramePose(Z_Param_Out_OutFramePose);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCVCameraComponent::execGetIntrinsicCalibrationParameters)
	{
		P_GET_STRUCT_REF(FMagicLeapCVCameraIntrinsicCalibrationParameters,Z_Param_Out_OutParams);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetIntrinsicCalibrationParameters(Z_Param_Out_OutParams);
		P_NATIVE_END;
	}
	void UMagicLeapCVCameraComponent::StaticRegisterNativesUMagicLeapCVCameraComponent()
	{
		UClass* Class = UMagicLeapCVCameraComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCameraOutput", &UMagicLeapCVCameraComponent::execGetCameraOutput },
			{ "GetFramePose", &UMagicLeapCVCameraComponent::execGetFramePose },
			{ "GetIntrinsicCalibrationParameters", &UMagicLeapCVCameraComponent::execGetIntrinsicCalibrationParameters },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics
	{
		struct MagicLeapCVCameraComponent_eventGetCameraOutput_Parms
		{
			FMagicLeapCameraOutput OutCameraOutput;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutCameraOutput;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::NewProp_OutCameraOutput = { "OutCameraOutput", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraComponent_eventGetCameraOutput_Parms, OutCameraOutput), Z_Construct_UScriptStruct_FMagicLeapCameraOutput, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraComponent_eventGetCameraOutput_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraComponent_eventGetCameraOutput_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::NewProp_OutCameraOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the latest transform of the camera.\n\x09\x09@param OutFramePose Contains the returned transform of the camera if the call is successful.\n\x09\x09@return True if the transform was successfully retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraComponent.h" },
		{ "ToolTip", "Gets the latest transform of the camera.\n@param OutFramePose Contains the returned transform of the camera if the call is successful.\n@return True if the transform was successfully retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraComponent, nullptr, "GetCameraOutput", nullptr, nullptr, sizeof(MagicLeapCVCameraComponent_eventGetCameraOutput_Parms), Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics
	{
		struct MagicLeapCVCameraComponent_eventGetFramePose_Parms
		{
			FTransform OutFramePose;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutFramePose;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::NewProp_OutFramePose = { "OutFramePose", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraComponent_eventGetFramePose_Parms, OutFramePose), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraComponent_eventGetFramePose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraComponent_eventGetFramePose_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::NewProp_OutFramePose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the latest transform of the camera.\n\x09\x09@param OutFramePose Contains the returned transform of the camera if the call is successful.\n\x09\x09@return True if the transform was successfully retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraComponent.h" },
		{ "ToolTip", "Gets the latest transform of the camera.\n@param OutFramePose Contains the returned transform of the camera if the call is successful.\n@return True if the transform was successfully retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraComponent, nullptr, "GetFramePose", nullptr, nullptr, sizeof(MagicLeapCVCameraComponent_eventGetFramePose_Parms), Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics
	{
		struct MagicLeapCVCameraComponent_eventGetIntrinsicCalibrationParameters_Parms
		{
			FMagicLeapCVCameraIntrinsicCalibrationParameters OutParams;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutParams;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::NewProp_OutParams = { "OutParams", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraComponent_eventGetIntrinsicCalibrationParameters_Parms, OutParams), Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraComponent_eventGetIntrinsicCalibrationParameters_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraComponent_eventGetIntrinsicCalibrationParameters_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::NewProp_OutParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the intrinsic calibration parameters of the camera.  Requires the camera to be connected.\n\x09\x09@param OutParam Contains the returned intrinsic calibration parameters if the call is successful.\n\x09\x09@return True if the parameters were successfully retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraComponent.h" },
		{ "ToolTip", "Gets the intrinsic calibration parameters of the camera.  Requires the camera to be connected.\n@param OutParam Contains the returned intrinsic calibration parameters if the call is successful.\n@return True if the parameters were successfully retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraComponent, nullptr, "GetIntrinsicCalibrationParameters", nullptr, nullptr, sizeof(MagicLeapCVCameraComponent_eventGetIntrinsicCalibrationParameters_Parms), Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapCVCameraComponent_NoRegister()
	{
		return UMagicLeapCVCameraComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDisabled_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnDisabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnLogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnLogMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCVCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetCameraOutput, "GetCameraOutput" }, // 4011450179
		{ &Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetFramePose, "GetFramePose" }, // 333099250
		{ &Z_Construct_UFunction_UMagicLeapCVCameraComponent_GetIntrinsicCalibrationParameters, "GetIntrinsicCalibrationParameters" }, // 1133003711
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n  The MagicLeapCVCameraComponent provides access to and maintains state for computer vision\n  camera capture functionality.  The connection to the device's camera is managed internally.\n  Users of this component are able to retrieve various computer vision data for processing.\n*/" },
		{ "IncludePath", "MagicLeapCVCameraComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraComponent.h" },
		{ "ToolTip", "The MagicLeapCVCameraComponent provides access to and maintains state for computer vision\ncamera capture functionality.  The connection to the device's camera is managed internally.\nUsers of this component are able to retrieve various computer vision data for processing." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnDisabled_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "CVCamera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnDisabled = { "OnDisabled", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCVCameraComponent, OnDisabled), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnDisabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnDisabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnLogMessage_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "CVCamera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnLogMessage = { "OnLogMessage", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCVCameraComponent, OnLogMessage), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnLogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnLogMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnDisabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::NewProp_OnLogMessage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapCVCameraComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::ClassParams = {
		&UMagicLeapCVCameraComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapCVCameraComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapCVCameraComponent, 4249281263);
	template<> MAGICLEAPCVCAMERA_API UClass* StaticClass<UMagicLeapCVCameraComponent>()
	{
		return UMagicLeapCVCameraComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapCVCameraComponent(Z_Construct_UClass_UMagicLeapCVCameraComponent, &UMagicLeapCVCameraComponent::StaticClass, TEXT("/Script/MagicLeapCVCamera"), TEXT("UMagicLeapCVCameraComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapCVCameraComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
