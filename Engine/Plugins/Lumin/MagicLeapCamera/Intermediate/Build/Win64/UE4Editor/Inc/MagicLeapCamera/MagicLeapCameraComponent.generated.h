// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPCAMERA_MagicLeapCameraComponent_generated_h
#error "MagicLeapCameraComponent.generated.h already included, missing '#pragma once' in MagicLeapCameraComponent.h"
#endif
#define MAGICLEAPCAMERA_MagicLeapCameraComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsCapturing); \
	DECLARE_FUNCTION(execStopRecordingAsync); \
	DECLARE_FUNCTION(execStartRecordingAsync); \
	DECLARE_FUNCTION(execCaptureImageToTextureAsync); \
	DECLARE_FUNCTION(execCaptureImageToFileAsync);


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsCapturing); \
	DECLARE_FUNCTION(execStopRecordingAsync); \
	DECLARE_FUNCTION(execStartRecordingAsync); \
	DECLARE_FUNCTION(execCaptureImageToTextureAsync); \
	DECLARE_FUNCTION(execCaptureImageToFileAsync);


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapCameraComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapCameraComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapCameraComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapCamera"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapCameraComponent)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapCameraComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapCameraComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapCameraComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapCamera"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapCameraComponent)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapCameraComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapCameraComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapCameraComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapCameraComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapCameraComponent(UMagicLeapCameraComponent&&); \
	NO_API UMagicLeapCameraComponent(const UMagicLeapCameraComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapCameraComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapCameraComponent(UMagicLeapCameraComponent&&); \
	NO_API UMagicLeapCameraComponent(const UMagicLeapCameraComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapCameraComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapCameraComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapCameraComponent)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnCaptureImgToFile() { return STRUCT_OFFSET(UMagicLeapCameraComponent, OnCaptureImgToFile); } \
	FORCEINLINE static uint32 __PPO__OnCaptureImgToTexture() { return STRUCT_OFFSET(UMagicLeapCameraComponent, OnCaptureImgToTexture); } \
	FORCEINLINE static uint32 __PPO__OnStartRecording() { return STRUCT_OFFSET(UMagicLeapCameraComponent, OnStartRecording); } \
	FORCEINLINE static uint32 __PPO__OnStopRecording() { return STRUCT_OFFSET(UMagicLeapCameraComponent, OnStopRecording); } \
	FORCEINLINE static uint32 __PPO__OnLogMessage() { return STRUCT_OFFSET(UMagicLeapCameraComponent, OnLogMessage); }


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_18_PROLOG
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_INCLASS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCAMERA_API UClass* StaticClass<class UMagicLeapCameraComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCamera_Public_MagicLeapCameraComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
