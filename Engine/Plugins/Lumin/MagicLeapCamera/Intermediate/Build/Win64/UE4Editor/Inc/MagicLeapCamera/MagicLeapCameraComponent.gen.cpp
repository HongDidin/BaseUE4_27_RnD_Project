// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapCamera/Public/MagicLeapCameraComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCameraComponent() {}
// Cross Module References
	MAGICLEAPCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCameraComponent_NoRegister();
	MAGICLEAPCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCameraComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCamera();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapCameraComponent::execIsCapturing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsCapturing();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraComponent::execStopRecordingAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->StopRecordingAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraComponent::execStartRecordingAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->StartRecordingAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraComponent::execCaptureImageToTextureAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CaptureImageToTextureAsync();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraComponent::execCaptureImageToFileAsync)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CaptureImageToFileAsync();
		P_NATIVE_END;
	}
	void UMagicLeapCameraComponent::StaticRegisterNativesUMagicLeapCameraComponent()
	{
		UClass* Class = UMagicLeapCameraComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CaptureImageToFileAsync", &UMagicLeapCameraComponent::execCaptureImageToFileAsync },
			{ "CaptureImageToTextureAsync", &UMagicLeapCameraComponent::execCaptureImageToTextureAsync },
			{ "IsCapturing", &UMagicLeapCameraComponent::execIsCapturing },
			{ "StartRecordingAsync", &UMagicLeapCameraComponent::execStartRecordingAsync },
			{ "StopRecordingAsync", &UMagicLeapCameraComponent::execStopRecordingAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics
	{
		struct MagicLeapCameraComponent_eventCaptureImageToFileAsync_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraComponent_eventCaptureImageToFileAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraComponent_eventCaptureImageToFileAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates a capture image to file task on a separate thread.\n\x09\x09@brief The newly created jpeg file will have an automatically generated name which is guaranteed\n\x09\x09\x09   to be unique.  Upon completion, a successful operation will provide the file path of the newly\n\x09\x09\x09   created jpeg to the FMagicLeapCameraCaptureImgToFile event handler.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
		{ "ToolTip", "Initiates a capture image to file task on a separate thread.\n@brief The newly created jpeg file will have an automatically generated name which is guaranteed\n           to be unique.  Upon completion, a successful operation will provide the file path of the newly\n           created jpeg to the FMagicLeapCameraCaptureImgToFile event handler.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraComponent, nullptr, "CaptureImageToFileAsync", nullptr, nullptr, sizeof(MagicLeapCameraComponent_eventCaptureImageToFileAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics
	{
		struct MagicLeapCameraComponent_eventCaptureImageToTextureAsync_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraComponent_eventCaptureImageToTextureAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraComponent_eventCaptureImageToTextureAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates a capture image to memory task on a speparate thread.\n\x09\x09@brief The user should register event handlers for both the success and fail events.  Upon completion,\n\x09\x09\x09   a successful operation will provide a dynamically generated texture containing the captured\n\x09\x09\x09   image to the FMagicLeapCameraCaptureImgToTextureSuccess event handler.\n\x09\x09@note The generated texture will be garbage collected when this app is destroyed.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
		{ "ToolTip", "Initiates a capture image to memory task on a speparate thread.\n@brief The user should register event handlers for both the success and fail events.  Upon completion,\n           a successful operation will provide a dynamically generated texture containing the captured\n           image to the FMagicLeapCameraCaptureImgToTextureSuccess event handler.\n@note The generated texture will be garbage collected when this app is destroyed.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraComponent, nullptr, "CaptureImageToTextureAsync", nullptr, nullptr, sizeof(MagicLeapCameraComponent_eventCaptureImageToTextureAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics
	{
		struct MagicLeapCameraComponent_eventIsCapturing_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraComponent_eventIsCapturing_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraComponent_eventIsCapturing_Parms), &Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the capture state of the component.\n\x09\x09@return True if the component is currently capturing, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
		{ "ToolTip", "Gets the capture state of the component.\n@return True if the component is currently capturing, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraComponent, nullptr, "IsCapturing", nullptr, nullptr, sizeof(MagicLeapCameraComponent_eventIsCapturing_Parms), Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics
	{
		struct MagicLeapCameraComponent_eventStartRecordingAsync_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraComponent_eventStartRecordingAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraComponent_eventStartRecordingAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the capturing of video/audio data on a separate thread.\n\x09\x09@note The system will continue to record video until StopRecordingVideo is called.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
		{ "ToolTip", "Initiates the capturing of video/audio data on a separate thread.\n@note The system will continue to record video until StopRecordingVideo is called.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraComponent, nullptr, "StartRecordingAsync", nullptr, nullptr, sizeof(MagicLeapCameraComponent_eventStartRecordingAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics
	{
		struct MagicLeapCameraComponent_eventStopRecordingAsync_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraComponent_eventStopRecordingAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraComponent_eventStopRecordingAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Stops the recording and saves the video/audio data to an mp4 file.\n\x09\x09@note The newly created mp4 file will have an automatically generated name which is guaranteed\n\x09\x09\x09  to be unique.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
		{ "ToolTip", "Stops the recording and saves the video/audio data to an mp4 file.\n@note The newly created mp4 file will have an automatically generated name which is guaranteed\n          to be unique.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraComponent, nullptr, "StopRecordingAsync", nullptr, nullptr, sizeof(MagicLeapCameraComponent_eventStopRecordingAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapCameraComponent_NoRegister()
	{
		return UMagicLeapCameraComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapCameraComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCaptureImgToFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCaptureImgToFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCaptureImgToTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCaptureImgToTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStartRecording_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStartRecording;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStopRecording_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStopRecording;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnLogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnLogMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapCameraComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapCameraComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToFileAsync, "CaptureImageToFileAsync" }, // 3988528846
		{ &Z_Construct_UFunction_UMagicLeapCameraComponent_CaptureImageToTextureAsync, "CaptureImageToTextureAsync" }, // 2847074487
		{ &Z_Construct_UFunction_UMagicLeapCameraComponent_IsCapturing, "IsCapturing" }, // 2637225408
		{ &Z_Construct_UFunction_UMagicLeapCameraComponent_StartRecordingAsync, "StartRecordingAsync" }, // 2702422558
		{ &Z_Construct_UFunction_UMagicLeapCameraComponent_StopRecordingAsync, "StopRecordingAsync" }, // 2206164081
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n  The MagicLeapCameraComponent provides access to and maintains state for camera capture functionality.\n  The connection to the device's camera is managed internally.  Users of this component\n  are able to asynchronously capture camera images and footage to file.  Alternatively,\n  a camera image can be captured directly to texture.  The user need only make the relevant\n  asynchronous call and then register the appropriate event handlers for the\n  operation's completion.\n*/" },
		{ "IncludePath", "MagicLeapCameraComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
		{ "ToolTip", "The MagicLeapCameraComponent provides access to and maintains state for camera capture functionality.\nThe connection to the device's camera is managed internally.  Users of this component\nare able to asynchronously capture camera images and footage to file.  Alternatively,\na camera image can be captured directly to texture.  The user need only make the relevant\nasynchronous call and then register the appropriate event handlers for the\noperation's completion." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToFile_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Camera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToFile = { "OnCaptureImgToFile", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCameraComponent, OnCaptureImgToFile), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToTexture_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Camera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToTexture = { "OnCaptureImgToTexture", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCameraComponent, OnCaptureImgToTexture), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStartRecording_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Camera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStartRecording = { "OnStartRecording", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCameraComponent, OnStartRecording), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStartRecording_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStartRecording_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStopRecording_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Camera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStopRecording = { "OnStopRecording", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCameraComponent, OnStopRecording), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStopRecording_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStopRecording_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnLogMessage_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Camera | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnLogMessage = { "OnLogMessage", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapCameraComponent, OnLogMessage), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnLogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnLogMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapCameraComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnCaptureImgToTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStartRecording,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnStopRecording,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapCameraComponent_Statics::NewProp_OnLogMessage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapCameraComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapCameraComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapCameraComponent_Statics::ClassParams = {
		&UMagicLeapCameraComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapCameraComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapCameraComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapCameraComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapCameraComponent, 2845652974);
	template<> MAGICLEAPCAMERA_API UClass* StaticClass<UMagicLeapCameraComponent>()
	{
		return UMagicLeapCameraComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapCameraComponent(Z_Construct_UClass_UMagicLeapCameraComponent, &UMagicLeapCameraComponent::StaticClass, TEXT("/Script/MagicLeapCamera"), TEXT("UMagicLeapCameraComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapCameraComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
