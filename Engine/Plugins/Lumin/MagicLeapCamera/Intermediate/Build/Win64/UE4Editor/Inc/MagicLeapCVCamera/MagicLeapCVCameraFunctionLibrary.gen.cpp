// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapCVCamera/Public/MagicLeapCVCameraFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCVCameraFunctionLibrary() {}
// Cross Module References
	MAGICLEAPCVCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_NoRegister();
	MAGICLEAPCVCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCVCamera();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature();
	MAGICLEAPCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCameraOutput();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	MAGICLEAPCVCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapCVCameraFunctionLibrary::execGetCameraOutput)
	{
		P_GET_STRUCT_REF(FMagicLeapCameraOutput,Z_Param_Out_OutCameraOutput);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCVCameraFunctionLibrary::GetCameraOutput(Z_Param_Out_OutCameraOutput);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCVCameraFunctionLibrary::execGetFramePose)
	{
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_OutFramePose);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCVCameraFunctionLibrary::GetFramePose(Z_Param_Out_OutFramePose);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCVCameraFunctionLibrary::execGetIntrinsicCalibrationParameters)
	{
		P_GET_STRUCT_REF(FMagicLeapCVCameraIntrinsicCalibrationParameters,Z_Param_Out_OutParams);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCVCameraFunctionLibrary::GetIntrinsicCalibrationParameters(Z_Param_Out_OutParams);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCVCameraFunctionLibrary::execDisableAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_OnDisable);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCVCameraFunctionLibrary::DisableAsync(FMagicLeapCVCameraDisable(Z_Param_Out_OnDisable));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCVCameraFunctionLibrary::execEnableAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_OnEnable);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCVCameraFunctionLibrary::EnableAsync(FMagicLeapCVCameraEnable(Z_Param_Out_OnEnable));
		P_NATIVE_END;
	}
	void UMagicLeapCVCameraFunctionLibrary::StaticRegisterNativesUMagicLeapCVCameraFunctionLibrary()
	{
		UClass* Class = UMagicLeapCVCameraFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DisableAsync", &UMagicLeapCVCameraFunctionLibrary::execDisableAsync },
			{ "EnableAsync", &UMagicLeapCVCameraFunctionLibrary::execEnableAsync },
			{ "GetCameraOutput", &UMagicLeapCVCameraFunctionLibrary::execGetCameraOutput },
			{ "GetFramePose", &UMagicLeapCVCameraFunctionLibrary::execGetFramePose },
			{ "GetIntrinsicCalibrationParameters", &UMagicLeapCVCameraFunctionLibrary::execGetIntrinsicCalibrationParameters },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics
	{
		struct MagicLeapCVCameraFunctionLibrary_eventDisableAsync_Parms
		{
			FScriptDelegate OnDisable;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnDisable_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnDisable;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_OnDisable_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_OnDisable = { "OnDisable", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraFunctionLibrary_eventDisableAsync_Parms, OnDisable), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_OnDisable_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_OnDisable_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraFunctionLibrary_eventDisableAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraFunctionLibrary_eventDisableAsync_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_OnDisable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera Function Library | MagicLeap" },
		{ "Comment", "/** Closes the computer vision stream. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraFunctionLibrary.h" },
		{ "ToolTip", "Closes the computer vision stream." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary, nullptr, "DisableAsync", nullptr, nullptr, sizeof(MagicLeapCVCameraFunctionLibrary_eventDisableAsync_Parms), Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics
	{
		struct MagicLeapCVCameraFunctionLibrary_eventEnableAsync_Parms
		{
			FScriptDelegate OnEnable;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnEnable_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnEnable;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_OnEnable_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_OnEnable = { "OnEnable", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraFunctionLibrary_eventEnableAsync_Parms, OnEnable), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_OnEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_OnEnable_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraFunctionLibrary_eventEnableAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraFunctionLibrary_eventEnableAsync_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_OnEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera Function Library | MagicLeap" },
		{ "Comment", "/** Initializes the computer vision stream. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraFunctionLibrary.h" },
		{ "ToolTip", "Initializes the computer vision stream." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary, nullptr, "EnableAsync", nullptr, nullptr, sizeof(MagicLeapCVCameraFunctionLibrary_eventEnableAsync_Parms), Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics
	{
		struct MagicLeapCVCameraFunctionLibrary_eventGetCameraOutput_Parms
		{
			FMagicLeapCameraOutput OutCameraOutput;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutCameraOutput;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::NewProp_OutCameraOutput = { "OutCameraOutput", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraFunctionLibrary_eventGetCameraOutput_Parms, OutCameraOutput), Z_Construct_UScriptStruct_FMagicLeapCameraOutput, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraFunctionLibrary_eventGetCameraOutput_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraFunctionLibrary_eventGetCameraOutput_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::NewProp_OutCameraOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the latest transform of the camera.\n\x09\x09@param OutFramePose Contains the returned transform of the camera if the call is successful.\n\x09\x09@return True if the transform was successfully retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraFunctionLibrary.h" },
		{ "ToolTip", "Gets the latest transform of the camera.\n@param OutFramePose Contains the returned transform of the camera if the call is successful.\n@return True if the transform was successfully retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary, nullptr, "GetCameraOutput", nullptr, nullptr, sizeof(MagicLeapCVCameraFunctionLibrary_eventGetCameraOutput_Parms), Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics
	{
		struct MagicLeapCVCameraFunctionLibrary_eventGetFramePose_Parms
		{
			FTransform OutFramePose;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutFramePose;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::NewProp_OutFramePose = { "OutFramePose", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraFunctionLibrary_eventGetFramePose_Parms, OutFramePose), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraFunctionLibrary_eventGetFramePose_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraFunctionLibrary_eventGetFramePose_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::NewProp_OutFramePose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the latest transform of the camera.\n\x09\x09@param OutFramePose Contains the returned transform of the camera if the call is successful.\n\x09\x09@return True if the transform was successfully retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraFunctionLibrary.h" },
		{ "ToolTip", "Gets the latest transform of the camera.\n@param OutFramePose Contains the returned transform of the camera if the call is successful.\n@return True if the transform was successfully retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary, nullptr, "GetFramePose", nullptr, nullptr, sizeof(MagicLeapCVCameraFunctionLibrary_eventGetFramePose_Parms), Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics
	{
		struct MagicLeapCVCameraFunctionLibrary_eventGetIntrinsicCalibrationParameters_Parms
		{
			FMagicLeapCVCameraIntrinsicCalibrationParameters OutParams;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutParams;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::NewProp_OutParams = { "OutParams", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCVCameraFunctionLibrary_eventGetIntrinsicCalibrationParameters_Parms, OutParams), Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCVCameraFunctionLibrary_eventGetIntrinsicCalibrationParameters_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCVCameraFunctionLibrary_eventGetIntrinsicCalibrationParameters_Parms), &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::NewProp_OutParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::Function_MetaDataParams[] = {
		{ "Category", "CVCamera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the intrinsic calibration parameters of the camera.  Requires the camera to be connected.\n\x09\x09@param OutParam Contains the returned intrinsic calibration parameters if the call is successful.\n\x09\x09@return True if the parameters were successfully retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraFunctionLibrary.h" },
		{ "ToolTip", "Gets the intrinsic calibration parameters of the camera.  Requires the camera to be connected.\n@param OutParam Contains the returned intrinsic calibration parameters if the call is successful.\n@return True if the parameters were successfully retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary, nullptr, "GetIntrinsicCalibrationParameters", nullptr, nullptr, sizeof(MagicLeapCVCameraFunctionLibrary_eventGetIntrinsicCalibrationParameters_Parms), Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_NoRegister()
	{
		return UMagicLeapCVCameraFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCVCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_DisableAsync, "DisableAsync" }, // 2236607159
		{ &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_EnableAsync, "EnableAsync" }, // 3408155457
		{ &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetCameraOutput, "GetCameraOutput" }, // 1171257990
		{ &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetFramePose, "GetFramePose" }, // 648996965
		{ &Z_Construct_UFunction_UMagicLeapCVCameraFunctionLibrary_GetIntrinsicCalibrationParameters, "GetIntrinsicCalibrationParameters" }, // 2456412716
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n  The MagicLeapCVCameraLibrary provides access to and maintains state for computer vision\n  camera capture functionality.  The connection to the device's camera is managed internally.\n  Users of this function library are able to retrieve various computer vision data for processing.\n*/" },
		{ "IncludePath", "MagicLeapCVCameraFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraFunctionLibrary.h" },
		{ "ToolTip", "The MagicLeapCVCameraLibrary provides access to and maintains state for computer vision\ncamera capture functionality.  The connection to the device's camera is managed internally.\nUsers of this function library are able to retrieve various computer vision data for processing." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapCVCameraFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapCVCameraFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapCVCameraFunctionLibrary, 1243467652);
	template<> MAGICLEAPCVCAMERA_API UClass* StaticClass<UMagicLeapCVCameraFunctionLibrary>()
	{
		return UMagicLeapCVCameraFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapCVCameraFunctionLibrary(Z_Construct_UClass_UMagicLeapCVCameraFunctionLibrary, &UMagicLeapCVCameraFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapCVCamera"), TEXT("UMagicLeapCVCameraFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapCVCameraFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
