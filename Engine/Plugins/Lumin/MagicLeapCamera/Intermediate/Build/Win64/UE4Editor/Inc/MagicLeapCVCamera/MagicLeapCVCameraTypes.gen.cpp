// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapCVCamera/Public/MagicLeapCVCameraTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCVCameraTypes() {}
// Cross Module References
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCVCamera();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature();
	MAGICLEAPCVCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature();
	MAGICLEAPCVCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisableMulti_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisableMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisableMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCVCamera, nullptr, "MagicLeapCVCameraDisableMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisableMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisableMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisable_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisable_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisable_Parms), &Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCVCamera, nullptr, "MagicLeapCVCameraDisable__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraDisable_Parms), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraDisable__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnableMulti_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnableMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnableMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCVCamera, nullptr, "MagicLeapCVCameraEnableMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnableMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnableMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnable_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnable_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnable_Parms), &Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCVCamera, nullptr, "MagicLeapCVCameraEnable__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCVCamera_eventMagicLeapCVCameraEnable_Parms), Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCVCamera_MagicLeapCVCameraEnable__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FMagicLeapCVCameraIntrinsicCalibrationParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPCVCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters, Z_Construct_UPackage__Script_MagicLeapCVCamera(), TEXT("MagicLeapCVCameraIntrinsicCalibrationParameters"), sizeof(FMagicLeapCVCameraIntrinsicCalibrationParameters), Get_Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPCVCAMERA_API UScriptStruct* StaticStruct<FMagicLeapCVCameraIntrinsicCalibrationParameters>()
{
	return FMagicLeapCVCameraIntrinsicCalibrationParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters(FMagicLeapCVCameraIntrinsicCalibrationParameters::StaticStruct, TEXT("/Script/MagicLeapCVCamera"), TEXT("MagicLeapCVCameraIntrinsicCalibrationParameters"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapCVCamera_StaticRegisterNativesFMagicLeapCVCameraIntrinsicCalibrationParameters
{
	FScriptStruct_MagicLeapCVCamera_StaticRegisterNativesFMagicLeapCVCameraIntrinsicCalibrationParameters()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapCVCameraIntrinsicCalibrationParameters>(FName(TEXT("MagicLeapCVCameraIntrinsicCalibrationParameters")));
	}
} ScriptStruct_MagicLeapCVCamera_StaticRegisterNativesFMagicLeapCVCameraIntrinsicCalibrationParameters;
	struct Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrincipalPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrincipalPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FOV_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FOV;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Distortion_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distortion_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Distortion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapCVCameraIntrinsicCalibrationParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "CVCamera|MagicLeap" },
		{ "Comment", "/*! Camera width. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
		{ "ToolTip", "! Camera width." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCVCameraIntrinsicCalibrationParameters, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "CVCamera|MagicLeap" },
		{ "Comment", "/*! Camera height. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
		{ "ToolTip", "! Camera height." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCVCameraIntrinsicCalibrationParameters, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FocalLength_MetaData[] = {
		{ "Category", "CVCamera|MagicLeap" },
		{ "Comment", "/*! Camera focal length. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
		{ "ToolTip", "! Camera focal length." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FocalLength = { "FocalLength", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCVCameraIntrinsicCalibrationParameters, FocalLength), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_PrincipalPoint_MetaData[] = {
		{ "Category", "CVCamera|MagicLeap" },
		{ "Comment", "/*! Camera principle point. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
		{ "ToolTip", "! Camera principle point." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_PrincipalPoint = { "PrincipalPoint", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCVCameraIntrinsicCalibrationParameters, PrincipalPoint), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_PrincipalPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_PrincipalPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FOV_MetaData[] = {
		{ "Category", "CVCamera|MagicLeap" },
		{ "Comment", "/*! Field of view. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
		{ "ToolTip", "! Field of view." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FOV = { "FOV", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCVCameraIntrinsicCalibrationParameters, FOV), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FOV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FOV_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion_Inner = { "Distortion", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion_MetaData[] = {
		{ "Category", "CVCamera|MagicLeap" },
		{ "Comment", "/*! Distortion vector. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCVCameraTypes.h" },
		{ "ToolTip", "! Distortion vector." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion = { "Distortion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCVCameraIntrinsicCalibrationParameters, Distortion), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_PrincipalPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_FOV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::NewProp_Distortion,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCVCamera,
		nullptr,
		&NewStructOps,
		"MagicLeapCVCameraIntrinsicCalibrationParameters",
		sizeof(FMagicLeapCVCameraIntrinsicCalibrationParameters),
		alignof(FMagicLeapCVCameraIntrinsicCalibrationParameters),
		Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapCVCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapCVCameraIntrinsicCalibrationParameters"), sizeof(FMagicLeapCVCameraIntrinsicCalibrationParameters), Get_Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCVCameraIntrinsicCalibrationParameters_Hash() { return 1022854028U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
