// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMagicLeapCameraOutput;
struct FTransform;
struct FMagicLeapCVCameraIntrinsicCalibrationParameters;
#ifdef MAGICLEAPCVCAMERA_MagicLeapCVCameraComponent_generated_h
#error "MagicLeapCVCameraComponent.generated.h already included, missing '#pragma once' in MagicLeapCVCameraComponent.h"
#endif
#define MAGICLEAPCVCAMERA_MagicLeapCVCameraComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCameraOutput); \
	DECLARE_FUNCTION(execGetFramePose); \
	DECLARE_FUNCTION(execGetIntrinsicCalibrationParameters);


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCameraOutput); \
	DECLARE_FUNCTION(execGetFramePose); \
	DECLARE_FUNCTION(execGetIntrinsicCalibrationParameters);


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapCVCameraComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapCVCameraComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapCVCamera"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapCVCameraComponent)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapCVCameraComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapCVCameraComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapCVCameraComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapCVCamera"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapCVCameraComponent)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapCVCameraComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapCVCameraComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapCVCameraComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapCVCameraComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapCVCameraComponent(UMagicLeapCVCameraComponent&&); \
	NO_API UMagicLeapCVCameraComponent(const UMagicLeapCVCameraComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapCVCameraComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapCVCameraComponent(UMagicLeapCVCameraComponent&&); \
	NO_API UMagicLeapCVCameraComponent(const UMagicLeapCVCameraComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapCVCameraComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapCVCameraComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapCVCameraComponent)


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnDisabled() { return STRUCT_OFFSET(UMagicLeapCVCameraComponent, OnDisabled); } \
	FORCEINLINE static uint32 __PPO__OnLogMessage() { return STRUCT_OFFSET(UMagicLeapCVCameraComponent, OnLogMessage); }


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_16_PROLOG
#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_INCLASS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPCVCAMERA_API UClass* StaticClass<class UMagicLeapCVCameraComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapCamera_Source_MagicLeapCVCamera_Public_MagicLeapCVCameraComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
