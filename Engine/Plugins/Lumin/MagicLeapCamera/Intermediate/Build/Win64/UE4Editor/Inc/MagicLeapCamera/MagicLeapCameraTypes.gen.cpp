// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapCamera/Public/MagicLeapCameraTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCameraTypes() {}
// Cross Module References
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCamera();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature();
	MAGICLEAPCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCameraOutput();
	MAGICLEAPCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EPixelFormat();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraLogMessageMulti_Parms
		{
			FString LogMessage;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LogMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage = { "LogMessage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraLogMessageMulti_Parms, LogMessage), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::NewProp_LogMessage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraLogMessageMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraLogMessageMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessageMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraLogMessage_Parms
		{
			FString LogMessage;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LogMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::NewProp_LogMessage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::NewProp_LogMessage = { "LogMessage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraLogMessage_Parms, LogMessage), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::NewProp_LogMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::NewProp_LogMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::NewProp_LogMessage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09""Delegate used to pass log messages from the capture worker thread to the initiating blueprint.\n\x09@note This is useful if the user wishes to have log messages in 3D space.\n\x09@param LogMessage A string containing the log message.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to pass log messages from the capture worker thread to the initiating blueprint.\n@note This is useful if the user wishes to have log messages in 3D space.\n@param LogMessage A string containing the log message." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraLogMessage__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraLogMessage_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms
		{
			bool bSuccess;
			FString FilePath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms, FilePath), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_FilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::NewProp_FilePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraStopRecordingMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStopRecordingMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecordingMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms
		{
			bool bSuccess;
			FString FilePath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms, FilePath), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_FilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::NewProp_FilePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09""Delegate used to notify the initiating blueprint of the result of a request to stop recording video.\n\x09@note Although this signals the task as complete, it may have failed or been cancelled.\n\x09@param bSuccess True if the task succeeded, false otherwise.\n\x09@param FilePath A string containing the path to the newly created mp4.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to notify the initiating blueprint of the result of a request to stop recording video.\n@note Although this signals the task as complete, it may have failed or been cancelled.\n@param bSuccess True if the task succeeded, false otherwise.\n@param FilePath A string containing the path to the newly created mp4." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraStopRecording__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStopRecording_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraStartRecordingMulti_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraStartRecordingMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStartRecordingMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraStartRecordingMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStartRecordingMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecordingMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraStartRecording_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraStartRecording_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStartRecording_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09""Delegate used to notify the initiating blueprint of the result of a request to begin recording video.\n\x09@note Although this signals the task as complete, it may have failed or been cancelled.\n\x09@param bSuccess True if the task succeeded, false otherwise.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to notify the initiating blueprint of the result of a request to begin recording video.\n@note Although this signals the task as complete, it may have failed or been cancelled.\n@param bSuccess True if the task succeeded, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraStartRecording__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraStartRecording_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms
		{
			bool bSuccess;
			UTexture2D* CaptureTexture;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CaptureTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_CaptureTexture = { "CaptureTexture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms, CaptureTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::NewProp_CaptureTexture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTextureMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTextureMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms
		{
			bool bSuccess;
			UTexture2D* CaptureTexture;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CaptureTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_CaptureTexture = { "CaptureTexture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms, CaptureTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::NewProp_CaptureTexture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09""Delegate used to pass the captured image back to the initiating blueprint.\n\x09@note The captured texture will remain in memory for the lifetime of the calling application (if the task succeeds).\n\x09@param bSuccess True if the task succeeded, false otherwise.\n\x09@param CaptureTexture A UTexture2D containing the captured image.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to pass the captured image back to the initiating blueprint.\n@note The captured texture will remain in memory for the lifetime of the calling application (if the task succeeds).\n@param bSuccess True if the task succeeded, false otherwise.\n@param CaptureTexture A UTexture2D containing the captured image." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraCaptureImgToTexture__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToTexture_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms
		{
			bool bSuccess;
			FString FilePath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms, FilePath), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_FilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::NewProp_FilePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraCaptureImgToFileMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFileMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFileMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms
		{
			bool bSuccess;
			FString FilePath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_FilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms, FilePath), METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_FilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::NewProp_FilePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n   Delegate used to notify the initiating blueprint when a capture image to file task has completed.\n   @note Although this signals the task as complete, it may have failed or been cancelled.\n   @param bSuccess True if the task succeeded, false otherwise.\n   @param FilePath A string containing the file path to the newly created jpeg.\n*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to notify the initiating blueprint when a capture image to file task has completed.\n@note Although this signals the task as complete, it may have failed or been cancelled.\n@param bSuccess True if the task succeeded, false otherwise.\n@param FilePath A string containing the file path to the newly created jpeg." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraCaptureImgToFile__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraCaptureImgToFile_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraDisconnect_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraDisconnect_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraDisconnect_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to notify the initiating blueprint when the camera disonnect task has completed. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to notify the initiating blueprint when the camera disonnect task has completed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraDisconnect__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraDisconnect_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics
	{
		struct _Script_MagicLeapCamera_eventMagicLeapCameraConnect_Parms
		{
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapCamera_eventMagicLeapCameraConnect_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraConnect_Parms), &Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to notify the initiating blueprint when the camera connect task has completed. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Delegate used to notify the initiating blueprint when the camera connect task has completed." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapCamera, nullptr, "MagicLeapCameraConnect__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapCamera_eventMagicLeapCameraConnect_Parms), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FMagicLeapCameraOutput::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapCameraOutput, Z_Construct_UPackage__Script_MagicLeapCamera(), TEXT("MagicLeapCameraOutput"), sizeof(FMagicLeapCameraOutput), Get_Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPCAMERA_API UScriptStruct* StaticStruct<FMagicLeapCameraOutput>()
{
	return FMagicLeapCameraOutput::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapCameraOutput(FMagicLeapCameraOutput::StaticStruct, TEXT("/Script/MagicLeapCamera"), TEXT("MagicLeapCameraOutput"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapCamera_StaticRegisterNativesFMagicLeapCameraOutput
{
	FScriptStruct_MagicLeapCamera_StaticRegisterNativesFMagicLeapCameraOutput()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapCameraOutput>(FName(TEXT("MagicLeapCameraOutput")));
	}
} ScriptStruct_MagicLeapCamera_StaticRegisterNativesFMagicLeapCameraOutput;
	struct Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Planes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Planes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Planes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Format_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Format;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapCameraOutput>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes_Inner = { "Planes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/**\n\x09\x09Output image plane info. The number of output planes is determined by the format:\n\x09\x09""1 for compressed output such as JPEG stream,\n\x09\x09""3 for separate color component output such as YUV/RGB.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Output image plane info. The number of output planes is determined by the format:\n1 for compressed output such as JPEG stream,\n3 for separate color component output such as YUV/RGB." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes = { "Planes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraOutput, Planes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Format_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/** Supported output format specified by MLCameraOutputFormat. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Supported output format specified by MLCameraOutputFormat." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Format = { "Format", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraOutput, Format), Z_Construct_UEnum_CoreUObject_EPixelFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Format_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Format_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Planes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::NewProp_Format,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCamera,
		nullptr,
		&NewStructOps,
		"MagicLeapCameraOutput",
		sizeof(FMagicLeapCameraOutput),
		alignof(FMagicLeapCameraOutput),
		Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCameraOutput()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapCameraOutput"), sizeof(FMagicLeapCameraOutput), Get_Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCameraOutput_Hash() { return 2788613310U; }
class UScriptStruct* FMagicLeapCameraPlaneInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo, Z_Construct_UPackage__Script_MagicLeapCamera(), TEXT("MagicLeapCameraPlaneInfo"), sizeof(FMagicLeapCameraPlaneInfo), Get_Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPCAMERA_API UScriptStruct* StaticStruct<FMagicLeapCameraPlaneInfo>()
{
	return FMagicLeapCameraPlaneInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapCameraPlaneInfo(FMagicLeapCameraPlaneInfo::StaticStruct, TEXT("/Script/MagicLeapCamera"), TEXT("MagicLeapCameraPlaneInfo"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapCamera_StaticRegisterNativesFMagicLeapCameraPlaneInfo
{
	FScriptStruct_MagicLeapCamera_StaticRegisterNativesFMagicLeapCameraPlaneInfo()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapCameraPlaneInfo>(FName(TEXT("MagicLeapCameraPlaneInfo")));
	}
} ScriptStruct_MagicLeapCamera_StaticRegisterNativesFMagicLeapCameraPlaneInfo;
	struct Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Stride_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Stride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BytesPerPixel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_BytesPerPixel;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapCameraPlaneInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/** Width of the output image in pixels. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Width of the output image in pixels." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraPlaneInfo, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/** Height of the output image in pixels. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Height of the output image in pixels." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraPlaneInfo, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Stride_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/** Stride of the output image in bytes. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Stride of the output image in bytes." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Stride = { "Stride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraPlaneInfo, Stride), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Stride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Stride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_BytesPerPixel_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/** Number of bytes used to represent a pixel. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Number of bytes used to represent a pixel." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_BytesPerPixel = { "BytesPerPixel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraPlaneInfo, BytesPerPixel), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_BytesPerPixel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_BytesPerPixel_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data_Inner = { "Data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data_MetaData[] = {
		{ "Category", "Camera|MagicLeap" },
		{ "Comment", "/** Image data. */" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraTypes.h" },
		{ "ToolTip", "Image data." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapCameraPlaneInfo, Data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Stride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_BytesPerPixel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCamera,
		nullptr,
		&NewStructOps,
		"MagicLeapCameraPlaneInfo",
		sizeof(FMagicLeapCameraPlaneInfo),
		alignof(FMagicLeapCameraPlaneInfo),
		Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapCameraPlaneInfo"), sizeof(FMagicLeapCameraPlaneInfo), Get_Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapCameraPlaneInfo_Hash() { return 2253724545U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
