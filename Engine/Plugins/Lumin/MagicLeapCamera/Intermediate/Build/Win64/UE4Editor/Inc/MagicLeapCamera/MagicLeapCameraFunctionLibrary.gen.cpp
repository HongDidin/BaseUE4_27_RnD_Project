// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapCamera/Public/MagicLeapCameraFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapCameraFunctionLibrary() {}
// Cross Module References
	MAGICLEAPCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_NoRegister();
	MAGICLEAPCAMERA_API UClass* Z_Construct_UClass_UMagicLeapCameraFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapCamera();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature();
	MAGICLEAPCAMERA_API UFunction* Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execIsCapturing)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::IsCapturing();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execSetLogDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_LogDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::SetLogDelegate(FMagicLeapCameraLogMessage(Z_Param_Out_LogDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execStopRecordingAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::StopRecordingAsync(FMagicLeapCameraStopRecording(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execStartRecordingAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::StartRecordingAsync(FMagicLeapCameraStartRecording(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execCaptureImageToTextureAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::CaptureImageToTextureAsync(FMagicLeapCameraCaptureImgToTexture(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execCaptureImageToFileAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::CaptureImageToFileAsync(FMagicLeapCameraCaptureImgToFile(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execCameraDisconnect)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::CameraDisconnect(FMagicLeapCameraDisconnect(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapCameraFunctionLibrary::execCameraConnect)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_ResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapCameraFunctionLibrary::CameraConnect(FMagicLeapCameraConnect(Z_Param_Out_ResultDelegate));
		P_NATIVE_END;
	}
	void UMagicLeapCameraFunctionLibrary::StaticRegisterNativesUMagicLeapCameraFunctionLibrary()
	{
		UClass* Class = UMagicLeapCameraFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CameraConnect", &UMagicLeapCameraFunctionLibrary::execCameraConnect },
			{ "CameraDisconnect", &UMagicLeapCameraFunctionLibrary::execCameraDisconnect },
			{ "CaptureImageToFileAsync", &UMagicLeapCameraFunctionLibrary::execCaptureImageToFileAsync },
			{ "CaptureImageToTextureAsync", &UMagicLeapCameraFunctionLibrary::execCaptureImageToTextureAsync },
			{ "IsCapturing", &UMagicLeapCameraFunctionLibrary::execIsCapturing },
			{ "SetLogDelegate", &UMagicLeapCameraFunctionLibrary::execSetLogDelegate },
			{ "StartRecordingAsync", &UMagicLeapCameraFunctionLibrary::execStartRecordingAsync },
			{ "StopRecordingAsync", &UMagicLeapCameraFunctionLibrary::execStopRecordingAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventCameraConnect_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventCameraConnect_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraConnect__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventCameraConnect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventCameraConnect_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/** \n\x09\x09""Establishes a connection with the device's camera.\n\x09\x09@note A connection will be made automatically upon the first capture call if this is\n\x09\x09\x09  not called first.  Calling this function manually allows the developer to control\n\x09\x09\x09  when privilege notifications for this plugin will be activated (if application\n\x09\x09\x09  is being used for the first time).\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Establishes a connection with the device's camera.\n@note A connection will be made automatically upon the first capture call if this is\n          not called first.  Calling this function manually allows the developer to control\n          when privilege notifications for this plugin will be activated (if application\n          is being used for the first time).\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "CameraConnect", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventCameraConnect_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventCameraDisconnect_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventCameraDisconnect_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraDisconnect__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventCameraDisconnect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventCameraDisconnect_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/** \n\x09\x09""Disconnects from the device's camera.\n\x09\x09@note This function must be called before the application terminates (if the camera has\n\x09\x09\x09  been connected to).  Failure to do so will result in the camera connection remaining\n\x09\x09\x09  open (and the camera icon remaining on screen).\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Disconnects from the device's camera.\n@note This function must be called before the application terminates (if the camera has\n          been connected to).  Failure to do so will result in the camera connection remaining\n          open (and the camera icon remaining on screen).\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "CameraDisconnect", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventCameraDisconnect_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventCaptureImageToFileAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventCaptureImageToFileAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToFile__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventCaptureImageToFileAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventCaptureImageToFileAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates a capture image to file task on a separate thread.\n\x09\x09@brief The newly created jpeg file will have an automatically generated name which is guaranteed\n\x09\x09\x09   to be unique.  Upon completion, a successful operation will provide the file path of the newly\n\x09\x09\x09   created jpeg to the FMagicLeapCameraCaptureImgToFile event handler.\n\x09\x09@param ResultDelegate The delegate to be notified once the camera image has been saved to a jpeg file.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Initiates a capture image to file task on a separate thread.\n@brief The newly created jpeg file will have an automatically generated name which is guaranteed\n           to be unique.  Upon completion, a successful operation will provide the file path of the newly\n           created jpeg to the FMagicLeapCameraCaptureImgToFile event handler.\n@param ResultDelegate The delegate to be notified once the camera image has been saved to a jpeg file.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "CaptureImageToFileAsync", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventCaptureImageToFileAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventCaptureImageToTextureAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventCaptureImageToTextureAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraCaptureImgToTexture__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventCaptureImageToTextureAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventCaptureImageToTextureAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates a capture image to memory task on a speparate thread.\n\x09\x09@brief The user should register event handlers for both the success and fail events.  Upon completion,\n\x09\x09\x09   a successful operation will provide a dynamically generated texture containing the captured\n\x09\x09\x09   image to the FMagicLeapCameraCaptureImgToTextureSuccess event handler.\n\x09\x09@note The generated texture will be garbage collected when this app is destroyed.\n\x09\x09@param ResultDelegate The delegate to be notified once the camera image has been saved to a texture.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Initiates a capture image to memory task on a speparate thread.\n@brief The user should register event handlers for both the success and fail events.  Upon completion,\n           a successful operation will provide a dynamically generated texture containing the captured\n           image to the FMagicLeapCameraCaptureImgToTextureSuccess event handler.\n@note The generated texture will be garbage collected when this app is destroyed.\n@param ResultDelegate The delegate to be notified once the camera image has been saved to a texture.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "CaptureImageToTextureAsync", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventCaptureImageToTextureAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventIsCapturing_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventIsCapturing_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventIsCapturing_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the capture state of the component.\n\x09\x09@return True if the component is currently capturing, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Gets the capture state of the component.\n@return True if the component is currently capturing, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "IsCapturing", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventIsCapturing_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventSetLogDelegate_Parms
		{
			FScriptDelegate LogDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_LogDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate = { "LogDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventSetLogDelegate_Parms, LogDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraLogMessage__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventSetLogDelegate_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventSetLogDelegate_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_LogDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the delegate by which the system can pass log messages back to the calling blueprint.\n\x09\x09@param LogDelegate The delegate by which the system will return log messages to the calling blueprint.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Sets the delegate by which the system can pass log messages back to the calling blueprint.\n@param LogDelegate The delegate by which the system will return log messages to the calling blueprint.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "SetLogDelegate", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventSetLogDelegate_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventStartRecordingAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventStartRecordingAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStartRecording__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventStartRecordingAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventStartRecordingAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Initiates the capturing of video/audio data on a separate thread.\n\x09\x09@note The system will continue to record video until StopRecordingVideo is called.\n\x09\x09@param ResultDelegate The delegate to be notified once the recording has begun or failed to begin.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Initiates the capturing of video/audio data on a separate thread.\n@note The system will continue to record video until StopRecordingVideo is called.\n@param ResultDelegate The delegate to be notified once the recording has begun or failed to begin.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "StartRecordingAsync", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventStartRecordingAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics
	{
		struct MagicLeapCameraFunctionLibrary_eventStopRecordingAsync_Parms
		{
			FScriptDelegate ResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ResultDelegate = { "ResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapCameraFunctionLibrary_eventStopRecordingAsync_Parms, ResultDelegate), Z_Construct_UDelegateFunction_MagicLeapCamera_MagicLeapCameraStopRecording__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapCameraFunctionLibrary_eventStopRecordingAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapCameraFunctionLibrary_eventStopRecordingAsync_Parms), &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Camera Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Stops the recording and saves the video/audio data to an mp4 file.\n\x09\x09@note The newly created mp4 file will have an automatically generated name which is guaranteed\n\x09\x09\x09  to be unique.\n\x09\x09@param ResultDelegate The delegate to be notified once the video/audio data has been saved to an mp4 file.\n\x09\x09@return True if the call succeeds, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "Stops the recording and saves the video/audio data to an mp4 file.\n@note The newly created mp4 file will have an automatically generated name which is guaranteed\n          to be unique.\n@param ResultDelegate The delegate to be notified once the video/audio data has been saved to an mp4 file.\n@return True if the call succeeds, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, nullptr, "StopRecordingAsync", nullptr, nullptr, sizeof(MagicLeapCameraFunctionLibrary_eventStopRecordingAsync_Parms), Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_NoRegister()
	{
		return UMagicLeapCameraFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraConnect, "CameraConnect" }, // 470005143
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CameraDisconnect, "CameraDisconnect" }, // 2593301544
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToFileAsync, "CaptureImageToFileAsync" }, // 4170307172
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_CaptureImageToTextureAsync, "CaptureImageToTextureAsync" }, // 1369996369
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_IsCapturing, "IsCapturing" }, // 3058465367
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_SetLogDelegate, "SetLogDelegate" }, // 388405799
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StartRecordingAsync, "StartRecordingAsync" }, // 2541636214
		{ &Z_Construct_UFunction_UMagicLeapCameraFunctionLibrary_StopRecordingAsync, "StopRecordingAsync" }, // 2682042377
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n  The MagicLeapCameraFunctionLibrary provides access to the camera capture functionality.\n  Users of this library are able to asynchronously capture camera images and footage to file.\n  Alternatively, a camera image can be captured directly to texture.  The user need only make\n  the relevant asynchronous call and then register the appropriate event handlers for the\n  operation's completion.\n*/" },
		{ "IncludePath", "MagicLeapCameraFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapCameraFunctionLibrary.h" },
		{ "ToolTip", "The MagicLeapCameraFunctionLibrary provides access to the camera capture functionality.\nUsers of this library are able to asynchronously capture camera images and footage to file.\nAlternatively, a camera image can be captured directly to texture.  The user need only make\nthe relevant asynchronous call and then register the appropriate event handlers for the\noperation's completion." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapCameraFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapCameraFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapCameraFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapCameraFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapCameraFunctionLibrary, 1118878119);
	template<> MAGICLEAPCAMERA_API UClass* StaticClass<UMagicLeapCameraFunctionLibrary>()
	{
		return UMagicLeapCameraFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapCameraFunctionLibrary(Z_Construct_UClass_UMagicLeapCameraFunctionLibrary, &UMagicLeapCameraFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapCamera"), TEXT("UMagicLeapCameraFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapCameraFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
