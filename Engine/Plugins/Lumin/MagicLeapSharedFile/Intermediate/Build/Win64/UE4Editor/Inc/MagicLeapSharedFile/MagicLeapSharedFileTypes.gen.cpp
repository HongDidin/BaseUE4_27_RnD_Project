// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapSharedFileTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapSharedFileTypes() {}
// Cross Module References
	MAGICLEAPSHAREDFILE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapSharedFile();
	MAGICLEAPSHAREDFILE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature();
	MAGICLEAPSHAREDFILE_API UScriptStruct* Z_Construct_UScriptStruct_FDummyMLSharedFileStruct();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegateMulti_Parms
		{
			TArray<FString> PickedFiles;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PickedFiles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PickedFiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PickedFiles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles_Inner = { "PickedFiles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles = { "PickedFiles", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegateMulti_Parms, PickedFiles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::NewProp_PickedFiles,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapSharedFileTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapSharedFile, nullptr, "MagicLeapFilesPickedResultDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegate_Parms
		{
			TArray<FString> PickedFiles;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PickedFiles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PickedFiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PickedFiles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles_Inner = { "PickedFiles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles = { "PickedFiles", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegate_Parms, PickedFiles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::NewProp_PickedFiles,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * Delegate used to convey the result of a file pick operation. \n * @param List of file names the user picked to provide access to this app. The file names can then be used to read the file using the IMagicLeapSharedFilePlugin::SharedFileRead() function.\n */" },
		{ "ModuleRelativePath", "Public/MagicLeapSharedFileTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of a file pick operation.\n@param List of file names the user picked to provide access to this app. The file names can then be used to read the file using the IMagicLeapSharedFilePlugin::SharedFileRead() function." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapSharedFile, nullptr, "MagicLeapFilesPickedResultDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FDummyMLSharedFileStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPSHAREDFILE_API uint32 Get_Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDummyMLSharedFileStruct, Z_Construct_UPackage__Script_MagicLeapSharedFile(), TEXT("DummyMLSharedFileStruct"), sizeof(FDummyMLSharedFileStruct), Get_Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPSHAREDFILE_API UScriptStruct* StaticStruct<FDummyMLSharedFileStruct>()
{
	return FDummyMLSharedFileStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDummyMLSharedFileStruct(FDummyMLSharedFileStruct::StaticStruct, TEXT("/Script/MagicLeapSharedFile"), TEXT("DummyMLSharedFileStruct"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapSharedFile_StaticRegisterNativesFDummyMLSharedFileStruct
{
	FScriptStruct_MagicLeapSharedFile_StaticRegisterNativesFDummyMLSharedFileStruct()
	{
		UScriptStruct::DeferCppStructOps<FDummyMLSharedFileStruct>(FName(TEXT("DummyMLSharedFileStruct")));
	}
} ScriptStruct_MagicLeapSharedFile_StaticRegisterNativesFDummyMLSharedFileStruct;
	struct Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapSharedFileTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDummyMLSharedFileStruct>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapSharedFile,
		nullptr,
		&NewStructOps,
		"DummyMLSharedFileStruct",
		sizeof(FDummyMLSharedFileStruct),
		alignof(FDummyMLSharedFileStruct),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDummyMLSharedFileStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapSharedFile();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DummyMLSharedFileStruct"), sizeof(FDummyMLSharedFileStruct), Get_Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Hash() { return 2081149974U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
