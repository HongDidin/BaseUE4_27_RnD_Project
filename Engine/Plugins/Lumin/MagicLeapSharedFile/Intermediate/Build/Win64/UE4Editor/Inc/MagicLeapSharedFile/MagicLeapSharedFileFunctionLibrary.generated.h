// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPSHAREDFILE_MagicLeapSharedFileFunctionLibrary_generated_h
#error "MagicLeapSharedFileFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapSharedFileFunctionLibrary.h"
#endif
#define MAGICLEAPSHAREDFILE_MagicLeapSharedFileFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSharedFilePickAsync); \
	DECLARE_FUNCTION(execSharedFileListAccessibleFiles);


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSharedFilePickAsync); \
	DECLARE_FUNCTION(execSharedFileListAccessibleFiles);


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapSharedFileFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapSharedFileFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapSharedFile"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapSharedFileFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapSharedFileFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapSharedFileFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapSharedFile"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapSharedFileFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapSharedFileFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapSharedFileFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapSharedFileFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapSharedFileFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapSharedFileFunctionLibrary(UMagicLeapSharedFileFunctionLibrary&&); \
	NO_API UMagicLeapSharedFileFunctionLibrary(const UMagicLeapSharedFileFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapSharedFileFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapSharedFileFunctionLibrary(UMagicLeapSharedFileFunctionLibrary&&); \
	NO_API UMagicLeapSharedFileFunctionLibrary(const UMagicLeapSharedFileFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapSharedFileFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapSharedFileFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapSharedFileFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_9_PROLOG
#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_INCLASS \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPSHAREDFILE_API UClass* StaticClass<class UMagicLeapSharedFileFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
