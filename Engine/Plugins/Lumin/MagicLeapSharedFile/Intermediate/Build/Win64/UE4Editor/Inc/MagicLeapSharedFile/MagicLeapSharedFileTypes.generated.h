// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPSHAREDFILE_MagicLeapSharedFileTypes_generated_h
#error "MagicLeapSharedFileTypes.generated.h already included, missing '#pragma once' in MagicLeapSharedFileTypes.h"
#endif
#define MAGICLEAPSHAREDFILE_MagicLeapSharedFileTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileTypes_h_10_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDummyMLSharedFileStruct_Statics; \
	MAGICLEAPSHAREDFILE_API static class UScriptStruct* StaticStruct();


template<> MAGICLEAPSHAREDFILE_API UScriptStruct* StaticStruct<struct FDummyMLSharedFileStruct>();

#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileTypes_h_18_DELEGATE \
struct _Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegateMulti_Parms \
{ \
	TArray<FString> PickedFiles; \
}; \
static inline void FMagicLeapFilesPickedResultDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapFilesPickedResultDelegateMulti, TArray<FString> const& PickedFiles) \
{ \
	_Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegateMulti_Parms Parms; \
	Parms.PickedFiles=PickedFiles; \
	MagicLeapFilesPickedResultDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileTypes_h_17_DELEGATE \
struct _Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegate_Parms \
{ \
	TArray<FString> PickedFiles; \
}; \
static inline void FMagicLeapFilesPickedResultDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapFilesPickedResultDelegate, TArray<FString> const& PickedFiles) \
{ \
	_Script_MagicLeapSharedFile_eventMagicLeapFilesPickedResultDelegate_Parms Parms; \
	Parms.PickedFiles=PickedFiles; \
	MagicLeapFilesPickedResultDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapSharedFile_Source_Public_MagicLeapSharedFileTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
