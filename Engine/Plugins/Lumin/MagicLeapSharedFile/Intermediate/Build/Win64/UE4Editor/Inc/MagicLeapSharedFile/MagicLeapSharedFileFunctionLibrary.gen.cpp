// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapSharedFileFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapSharedFileFunctionLibrary() {}
// Cross Module References
	MAGICLEAPSHAREDFILE_API UClass* Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_NoRegister();
	MAGICLEAPSHAREDFILE_API UClass* Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapSharedFile();
	MAGICLEAPSHAREDFILE_API UFunction* Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapSharedFileFunctionLibrary::execSharedFilePickAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_InResultDelegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapSharedFileFunctionLibrary::SharedFilePickAsync(FMagicLeapFilesPickedResultDelegate(Z_Param_Out_InResultDelegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapSharedFileFunctionLibrary::execSharedFileListAccessibleFiles)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutSharedFileList);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapSharedFileFunctionLibrary::SharedFileListAccessibleFiles(Z_Param_Out_OutSharedFileList);
		P_NATIVE_END;
	}
	void UMagicLeapSharedFileFunctionLibrary::StaticRegisterNativesUMagicLeapSharedFileFunctionLibrary()
	{
		UClass* Class = UMagicLeapSharedFileFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SharedFileListAccessibleFiles", &UMagicLeapSharedFileFunctionLibrary::execSharedFileListAccessibleFiles },
			{ "SharedFilePickAsync", &UMagicLeapSharedFileFunctionLibrary::execSharedFilePickAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics
	{
		struct MagicLeapSharedFileFunctionLibrary_eventSharedFileListAccessibleFiles_Parms
		{
			TArray<FString> OutSharedFileList;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutSharedFileList_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutSharedFileList;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_OutSharedFileList_Inner = { "OutSharedFileList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_OutSharedFileList = { "OutSharedFileList", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapSharedFileFunctionLibrary_eventSharedFileListAccessibleFiles_Parms, OutSharedFileList), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapSharedFileFunctionLibrary_eventSharedFileListAccessibleFiles_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapSharedFileFunctionLibrary_eventSharedFileListAccessibleFiles_Parms), &Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_OutSharedFileList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_OutSharedFileList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::Function_MetaDataParams[] = {
		{ "Category", "SharedFile Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Get the names of the files that the application has access to.\n\x09\x09The application can then use the file names and read them with the IMagicLeapSharedFilePlugin::SharedFileRead() function.\n\x09\x09@param OutSharedFileList Output param containing list of file names this app has acces to.\n\x09\x09@return true if function call succeeded and output param is valid, false otherwise\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapSharedFileFunctionLibrary.h" },
		{ "ToolTip", "Get the names of the files that the application has access to.\nThe application can then use the file names and read them with the IMagicLeapSharedFilePlugin::SharedFileRead() function.\n@param OutSharedFileList Output param containing list of file names this app has acces to.\n@return true if function call succeeded and output param is valid, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary, nullptr, "SharedFileListAccessibleFiles", nullptr, nullptr, sizeof(MagicLeapSharedFileFunctionLibrary_eventSharedFileListAccessibleFiles_Parms), Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics
	{
		struct MagicLeapSharedFileFunctionLibrary_eventSharedFilePickAsync_Parms
		{
			FScriptDelegate InResultDelegate;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_InResultDelegate;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_InResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_InResultDelegate = { "InResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapSharedFileFunctionLibrary_eventSharedFilePickAsync_Parms, InResultDelegate), Z_Construct_UDelegateFunction_MagicLeapSharedFile_MagicLeapFilesPickedResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_InResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_InResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapSharedFileFunctionLibrary_eventSharedFilePickAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapSharedFileFunctionLibrary_eventSharedFilePickAsync_Parms), &Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_InResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "SharedFile Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Let the app get access to the user's shared files from the common storage location.  This API will pop up a System UI\n\x09\x09""dialogue box with a file picker through which the users can pick files they wants to let the app have access to.  The list\n\x09\x09of selected file names will be returned to the app via the delegate.\n\x09\x09@param InResultDelegate Delegate to be called when the user finishes picking files.\n\x09\x09@return true if call to invoke the file picker succeeded, false otherwise\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapSharedFileFunctionLibrary.h" },
		{ "ToolTip", "Let the app get access to the user's shared files from the common storage location.  This API will pop up a System UI\ndialogue box with a file picker through which the users can pick files they wants to let the app have access to.  The list\nof selected file names will be returned to the app via the delegate.\n@param InResultDelegate Delegate to be called when the user finishes picking files.\n@return true if call to invoke the file picker succeeded, false otherwise" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary, nullptr, "SharedFilePickAsync", nullptr, nullptr, sizeof(MagicLeapSharedFileFunctionLibrary_eventSharedFilePickAsync_Parms), Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_NoRegister()
	{
		return UMagicLeapSharedFileFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapSharedFile,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFileListAccessibleFiles, "SharedFileListAccessibleFiles" }, // 655343588
		{ &Z_Construct_UFunction_UMagicLeapSharedFileFunctionLibrary_SharedFilePickAsync, "SharedFilePickAsync" }, // 614318112
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "IncludePath", "MagicLeapSharedFileFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapSharedFileFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapSharedFileFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapSharedFileFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapSharedFileFunctionLibrary, 701750030);
	template<> MAGICLEAPSHAREDFILE_API UClass* StaticClass<UMagicLeapSharedFileFunctionLibrary>()
	{
		return UMagicLeapSharedFileFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapSharedFileFunctionLibrary(Z_Construct_UClass_UMagicLeapSharedFileFunctionLibrary, &UMagicLeapSharedFileFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapSharedFile"), TEXT("UMagicLeapSharedFileFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapSharedFileFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
