// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapLocation/Public/MagicLeapLocationComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapLocationComponent() {}
// Cross Module References
	MAGICLEAPLOCATION_API UClass* Z_Construct_UClass_UMagicLeapLocationComponent_NoRegister();
	MAGICLEAPLOCATION_API UClass* Z_Construct_UClass_UMagicLeapLocationComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapLocation();
	MAGICLEAPLOCATION_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapLocationData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapLocationComponent::execGetLastLocationOnSphereAsync)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InRadius);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetLastLocationOnSphereAsync(Z_Param_InRadius,Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapLocationComponent::execGetLastLocationOnSphere)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InRadius);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutLocation);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetLastLocationOnSphere(Z_Param_InRadius,Z_Param_Out_OutLocation,Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapLocationComponent::execGetLastLocationAsync)
	{
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetLastLocationAsync(Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapLocationComponent::execGetLastLocation)
	{
		P_GET_STRUCT_REF(FMagicLeapLocationData,Z_Param_Out_OutLocation);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetLastLocation(Z_Param_Out_OutLocation,Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	void UMagicLeapLocationComponent::StaticRegisterNativesUMagicLeapLocationComponent()
	{
		UClass* Class = UMagicLeapLocationComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetLastLocation", &UMagicLeapLocationComponent::execGetLastLocation },
			{ "GetLastLocationAsync", &UMagicLeapLocationComponent::execGetLastLocationAsync },
			{ "GetLastLocationOnSphere", &UMagicLeapLocationComponent::execGetLastLocationOnSphere },
			{ "GetLastLocationOnSphereAsync", &UMagicLeapLocationComponent::execGetLastLocationOnSphereAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics
	{
		struct MagicLeapLocationComponent_eventGetLastLocation_Parms
		{
			FMagicLeapLocationData OutLocation;
			bool bUseFineLocation;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutLocation;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_OutLocation = { "OutLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationComponent_eventGetLastLocation_Parms, OutLocation), Z_Construct_UScriptStruct_FMagicLeapLocationData, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocation_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocation_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocation_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocation_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_OutLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve the latitude, longitude and postcode of the device.\n\x09\x09@param OutLocation If successful this will contain the latitude, longitude and postcode of the device.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location data is valid, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
		{ "ToolTip", "Attempts to retrieve the latitude, longitude and postcode of the device.\n@param OutLocation If successful this will contain the latitude, longitude and postcode of the device.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location data is valid, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationComponent, nullptr, "GetLastLocation", nullptr, nullptr, sizeof(MagicLeapLocationComponent_eventGetLastLocation_Parms), Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics
	{
		struct MagicLeapLocationComponent_eventGetLastLocationAsync_Parms
		{
			bool bUseFineLocation;
			bool ReturnValue;
		};
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocationAsync_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocationAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocationAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocationAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve the latitude, longitude and postcode of the device asynchronously.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location is immediately resolved, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
		{ "ToolTip", "Attempts to retrieve the latitude, longitude and postcode of the device asynchronously.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location is immediately resolved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationComponent, nullptr, "GetLastLocationAsync", nullptr, nullptr, sizeof(MagicLeapLocationComponent_eventGetLastLocationAsync_Parms), Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics
	{
		struct MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms
		{
			float InRadius;
			FVector OutLocation;
			bool bUseFineLocation;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InRadius;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutLocation;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_InRadius = { "InRadius", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms, InRadius), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_OutLocation = { "OutLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms, OutLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_InRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_OutLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve a point on a sphere representing the location of the device.\n\x09\x09@param InRadius The radius of the sphere that the location will be projected onto.\n\x09\x09@param OutLocation If successful this will be a valid point on a sphere representing the location of the device.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location is valid, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
		{ "ToolTip", "Attempts to retrieve a point on a sphere representing the location of the device.\n@param InRadius The radius of the sphere that the location will be projected onto.\n@param OutLocation If successful this will be a valid point on a sphere representing the location of the device.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location is valid, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationComponent, nullptr, "GetLastLocationOnSphere", nullptr, nullptr, sizeof(MagicLeapLocationComponent_eventGetLastLocationOnSphere_Parms), Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics
	{
		struct MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms
		{
			float InRadius;
			bool bUseFineLocation;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InRadius;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_InRadius = { "InRadius", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms, InRadius), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_InRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve a point on a sphere representing the location of the device asynchronously.\n\x09\x09@param InRadius The radius of the sphere that the location will be projected onto.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location is immediately resolved, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
		{ "ToolTip", "Attempts to retrieve a point on a sphere representing the location of the device asynchronously.\n@param InRadius The radius of the sphere that the location will be projected onto.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location is immediately resolved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationComponent, nullptr, "GetLastLocationOnSphereAsync", nullptr, nullptr, sizeof(MagicLeapLocationComponent_eventGetLastLocationOnSphereAsync_Parms), Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapLocationComponent_NoRegister()
	{
		return UMagicLeapLocationComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapLocationComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnGotLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGotLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnGotLocationOnSphere_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGotLocationOnSphere;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapLocationComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapLocation,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapLocationComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocation, "GetLastLocation" }, // 2548770146
		{ &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationAsync, "GetLastLocationAsync" }, // 2956364205
		{ &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphere, "GetLastLocationOnSphere" }, // 164903099
		{ &Z_Construct_UFunction_UMagicLeapLocationComponent_GetLastLocationOnSphereAsync, "GetLastLocationOnSphereAsync" }, // 2908985063
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapLocationComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09""Component that provides access to the Location API functionality.\n*/" },
		{ "IncludePath", "MagicLeapLocationComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
		{ "ToolTip", "Component that provides access to the Location API functionality." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocation_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "// Delegate instances\n" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
		{ "ToolTip", "Delegate instances" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocation = { "OnGotLocation", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapLocationComponent, OnGotLocation), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocationOnSphere_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Location | MagicLeap" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocationOnSphere = { "OnGotLocationOnSphere", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapLocationComponent, OnGotLocationOnSphere), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocationOnSphere_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocationOnSphere_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapLocationComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapLocationComponent_Statics::NewProp_OnGotLocationOnSphere,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapLocationComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapLocationComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapLocationComponent_Statics::ClassParams = {
		&UMagicLeapLocationComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapLocationComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapLocationComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapLocationComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapLocationComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapLocationComponent, 3297050073);
	template<> MAGICLEAPLOCATION_API UClass* StaticClass<UMagicLeapLocationComponent>()
	{
		return UMagicLeapLocationComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapLocationComponent(Z_Construct_UClass_UMagicLeapLocationComponent, &UMagicLeapLocationComponent::StaticClass, TEXT("/Script/MagicLeapLocation"), TEXT("UMagicLeapLocationComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapLocationComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
