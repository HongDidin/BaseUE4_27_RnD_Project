// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FMagicLeapLocationData;
#ifdef MAGICLEAPLOCATION_MagicLeapLocationTypes_generated_h
#error "MagicLeapLocationTypes.generated.h already included, missing '#pragma once' in MagicLeapLocationTypes.h"
#endif
#define MAGICLEAPLOCATION_MagicLeapLocationTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationTypes_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPLOCATION_API UScriptStruct* StaticStruct<struct FMagicLeapLocationData>();

#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationTypes_h_49_DELEGATE \
struct _Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms \
{ \
	FVector LocationOnSphere; \
	bool bSuccess; \
}; \
static inline void FMagicLeapLocationOnSphereResultDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapLocationOnSphereResultDelegateMulti, FVector const& LocationOnSphere, bool bSuccess) \
{ \
	_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms Parms; \
	Parms.LocationOnSphere=LocationOnSphere; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapLocationOnSphereResultDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationTypes_h_48_DELEGATE \
struct _Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms \
{ \
	FVector LocationOnSphere; \
	bool bSuccess; \
}; \
static inline void FMagicLeapLocationOnSphereResultDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapLocationOnSphereResultDelegate, FVector const& LocationOnSphere, bool bSuccess) \
{ \
	_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms Parms; \
	Parms.LocationOnSphere=LocationOnSphere; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapLocationOnSphereResultDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationTypes_h_45_DELEGATE \
struct _Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms \
{ \
	FMagicLeapLocationData LocationData; \
	bool bSuccess; \
}; \
static inline void FMagicLeapLocationResultDelegateMulti_DelegateWrapper(const FMulticastScriptDelegate& MagicLeapLocationResultDelegateMulti, FMagicLeapLocationData const& LocationData, bool bSuccess) \
{ \
	_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms Parms; \
	Parms.LocationData=LocationData; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapLocationResultDelegateMulti.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationTypes_h_44_DELEGATE \
struct _Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms \
{ \
	FMagicLeapLocationData LocationData; \
	bool bSuccess; \
}; \
static inline void FMagicLeapLocationResultDelegate_DelegateWrapper(const FScriptDelegate& MagicLeapLocationResultDelegate, FMagicLeapLocationData const& LocationData, bool bSuccess) \
{ \
	_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms Parms; \
	Parms.LocationData=LocationData; \
	Parms.bSuccess=bSuccess ? true : false; \
	MagicLeapLocationResultDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationTypes_h


#define FOREACH_ENUM_EMAGICLEAPLOCATIONRESULT(op) \
	op(EMagicLeapLocationResult::Unknown) \
	op(EMagicLeapLocationResult::NoNetworkConnection) \
	op(EMagicLeapLocationResult::NoLocation) \
	op(EMagicLeapLocationResult::ProviderNotFound) 

enum class EMagicLeapLocationResult : uint8;
template<> MAGICLEAPLOCATION_API UEnum* StaticEnum<EMagicLeapLocationResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
