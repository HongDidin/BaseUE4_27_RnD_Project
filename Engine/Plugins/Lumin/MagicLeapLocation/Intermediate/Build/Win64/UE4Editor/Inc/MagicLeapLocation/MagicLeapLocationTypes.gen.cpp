// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapLocation/Public/MagicLeapLocationTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapLocationTypes() {}
// Cross Module References
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MagicLeapLocation();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature();
	MAGICLEAPLOCATION_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapLocationData();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature();
	MAGICLEAPLOCATION_API UEnum* Z_Construct_UEnum_MagicLeapLocation_EMagicLeapLocationResult();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms
		{
			FVector LocationOnSphere;
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationOnSphere_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationOnSphere;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationOnSphere_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationOnSphere = { "LocationOnSphere", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms, LocationOnSphere), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationOnSphere_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationOnSphere_MetaData)) };
	void Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationOnSphere,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapLocation, nullptr, "MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms
		{
			FVector LocationOnSphere;
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationOnSphere_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationOnSphere;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_LocationOnSphere_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_LocationOnSphere = { "LocationOnSphere", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms, LocationOnSphere), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_LocationOnSphere_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_LocationOnSphere_MetaData)) };
	void Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms), &Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_LocationOnSphere,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to convey the result of a coarse location on sphere query. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of a coarse location on sphere query." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapLocation, nullptr, "MagicLeapLocationOnSphereResultDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationOnSphereResultDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics
	{
		struct _Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms
		{
			FMagicLeapLocationData LocationData;
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationData;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationData = { "LocationData", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms, LocationData), Z_Construct_UScriptStruct_FMagicLeapLocationData, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationData_MetaData)) };
	void Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms), &Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_LocationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapLocation, nullptr, "MagicLeapLocationResultDelegateMulti__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegateMulti_Parms), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegateMulti__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics
	{
		struct _Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms
		{
			FMagicLeapLocationData LocationData;
			bool bSuccess;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationData;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_LocationData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_LocationData = { "LocationData", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms, LocationData), Z_Construct_UScriptStruct_FMagicLeapLocationData, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_LocationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_LocationData_MetaData)) };
	void Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms), &Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_LocationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Delegate used to convey the result of a coarse location query. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "Delegate used to convey the result of a coarse location query." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MagicLeapLocation, nullptr, "MagicLeapLocationResultDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_MagicLeapLocation_eventMagicLeapLocationResultDelegate_Parms), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMagicLeapLocationResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapLocation_EMagicLeapLocationResult, Z_Construct_UPackage__Script_MagicLeapLocation(), TEXT("EMagicLeapLocationResult"));
		}
		return Singleton;
	}
	template<> MAGICLEAPLOCATION_API UEnum* StaticEnum<EMagicLeapLocationResult>()
	{
		return EMagicLeapLocationResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapLocationResult(EMagicLeapLocationResult_StaticEnum, TEXT("/Script/MagicLeapLocation"), TEXT("EMagicLeapLocationResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapLocation_EMagicLeapLocationResult_Hash() { return 559261884U; }
	UEnum* Z_Construct_UEnum_MagicLeapLocation_EMagicLeapLocationResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapLocation();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapLocationResult"), 0, Get_Z_Construct_UEnum_MagicLeapLocation_EMagicLeapLocationResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapLocationResult::Unknown", (int64)EMagicLeapLocationResult::Unknown },
				{ "EMagicLeapLocationResult::NoNetworkConnection", (int64)EMagicLeapLocationResult::NoNetworkConnection },
				{ "EMagicLeapLocationResult::NoLocation", (int64)EMagicLeapLocationResult::NoLocation },
				{ "EMagicLeapLocationResult::ProviderNotFound", (int64)EMagicLeapLocationResult::ProviderNotFound },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
				{ "NoLocation.Comment", "/** No location data received. */" },
				{ "NoLocation.Name", "EMagicLeapLocationResult::NoLocation" },
				{ "NoLocation.ToolTip", "No location data received." },
				{ "NoNetworkConnection.Comment", "/** No connection to server. */" },
				{ "NoNetworkConnection.Name", "EMagicLeapLocationResult::NoNetworkConnection" },
				{ "NoNetworkConnection.ToolTip", "No connection to server." },
				{ "ProviderNotFound.Comment", "/** Location provider is not found. */" },
				{ "ProviderNotFound.Name", "EMagicLeapLocationResult::ProviderNotFound" },
				{ "ProviderNotFound.ToolTip", "Location provider is not found." },
				{ "Unknown.Comment", "/** Unknown location error. */" },
				{ "Unknown.Name", "EMagicLeapLocationResult::Unknown" },
				{ "Unknown.ToolTip", "Unknown location error." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapLocation,
				nullptr,
				"EMagicLeapLocationResult",
				"EMagicLeapLocationResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMagicLeapLocationData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPLOCATION_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapLocationData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapLocationData, Z_Construct_UPackage__Script_MagicLeapLocation(), TEXT("MagicLeapLocationData"), sizeof(FMagicLeapLocationData), Get_Z_Construct_UScriptStruct_FMagicLeapLocationData_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPLOCATION_API UScriptStruct* StaticStruct<FMagicLeapLocationData>()
{
	return FMagicLeapLocationData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapLocationData(FMagicLeapLocationData::StaticStruct, TEXT("/Script/MagicLeapLocation"), TEXT("MagicLeapLocationData"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapLocation_StaticRegisterNativesFMagicLeapLocationData
{
	FScriptStruct_MagicLeapLocation_StaticRegisterNativesFMagicLeapLocationData()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapLocationData>(FName(TEXT("MagicLeapLocationData")));
	}
} ScriptStruct_MagicLeapLocation_StaticRegisterNativesFMagicLeapLocationData;
	struct Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Latitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Latitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Longitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Longitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostalCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PostalCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Accuracy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Accuracy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Location request result. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "Location request result." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapLocationData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Latitude_MetaData[] = {
		{ "Category", "Location|MagicLeap" },
		{ "Comment", "/** Location latitude. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "Location latitude." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Latitude = { "Latitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapLocationData, Latitude), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Latitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Latitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Longitude_MetaData[] = {
		{ "Category", "Location|MagicLeap" },
		{ "Comment", "/** Location longitude. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "Location longitude." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Longitude = { "Longitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapLocationData, Longitude), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Longitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Longitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_PostalCode_MetaData[] = {
		{ "Category", "Location|MagicLeap" },
		{ "Comment", "/** Approximate postal code. Remains blank if not provided. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "Approximate postal code. Remains blank if not provided." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_PostalCode = { "PostalCode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapLocationData, PostalCode), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_PostalCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_PostalCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Accuracy_MetaData[] = {
		{ "Category", "Location|MagicLeap" },
		{ "Comment", "/** The degree of accuracy in Unreal Units (typically centimeters). Set to -1.0f if not provided. */" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationTypes.h" },
		{ "ToolTip", "The degree of accuracy in Unreal Units (typically centimeters). Set to -1.0f if not provided." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Accuracy = { "Accuracy", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapLocationData, Accuracy), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Accuracy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Accuracy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Latitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Longitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_PostalCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::NewProp_Accuracy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapLocation,
		nullptr,
		&NewStructOps,
		"MagicLeapLocationData",
		sizeof(FMagicLeapLocationData),
		alignof(FMagicLeapLocationData),
		Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapLocationData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapLocationData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapLocation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapLocationData"), sizeof(FMagicLeapLocationData), Get_Z_Construct_UScriptStruct_FMagicLeapLocationData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapLocationData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapLocationData_Hash() { return 1150057908U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
