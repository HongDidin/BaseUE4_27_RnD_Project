// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MagicLeapLocation/Public/MagicLeapLocationFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapLocationFunctionLibrary() {}
// Cross Module References
	MAGICLEAPLOCATION_API UClass* Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_NoRegister();
	MAGICLEAPLOCATION_API UClass* Z_Construct_UClass_UMagicLeapLocationFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MagicLeapLocation();
	MAGICLEAPLOCATION_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapLocationData();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	MAGICLEAPLOCATION_API UFunction* Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapLocationFunctionLibrary::execGetLastLocationOnSphereAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_InResultDelegate);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InRadius);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapLocationFunctionLibrary::GetLastLocationOnSphereAsync(FMagicLeapLocationOnSphereResultDelegate(Z_Param_Out_InResultDelegate),Z_Param_InRadius,Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapLocationFunctionLibrary::execGetLastLocationOnSphere)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InRadius);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutLocation);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapLocationFunctionLibrary::GetLastLocationOnSphere(Z_Param_InRadius,Z_Param_Out_OutLocation,Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapLocationFunctionLibrary::execGetLastLocationAsync)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_InResultDelegate);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapLocationFunctionLibrary::GetLastLocationAsync(FMagicLeapLocationResultDelegate(Z_Param_Out_InResultDelegate),Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapLocationFunctionLibrary::execGetLastLocation)
	{
		P_GET_STRUCT_REF(FMagicLeapLocationData,Z_Param_Out_OutLocation);
		P_GET_UBOOL(Z_Param_bUseFineLocation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMagicLeapLocationFunctionLibrary::GetLastLocation(Z_Param_Out_OutLocation,Z_Param_bUseFineLocation);
		P_NATIVE_END;
	}
	void UMagicLeapLocationFunctionLibrary::StaticRegisterNativesUMagicLeapLocationFunctionLibrary()
	{
		UClass* Class = UMagicLeapLocationFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetLastLocation", &UMagicLeapLocationFunctionLibrary::execGetLastLocation },
			{ "GetLastLocationAsync", &UMagicLeapLocationFunctionLibrary::execGetLastLocationAsync },
			{ "GetLastLocationOnSphere", &UMagicLeapLocationFunctionLibrary::execGetLastLocationOnSphere },
			{ "GetLastLocationOnSphereAsync", &UMagicLeapLocationFunctionLibrary::execGetLastLocationOnSphereAsync },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics
	{
		struct MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms
		{
			FMagicLeapLocationData OutLocation;
			bool bUseFineLocation;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutLocation;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_OutLocation = { "OutLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms, OutLocation), Z_Construct_UScriptStruct_FMagicLeapLocationData, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_OutLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve the latitude, longitude and postcode of the device.\n\x09\x09@param OutLocation If successful this will contain the latitude, longitude and postcode of the device.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location data is valid, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationFunctionLibrary.h" },
		{ "ToolTip", "Attempts to retrieve the latitude, longitude and postcode of the device.\n@param OutLocation If successful this will contain the latitude, longitude and postcode of the device.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location data is valid, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationFunctionLibrary, nullptr, "GetLastLocation", nullptr, nullptr, sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocation_Parms), Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics
	{
		struct MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms
		{
			FScriptDelegate InResultDelegate;
			bool bUseFineLocation;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_InResultDelegate;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_InResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_InResultDelegate = { "InResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms, InResultDelegate), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_InResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_InResultDelegate_MetaData)) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_InResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve the latitude, longitude and postcode of the device asynchronously.\n\x09\x09@param The delegate to notify once the privilege has been granted.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location is immediately resolved, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationFunctionLibrary.h" },
		{ "ToolTip", "Attempts to retrieve the latitude, longitude and postcode of the device asynchronously.\n@param The delegate to notify once the privilege has been granted.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location is immediately resolved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationFunctionLibrary, nullptr, "GetLastLocationAsync", nullptr, nullptr, sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationAsync_Parms), Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics
	{
		struct MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms
		{
			float InRadius;
			FVector OutLocation;
			bool bUseFineLocation;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InRadius;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutLocation;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_InRadius = { "InRadius", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms, InRadius), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_OutLocation = { "OutLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms, OutLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_InRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_OutLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location Function Library | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve a point on a sphere representing the location of the device.\n\x09\x09@param OutLocation If successful this will be a valid point on a sphere representing the location of the device.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location is valid, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationFunctionLibrary.h" },
		{ "ToolTip", "Attempts to retrieve a point on a sphere representing the location of the device.\n@param OutLocation If successful this will be a valid point on a sphere representing the location of the device.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location is valid, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationFunctionLibrary, nullptr, "GetLastLocationOnSphere", nullptr, nullptr, sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphere_Parms), Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics
	{
		struct MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms
		{
			FScriptDelegate InResultDelegate;
			float InRadius;
			bool bUseFineLocation;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InResultDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_InResultDelegate;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InRadius;
		static void NewProp_bUseFineLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFineLocation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InResultDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InResultDelegate = { "InResultDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms, InResultDelegate), Z_Construct_UDelegateFunction_MagicLeapLocation_MagicLeapLocationOnSphereResultDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InResultDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InResultDelegate_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InRadius = { "InRadius", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms, InRadius), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms*)Obj)->bUseFineLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation = { "bUseFineLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms), &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InResultDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_InRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_bUseFineLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::Function_MetaDataParams[] = {
		{ "Category", "Location | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Attempts to retrieve a point on a sphere representing the location of the device asynchronously.\n\x09\x09@param The delegate to notify once the privilege has been granted.\n\x09\x09@param InRadius The radius of the sphere that the location will be projected onto.\n\x09\x09@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n\x09\x09@return True if the location is immediately resolved, false otherwise.\n\x09*/" },
		{ "CPP_Default_bUseFineLocation", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationFunctionLibrary.h" },
		{ "ToolTip", "Attempts to retrieve a point on a sphere representing the location of the device asynchronously.\n@param The delegate to notify once the privilege has been granted.\n@param InRadius The radius of the sphere that the location will be projected onto.\n@param bUseFineLocation Flags whether or not to request a fine or coarse location.\n@return True if the location is immediately resolved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapLocationFunctionLibrary, nullptr, "GetLastLocationOnSphereAsync", nullptr, nullptr, sizeof(MagicLeapLocationFunctionLibrary_eventGetLastLocationOnSphereAsync_Parms), Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_NoRegister()
	{
		return UMagicLeapLocationFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapLocation,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocation, "GetLastLocation" }, // 45619048
		{ &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationAsync, "GetLastLocationAsync" }, // 3747720295
		{ &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphere, "GetLastLocationOnSphere" }, // 3677661169
		{ &Z_Construct_UFunction_UMagicLeapLocationFunctionLibrary_GetLastLocationOnSphereAsync, "GetLastLocationOnSphereAsync" }, // 1085686518
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "MagicLeap" },
		{ "IncludePath", "MagicLeapLocationFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/MagicLeapLocationFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapLocationFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::ClassParams = {
		&UMagicLeapLocationFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapLocationFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapLocationFunctionLibrary, 4028747590);
	template<> MAGICLEAPLOCATION_API UClass* StaticClass<UMagicLeapLocationFunctionLibrary>()
	{
		return UMagicLeapLocationFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapLocationFunctionLibrary(Z_Construct_UClass_UMagicLeapLocationFunctionLibrary, &UMagicLeapLocationFunctionLibrary::StaticClass, TEXT("/Script/MagicLeapLocation"), TEXT("UMagicLeapLocationFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapLocationFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
