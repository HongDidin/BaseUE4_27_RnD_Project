// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FMagicLeapLocationData;
#ifdef MAGICLEAPLOCATION_MagicLeapLocationComponent_generated_h
#error "MagicLeapLocationComponent.generated.h already included, missing '#pragma once' in MagicLeapLocationComponent.h"
#endif
#define MAGICLEAPLOCATION_MagicLeapLocationComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetLastLocationOnSphereAsync); \
	DECLARE_FUNCTION(execGetLastLocationOnSphere); \
	DECLARE_FUNCTION(execGetLastLocationAsync); \
	DECLARE_FUNCTION(execGetLastLocation);


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLastLocationOnSphereAsync); \
	DECLARE_FUNCTION(execGetLastLocationOnSphere); \
	DECLARE_FUNCTION(execGetLastLocationAsync); \
	DECLARE_FUNCTION(execGetLastLocation);


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapLocationComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapLocationComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapLocationComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapLocation"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapLocationComponent)


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapLocationComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapLocationComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapLocationComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapLocation"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapLocationComponent)


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapLocationComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapLocationComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapLocationComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapLocationComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapLocationComponent(UMagicLeapLocationComponent&&); \
	NO_API UMagicLeapLocationComponent(const UMagicLeapLocationComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapLocationComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapLocationComponent(UMagicLeapLocationComponent&&); \
	NO_API UMagicLeapLocationComponent(const UMagicLeapLocationComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapLocationComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapLocationComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapLocationComponent)


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnGotLocation() { return STRUCT_OFFSET(UMagicLeapLocationComponent, OnGotLocation); } \
	FORCEINLINE static uint32 __PPO__OnGotLocationOnSphere() { return STRUCT_OFFSET(UMagicLeapLocationComponent, OnGotLocationOnSphere); }


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_13_PROLOG
#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_INCLASS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPLOCATION_API UClass* StaticClass<class UMagicLeapLocationComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
