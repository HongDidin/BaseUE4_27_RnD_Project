// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FMagicLeapLocationData;
#ifdef MAGICLEAPLOCATION_MagicLeapLocationFunctionLibrary_generated_h
#error "MagicLeapLocationFunctionLibrary.generated.h already included, missing '#pragma once' in MagicLeapLocationFunctionLibrary.h"
#endif
#define MAGICLEAPLOCATION_MagicLeapLocationFunctionLibrary_generated_h

#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetLastLocationOnSphereAsync); \
	DECLARE_FUNCTION(execGetLastLocationOnSphere); \
	DECLARE_FUNCTION(execGetLastLocationAsync); \
	DECLARE_FUNCTION(execGetLastLocation);


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLastLocationOnSphereAsync); \
	DECLARE_FUNCTION(execGetLastLocationOnSphere); \
	DECLARE_FUNCTION(execGetLastLocationAsync); \
	DECLARE_FUNCTION(execGetLastLocation);


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapLocationFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapLocationFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapLocation"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapLocationFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapLocationFunctionLibrary(); \
	friend struct Z_Construct_UClass_UMagicLeapLocationFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapLocationFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MagicLeapLocation"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapLocationFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapLocationFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapLocationFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapLocationFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapLocationFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapLocationFunctionLibrary(UMagicLeapLocationFunctionLibrary&&); \
	NO_API UMagicLeapLocationFunctionLibrary(const UMagicLeapLocationFunctionLibrary&); \
public:


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapLocationFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapLocationFunctionLibrary(UMagicLeapLocationFunctionLibrary&&); \
	NO_API UMagicLeapLocationFunctionLibrary(const UMagicLeapLocationFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapLocationFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapLocationFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapLocationFunctionLibrary)


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_9_PROLOG
#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_INCLASS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPLOCATION_API UClass* StaticClass<class UMagicLeapLocationFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapLocation_Source_MagicLeapLocation_Public_MagicLeapLocationFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
