// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapMovementTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapMovementTypes() {}
// Cross Module References
	MAGICLEAPMOVEMENT_API UEnum* Z_Construct_UEnum_MagicLeapMovement_EMagicLeapMovementType();
	UPackage* Z_Construct_UPackage__Script_MagicLeapMovement();
	MAGICLEAPMOVEMENT_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings();
	MAGICLEAPMOVEMENT_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings();
	MAGICLEAPMOVEMENT_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovementSettings();
// End Cross Module References
	static UEnum* EMagicLeapMovementType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MagicLeapMovement_EMagicLeapMovementType, Z_Construct_UPackage__Script_MagicLeapMovement(), TEXT("EMagicLeapMovementType"));
		}
		return Singleton;
	}
	template<> MAGICLEAPMOVEMENT_API UEnum* StaticEnum<EMagicLeapMovementType>()
	{
		return EMagicLeapMovementType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMagicLeapMovementType(EMagicLeapMovementType_StaticEnum, TEXT("/Script/MagicLeapMovement"), TEXT("EMagicLeapMovementType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MagicLeapMovement_EMagicLeapMovementType_Hash() { return 1413200625U; }
	UEnum* Z_Construct_UEnum_MagicLeapMovement_EMagicLeapMovementType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMovement();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMagicLeapMovementType"), 0, Get_Z_Construct_UEnum_MagicLeapMovement_EMagicLeapMovementType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMagicLeapMovementType::Controller3DOF", (int64)EMagicLeapMovementType::Controller3DOF },
				{ "EMagicLeapMovementType::Controller6DOF", (int64)EMagicLeapMovementType::Controller6DOF },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Controller3DOF.Name", "EMagicLeapMovementType::Controller3DOF" },
				{ "Controller6DOF.Name", "EMagicLeapMovementType::Controller6DOF" },
				{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MagicLeapMovement,
				nullptr,
				"EMagicLeapMovementType",
				"EMagicLeapMovementType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMagicLeapMovement6DofSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPMOVEMENT_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings, Z_Construct_UPackage__Script_MagicLeapMovement(), TEXT("MagicLeapMovement6DofSettings"), sizeof(FMagicLeapMovement6DofSettings), Get_Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPMOVEMENT_API UScriptStruct* StaticStruct<FMagicLeapMovement6DofSettings>()
{
	return FMagicLeapMovement6DofSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapMovement6DofSettings(FMagicLeapMovement6DofSettings::StaticStruct, TEXT("/Script/MagicLeapMovement"), TEXT("MagicLeapMovement6DofSettings"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovement6DofSettings
{
	FScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovement6DofSettings()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapMovement6DofSettings>(FName(TEXT("MagicLeapMovement6DofSettings")));
	}
} ScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovement6DofSettings;
	struct Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoCenter_MetaData[];
#endif
		static void NewProp_bAutoCenter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoCenter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** 6DoF specific movement settings that must be provided when starting a 3DoF movement session. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "6DoF specific movement settings that must be provided when starting a 3DoF movement session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapMovement6DofSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** If the object should automatically center on the control direction when beginning movement. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "If the object should automatically center on the control direction when beginning movement." },
	};
#endif
	void Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter_SetBit(void* Obj)
	{
		((FMagicLeapMovement6DofSettings*)Obj)->bAutoCenter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter = { "bAutoCenter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMagicLeapMovement6DofSettings), &Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::NewProp_bAutoCenter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMovement,
		nullptr,
		&NewStructOps,
		"MagicLeapMovement6DofSettings",
		sizeof(FMagicLeapMovement6DofSettings),
		alignof(FMagicLeapMovement6DofSettings),
		Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMovement();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapMovement6DofSettings"), sizeof(FMagicLeapMovement6DofSettings), Get_Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Hash() { return 252608725U; }
class UScriptStruct* FMagicLeapMovement3DofSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPMOVEMENT_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings, Z_Construct_UPackage__Script_MagicLeapMovement(), TEXT("MagicLeapMovement3DofSettings"), sizeof(FMagicLeapMovement3DofSettings), Get_Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPMOVEMENT_API UScriptStruct* StaticStruct<FMagicLeapMovement3DofSettings>()
{
	return FMagicLeapMovement3DofSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapMovement3DofSettings(FMagicLeapMovement3DofSettings::StaticStruct, TEXT("/Script/MagicLeapMovement"), TEXT("MagicLeapMovement3DofSettings"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovement3DofSettings
{
	FScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovement3DofSettings()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapMovement3DofSettings>(FName(TEXT("MagicLeapMovement3DofSettings")));
	}
} ScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovement3DofSettings;
	struct Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoCenter_MetaData[];
#endif
		static void NewProp_bAutoCenter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoCenter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** 3DoF specific movement settings that must be provided when starting a 3DoF movement session. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "3DoF specific movement settings that must be provided when starting a 3DoF movement session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapMovement3DofSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** If the object should automatically center on the control direction when beginning movement. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "If the object should automatically center on the control direction when beginning movement." },
	};
#endif
	void Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter_SetBit(void* Obj)
	{
		((FMagicLeapMovement3DofSettings*)Obj)->bAutoCenter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter = { "bAutoCenter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMagicLeapMovement3DofSettings), &Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::NewProp_bAutoCenter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMovement,
		nullptr,
		&NewStructOps,
		"MagicLeapMovement3DofSettings",
		sizeof(FMagicLeapMovement3DofSettings),
		alignof(FMagicLeapMovement3DofSettings),
		Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMovement();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapMovement3DofSettings"), sizeof(FMagicLeapMovement3DofSettings), Get_Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Hash() { return 1719292675U; }
class UScriptStruct* FMagicLeapMovementSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MAGICLEAPMOVEMENT_API uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMagicLeapMovementSettings, Z_Construct_UPackage__Script_MagicLeapMovement(), TEXT("MagicLeapMovementSettings"), sizeof(FMagicLeapMovementSettings), Get_Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Hash());
	}
	return Singleton;
}
template<> MAGICLEAPMOVEMENT_API UScriptStruct* StaticStruct<FMagicLeapMovementSettings>()
{
	return FMagicLeapMovementSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMagicLeapMovementSettings(FMagicLeapMovementSettings::StaticStruct, TEXT("/Script/MagicLeapMovement"), TEXT("MagicLeapMovementSettings"), false, nullptr, nullptr);
static struct FScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovementSettings
{
	FScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovementSettings()
	{
		UScriptStruct::DeferCppStructOps<FMagicLeapMovementSettings>(FName(TEXT("MagicLeapMovementSettings")));
	}
} ScriptStruct_MagicLeapMovement_StaticRegisterNativesFMagicLeapMovementSettings;
	struct Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MovementType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MovementType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwayHistorySize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SwayHistorySize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDeltaAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxDeltaAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ControlDampeningFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ControlDampeningFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxSwayAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxSwayAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumHeadposeRotationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumHeadposeRotationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumHeadposeMovementSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumHeadposeMovementSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumDepthDeltaForSway_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumDepthDeltaForSway;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinimumDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumSwayTimeS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumSwayTimeS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndResolveTimeoutS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EndResolveTimeoutS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxPenetrationPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxPenetrationPercentage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Settings for a movement session. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "Settings for a movement session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMagicLeapMovementSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** The movement type to use when updating the transform of the set controller. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The movement type to use when updating the transform of the set controller." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType = { "MovementType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MovementType), Z_Construct_UEnum_MagicLeapMovement_EMagicLeapMovementType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_SwayHistorySize_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMax", "100" },
		{ "ClampMin", "3" },
		{ "Comment", "/** Number of frames of sway history to track.  Increase to improve smoothing.  Minimum value of 3. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "Number of frames of sway history to track.  Increase to improve smoothing.  Minimum value of 3." },
		{ "UIMax", "100" },
		{ "UIMin", "3" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_SwayHistorySize = { "SwayHistorySize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, SwayHistorySize), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_SwayHistorySize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_SwayHistorySize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxDeltaAngle_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMax", "360.0" },
		{ "ClampMin", "1.0" },
		{ "Comment", "/**\n\x09\x09Maximum angle, in degrees, between the oldest and newest headpose to object vector.  Increasing this will increase the\n\x09\x09maximum speed of movement.  Must be greater than zero.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "Maximum angle, in degrees, between the oldest and newest headpose to object vector.  Increasing this will increase the\nmaximum speed of movement.  Must be greater than zero." },
		{ "UIMax", "360.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxDeltaAngle = { "MaxDeltaAngle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaxDeltaAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxDeltaAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxDeltaAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_ControlDampeningFactor_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "0.5" },
		{ "Comment", "/**\n\x09\x09""A unitless number that governs the smoothing of Control input.  Larger values will make the movement more twitchy,\n\x09\x09smaller values will make it smoother by increasing latency between Control input and object movement response by averaging\n\x09\x09multiple frames of input values.  Must be greater than zero.  Typical values would be between 0.5 and 10.0.  This is\n\x09\x09""defaulted to 7.0.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "A unitless number that governs the smoothing of Control input.  Larger values will make the movement more twitchy,\nsmaller values will make it smoother by increasing latency between Control input and object movement response by averaging\nmultiple frames of input values.  Must be greater than zero.  Typical values would be between 0.5 and 10.0.  This is\ndefaulted to 7.0." },
		{ "UIMin", "0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_ControlDampeningFactor = { "ControlDampeningFactor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, ControlDampeningFactor), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_ControlDampeningFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_ControlDampeningFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxSwayAngle_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMax", "360.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/**\n\x09\x09The maximum angle, in degrees, that the object will be tilted left/right and front/back.  Cannot be a negative value,\n\x09\x09""but may be zero.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The maximum angle, in degrees, that the object will be tilted left/right and front/back.  Cannot be a negative value,\nbut may be zero." },
		{ "UIMax", "360.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxSwayAngle = { "MaxSwayAngle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaxSwayAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxSwayAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxSwayAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeRotationSpeed_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMax", "360.0" },
		{ "ClampMin", "1.0" },
		{ "Comment", "/**\n\x09\x09The speed of rotation that will stop implicit depth translation from happening.  The speed of rotation about the\n\x09\x09headpose Y-axis, in degrees per second, that if exceeded, stops implicit depth translation from happening.  Must be greater\n\x09\x09than zero.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The speed of rotation that will stop implicit depth translation from happening.  The speed of rotation about the\nheadpose Y-axis, in degrees per second, that if exceeded, stops implicit depth translation from happening.  Must be greater\nthan zero." },
		{ "UIMax", "360.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeRotationSpeed = { "MaximumHeadposeRotationSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaximumHeadposeRotationSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeRotationSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeRotationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeMovementSpeed_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "75.0" },
		{ "Comment", "/**\n\x09\x09The maximum speed that headpose can move, in cm per second, that will stop implicit depth translation.  If the\n\x09\x09headpose is moving faster than this speed (meters per second) implicit depth translation doesn't happen.  Must be greater\n\x09\x09than zero.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The maximum speed that headpose can move, in cm per second, that will stop implicit depth translation.  If the\nheadpose is moving faster than this speed (meters per second) implicit depth translation doesn't happen.  Must be greater\nthan zero." },
		{ "UIMin", "75.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeMovementSpeed = { "MaximumHeadposeMovementSpeed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaximumHeadposeMovementSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeMovementSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeMovementSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDepthDeltaForSway_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "10.0" },
		{ "Comment", "/** Distance object must move in depth since the last frame to cause maximum push/pull sway.  Must be greater than zero. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "Distance object must move in depth since the last frame to cause maximum push/pull sway.  Must be greater than zero." },
		{ "UIMin", "10.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDepthDeltaForSway = { "MaximumDepthDeltaForSway", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaximumDepthDeltaForSway), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDepthDeltaForSway_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDepthDeltaForSway_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MinimumDistance_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "50.0" },
		{ "Comment", "/**\n\x09\x09The minimum distance in cm the object can be moved in depth relative to the headpose.  This must be greater than\n\x09\x09zero and less than MaximumDistance.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The minimum distance in cm the object can be moved in depth relative to the headpose.  This must be greater than\nzero and less than MaximumDistance." },
		{ "UIMin", "50.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MinimumDistance = { "MinimumDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MinimumDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MinimumDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MinimumDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDistance_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "60.0" },
		{ "Comment", "/**\n\x09\x09The maximum distance in cm the object can be moved in depth relative to the headpose.  This must be greater than\n\x09\x09zero and MinimumDistance.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The maximum distance in cm the object can be moved in depth relative to the headpose.  This must be greater than\nzero and MinimumDistance." },
		{ "UIMin", "60.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDistance = { "MaximumDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaximumDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumSwayTimeS_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "0.15" },
		{ "Comment", "/**\n\x09\x09Maximum length of time, in seconds, lateral sway should take to decay.  Maximum length of time (in seconds) lateral sway\n\x09\x09should take to decay back to an upright orientation once lateral movement stops.  Defaults to 0.15, must be greater\n\x09\x09than zero.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "Maximum length of time, in seconds, lateral sway should take to decay.  Maximum length of time (in seconds) lateral sway\nshould take to decay back to an upright orientation once lateral movement stops.  Defaults to 0.15, must be greater\nthan zero." },
		{ "UIMin", "0.15" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumSwayTimeS = { "MaximumSwayTimeS", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaximumSwayTimeS), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumSwayTimeS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumSwayTimeS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_EndResolveTimeoutS_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/**\n\x09\x09Maximum length of time, in seconds, to allow DetatchObject() to resolve before forcefully aborting.  This serves as a\n\x09\x09""fail-safe for instances where the object is in a bad position and can't resolve to a safe position.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "Maximum length of time, in seconds, to allow DetatchObject() to resolve before forcefully aborting.  This serves as a\nfail-safe for instances where the object is in a bad position and can't resolve to a safe position." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_EndResolveTimeoutS = { "EndResolveTimeoutS", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, EndResolveTimeoutS), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_EndResolveTimeoutS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_EndResolveTimeoutS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxPenetrationPercentage_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/**\n\x09\x09The percentage (0 to 1) of the moved object's radius that can penetrate a colliding object.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementTypes.h" },
		{ "ToolTip", "The percentage (0 to 1) of the moved object's radius that can penetrate a colliding object." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxPenetrationPercentage = { "MaxPenetrationPercentage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMagicLeapMovementSettings, MaxPenetrationPercentage), METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxPenetrationPercentage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxPenetrationPercentage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MovementType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_SwayHistorySize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxDeltaAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_ControlDampeningFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxSwayAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeRotationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumHeadposeMovementSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDepthDeltaForSway,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MinimumDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaximumSwayTimeS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_EndResolveTimeoutS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::NewProp_MaxPenetrationPercentage,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMovement,
		nullptr,
		&NewStructOps,
		"MagicLeapMovementSettings",
		sizeof(FMagicLeapMovementSettings),
		alignof(FMagicLeapMovementSettings),
		Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovementSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MagicLeapMovement();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MagicLeapMovementSettings"), sizeof(FMagicLeapMovementSettings), Get_Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Hash() { return 2274768121U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
