// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/MagicLeapMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMagicLeapMovementComponent() {}
// Cross Module References
	MAGICLEAPMOVEMENT_API UClass* Z_Construct_UClass_UMagicLeapMovementComponent_NoRegister();
	MAGICLEAPMOVEMENT_API UClass* Z_Construct_UClass_UMagicLeapMovementComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMovementComponent();
	UPackage* Z_Construct_UPackage__Script_MagicLeapMovement();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	MAGICLEAPMOVEMENT_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovementSettings();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	MAGICLEAPMOVEMENT_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings();
	MAGICLEAPMOVEMENT_API UScriptStruct* Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings();
// End Cross Module References
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execOnUpdatedComponentOverlapEnd)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_Other);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnUpdatedComponentOverlapEnd(Z_Param_OverlappedComp,Z_Param_Other,Z_Param_OtherComp,Z_Param_OtherBodyIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execOnUpdatedComponentOverlapBegin)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_Other);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnUpdatedComponentOverlapBegin(Z_Param_OverlappedComp,Z_Param_Other,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execChangeRotation)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaDegrees);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ChangeRotation(Z_Param_DeltaDegrees);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execChangeDepth)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaDepth);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ChangeDepth(Z_Param_DeltaDepth);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execGetDefaultSettings)
	{
		P_GET_STRUCT_REF(FMagicLeapMovementSettings,Z_Param_Out_OutSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetDefaultSettings(Z_Param_Out_OutSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execDetachObject)
	{
		P_GET_UBOOL(Z_Param_bResolvePendingSoftCollisions);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DetachObject(Z_Param_bResolvePendingSoftCollisions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMagicLeapMovementComponent::execAttachObjectToMovementController)
	{
		P_GET_OBJECT(USceneComponent,Z_Param_InMovementController);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AttachObjectToMovementController(Z_Param_InMovementController);
		P_NATIVE_END;
	}
	void UMagicLeapMovementComponent::StaticRegisterNativesUMagicLeapMovementComponent()
	{
		UClass* Class = UMagicLeapMovementComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AttachObjectToMovementController", &UMagicLeapMovementComponent::execAttachObjectToMovementController },
			{ "ChangeDepth", &UMagicLeapMovementComponent::execChangeDepth },
			{ "ChangeRotation", &UMagicLeapMovementComponent::execChangeRotation },
			{ "DetachObject", &UMagicLeapMovementComponent::execDetachObject },
			{ "GetDefaultSettings", &UMagicLeapMovementComponent::execGetDefaultSettings },
			{ "OnUpdatedComponentOverlapBegin", &UMagicLeapMovementComponent::execOnUpdatedComponentOverlapBegin },
			{ "OnUpdatedComponentOverlapEnd", &UMagicLeapMovementComponent::execOnUpdatedComponentOverlapEnd },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics
	{
		struct MagicLeapMovementComponent_eventAttachObjectToMovementController_Parms
		{
			const USceneComponent* InMovementController;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InMovementController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InMovementController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::NewProp_InMovementController_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::NewProp_InMovementController = { "InMovementController", nullptr, (EPropertyFlags)0x0010000000080082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventAttachObjectToMovementController_Parms, InMovementController), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::NewProp_InMovementController_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::NewProp_InMovementController_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::NewProp_InMovementController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movement | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Sets the SceneComponent whose transform will control the movement according to the provided movement settings.\n\x09\x09@param InMovementController The SceneComponent to be used for movement. MotionControllerComponent is a common input here.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Sets the SceneComponent whose transform will control the movement according to the provided movement settings.\n@param InMovementController The SceneComponent to be used for movement. MotionControllerComponent is a common input here." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "AttachObjectToMovementController", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventAttachObjectToMovementController_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics
	{
		struct MagicLeapMovementComponent_eventChangeDepth_Parms
		{
			float DeltaDepth;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaDepth;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::NewProp_DeltaDepth = { "DeltaDepth", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventChangeDepth_Parms, DeltaDepth), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMovementComponent_eventChangeDepth_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMovementComponent_eventChangeDepth_Parms), &Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::NewProp_DeltaDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movement | MagicLeap" },
		{ "Comment", "/** \n\x09\x09""Changes the depth offset of the object from the user's headpose (3Dof) or pointing device (6Dof).\n\x09\x09@param DeltaDepth The change in the depth offset in cm (can be negative).\n\x09\x09@return True if the depth was changed, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Changes the depth offset of the object from the user's headpose (3Dof) or pointing device (6Dof).\n@param DeltaDepth The change in the depth offset in cm (can be negative).\n@return True if the depth was changed, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "ChangeDepth", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventChangeDepth_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics
	{
		struct MagicLeapMovementComponent_eventChangeRotation_Parms
		{
			float DeltaDegrees;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaDegrees;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::NewProp_DeltaDegrees = { "DeltaDegrees", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventChangeRotation_Parms, DeltaDegrees), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMovementComponent_eventChangeRotation_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMovementComponent_eventChangeRotation_Parms), &Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::NewProp_DeltaDegrees,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movement | MagicLeap" },
		{ "Comment", "/** \n\x09\x09""Changes the rotation about the up-axis of the object being moved.\n\x09\x09@param DeltaDegrees The change (in degrees) of the rotation about the up axis.\n\x09\x09@return True if the rotation was changed, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Changes the rotation about the up-axis of the object being moved.\n@param DeltaDegrees The change (in degrees) of the rotation about the up axis.\n@return True if the rotation was changed, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "ChangeRotation", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventChangeRotation_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics
	{
		struct MagicLeapMovementComponent_eventDetachObject_Parms
		{
			bool bResolvePendingSoftCollisions;
		};
		static void NewProp_bResolvePendingSoftCollisions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResolvePendingSoftCollisions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::NewProp_bResolvePendingSoftCollisions_SetBit(void* Obj)
	{
		((MagicLeapMovementComponent_eventDetachObject_Parms*)Obj)->bResolvePendingSoftCollisions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::NewProp_bResolvePendingSoftCollisions = { "bResolvePendingSoftCollisions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMovementComponent_eventDetachObject_Parms), &Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::NewProp_bResolvePendingSoftCollisions_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::NewProp_bResolvePendingSoftCollisions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movement | MagicLeap" },
		{ "Comment", "/**\n\x09\x09""Detaches the currently selected movement controller.\n\x09\x09@param bResolvePendingSoftCollisions If true, soft collisions will continue to resolve after detaching.\n\x09*/" },
		{ "CPP_Default_bResolvePendingSoftCollisions", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Detaches the currently selected movement controller.\n@param bResolvePendingSoftCollisions If true, soft collisions will continue to resolve after detaching." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "DetachObject", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventDetachObject_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics
	{
		struct MagicLeapMovementComponent_eventGetDefaultSettings_Parms
		{
			FMagicLeapMovementSettings OutSettings;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutSettings;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::NewProp_OutSettings = { "OutSettings", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventGetDefaultSettings_Parms, OutSettings), Z_Construct_UScriptStruct_FMagicLeapMovementSettings, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MagicLeapMovementComponent_eventGetDefaultSettings_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMovementComponent_eventGetDefaultSettings_Parms), &Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::NewProp_OutSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movement | MagicLeap" },
		{ "Comment", "/**\n\x09\x09Gets the default settings.\n\x09\x09@param OutSettings The default settings.\n\x09\x09@return True if the default settings were retrieved, false otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Gets the default settings.\n@param OutSettings The default settings.\n@return True if the default settings were retrieved, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "GetDefaultSettings", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventGetDefaultSettings_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics
	{
		struct MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* Other;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Other;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_Other = { "Other", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms, Other), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms), &Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_SweepResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OverlappedComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_Other,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::NewProp_SweepResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "OnUpdatedComponentOverlapBegin", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapBegin_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00440401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics
	{
		struct MagicLeapMovementComponent_eventOnUpdatedComponentOverlapEnd_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* Other;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Other;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapEnd_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_Other = { "Other", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapEnd_Parms, Other), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapEnd_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapEnd_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OverlappedComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_Other,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::NewProp_OtherBodyIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMagicLeapMovementComponent, nullptr, "OnUpdatedComponentOverlapEnd", nullptr, nullptr, sizeof(MagicLeapMovementComponent_eventOnUpdatedComponentOverlapEnd_Parms), Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMagicLeapMovementComponent_NoRegister()
	{
		return UMagicLeapMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMagicLeapMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MovementSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings3Dof_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings3Dof;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings6Dof_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings6Dof;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSlideAlongSurfaceOnBlockingHit_MetaData[];
#endif
		static void NewProp_bSlideAlongSurfaceOnBlockingHit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSlideAlongSurfaceOnBlockingHit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSoftCollideWithOverlappingActors_MetaData[];
#endif
		static void NewProp_bSoftCollideWithOverlappingActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSoftCollideWithOverlappingActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMagicLeapMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MagicLeapMovement,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMagicLeapMovementComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_AttachObjectToMovementController, "AttachObjectToMovementController" }, // 2501901652
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeDepth, "ChangeDepth" }, // 2520063132
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_ChangeRotation, "ChangeRotation" }, // 478621384
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_DetachObject, "DetachObject" }, // 828812117
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_GetDefaultSettings, "GetDefaultSettings" }, // 2601716221
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapBegin, "OnUpdatedComponentOverlapBegin" }, // 1322504454
		{ &Z_Construct_UFunction_UMagicLeapMovementComponent_OnUpdatedComponentOverlapEnd, "OnUpdatedComponentOverlapEnd" }, // 3450810219
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "MagicLeap" },
		{ "Comment", "/**\n\x09LuminOS designed spatial movement for objects to feel like they have mass and inertia by tilting and swaying during\n\x09movement, but still respecting the user's desired placement. The component also offers two options for handling collision during movement (hard and soft).\n\x09Hard collisions are basically the blocking collisions from the Engine.\n\x09Soft collisions allow a degree of interpenetration before hitting an impenetrable core as defined by movement settings.\n\x09Soft collisions are possible only with components that have an \"Overlap\" response for the moving component.\n\x09""By default the root SceneComponent of the owning Actor of this component is updated. Component to move can be changed by\n\x09""calling SetUpdatedComponent().\n*/" },
		{ "IncludePath", "MagicLeapMovementComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "LuminOS designed spatial movement for objects to feel like they have mass and inertia by tilting and swaying during\nmovement, but still respecting the user's desired placement. The component also offers two options for handling collision during movement (hard and soft).\nHard collisions are basically the blocking collisions from the Engine.\nSoft collisions allow a degree of interpenetration before hitting an impenetrable core as defined by movement settings.\nSoft collisions are possible only with components that have an \"Overlap\" response for the moving component.\nBy default the root SceneComponent of the owning Actor of this component is updated. Component to move can be changed by\ncalling SetUpdatedComponent()." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_MovementSettings_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** The settings to be used when transforming the attached object. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "The settings to be used when transforming the attached object." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_MovementSettings = { "MovementSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapMovementComponent, MovementSettings), Z_Construct_UScriptStruct_FMagicLeapMovementSettings, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_MovementSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_MovementSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings3Dof_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** Additional settings to be used when transforming the attached object in 3DOF mode. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Additional settings to be used when transforming the attached object in 3DOF mode." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings3Dof = { "Settings3Dof", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapMovementComponent, Settings3Dof), Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings3Dof_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings3Dof_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings6Dof_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** Additional settings to be used when transforming the attached object in 6DOF mode. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "Additional settings to be used when transforming the attached object in 6DOF mode." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings6Dof = { "Settings6Dof", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMagicLeapMovementComponent, Settings6Dof), Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings6Dof_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings6Dof_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** In case of a blocking hit, slide the attached object along the hit surface. */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "In case of a blocking hit, slide the attached object along the hit surface." },
	};
#endif
	void Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit_SetBit(void* Obj)
	{
		((UMagicLeapMovementComponent*)Obj)->bSlideAlongSurfaceOnBlockingHit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit = { "bSlideAlongSurfaceOnBlockingHit", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMagicLeapMovementComponent), &Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors_MetaData[] = {
		{ "Category", "Movement|MagicLeap" },
		{ "Comment", "/** If true, attached object will have a degree of interpenetration with the overlapping objects.\n\x09 *  This changes the regular interaction of the attached object with overlapping components and gradually\n\x09 *  springs out of the overlapping volume instead of smoothly going through it. \n\x09 * \"Generate Overlap Event\" should be enabled to use this feature.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MagicLeapMovementComponent.h" },
		{ "ToolTip", "If true, attached object will have a degree of interpenetration with the overlapping objects.\nThis changes the regular interaction of the attached object with overlapping components and gradually\nsprings out of the overlapping volume instead of smoothly going through it.\n\"Generate Overlap Event\" should be enabled to use this feature." },
	};
#endif
	void Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors_SetBit(void* Obj)
	{
		((UMagicLeapMovementComponent*)Obj)->bSoftCollideWithOverlappingActors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors = { "bSoftCollideWithOverlappingActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMagicLeapMovementComponent), &Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMagicLeapMovementComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_MovementSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings3Dof,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_Settings6Dof,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSlideAlongSurfaceOnBlockingHit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMagicLeapMovementComponent_Statics::NewProp_bSoftCollideWithOverlappingActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMagicLeapMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMagicLeapMovementComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMagicLeapMovementComponent_Statics::ClassParams = {
		&UMagicLeapMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMagicLeapMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMagicLeapMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMagicLeapMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMagicLeapMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMagicLeapMovementComponent, 2194451807);
	template<> MAGICLEAPMOVEMENT_API UClass* StaticClass<UMagicLeapMovementComponent>()
	{
		return UMagicLeapMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMagicLeapMovementComponent(Z_Construct_UClass_UMagicLeapMovementComponent, &UMagicLeapMovementComponent::StaticClass, TEXT("/Script/MagicLeapMovement"), TEXT("UMagicLeapMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMagicLeapMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
