// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
struct FMagicLeapMovementSettings;
class USceneComponent;
#ifdef MAGICLEAPMOVEMENT_MagicLeapMovementComponent_generated_h
#error "MagicLeapMovementComponent.generated.h already included, missing '#pragma once' in MagicLeapMovementComponent.h"
#endif
#define MAGICLEAPMOVEMENT_MagicLeapMovementComponent_generated_h

#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_SPARSE_DATA
#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnUpdatedComponentOverlapEnd); \
	DECLARE_FUNCTION(execOnUpdatedComponentOverlapBegin); \
	DECLARE_FUNCTION(execChangeRotation); \
	DECLARE_FUNCTION(execChangeDepth); \
	DECLARE_FUNCTION(execGetDefaultSettings); \
	DECLARE_FUNCTION(execDetachObject); \
	DECLARE_FUNCTION(execAttachObjectToMovementController);


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnUpdatedComponentOverlapEnd); \
	DECLARE_FUNCTION(execOnUpdatedComponentOverlapBegin); \
	DECLARE_FUNCTION(execChangeRotation); \
	DECLARE_FUNCTION(execChangeDepth); \
	DECLARE_FUNCTION(execGetDefaultSettings); \
	DECLARE_FUNCTION(execDetachObject); \
	DECLARE_FUNCTION(execAttachObjectToMovementController);


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMagicLeapMovementComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapMovementComponent, UMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapMovement"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapMovementComponent)


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUMagicLeapMovementComponent(); \
	friend struct Z_Construct_UClass_UMagicLeapMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMagicLeapMovementComponent, UMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MagicLeapMovement"), NO_API) \
	DECLARE_SERIALIZER(UMagicLeapMovementComponent)


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMagicLeapMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMagicLeapMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapMovementComponent(UMagicLeapMovementComponent&&); \
	NO_API UMagicLeapMovementComponent(const UMagicLeapMovementComponent&); \
public:


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMagicLeapMovementComponent(UMagicLeapMovementComponent&&); \
	NO_API UMagicLeapMovementComponent(const UMagicLeapMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMagicLeapMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMagicLeapMovementComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMagicLeapMovementComponent)


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_21_PROLOG
#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_RPC_WRAPPERS \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_INCLASS \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_SPARSE_DATA \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAGICLEAPMOVEMENT_API UClass* StaticClass<class UMagicLeapMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
