// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAGICLEAPMOVEMENT_MagicLeapMovementTypes_generated_h
#error "MagicLeapMovementTypes.generated.h already included, missing '#pragma once' in MagicLeapMovementTypes.h"
#endif
#define MAGICLEAPMOVEMENT_MagicLeapMovementTypes_generated_h

#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementTypes_h_122_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapMovement6DofSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPMOVEMENT_API UScriptStruct* StaticStruct<struct FMagicLeapMovement6DofSettings>();

#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementTypes_h_111_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapMovement3DofSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPMOVEMENT_API UScriptStruct* StaticStruct<struct FMagicLeapMovement3DofSettings>();

#define Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementTypes_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMagicLeapMovementSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> MAGICLEAPMOVEMENT_API UScriptStruct* StaticStruct<struct FMagicLeapMovementSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Lumin_MagicLeapMovement_Source_Public_MagicLeapMovementTypes_h


#define FOREACH_ENUM_EMAGICLEAPMOVEMENTTYPE(op) \
	op(EMagicLeapMovementType::Controller3DOF) \
	op(EMagicLeapMovementType::Controller6DOF) 

enum class EMagicLeapMovementType : uint8;
template<> MAGICLEAPMOVEMENT_API UEnum* StaticEnum<EMagicLeapMovementType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
