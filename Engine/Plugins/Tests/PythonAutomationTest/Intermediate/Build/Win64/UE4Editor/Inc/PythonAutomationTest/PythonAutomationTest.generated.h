// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PYTHONAUTOMATIONTEST_PythonAutomationTest_generated_h
#error "PythonAutomationTest.generated.h already included, missing '#pragma once' in PythonAutomationTest.h"
#endif
#define PYTHONAUTOMATIONTEST_PythonAutomationTest_generated_h

#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_SPARSE_DATA
#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execResetPyLatentCommand); \
	DECLARE_FUNCTION(execGetPyLatentCommandTimeout); \
	DECLARE_FUNCTION(execSetPyLatentCommandTimeout); \
	DECLARE_FUNCTION(execGetIsRunningPyLatentCommand); \
	DECLARE_FUNCTION(execSetIsRunningPyLatentCommand);


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execResetPyLatentCommand); \
	DECLARE_FUNCTION(execGetPyLatentCommandTimeout); \
	DECLARE_FUNCTION(execSetPyLatentCommandTimeout); \
	DECLARE_FUNCTION(execGetIsRunningPyLatentCommand); \
	DECLARE_FUNCTION(execSetIsRunningPyLatentCommand);


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPyAutomationTestLibrary(); \
	friend struct Z_Construct_UClass_UPyAutomationTestLibrary_Statics; \
public: \
	DECLARE_CLASS(UPyAutomationTestLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonAutomationTest"), NO_API) \
	DECLARE_SERIALIZER(UPyAutomationTestLibrary)


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUPyAutomationTestLibrary(); \
	friend struct Z_Construct_UClass_UPyAutomationTestLibrary_Statics; \
public: \
	DECLARE_CLASS(UPyAutomationTestLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonAutomationTest"), NO_API) \
	DECLARE_SERIALIZER(UPyAutomationTestLibrary)


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyAutomationTestLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyAutomationTestLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyAutomationTestLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyAutomationTestLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyAutomationTestLibrary(UPyAutomationTestLibrary&&); \
	NO_API UPyAutomationTestLibrary(const UPyAutomationTestLibrary&); \
public:


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyAutomationTestLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyAutomationTestLibrary(UPyAutomationTestLibrary&&); \
	NO_API UPyAutomationTestLibrary(const UPyAutomationTestLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyAutomationTestLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyAutomationTestLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyAutomationTestLibrary)


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_11_PROLOG
#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_SPARSE_DATA \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_RPC_WRAPPERS \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_INCLASS \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_SPARSE_DATA \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONAUTOMATIONTEST_API UClass* StaticClass<class UPyAutomationTestLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Tests_PythonAutomationTest_Source_PythonAutomationTest_Public_PythonAutomationTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
