// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EditorTests/Private/UnrealEd/CollisionAutomationTests.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCollisionAutomationTests() {}
// Cross Module References
	EDITORTESTS_API UScriptStruct* Z_Construct_UScriptStruct_FCollisionPerfTest();
	UPackage* Z_Construct_UPackage__Script_EditorTests();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	EDITORTESTS_API UScriptStruct* Z_Construct_UScriptStruct_FCollisionTestEntry();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	EDITORTESTS_API UClass* Z_Construct_UClass_UCollisionAutomationTestConfigData_NoRegister();
	EDITORTESTS_API UClass* Z_Construct_UClass_UCollisionAutomationTestConfigData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FCollisionPerfTest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORTESTS_API uint32 Get_Z_Construct_UScriptStruct_FCollisionPerfTest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCollisionPerfTest, Z_Construct_UPackage__Script_EditorTests(), TEXT("CollisionPerfTest"), sizeof(FCollisionPerfTest), Get_Z_Construct_UScriptStruct_FCollisionPerfTest_Hash());
	}
	return Singleton;
}
template<> EDITORTESTS_API UScriptStruct* StaticStruct<FCollisionPerfTest>()
{
	return FCollisionPerfTest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCollisionPerfTest(FCollisionPerfTest::StaticStruct, TEXT("/Script/EditorTests"), TEXT("CollisionPerfTest"), false, nullptr, nullptr);
static struct FScriptStruct_EditorTests_StaticRegisterNativesFCollisionPerfTest
{
	FScriptStruct_EditorTests_StaticRegisterNativesFCollisionPerfTest()
	{
		UScriptStruct::DeferCppStructOps<FCollisionPerfTest>(FName(TEXT("CollisionPerfTest")));
	}
} ScriptStruct_EditorTests_StaticRegisterNativesFCollisionPerfTest;
	struct Z_Construct_UScriptStruct_FCollisionPerfTest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootShapeAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RootShapeAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShapeType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreationBounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CreationBounds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreationElements_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CreationElements;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCollisionPerfTest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_RootShapeAsset_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_RootShapeAsset = { "RootShapeAsset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionPerfTest, RootShapeAsset), METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_RootShapeAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_RootShapeAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_ShapeType_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_ShapeType = { "ShapeType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionPerfTest, ShapeType), METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_ShapeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_ShapeType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationBounds_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationBounds = { "CreationBounds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionPerfTest, CreationBounds), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationBounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationBounds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationElements_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationElements = { "CreationElements", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionPerfTest, CreationElements), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationElements_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationElements_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_RootShapeAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_ShapeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationBounds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::NewProp_CreationElements,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorTests,
		nullptr,
		&NewStructOps,
		"CollisionPerfTest",
		sizeof(FCollisionPerfTest),
		alignof(FCollisionPerfTest),
		Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCollisionPerfTest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCollisionPerfTest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorTests();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CollisionPerfTest"), sizeof(FCollisionPerfTest), Get_Z_Construct_UScriptStruct_FCollisionPerfTest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCollisionPerfTest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCollisionPerfTest_Hash() { return 2415796295U; }
class UScriptStruct* FCollisionTestEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern EDITORTESTS_API uint32 Get_Z_Construct_UScriptStruct_FCollisionTestEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCollisionTestEntry, Z_Construct_UPackage__Script_EditorTests(), TEXT("CollisionTestEntry"), sizeof(FCollisionTestEntry), Get_Z_Construct_UScriptStruct_FCollisionTestEntry_Hash());
	}
	return Singleton;
}
template<> EDITORTESTS_API UScriptStruct* StaticStruct<FCollisionTestEntry>()
{
	return FCollisionTestEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCollisionTestEntry(FCollisionTestEntry::StaticStruct, TEXT("/Script/EditorTests"), TEXT("CollisionTestEntry"), false, nullptr, nullptr);
static struct FScriptStruct_EditorTests_StaticRegisterNativesFCollisionTestEntry
{
	FScriptStruct_EditorTests_StaticRegisterNativesFCollisionTestEntry()
	{
		UScriptStruct::DeferCppStructOps<FCollisionTestEntry>(FName(TEXT("CollisionTestEntry")));
	}
} ScriptStruct_EditorTests_StaticRegisterNativesFCollisionTestEntry;
	struct Z_Construct_UScriptStruct_FCollisionTestEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootShapeAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RootShapeAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShapeType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Container for detailing collision automated test data.\n */" },
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
		{ "ToolTip", "Container for detailing collision automated test data." },
	};
#endif
	void* Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCollisionTestEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_RootShapeAsset_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_RootShapeAsset = { "RootShapeAsset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionTestEntry, RootShapeAsset), METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_RootShapeAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_RootShapeAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_ShapeType_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_ShapeType = { "ShapeType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionTestEntry, ShapeType), METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_ShapeType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_ShapeType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_HitResult_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCollisionTestEntry, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_HitResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_RootShapeAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_ShapeType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::NewProp_HitResult,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_EditorTests,
		nullptr,
		&NewStructOps,
		"CollisionTestEntry",
		sizeof(FCollisionTestEntry),
		alignof(FCollisionTestEntry),
		Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCollisionTestEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCollisionTestEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_EditorTests();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CollisionTestEntry"), sizeof(FCollisionTestEntry), Get_Z_Construct_UScriptStruct_FCollisionTestEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCollisionTestEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCollisionTestEntry_Hash() { return 3849865154U; }
	void UCollisionAutomationTestConfigData::StaticRegisterNativesUCollisionAutomationTestConfigData()
	{
	}
	UClass* Z_Construct_UClass_UCollisionAutomationTestConfigData_NoRegister()
	{
		return UCollisionAutomationTestConfigData::StaticClass();
	}
	struct Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ComponentSweepMultiTests_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentSweepMultiTests_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ComponentSweepMultiTests;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LineTraceSingleByChannelTests_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineTraceSingleByChannelTests_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LineTraceSingleByChannelTests;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LineTracePerformanceTests_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineTracePerformanceTests_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LineTracePerformanceTests;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EditorTests,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "UnrealEd/CollisionAutomationTests.h" },
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests_Inner = { "ComponentSweepMultiTests", nullptr, (EPropertyFlags)0x0000008000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCollisionTestEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests = { "ComponentSweepMultiTests", nullptr, (EPropertyFlags)0x0010008000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCollisionAutomationTestConfigData, ComponentSweepMultiTests), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests_Inner = { "LineTraceSingleByChannelTests", nullptr, (EPropertyFlags)0x0000008000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCollisionTestEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests = { "LineTraceSingleByChannelTests", nullptr, (EPropertyFlags)0x0010008000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCollisionAutomationTestConfigData, LineTraceSingleByChannelTests), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests_Inner = { "LineTracePerformanceTests", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCollisionPerfTest, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests_MetaData[] = {
		{ "ModuleRelativePath", "Private/UnrealEd/CollisionAutomationTests.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests = { "LineTracePerformanceTests", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCollisionAutomationTestConfigData, LineTracePerformanceTests), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_ComponentSweepMultiTests,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTraceSingleByChannelTests,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::NewProp_LineTracePerformanceTests,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCollisionAutomationTestConfigData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::ClassParams = {
		&UCollisionAutomationTestConfigData::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCollisionAutomationTestConfigData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCollisionAutomationTestConfigData, 4187005938);
	template<> EDITORTESTS_API UClass* StaticClass<UCollisionAutomationTestConfigData>()
	{
		return UCollisionAutomationTestConfigData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCollisionAutomationTestConfigData(Z_Construct_UClass_UCollisionAutomationTestConfigData, &UCollisionAutomationTestConfigData::StaticClass, TEXT("/Script/EditorTests"), TEXT("UCollisionAutomationTestConfigData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCollisionAutomationTestConfigData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
