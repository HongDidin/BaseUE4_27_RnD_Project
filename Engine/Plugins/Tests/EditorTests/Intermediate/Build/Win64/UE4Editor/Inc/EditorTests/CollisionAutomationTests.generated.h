// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EDITORTESTS_CollisionAutomationTests_generated_h
#error "CollisionAutomationTests.generated.h already included, missing '#pragma once' in CollisionAutomationTests.h"
#endif
#define EDITORTESTS_CollisionAutomationTests_generated_h

#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCollisionPerfTest_Statics; \
	EDITORTESTS_API static class UScriptStruct* StaticStruct();


template<> EDITORTESTS_API UScriptStruct* StaticStruct<struct FCollisionPerfTest>();

#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCollisionTestEntry_Statics; \
	EDITORTESTS_API static class UScriptStruct* StaticStruct();


template<> EDITORTESTS_API UScriptStruct* StaticStruct<struct FCollisionTestEntry>();

#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_SPARSE_DATA
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_RPC_WRAPPERS
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCollisionAutomationTestConfigData(); \
	friend struct Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics; \
public: \
	DECLARE_CLASS(UCollisionAutomationTestConfigData, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EditorTests"), NO_API) \
	DECLARE_SERIALIZER(UCollisionAutomationTestConfigData) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUCollisionAutomationTestConfigData(); \
	friend struct Z_Construct_UClass_UCollisionAutomationTestConfigData_Statics; \
public: \
	DECLARE_CLASS(UCollisionAutomationTestConfigData, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EditorTests"), NO_API) \
	DECLARE_SERIALIZER(UCollisionAutomationTestConfigData) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCollisionAutomationTestConfigData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCollisionAutomationTestConfigData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCollisionAutomationTestConfigData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCollisionAutomationTestConfigData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCollisionAutomationTestConfigData(UCollisionAutomationTestConfigData&&); \
	NO_API UCollisionAutomationTestConfigData(const UCollisionAutomationTestConfigData&); \
public:


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCollisionAutomationTestConfigData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCollisionAutomationTestConfigData(UCollisionAutomationTestConfigData&&); \
	NO_API UCollisionAutomationTestConfigData(const UCollisionAutomationTestConfigData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCollisionAutomationTestConfigData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCollisionAutomationTestConfigData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCollisionAutomationTestConfigData)


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_57_PROLOG
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_SPARSE_DATA \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_RPC_WRAPPERS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_INCLASS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_SPARSE_DATA \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h_61_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EDITORTESTS_API UClass* StaticClass<class UCollisionAutomationTestConfigData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Tests_EditorTests_Source_EditorTests_Private_UnrealEd_CollisionAutomationTests_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
