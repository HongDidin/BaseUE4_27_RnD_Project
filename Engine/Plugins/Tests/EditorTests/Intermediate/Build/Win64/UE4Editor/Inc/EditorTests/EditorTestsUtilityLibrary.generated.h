// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UStaticMeshComponent;
struct FMeshMergingSettings;
class UMaterialOptions;
class UMaterialMergeOptions;
#ifdef EDITORTESTS_EditorTestsUtilityLibrary_generated_h
#error "EditorTestsUtilityLibrary.generated.h already included, missing '#pragma once' in EditorTestsUtilityLibrary.h"
#endif
#define EDITORTESTS_EditorTestsUtilityLibrary_generated_h

#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_SPARSE_DATA
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMergeStaticMeshComponents); \
	DECLARE_FUNCTION(execBakeMaterialsForComponent);


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMergeStaticMeshComponents); \
	DECLARE_FUNCTION(execBakeMaterialsForComponent);


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditorTestsUtilityLibrary(); \
	friend struct Z_Construct_UClass_UEditorTestsUtilityLibrary_Statics; \
public: \
	DECLARE_CLASS(UEditorTestsUtilityLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EditorTests"), NO_API) \
	DECLARE_SERIALIZER(UEditorTestsUtilityLibrary)


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUEditorTestsUtilityLibrary(); \
	friend struct Z_Construct_UClass_UEditorTestsUtilityLibrary_Statics; \
public: \
	DECLARE_CLASS(UEditorTestsUtilityLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EditorTests"), NO_API) \
	DECLARE_SERIALIZER(UEditorTestsUtilityLibrary)


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditorTestsUtilityLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditorTestsUtilityLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorTestsUtilityLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorTestsUtilityLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorTestsUtilityLibrary(UEditorTestsUtilityLibrary&&); \
	NO_API UEditorTestsUtilityLibrary(const UEditorTestsUtilityLibrary&); \
public:


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditorTestsUtilityLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorTestsUtilityLibrary(UEditorTestsUtilityLibrary&&); \
	NO_API UEditorTestsUtilityLibrary(const UEditorTestsUtilityLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorTestsUtilityLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorTestsUtilityLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditorTestsUtilityLibrary)


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_15_PROLOG
#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_SPARSE_DATA \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_RPC_WRAPPERS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_INCLASS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_SPARSE_DATA \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EDITORTESTS_API UClass* StaticClass<class UEditorTestsUtilityLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Tests_EditorTests_Source_EditorTests_Public_EditorTestsUtilityLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
