// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Public/EditorFunctionalTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorFunctionalTest() {}
// Cross Module References
	FUNCTIONALTESTINGEDITOR_API UClass* Z_Construct_UClass_AEditorFunctionalTest_NoRegister();
	FUNCTIONALTESTINGEDITOR_API UClass* Z_Construct_UClass_AEditorFunctionalTest();
	FUNCTIONALTESTING_API UClass* Z_Construct_UClass_AFunctionalTest();
	UPackage* Z_Construct_UPackage__Script_FunctionalTestingEditor();
	FUNCTIONALTESTINGEDITOR_API UClass* Z_Construct_UClass_AEditorScreenshotFunctionalTest_NoRegister();
	FUNCTIONALTESTINGEDITOR_API UClass* Z_Construct_UClass_AEditorScreenshotFunctionalTest();
	FUNCTIONALTESTING_API UClass* Z_Construct_UClass_AScreenshotFunctionalTest();
// End Cross Module References
	void AEditorFunctionalTest::StaticRegisterNativesAEditorFunctionalTest()
	{
	}
	UClass* Z_Construct_UClass_AEditorFunctionalTest_NoRegister()
	{
		return AEditorFunctionalTest::StaticClass();
	}
	struct Z_Construct_UClass_AEditorFunctionalTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEditorFunctionalTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFunctionalTest,
		(UObject* (*)())Z_Construct_UPackage__Script_FunctionalTestingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEditorFunctionalTest_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Editor-only version of functional test actor, tests that call editor-only blueprints must inherit from this class */" },
		{ "HideCategories", "Actor Input Rendering" },
		{ "IncludePath", "EditorFunctionalTest.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EditorFunctionalTest.h" },
		{ "ToolTip", "Editor-only version of functional test actor, tests that call editor-only blueprints must inherit from this class" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEditorFunctionalTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEditorFunctionalTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEditorFunctionalTest_Statics::ClassParams = {
		&AEditorFunctionalTest::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008800A4u,
		METADATA_PARAMS(Z_Construct_UClass_AEditorFunctionalTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEditorFunctionalTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEditorFunctionalTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEditorFunctionalTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEditorFunctionalTest, 302688491);
	template<> FUNCTIONALTESTINGEDITOR_API UClass* StaticClass<AEditorFunctionalTest>()
	{
		return AEditorFunctionalTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEditorFunctionalTest(Z_Construct_UClass_AEditorFunctionalTest, &AEditorFunctionalTest::StaticClass, TEXT("/Script/FunctionalTestingEditor"), TEXT("AEditorFunctionalTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEditorFunctionalTest);
	void AEditorScreenshotFunctionalTest::StaticRegisterNativesAEditorScreenshotFunctionalTest()
	{
	}
	UClass* Z_Construct_UClass_AEditorScreenshotFunctionalTest_NoRegister()
	{
		return AEditorScreenshotFunctionalTest::StaticClass();
	}
	struct Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AScreenshotFunctionalTest,
		(UObject* (*)())Z_Construct_UPackage__Script_FunctionalTestingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Editor-only version of screenshot functional test */" },
		{ "HideCategories", "Actor Input Rendering" },
		{ "IncludePath", "EditorFunctionalTest.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EditorFunctionalTest.h" },
		{ "ToolTip", "Editor-only version of screenshot functional test" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEditorScreenshotFunctionalTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::ClassParams = {
		&AEditorScreenshotFunctionalTest::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008800A4u,
		METADATA_PARAMS(Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEditorScreenshotFunctionalTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEditorScreenshotFunctionalTest, 1292698813);
	template<> FUNCTIONALTESTINGEDITOR_API UClass* StaticClass<AEditorScreenshotFunctionalTest>()
	{
		return AEditorScreenshotFunctionalTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEditorScreenshotFunctionalTest(Z_Construct_UClass_AEditorScreenshotFunctionalTest, &AEditorScreenshotFunctionalTest::StaticClass, TEXT("/Script/FunctionalTestingEditor"), TEXT("AEditorScreenshotFunctionalTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEditorScreenshotFunctionalTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
