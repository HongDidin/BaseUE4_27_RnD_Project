// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FUNCTIONALTESTINGEDITOR_EditorFunctionalTest_generated_h
#error "EditorFunctionalTest.generated.h already included, missing '#pragma once' in EditorFunctionalTest.h"
#endif
#define FUNCTIONALTESTINGEDITOR_EditorFunctionalTest_generated_h

#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_SPARSE_DATA
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_RPC_WRAPPERS
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEditorFunctionalTest(); \
	friend struct Z_Construct_UClass_AEditorFunctionalTest_Statics; \
public: \
	DECLARE_CLASS(AEditorFunctionalTest, AFunctionalTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FunctionalTestingEditor"), FUNCTIONALTESTINGEDITOR_API) \
	DECLARE_SERIALIZER(AEditorFunctionalTest)


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAEditorFunctionalTest(); \
	friend struct Z_Construct_UClass_AEditorFunctionalTest_Statics; \
public: \
	DECLARE_CLASS(AEditorFunctionalTest, AFunctionalTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FunctionalTestingEditor"), FUNCTIONALTESTINGEDITOR_API) \
	DECLARE_SERIALIZER(AEditorFunctionalTest)


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FUNCTIONALTESTINGEDITOR_API AEditorFunctionalTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEditorFunctionalTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FUNCTIONALTESTINGEDITOR_API, AEditorFunctionalTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEditorFunctionalTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FUNCTIONALTESTINGEDITOR_API AEditorFunctionalTest(AEditorFunctionalTest&&); \
	FUNCTIONALTESTINGEDITOR_API AEditorFunctionalTest(const AEditorFunctionalTest&); \
public:


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FUNCTIONALTESTINGEDITOR_API AEditorFunctionalTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FUNCTIONALTESTINGEDITOR_API AEditorFunctionalTest(AEditorFunctionalTest&&); \
	FUNCTIONALTESTINGEDITOR_API AEditorFunctionalTest(const AEditorFunctionalTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FUNCTIONALTESTINGEDITOR_API, AEditorFunctionalTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEditorFunctionalTest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEditorFunctionalTest)


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_10_PROLOG
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_SPARSE_DATA \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_RPC_WRAPPERS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_INCLASS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_SPARSE_DATA \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FUNCTIONALTESTINGEDITOR_API UClass* StaticClass<class AEditorFunctionalTest>();

#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_SPARSE_DATA
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_RPC_WRAPPERS
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEditorScreenshotFunctionalTest(); \
	friend struct Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics; \
public: \
	DECLARE_CLASS(AEditorScreenshotFunctionalTest, AScreenshotFunctionalTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FunctionalTestingEditor"), FUNCTIONALTESTINGEDITOR_API) \
	DECLARE_SERIALIZER(AEditorScreenshotFunctionalTest)


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_INCLASS \
private: \
	static void StaticRegisterNativesAEditorScreenshotFunctionalTest(); \
	friend struct Z_Construct_UClass_AEditorScreenshotFunctionalTest_Statics; \
public: \
	DECLARE_CLASS(AEditorScreenshotFunctionalTest, AScreenshotFunctionalTest, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FunctionalTestingEditor"), FUNCTIONALTESTINGEDITOR_API) \
	DECLARE_SERIALIZER(AEditorScreenshotFunctionalTest)


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FUNCTIONALTESTINGEDITOR_API AEditorScreenshotFunctionalTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEditorScreenshotFunctionalTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FUNCTIONALTESTINGEDITOR_API, AEditorScreenshotFunctionalTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEditorScreenshotFunctionalTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FUNCTIONALTESTINGEDITOR_API AEditorScreenshotFunctionalTest(AEditorScreenshotFunctionalTest&&); \
	FUNCTIONALTESTINGEDITOR_API AEditorScreenshotFunctionalTest(const AEditorScreenshotFunctionalTest&); \
public:


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FUNCTIONALTESTINGEDITOR_API AEditorScreenshotFunctionalTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FUNCTIONALTESTINGEDITOR_API AEditorScreenshotFunctionalTest(AEditorScreenshotFunctionalTest&&); \
	FUNCTIONALTESTINGEDITOR_API AEditorScreenshotFunctionalTest(const AEditorScreenshotFunctionalTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FUNCTIONALTESTINGEDITOR_API, AEditorScreenshotFunctionalTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEditorScreenshotFunctionalTest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEditorScreenshotFunctionalTest)


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_22_PROLOG
#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_SPARSE_DATA \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_RPC_WRAPPERS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_INCLASS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_SPARSE_DATA \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FUNCTIONALTESTINGEDITOR_API UClass* StaticClass<class AEditorScreenshotFunctionalTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Tests_FunctionalTestingEditor_Source_Public_EditorFunctionalTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
