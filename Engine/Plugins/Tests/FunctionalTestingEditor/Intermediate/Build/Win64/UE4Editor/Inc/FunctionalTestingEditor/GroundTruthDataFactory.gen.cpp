// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/GroundTruthDataFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroundTruthDataFactory() {}
// Cross Module References
	FUNCTIONALTESTINGEDITOR_API UClass* Z_Construct_UClass_UGroundTruthDataFactory_NoRegister();
	FUNCTIONALTESTINGEDITOR_API UClass* Z_Construct_UClass_UGroundTruthDataFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_FunctionalTestingEditor();
// End Cross Module References
	void UGroundTruthDataFactory::StaticRegisterNativesUGroundTruthDataFactory()
	{
	}
	UClass* Z_Construct_UClass_UGroundTruthDataFactory_NoRegister()
	{
		return UGroundTruthDataFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGroundTruthDataFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroundTruthDataFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_FunctionalTestingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroundTruthDataFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "GroundTruthDataFactory.h" },
		{ "ModuleRelativePath", "Private/GroundTruthDataFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroundTruthDataFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroundTruthDataFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroundTruthDataFactory_Statics::ClassParams = {
		&UGroundTruthDataFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000020A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroundTruthDataFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroundTruthDataFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroundTruthDataFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroundTruthDataFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroundTruthDataFactory, 4088101883);
	template<> FUNCTIONALTESTINGEDITOR_API UClass* StaticClass<UGroundTruthDataFactory>()
	{
		return UGroundTruthDataFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroundTruthDataFactory(Z_Construct_UClass_UGroundTruthDataFactory, &UGroundTruthDataFactory::StaticClass, TEXT("/Script/FunctionalTestingEditor"), TEXT("UGroundTruthDataFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroundTruthDataFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
