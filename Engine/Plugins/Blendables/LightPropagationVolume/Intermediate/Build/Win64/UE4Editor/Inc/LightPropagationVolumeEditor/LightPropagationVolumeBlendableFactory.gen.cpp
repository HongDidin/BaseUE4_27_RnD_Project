// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LightPropagationVolumeEditor/Classes/LightPropagationVolumeBlendableFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLightPropagationVolumeBlendableFactory() {}
// Cross Module References
	LIGHTPROPAGATIONVOLUMEEDITOR_API UClass* Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_NoRegister();
	LIGHTPROPAGATIONVOLUMEEDITOR_API UClass* Z_Construct_UClass_ULightPropagationVolumeBlendableFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_LightPropagationVolumeEditor();
// End Cross Module References
	void ULightPropagationVolumeBlendableFactory::StaticRegisterNativesULightPropagationVolumeBlendableFactory()
	{
	}
	UClass* Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_NoRegister()
	{
		return ULightPropagationVolumeBlendableFactory::StaticClass();
	}
	struct Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LightPropagationVolumeEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "LightPropagationVolumeBlendableFactory.h" },
		{ "ModuleRelativePath", "Classes/LightPropagationVolumeBlendableFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULightPropagationVolumeBlendableFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::ClassParams = {
		&ULightPropagationVolumeBlendableFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULightPropagationVolumeBlendableFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULightPropagationVolumeBlendableFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULightPropagationVolumeBlendableFactory, 1527794267);
	template<> LIGHTPROPAGATIONVOLUMEEDITOR_API UClass* StaticClass<ULightPropagationVolumeBlendableFactory>()
	{
		return ULightPropagationVolumeBlendableFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULightPropagationVolumeBlendableFactory(Z_Construct_UClass_ULightPropagationVolumeBlendableFactory, &ULightPropagationVolumeBlendableFactory::StaticClass, TEXT("/Script/LightPropagationVolumeEditor"), TEXT("ULightPropagationVolumeBlendableFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULightPropagationVolumeBlendableFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
