// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkMasterLockit/Public/LiveLinkMasterLockitConnectionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkMasterLockitConnectionSettings() {}
// Cross Module References
	LIVELINKMASTERLOCKIT_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkMasterLockit();
// End Cross Module References
class UScriptStruct* FLiveLinkMasterLockitConnectionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKMASTERLOCKIT_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings, Z_Construct_UPackage__Script_LiveLinkMasterLockit(), TEXT("LiveLinkMasterLockitConnectionSettings"), sizeof(FLiveLinkMasterLockitConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Hash());
	}
	return Singleton;
}
template<> LIVELINKMASTERLOCKIT_API UScriptStruct* StaticStruct<FLiveLinkMasterLockitConnectionSettings>()
{
	return FLiveLinkMasterLockitConnectionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings(FLiveLinkMasterLockitConnectionSettings::StaticStruct, TEXT("/Script/LiveLinkMasterLockit"), TEXT("LiveLinkMasterLockitConnectionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkMasterLockit_StaticRegisterNativesFLiveLinkMasterLockitConnectionSettings
{
	FScriptStruct_LiveLinkMasterLockit_StaticRegisterNativesFLiveLinkMasterLockitConnectionSettings()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkMasterLockitConnectionSettings>(FName(TEXT("LiveLinkMasterLockitConnectionSettings")));
	}
} ScriptStruct_LiveLinkMasterLockit_StaticRegisterNativesFLiveLinkMasterLockitConnectionSettings;
	struct Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IPAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IPAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LiveLinkMasterLockitConnectionSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkMasterLockitConnectionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_IPAddress_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/LiveLinkMasterLockitConnectionSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_IPAddress = { "IPAddress", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkMasterLockitConnectionSettings, IPAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_IPAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_IPAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/LiveLinkMasterLockitConnectionSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkMasterLockitConnectionSettings, SubjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_SubjectName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_IPAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::NewProp_SubjectName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkMasterLockit,
		nullptr,
		&NewStructOps,
		"LiveLinkMasterLockitConnectionSettings",
		sizeof(FLiveLinkMasterLockitConnectionSettings),
		alignof(FLiveLinkMasterLockitConnectionSettings),
		Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkMasterLockit();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkMasterLockitConnectionSettings"), sizeof(FLiveLinkMasterLockitConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkMasterLockitConnectionSettings_Hash() { return 2680014138U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
