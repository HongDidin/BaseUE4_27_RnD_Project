// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKMASTERLOCKIT_LiveLinkMasterLockitFactory_generated_h
#error "LiveLinkMasterLockitFactory.generated.h already included, missing '#pragma once' in LiveLinkMasterLockitFactory.h"
#endif
#define LIVELINKMASTERLOCKIT_LiveLinkMasterLockitFactory_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkMasterLockitSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkMasterLockitSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkMasterLockitSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkMasterLockit"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkMasterLockitSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkMasterLockitSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkMasterLockitSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkMasterLockitSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkMasterLockit"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkMasterLockitSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkMasterLockitSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkMasterLockitSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkMasterLockitSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkMasterLockitSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkMasterLockitSourceFactory(ULiveLinkMasterLockitSourceFactory&&); \
	NO_API ULiveLinkMasterLockitSourceFactory(const ULiveLinkMasterLockitSourceFactory&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkMasterLockitSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkMasterLockitSourceFactory(ULiveLinkMasterLockitSourceFactory&&); \
	NO_API ULiveLinkMasterLockitSourceFactory(const ULiveLinkMasterLockitSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkMasterLockitSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkMasterLockitSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkMasterLockitSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h_20_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKMASTERLOCKIT_API UClass* StaticClass<class ULiveLinkMasterLockitSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Public_LiveLinkMasterLockitFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
