// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkMasterLockit/Private/LiveLinkMasterLockitSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkMasterLockitSourceSettings() {}
// Cross Module References
	LIVELINKMASTERLOCKIT_API UClass* Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_NoRegister();
	LIVELINKMASTERLOCKIT_API UClass* Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkSourceSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkMasterLockit();
// End Cross Module References
	void ULiveLinkMasterLockitSourceSettings::StaticRegisterNativesULiveLinkMasterLockitSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_NoRegister()
	{
		return ULiveLinkMasterLockitSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkSourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkMasterLockit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkMasterLockitSourceSettings.h" },
		{ "ModuleRelativePath", "Private/LiveLinkMasterLockitSourceSettings.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkMasterLockitSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::ClassParams = {
		&ULiveLinkMasterLockitSourceSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkMasterLockitSourceSettings, 3030115461);
	template<> LIVELINKMASTERLOCKIT_API UClass* StaticClass<ULiveLinkMasterLockitSourceSettings>()
	{
		return ULiveLinkMasterLockitSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkMasterLockitSourceSettings(Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings, &ULiveLinkMasterLockitSourceSettings::StaticClass, TEXT("/Script/LiveLinkMasterLockit"), TEXT("ULiveLinkMasterLockitSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkMasterLockitSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
