// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKMASTERLOCKIT_LiveLinkMasterLockitSourceSettings_generated_h
#error "LiveLinkMasterLockitSourceSettings.generated.h already included, missing '#pragma once' in LiveLinkMasterLockitSourceSettings.h"
#endif
#define LIVELINKMASTERLOCKIT_LiveLinkMasterLockitSourceSettings_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkMasterLockitSourceSettings(); \
	friend struct Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkMasterLockitSourceSettings, ULiveLinkSourceSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkMasterLockit"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkMasterLockitSourceSettings)


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkMasterLockitSourceSettings(); \
	friend struct Z_Construct_UClass_ULiveLinkMasterLockitSourceSettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkMasterLockitSourceSettings, ULiveLinkSourceSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkMasterLockit"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkMasterLockitSourceSettings)


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkMasterLockitSourceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkMasterLockitSourceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkMasterLockitSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkMasterLockitSourceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkMasterLockitSourceSettings(ULiveLinkMasterLockitSourceSettings&&); \
	NO_API ULiveLinkMasterLockitSourceSettings(const ULiveLinkMasterLockitSourceSettings&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkMasterLockitSourceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkMasterLockitSourceSettings(ULiveLinkMasterLockitSourceSettings&&); \
	NO_API ULiveLinkMasterLockitSourceSettings(const ULiveLinkMasterLockitSourceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkMasterLockitSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkMasterLockitSourceSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkMasterLockitSourceSettings)


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_11_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKMASTERLOCKIT_API UClass* StaticClass<class ULiveLinkMasterLockitSourceSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkMasterLockit_Source_LiveLinkMasterLockit_Private_LiveLinkMasterLockitSourceSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
