// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlProtocolMIDI/Public/RemoteControlProtocolMIDISettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolMIDISettings() {}
// Cross Module References
	REMOTECONTROLPROTOCOLMIDI_API UClass* Z_Construct_UClass_URemoteControlProtocolMIDISettings_NoRegister();
	REMOTECONTROLPROTOCOLMIDI_API UClass* Z_Construct_UClass_URemoteControlProtocolMIDISettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_RemoteControlProtocolMIDI();
	REMOTECONTROLPROTOCOLMIDI_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlMIDIDevice();
// End Cross Module References
	void URemoteControlProtocolMIDISettings::StaticRegisterNativesURemoteControlProtocolMIDISettings()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlProtocolMIDISettings_NoRegister()
	{
		return URemoteControlProtocolMIDISettings::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDevice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultDevice;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolMIDI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * MIDI Remote Control Settings\n */" },
		{ "IncludePath", "RemoteControlProtocolMIDISettings.h" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDISettings.h" },
		{ "ToolTip", "MIDI Remote Control Settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::NewProp_DefaultDevice_MetaData[] = {
		{ "Category", "MIDI" },
		{ "Comment", "/** Midi Default Device */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDISettings.h" },
		{ "ToolTip", "Midi Default Device" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::NewProp_DefaultDevice = { "DefaultDevice", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlProtocolMIDISettings, DefaultDevice), Z_Construct_UScriptStruct_FRemoteControlMIDIDevice, METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::NewProp_DefaultDevice_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::NewProp_DefaultDevice_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::NewProp_DefaultDevice,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlProtocolMIDISettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::ClassParams = {
		&URemoteControlProtocolMIDISettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlProtocolMIDISettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlProtocolMIDISettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlProtocolMIDISettings, 2335576521);
	template<> REMOTECONTROLPROTOCOLMIDI_API UClass* StaticClass<URemoteControlProtocolMIDISettings>()
	{
		return URemoteControlProtocolMIDISettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlProtocolMIDISettings(Z_Construct_UClass_URemoteControlProtocolMIDISettings, &URemoteControlProtocolMIDISettings::StaticClass, TEXT("/Script/RemoteControlProtocolMIDI"), TEXT("URemoteControlProtocolMIDISettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlProtocolMIDISettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
