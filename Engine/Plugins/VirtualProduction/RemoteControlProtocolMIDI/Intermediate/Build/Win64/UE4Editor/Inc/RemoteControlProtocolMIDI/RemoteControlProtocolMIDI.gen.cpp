// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlProtocolMIDI/Public/RemoteControlProtocolMIDI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolMIDI() {}
// Cross Module References
	REMOTECONTROLPROTOCOLMIDI_API UEnum* Z_Construct_UEnum_RemoteControlProtocolMIDI_ERemoteControlMIDIDeviceSelector();
	UPackage* Z_Construct_UPackage__Script_RemoteControlProtocolMIDI();
	REMOTECONTROLPROTOCOLMIDI_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolEntity();
	REMOTECONTROLPROTOCOLMIDI_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlMIDIDevice();
	MIDIDEVICE_API UEnum* Z_Construct_UEnum_MIDIDevice_EMIDIEventType();
// End Cross Module References
	static UEnum* ERemoteControlMIDIDeviceSelector_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteControlProtocolMIDI_ERemoteControlMIDIDeviceSelector, Z_Construct_UPackage__Script_RemoteControlProtocolMIDI(), TEXT("ERemoteControlMIDIDeviceSelector"));
		}
		return Singleton;
	}
	template<> REMOTECONTROLPROTOCOLMIDI_API UEnum* StaticEnum<ERemoteControlMIDIDeviceSelector>()
	{
		return ERemoteControlMIDIDeviceSelector_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteControlMIDIDeviceSelector(ERemoteControlMIDIDeviceSelector_StaticEnum, TEXT("/Script/RemoteControlProtocolMIDI"), TEXT("ERemoteControlMIDIDeviceSelector"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteControlProtocolMIDI_ERemoteControlMIDIDeviceSelector_Hash() { return 1237587461U; }
	UEnum* Z_Construct_UEnum_RemoteControlProtocolMIDI_ERemoteControlMIDIDeviceSelector()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolMIDI();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteControlMIDIDeviceSelector"), 0, Get_Z_Construct_UEnum_RemoteControlProtocolMIDI_ERemoteControlMIDIDeviceSelector_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteControlMIDIDeviceSelector::ProjectSettings", (int64)ERemoteControlMIDIDeviceSelector::ProjectSettings },
				{ "ERemoteControlMIDIDeviceSelector::DeviceName", (int64)ERemoteControlMIDIDeviceSelector::DeviceName },
				{ "ERemoteControlMIDIDeviceSelector::DeviceId", (int64)ERemoteControlMIDIDeviceSelector::DeviceId },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * MIDI protocol device selector type\n */" },
				{ "DeviceId.DisplayName", "Device Id" },
				{ "DeviceId.Name", "ERemoteControlMIDIDeviceSelector::DeviceId" },
				{ "DeviceId.ToolTip", "User-specified device id." },
				{ "DeviceName.DisplayName", "Device Name" },
				{ "DeviceName.Name", "ERemoteControlMIDIDeviceSelector::DeviceName" },
				{ "DeviceName.ToolTip", "User-specified device name." },
				{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
				{ "ProjectSettings.DisplayName", "Use Project Settings" },
				{ "ProjectSettings.Name", "ERemoteControlMIDIDeviceSelector::ProjectSettings" },
				{ "ProjectSettings.ToolTip", "Uses the Default MIDI Device specified in Project Settings." },
				{ "ToolTip", "MIDI protocol device selector type" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteControlProtocolMIDI,
				nullptr,
				"ERemoteControlMIDIDeviceSelector",
				"ERemoteControlMIDIDeviceSelector",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FRemoteControlMIDIProtocolEntity>() == std::is_polymorphic<FRemoteControlProtocolEntity>(), "USTRUCT FRemoteControlMIDIProtocolEntity cannot be polymorphic unless super FRemoteControlProtocolEntity is polymorphic");

class UScriptStruct* FRemoteControlMIDIProtocolEntity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLPROTOCOLMIDI_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity, Z_Construct_UPackage__Script_RemoteControlProtocolMIDI(), TEXT("RemoteControlMIDIProtocolEntity"), sizeof(FRemoteControlMIDIProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLPROTOCOLMIDI_API UScriptStruct* StaticStruct<FRemoteControlMIDIProtocolEntity>()
{
	return FRemoteControlMIDIProtocolEntity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlMIDIProtocolEntity(FRemoteControlMIDIProtocolEntity::StaticStruct, TEXT("/Script/RemoteControlProtocolMIDI"), TEXT("RemoteControlMIDIProtocolEntity"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlProtocolMIDI_StaticRegisterNativesFRemoteControlMIDIProtocolEntity
{
	FScriptStruct_RemoteControlProtocolMIDI_StaticRegisterNativesFRemoteControlMIDIProtocolEntity()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlMIDIProtocolEntity>(FName(TEXT("RemoteControlMIDIProtocolEntity")));
	}
} ScriptStruct_RemoteControlProtocolMIDI_StaticRegisterNativesFRemoteControlMIDIProtocolEntity;
	struct Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Device_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Device;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageData1_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MessageData1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Channel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeInputTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RangeInputTemplate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * MIDI protocol entity for remote control binding\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "MIDI protocol entity for remote control binding" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlMIDIProtocolEntity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Device_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Midi Device */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi Device" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Device = { "Device", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIProtocolEntity, Device), Z_Construct_UScriptStruct_FRemoteControlMIDIDevice, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Device_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Device_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Midi Event type */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi Event type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType = { "EventType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIProtocolEntity, EventType), Z_Construct_UEnum_MIDIDevice_EMIDIEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_MessageData1_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Midi button event message data id for binding */" },
		{ "DisplayName", "Mapped channel Id" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi button event message data id for binding" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_MessageData1 = { "MessageData1", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIProtocolEntity, MessageData1), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_MessageData1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_MessageData1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Channel_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Midi device channel */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi device channel" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIProtocolEntity, Channel), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Channel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData[] = {
		{ "ClampMax", "127" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Midi range input property template, used for binding. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi range input property template, used for binding." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_RangeInputTemplate = { "RangeInputTemplate", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIProtocolEntity, RangeInputTemplate), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Device,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_EventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_MessageData1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::NewProp_RangeInputTemplate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolMIDI,
		Z_Construct_UScriptStruct_FRemoteControlProtocolEntity,
		&NewStructOps,
		"RemoteControlMIDIProtocolEntity",
		sizeof(FRemoteControlMIDIProtocolEntity),
		alignof(FRemoteControlMIDIProtocolEntity),
		Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolMIDI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlMIDIProtocolEntity"), sizeof(FRemoteControlMIDIProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlMIDIProtocolEntity_Hash() { return 2236349006U; }
class UScriptStruct* FRemoteControlMIDIDevice::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLPROTOCOLMIDI_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice, Z_Construct_UPackage__Script_RemoteControlProtocolMIDI(), TEXT("RemoteControlMIDIDevice"), sizeof(FRemoteControlMIDIDevice), Get_Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLPROTOCOLMIDI_API UScriptStruct* StaticStruct<FRemoteControlMIDIDevice>()
{
	return FRemoteControlMIDIDevice::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlMIDIDevice(FRemoteControlMIDIDevice::StaticStruct, TEXT("/Script/RemoteControlProtocolMIDI"), TEXT("RemoteControlMIDIDevice"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlProtocolMIDI_StaticRegisterNativesFRemoteControlMIDIDevice
{
	FScriptStruct_RemoteControlProtocolMIDI_StaticRegisterNativesFRemoteControlMIDIDevice()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlMIDIDevice>(FName(TEXT("RemoteControlMIDIDevice")));
	}
} ScriptStruct_RemoteControlProtocolMIDI_StaticRegisterNativesFRemoteControlMIDIDevice;
	struct Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DeviceSelector_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceSelector_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeviceSelector;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResolvedDeviceId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ResolvedDeviceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DeviceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeviceIsAvailable_MetaData[];
#endif
		static void NewProp_bDeviceIsAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeviceIsAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * MIDI protocol device identifier\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "MIDI protocol device identifier" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlMIDIDevice>();
	}
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Midi Device Selector */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi Device Selector" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector = { "DeviceSelector", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIDevice, DeviceSelector), Z_Construct_UEnum_RemoteControlProtocolMIDI_ERemoteControlMIDIDeviceSelector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_ResolvedDeviceId_MetaData[] = {
		{ "Category", "Mapping" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Midi Resolved Device Id. Distinct from the user specified Device Id. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi Resolved Device Id. Distinct from the user specified Device Id." },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_ResolvedDeviceId = { "ResolvedDeviceId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIDevice, ResolvedDeviceId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_ResolvedDeviceId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_ResolvedDeviceId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Midi Device Name. If specified, takes priority over DeviceId. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "Midi Device Name. If specified, takes priority over DeviceId." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIDevice, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceId_MetaData[] = {
		{ "Category", "Mapping" },
		{ "ClampMin", "0" },
		{ "Comment", "/** User-specified Midi Device Id */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "User-specified Midi Device Id" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceId = { "DeviceId", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlMIDIDevice, DeviceId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** If device available for use. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolMIDI.h" },
		{ "ToolTip", "If device available for use." },
	};
#endif
	void Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable_SetBit(void* Obj)
	{
		((FRemoteControlMIDIDevice*)Obj)->bDeviceIsAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable = { "bDeviceIsAvailable", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRemoteControlMIDIDevice), &Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceSelector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_ResolvedDeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_DeviceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::NewProp_bDeviceIsAvailable,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolMIDI,
		nullptr,
		&NewStructOps,
		"RemoteControlMIDIDevice",
		sizeof(FRemoteControlMIDIDevice),
		alignof(FRemoteControlMIDIDevice),
		Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlMIDIDevice()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolMIDI();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlMIDIDevice"), sizeof(FRemoteControlMIDIDevice), Get_Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlMIDIDevice_Hash() { return 2146466354U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
