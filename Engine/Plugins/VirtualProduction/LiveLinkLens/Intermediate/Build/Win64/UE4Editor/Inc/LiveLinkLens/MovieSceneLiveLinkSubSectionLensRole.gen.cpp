// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkLens/Private/MovieSceneLiveLinkSubSectionLensRole.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneLiveLinkSubSectionLensRole() {}
// Cross Module References
	LIVELINKLENS_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_NoRegister();
	LIVELINKLENS_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole();
	LIVELINKMOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkSubSection();
	UPackage* Z_Construct_UPackage__Script_LiveLinkLens();
// End Cross Module References
	void UMovieSceneLiveLinkSubSectionLensRole::StaticRegisterNativesUMovieSceneLiveLinkSubSectionLensRole()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_NoRegister()
	{
		return UMovieSceneLiveLinkSubSectionLensRole::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneLiveLinkSubSection,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkLens,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A LiveLinkSubSection managing special properties of the LensRole\n */" },
		{ "IncludePath", "MovieSceneLiveLinkSubSectionLensRole.h" },
		{ "ModuleRelativePath", "Private/MovieSceneLiveLinkSubSectionLensRole.h" },
		{ "ToolTip", "A LiveLinkSubSection managing special properties of the LensRole" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneLiveLinkSubSectionLensRole>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::ClassParams = {
		&UMovieSceneLiveLinkSubSectionLensRole::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneLiveLinkSubSectionLensRole, 1174928476);
	template<> LIVELINKLENS_API UClass* StaticClass<UMovieSceneLiveLinkSubSectionLensRole>()
	{
		return UMovieSceneLiveLinkSubSectionLensRole::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneLiveLinkSubSectionLensRole(Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole, &UMovieSceneLiveLinkSubSectionLensRole::StaticClass, TEXT("/Script/LiveLinkLens"), TEXT("UMovieSceneLiveLinkSubSectionLensRole"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneLiveLinkSubSectionLensRole);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
