// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKLENS_MovieSceneLiveLinkSubSectionLensRole_generated_h
#error "MovieSceneLiveLinkSubSectionLensRole.generated.h already included, missing '#pragma once' in MovieSceneLiveLinkSubSectionLensRole.h"
#endif
#define LIVELINKLENS_MovieSceneLiveLinkSubSectionLensRole_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkSubSectionLensRole(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkSubSectionLensRole, UMovieSceneLiveLinkSubSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkLens"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkSubSectionLensRole)


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkSubSectionLensRole(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkSubSectionLensRole_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkSubSectionLensRole, UMovieSceneLiveLinkSubSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkLens"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkSubSectionLensRole)


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkSubSectionLensRole(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkSubSectionLensRole) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkSubSectionLensRole); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkSubSectionLensRole); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkSubSectionLensRole(UMovieSceneLiveLinkSubSectionLensRole&&); \
	NO_API UMovieSceneLiveLinkSubSectionLensRole(const UMovieSceneLiveLinkSubSectionLensRole&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkSubSectionLensRole(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkSubSectionLensRole(UMovieSceneLiveLinkSubSectionLensRole&&); \
	NO_API UMovieSceneLiveLinkSubSectionLensRole(const UMovieSceneLiveLinkSubSectionLensRole&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkSubSectionLensRole); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkSubSectionLensRole); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkSubSectionLensRole)


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_16_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKLENS_API UClass* StaticClass<class UMovieSceneLiveLinkSubSectionLensRole>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Private_MovieSceneLiveLinkSubSectionLensRole_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
