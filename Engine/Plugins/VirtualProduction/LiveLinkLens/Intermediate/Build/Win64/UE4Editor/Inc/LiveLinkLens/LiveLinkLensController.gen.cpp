// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkLens/Public/LiveLinkLensController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkLensController() {}
// Cross Module References
	LIVELINKLENS_API UClass* Z_Construct_UClass_ULiveLinkLensController_NoRegister();
	LIVELINKLENS_API UClass* Z_Construct_UClass_ULiveLinkLensController();
	LIVELINKCOMPONENTS_API UClass* Z_Construct_UClass_ULiveLinkControllerBase();
	UPackage* Z_Construct_UPackage__Script_LiveLinkLens();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void ULiveLinkLensController::StaticRegisterNativesULiveLinkLensController()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkLensController_NoRegister()
	{
		return ULiveLinkLensController::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkLensController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensDistortionHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LensDistortionHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionProducerID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionProducerID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bScaleOverscan_MetaData[];
#endif
		static void NewProp_bScaleOverscan_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bScaleOverscan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanMultiplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkLensController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkControllerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkLens,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkLensController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * LiveLink Controller for the LensRole to drive lens distortion data \n */" },
		{ "IncludePath", "LiveLinkLensController.h" },
		{ "ModuleRelativePath", "Public/LiveLinkLensController.h" },
		{ "ToolTip", "LiveLink Controller for the LensRole to drive lens distortion data" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_LensDistortionHandler_MetaData[] = {
		{ "Comment", "/** Cached distortion handler associated with attached camera component */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensController.h" },
		{ "ToolTip", "Cached distortion handler associated with attached camera component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_LensDistortionHandler = { "LensDistortionHandler", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkLensController, LensDistortionHandler), Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_LensDistortionHandler_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_LensDistortionHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_DistortionProducerID_MetaData[] = {
		{ "Comment", "/** Unique identifier representing the source of distortion data */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensController.h" },
		{ "ToolTip", "Unique identifier representing the source of distortion data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_DistortionProducerID = { "DistortionProducerID", nullptr, (EPropertyFlags)0x0020080000200000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkLensController, DistortionProducerID), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_DistortionProducerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_DistortionProducerID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** Whether to scale the computed overscan by the overscan percentage */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensController.h" },
		{ "ToolTip", "Whether to scale the computed overscan by the overscan percentage" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan_SetBit(void* Obj)
	{
		((ULiveLinkLensController*)Obj)->bScaleOverscan = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan = { "bScaleOverscan", nullptr, (EPropertyFlags)0x0020080000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkLensController), &Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_OverscanMultiplier_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "ClampMax", "2.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** The percentage of the computed overscan that should be applied to the target camera */" },
		{ "EditCondition", "bScaleOverscan" },
		{ "ModuleRelativePath", "Public/LiveLinkLensController.h" },
		{ "ToolTip", "The percentage of the computed overscan that should be applied to the target camera" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_OverscanMultiplier = { "OverscanMultiplier", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkLensController, OverscanMultiplier), METADATA_PARAMS(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_OverscanMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_OverscanMultiplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkLensController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_LensDistortionHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_DistortionProducerID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_bScaleOverscan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkLensController_Statics::NewProp_OverscanMultiplier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkLensController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkLensController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkLensController_Statics::ClassParams = {
		&ULiveLinkLensController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkLensController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensController_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkLensController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkLensController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkLensController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkLensController, 4234805735);
	template<> LIVELINKLENS_API UClass* StaticClass<ULiveLinkLensController>()
	{
		return ULiveLinkLensController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkLensController(Z_Construct_UClass_ULiveLinkLensController, &ULiveLinkLensController::StaticClass, TEXT("/Script/LiveLinkLens"), TEXT("ULiveLinkLensController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkLensController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
