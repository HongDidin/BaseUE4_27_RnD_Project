// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkLens/Public/LiveLinkLensTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkLensTypes() {}
// Cross Module References
	LIVELINKLENS_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData();
	UPackage* Z_Construct_UPackage__Script_LiveLinkLens();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkBaseBlueprintData();
	LIVELINKLENS_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkLensStaticData();
	LIVELINKLENS_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkLensFrameData();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraFrameData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraStaticData();
// End Cross Module References

static_assert(std::is_polymorphic<FLiveLinkLensBlueprintData>() == std::is_polymorphic<FLiveLinkBaseBlueprintData>(), "USTRUCT FLiveLinkLensBlueprintData cannot be polymorphic unless super FLiveLinkBaseBlueprintData is polymorphic");

class UScriptStruct* FLiveLinkLensBlueprintData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKLENS_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData, Z_Construct_UPackage__Script_LiveLinkLens(), TEXT("LiveLinkLensBlueprintData"), sizeof(FLiveLinkLensBlueprintData), Get_Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Hash());
	}
	return Singleton;
}
template<> LIVELINKLENS_API UScriptStruct* StaticStruct<FLiveLinkLensBlueprintData>()
{
	return FLiveLinkLensBlueprintData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkLensBlueprintData(FLiveLinkLensBlueprintData::StaticStruct, TEXT("/Script/LiveLinkLens"), TEXT("LiveLinkLensBlueprintData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensBlueprintData
{
	FScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensBlueprintData()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkLensBlueprintData>(FName(TEXT("LiveLinkLensBlueprintData")));
	}
} ScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensBlueprintData;
	struct Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StaticData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrameData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Facility structure to handle lens data in blueprint\n */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Facility structure to handle lens data in blueprint" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkLensBlueprintData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_StaticData_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Static data that should not change every frame */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Static data that should not change every frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_StaticData = { "StaticData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkLensBlueprintData, StaticData), Z_Construct_UScriptStruct_FLiveLinkLensStaticData, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_StaticData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_StaticData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_FrameData_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Dynamic data that can change every frame  */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Dynamic data that can change every frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_FrameData = { "FrameData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkLensBlueprintData, FrameData), Z_Construct_UScriptStruct_FLiveLinkLensFrameData, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_FrameData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_FrameData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_StaticData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::NewProp_FrameData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkLens,
		Z_Construct_UScriptStruct_FLiveLinkBaseBlueprintData,
		&NewStructOps,
		"LiveLinkLensBlueprintData",
		sizeof(FLiveLinkLensBlueprintData),
		alignof(FLiveLinkLensBlueprintData),
		Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkLens();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkLensBlueprintData"), sizeof(FLiveLinkLensBlueprintData), Get_Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensBlueprintData_Hash() { return 1186174287U; }

static_assert(std::is_polymorphic<FLiveLinkLensFrameData>() == std::is_polymorphic<FLiveLinkCameraFrameData>(), "USTRUCT FLiveLinkLensFrameData cannot be polymorphic unless super FLiveLinkCameraFrameData is polymorphic");

class UScriptStruct* FLiveLinkLensFrameData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKLENS_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkLensFrameData, Z_Construct_UPackage__Script_LiveLinkLens(), TEXT("LiveLinkLensFrameData"), sizeof(FLiveLinkLensFrameData), Get_Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Hash());
	}
	return Singleton;
}
template<> LIVELINKLENS_API UScriptStruct* StaticStruct<FLiveLinkLensFrameData>()
{
	return FLiveLinkLensFrameData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkLensFrameData(FLiveLinkLensFrameData::StaticStruct, TEXT("/Script/LiveLinkLens"), TEXT("LiveLinkLensFrameData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensFrameData
{
	FScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensFrameData()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkLensFrameData>(FName(TEXT("LiveLinkLensFrameData")));
	}
} ScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensFrameData;
	struct Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistortionParameters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DistortionParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FxFy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FxFy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrincipalPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrincipalPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Struct for dynamic (per-frame) lens data\n */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Struct for dynamic (per-frame) lens data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkLensFrameData>();
	}
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters_Inner = { "DistortionParameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Parameters used by the distortion model */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Parameters used by the distortion model" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters = { "DistortionParameters", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkLensFrameData, DistortionParameters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_FxFy_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Normalized focal length used by the distortion model */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Normalized focal length used by the distortion model" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_FxFy = { "FxFy", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkLensFrameData, FxFy), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_FxFy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_FxFy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_PrincipalPoint_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Normalized center of the image, in the range [0.0f, 1.0f] */" },
		{ "DisplayName", "Image Center" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Normalized center of the image, in the range [0.0f, 1.0f]" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_PrincipalPoint = { "PrincipalPoint", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkLensFrameData, PrincipalPoint), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_PrincipalPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_PrincipalPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_DistortionParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_FxFy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::NewProp_PrincipalPoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkLens,
		Z_Construct_UScriptStruct_FLiveLinkCameraFrameData,
		&NewStructOps,
		"LiveLinkLensFrameData",
		sizeof(FLiveLinkLensFrameData),
		alignof(FLiveLinkLensFrameData),
		Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkLensFrameData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkLens();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkLensFrameData"), sizeof(FLiveLinkLensFrameData), Get_Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensFrameData_Hash() { return 614276094U; }

static_assert(std::is_polymorphic<FLiveLinkLensStaticData>() == std::is_polymorphic<FLiveLinkCameraStaticData>(), "USTRUCT FLiveLinkLensStaticData cannot be polymorphic unless super FLiveLinkCameraStaticData is polymorphic");

class UScriptStruct* FLiveLinkLensStaticData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKLENS_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkLensStaticData, Z_Construct_UPackage__Script_LiveLinkLens(), TEXT("LiveLinkLensStaticData"), sizeof(FLiveLinkLensStaticData), Get_Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Hash());
	}
	return Singleton;
}
template<> LIVELINKLENS_API UScriptStruct* StaticStruct<FLiveLinkLensStaticData>()
{
	return FLiveLinkLensStaticData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkLensStaticData(FLiveLinkLensStaticData::StaticStruct, TEXT("/Script/LiveLinkLens"), TEXT("LiveLinkLensStaticData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensStaticData
{
	FScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensStaticData()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkLensStaticData>(FName(TEXT("LiveLinkLensStaticData")));
	}
} ScriptStruct_LiveLinkLens_StaticRegisterNativesFLiveLinkLensStaticData;
	struct Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensModel_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LensModel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Struct for static lens data\n */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Struct for static lens data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkLensStaticData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::NewProp_LensModel_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Specifies the type/character of the lens (spherical, anamorphic, etc.) */" },
		{ "ModuleRelativePath", "Public/LiveLinkLensTypes.h" },
		{ "ToolTip", "Specifies the type/character of the lens (spherical, anamorphic, etc.)" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::NewProp_LensModel = { "LensModel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkLensStaticData, LensModel), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::NewProp_LensModel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::NewProp_LensModel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::NewProp_LensModel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkLens,
		Z_Construct_UScriptStruct_FLiveLinkCameraStaticData,
		&NewStructOps,
		"LiveLinkLensStaticData",
		sizeof(FLiveLinkLensStaticData),
		alignof(FLiveLinkLensStaticData),
		Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkLensStaticData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkLens();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkLensStaticData"), sizeof(FLiveLinkLensStaticData), Get_Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkLensStaticData_Hash() { return 1685854414U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
