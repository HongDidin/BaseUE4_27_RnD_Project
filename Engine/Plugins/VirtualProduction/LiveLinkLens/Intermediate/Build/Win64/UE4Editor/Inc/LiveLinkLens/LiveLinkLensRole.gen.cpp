// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkLens/Public/LiveLinkLensRole.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkLensRole() {}
// Cross Module References
	LIVELINKLENS_API UClass* Z_Construct_UClass_ULiveLinkLensRole_NoRegister();
	LIVELINKLENS_API UClass* Z_Construct_UClass_ULiveLinkLensRole();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkCameraRole();
	UPackage* Z_Construct_UPackage__Script_LiveLinkLens();
// End Cross Module References
	void ULiveLinkLensRole::StaticRegisterNativesULiveLinkLensRole()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkLensRole_NoRegister()
	{
		return ULiveLinkLensRole::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkLensRole_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkLensRole_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkCameraRole,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkLens,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkLensRole_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Role associated with lens data\n */" },
		{ "DisplayName", "Lens Role" },
		{ "IncludePath", "LiveLinkLensRole.h" },
		{ "ModuleRelativePath", "Public/LiveLinkLensRole.h" },
		{ "ToolTip", "Role associated with lens data" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkLensRole_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkLensRole>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkLensRole_Statics::ClassParams = {
		&ULiveLinkLensRole::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkLensRole_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkLensRole_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkLensRole()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkLensRole_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkLensRole, 2367972894);
	template<> LIVELINKLENS_API UClass* StaticClass<ULiveLinkLensRole>()
	{
		return ULiveLinkLensRole::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkLensRole(Z_Construct_UClass_ULiveLinkLensRole, &ULiveLinkLensRole::StaticClass, TEXT("/Script/LiveLinkLens"), TEXT("ULiveLinkLensRole"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkLensRole);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
