// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKLENS_LiveLinkLensController_generated_h
#error "LiveLinkLensController.generated.h already included, missing '#pragma once' in LiveLinkLensController.h"
#endif
#define LIVELINKLENS_LiveLinkLensController_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkLensController(); \
	friend struct Z_Construct_UClass_ULiveLinkLensController_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkLensController, ULiveLinkControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkLens"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkLensController)


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkLensController(); \
	friend struct Z_Construct_UClass_ULiveLinkLensController_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkLensController, ULiveLinkControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkLens"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkLensController)


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkLensController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkLensController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkLensController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkLensController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkLensController(ULiveLinkLensController&&); \
	NO_API ULiveLinkLensController(const ULiveLinkLensController&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkLensController(ULiveLinkLensController&&); \
	NO_API ULiveLinkLensController(const ULiveLinkLensController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkLensController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkLensController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULiveLinkLensController)


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LensDistortionHandler() { return STRUCT_OFFSET(ULiveLinkLensController, LensDistortionHandler); } \
	FORCEINLINE static uint32 __PPO__DistortionProducerID() { return STRUCT_OFFSET(ULiveLinkLensController, DistortionProducerID); } \
	FORCEINLINE static uint32 __PPO__bScaleOverscan() { return STRUCT_OFFSET(ULiveLinkLensController, bScaleOverscan); } \
	FORCEINLINE static uint32 __PPO__OverscanMultiplier() { return STRUCT_OFFSET(ULiveLinkLensController, OverscanMultiplier); }


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKLENS_API UClass* StaticClass<class ULiveLinkLensController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkLens_Source_LiveLinkLens_Public_LiveLinkLensController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
