// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/BaseLensTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseLensTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensFile_NoRegister();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint();
// End Cross Module References
class UScriptStruct* FBaseLensTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FBaseLensTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBaseLensTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("BaseLensTable"), sizeof(FBaseLensTable), Get_Z_Construct_UScriptStruct_FBaseLensTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FBaseLensTable>()
{
	return FBaseLensTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBaseLensTable(FBaseLensTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("BaseLensTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFBaseLensTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFBaseLensTable()
	{
		UScriptStruct::DeferCppStructOps<FBaseLensTable>(FName(TEXT("BaseLensTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFBaseLensTable;
	struct Z_Construct_UScriptStruct_FBaseLensTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_LensFile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBaseLensTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Base data table struct\n */" },
		{ "ModuleRelativePath", "Public/Tables/BaseLensTable.h" },
		{ "ToolTip", "Base data table struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBaseLensTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBaseLensTable>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBaseLensTable_Statics::NewProp_LensFile_MetaData[] = {
		{ "Comment", "/**\n\x09 * Lens file owner reference\n\x09 */" },
		{ "ModuleRelativePath", "Public/Tables/BaseLensTable.h" },
		{ "ToolTip", "Lens file owner reference" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FBaseLensTable_Statics::NewProp_LensFile = { "LensFile", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBaseLensTable, LensFile), Z_Construct_UClass_ULensFile_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FBaseLensTable_Statics::NewProp_LensFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBaseLensTable_Statics::NewProp_LensFile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBaseLensTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBaseLensTable_Statics::NewProp_LensFile,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBaseLensTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"BaseLensTable",
		sizeof(FBaseLensTable),
		alignof(FBaseLensTable),
		Z_Construct_UScriptStruct_FBaseLensTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBaseLensTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBaseLensTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBaseLensTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBaseLensTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BaseLensTable"), sizeof(FBaseLensTable), Get_Z_Construct_UScriptStruct_FBaseLensTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBaseLensTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBaseLensTable_Hash() { return 2110262485U; }
class UScriptStruct* FBaseFocusPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FBaseFocusPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBaseFocusPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("BaseFocusPoint"), sizeof(FBaseFocusPoint), Get_Z_Construct_UScriptStruct_FBaseFocusPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FBaseFocusPoint>()
{
	return FBaseFocusPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBaseFocusPoint(FBaseFocusPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("BaseFocusPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFBaseFocusPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFBaseFocusPoint()
	{
		UScriptStruct::DeferCppStructOps<FBaseFocusPoint>(FName(TEXT("BaseFocusPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFBaseFocusPoint;
	struct Z_Construct_UScriptStruct_FBaseFocusPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBaseFocusPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Base focus point struct\n */" },
		{ "ModuleRelativePath", "Public/Tables/BaseLensTable.h" },
		{ "ToolTip", "Base focus point struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBaseFocusPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBaseFocusPoint>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBaseFocusPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"BaseFocusPoint",
		sizeof(FBaseFocusPoint),
		alignof(FBaseFocusPoint),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBaseFocusPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBaseFocusPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBaseFocusPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BaseFocusPoint"), sizeof(FBaseFocusPoint), Get_Z_Construct_UScriptStruct_FBaseFocusPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBaseFocusPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBaseFocusPoint_Hash() { return 2550295339U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
