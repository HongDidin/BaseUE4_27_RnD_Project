// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/FocalLengthTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFocalLengthTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthFocusPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthZoomPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthInfo();
// End Cross Module References

static_assert(std::is_polymorphic<FFocalLengthTable>() == std::is_polymorphic<FBaseLensTable>(), "USTRUCT FFocalLengthTable cannot be polymorphic unless super FBaseLensTable is polymorphic");

class UScriptStruct* FFocalLengthTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FFocalLengthTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFocalLengthTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("FocalLengthTable"), sizeof(FFocalLengthTable), Get_Z_Construct_UScriptStruct_FFocalLengthTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FFocalLengthTable>()
{
	return FFocalLengthTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFocalLengthTable(FFocalLengthTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("FocalLengthTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthTable()
	{
		UScriptStruct::DeferCppStructOps<FFocalLengthTable>(FName(TEXT("FocalLengthTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthTable;
	struct Z_Construct_UScriptStruct_FFocalLengthTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocusPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Focal Length table containing FxFy values for each focus and zoom input values\n */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Focal Length table containing FxFy values for each focus and zoom input values" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFocalLengthTable>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints_Inner = { "FocusPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFocalLengthFocusPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints_MetaData[] = {
		{ "Comment", "/** Lists of focus points */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Lists of focus points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints = { "FocusPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthTable, FocusPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFocalLengthTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthTable_Statics::NewProp_FocusPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFocalLengthTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseLensTable,
		&NewStructOps,
		"FocalLengthTable",
		sizeof(FFocalLengthTable),
		alignof(FFocalLengthTable),
		Z_Construct_UScriptStruct_FFocalLengthTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFocalLengthTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FocalLengthTable"), sizeof(FFocalLengthTable), Get_Z_Construct_UScriptStruct_FFocalLengthTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFocalLengthTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFocalLengthTable_Hash() { return 1846778414U; }

static_assert(std::is_polymorphic<FFocalLengthFocusPoint>() == std::is_polymorphic<FBaseFocusPoint>(), "USTRUCT FFocalLengthFocusPoint cannot be polymorphic unless super FBaseFocusPoint is polymorphic");

class UScriptStruct* FFocalLengthFocusPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFocalLengthFocusPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("FocalLengthFocusPoint"), sizeof(FFocalLengthFocusPoint), Get_Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FFocalLengthFocusPoint>()
{
	return FFocalLengthFocusPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFocalLengthFocusPoint(FFocalLengthFocusPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("FocalLengthFocusPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthFocusPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthFocusPoint()
	{
		UScriptStruct::DeferCppStructOps<FFocalLengthFocusPoint>(FName(TEXT("FocalLengthFocusPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthFocusPoint;
	struct Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fx_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Fx;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Fy;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ZoomPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZoomPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ZoomPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Contains list of focal length points associated to zoom value\n */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Contains list of focal length points associated to zoom value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFocalLengthFocusPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Focus_MetaData[] = {
		{ "Comment", "/** Input focus for this point */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Input focus for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthFocusPoint, Focus), METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fx_MetaData[] = {
		{ "Comment", "/** Curves mapping normalized Fx value to Zoom value (Time) */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Curves mapping normalized Fx value to Zoom value (Time)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fx = { "Fx", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthFocusPoint, Fx), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fx_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fx_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fy_MetaData[] = {
		{ "Comment", "/** Curves mapping normalized Fy value to Zoom value (Time) */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Curves mapping normalized Fy value to Zoom value (Time)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fy = { "Fy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthFocusPoint, Fy), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fy_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints_Inner = { "ZoomPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFocalLengthZoomPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints_MetaData[] = {
		{ "Comment", "/** Used to know points that are locked */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Used to know points that are locked" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints = { "ZoomPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthFocusPoint, ZoomPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fx,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_Fy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::NewProp_ZoomPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseFocusPoint,
		&NewStructOps,
		"FocalLengthFocusPoint",
		sizeof(FFocalLengthFocusPoint),
		alignof(FFocalLengthFocusPoint),
		Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthFocusPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FocalLengthFocusPoint"), sizeof(FFocalLengthFocusPoint), Get_Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Hash() { return 1571523045U; }
class UScriptStruct* FFocalLengthZoomPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFocalLengthZoomPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("FocalLengthZoomPoint"), sizeof(FFocalLengthZoomPoint), Get_Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FFocalLengthZoomPoint>()
{
	return FFocalLengthZoomPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFocalLengthZoomPoint(FFocalLengthZoomPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("FocalLengthZoomPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthZoomPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthZoomPoint()
	{
		UScriptStruct::DeferCppStructOps<FFocalLengthZoomPoint>(FName(TEXT("FocalLengthZoomPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthZoomPoint;
	struct Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Zoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Zoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLengthInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCalibrationPoint_MetaData[];
#endif
		static void NewProp_bIsCalibrationPoint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCalibrationPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Focal length associated to a zoom value\n */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Focal length associated to a zoom value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFocalLengthZoomPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_Zoom_MetaData[] = {
		{ "Comment", "/** Input zoom value for this point */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Input zoom value for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_Zoom = { "Zoom", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthZoomPoint, Zoom), METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_Zoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_Zoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_FocalLengthInfo_MetaData[] = {
		{ "Comment", "/** Value expected to be normalized (unitless) */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Value expected to be normalized (unitless)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_FocalLengthInfo = { "FocalLengthInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthZoomPoint, FocalLengthInfo), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_FocalLengthInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_FocalLengthInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint_MetaData[] = {
		{ "Comment", "/** Whether this focal length was added along calibrated distortion parameters */" },
		{ "ModuleRelativePath", "Public/Tables/FocalLengthTable.h" },
		{ "ToolTip", "Whether this focal length was added along calibrated distortion parameters" },
	};
#endif
	void Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint_SetBit(void* Obj)
	{
		((FFocalLengthZoomPoint*)Obj)->bIsCalibrationPoint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint = { "bIsCalibrationPoint", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFocalLengthZoomPoint), &Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_Zoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_FocalLengthInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::NewProp_bIsCalibrationPoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"FocalLengthZoomPoint",
		sizeof(FFocalLengthZoomPoint),
		alignof(FFocalLengthZoomPoint),
		Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthZoomPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FocalLengthZoomPoint"), sizeof(FFocalLengthZoomPoint), Get_Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Hash() { return 2476367132U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
