// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/STMapTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSTMapTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapFocusPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapZoomPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDerivedDistortionData();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionData();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FSTMapTable>() == std::is_polymorphic<FBaseLensTable>(), "USTRUCT FSTMapTable cannot be polymorphic unless super FBaseLensTable is polymorphic");

class UScriptStruct* FSTMapTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FSTMapTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSTMapTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("STMapTable"), sizeof(FSTMapTable), Get_Z_Construct_UScriptStruct_FSTMapTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FSTMapTable>()
{
	return FSTMapTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSTMapTable(FSTMapTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("STMapTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapTable()
	{
		UScriptStruct::DeferCppStructOps<FSTMapTable>(FName(TEXT("STMapTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapTable;
	struct Z_Construct_UScriptStruct_FSTMapTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocusPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * STMap table containing list of points for each focus and zoom inputs\n */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "STMap table containing list of points for each focus and zoom inputs" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSTMapTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSTMapTable>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints_Inner = { "FocusPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSTMapFocusPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints_MetaData[] = {
		{ "Comment", "/** Lists of focus points */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Lists of focus points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints = { "FocusPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapTable, FocusPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSTMapTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapTable_Statics::NewProp_FocusPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSTMapTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseLensTable,
		&NewStructOps,
		"STMapTable",
		sizeof(FSTMapTable),
		alignof(FSTMapTable),
		Z_Construct_UScriptStruct_FSTMapTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSTMapTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSTMapTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("STMapTable"), sizeof(FSTMapTable), Get_Z_Construct_UScriptStruct_FSTMapTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSTMapTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSTMapTable_Hash() { return 1755119402U; }

static_assert(std::is_polymorphic<FSTMapFocusPoint>() == std::is_polymorphic<FBaseFocusPoint>(), "USTRUCT FSTMapFocusPoint cannot be polymorphic unless super FBaseFocusPoint is polymorphic");

class UScriptStruct* FSTMapFocusPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FSTMapFocusPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSTMapFocusPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("STMapFocusPoint"), sizeof(FSTMapFocusPoint), Get_Z_Construct_UScriptStruct_FSTMapFocusPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FSTMapFocusPoint>()
{
	return FSTMapFocusPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSTMapFocusPoint(FSTMapFocusPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("STMapFocusPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapFocusPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapFocusPoint()
	{
		UScriptStruct::DeferCppStructOps<FSTMapFocusPoint>(FName(TEXT("STMapFocusPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapFocusPoint;
	struct Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapBlendingCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MapBlendingCurve;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ZoomPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZoomPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ZoomPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * A data point associating focus and zoom to lens parameters\n */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "A data point associating focus and zoom to lens parameters" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSTMapFocusPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_Focus_MetaData[] = {
		{ "Comment", "/** Input focus for this point */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Input focus for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapFocusPoint, Focus), METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_MapBlendingCurve_MetaData[] = {
		{ "Comment", "/** Curve used to blend displacement map together to give user more flexibility */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Curve used to blend displacement map together to give user more flexibility" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_MapBlendingCurve = { "MapBlendingCurve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapFocusPoint, MapBlendingCurve), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_MapBlendingCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_MapBlendingCurve_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints_Inner = { "ZoomPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSTMapZoomPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints_MetaData[] = {
		{ "Comment", "/** Zoom points for this focus */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Zoom points for this focus" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints = { "ZoomPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapFocusPoint, ZoomPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_MapBlendingCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::NewProp_ZoomPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseFocusPoint,
		&NewStructOps,
		"STMapFocusPoint",
		sizeof(FSTMapFocusPoint),
		alignof(FSTMapFocusPoint),
		Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSTMapFocusPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSTMapFocusPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("STMapFocusPoint"), sizeof(FSTMapFocusPoint), Get_Z_Construct_UScriptStruct_FSTMapFocusPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSTMapFocusPoint_Hash() { return 774690687U; }
class UScriptStruct* FSTMapZoomPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FSTMapZoomPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSTMapZoomPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("STMapZoomPoint"), sizeof(FSTMapZoomPoint), Get_Z_Construct_UScriptStruct_FSTMapZoomPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FSTMapZoomPoint>()
{
	return FSTMapZoomPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSTMapZoomPoint(FSTMapZoomPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("STMapZoomPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapZoomPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapZoomPoint()
	{
		UScriptStruct::DeferCppStructOps<FSTMapZoomPoint>(FName(TEXT("STMapZoomPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapZoomPoint;
	struct Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Zoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Zoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_STMapInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_STMapInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DerivedDistortionData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DerivedDistortionData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCalibrationPoint_MetaData[];
#endif
		static void NewProp_bIsCalibrationPoint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCalibrationPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * STMap data associated to a zoom input value\n */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "STMap data associated to a zoom input value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSTMapZoomPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_Zoom_MetaData[] = {
		{ "Comment", "/** Input zoom value for this point */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Input zoom value for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_Zoom = { "Zoom", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapZoomPoint, Zoom), METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_Zoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_Zoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_STMapInfo_MetaData[] = {
		{ "Comment", "/** Data for this zoom point */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Data for this zoom point" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_STMapInfo = { "STMapInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapZoomPoint, STMapInfo), Z_Construct_UScriptStruct_FSTMapInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_STMapInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_STMapInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_DerivedDistortionData_MetaData[] = {
		{ "Comment", "/** Derived distortion data associated with this point */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Derived distortion data associated with this point" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_DerivedDistortionData = { "DerivedDistortionData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapZoomPoint, DerivedDistortionData), Z_Construct_UScriptStruct_FDerivedDistortionData, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_DerivedDistortionData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_DerivedDistortionData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint_MetaData[] = {
		{ "Comment", "/** Whether this point was added in calibration along distortion */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Whether this point was added in calibration along distortion" },
	};
#endif
	void Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint_SetBit(void* Obj)
	{
		((FSTMapZoomPoint*)Obj)->bIsCalibrationPoint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint = { "bIsCalibrationPoint", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSTMapZoomPoint), &Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_Zoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_STMapInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_DerivedDistortionData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::NewProp_bIsCalibrationPoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"STMapZoomPoint",
		sizeof(FSTMapZoomPoint),
		alignof(FSTMapZoomPoint),
		Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSTMapZoomPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSTMapZoomPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("STMapZoomPoint"), sizeof(FSTMapZoomPoint), Get_Z_Construct_UScriptStruct_FSTMapZoomPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSTMapZoomPoint_Hash() { return 1062372894U; }
class UScriptStruct* FDerivedDistortionData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDerivedDistortionData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDerivedDistortionData, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DerivedDistortionData"), sizeof(FDerivedDistortionData), Get_Z_Construct_UScriptStruct_FDerivedDistortionData_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDerivedDistortionData>()
{
	return FDerivedDistortionData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDerivedDistortionData(FDerivedDistortionData::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DerivedDistortionData"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDerivedDistortionData
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDerivedDistortionData()
	{
		UScriptStruct::DeferCppStructOps<FDerivedDistortionData>(FName(TEXT("DerivedDistortionData")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDerivedDistortionData;
	struct Z_Construct_UScriptStruct_FDerivedDistortionData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UndistortionDisplacementMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UndistortionDisplacementMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionDisplacementMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DistortionDisplacementMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Derived data computed from parameters or stmap\n */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Derived data computed from parameters or stmap" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDerivedDistortionData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionData_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Precomputed data about distortion */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Precomputed data about distortion" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionData = { "DistortionData", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDerivedDistortionData, DistortionData), Z_Construct_UScriptStruct_FDistortionData, METADATA_PARAMS(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_UndistortionDisplacementMap_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Computed displacement map based on undistortion data */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Computed displacement map based on undistortion data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_UndistortionDisplacementMap = { "UndistortionDisplacementMap", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDerivedDistortionData, UndistortionDisplacementMap), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_UndistortionDisplacementMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_UndistortionDisplacementMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionDisplacementMap_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Computed displacement map based on distortion data */" },
		{ "ModuleRelativePath", "Public/Tables/STMapTable.h" },
		{ "ToolTip", "Computed displacement map based on distortion data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionDisplacementMap = { "DistortionDisplacementMap", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDerivedDistortionData, DistortionDisplacementMap), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionDisplacementMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionDisplacementMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_UndistortionDisplacementMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::NewProp_DistortionDisplacementMap,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"DerivedDistortionData",
		sizeof(FDerivedDistortionData),
		alignof(FDerivedDistortionData),
		Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDerivedDistortionData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDerivedDistortionData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DerivedDistortionData"), sizeof(FDerivedDistortionData), Get_Z_Construct_UScriptStruct_FDerivedDistortionData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDerivedDistortionData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDerivedDistortionData_Hash() { return 491572832U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
