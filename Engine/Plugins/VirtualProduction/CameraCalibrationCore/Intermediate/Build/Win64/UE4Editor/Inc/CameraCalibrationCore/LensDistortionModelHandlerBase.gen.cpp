// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/LensDistortionModelHandlerBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensDistortionModelHandlerBase() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensDistortionState();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterInfo();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensModel_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FLensDistortionState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FLensDistortionState_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLensDistortionState, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("LensDistortionState"), sizeof(FLensDistortionState), Get_Z_Construct_UScriptStruct_FLensDistortionState_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FLensDistortionState>()
{
	return FLensDistortionState::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLensDistortionState(FLensDistortionState::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("LensDistortionState"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensDistortionState
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensDistortionState()
	{
		UScriptStruct::DeferCppStructOps<FLensDistortionState>(FName(TEXT("LensDistortionState")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensDistortionState;
	struct Z_Construct_UScriptStruct_FLensDistortionState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLengthInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageCenter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageCenter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDistortionState_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLensDistortionState>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_DistortionInfo_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Generic array of distortion parameters */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Generic array of distortion parameters" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_DistortionInfo = { "DistortionInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDistortionState, DistortionInfo), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_DistortionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_DistortionInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_FocalLengthInfo_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Normalized focal fength in both dimensions */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Normalized focal fength in both dimensions" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_FocalLengthInfo = { "FocalLengthInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDistortionState, FocalLengthInfo), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_FocalLengthInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_FocalLengthInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_ImageCenter_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Normalized center of the image, in the range [0.0f, 1.0f] */" },
		{ "DisplayName", "Image Center" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Normalized center of the image, in the range [0.0f, 1.0f]" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_ImageCenter = { "ImageCenter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDistortionState, ImageCenter), Z_Construct_UScriptStruct_FImageCenterInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_ImageCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_ImageCenter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLensDistortionState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_DistortionInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_FocalLengthInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDistortionState_Statics::NewProp_ImageCenter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLensDistortionState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"LensDistortionState",
		sizeof(FLensDistortionState),
		alignof(FLensDistortionState),
		Z_Construct_UScriptStruct_FLensDistortionState_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDistortionState_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDistortionState_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDistortionState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLensDistortionState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLensDistortionState_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LensDistortionState"), sizeof(FLensDistortionState), Get_Z_Construct_UScriptStruct_FLensDistortionState_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLensDistortionState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLensDistortionState_Hash() { return 2539682783U; }
	DEFINE_FUNCTION(ULensDistortionModelHandlerBase::execGetDistortionDisplacementMap)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTextureRenderTarget2D**)Z_Param__Result=P_THIS->GetDistortionDisplacementMap();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensDistortionModelHandlerBase::execGetUndistortionDisplacementMap)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTextureRenderTarget2D**)Z_Param__Result=P_THIS->GetUndistortionDisplacementMap();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensDistortionModelHandlerBase::execSetDistortionState)
	{
		P_GET_STRUCT_REF(FLensDistortionState,Z_Param_Out_InNewState);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDistortionState(Z_Param_Out_InNewState);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensDistortionModelHandlerBase::execIsModelSupported)
	{
		P_GET_OBJECT_REF_NO_PTR(TSubclassOf<ULensModel> ,Z_Param_Out_ModelToSupport);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsModelSupported(Z_Param_Out_ModelToSupport);
		P_NATIVE_END;
	}
	void ULensDistortionModelHandlerBase::StaticRegisterNativesULensDistortionModelHandlerBase()
	{
		UClass* Class = ULensDistortionModelHandlerBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDistortionDisplacementMap", &ULensDistortionModelHandlerBase::execGetDistortionDisplacementMap },
			{ "GetUndistortionDisplacementMap", &ULensDistortionModelHandlerBase::execGetUndistortionDisplacementMap },
			{ "IsModelSupported", &ULensDistortionModelHandlerBase::execIsModelSupported },
			{ "SetDistortionState", &ULensDistortionModelHandlerBase::execSetDistortionState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics
	{
		struct LensDistortionModelHandlerBase_eventGetDistortionDisplacementMap_Parms
		{
			UTextureRenderTarget2D* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensDistortionModelHandlerBase_eventGetDistortionDisplacementMap_Parms, ReturnValue), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Get the UV displacement map used to distort an undistorted image */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Get the UV displacement map used to distort an undistorted image" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensDistortionModelHandlerBase, nullptr, "GetDistortionDisplacementMap", nullptr, nullptr, sizeof(LensDistortionModelHandlerBase_eventGetDistortionDisplacementMap_Parms), Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics
	{
		struct LensDistortionModelHandlerBase_eventGetUndistortionDisplacementMap_Parms
		{
			UTextureRenderTarget2D* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensDistortionModelHandlerBase_eventGetUndistortionDisplacementMap_Parms, ReturnValue), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Get the UV displacement map used to undistort a distorted image */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Get the UV displacement map used to undistort a distorted image" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensDistortionModelHandlerBase, nullptr, "GetUndistortionDisplacementMap", nullptr, nullptr, sizeof(LensDistortionModelHandlerBase_eventGetUndistortionDisplacementMap_Parms), Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics
	{
		struct LensDistortionModelHandlerBase_eventIsModelSupported_Parms
		{
			const TSubclassOf<ULensModel>  ModelToSupport;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModelToSupport_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ModelToSupport;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ModelToSupport_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ModelToSupport = { "ModelToSupport", nullptr, (EPropertyFlags)0x0014000008000182, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensDistortionModelHandlerBase_eventIsModelSupported_Parms, ModelToSupport), Z_Construct_UClass_ULensModel_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ModelToSupport_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ModelToSupport_MetaData)) };
	void Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensDistortionModelHandlerBase_eventIsModelSupported_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensDistortionModelHandlerBase_eventIsModelSupported_Parms), &Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ModelToSupport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::Function_MetaDataParams[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Returns true if the input model is supported by this model handler, false otherwise. */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Returns true if the input model is supported by this model handler, false otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensDistortionModelHandlerBase, nullptr, "IsModelSupported", nullptr, nullptr, sizeof(LensDistortionModelHandlerBase_eventIsModelSupported_Parms), Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics
	{
		struct LensDistortionModelHandlerBase_eventSetDistortionState_Parms
		{
			FLensDistortionState InNewState;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNewState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InNewState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::NewProp_InNewState_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::NewProp_InNewState = { "InNewState", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensDistortionModelHandlerBase_eventSetDistortionState_Parms, InNewState), Z_Construct_UScriptStruct_FLensDistortionState, METADATA_PARAMS(Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::NewProp_InNewState_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::NewProp_InNewState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::NewProp_InNewState,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Update the lens distortion state, recompute the overscan factor, and set all material parameters */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Update the lens distortion state, recompute the overscan factor, and set all material parameters" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensDistortionModelHandlerBase, nullptr, "SetDistortionState", nullptr, nullptr, sizeof(LensDistortionModelHandlerBase_eventSetDistortionState_Parms), Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister()
	{
		return ULensDistortionModelHandlerBase::StaticClass();
	}
	struct Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensModelClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_LensModelClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionPostProcessMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DistortionPostProcessMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UndistortionDisplacementMapMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UndistortionDisplacementMapMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionDisplacementMapMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DistortionDisplacementMapMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UndistortionDisplacementMapRT_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UndistortionDisplacementMapRT;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionDisplacementMapRT_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DistortionDisplacementMapRT;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionProducerID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionProducerID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetDistortionDisplacementMap, "GetDistortionDisplacementMap" }, // 1208553193
		{ &Z_Construct_UFunction_ULensDistortionModelHandlerBase_GetUndistortionDisplacementMap, "GetUndistortionDisplacementMap" }, // 1658927739
		{ &Z_Construct_UFunction_ULensDistortionModelHandlerBase_IsModelSupported, "IsModelSupported" }, // 2084072899
		{ &Z_Construct_UFunction_ULensDistortionModelHandlerBase_SetDistortionState, "SetDistortionState" }, // 1368902571
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Asset user data that can be used on Camera Actors to manage lens distortion state and utilities  */" },
		{ "IncludePath", "LensDistortionModelHandlerBase.h" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Asset user data that can be used on Camera Actors to manage lens distortion state and utilities" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_LensModelClass_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Lens Model describing how to interpret the distortion parameters */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Lens Model describing how to interpret the distortion parameters" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_LensModelClass = { "LensModelClass", nullptr, (EPropertyFlags)0x0024080000020005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, LensModelClass), Z_Construct_UClass_ULensModel_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_LensModelClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_LensModelClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionPostProcessMID_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Dynamically created post-process material instance for the currently specified lens model */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Dynamically created post-process material instance for the currently specified lens model" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionPostProcessMID = { "DistortionPostProcessMID", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionPostProcessMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionPostProcessMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionPostProcessMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_CurrentState_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Current state as set by the most recent call to Update() */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Current state as set by the most recent call to Update()" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_CurrentState = { "CurrentState", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, CurrentState), Z_Construct_UScriptStruct_FLensDistortionState, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_CurrentState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_CurrentState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Display name, used to identify handler in-editor details panels */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Display name, used to identify handler in-editor details panels" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, DisplayName), METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_OverscanFactor_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Computed overscan factor needed to scale the camera's FOV (read-only) */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "Computed overscan factor needed to scale the camera's FOV (read-only)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_OverscanFactor = { "OverscanFactor", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, OverscanFactor), METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_OverscanFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_OverscanFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapMID_MetaData[] = {
		{ "Comment", "/** MID used to draw the undistortion displacement map */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "MID used to draw the undistortion displacement map" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapMID = { "UndistortionDisplacementMapMID", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, UndistortionDisplacementMapMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapMID_MetaData[] = {
		{ "Comment", "/** MID used to draw the distortion displacement map */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "MID used to draw the distortion displacement map" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapMID = { "DistortionDisplacementMapMID", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionDisplacementMapMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapRT_MetaData[] = {
		{ "Comment", "/** UV displacement map used to undistort a distorted image */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "UV displacement map used to undistort a distorted image" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapRT = { "UndistortionDisplacementMapRT", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, UndistortionDisplacementMapRT), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapRT_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapRT_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapRT_MetaData[] = {
		{ "Comment", "/** UV displacement map used to distort an undistorted image */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "UV displacement map used to distort an undistorted image" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapRT = { "DistortionDisplacementMapRT", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionDisplacementMapRT), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapRT_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapRT_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionProducerID_MetaData[] = {
		{ "Comment", "/** UObject that is producing the distortion state for this handler */" },
		{ "ModuleRelativePath", "Public/LensDistortionModelHandlerBase.h" },
		{ "ToolTip", "UObject that is producing the distortion state for this handler" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionProducerID = { "DistortionProducerID", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionProducerID), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionProducerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionProducerID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_LensModelClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionPostProcessMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_CurrentState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_OverscanFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_UndistortionDisplacementMapRT,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionDisplacementMapRT,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::NewProp_DistortionProducerID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensDistortionModelHandlerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::ClassParams = {
		&ULensDistortionModelHandlerBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensDistortionModelHandlerBase, 938446117);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<ULensDistortionModelHandlerBase>()
	{
		return ULensDistortionModelHandlerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensDistortionModelHandlerBase(Z_Construct_UClass_ULensDistortionModelHandlerBase, &ULensDistortionModelHandlerBase::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("ULensDistortionModelHandlerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensDistortionModelHandlerBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
