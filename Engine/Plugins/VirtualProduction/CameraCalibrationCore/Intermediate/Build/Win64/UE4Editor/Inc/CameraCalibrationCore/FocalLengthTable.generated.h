// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_FocalLengthTable_generated_h
#error "FocalLengthTable.generated.h already included, missing '#pragma once' in FocalLengthTable.h"
#endif
#define CAMERACALIBRATIONCORE_FocalLengthTable_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_FocalLengthTable_h_93_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFocalLengthTable_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBaseLensTable Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FFocalLengthTable>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_FocalLengthTable_h_41_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFocalLengthFocusPoint_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBaseFocusPoint Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FFocalLengthFocusPoint>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_FocalLengthTable_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFocalLengthZoomPoint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FFocalLengthZoomPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_FocalLengthTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
