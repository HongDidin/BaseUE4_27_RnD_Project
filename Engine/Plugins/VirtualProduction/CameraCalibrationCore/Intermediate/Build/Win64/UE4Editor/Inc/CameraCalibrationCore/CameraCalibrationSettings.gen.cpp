// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/CameraCalibrationSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraCalibrationSettings() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensDataCategoryEditorColor();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationSettings_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensFile_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationEditorSettings_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationEditorSettings();
// End Cross Module References
class UScriptStruct* FLensDataCategoryEditorColor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("LensDataCategoryEditorColor"), sizeof(FLensDataCategoryEditorColor), Get_Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FLensDataCategoryEditorColor>()
{
	return FLensDataCategoryEditorColor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLensDataCategoryEditorColor(FLensDataCategoryEditorColor::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("LensDataCategoryEditorColor"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensDataCategoryEditorColor
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensDataCategoryEditorColor()
	{
		UScriptStruct::DeferCppStructOps<FLensDataCategoryEditorColor>(FName(TEXT("LensDataCategoryEditorColor")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensDataCategoryEditorColor;
	struct Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Iris_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Iris;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Zoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Zoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distortion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Distortion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageCenter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageCenter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_STMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_STMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodalOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NodalOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Lens Data Table Editor Category color. Using for the color of the curves\n*/" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Lens Data Table Editor Category color. Using for the color of the curves" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLensDataCategoryEditorColor>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Focus_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, Focus), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Iris_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Iris = { "Iris", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, Iris), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Iris_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Iris_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Zoom_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Zoom = { "Zoom", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, Zoom), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Zoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Zoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Distortion_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Distortion = { "Distortion", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, Distortion), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Distortion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Distortion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_ImageCenter_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_ImageCenter = { "ImageCenter", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, ImageCenter), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_ImageCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_ImageCenter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_STMap_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_STMap = { "STMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, STMap), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_STMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_STMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_NodalOffset_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_NodalOffset = { "NodalOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensDataCategoryEditorColor, NodalOffset), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_NodalOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_NodalOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Iris,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Zoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_Distortion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_ImageCenter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_STMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::NewProp_NodalOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"LensDataCategoryEditorColor",
		sizeof(FLensDataCategoryEditorColor),
		alignof(FLensDataCategoryEditorColor),
		Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLensDataCategoryEditorColor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LensDataCategoryEditorColor"), sizeof(FLensDataCategoryEditorColor), Get_Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLensDataCategoryEditorColor_Hash() { return 3120327808U; }
	void UCameraCalibrationSettings::StaticRegisterNativesUCameraCalibrationSettings()
	{
	}
	UClass* Z_Construct_UClass_UCameraCalibrationSettings_NoRegister()
	{
		return UCameraCalibrationSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCameraCalibrationSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartupLensFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_StartupLensFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementMapResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DisplacementMapResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalibrationInputTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CalibrationInputTolerance;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultUndistortionDisplacementMaterials_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultUndistortionDisplacementMaterials_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultUndistortionDisplacementMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_DefaultUndistortionDisplacementMaterials;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultDistortionDisplacementMaterials_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultDistortionDisplacementMaterials_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDistortionDisplacementMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_DefaultDistortionDisplacementMaterials;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultDistortionMaterials_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultDistortionMaterials_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDistortionMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_DefaultDistortionMaterials;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraCalibrationSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the CameraCalibration plugin modules. \n */" },
		{ "IncludePath", "CameraCalibrationSettings.h" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Settings for the CameraCalibration plugin modules." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_StartupLensFile_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** \n\x09 * Startup lens file for the project \n\x09 * Can be overriden. Priority of operation is\n\x09 * 1. Apply startup lens file found in 'CameraCalibration.StartupLensFile' cvar at launch\n\x09 * 2. If none found, apply user startup file (only for editor runs)\n\x09 * 3. If none found, apply projet startup file (this one)\n\x09 */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Startup lens file for the project\nCan be overriden. Priority of operation is\n1. Apply startup lens file found in 'CameraCalibration.StartupLensFile' cvar at launch\n2. If none found, apply user startup file (only for editor runs)\n3. If none found, apply projet startup file (this one)" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_StartupLensFile = { "StartupLensFile", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationSettings, StartupLensFile), Z_Construct_UClass_ULensFile_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_StartupLensFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_StartupLensFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DisplacementMapResolution_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Resolution used when creating new distortion and undistortion displacement maps */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Resolution used when creating new distortion and undistortion displacement maps" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DisplacementMapResolution = { "DisplacementMapResolution", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationSettings, DisplacementMapResolution), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DisplacementMapResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DisplacementMapResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_CalibrationInputTolerance_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Tolerance to use when adding or accessing data in a calibrated LensFile */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Tolerance to use when adding or accessing data in a calibrated LensFile" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_CalibrationInputTolerance = { "CalibrationInputTolerance", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationSettings, CalibrationInputTolerance), METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_CalibrationInputTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_CalibrationInputTolerance_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_ValueProp = { "DefaultUndistortionDisplacementMaterials", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_Key_KeyProp = { "DefaultUndistortionDisplacementMaterials_Key", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_MetaData[] = {
		{ "Comment", "/** Map of Lens Distortion Model Handler classes to the default displacement map material used by that class */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Map of Lens Distortion Model Handler classes to the default displacement map material used by that class" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials = { "DefaultUndistortionDisplacementMaterials", nullptr, (EPropertyFlags)0x0044000000004000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationSettings, DefaultUndistortionDisplacementMaterials), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_ValueProp = { "DefaultDistortionDisplacementMaterials", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_Key_KeyProp = { "DefaultDistortionDisplacementMaterials_Key", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_MetaData[] = {
		{ "Comment", "/** Map of Lens Distortion Model Handler classes to the default displacement map material used by that class */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Map of Lens Distortion Model Handler classes to the default displacement map material used by that class" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials = { "DefaultDistortionDisplacementMaterials", nullptr, (EPropertyFlags)0x0044000000004000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationSettings, DefaultDistortionDisplacementMaterials), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_ValueProp = { "DefaultDistortionMaterials", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_Key_KeyProp = { "DefaultDistortionMaterials_Key", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_MetaData[] = {
		{ "Comment", "/** Map of Lens Distortion Model Handler classes to the default lens distortion post-process material used by that class */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Map of Lens Distortion Model Handler classes to the default lens distortion post-process material used by that class" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials = { "DefaultDistortionMaterials", nullptr, (EPropertyFlags)0x0044000000004000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationSettings, DefaultDistortionMaterials), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCameraCalibrationSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_StartupLensFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DisplacementMapResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_CalibrationInputTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultUndistortionDisplacementMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionDisplacementMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationSettings_Statics::NewProp_DefaultDistortionMaterials,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraCalibrationSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraCalibrationSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraCalibrationSettings_Statics::ClassParams = {
		&UCameraCalibrationSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCameraCalibrationSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraCalibrationSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraCalibrationSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraCalibrationSettings, 2219823326);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<UCameraCalibrationSettings>()
	{
		return UCameraCalibrationSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraCalibrationSettings(Z_Construct_UClass_UCameraCalibrationSettings, &UCameraCalibrationSettings::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("UCameraCalibrationSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraCalibrationSettings);
	void UCameraCalibrationEditorSettings::StaticRegisterNativesUCameraCalibrationEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UCameraCalibrationEditorSettings_NoRegister()
	{
		return UCameraCalibrationEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowEditorToolbarButton_MetaData[];
#endif
		static void NewProp_bShowEditorToolbarButton_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowEditorToolbarButton;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CategoryColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CategoryColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableTimeSlider_MetaData[];
#endif
		static void NewProp_bEnableTimeSlider_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableTimeSlider;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserLensFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_UserLensFile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the camera calibration when in editor and standalone.\n * @note Cooked games don't use this setting.\n */" },
		{ "IncludePath", "CameraCalibrationSettings.h" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Settings for the camera calibration when in editor and standalone.\n@note Cooked games don't use this setting." },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/**\n\x09 * True if a lens file button shortcut should be added to level editor toolbar.\n\x09 */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Enable Lens File Toolbar Button" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "True if a lens file button shortcut should be added to level editor toolbar." },
	};
#endif
	void Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton_SetBit(void* Obj)
	{
		((UCameraCalibrationEditorSettings*)Obj)->bShowEditorToolbarButton = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton = { "bShowEditorToolbarButton", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCameraCalibrationEditorSettings), &Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_CategoryColor_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/**\n\x09 * Data Table category color settings\n\x09 */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Data Table category color settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_CategoryColor = { "CategoryColor", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationEditorSettings, CategoryColor), Z_Construct_UScriptStruct_FLensDataCategoryEditorColor, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_CategoryColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_CategoryColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Enable or Disable Time input driven by Live Link." },
	};
#endif
	void Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider_SetBit(void* Obj)
	{
		((UCameraCalibrationEditorSettings*)Obj)->bEnableTimeSlider = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider = { "bEnableTimeSlider", nullptr, (EPropertyFlags)0x0010000800004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCameraCalibrationEditorSettings), &Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_UserLensFile_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** \n\x09 * Startup lens file per user in editor \n\x09 * Can be overridden. Priority of operation is\n\x09 * 1. Apply startup lens file found in 'CameraCalibration.StartupLensFile' cvar at launch\n\x09 * 2. If none found, apply user startup file (this one)\n\x09 * 3. If none found, apply project startup file\n\x09 */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationSettings.h" },
		{ "ToolTip", "Startup lens file per user in editor\nCan be overridden. Priority of operation is\n1. Apply startup lens file found in 'CameraCalibration.StartupLensFile' cvar at launch\n2. If none found, apply user startup file (this one)\n3. If none found, apply project startup file" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_UserLensFile = { "UserLensFile", nullptr, (EPropertyFlags)0x0044000800004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraCalibrationEditorSettings, UserLensFile), Z_Construct_UClass_ULensFile_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_UserLensFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_UserLensFile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bShowEditorToolbarButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_CategoryColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_bEnableTimeSlider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::NewProp_UserLensFile,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraCalibrationEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::ClassParams = {
		&UCameraCalibrationEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::PropPointers), 0),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraCalibrationEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraCalibrationEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraCalibrationEditorSettings, 1333221263);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<UCameraCalibrationEditorSettings>()
	{
		return UCameraCalibrationEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraCalibrationEditorSettings(Z_Construct_UClass_UCameraCalibrationEditorSettings, &UCameraCalibrationEditorSettings::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("UCameraCalibrationEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraCalibrationEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
