// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_LensData_generated_h
#error "LensData.generated.h already included, missing '#pragma once' in LensData.h"
#endif
#define CAMERACALIBRATIONCORE_LensData_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_282_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDataTablePointInfoBase Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FNodalOffsetPointInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_258_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDataTablePointInfoBase Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FImageCenterPointInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_234_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSTMapPointInfo_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDataTablePointInfoBase Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FSTMapPointInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_210_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDataTablePointInfoBase Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FFocalLengthPointInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_185_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDistortionPointInfo_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDataTablePointInfoBase Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDistortionPointInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_157_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDataTablePointInfoBase>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_138_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDistortionData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDistortionData>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_121_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNodalPointOffset_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FNodalPointOffset>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_107_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImageCenterInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FImageCenterInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_89_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSTMapInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FSTMapInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_74_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFocalLengthInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FFocalLengthInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDistortionInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDistortionInfo>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLensInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FLensInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
