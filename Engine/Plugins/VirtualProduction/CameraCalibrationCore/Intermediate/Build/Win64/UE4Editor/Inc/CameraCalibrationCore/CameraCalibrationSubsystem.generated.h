// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCameraCalibrationStep;
class UCameraNodalOffsetAlgo;
class ULensModel;
class UCineCameraComponent;
class ULensDistortionModelHandlerBase;
struct FDistortionHandlerPicker;
struct FLensFilePicker;
class ULensFile;
#ifdef CAMERACALIBRATIONCORE_CameraCalibrationSubsystem_generated_h
#error "CameraCalibrationSubsystem.generated.h already included, missing '#pragma once' in CameraCalibrationSubsystem.h"
#endif
#define CAMERACALIBRATIONCORE_CameraCalibrationSubsystem_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCameraCalibrationStep); \
	DECLARE_FUNCTION(execGetCameraCalibrationSteps); \
	DECLARE_FUNCTION(execGetCameraNodalOffsetAlgos); \
	DECLARE_FUNCTION(execGetCameraNodalOffsetAlgo); \
	DECLARE_FUNCTION(execGetRegisteredLensModel); \
	DECLARE_FUNCTION(execUnregisterDistortionModelHandler); \
	DECLARE_FUNCTION(execFindOrCreateDistortionModelHandler); \
	DECLARE_FUNCTION(execFindDistortionModelHandler); \
	DECLARE_FUNCTION(execGetDistortionModelHandlers); \
	DECLARE_FUNCTION(execGetLensFile); \
	DECLARE_FUNCTION(execSetDefaultLensFile); \
	DECLARE_FUNCTION(execGetDefaultLensFile);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCameraCalibrationStep); \
	DECLARE_FUNCTION(execGetCameraCalibrationSteps); \
	DECLARE_FUNCTION(execGetCameraNodalOffsetAlgos); \
	DECLARE_FUNCTION(execGetCameraNodalOffsetAlgo); \
	DECLARE_FUNCTION(execGetRegisteredLensModel); \
	DECLARE_FUNCTION(execUnregisterDistortionModelHandler); \
	DECLARE_FUNCTION(execFindOrCreateDistortionModelHandler); \
	DECLARE_FUNCTION(execFindDistortionModelHandler); \
	DECLARE_FUNCTION(execGetDistortionModelHandlers); \
	DECLARE_FUNCTION(execGetLensFile); \
	DECLARE_FUNCTION(execSetDefaultLensFile); \
	DECLARE_FUNCTION(execGetDefaultLensFile);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraCalibrationSubsystem(); \
	friend struct Z_Construct_UClass_UCameraCalibrationSubsystem_Statics; \
public: \
	DECLARE_CLASS(UCameraCalibrationSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCameraCalibrationSubsystem)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUCameraCalibrationSubsystem(); \
	friend struct Z_Construct_UClass_UCameraCalibrationSubsystem_Statics; \
public: \
	DECLARE_CLASS(UCameraCalibrationSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCameraCalibrationSubsystem)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraCalibrationSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraCalibrationSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraCalibrationSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraCalibrationSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraCalibrationSubsystem(UCameraCalibrationSubsystem&&); \
	NO_API UCameraCalibrationSubsystem(const UCameraCalibrationSubsystem&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraCalibrationSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraCalibrationSubsystem(UCameraCalibrationSubsystem&&); \
	NO_API UCameraCalibrationSubsystem(const UCameraCalibrationSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraCalibrationSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraCalibrationSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCameraCalibrationSubsystem)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DefaultLensFile() { return STRUCT_OFFSET(UCameraCalibrationSubsystem, DefaultLensFile); } \
	FORCEINLINE static uint32 __PPO__LensModelMap() { return STRUCT_OFFSET(UCameraCalibrationSubsystem, LensModelMap); } \
	FORCEINLINE static uint32 __PPO__CameraNodalOffsetAlgosMap() { return STRUCT_OFFSET(UCameraCalibrationSubsystem, CameraNodalOffsetAlgosMap); } \
	FORCEINLINE static uint32 __PPO__CameraCalibrationStepsMap() { return STRUCT_OFFSET(UCameraCalibrationSubsystem, CameraCalibrationStepsMap); }


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_32_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class UCameraCalibrationSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
