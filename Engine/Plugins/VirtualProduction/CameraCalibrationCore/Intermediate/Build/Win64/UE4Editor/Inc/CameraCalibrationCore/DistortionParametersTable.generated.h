// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_DistortionParametersTable_generated_h
#error "DistortionParametersTable.generated.h already included, missing '#pragma once' in DistortionParametersTable.h"
#endif
#define CAMERACALIBRATIONCORE_DistortionParametersTable_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_DistortionParametersTable_h_82_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDistortionTable_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBaseLensTable Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDistortionTable>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_DistortionParametersTable_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBaseFocusPoint Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDistortionFocusPoint>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_DistortionParametersTable_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDistortionZoomPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_DistortionParametersTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
