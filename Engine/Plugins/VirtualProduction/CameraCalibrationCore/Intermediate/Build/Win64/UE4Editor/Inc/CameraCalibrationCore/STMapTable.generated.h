// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_STMapTable_generated_h
#error "STMapTable.generated.h already included, missing '#pragma once' in STMapTable.h"
#endif
#define CAMERACALIBRATIONCORE_STMapTable_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_STMapTable_h_121_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSTMapTable_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBaseLensTable Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FSTMapTable>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_STMapTable_h_70_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSTMapFocusPoint_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FBaseFocusPoint Super;


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FSTMapFocusPoint>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_STMapTable_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSTMapZoomPoint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FSTMapZoomPoint>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_STMapTable_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDerivedDistortionData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FDerivedDistortionData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_STMapTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
