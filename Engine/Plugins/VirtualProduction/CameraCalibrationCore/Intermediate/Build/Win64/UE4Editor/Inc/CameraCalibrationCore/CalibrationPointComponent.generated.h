// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef CAMERACALIBRATIONCORE_CalibrationPointComponent_generated_h
#error "CalibrationPointComponent.generated.h already included, missing '#pragma once' in CalibrationPointComponent.h"
#endif
#define CAMERACALIBRATIONCORE_CalibrationPointComponent_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRebuildVertices); \
	DECLARE_FUNCTION(execGetNamespacedPointNames); \
	DECLARE_FUNCTION(execNamespacedSubpointName); \
	DECLARE_FUNCTION(execGetWorldLocation);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRebuildVertices); \
	DECLARE_FUNCTION(execGetNamespacedPointNames); \
	DECLARE_FUNCTION(execNamespacedSubpointName); \
	DECLARE_FUNCTION(execGetWorldLocation);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCalibrationPointComponent(); \
	friend struct Z_Construct_UClass_UCalibrationPointComponent_Statics; \
public: \
	DECLARE_CLASS(UCalibrationPointComponent, UProceduralMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCalibrationPointComponent)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUCalibrationPointComponent(); \
	friend struct Z_Construct_UClass_UCalibrationPointComponent_Statics; \
public: \
	DECLARE_CLASS(UCalibrationPointComponent, UProceduralMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCalibrationPointComponent)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCalibrationPointComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCalibrationPointComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCalibrationPointComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCalibrationPointComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCalibrationPointComponent(UCalibrationPointComponent&&); \
	NO_API UCalibrationPointComponent(const UCalibrationPointComponent&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCalibrationPointComponent(UCalibrationPointComponent&&); \
	NO_API UCalibrationPointComponent(const UCalibrationPointComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCalibrationPointComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCalibrationPointComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCalibrationPointComponent)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_27_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class UCalibrationPointComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CalibrationPointComponent_h


#define FOREACH_ENUM_ECALIBRATIONPOINTVISUALIZATION(op) \
	op(CalibrationPointVisualizationCube) \
	op(CalibrationPointVisualizationPyramid) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
