// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ELensDataCategory : uint8;
struct FSTMapInfo;
struct FNodalPointOffset;
struct FImageCenterInfo;
struct FFocalLengthInfo;
struct FDistortionInfo;
struct FNodalOffsetPointInfo;
struct FImageCenterPointInfo;
struct FSTMapPointInfo;
struct FFocalLengthPointInfo;
struct FDistortionPointInfo;
struct FVector2D;
class ULensDistortionModelHandlerBase;
#ifdef CAMERACALIBRATIONCORE_LensFile_generated_h
#error "LensFile.generated.h already included, missing '#pragma once' in LensFile.h"
#endif
#define CAMERACALIBRATIONCORE_LensFile_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_300_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLensFilePicker_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FLensFilePicker>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHasSamples); \
	DECLARE_FUNCTION(execClearData); \
	DECLARE_FUNCTION(execClearAll); \
	DECLARE_FUNCTION(execRemoveZoomPoint); \
	DECLARE_FUNCTION(execRemoveFocusPoint); \
	DECLARE_FUNCTION(execAddSTMapPoint); \
	DECLARE_FUNCTION(execAddNodalOffsetPoint); \
	DECLARE_FUNCTION(execAddImageCenterPoint); \
	DECLARE_FUNCTION(execAddFocalLengthPoint); \
	DECLARE_FUNCTION(execAddDistortionPoint); \
	DECLARE_FUNCTION(execGetSTMapPoint); \
	DECLARE_FUNCTION(execGetNodalOffsetPoint); \
	DECLARE_FUNCTION(execGetImageCenterPoint); \
	DECLARE_FUNCTION(execGetFocalLengthPoint); \
	DECLARE_FUNCTION(execGetDistortionPoint); \
	DECLARE_FUNCTION(execGetNodalOffsetPoints); \
	DECLARE_FUNCTION(execGetImageCenterPoints); \
	DECLARE_FUNCTION(execGetSTMapPoints); \
	DECLARE_FUNCTION(execGetFocalLengthPoints); \
	DECLARE_FUNCTION(execGetDistortionPoints); \
	DECLARE_FUNCTION(execEvaluateNormalizedIris); \
	DECLARE_FUNCTION(execHasIrisEncoderMapping); \
	DECLARE_FUNCTION(execEvaluateNormalizedFocus); \
	DECLARE_FUNCTION(execHasFocusEncoderMapping); \
	DECLARE_FUNCTION(execEvaluateNodalPointOffset); \
	DECLARE_FUNCTION(execEvaluateDistortionData); \
	DECLARE_FUNCTION(execEvaluateImageCenterParameters); \
	DECLARE_FUNCTION(execEvaluateFocalLength); \
	DECLARE_FUNCTION(execEvaluateDistortionParameters);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHasSamples); \
	DECLARE_FUNCTION(execClearData); \
	DECLARE_FUNCTION(execClearAll); \
	DECLARE_FUNCTION(execRemoveZoomPoint); \
	DECLARE_FUNCTION(execRemoveFocusPoint); \
	DECLARE_FUNCTION(execAddSTMapPoint); \
	DECLARE_FUNCTION(execAddNodalOffsetPoint); \
	DECLARE_FUNCTION(execAddImageCenterPoint); \
	DECLARE_FUNCTION(execAddFocalLengthPoint); \
	DECLARE_FUNCTION(execAddDistortionPoint); \
	DECLARE_FUNCTION(execGetSTMapPoint); \
	DECLARE_FUNCTION(execGetNodalOffsetPoint); \
	DECLARE_FUNCTION(execGetImageCenterPoint); \
	DECLARE_FUNCTION(execGetFocalLengthPoint); \
	DECLARE_FUNCTION(execGetDistortionPoint); \
	DECLARE_FUNCTION(execGetNodalOffsetPoints); \
	DECLARE_FUNCTION(execGetImageCenterPoints); \
	DECLARE_FUNCTION(execGetSTMapPoints); \
	DECLARE_FUNCTION(execGetFocalLengthPoints); \
	DECLARE_FUNCTION(execGetDistortionPoints); \
	DECLARE_FUNCTION(execEvaluateNormalizedIris); \
	DECLARE_FUNCTION(execHasIrisEncoderMapping); \
	DECLARE_FUNCTION(execEvaluateNormalizedFocus); \
	DECLARE_FUNCTION(execHasFocusEncoderMapping); \
	DECLARE_FUNCTION(execEvaluateNodalPointOffset); \
	DECLARE_FUNCTION(execEvaluateDistortionData); \
	DECLARE_FUNCTION(execEvaluateImageCenterParameters); \
	DECLARE_FUNCTION(execEvaluateFocalLength); \
	DECLARE_FUNCTION(execEvaluateDistortionParameters);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULensFile(); \
	friend struct Z_Construct_UClass_ULensFile_Statics; \
public: \
	DECLARE_CLASS(ULensFile, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(ULensFile)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_INCLASS \
private: \
	static void StaticRegisterNativesULensFile(); \
	friend struct Z_Construct_UClass_ULensFile_Statics; \
public: \
	DECLARE_CLASS(ULensFile, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(ULensFile)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensFile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensFile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensFile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensFile(ULensFile&&); \
	NO_API ULensFile(const ULensFile&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensFile(ULensFile&&); \
	NO_API ULensFile(const ULensFile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensFile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULensFile)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__UndistortionDisplacementMapHolders() { return STRUCT_OFFSET(ULensFile, UndistortionDisplacementMapHolders); } \
	FORCEINLINE static uint32 __PPO__DistortionDisplacementMapHolders() { return STRUCT_OFFSET(ULensFile, DistortionDisplacementMapHolders); }


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_55_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h_58_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class ULensFile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensFile_h


#define FOREACH_ENUM_ELENSDATACATEGORY(op) \
	op(ELensDataCategory::Focus) \
	op(ELensDataCategory::Iris) \
	op(ELensDataCategory::Zoom) \
	op(ELensDataCategory::Distortion) \
	op(ELensDataCategory::ImageCenter) \
	op(ELensDataCategory::STMap) \
	op(ELensDataCategory::NodalOffset) 

enum class ELensDataCategory : uint8;
template<> CAMERACALIBRATIONCORE_API UEnum* StaticEnum<ELensDataCategory>();

#define FOREACH_ENUM_ELENSDATAMODE(op) \
	op(ELensDataMode::Parameters) \
	op(ELensDataMode::STMap) 

enum class ELensDataMode : uint8;
template<> CAMERACALIBRATIONCORE_API UEnum* StaticEnum<ELensDataMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
