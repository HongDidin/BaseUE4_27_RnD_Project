// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Models/LensModel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensModel() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensModel_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensModel();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
// End Cross Module References
	void ULensModel::StaticRegisterNativesULensModel()
	{
	}
	UClass* Z_Construct_UClass_ULensModel_NoRegister()
	{
		return ULensModel::StaticClass();
	}
	struct Z_Construct_UClass_ULensModel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensModel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensModel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Abstract base class for lens models\n */" },
		{ "IncludePath", "Models/LensModel.h" },
		{ "ModuleRelativePath", "Public/Models/LensModel.h" },
		{ "ToolTip", "Abstract base class for lens models" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensModel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensModel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensModel_Statics::ClassParams = {
		&ULensModel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_ULensModel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensModel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensModel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensModel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensModel, 3604062719);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<ULensModel>()
	{
		return ULensModel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensModel(Z_Construct_UClass_ULensModel, &ULensModel::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("ULensModel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensModel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
