// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_CameraLensDistortionAlgo_generated_h
#error "CameraLensDistortionAlgo.generated.h already included, missing '#pragma once' in CameraLensDistortionAlgo.h"
#endif
#define CAMERACALIBRATIONCORE_CameraLensDistortionAlgo_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraLensDistortionAlgo(); \
	friend struct Z_Construct_UClass_UCameraLensDistortionAlgo_Statics; \
public: \
	DECLARE_CLASS(UCameraLensDistortionAlgo, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCameraLensDistortionAlgo)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUCameraLensDistortionAlgo(); \
	friend struct Z_Construct_UClass_UCameraLensDistortionAlgo_Statics; \
public: \
	DECLARE_CLASS(UCameraLensDistortionAlgo, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCameraLensDistortionAlgo)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraLensDistortionAlgo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraLensDistortionAlgo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraLensDistortionAlgo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraLensDistortionAlgo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraLensDistortionAlgo(UCameraLensDistortionAlgo&&); \
	NO_API UCameraLensDistortionAlgo(const UCameraLensDistortionAlgo&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraLensDistortionAlgo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraLensDistortionAlgo(UCameraLensDistortionAlgo&&); \
	NO_API UCameraLensDistortionAlgo(const UCameraLensDistortionAlgo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraLensDistortionAlgo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraLensDistortionAlgo); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraLensDistortionAlgo)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_27_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class UCameraLensDistortionAlgo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraLensDistortionAlgo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
