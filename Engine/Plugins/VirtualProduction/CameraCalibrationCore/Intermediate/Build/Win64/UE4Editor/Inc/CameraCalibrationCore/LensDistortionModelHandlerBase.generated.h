// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTextureRenderTarget2D;
struct FLensDistortionState;
class ULensModel;
#ifdef CAMERACALIBRATIONCORE_LensDistortionModelHandlerBase_generated_h
#error "LensDistortionModelHandlerBase.generated.h already included, missing '#pragma once' in LensDistortionModelHandlerBase.h"
#endif
#define CAMERACALIBRATIONCORE_LensDistortionModelHandlerBase_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLensDistortionState_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FLensDistortionState>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDistortionDisplacementMap); \
	DECLARE_FUNCTION(execGetUndistortionDisplacementMap); \
	DECLARE_FUNCTION(execSetDistortionState); \
	DECLARE_FUNCTION(execIsModelSupported);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDistortionDisplacementMap); \
	DECLARE_FUNCTION(execGetUndistortionDisplacementMap); \
	DECLARE_FUNCTION(execSetDistortionState); \
	DECLARE_FUNCTION(execIsModelSupported);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULensDistortionModelHandlerBase(); \
	friend struct Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics; \
public: \
	DECLARE_CLASS(ULensDistortionModelHandlerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(ULensDistortionModelHandlerBase)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_INCLASS \
private: \
	static void StaticRegisterNativesULensDistortionModelHandlerBase(); \
	friend struct Z_Construct_UClass_ULensDistortionModelHandlerBase_Statics; \
public: \
	DECLARE_CLASS(ULensDistortionModelHandlerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(ULensDistortionModelHandlerBase)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensDistortionModelHandlerBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensDistortionModelHandlerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensDistortionModelHandlerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensDistortionModelHandlerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensDistortionModelHandlerBase(ULensDistortionModelHandlerBase&&); \
	NO_API ULensDistortionModelHandlerBase(const ULensDistortionModelHandlerBase&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensDistortionModelHandlerBase(ULensDistortionModelHandlerBase&&); \
	NO_API ULensDistortionModelHandlerBase(const ULensDistortionModelHandlerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensDistortionModelHandlerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensDistortionModelHandlerBase); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(ULensDistortionModelHandlerBase)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LensModelClass() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, LensModelClass); } \
	FORCEINLINE static uint32 __PPO__DistortionPostProcessMID() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionPostProcessMID); } \
	FORCEINLINE static uint32 __PPO__CurrentState() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, CurrentState); } \
	FORCEINLINE static uint32 __PPO__DisplayName() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, DisplayName); } \
	FORCEINLINE static uint32 __PPO__OverscanFactor() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, OverscanFactor); } \
	FORCEINLINE static uint32 __PPO__UndistortionDisplacementMapMID() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, UndistortionDisplacementMapMID); } \
	FORCEINLINE static uint32 __PPO__DistortionDisplacementMapMID() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionDisplacementMapMID); } \
	FORCEINLINE static uint32 __PPO__UndistortionDisplacementMapRT() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, UndistortionDisplacementMapRT); } \
	FORCEINLINE static uint32 __PPO__DistortionDisplacementMapRT() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionDisplacementMapRT); } \
	FORCEINLINE static uint32 __PPO__DistortionProducerID() { return STRUCT_OFFSET(ULensDistortionModelHandlerBase, DistortionProducerID); }


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_37_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h_40_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class ULensDistortionModelHandlerBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_LensDistortionModelHandlerBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
