// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_CameraCalibrationCheckerboard_generated_h
#error "CameraCalibrationCheckerboard.generated.h already included, missing '#pragma once' in CameraCalibrationCheckerboard.h"
#endif
#define CAMERACALIBRATIONCORE_CameraCalibrationCheckerboard_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRebuild);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRebuild);


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACameraCalibrationCheckerboard(); \
	friend struct Z_Construct_UClass_ACameraCalibrationCheckerboard_Statics; \
public: \
	DECLARE_CLASS(ACameraCalibrationCheckerboard, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(ACameraCalibrationCheckerboard)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_INCLASS \
private: \
	static void StaticRegisterNativesACameraCalibrationCheckerboard(); \
	friend struct Z_Construct_UClass_ACameraCalibrationCheckerboard_Statics; \
public: \
	DECLARE_CLASS(ACameraCalibrationCheckerboard, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(ACameraCalibrationCheckerboard)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACameraCalibrationCheckerboard(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACameraCalibrationCheckerboard) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraCalibrationCheckerboard); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraCalibrationCheckerboard); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraCalibrationCheckerboard(ACameraCalibrationCheckerboard&&); \
	NO_API ACameraCalibrationCheckerboard(const ACameraCalibrationCheckerboard&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraCalibrationCheckerboard(ACameraCalibrationCheckerboard&&); \
	NO_API ACameraCalibrationCheckerboard(const ACameraCalibrationCheckerboard&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraCalibrationCheckerboard); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraCalibrationCheckerboard); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACameraCalibrationCheckerboard)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_18_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class ACameraCalibrationCheckerboard>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraCalibrationCheckerboard_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
