// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_CameraNodalOffsetAlgo_generated_h
#error "CameraNodalOffsetAlgo.generated.h already included, missing '#pragma once' in CameraNodalOffsetAlgo.h"
#endif
#define CAMERACALIBRATIONCORE_CameraNodalOffsetAlgo_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraNodalOffsetAlgo(); \
	friend struct Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics; \
public: \
	DECLARE_CLASS(UCameraNodalOffsetAlgo, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCameraNodalOffsetAlgo)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUCameraNodalOffsetAlgo(); \
	friend struct Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics; \
public: \
	DECLARE_CLASS(UCameraNodalOffsetAlgo, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(UCameraNodalOffsetAlgo)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraNodalOffsetAlgo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraNodalOffsetAlgo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraNodalOffsetAlgo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraNodalOffsetAlgo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraNodalOffsetAlgo(UCameraNodalOffsetAlgo&&); \
	NO_API UCameraNodalOffsetAlgo(const UCameraNodalOffsetAlgo&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraNodalOffsetAlgo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraNodalOffsetAlgo(UCameraNodalOffsetAlgo&&); \
	NO_API UCameraNodalOffsetAlgo(const UCameraNodalOffsetAlgo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraNodalOffsetAlgo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraNodalOffsetAlgo); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraNodalOffsetAlgo)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_23_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class UCameraNodalOffsetAlgo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_CameraNodalOffsetAlgo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
