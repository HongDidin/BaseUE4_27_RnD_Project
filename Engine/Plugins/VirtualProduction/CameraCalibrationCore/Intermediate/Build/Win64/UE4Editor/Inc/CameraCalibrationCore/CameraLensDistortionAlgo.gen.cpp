// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/CameraLensDistortionAlgo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraLensDistortionAlgo() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraLensDistortionAlgo_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraLensDistortionAlgo();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
// End Cross Module References
	void UCameraLensDistortionAlgo::StaticRegisterNativesUCameraLensDistortionAlgo()
	{
	}
	UClass* Z_Construct_UClass_UCameraLensDistortionAlgo_NoRegister()
	{
		return UCameraLensDistortionAlgo::StaticClass();
	}
	struct Z_Construct_UClass_UCameraLensDistortionAlgo_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Defines the interface that any lens distortion algorithm should implement\n * in order to be used and listed by the Lens Distortion Tool. \n */" },
		{ "IncludePath", "CameraLensDistortionAlgo.h" },
		{ "ModuleRelativePath", "Public/CameraLensDistortionAlgo.h" },
		{ "ToolTip", "Defines the interface that any lens distortion algorithm should implement\nin order to be used and listed by the Lens Distortion Tool." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraLensDistortionAlgo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::ClassParams = {
		&UCameraLensDistortionAlgo::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraLensDistortionAlgo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraLensDistortionAlgo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraLensDistortionAlgo, 3501008843);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<UCameraLensDistortionAlgo>()
	{
		return UCameraLensDistortionAlgo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraLensDistortionAlgo(Z_Construct_UClass_UCameraLensDistortionAlgo, &UCameraLensDistortionAlgo::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("UCameraLensDistortionAlgo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraLensDistortionAlgo);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
