// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_BaseLensTable_generated_h
#error "BaseLensTable.generated.h already included, missing '#pragma once' in BaseLensTable.h"
#endif
#define CAMERACALIBRATIONCORE_BaseLensTable_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_BaseLensTable_h_65_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBaseLensTable_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__LensFile() { return STRUCT_OFFSET(FBaseLensTable, LensFile); }


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FBaseLensTable>();

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_BaseLensTable_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBaseFocusPoint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<struct FBaseFocusPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_Tables_BaseLensTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
