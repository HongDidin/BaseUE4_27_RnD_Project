// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/LensFile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensFile() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UEnum* Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UEnum* Z_Construct_UEnum_CameraCalibrationCore_ELensDataMode();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensFilePicker();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensFile_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensFile();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalPointOffset();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FEncodersTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapTable();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	static UEnum* ELensDataCategory_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("ELensDataCategory"));
		}
		return Singleton;
	}
	template<> CAMERACALIBRATIONCORE_API UEnum* StaticEnum<ELensDataCategory>()
	{
		return ELensDataCategory_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELensDataCategory(ELensDataCategory_StaticEnum, TEXT("/Script/CameraCalibrationCore"), TEXT("ELensDataCategory"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory_Hash() { return 741071246U; }
	UEnum* Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELensDataCategory"), 0, Get_Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELensDataCategory::Focus", (int64)ELensDataCategory::Focus },
				{ "ELensDataCategory::Iris", (int64)ELensDataCategory::Iris },
				{ "ELensDataCategory::Zoom", (int64)ELensDataCategory::Zoom },
				{ "ELensDataCategory::Distortion", (int64)ELensDataCategory::Distortion },
				{ "ELensDataCategory::ImageCenter", (int64)ELensDataCategory::ImageCenter },
				{ "ELensDataCategory::STMap", (int64)ELensDataCategory::STMap },
				{ "ELensDataCategory::NodalOffset", (int64)ELensDataCategory::NodalOffset },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Data categories manipulated in the camera calibration tools */" },
				{ "Distortion.Name", "ELensDataCategory::Distortion" },
				{ "Focus.Name", "ELensDataCategory::Focus" },
				{ "ImageCenter.Name", "ELensDataCategory::ImageCenter" },
				{ "Iris.Name", "ELensDataCategory::Iris" },
				{ "ModuleRelativePath", "Public/LensFile.h" },
				{ "NodalOffset.Name", "ELensDataCategory::NodalOffset" },
				{ "STMap.Name", "ELensDataCategory::STMap" },
				{ "ToolTip", "Data categories manipulated in the camera calibration tools" },
				{ "Zoom.Name", "ELensDataCategory::Zoom" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
				nullptr,
				"ELensDataCategory",
				"ELensDataCategory",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ELensDataMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CameraCalibrationCore_ELensDataMode, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("ELensDataMode"));
		}
		return Singleton;
	}
	template<> CAMERACALIBRATIONCORE_API UEnum* StaticEnum<ELensDataMode>()
	{
		return ELensDataMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELensDataMode(ELensDataMode_StaticEnum, TEXT("/Script/CameraCalibrationCore"), TEXT("ELensDataMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CameraCalibrationCore_ELensDataMode_Hash() { return 1536448275U; }
	UEnum* Z_Construct_UEnum_CameraCalibrationCore_ELensDataMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELensDataMode"), 0, Get_Z_Construct_UEnum_CameraCalibrationCore_ELensDataMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELensDataMode::Parameters", (int64)ELensDataMode::Parameters },
				{ "ELensDataMode::STMap", (int64)ELensDataMode::STMap },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Mode of operation of Lens File */" },
				{ "ModuleRelativePath", "Public/LensFile.h" },
				{ "Parameters.Name", "ELensDataMode::Parameters" },
				{ "STMap.Name", "ELensDataMode::STMap" },
				{ "ToolTip", "Mode of operation of Lens File" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
				nullptr,
				"ELensDataMode",
				"ELensDataMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FLensFilePicker::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FLensFilePicker_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLensFilePicker, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("LensFilePicker"), sizeof(FLensFilePicker), Get_Z_Construct_UScriptStruct_FLensFilePicker_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FLensFilePicker>()
{
	return FLensFilePicker::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLensFilePicker(FLensFilePicker::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("LensFilePicker"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensFilePicker
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensFilePicker()
	{
		UScriptStruct::DeferCppStructOps<FLensFilePicker>(FName(TEXT("LensFilePicker")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensFilePicker;
	struct Z_Construct_UScriptStruct_FLensFilePicker_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseDefaultLensFile_MetaData[];
#endif
		static void NewProp_bUseDefaultLensFile_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseDefaultLensFile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LensFile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensFilePicker_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Wrapper to facilitate default LensFile vs picker\n */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Wrapper to facilitate default LensFile vs picker" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLensFilePicker>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile_MetaData[] = {
		{ "Category", "Lens File" },
		{ "Comment", "/** If true, default LensFile will be used, if one is set */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "If true, default LensFile will be used, if one is set" },
	};
#endif
	void Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile_SetBit(void* Obj)
	{
		((FLensFilePicker*)Obj)->bUseDefaultLensFile = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile = { "bUseDefaultLensFile", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLensFilePicker), &Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_LensFile_MetaData[] = {
		{ "Category", "Lens File" },
		{ "Comment", "/** LensFile asset to use if DefaultLensFile is not desired */" },
		{ "EditCondition", "!bUseDefaultLensFile" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "LensFile asset to use if DefaultLensFile is not desired" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_LensFile = { "LensFile", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensFilePicker, LensFile), Z_Construct_UClass_ULensFile_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_LensFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_LensFile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLensFilePicker_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_bUseDefaultLensFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensFilePicker_Statics::NewProp_LensFile,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLensFilePicker_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"LensFilePicker",
		sizeof(FLensFilePicker),
		alignof(FLensFilePicker),
		Z_Construct_UScriptStruct_FLensFilePicker_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensFilePicker_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLensFilePicker_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensFilePicker_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLensFilePicker()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLensFilePicker_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LensFilePicker"), sizeof(FLensFilePicker), Get_Z_Construct_UScriptStruct_FLensFilePicker_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLensFilePicker_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLensFilePicker_Hash() { return 1491520024U; }
	DEFINE_FUNCTION(ULensFile::execHasSamples)
	{
		P_GET_ENUM(ELensDataCategory,Z_Param_InDataCategory);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasSamples(ELensDataCategory(Z_Param_InDataCategory));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execClearData)
	{
		P_GET_ENUM(ELensDataCategory,Z_Param_InDataCategory);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearData(ELensDataCategory(Z_Param_InDataCategory));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execClearAll)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearAll();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execRemoveZoomPoint)
	{
		P_GET_ENUM(ELensDataCategory,Z_Param_InDataCategory);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveZoomPoint(ELensDataCategory(Z_Param_InDataCategory),Z_Param_InFocus,Z_Param_InZoom);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execRemoveFocusPoint)
	{
		P_GET_ENUM(ELensDataCategory,Z_Param_InDataCategory);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveFocusPoint(ELensDataCategory(Z_Param_InDataCategory),Z_Param_InFocus);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execAddSTMapPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewZoom);
		P_GET_STRUCT_REF(FSTMapInfo,Z_Param_Out_NewPoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddSTMapPoint(Z_Param_NewFocus,Z_Param_NewZoom,Z_Param_Out_NewPoint);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execAddNodalOffsetPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewZoom);
		P_GET_STRUCT_REF(FNodalPointOffset,Z_Param_Out_NewPoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddNodalOffsetPoint(Z_Param_NewFocus,Z_Param_NewZoom,Z_Param_Out_NewPoint);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execAddImageCenterPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewZoom);
		P_GET_STRUCT_REF(FImageCenterInfo,Z_Param_Out_NewPoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddImageCenterPoint(Z_Param_NewFocus,Z_Param_NewZoom,Z_Param_Out_NewPoint);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execAddFocalLengthPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewZoom);
		P_GET_STRUCT_REF(FFocalLengthInfo,Z_Param_Out_NewFocalLength);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddFocalLengthPoint(Z_Param_NewFocus,Z_Param_NewZoom,Z_Param_Out_NewFocalLength);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execAddDistortionPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_NewZoom);
		P_GET_STRUCT_REF(FDistortionInfo,Z_Param_Out_NewPoint);
		P_GET_STRUCT_REF(FFocalLengthInfo,Z_Param_Out_NewFocalLength);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddDistortionPoint(Z_Param_NewFocus,Z_Param_NewZoom,Z_Param_Out_NewPoint,Z_Param_Out_NewFocalLength);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetSTMapPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FSTMapInfo,Z_Param_Out_OutSTMapInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetSTMapPoint(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutSTMapInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetNodalOffsetPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FNodalPointOffset,Z_Param_Out_OutNodalPointOffset);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetNodalOffsetPoint(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutNodalPointOffset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetImageCenterPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FImageCenterInfo,Z_Param_Out_OutImageCenterInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetImageCenterPoint(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutImageCenterInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetFocalLengthPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FFocalLengthInfo,Z_Param_Out_OutFocalLengthInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetFocalLengthPoint(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutFocalLengthInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetDistortionPoint)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FDistortionInfo,Z_Param_Out_OutDistortionInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetDistortionPoint(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutDistortionInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetNodalOffsetPoints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FNodalOffsetPointInfo>*)Z_Param__Result=P_THIS->GetNodalOffsetPoints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetImageCenterPoints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FImageCenterPointInfo>*)Z_Param__Result=P_THIS->GetImageCenterPoints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetSTMapPoints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSTMapPointInfo>*)Z_Param__Result=P_THIS->GetSTMapPoints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetFocalLengthPoints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FFocalLengthPointInfo>*)Z_Param__Result=P_THIS->GetFocalLengthPoints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execGetDistortionPoints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FDistortionPointInfo>*)Z_Param__Result=P_THIS->GetDistortionPoints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateNormalizedIris)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InNormalizedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->EvaluateNormalizedIris(Z_Param_InNormalizedValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execHasIrisEncoderMapping)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasIrisEncoderMapping();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateNormalizedFocus)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InNormalizedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->EvaluateNormalizedFocus(Z_Param_InNormalizedValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execHasFocusEncoderMapping)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->HasFocusEncoderMapping();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateNodalPointOffset)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FNodalPointOffset,Z_Param_Out_OutEvaluatedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->EvaluateNodalPointOffset(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutEvaluatedValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateDistortionData)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT(FVector2D,Z_Param_InFilmback);
		P_GET_OBJECT(ULensDistortionModelHandlerBase,Z_Param_InLensHandler);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->EvaluateDistortionData(Z_Param_InFocus,Z_Param_InZoom,Z_Param_InFilmback,Z_Param_InLensHandler);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateImageCenterParameters)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FImageCenterInfo,Z_Param_Out_OutEvaluatedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->EvaluateImageCenterParameters(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutEvaluatedValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateFocalLength)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FFocalLengthInfo,Z_Param_Out_OutEvaluatedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->EvaluateFocalLength(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutEvaluatedValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULensFile::execEvaluateDistortionParameters)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InFocus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InZoom);
		P_GET_STRUCT_REF(FDistortionInfo,Z_Param_Out_OutEvaluatedValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->EvaluateDistortionParameters(Z_Param_InFocus,Z_Param_InZoom,Z_Param_Out_OutEvaluatedValue);
		P_NATIVE_END;
	}
	void ULensFile::StaticRegisterNativesULensFile()
	{
		UClass* Class = ULensFile::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddDistortionPoint", &ULensFile::execAddDistortionPoint },
			{ "AddFocalLengthPoint", &ULensFile::execAddFocalLengthPoint },
			{ "AddImageCenterPoint", &ULensFile::execAddImageCenterPoint },
			{ "AddNodalOffsetPoint", &ULensFile::execAddNodalOffsetPoint },
			{ "AddSTMapPoint", &ULensFile::execAddSTMapPoint },
			{ "ClearAll", &ULensFile::execClearAll },
			{ "ClearData", &ULensFile::execClearData },
			{ "EvaluateDistortionData", &ULensFile::execEvaluateDistortionData },
			{ "EvaluateDistortionParameters", &ULensFile::execEvaluateDistortionParameters },
			{ "EvaluateFocalLength", &ULensFile::execEvaluateFocalLength },
			{ "EvaluateImageCenterParameters", &ULensFile::execEvaluateImageCenterParameters },
			{ "EvaluateNodalPointOffset", &ULensFile::execEvaluateNodalPointOffset },
			{ "EvaluateNormalizedFocus", &ULensFile::execEvaluateNormalizedFocus },
			{ "EvaluateNormalizedIris", &ULensFile::execEvaluateNormalizedIris },
			{ "GetDistortionPoint", &ULensFile::execGetDistortionPoint },
			{ "GetDistortionPoints", &ULensFile::execGetDistortionPoints },
			{ "GetFocalLengthPoint", &ULensFile::execGetFocalLengthPoint },
			{ "GetFocalLengthPoints", &ULensFile::execGetFocalLengthPoints },
			{ "GetImageCenterPoint", &ULensFile::execGetImageCenterPoint },
			{ "GetImageCenterPoints", &ULensFile::execGetImageCenterPoints },
			{ "GetNodalOffsetPoint", &ULensFile::execGetNodalOffsetPoint },
			{ "GetNodalOffsetPoints", &ULensFile::execGetNodalOffsetPoints },
			{ "GetSTMapPoint", &ULensFile::execGetSTMapPoint },
			{ "GetSTMapPoints", &ULensFile::execGetSTMapPoints },
			{ "HasFocusEncoderMapping", &ULensFile::execHasFocusEncoderMapping },
			{ "HasIrisEncoderMapping", &ULensFile::execHasIrisEncoderMapping },
			{ "HasSamples", &ULensFile::execHasSamples },
			{ "RemoveFocusPoint", &ULensFile::execRemoveFocusPoint },
			{ "RemoveZoomPoint", &ULensFile::execRemoveZoomPoint },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics
	{
		struct LensFile_eventAddDistortionPoint_Parms
		{
			float NewFocus;
			float NewZoom;
			FDistortionInfo NewPoint;
			FFocalLengthInfo NewFocalLength;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewZoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewFocalLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocus = { "NewFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddDistortionPoint_Parms, NewFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewZoom = { "NewZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddDistortionPoint_Parms, NewZoom), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewPoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewPoint = { "NewPoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddDistortionPoint_Parms, NewPoint), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocalLength_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocalLength = { "NewFocalLength", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddDistortionPoint_Parms, NewFocalLength), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocalLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::NewProp_NewFocalLength,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Adds a distortion point in our map. If a point already exist at the location, it is updated */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Adds a distortion point in our map. If a point already exist at the location, it is updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "AddDistortionPoint", nullptr, nullptr, sizeof(LensFile_eventAddDistortionPoint_Parms), Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_AddDistortionPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_AddDistortionPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics
	{
		struct LensFile_eventAddFocalLengthPoint_Parms
		{
			float NewFocus;
			float NewZoom;
			FFocalLengthInfo NewFocalLength;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewZoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewFocalLength;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocus = { "NewFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddFocalLengthPoint_Parms, NewFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewZoom = { "NewZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddFocalLengthPoint_Parms, NewZoom), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocalLength_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocalLength = { "NewFocalLength", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddFocalLengthPoint_Parms, NewFocalLength), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocalLength_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::NewProp_NewFocalLength,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Adds a focal length point in our map. If a point already exist at the location, it is updated */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Adds a focal length point in our map. If a point already exist at the location, it is updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "AddFocalLengthPoint", nullptr, nullptr, sizeof(LensFile_eventAddFocalLengthPoint_Parms), Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_AddFocalLengthPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_AddFocalLengthPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics
	{
		struct LensFile_eventAddImageCenterPoint_Parms
		{
			float NewFocus;
			float NewZoom;
			FImageCenterInfo NewPoint;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewZoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewFocus = { "NewFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddImageCenterPoint_Parms, NewFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewZoom = { "NewZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddImageCenterPoint_Parms, NewZoom), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewPoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewPoint = { "NewPoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddImageCenterPoint_Parms, NewPoint), Z_Construct_UScriptStruct_FImageCenterInfo, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::NewProp_NewPoint,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Adds an ImageCenter point in our map. If a point already exist at the location, it is updated */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Adds an ImageCenter point in our map. If a point already exist at the location, it is updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "AddImageCenterPoint", nullptr, nullptr, sizeof(LensFile_eventAddImageCenterPoint_Parms), Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_AddImageCenterPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_AddImageCenterPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics
	{
		struct LensFile_eventAddNodalOffsetPoint_Parms
		{
			float NewFocus;
			float NewZoom;
			FNodalPointOffset NewPoint;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewZoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewFocus = { "NewFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddNodalOffsetPoint_Parms, NewFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewZoom = { "NewZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddNodalOffsetPoint_Parms, NewZoom), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewPoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewPoint = { "NewPoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddNodalOffsetPoint_Parms, NewPoint), Z_Construct_UScriptStruct_FNodalPointOffset, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::NewProp_NewPoint,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Adds an NodalOffset point in our map. If a point already exist at the location, it is updated */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Adds an NodalOffset point in our map. If a point already exist at the location, it is updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "AddNodalOffsetPoint", nullptr, nullptr, sizeof(LensFile_eventAddNodalOffsetPoint_Parms), Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics
	{
		struct LensFile_eventAddSTMapPoint_Parms
		{
			float NewFocus;
			float NewZoom;
			FSTMapInfo NewPoint;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewZoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewFocus = { "NewFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddSTMapPoint_Parms, NewFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewZoom = { "NewZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddSTMapPoint_Parms, NewZoom), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewPoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewPoint = { "NewPoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventAddSTMapPoint_Parms, NewPoint), Z_Construct_UScriptStruct_FSTMapInfo, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::NewProp_NewPoint,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Adds an STMap point in our map. If a point already exist at the location, it is updated */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Adds an STMap point in our map. If a point already exist at the location, it is updated" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "AddSTMapPoint", nullptr, nullptr, sizeof(LensFile_eventAddSTMapPoint_Parms), Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_AddSTMapPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_AddSTMapPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_ClearAll_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_ClearAll_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Removes all points of all tables */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Removes all points of all tables" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_ClearAll_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "ClearAll", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_ClearAll_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_ClearAll_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_ClearAll()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_ClearAll_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_ClearData_Statics
	{
		struct LensFile_eventClearData_Parms
		{
			ELensDataCategory InDataCategory;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InDataCategory_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InDataCategory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULensFile_ClearData_Statics::NewProp_InDataCategory_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULensFile_ClearData_Statics::NewProp_InDataCategory = { "InDataCategory", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventClearData_Parms, InDataCategory), Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_ClearData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_ClearData_Statics::NewProp_InDataCategory_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_ClearData_Statics::NewProp_InDataCategory,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_ClearData_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Removes table associated to data category */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Removes table associated to data category" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_ClearData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "ClearData", nullptr, nullptr, sizeof(LensFile_eventClearData_Parms), Z_Construct_UFunction_ULensFile_ClearData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_ClearData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_ClearData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_ClearData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_ClearData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_ClearData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics
	{
		struct LensFile_eventEvaluateDistortionData_Parms
		{
			float InFocus;
			float InZoom;
			FVector2D InFilmback;
			ULensDistortionModelHandlerBase* InLensHandler;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFilmback;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InLensHandler;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionData_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionData_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InFilmback = { "InFilmback", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionData_Parms, InFilmback), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InLensHandler = { "InLensHandler", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionData_Parms, InLensHandler), Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventEvaluateDistortionData_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventEvaluateDistortionData_Parms), &Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InFilmback,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_InLensHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Draws the distortion map based on evaluation point*/" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Draws the distortion map based on evaluation point" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateDistortionData", nullptr, nullptr, sizeof(LensFile_eventEvaluateDistortionData_Parms), Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateDistortionData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateDistortionData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics
	{
		struct LensFile_eventEvaluateDistortionParameters_Parms
		{
			float InFocus;
			float InZoom;
			FDistortionInfo OutEvaluatedValue;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutEvaluatedValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionParameters_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionParameters_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_OutEvaluatedValue = { "OutEvaluatedValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateDistortionParameters_Parms, OutEvaluatedValue), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventEvaluateDistortionParameters_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventEvaluateDistortionParameters_Parms), &Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_OutEvaluatedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Returns interpolated distortion parameters */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns interpolated distortion parameters" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateDistortionParameters", nullptr, nullptr, sizeof(LensFile_eventEvaluateDistortionParameters_Parms), Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics
	{
		struct LensFile_eventEvaluateFocalLength_Parms
		{
			float InFocus;
			float InZoom;
			FFocalLengthInfo OutEvaluatedValue;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutEvaluatedValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateFocalLength_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateFocalLength_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_OutEvaluatedValue = { "OutEvaluatedValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateFocalLength_Parms, OutEvaluatedValue), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventEvaluateFocalLength_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventEvaluateFocalLength_Parms), &Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_OutEvaluatedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Returns interpolated focal length */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns interpolated focal length" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateFocalLength", nullptr, nullptr, sizeof(LensFile_eventEvaluateFocalLength_Parms), Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateFocalLength()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateFocalLength_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics
	{
		struct LensFile_eventEvaluateImageCenterParameters_Parms
		{
			float InFocus;
			float InZoom;
			FImageCenterInfo OutEvaluatedValue;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutEvaluatedValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateImageCenterParameters_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateImageCenterParameters_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_OutEvaluatedValue = { "OutEvaluatedValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateImageCenterParameters_Parms, OutEvaluatedValue), Z_Construct_UScriptStruct_FImageCenterInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventEvaluateImageCenterParameters_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventEvaluateImageCenterParameters_Parms), &Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_OutEvaluatedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Returns interpolated image center parameters based on input focus and zoom */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns interpolated image center parameters based on input focus and zoom" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateImageCenterParameters", nullptr, nullptr, sizeof(LensFile_eventEvaluateImageCenterParameters_Parms), Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics
	{
		struct LensFile_eventEvaluateNodalPointOffset_Parms
		{
			float InFocus;
			float InZoom;
			FNodalPointOffset OutEvaluatedValue;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutEvaluatedValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNodalPointOffset_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNodalPointOffset_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_OutEvaluatedValue = { "OutEvaluatedValue", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNodalPointOffset_Parms, OutEvaluatedValue), Z_Construct_UScriptStruct_FNodalPointOffset, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventEvaluateNodalPointOffset_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventEvaluateNodalPointOffset_Parms), &Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_OutEvaluatedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Returns interpolated nodal point offset based on input focus and zoom */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns interpolated nodal point offset based on input focus and zoom" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateNodalPointOffset", nullptr, nullptr, sizeof(LensFile_eventEvaluateNodalPointOffset_Parms), Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics
	{
		struct LensFile_eventEvaluateNormalizedFocus_Parms
		{
			float InNormalizedValue;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InNormalizedValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::NewProp_InNormalizedValue = { "InNormalizedValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNormalizedFocus_Parms, InNormalizedValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNormalizedFocus_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::NewProp_InNormalizedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Returns interpolated focus based on input normalized value and mapping */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns interpolated focus based on input normalized value and mapping" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateNormalizedFocus", nullptr, nullptr, sizeof(LensFile_eventEvaluateNormalizedFocus_Parms), Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics
	{
		struct LensFile_eventEvaluateNormalizedIris_Parms
		{
			float InNormalizedValue;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InNormalizedValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::NewProp_InNormalizedValue = { "InNormalizedValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNormalizedIris_Parms, InNormalizedValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventEvaluateNormalizedIris_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::NewProp_InNormalizedValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::Function_MetaDataParams[] = {
		{ "Category", "Utility" },
		{ "Comment", "/** Returns interpolated iris based on input normalized value and mapping */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns interpolated iris based on input normalized value and mapping" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "EvaluateNormalizedIris", nullptr, nullptr, sizeof(LensFile_eventEvaluateNormalizedIris_Parms), Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics
	{
		struct LensFile_eventGetDistortionPoint_Parms
		{
			float InFocus;
			float InZoom;
			FDistortionInfo OutDistortionInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutDistortionInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetDistortionPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetDistortionPoint_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_OutDistortionInfo = { "OutDistortionInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetDistortionPoint_Parms, OutDistortionInfo), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventGetDistortionPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventGetDistortionPoint_Parms), &Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_OutDistortionInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets a Distortion point by given focus and zoom, if point does not exists returns false */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets a Distortion point by given focus and zoom, if point does not exists returns false" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetDistortionPoint", nullptr, nullptr, sizeof(LensFile_eventGetDistortionPoint_Parms), Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetDistortionPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetDistortionPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics
	{
		struct LensFile_eventGetDistortionPoints_Parms
		{
			TArray<FDistortionPointInfo> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDistortionPointInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetDistortionPoints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets all Distortion points struct with focus, zoom and info  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets all Distortion points struct with focus, zoom and info" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetDistortionPoints", nullptr, nullptr, sizeof(LensFile_eventGetDistortionPoints_Parms), Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetDistortionPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetDistortionPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics
	{
		struct LensFile_eventGetFocalLengthPoint_Parms
		{
			float InFocus;
			float InZoom;
			FFocalLengthInfo OutFocalLengthInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutFocalLengthInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetFocalLengthPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetFocalLengthPoint_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_OutFocalLengthInfo = { "OutFocalLengthInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetFocalLengthPoint_Parms, OutFocalLengthInfo), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventGetFocalLengthPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventGetFocalLengthPoint_Parms), &Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_OutFocalLengthInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets a Focal Length point by given focus and zoom, if point does not exists returns false */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets a Focal Length point by given focus and zoom, if point does not exists returns false" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetFocalLengthPoint", nullptr, nullptr, sizeof(LensFile_eventGetFocalLengthPoint_Parms), Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetFocalLengthPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetFocalLengthPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics
	{
		struct LensFile_eventGetFocalLengthPoints_Parms
		{
			TArray<FFocalLengthPointInfo> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFocalLengthPointInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetFocalLengthPoints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets all Focal Length points struct with focus, zoom and info  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets all Focal Length points struct with focus, zoom and info" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetFocalLengthPoints", nullptr, nullptr, sizeof(LensFile_eventGetFocalLengthPoints_Parms), Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetFocalLengthPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetFocalLengthPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics
	{
		struct LensFile_eventGetImageCenterPoint_Parms
		{
			float InFocus;
			float InZoom;
			FImageCenterInfo OutImageCenterInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutImageCenterInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetImageCenterPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetImageCenterPoint_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_OutImageCenterInfo = { "OutImageCenterInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetImageCenterPoint_Parms, OutImageCenterInfo), Z_Construct_UScriptStruct_FImageCenterInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventGetImageCenterPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventGetImageCenterPoint_Parms), &Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_OutImageCenterInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets a Image Center point by given focus and zoom, if point does not exists returns false */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets a Image Center point by given focus and zoom, if point does not exists returns false" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetImageCenterPoint", nullptr, nullptr, sizeof(LensFile_eventGetImageCenterPoint_Parms), Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetImageCenterPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetImageCenterPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics
	{
		struct LensFile_eventGetImageCenterPoints_Parms
		{
			TArray<FImageCenterPointInfo> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FImageCenterPointInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetImageCenterPoints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets all Image Center points struct with focus, zoom and info  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets all Image Center points struct with focus, zoom and info" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetImageCenterPoints", nullptr, nullptr, sizeof(LensFile_eventGetImageCenterPoints_Parms), Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetImageCenterPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetImageCenterPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics
	{
		struct LensFile_eventGetNodalOffsetPoint_Parms
		{
			float InFocus;
			float InZoom;
			FNodalPointOffset OutNodalPointOffset;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutNodalPointOffset;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetNodalOffsetPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetNodalOffsetPoint_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_OutNodalPointOffset = { "OutNodalPointOffset", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetNodalOffsetPoint_Parms, OutNodalPointOffset), Z_Construct_UScriptStruct_FNodalPointOffset, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventGetNodalOffsetPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventGetNodalOffsetPoint_Parms), &Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_OutNodalPointOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets a Nodal Offset point by given focus and zoom, if point does not exists returns false */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets a Nodal Offset point by given focus and zoom, if point does not exists returns false" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetNodalOffsetPoint", nullptr, nullptr, sizeof(LensFile_eventGetNodalOffsetPoint_Parms), Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics
	{
		struct LensFile_eventGetNodalOffsetPoints_Parms
		{
			TArray<FNodalOffsetPointInfo> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNodalOffsetPointInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetNodalOffsetPoints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets all Nodal Offset points struct with focus, zoom and info  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets all Nodal Offset points struct with focus, zoom and info" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetNodalOffsetPoints", nullptr, nullptr, sizeof(LensFile_eventGetNodalOffsetPoints_Parms), Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics
	{
		struct LensFile_eventGetSTMapPoint_Parms
		{
			float InFocus;
			float InZoom;
			FSTMapInfo OutSTMapInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutSTMapInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetSTMapPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetSTMapPoint_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_OutSTMapInfo = { "OutSTMapInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetSTMapPoint_Parms, OutSTMapInfo), Z_Construct_UScriptStruct_FSTMapInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventGetSTMapPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventGetSTMapPoint_Parms), &Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_InZoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_OutSTMapInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets a ST Map point by given focus and zoom, if point does not exists returns false */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets a ST Map point by given focus and zoom, if point does not exists returns false" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetSTMapPoint", nullptr, nullptr, sizeof(LensFile_eventGetSTMapPoint_Parms), Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetSTMapPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetSTMapPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics
	{
		struct LensFile_eventGetSTMapPoints_Parms
		{
			TArray<FSTMapPointInfo> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSTMapPointInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventGetSTMapPoints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Gets all ST Map points struct with focus, zoom and info  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Gets all ST Map points struct with focus, zoom and info" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "GetSTMapPoints", nullptr, nullptr, sizeof(LensFile_eventGetSTMapPoints_Parms), Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_GetSTMapPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_GetSTMapPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics
	{
		struct LensFile_eventHasFocusEncoderMapping_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventHasFocusEncoderMapping_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventHasFocusEncoderMapping_Parms), &Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Whether focus encoder mapping is configured */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Whether focus encoder mapping is configured" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "HasFocusEncoderMapping", nullptr, nullptr, sizeof(LensFile_eventHasFocusEncoderMapping_Parms), Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics
	{
		struct LensFile_eventHasIrisEncoderMapping_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventHasIrisEncoderMapping_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventHasIrisEncoderMapping_Parms), &Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Whether iris encoder mapping is configured */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Whether iris encoder mapping is configured" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "HasIrisEncoderMapping", nullptr, nullptr, sizeof(LensFile_eventHasIrisEncoderMapping_Parms), Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_HasSamples_Statics
	{
		struct LensFile_eventHasSamples_Parms
		{
			ELensDataCategory InDataCategory;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InDataCategory_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InDataCategory;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_InDataCategory_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_InDataCategory = { "InDataCategory", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventHasSamples_Parms, InDataCategory), Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LensFile_eventHasSamples_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LensFile_eventHasSamples_Parms), &Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_HasSamples_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_InDataCategory_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_InDataCategory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_HasSamples_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_HasSamples_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Returns whether a category has data samples */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Returns whether a category has data samples" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_HasSamples_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "HasSamples", nullptr, nullptr, sizeof(LensFile_eventHasSamples_Parms), Z_Construct_UFunction_ULensFile_HasSamples_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_HasSamples_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_HasSamples_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_HasSamples_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_HasSamples()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_HasSamples_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics
	{
		struct LensFile_eventRemoveFocusPoint_Parms
		{
			ELensDataCategory InDataCategory;
			float InFocus;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InDataCategory_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InDataCategory;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::NewProp_InDataCategory_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::NewProp_InDataCategory = { "InDataCategory", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventRemoveFocusPoint_Parms, InDataCategory), Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventRemoveFocusPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::NewProp_InDataCategory_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::NewProp_InDataCategory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::NewProp_InFocus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Removes a focus point */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Removes a focus point" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "RemoveFocusPoint", nullptr, nullptr, sizeof(LensFile_eventRemoveFocusPoint_Parms), Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_RemoveFocusPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_RemoveFocusPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics
	{
		struct LensFile_eventRemoveZoomPoint_Parms
		{
			ELensDataCategory InDataCategory;
			float InFocus;
			float InZoom;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InDataCategory_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InDataCategory;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InFocus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InZoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InDataCategory_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InDataCategory = { "InDataCategory", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventRemoveZoomPoint_Parms, InDataCategory), Z_Construct_UEnum_CameraCalibrationCore_ELensDataCategory, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InFocus = { "InFocus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventRemoveZoomPoint_Parms, InFocus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InZoom = { "InZoom", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LensFile_eventRemoveZoomPoint_Parms, InZoom), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InDataCategory_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InDataCategory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InFocus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::NewProp_InZoom,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Table" },
		{ "Comment", "/** Removes a zoom point */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Removes a zoom point" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULensFile, nullptr, "RemoveZoomPoint", nullptr, nullptr, sizeof(LensFile_eventRemoveZoomPoint_Parms), Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULensFile_RemoveZoomPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULensFile_RemoveZoomPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULensFile_NoRegister()
	{
		return ULensFile::StaticClass();
	}
	struct Z_Construct_UClass_ULensFile_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensInfo;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataMode;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserMetadata_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserMetadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_UserMetadata;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EncodersTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EncodersTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLengthTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageCenterTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageCenterTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodalOffsetTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NodalOffsetTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_STMapTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_STMapTable;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UndistortionDisplacementMapHolders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UndistortionDisplacementMapHolders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UndistortionDisplacementMapHolders;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DistortionDisplacementMapHolders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionDisplacementMapHolders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DistortionDisplacementMapHolders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensFile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULensFile_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULensFile_AddDistortionPoint, "AddDistortionPoint" }, // 415674536
		{ &Z_Construct_UFunction_ULensFile_AddFocalLengthPoint, "AddFocalLengthPoint" }, // 619585135
		{ &Z_Construct_UFunction_ULensFile_AddImageCenterPoint, "AddImageCenterPoint" }, // 3666182191
		{ &Z_Construct_UFunction_ULensFile_AddNodalOffsetPoint, "AddNodalOffsetPoint" }, // 2168751768
		{ &Z_Construct_UFunction_ULensFile_AddSTMapPoint, "AddSTMapPoint" }, // 1548912715
		{ &Z_Construct_UFunction_ULensFile_ClearAll, "ClearAll" }, // 4106782298
		{ &Z_Construct_UFunction_ULensFile_ClearData, "ClearData" }, // 329099435
		{ &Z_Construct_UFunction_ULensFile_EvaluateDistortionData, "EvaluateDistortionData" }, // 2051765210
		{ &Z_Construct_UFunction_ULensFile_EvaluateDistortionParameters, "EvaluateDistortionParameters" }, // 3397363099
		{ &Z_Construct_UFunction_ULensFile_EvaluateFocalLength, "EvaluateFocalLength" }, // 2396763584
		{ &Z_Construct_UFunction_ULensFile_EvaluateImageCenterParameters, "EvaluateImageCenterParameters" }, // 2030903977
		{ &Z_Construct_UFunction_ULensFile_EvaluateNodalPointOffset, "EvaluateNodalPointOffset" }, // 1999802905
		{ &Z_Construct_UFunction_ULensFile_EvaluateNormalizedFocus, "EvaluateNormalizedFocus" }, // 3037959579
		{ &Z_Construct_UFunction_ULensFile_EvaluateNormalizedIris, "EvaluateNormalizedIris" }, // 4153453553
		{ &Z_Construct_UFunction_ULensFile_GetDistortionPoint, "GetDistortionPoint" }, // 811371368
		{ &Z_Construct_UFunction_ULensFile_GetDistortionPoints, "GetDistortionPoints" }, // 3089791441
		{ &Z_Construct_UFunction_ULensFile_GetFocalLengthPoint, "GetFocalLengthPoint" }, // 2021528831
		{ &Z_Construct_UFunction_ULensFile_GetFocalLengthPoints, "GetFocalLengthPoints" }, // 974542259
		{ &Z_Construct_UFunction_ULensFile_GetImageCenterPoint, "GetImageCenterPoint" }, // 2682726536
		{ &Z_Construct_UFunction_ULensFile_GetImageCenterPoints, "GetImageCenterPoints" }, // 948357468
		{ &Z_Construct_UFunction_ULensFile_GetNodalOffsetPoint, "GetNodalOffsetPoint" }, // 4286416603
		{ &Z_Construct_UFunction_ULensFile_GetNodalOffsetPoints, "GetNodalOffsetPoints" }, // 2681832010
		{ &Z_Construct_UFunction_ULensFile_GetSTMapPoint, "GetSTMapPoint" }, // 3784084385
		{ &Z_Construct_UFunction_ULensFile_GetSTMapPoints, "GetSTMapPoints" }, // 296209955
		{ &Z_Construct_UFunction_ULensFile_HasFocusEncoderMapping, "HasFocusEncoderMapping" }, // 2364145172
		{ &Z_Construct_UFunction_ULensFile_HasIrisEncoderMapping, "HasIrisEncoderMapping" }, // 881282431
		{ &Z_Construct_UFunction_ULensFile_HasSamples, "HasSamples" }, // 1050069978
		{ &Z_Construct_UFunction_ULensFile_RemoveFocusPoint, "RemoveFocusPoint" }, // 4098273669
		{ &Z_Construct_UFunction_ULensFile_RemoveZoomPoint, "RemoveZoomPoint" }, // 679475527
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A Lens file containing calibration mapping from FIZ data\n */" },
		{ "IncludePath", "LensFile.h" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "A Lens file containing calibration mapping from FIZ data" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_LensInfo_MetaData[] = {
		{ "Category", "Lens info" },
		{ "Comment", "/** Lens information */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Lens information" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_LensInfo = { "LensInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, LensInfo), Z_Construct_UScriptStruct_FLensInfo, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_LensInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_LensInfo_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode_MetaData[] = {
		{ "Category", "Lens mapping" },
		{ "Comment", "/** Type of data used for lens mapping */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Type of data used for lens mapping" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode = { "DataMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, DataMode), Z_Construct_UEnum_CameraCalibrationCore_ELensDataMode, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_ValueProp = { "UserMetadata", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_Key_KeyProp = { "UserMetadata_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_MetaData[] = {
		{ "Category", "Metadata" },
		{ "Comment", "/** Metadata user could enter for its lens */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Metadata user could enter for its lens" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata = { "UserMetadata", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, UserMetadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_EncodersTable_MetaData[] = {
		{ "Comment", "/** Encoder mapping table */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Encoder mapping table" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_EncodersTable = { "EncodersTable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, EncodersTable), Z_Construct_UScriptStruct_FEncodersTable, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_EncodersTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_EncodersTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionTable_MetaData[] = {
		{ "Comment", "/** Distortion parameters table mapping to input focus/zoom  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Distortion parameters table mapping to input focus/zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionTable = { "DistortionTable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, DistortionTable), Z_Construct_UScriptStruct_FDistortionTable, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_FocalLengthTable_MetaData[] = {
		{ "Comment", "/** Focal length table mapping to input focus/zoom  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Focal length table mapping to input focus/zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_FocalLengthTable = { "FocalLengthTable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, FocalLengthTable), Z_Construct_UScriptStruct_FFocalLengthTable, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_FocalLengthTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_FocalLengthTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_ImageCenterTable_MetaData[] = {
		{ "Comment", "/** Image center table mapping to input focus/zoom  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Image center table mapping to input focus/zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_ImageCenterTable = { "ImageCenterTable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, ImageCenterTable), Z_Construct_UScriptStruct_FImageCenterTable, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_ImageCenterTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_ImageCenterTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_NodalOffsetTable_MetaData[] = {
		{ "Comment", "/** Nodal offset table mapping to input focus/zoom  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Nodal offset table mapping to input focus/zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_NodalOffsetTable = { "NodalOffsetTable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, NodalOffsetTable), Z_Construct_UScriptStruct_FNodalOffsetTable, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_NodalOffsetTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_NodalOffsetTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_STMapTable_MetaData[] = {
		{ "Comment", "/** STMap table mapping to input focus/zoom  */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "STMap table mapping to input focus/zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_STMapTable = { "STMapTable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, STMapTable), Z_Construct_UScriptStruct_FSTMapTable, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_STMapTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_STMapTable_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders_Inner = { "UndistortionDisplacementMapHolders", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders_MetaData[] = {
		{ "Comment", "/** Texture used to store temporary undistortion displacement map when using map blending */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Texture used to store temporary undistortion displacement map when using map blending" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders = { "UndistortionDisplacementMapHolders", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, UndistortionDisplacementMapHolders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders_Inner = { "DistortionDisplacementMapHolders", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders_MetaData[] = {
		{ "Comment", "/** Texture used to store temporary distortion displacement map when using map blending */" },
		{ "ModuleRelativePath", "Public/LensFile.h" },
		{ "ToolTip", "Texture used to store temporary distortion displacement map when using map blending" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders = { "DistortionDisplacementMapHolders", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensFile, DistortionDisplacementMapHolders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULensFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_LensInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_DataMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_UserMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_EncodersTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_FocalLengthTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_ImageCenterTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_NodalOffsetTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_STMapTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_UndistortionDisplacementMapHolders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensFile_Statics::NewProp_DistortionDisplacementMapHolders,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensFile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensFile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensFile_Statics::ClassParams = {
		&ULensFile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULensFile_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULensFile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensFile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensFile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensFile, 3376915271);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<ULensFile>()
	{
		return ULensFile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensFile(Z_Construct_UClass_ULensFile, &ULensFile::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("ULensFile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensFile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
