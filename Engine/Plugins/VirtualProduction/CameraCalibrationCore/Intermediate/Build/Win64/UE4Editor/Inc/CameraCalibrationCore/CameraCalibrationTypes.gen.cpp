// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/CameraCalibrationTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraCalibrationTypes() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionHandlerPicker();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FDistortionHandlerPicker::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionHandlerPicker_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionHandlerPicker, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionHandlerPicker"), sizeof(FDistortionHandlerPicker), Get_Z_Construct_UScriptStruct_FDistortionHandlerPicker_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionHandlerPicker>()
{
	return FDistortionHandlerPicker::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionHandlerPicker(FDistortionHandlerPicker::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionHandlerPicker"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionHandlerPicker
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionHandlerPicker()
	{
		UScriptStruct::DeferCppStructOps<FDistortionHandlerPicker>(FName(TEXT("DistortionHandlerPicker")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionHandlerPicker;
	struct Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionProducerID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionProducerID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandlerDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_HandlerDisplayName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Utility structure for selecting a distortion handler from the camera calibration subsystem */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationTypes.h" },
		{ "ToolTip", "Utility structure for selecting a distortion handler from the camera calibration subsystem" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionHandlerPicker>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_TargetCameraComponent_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** CineCameraComponent with which the desired distortion handler is associated */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/CameraCalibrationTypes.h" },
		{ "ToolTip", "CineCameraComponent with which the desired distortion handler is associated" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_TargetCameraComponent = { "TargetCameraComponent", nullptr, (EPropertyFlags)0x001000000008200c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionHandlerPicker, TargetCameraComponent), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_TargetCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_TargetCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_DistortionProducerID_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** UObject that produces the distortion state for the desired distortion handler */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationTypes.h" },
		{ "ToolTip", "UObject that produces the distortion state for the desired distortion handler" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_DistortionProducerID = { "DistortionProducerID", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionHandlerPicker, DistortionProducerID), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_DistortionProducerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_DistortionProducerID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_HandlerDisplayName_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Display name of the desired distortion handler */" },
		{ "ModuleRelativePath", "Public/CameraCalibrationTypes.h" },
		{ "ToolTip", "Display name of the desired distortion handler" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_HandlerDisplayName = { "HandlerDisplayName", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionHandlerPicker, HandlerDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_HandlerDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_HandlerDisplayName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_TargetCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_DistortionProducerID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::NewProp_HandlerDisplayName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"DistortionHandlerPicker",
		sizeof(FDistortionHandlerPicker),
		alignof(FDistortionHandlerPicker),
		Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionHandlerPicker()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionHandlerPicker_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionHandlerPicker"), sizeof(FDistortionHandlerPicker), Get_Z_Construct_UScriptStruct_FDistortionHandlerPicker_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionHandlerPicker_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionHandlerPicker_Hash() { return 2279322934U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
