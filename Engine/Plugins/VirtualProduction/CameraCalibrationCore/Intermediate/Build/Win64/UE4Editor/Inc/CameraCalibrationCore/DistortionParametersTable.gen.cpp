// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/DistortionParametersTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDistortionParametersTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionFocusPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionZoomPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfo();
// End Cross Module References

static_assert(std::is_polymorphic<FDistortionTable>() == std::is_polymorphic<FBaseLensTable>(), "USTRUCT FDistortionTable cannot be polymorphic unless super FBaseLensTable is polymorphic");

class UScriptStruct* FDistortionTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionTable"), sizeof(FDistortionTable), Get_Z_Construct_UScriptStruct_FDistortionTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionTable>()
{
	return FDistortionTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionTable(FDistortionTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionTable()
	{
		UScriptStruct::DeferCppStructOps<FDistortionTable>(FName(TEXT("DistortionTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionTable;
	struct Z_Construct_UScriptStruct_FDistortionTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocusPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Distortion table containing list of points for each focus and zoom input\n */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Distortion table containing list of points for each focus and zoom input" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionTable>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints_Inner = { "FocusPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDistortionFocusPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints_MetaData[] = {
		{ "Comment", "/** Lists of focus points */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Lists of focus points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints = { "FocusPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionTable, FocusPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionTable_Statics::NewProp_FocusPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseLensTable,
		&NewStructOps,
		"DistortionTable",
		sizeof(FDistortionTable),
		alignof(FDistortionTable),
		Z_Construct_UScriptStruct_FDistortionTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionTable"), sizeof(FDistortionTable), Get_Z_Construct_UScriptStruct_FDistortionTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionTable_Hash() { return 3336653683U; }

static_assert(std::is_polymorphic<FDistortionFocusPoint>() == std::is_polymorphic<FBaseFocusPoint>(), "USTRUCT FDistortionFocusPoint cannot be polymorphic unless super FBaseFocusPoint is polymorphic");

class UScriptStruct* FDistortionFocusPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionFocusPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionFocusPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionFocusPoint"), sizeof(FDistortionFocusPoint), Get_Z_Construct_UScriptStruct_FDistortionFocusPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionFocusPoint>()
{
	return FDistortionFocusPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionFocusPoint(FDistortionFocusPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionFocusPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionFocusPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionFocusPoint()
	{
		UScriptStruct::DeferCppStructOps<FDistortionFocusPoint>(FName(TEXT("DistortionFocusPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionFocusPoint;
	struct Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapBlendingCurve_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MapBlendingCurve;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ZoomPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZoomPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ZoomPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Contains list of distortion parameters points associated to zoom value\n */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Contains list of distortion parameters points associated to zoom value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionFocusPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_Focus_MetaData[] = {
		{ "Comment", "/** Input focus value for this point */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Input focus value for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionFocusPoint, Focus), METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_MapBlendingCurve_MetaData[] = {
		{ "Comment", "/** Curves describing desired blending between resulting displacement maps */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Curves describing desired blending between resulting displacement maps" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_MapBlendingCurve = { "MapBlendingCurve", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionFocusPoint, MapBlendingCurve), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_MapBlendingCurve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_MapBlendingCurve_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints_Inner = { "ZoomPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDistortionZoomPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints_MetaData[] = {
		{ "Comment", "/** List of zoom points */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "List of zoom points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints = { "ZoomPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionFocusPoint, ZoomPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_MapBlendingCurve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::NewProp_ZoomPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseFocusPoint,
		&NewStructOps,
		"DistortionFocusPoint",
		sizeof(FDistortionFocusPoint),
		alignof(FDistortionFocusPoint),
		Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionFocusPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionFocusPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionFocusPoint"), sizeof(FDistortionFocusPoint), Get_Z_Construct_UScriptStruct_FDistortionFocusPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionFocusPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionFocusPoint_Hash() { return 3314276953U; }
class UScriptStruct* FDistortionZoomPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionZoomPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionZoomPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionZoomPoint"), sizeof(FDistortionZoomPoint), Get_Z_Construct_UScriptStruct_FDistortionZoomPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionZoomPoint>()
{
	return FDistortionZoomPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionZoomPoint(FDistortionZoomPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionZoomPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionZoomPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionZoomPoint()
	{
		UScriptStruct::DeferCppStructOps<FDistortionZoomPoint>(FName(TEXT("DistortionZoomPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionZoomPoint;
	struct Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Zoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Zoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Distortion parameters associated to a zoom value\n */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Distortion parameters associated to a zoom value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionZoomPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_Zoom_MetaData[] = {
		{ "Comment", "/** Input zoom value for this point */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Input zoom value for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_Zoom = { "Zoom", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionZoomPoint, Zoom), METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_Zoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_Zoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_DistortionInfo_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Distortion parameters for this point */" },
		{ "ModuleRelativePath", "Public/Tables/DistortionParametersTable.h" },
		{ "ToolTip", "Distortion parameters for this point" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_DistortionInfo = { "DistortionInfo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionZoomPoint, DistortionInfo), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_DistortionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_DistortionInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_Zoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::NewProp_DistortionInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"DistortionZoomPoint",
		sizeof(FDistortionZoomPoint),
		alignof(FDistortionZoomPoint),
		Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionZoomPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionZoomPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionZoomPoint"), sizeof(FDistortionZoomPoint), Get_Z_Construct_UScriptStruct_FDistortionZoomPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionZoomPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionZoomPoint_Hash() { return 4238167658U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
