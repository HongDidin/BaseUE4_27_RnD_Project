// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/CameraCalibrationStep.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraCalibrationStep() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationStep_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationStep();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
// End Cross Module References
	void UCameraCalibrationStep::StaticRegisterNativesUCameraCalibrationStep()
	{
	}
	UClass* Z_Construct_UClass_UCameraCalibrationStep_NoRegister()
	{
		return UCameraCalibrationStep::StaticClass();
	}
	struct Z_Construct_UClass_UCameraCalibrationStep_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraCalibrationStep_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraCalibrationStep_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Interface of a camera calibration step. These will appear in a Camera Calibration Toolkit tab.\n */" },
		{ "IncludePath", "CameraCalibrationStep.h" },
		{ "ModuleRelativePath", "Public/CameraCalibrationStep.h" },
		{ "ToolTip", "Interface of a camera calibration step. These will appear in a Camera Calibration Toolkit tab." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraCalibrationStep_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraCalibrationStep>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraCalibrationStep_Statics::ClassParams = {
		&UCameraCalibrationStep::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraCalibrationStep_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraCalibrationStep_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraCalibrationStep()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraCalibrationStep_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraCalibrationStep, 3074575635);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<UCameraCalibrationStep>()
	{
		return UCameraCalibrationStep::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraCalibrationStep(Z_Construct_UClass_UCameraCalibrationStep, &UCameraCalibrationStep::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("UCameraCalibrationStep"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraCalibrationStep);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
