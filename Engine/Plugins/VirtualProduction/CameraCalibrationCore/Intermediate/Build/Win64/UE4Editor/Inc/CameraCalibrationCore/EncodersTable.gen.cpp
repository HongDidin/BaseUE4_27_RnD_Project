// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/EncodersTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEncodersTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FEncodersTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
// End Cross Module References
class UScriptStruct* FEncodersTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FEncodersTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEncodersTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("EncodersTable"), sizeof(FEncodersTable), Get_Z_Construct_UScriptStruct_FEncodersTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FEncodersTable>()
{
	return FEncodersTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEncodersTable(FEncodersTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("EncodersTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFEncodersTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFEncodersTable()
	{
		UScriptStruct::DeferCppStructOps<FEncodersTable>(FName(TEXT("EncodersTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFEncodersTable;
	struct Z_Construct_UScriptStruct_FEncodersTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Iris_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Iris;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEncodersTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Encoder table containing mapping from raw input value to nominal value\n */" },
		{ "ModuleRelativePath", "Public/Tables/EncodersTable.h" },
		{ "ToolTip", "Encoder table containing mapping from raw input value to nominal value" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEncodersTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEncodersTable>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Focus_MetaData[] = {
		{ "Comment", "/** Focus curve from encoder values to nominal values */" },
		{ "ModuleRelativePath", "Public/Tables/EncodersTable.h" },
		{ "ToolTip", "Focus curve from encoder values to nominal values" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEncodersTable, Focus), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Iris_MetaData[] = {
		{ "Comment", "/** Iris curve from encoder values to nominal values */" },
		{ "ModuleRelativePath", "Public/Tables/EncodersTable.h" },
		{ "ToolTip", "Iris curve from encoder values to nominal values" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Iris = { "Iris", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEncodersTable, Iris), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Iris_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Iris_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEncodersTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEncodersTable_Statics::NewProp_Iris,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEncodersTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"EncodersTable",
		sizeof(FEncodersTable),
		alignof(FEncodersTable),
		Z_Construct_UScriptStruct_FEncodersTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncodersTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEncodersTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncodersTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEncodersTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEncodersTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EncodersTable"), sizeof(FEncodersTable), Get_Z_Construct_UScriptStruct_FEncodersTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEncodersTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEncodersTable_Hash() { return 889099114U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
