// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/CameraNodalOffsetAlgo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraNodalOffsetAlgo() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgo_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgo();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
// End Cross Module References
	void UCameraNodalOffsetAlgo::StaticRegisterNativesUCameraNodalOffsetAlgo()
	{
	}
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgo_NoRegister()
	{
		return UCameraNodalOffsetAlgo::StaticClass();
	}
	struct Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UCameraNodalOffsetAlgo defines the interface that any nodal calibration point algorithm should implement\n * in order to be used and listed by the Nodal Offset Tool. \n */" },
		{ "IncludePath", "CameraNodalOffsetAlgo.h" },
		{ "ModuleRelativePath", "Public/CameraNodalOffsetAlgo.h" },
		{ "ToolTip", "UCameraNodalOffsetAlgo defines the interface that any nodal calibration point algorithm should implement\nin order to be used and listed by the Nodal Offset Tool." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraNodalOffsetAlgo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::ClassParams = {
		&UCameraNodalOffsetAlgo::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraNodalOffsetAlgo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraNodalOffsetAlgo, 1790059801);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<UCameraNodalOffsetAlgo>()
	{
		return UCameraNodalOffsetAlgo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraNodalOffsetAlgo(Z_Construct_UClass_UCameraNodalOffsetAlgo, &UCameraNodalOffsetAlgo::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("UCameraNodalOffsetAlgo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraNodalOffsetAlgo);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
