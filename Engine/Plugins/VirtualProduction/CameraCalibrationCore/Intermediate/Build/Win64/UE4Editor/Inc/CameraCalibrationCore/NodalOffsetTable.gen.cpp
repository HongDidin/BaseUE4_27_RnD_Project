// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/NodalOffsetTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNodalOffsetTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetFocusPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
// End Cross Module References

static_assert(std::is_polymorphic<FNodalOffsetTable>() == std::is_polymorphic<FBaseLensTable>(), "USTRUCT FNodalOffsetTable cannot be polymorphic unless super FBaseLensTable is polymorphic");

class UScriptStruct* FNodalOffsetTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNodalOffsetTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("NodalOffsetTable"), sizeof(FNodalOffsetTable), Get_Z_Construct_UScriptStruct_FNodalOffsetTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FNodalOffsetTable>()
{
	return FNodalOffsetTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNodalOffsetTable(FNodalOffsetTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("NodalOffsetTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetTable()
	{
		UScriptStruct::DeferCppStructOps<FNodalOffsetTable>(FName(TEXT("NodalOffsetTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetTable;
	struct Z_Construct_UScriptStruct_FNodalOffsetTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocusPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Table containing nodal offset mapping to focus and zoom\n */" },
		{ "ModuleRelativePath", "Public/Tables/NodalOffsetTable.h" },
		{ "ToolTip", "Table containing nodal offset mapping to focus and zoom" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNodalOffsetTable>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints_Inner = { "FocusPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNodalOffsetFocusPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints_MetaData[] = {
		{ "Comment", "/** Lists of focus points */" },
		{ "ModuleRelativePath", "Public/Tables/NodalOffsetTable.h" },
		{ "ToolTip", "Lists of focus points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints = { "FocusPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNodalOffsetTable, FocusPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::NewProp_FocusPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseLensTable,
		&NewStructOps,
		"NodalOffsetTable",
		sizeof(FNodalOffsetTable),
		alignof(FNodalOffsetTable),
		Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NodalOffsetTable"), sizeof(FNodalOffsetTable), Get_Z_Construct_UScriptStruct_FNodalOffsetTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNodalOffsetTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetTable_Hash() { return 88685275U; }

static_assert(std::is_polymorphic<FNodalOffsetFocusPoint>() == std::is_polymorphic<FBaseFocusPoint>(), "USTRUCT FNodalOffsetFocusPoint cannot be polymorphic unless super FBaseFocusPoint is polymorphic");

class UScriptStruct* FNodalOffsetFocusPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("NodalOffsetFocusPoint"), sizeof(FNodalOffsetFocusPoint), Get_Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FNodalOffsetFocusPoint>()
{
	return FNodalOffsetFocusPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNodalOffsetFocusPoint(FNodalOffsetFocusPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("NodalOffsetFocusPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetFocusPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetFocusPoint()
	{
		UScriptStruct::DeferCppStructOps<FNodalOffsetFocusPoint>(FName(TEXT("NodalOffsetFocusPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetFocusPoint;
	struct Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Focus point for nodal offset curves\n */" },
		{ "ModuleRelativePath", "Public/Tables/NodalOffsetTable.h" },
		{ "ToolTip", "Focus point for nodal offset curves" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNodalOffsetFocusPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_Focus_MetaData[] = {
		{ "Comment", "/** Input focus for this point */" },
		{ "ModuleRelativePath", "Public/Tables/NodalOffsetTable.h" },
		{ "ToolTip", "Input focus for this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNodalOffsetFocusPoint, Focus), METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_LocationOffset_MetaData[] = {
		{ "Comment", "/** XYZ offsets curves mapped to zoom */" },
		{ "ModuleRelativePath", "Public/Tables/NodalOffsetTable.h" },
		{ "ToolTip", "XYZ offsets curves mapped to zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_LocationOffset = { "LocationOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(LocationOffset, FNodalOffsetFocusPoint), STRUCT_OFFSET(FNodalOffsetFocusPoint, LocationOffset), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_LocationOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_LocationOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_RotationOffset_MetaData[] = {
		{ "Comment", "/** Yaw, Pitch and Roll offset curves mapped to zoom */" },
		{ "ModuleRelativePath", "Public/Tables/NodalOffsetTable.h" },
		{ "ToolTip", "Yaw, Pitch and Roll offset curves mapped to zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_RotationOffset = { "RotationOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(RotationOffset, FNodalOffsetFocusPoint), STRUCT_OFFSET(FNodalOffsetFocusPoint, RotationOffset), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_RotationOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_RotationOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_LocationOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::NewProp_RotationOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseFocusPoint,
		&NewStructOps,
		"NodalOffsetFocusPoint",
		sizeof(FNodalOffsetFocusPoint),
		alignof(FNodalOffsetFocusPoint),
		Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetFocusPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NodalOffsetFocusPoint"), sizeof(FNodalOffsetFocusPoint), Get_Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetFocusPoint_Hash() { return 1856877150U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
