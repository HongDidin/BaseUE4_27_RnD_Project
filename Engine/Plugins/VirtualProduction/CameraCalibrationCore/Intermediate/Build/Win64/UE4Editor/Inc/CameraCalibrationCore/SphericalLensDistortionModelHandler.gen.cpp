// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/SphericalLensDistortionModelHandler.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSphericalLensDistortionModelHandler() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_USphericalLensDistortionModelHandler_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_USphericalLensDistortionModelHandler();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
// End Cross Module References
	void USphericalLensDistortionModelHandler::StaticRegisterNativesUSphericalLensDistortionModelHandler()
	{
	}
	UClass* Z_Construct_UClass_USphericalLensDistortionModelHandler_NoRegister()
	{
		return USphericalLensDistortionModelHandler::StaticClass();
	}
	struct Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULensDistortionModelHandlerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Lens distortion handler for a spherical lens model that implements the Brown-Conrady polynomial model */" },
		{ "IncludePath", "SphericalLensDistortionModelHandler.h" },
		{ "ModuleRelativePath", "Public/SphericalLensDistortionModelHandler.h" },
		{ "ToolTip", "Lens distortion handler for a spherical lens model that implements the Brown-Conrady polynomial model" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USphericalLensDistortionModelHandler>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::ClassParams = {
		&USphericalLensDistortionModelHandler::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USphericalLensDistortionModelHandler()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USphericalLensDistortionModelHandler, 3456312149);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<USphericalLensDistortionModelHandler>()
	{
		return USphericalLensDistortionModelHandler::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USphericalLensDistortionModelHandler(Z_Construct_UClass_USphericalLensDistortionModelHandler, &USphericalLensDistortionModelHandler::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("USphericalLensDistortionModelHandler"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USphericalLensDistortionModelHandler);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
