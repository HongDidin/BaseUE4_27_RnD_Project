// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONCORE_SphericalLensDistortionModelHandler_generated_h
#error "SphericalLensDistortionModelHandler.generated.h already included, missing '#pragma once' in SphericalLensDistortionModelHandler.h"
#endif
#define CAMERACALIBRATIONCORE_SphericalLensDistortionModelHandler_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSphericalLensDistortionModelHandler(); \
	friend struct Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics; \
public: \
	DECLARE_CLASS(USphericalLensDistortionModelHandler, ULensDistortionModelHandlerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(USphericalLensDistortionModelHandler)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSphericalLensDistortionModelHandler(); \
	friend struct Z_Construct_UClass_USphericalLensDistortionModelHandler_Statics; \
public: \
	DECLARE_CLASS(USphericalLensDistortionModelHandler, ULensDistortionModelHandlerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationCore"), NO_API) \
	DECLARE_SERIALIZER(USphericalLensDistortionModelHandler)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USphericalLensDistortionModelHandler(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USphericalLensDistortionModelHandler) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USphericalLensDistortionModelHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USphericalLensDistortionModelHandler); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USphericalLensDistortionModelHandler(USphericalLensDistortionModelHandler&&); \
	NO_API USphericalLensDistortionModelHandler(const USphericalLensDistortionModelHandler&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USphericalLensDistortionModelHandler() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USphericalLensDistortionModelHandler(USphericalLensDistortionModelHandler&&); \
	NO_API USphericalLensDistortionModelHandler(const USphericalLensDistortionModelHandler&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USphericalLensDistortionModelHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USphericalLensDistortionModelHandler); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USphericalLensDistortionModelHandler)


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<class USphericalLensDistortionModelHandler>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibrationCore_Source_CameraCalibrationCore_Public_SphericalLensDistortionModelHandler_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
