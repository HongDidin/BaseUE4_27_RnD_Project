// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/LensData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensData() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetPointInfo();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataTablePointInfoBase();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FNodalPointOffset();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSTMapInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionPointInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfo();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensInfo();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensModel_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FNodalOffsetPointInfo>() == std::is_polymorphic<FDataTablePointInfoBase>(), "USTRUCT FNodalOffsetPointInfo cannot be polymorphic unless super FDataTablePointInfoBase is polymorphic");

class UScriptStruct* FNodalOffsetPointInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNodalOffsetPointInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("NodalOffsetPointInfo"), sizeof(FNodalOffsetPointInfo), Get_Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FNodalOffsetPointInfo>()
{
	return FNodalOffsetPointInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNodalOffsetPointInfo(FNodalOffsetPointInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("NodalOffsetPointInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetPointInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetPointInfo()
	{
		UScriptStruct::DeferCppStructOps<FNodalOffsetPointInfo>(FName(TEXT("NodalOffsetPointInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalOffsetPointInfo;
	struct Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodalPointOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NodalPointOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Nodal Point Point Info struct\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Nodal Point Point Info struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNodalOffsetPointInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::NewProp_NodalPointOffset_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** Nodal Point parameter */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Nodal Point parameter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::NewProp_NodalPointOffset = { "NodalPointOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNodalOffsetPointInfo, NodalPointOffset), Z_Construct_UScriptStruct_FNodalPointOffset, METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::NewProp_NodalPointOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::NewProp_NodalPointOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::NewProp_NodalPointOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FDataTablePointInfoBase,
		&NewStructOps,
		"NodalOffsetPointInfo",
		sizeof(FNodalOffsetPointInfo),
		alignof(FNodalOffsetPointInfo),
		Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNodalOffsetPointInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NodalOffsetPointInfo"), sizeof(FNodalOffsetPointInfo), Get_Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNodalOffsetPointInfo_Hash() { return 1982613322U; }

static_assert(std::is_polymorphic<FImageCenterPointInfo>() == std::is_polymorphic<FDataTablePointInfoBase>(), "USTRUCT FImageCenterPointInfo cannot be polymorphic unless super FDataTablePointInfoBase is polymorphic");

class UScriptStruct* FImageCenterPointInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FImageCenterPointInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FImageCenterPointInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("ImageCenterPointInfo"), sizeof(FImageCenterPointInfo), Get_Z_Construct_UScriptStruct_FImageCenterPointInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FImageCenterPointInfo>()
{
	return FImageCenterPointInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FImageCenterPointInfo(FImageCenterPointInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("ImageCenterPointInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterPointInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterPointInfo()
	{
		UScriptStruct::DeferCppStructOps<FImageCenterPointInfo>(FName(TEXT("ImageCenterPointInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterPointInfo;
	struct Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageCenterInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageCenterInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Image Center Point Info struct\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Image Center Point Info struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FImageCenterPointInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::NewProp_ImageCenterInfo_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** Image Center parameter */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Image Center parameter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::NewProp_ImageCenterInfo = { "ImageCenterInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FImageCenterPointInfo, ImageCenterInfo), Z_Construct_UScriptStruct_FImageCenterInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::NewProp_ImageCenterInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::NewProp_ImageCenterInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::NewProp_ImageCenterInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FDataTablePointInfoBase,
		&NewStructOps,
		"ImageCenterPointInfo",
		sizeof(FImageCenterPointInfo),
		alignof(FImageCenterPointInfo),
		Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FImageCenterPointInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FImageCenterPointInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ImageCenterPointInfo"), sizeof(FImageCenterPointInfo), Get_Z_Construct_UScriptStruct_FImageCenterPointInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FImageCenterPointInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FImageCenterPointInfo_Hash() { return 951302832U; }

static_assert(std::is_polymorphic<FSTMapPointInfo>() == std::is_polymorphic<FDataTablePointInfoBase>(), "USTRUCT FSTMapPointInfo cannot be polymorphic unless super FDataTablePointInfoBase is polymorphic");

class UScriptStruct* FSTMapPointInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FSTMapPointInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSTMapPointInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("STMapPointInfo"), sizeof(FSTMapPointInfo), Get_Z_Construct_UScriptStruct_FSTMapPointInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FSTMapPointInfo>()
{
	return FSTMapPointInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSTMapPointInfo(FSTMapPointInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("STMapPointInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapPointInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapPointInfo()
	{
		UScriptStruct::DeferCppStructOps<FSTMapPointInfo>(FName(TEXT("STMapPointInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapPointInfo;
	struct Z_Construct_UScriptStruct_FSTMapPointInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_STMapInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_STMapInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * ST Map Point Info struct\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "ST Map Point Info struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSTMapPointInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::NewProp_STMapInfo_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** ST Map parameter */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "ST Map parameter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::NewProp_STMapInfo = { "STMapInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapPointInfo, STMapInfo), Z_Construct_UScriptStruct_FSTMapInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::NewProp_STMapInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::NewProp_STMapInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::NewProp_STMapInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FDataTablePointInfoBase,
		&NewStructOps,
		"STMapPointInfo",
		sizeof(FSTMapPointInfo),
		alignof(FSTMapPointInfo),
		Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSTMapPointInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSTMapPointInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("STMapPointInfo"), sizeof(FSTMapPointInfo), Get_Z_Construct_UScriptStruct_FSTMapPointInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSTMapPointInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSTMapPointInfo_Hash() { return 1804932637U; }

static_assert(std::is_polymorphic<FFocalLengthPointInfo>() == std::is_polymorphic<FDataTablePointInfoBase>(), "USTRUCT FFocalLengthPointInfo cannot be polymorphic unless super FDataTablePointInfoBase is polymorphic");

class UScriptStruct* FFocalLengthPointInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FFocalLengthPointInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFocalLengthPointInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("FocalLengthPointInfo"), sizeof(FFocalLengthPointInfo), Get_Z_Construct_UScriptStruct_FFocalLengthPointInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FFocalLengthPointInfo>()
{
	return FFocalLengthPointInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFocalLengthPointInfo(FFocalLengthPointInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("FocalLengthPointInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthPointInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthPointInfo()
	{
		UScriptStruct::DeferCppStructOps<FFocalLengthPointInfo>(FName(TEXT("FocalLengthPointInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthPointInfo;
	struct Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLengthInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Focal Length Point Info struct\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Focal Length Point Info struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFocalLengthPointInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::NewProp_FocalLengthInfo_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** Focal Length parameter */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Focal Length parameter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::NewProp_FocalLengthInfo = { "FocalLengthInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthPointInfo, FocalLengthInfo), Z_Construct_UScriptStruct_FFocalLengthInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::NewProp_FocalLengthInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::NewProp_FocalLengthInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::NewProp_FocalLengthInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FDataTablePointInfoBase,
		&NewStructOps,
		"FocalLengthPointInfo",
		sizeof(FFocalLengthPointInfo),
		alignof(FFocalLengthPointInfo),
		Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthPointInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFocalLengthPointInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FocalLengthPointInfo"), sizeof(FFocalLengthPointInfo), Get_Z_Construct_UScriptStruct_FFocalLengthPointInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFocalLengthPointInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFocalLengthPointInfo_Hash() { return 3763764842U; }

static_assert(std::is_polymorphic<FDistortionPointInfo>() == std::is_polymorphic<FDataTablePointInfoBase>(), "USTRUCT FDistortionPointInfo cannot be polymorphic unless super FDataTablePointInfoBase is polymorphic");

class UScriptStruct* FDistortionPointInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionPointInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionPointInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionPointInfo"), sizeof(FDistortionPointInfo), Get_Z_Construct_UScriptStruct_FDistortionPointInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionPointInfo>()
{
	return FDistortionPointInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionPointInfo(FDistortionPointInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionPointInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionPointInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionPointInfo()
	{
		UScriptStruct::DeferCppStructOps<FDistortionPointInfo>(FName(TEXT("DistortionPointInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionPointInfo;
	struct Z_Construct_UScriptStruct_FDistortionPointInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Distortion Point Info struct\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Distortion Point Info struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionPointInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::NewProp_DistortionInfo_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** Lens distortion parameter */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Lens distortion parameter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::NewProp_DistortionInfo = { "DistortionInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionPointInfo, DistortionInfo), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::NewProp_DistortionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::NewProp_DistortionInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::NewProp_DistortionInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FDataTablePointInfoBase,
		&NewStructOps,
		"DistortionPointInfo",
		sizeof(FDistortionPointInfo),
		alignof(FDistortionPointInfo),
		Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionPointInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionPointInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionPointInfo"), sizeof(FDistortionPointInfo), Get_Z_Construct_UScriptStruct_FDistortionPointInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionPointInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionPointInfo_Hash() { return 4245232741U; }
class UScriptStruct* FDataTablePointInfoBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDataTablePointInfoBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataTablePointInfoBase, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DataTablePointInfoBase"), sizeof(FDataTablePointInfoBase), Get_Z_Construct_UScriptStruct_FDataTablePointInfoBase_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDataTablePointInfoBase>()
{
	return FDataTablePointInfoBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataTablePointInfoBase(FDataTablePointInfoBase::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DataTablePointInfoBase"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDataTablePointInfoBase
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDataTablePointInfoBase()
	{
		UScriptStruct::DeferCppStructOps<FDataTablePointInfoBase>(FName(TEXT("DataTablePointInfoBase")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDataTablePointInfoBase;
	struct Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Zoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Zoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base struct for point info wrapper which holds focus and zoom\n * Child classes should hold the point info itself\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Base struct for point info wrapper which holds focus and zoom\nChild classes should hold the point info itself" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataTablePointInfoBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Focus_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** Point Focus Value */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Point Focus Value" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataTablePointInfoBase, Focus), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Zoom_MetaData[] = {
		{ "Category", "Point" },
		{ "Comment", "/** Point Zoom Value */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Point Zoom Value" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Zoom = { "Zoom", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataTablePointInfoBase, Zoom), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Zoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Zoom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::NewProp_Zoom,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"DataTablePointInfoBase",
		sizeof(FDataTablePointInfoBase),
		alignof(FDataTablePointInfoBase),
		Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataTablePointInfoBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataTablePointInfoBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataTablePointInfoBase"), sizeof(FDataTablePointInfoBase), Get_Z_Construct_UScriptStruct_FDataTablePointInfoBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataTablePointInfoBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataTablePointInfoBase_Hash() { return 3197314952U; }
class UScriptStruct* FDistortionData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionData, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionData"), sizeof(FDistortionData), Get_Z_Construct_UScriptStruct_FDistortionData_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionData>()
{
	return FDistortionData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionData(FDistortionData::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionData"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionData
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionData()
	{
		UScriptStruct::DeferCppStructOps<FDistortionData>(FName(TEXT("DistortionData")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionData;
	struct Z_Construct_UScriptStruct_FDistortionData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortedUVs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortedUVs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DistortedUVs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanFactor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* Distortion data evaluated for given FZ pair based on lens parameters\n*/" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Distortion data evaluated for given FZ pair based on lens parameters" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionData>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs_Inner = { "DistortedUVs", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs_MetaData[] = {
		{ "Category", "Distortion" },
		{ "ModuleRelativePath", "Public/LensData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs = { "DistortedUVs", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionData, DistortedUVs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_OverscanFactor_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Estimated overscan factor based on distortion to have distorted cg covering full size */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Estimated overscan factor based on distortion to have distorted cg covering full size" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_OverscanFactor = { "OverscanFactor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionData, OverscanFactor), METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_OverscanFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_OverscanFactor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_DistortedUVs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionData_Statics::NewProp_OverscanFactor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"DistortionData",
		sizeof(FDistortionData),
		alignof(FDistortionData),
		Z_Construct_UScriptStruct_FDistortionData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionData"), sizeof(FDistortionData), Get_Z_Construct_UScriptStruct_FDistortionData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionData_Hash() { return 4057184778U; }
class UScriptStruct* FNodalPointOffset::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FNodalPointOffset_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNodalPointOffset, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("NodalPointOffset"), sizeof(FNodalPointOffset), Get_Z_Construct_UScriptStruct_FNodalPointOffset_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FNodalPointOffset>()
{
	return FNodalPointOffset::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNodalPointOffset(FNodalPointOffset::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("NodalPointOffset"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalPointOffset
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalPointOffset()
	{
		UScriptStruct::DeferCppStructOps<FNodalPointOffset>(FName(TEXT("NodalPointOffset")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFNodalPointOffset;
	struct Z_Construct_UScriptStruct_FNodalPointOffset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocationOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocationOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalPointOffset_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Lens nodal point offset\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Lens nodal point offset" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNodalPointOffset>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_LocationOffset_MetaData[] = {
		{ "Category", "Nodal point" },
		{ "ModuleRelativePath", "Public/LensData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_LocationOffset = { "LocationOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNodalPointOffset, LocationOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_LocationOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_LocationOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_RotationOffset_MetaData[] = {
		{ "Category", "Nodal point" },
		{ "ModuleRelativePath", "Public/LensData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_RotationOffset = { "RotationOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNodalPointOffset, RotationOffset), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_RotationOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_RotationOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNodalPointOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_LocationOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNodalPointOffset_Statics::NewProp_RotationOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNodalPointOffset_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"NodalPointOffset",
		sizeof(FNodalPointOffset),
		alignof(FNodalPointOffset),
		Z_Construct_UScriptStruct_FNodalPointOffset_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNodalPointOffset_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNodalPointOffset()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNodalPointOffset_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NodalPointOffset"), sizeof(FNodalPointOffset), Get_Z_Construct_UScriptStruct_FNodalPointOffset_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNodalPointOffset_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNodalPointOffset_Hash() { return 4189013757U; }
class UScriptStruct* FImageCenterInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FImageCenterInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FImageCenterInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("ImageCenterInfo"), sizeof(FImageCenterInfo), Get_Z_Construct_UScriptStruct_FImageCenterInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FImageCenterInfo>()
{
	return FImageCenterInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FImageCenterInfo(FImageCenterInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("ImageCenterInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterInfo()
	{
		UScriptStruct::DeferCppStructOps<FImageCenterInfo>(FName(TEXT("ImageCenterInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterInfo;
	struct Z_Construct_UScriptStruct_FImageCenterInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrincipalPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrincipalPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Lens camera image center parameters\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Lens camera image center parameters" },
	};
#endif
	void* Z_Construct_UScriptStruct_FImageCenterInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FImageCenterInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterInfo_Statics::NewProp_PrincipalPoint_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Value expected to be normalized [0,1] */" },
		{ "DisplayName", "Image Center" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Value expected to be normalized [0,1]" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FImageCenterInfo_Statics::NewProp_PrincipalPoint = { "PrincipalPoint", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FImageCenterInfo, PrincipalPoint), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterInfo_Statics::NewProp_PrincipalPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterInfo_Statics::NewProp_PrincipalPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FImageCenterInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterInfo_Statics::NewProp_PrincipalPoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FImageCenterInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"ImageCenterInfo",
		sizeof(FImageCenterInfo),
		alignof(FImageCenterInfo),
		Z_Construct_UScriptStruct_FImageCenterInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FImageCenterInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FImageCenterInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ImageCenterInfo"), sizeof(FImageCenterInfo), Get_Z_Construct_UScriptStruct_FImageCenterInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FImageCenterInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FImageCenterInfo_Hash() { return 2540370544U; }
class UScriptStruct* FSTMapInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FSTMapInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSTMapInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("STMapInfo"), sizeof(FSTMapInfo), Get_Z_Construct_UScriptStruct_FSTMapInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FSTMapInfo>()
{
	return FSTMapInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSTMapInfo(FSTMapInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("STMapInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapInfo()
	{
		UScriptStruct::DeferCppStructOps<FSTMapInfo>(FName(TEXT("STMapInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSTMapInfo;
	struct Z_Construct_UScriptStruct_FSTMapInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DistortionMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Pre generate STMap and normalized focal length information\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Pre generate STMap and normalized focal length information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSTMapInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSTMapInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSTMapInfo_Statics::NewProp_DistortionMap_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** \n\x09 * Pre calibrated UVMap/STMap\n\x09 * RG channels are expected to have undistortion map (from distorted to undistorted)\n\x09 * BA channels are expected to have distortion map (from undistorted (CG) to distorted)\n\x09 */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Pre calibrated UVMap/STMap\nRG channels are expected to have undistortion map (from distorted to undistorted)\nBA channels are expected to have distortion map (from undistorted (CG) to distorted)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSTMapInfo_Statics::NewProp_DistortionMap = { "DistortionMap", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSTMapInfo, DistortionMap), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapInfo_Statics::NewProp_DistortionMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapInfo_Statics::NewProp_DistortionMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSTMapInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSTMapInfo_Statics::NewProp_DistortionMap,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSTMapInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"STMapInfo",
		sizeof(FSTMapInfo),
		alignof(FSTMapInfo),
		Z_Construct_UScriptStruct_FSTMapInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSTMapInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSTMapInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSTMapInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSTMapInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("STMapInfo"), sizeof(FSTMapInfo), Get_Z_Construct_UScriptStruct_FSTMapInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSTMapInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSTMapInfo_Hash() { return 2450613988U; }
class UScriptStruct* FFocalLengthInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FFocalLengthInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFocalLengthInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("FocalLengthInfo"), sizeof(FFocalLengthInfo), Get_Z_Construct_UScriptStruct_FFocalLengthInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FFocalLengthInfo>()
{
	return FFocalLengthInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFocalLengthInfo(FFocalLengthInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("FocalLengthInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthInfo()
	{
		UScriptStruct::DeferCppStructOps<FFocalLengthInfo>(FName(TEXT("FocalLengthInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFFocalLengthInfo;
	struct Z_Construct_UScriptStruct_FFocalLengthInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FxFy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FxFy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Normalized focal length information for both width and height dimension\n * If focal length is in pixel, normalize using pixel dimensions\n * If focal length is in mm, normalize using sensor dimensions\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Normalized focal length information for both width and height dimension\nIf focal length is in pixel, normalize using pixel dimensions\nIf focal length is in mm, normalize using sensor dimensions" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFocalLengthInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::NewProp_FxFy_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Value expected to be normalized (unitless) */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Value expected to be normalized (unitless)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::NewProp_FxFy = { "FxFy", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFocalLengthInfo, FxFy), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::NewProp_FxFy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::NewProp_FxFy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::NewProp_FxFy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"FocalLengthInfo",
		sizeof(FFocalLengthInfo),
		alignof(FFocalLengthInfo),
		Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFocalLengthInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFocalLengthInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FocalLengthInfo"), sizeof(FFocalLengthInfo), Get_Z_Construct_UScriptStruct_FFocalLengthInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFocalLengthInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFocalLengthInfo_Hash() { return 4254461307U; }
class UScriptStruct* FDistortionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FDistortionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("DistortionInfo"), sizeof(FDistortionInfo), Get_Z_Construct_UScriptStruct_FDistortionInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FDistortionInfo>()
{
	return FDistortionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionInfo(FDistortionInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("DistortionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionInfo()
	{
		UScriptStruct::DeferCppStructOps<FDistortionInfo>(FName(TEXT("DistortionInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFDistortionInfo;
	struct Z_Construct_UScriptStruct_FDistortionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Parameters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Lens distortion parameters\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Lens distortion parameters" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionInfo>();
	}
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters_Inner = { "Parameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Generic array of floating-point lens distortion parameters */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Generic array of floating-point lens distortion parameters" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionInfo, Parameters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionInfo_Statics::NewProp_Parameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"DistortionInfo",
		sizeof(FDistortionInfo),
		alignof(FDistortionInfo),
		Z_Construct_UScriptStruct_FDistortionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionInfo"), sizeof(FDistortionInfo), Get_Z_Construct_UScriptStruct_FDistortionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionInfo_Hash() { return 3375125066U; }
class UScriptStruct* FLensInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FLensInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLensInfo, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("LensInfo"), sizeof(FLensInfo), Get_Z_Construct_UScriptStruct_FLensInfo_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FLensInfo>()
{
	return FLensInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLensInfo(FLensInfo::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("LensInfo"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensInfo
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensInfo()
	{
		UScriptStruct::DeferCppStructOps<FLensInfo>(FName(TEXT("LensInfo")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFLensInfo;
	struct Z_Construct_UScriptStruct_FLensInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensModelName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LensModelName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensSerialNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LensSerialNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensModel_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_LensModel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SensorDimensions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SensorDimensions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Information about the lens rig\n */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Information about the lens rig" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLensInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLensInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModelName_MetaData[] = {
		{ "Category", "Lens Info" },
		{ "Comment", "/** Model name of the lens */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Model name of the lens" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModelName = { "LensModelName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensInfo, LensModelName), METADATA_PARAMS(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModelName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModelName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensSerialNumber_MetaData[] = {
		{ "Category", "Lens Info" },
		{ "Comment", "/** Serial number of the lens */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Serial number of the lens" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensSerialNumber = { "LensSerialNumber", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensInfo, LensSerialNumber), METADATA_PARAMS(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensSerialNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensSerialNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModel_MetaData[] = {
		{ "Category", "Lens Info" },
		{ "Comment", "/** Model of the lens (spherical, anamorphic, etc...) */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Model of the lens (spherical, anamorphic, etc...)" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModel = { "LensModel", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensInfo, LensModel), Z_Construct_UClass_ULensModel_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_SensorDimensions_MetaData[] = {
		{ "Category", "Lens Info" },
		{ "Comment", "/** Width and height of the calibrated camera's sensor, in millimeters */" },
		{ "ModuleRelativePath", "Public/LensData.h" },
		{ "ToolTip", "Width and height of the calibrated camera's sensor, in millimeters" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_SensorDimensions = { "SensorDimensions", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLensInfo, SensorDimensions), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_SensorDimensions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_SensorDimensions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLensInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModelName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensSerialNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_LensModel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLensInfo_Statics::NewProp_SensorDimensions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLensInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"LensInfo",
		sizeof(FLensInfo),
		alignof(FLensInfo),
		Z_Construct_UScriptStruct_FLensInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLensInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLensInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLensInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLensInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LensInfo"), sizeof(FLensInfo), Get_Z_Construct_UScriptStruct_FLensInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLensInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLensInfo_Hash() { return 2310578429U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
