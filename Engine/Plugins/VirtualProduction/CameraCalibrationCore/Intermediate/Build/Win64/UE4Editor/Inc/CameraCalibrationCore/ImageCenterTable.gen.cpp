// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Tables/ImageCenterTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImageCenterTable() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterTable();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseLensTable();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FImageCenterFocusPoint();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FBaseFocusPoint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
// End Cross Module References

static_assert(std::is_polymorphic<FImageCenterTable>() == std::is_polymorphic<FBaseLensTable>(), "USTRUCT FImageCenterTable cannot be polymorphic unless super FBaseLensTable is polymorphic");

class UScriptStruct* FImageCenterTable::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FImageCenterTable_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FImageCenterTable, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("ImageCenterTable"), sizeof(FImageCenterTable), Get_Z_Construct_UScriptStruct_FImageCenterTable_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FImageCenterTable>()
{
	return FImageCenterTable::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FImageCenterTable(FImageCenterTable::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("ImageCenterTable"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterTable
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterTable()
	{
		UScriptStruct::DeferCppStructOps<FImageCenterTable>(FName(TEXT("ImageCenterTable")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterTable;
	struct Z_Construct_UScriptStruct_FImageCenterTable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocusPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterTable_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Image Center table associating CxCy values to focus and zoom\n */" },
		{ "ModuleRelativePath", "Public/Tables/ImageCenterTable.h" },
		{ "ToolTip", "Image Center table associating CxCy values to focus and zoom" },
	};
#endif
	void* Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FImageCenterTable>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints_Inner = { "FocusPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FImageCenterFocusPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints_MetaData[] = {
		{ "Comment", "/** Lists of focus points */" },
		{ "ModuleRelativePath", "Public/Tables/ImageCenterTable.h" },
		{ "ToolTip", "Lists of focus points" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints = { "FocusPoints", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FImageCenterTable, FocusPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FImageCenterTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterTable_Statics::NewProp_FocusPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FImageCenterTable_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseLensTable,
		&NewStructOps,
		"ImageCenterTable",
		sizeof(FImageCenterTable),
		alignof(FImageCenterTable),
		Z_Construct_UScriptStruct_FImageCenterTable_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterTable_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterTable_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterTable_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FImageCenterTable()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FImageCenterTable_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ImageCenterTable"), sizeof(FImageCenterTable), Get_Z_Construct_UScriptStruct_FImageCenterTable_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FImageCenterTable_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FImageCenterTable_Hash() { return 2709565985U; }

static_assert(std::is_polymorphic<FImageCenterFocusPoint>() == std::is_polymorphic<FBaseFocusPoint>(), "USTRUCT FImageCenterFocusPoint cannot be polymorphic unless super FBaseFocusPoint is polymorphic");

class UScriptStruct* FImageCenterFocusPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FImageCenterFocusPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FImageCenterFocusPoint, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("ImageCenterFocusPoint"), sizeof(FImageCenterFocusPoint), Get_Z_Construct_UScriptStruct_FImageCenterFocusPoint_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FImageCenterFocusPoint>()
{
	return FImageCenterFocusPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FImageCenterFocusPoint(FImageCenterFocusPoint::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("ImageCenterFocusPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterFocusPoint
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterFocusPoint()
	{
		UScriptStruct::DeferCppStructOps<FImageCenterFocusPoint>(FName(TEXT("ImageCenterFocusPoint")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFImageCenterFocusPoint;
	struct Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Focus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cx_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cx;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * ImageCenter focus point containing curves for CxCy \n */" },
		{ "ModuleRelativePath", "Public/Tables/ImageCenterTable.h" },
		{ "ToolTip", "ImageCenter focus point containing curves for CxCy" },
	};
#endif
	void* Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FImageCenterFocusPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Focus_MetaData[] = {
		{ "Comment", "/** Focus value of this point */" },
		{ "ModuleRelativePath", "Public/Tables/ImageCenterTable.h" },
		{ "ToolTip", "Focus value of this point" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FImageCenterFocusPoint, Focus), METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Focus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Focus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cx_MetaData[] = {
		{ "Comment", "/** Curves representing normalized Cx over zoom */" },
		{ "ModuleRelativePath", "Public/Tables/ImageCenterTable.h" },
		{ "ToolTip", "Curves representing normalized Cx over zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cx = { "Cx", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FImageCenterFocusPoint, Cx), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cx_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cx_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cy_MetaData[] = {
		{ "Comment", "/** Curves representing normalized Cy over zoom */" },
		{ "ModuleRelativePath", "Public/Tables/ImageCenterTable.h" },
		{ "ToolTip", "Curves representing normalized Cy over zoom" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cy = { "Cy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FImageCenterFocusPoint, Cy), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cx,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::NewProp_Cy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		Z_Construct_UScriptStruct_FBaseFocusPoint,
		&NewStructOps,
		"ImageCenterFocusPoint",
		sizeof(FImageCenterFocusPoint),
		alignof(FImageCenterFocusPoint),
		Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FImageCenterFocusPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FImageCenterFocusPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ImageCenterFocusPoint"), sizeof(FImageCenterFocusPoint), Get_Z_Construct_UScriptStruct_FImageCenterFocusPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FImageCenterFocusPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FImageCenterFocusPoint_Hash() { return 3526542607U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
