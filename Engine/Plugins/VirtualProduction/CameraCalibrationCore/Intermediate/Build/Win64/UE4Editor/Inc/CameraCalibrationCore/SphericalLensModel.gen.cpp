// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationCore/Public/Models/SphericalLensModel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSphericalLensModel() {}
// Cross Module References
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FSphericalDistortionParameters();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationCore();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_USphericalLensModel_NoRegister();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_USphericalLensModel();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensModel();
// End Cross Module References
class UScriptStruct* FSphericalDistortionParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONCORE_API uint32 Get_Z_Construct_UScriptStruct_FSphericalDistortionParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSphericalDistortionParameters, Z_Construct_UPackage__Script_CameraCalibrationCore(), TEXT("SphericalDistortionParameters"), sizeof(FSphericalDistortionParameters), Get_Z_Construct_UScriptStruct_FSphericalDistortionParameters_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONCORE_API UScriptStruct* StaticStruct<FSphericalDistortionParameters>()
{
	return FSphericalDistortionParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSphericalDistortionParameters(FSphericalDistortionParameters::StaticStruct, TEXT("/Script/CameraCalibrationCore"), TEXT("SphericalDistortionParameters"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSphericalDistortionParameters
{
	FScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSphericalDistortionParameters()
	{
		UScriptStruct::DeferCppStructOps<FSphericalDistortionParameters>(FName(TEXT("SphericalDistortionParameters")));
	}
} ScriptStruct_CameraCalibrationCore_StaticRegisterNativesFSphericalDistortionParameters;
	struct Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K1_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K2_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K3_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_P1_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_P1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_P2_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_P2;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Spherical lens distortion parameters\n * All parameters are unitless and represent the coefficients used to undistort a distorted image\n * Refer to the OpenCV camera calibration documentation for the intended units/usage of these parameters:\n * https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html\n */" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "Spherical lens distortion parameters\nAll parameters are unitless and represent the coefficients used to undistort a distorted image\nRefer to the OpenCV camera calibration documentation for the intended units/usage of these parameters:\nhttps://docs.opencv.org/3.4/d9/d0c/group__calib3d.html" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSphericalDistortionParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K1_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Radial coefficient of the r^2 term */" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "Radial coefficient of the r^2 term" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K1 = { "K1", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalDistortionParameters, K1), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K2_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Radial coefficient of the r^4 term */" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "Radial coefficient of the r^4 term" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K2 = { "K2", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalDistortionParameters, K2), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K3_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Radial coefficient of the r^6 term */" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "Radial coefficient of the r^6 term" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K3 = { "K3", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalDistortionParameters, K3), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K3_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P1_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** First tangential coefficient */" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "First tangential coefficient" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P1 = { "P1", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalDistortionParameters, P1), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P2_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Second tangential coefficient */" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "Second tangential coefficient" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P2 = { "P2", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalDistortionParameters, P2), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P2_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_K3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::NewProp_P2,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
		nullptr,
		&NewStructOps,
		"SphericalDistortionParameters",
		sizeof(FSphericalDistortionParameters),
		alignof(FSphericalDistortionParameters),
		Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSphericalDistortionParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSphericalDistortionParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SphericalDistortionParameters"), sizeof(FSphericalDistortionParameters), Get_Z_Construct_UScriptStruct_FSphericalDistortionParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSphericalDistortionParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSphericalDistortionParameters_Hash() { return 3844781452U; }
	void USphericalLensModel::StaticRegisterNativesUSphericalLensModel()
	{
	}
	UClass* Z_Construct_UClass_USphericalLensModel_NoRegister()
	{
		return USphericalLensModel::StaticClass();
	}
	struct Z_Construct_UClass_USphericalLensModel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USphericalLensModel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULensModel,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USphericalLensModel_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Spherical lens model, using spherical lens distortion parameters\n */" },
		{ "DisplayName", "Spherical Lens Model" },
		{ "IncludePath", "Models/SphericalLensModel.h" },
		{ "ModuleRelativePath", "Public/Models/SphericalLensModel.h" },
		{ "ToolTip", "Spherical lens model, using spherical lens distortion parameters" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USphericalLensModel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USphericalLensModel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USphericalLensModel_Statics::ClassParams = {
		&USphericalLensModel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USphericalLensModel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USphericalLensModel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USphericalLensModel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USphericalLensModel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USphericalLensModel, 768308452);
	template<> CAMERACALIBRATIONCORE_API UClass* StaticClass<USphericalLensModel>()
	{
		return USphericalLensModel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USphericalLensModel(Z_Construct_UClass_USphericalLensModel, &USphericalLensModel::StaticClass, TEXT("/Script/CameraCalibrationCore"), TEXT("USphericalLensModel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USphericalLensModel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
