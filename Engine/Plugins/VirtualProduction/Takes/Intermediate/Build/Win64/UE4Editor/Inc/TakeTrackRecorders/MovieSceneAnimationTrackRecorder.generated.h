// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKETRACKRECORDERS_MovieSceneAnimationTrackRecorder_generated_h
#error "MovieSceneAnimationTrackRecorder.generated.h already included, missing '#pragma once' in MovieSceneAnimationTrackRecorder.h"
#endif
#define TAKETRACKRECORDERS_MovieSceneAnimationTrackRecorder_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneAnimationTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneAnimationTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneAnimationTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TakeTrackRecorders"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneAnimationTrackRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneAnimationTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneAnimationTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneAnimationTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TakeTrackRecorders"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneAnimationTrackRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneAnimationTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneAnimationTrackRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneAnimationTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneAnimationTrackRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneAnimationTrackRecorder(UMovieSceneAnimationTrackRecorder&&); \
	NO_API UMovieSceneAnimationTrackRecorder(const UMovieSceneAnimationTrackRecorder&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneAnimationTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneAnimationTrackRecorder(UMovieSceneAnimationTrackRecorder&&); \
	NO_API UMovieSceneAnimationTrackRecorder(const UMovieSceneAnimationTrackRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneAnimationTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneAnimationTrackRecorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneAnimationTrackRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_40_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKETRACKRECORDERS_API UClass* StaticClass<class UMovieSceneAnimationTrackRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneAnimationTrackRecorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
