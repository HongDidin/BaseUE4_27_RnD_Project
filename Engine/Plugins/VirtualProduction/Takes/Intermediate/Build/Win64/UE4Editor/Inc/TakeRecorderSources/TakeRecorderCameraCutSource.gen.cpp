// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderCameraCutSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderCameraCutSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderCameraCutSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderCameraCutSource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
// End Cross Module References
	void UTakeRecorderCameraCutSource::StaticRegisterNativesUTakeRecorderCameraCutSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderCameraCutSource_NoRegister()
	{
		return UTakeRecorderCameraCutSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_World_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterLevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MasterLevelSequence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Other" },
		{ "Comment", "/** A recording source that detects camera switching and creates a camera cut track */" },
		{ "IncludePath", "TakeRecorderCameraCutSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderCameraCutSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Camera Cuts" },
		{ "ToolTip", "A recording source that detects camera switching and creates a camera cut track" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_World_MetaData[] = {
		{ "ModuleRelativePath", "Private/TakeRecorderCameraCutSource.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderCameraCutSource, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_World_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_World_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_MasterLevelSequence_MetaData[] = {
		{ "Comment", "/** The master or uppermost level sequence that this source is being recorded into. Set during PreRecording, null after PostRecording. */" },
		{ "ModuleRelativePath", "Private/TakeRecorderCameraCutSource.h" },
		{ "ToolTip", "The master or uppermost level sequence that this source is being recorded into. Set during PreRecording, null after PostRecording." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_MasterLevelSequence = { "MasterLevelSequence", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderCameraCutSource, MasterLevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_MasterLevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_MasterLevelSequence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::NewProp_MasterLevelSequence,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderCameraCutSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::ClassParams = {
		&UTakeRecorderCameraCutSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderCameraCutSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderCameraCutSource, 3400377777);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderCameraCutSource>()
	{
		return UTakeRecorderCameraCutSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderCameraCutSource(Z_Construct_UClass_UTakeRecorderCameraCutSource, &UTakeRecorderCameraCutSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderCameraCutSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderCameraCutSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
