// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKERECORDERSOURCES_TakeRecorderLevelSequenceSource_generated_h
#error "TakeRecorderLevelSequenceSource.generated.h already included, missing '#pragma once' in TakeRecorderLevelSequenceSource.h"
#endif
#define TAKERECORDERSOURCES_TakeRecorderLevelSequenceSource_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderLevelSequenceSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderLevelSequenceSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderLevelSequenceSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderLevelSequenceSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderLevelSequenceSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderLevelSequenceSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderLevelSequenceSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderLevelSequenceSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderLevelSequenceSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderLevelSequenceSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderLevelSequenceSource(UTakeRecorderLevelSequenceSource&&); \
	NO_API UTakeRecorderLevelSequenceSource(const UTakeRecorderLevelSequenceSource&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderLevelSequenceSource(UTakeRecorderLevelSequenceSource&&); \
	NO_API UTakeRecorderLevelSequenceSource(const UTakeRecorderLevelSequenceSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderLevelSequenceSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderLevelSequenceSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderLevelSequenceSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDERSOURCES_API UClass* StaticClass<class UTakeRecorderLevelSequenceSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderLevelSequenceSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
