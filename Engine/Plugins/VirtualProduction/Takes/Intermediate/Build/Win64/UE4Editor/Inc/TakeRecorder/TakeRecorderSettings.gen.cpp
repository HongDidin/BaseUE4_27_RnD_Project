// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorder/Public/TakeRecorderSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderSettings() {}
// Cross Module References
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderUserSettings_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderUserSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_TakeRecorder();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderUserParameters();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakePreset_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderProjectSettings_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderProjectSettings();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderProjectParameters();
// End Cross Module References
	void UTakeRecorderUserSettings::StaticRegisterNativesUTakeRecorderUserSettings()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderUserSettings_NoRegister()
	{
		return UTakeRecorderUserSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderUserSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetSaveDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PresetSaveDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastOpenedPreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_LastOpenedPreset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSequenceOpen_MetaData[];
#endif
		static void NewProp_bIsSequenceOpen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSequenceOpen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowUserSettingsOnUI_MetaData[];
#endif
		static void NewProp_bShowUserSettingsOnUI_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowUserSettingsOnUI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderUserSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderUserSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Universal take recorder settings that apply to a whole take\n */" },
		{ "IncludePath", "TakeRecorderSettings.h" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ToolTip", "Universal take recorder settings that apply to a whole take" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** User settings that should be passed to a recorder instance */" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "User settings that should be passed to a recorder instance" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000004015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderUserSettings, Settings), Z_Construct_UScriptStruct_FTakeRecorderUserParameters, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_PresetSaveDir_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** The default location in which to save take presets */" },
		{ "DisplayName", "Preset Save Location" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ToolTip", "The default location in which to save take presets" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_PresetSaveDir = { "PresetSaveDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderUserSettings, PresetSaveDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_PresetSaveDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_PresetSaveDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_LastOpenedPreset_MetaData[] = {
		{ "Comment", "/** Soft reference to the preset last opened on the take recording UI */" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ToolTip", "Soft reference to the preset last opened on the take recording UI" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_LastOpenedPreset = { "LastOpenedPreset", nullptr, (EPropertyFlags)0x0014000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderUserSettings, LastOpenedPreset), Z_Construct_UClass_UTakePreset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_LastOpenedPreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_LastOpenedPreset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen_MetaData[] = {
		{ "Comment", "/** Whether the sequence editor is open for the take recorder */" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ToolTip", "Whether the sequence editor is open for the take recorder" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen_SetBit(void* Obj)
	{
		((UTakeRecorderUserSettings*)Obj)->bIsSequenceOpen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen = { "bIsSequenceOpen", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderUserSettings), &Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI_MetaData[] = {
		{ "Comment", "/** Whether the sequence editor is open for the take recorder */" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ToolTip", "Whether the sequence editor is open for the take recorder" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI_SetBit(void* Obj)
	{
		((UTakeRecorderUserSettings*)Obj)->bShowUserSettingsOnUI = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI = { "bShowUserSettingsOnUI", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderUserSettings), &Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderUserSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_PresetSaveDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_LastOpenedPreset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bIsSequenceOpen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderUserSettings_Statics::NewProp_bShowUserSettingsOnUI,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderUserSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderUserSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderUserSettings_Statics::ClassParams = {
		&UTakeRecorderUserSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderUserSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderUserSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderUserSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderUserSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderUserSettings, 3255072271);
	template<> TAKERECORDER_API UClass* StaticClass<UTakeRecorderUserSettings>()
	{
		return UTakeRecorderUserSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderUserSettings(Z_Construct_UClass_UTakeRecorderUserSettings, &UTakeRecorderUserSettings::StaticClass, TEXT("/Script/TakeRecorder"), TEXT("UTakeRecorderUserSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderUserSettings);
	void UTakeRecorderProjectSettings::StaticRegisterNativesUTakeRecorderProjectSettings()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderProjectSettings_NoRegister()
	{
		return UTakeRecorderProjectSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderProjectSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Universal take recorder settings that apply to a whole take\n */" },
		{ "IncludePath", "TakeRecorderSettings.h" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ToolTip", "Universal take recorder settings that apply to a whole take" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "ModuleRelativePath", "Public/TakeRecorderSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000004015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderProjectSettings, Settings), Z_Construct_UScriptStruct_FTakeRecorderProjectParameters, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderProjectSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::ClassParams = {
		&UTakeRecorderProjectSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderProjectSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderProjectSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderProjectSettings, 3789582891);
	template<> TAKERECORDER_API UClass* StaticClass<UTakeRecorderProjectSettings>()
	{
		return UTakeRecorderProjectSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderProjectSettings(Z_Construct_UClass_UTakeRecorderProjectSettings, &UTakeRecorderProjectSettings::StaticClass, TEXT("/Script/TakeRecorder"), TEXT("UTakeRecorderProjectSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderProjectSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
