// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakesCore/Public/TakeRecorderSourceProperty.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderSourceProperty() {}
// Cross Module References
	TAKESCORE_API UScriptStruct* Z_Construct_UScriptStruct_FActorRecordedProperty();
	UPackage* Z_Construct_UPackage__Script_TakesCore();
	TAKESCORE_API UClass* Z_Construct_UClass_UActorRecorderPropertyMap_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UActorRecorderPropertyMap();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
class UScriptStruct* FActorRecordedProperty::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TAKESCORE_API uint32 Get_Z_Construct_UScriptStruct_FActorRecordedProperty_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FActorRecordedProperty, Z_Construct_UPackage__Script_TakesCore(), TEXT("ActorRecordedProperty"), sizeof(FActorRecordedProperty), Get_Z_Construct_UScriptStruct_FActorRecordedProperty_Hash());
	}
	return Singleton;
}
template<> TAKESCORE_API UScriptStruct* StaticStruct<FActorRecordedProperty>()
{
	return FActorRecordedProperty::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FActorRecordedProperty(FActorRecordedProperty::StaticStruct, TEXT("/Script/TakesCore"), TEXT("ActorRecordedProperty"), false, nullptr, nullptr);
static struct FScriptStruct_TakesCore_StaticRegisterNativesFActorRecordedProperty
{
	FScriptStruct_TakesCore_StaticRegisterNativesFActorRecordedProperty()
	{
		UScriptStruct::DeferCppStructOps<FActorRecordedProperty>(FName(TEXT("ActorRecordedProperty")));
	}
} ScriptStruct_TakesCore_StaticRegisterNativesFActorRecordedProperty;
	struct Z_Construct_UScriptStruct_FActorRecordedProperty_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PropertyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecorderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_RecorderName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FActorRecordedProperty>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_PropertyName_MetaData[] = {
		{ "Category", "Property" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_PropertyName = { "PropertyName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorRecordedProperty, PropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_PropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_PropertyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Property" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FActorRecordedProperty*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FActorRecordedProperty), &Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_RecorderName_MetaData[] = {
		{ "Category", "Property" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_RecorderName = { "RecorderName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorRecordedProperty, RecorderName), METADATA_PARAMS(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_RecorderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_RecorderName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_PropertyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::NewProp_RecorderName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TakesCore,
		nullptr,
		&NewStructOps,
		"ActorRecordedProperty",
		sizeof(FActorRecordedProperty),
		alignof(FActorRecordedProperty),
		Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FActorRecordedProperty()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FActorRecordedProperty_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TakesCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ActorRecordedProperty"), sizeof(FActorRecordedProperty), Get_Z_Construct_UScriptStruct_FActorRecordedProperty_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FActorRecordedProperty_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FActorRecordedProperty_Hash() { return 3783637848U; }
	void UActorRecorderPropertyMap::StaticRegisterNativesUActorRecorderPropertyMap()
	{
	}
	UClass* Z_Construct_UClass_UActorRecorderPropertyMap_NoRegister()
	{
		return UActorRecorderPropertyMap::StaticClass();
	}
	struct Z_Construct_UClass_UActorRecorderPropertyMap_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecordedObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_RecordedObject;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Properties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Properties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Children;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorRecorderPropertyMap_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakesCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorRecorderPropertyMap_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* This represents a list of all possible properties and components on an actor\n* which can be recorded by the Actor Recorder and whether or not the user wishes\n* to record them. If you wish to expose a property to be recorded it needs to be marked\n* as \"Interp\" (C++) or \"Expose to Cinematics\" in Blueprints.\n*/" },
		{ "IncludePath", "TakeRecorderSourceProperty.h" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
		{ "ToolTip", "This represents a list of all possible properties and components on an actor\nwhich can be recorded by the Actor Recorder and whether or not the user wishes\nto record them. If you wish to expose a property to be recorded it needs to be marked\nas \"Interp\" (C++) or \"Expose to Cinematics\" in Blueprints." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_RecordedObject_MetaData[] = {
		{ "Category", "Property" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_RecordedObject = { "RecordedObject", nullptr, (EPropertyFlags)0x0014000000020001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorRecorderPropertyMap, RecordedObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_RecordedObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_RecordedObject_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties_Inner = { "Properties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FActorRecordedProperty, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties_MetaData[] = {
		{ "Category", "Property" },
		{ "Comment", "/* Represents properties exposed to Cinematics that can possibly be recorded. */" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
		{ "ToolTip", "Represents properties exposed to Cinematics that can possibly be recorded." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorRecorderPropertyMap, Properties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_Inner_MetaData[] = {
		{ "Category", "Property" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_Inner = { "Children", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UActorRecorderPropertyMap_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_MetaData[] = {
		{ "Category", "Property" },
		{ "EditFixedOrder", "" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TakeRecorderSourceProperty.h" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x001000800000000d, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorRecorderPropertyMap, Children), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActorRecorderPropertyMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_RecordedObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Properties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorRecorderPropertyMap_Statics::NewProp_Children,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorRecorderPropertyMap_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorRecorderPropertyMap>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorRecorderPropertyMap_Statics::ClassParams = {
		&UActorRecorderPropertyMap::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UActorRecorderPropertyMap_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorRecorderPropertyMap_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorRecorderPropertyMap()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorRecorderPropertyMap_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorRecorderPropertyMap, 3173985847);
	template<> TAKESCORE_API UClass* StaticClass<UActorRecorderPropertyMap>()
	{
		return UActorRecorderPropertyMap::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorRecorderPropertyMap(Z_Construct_UClass_UActorRecorderPropertyMap, &UActorRecorderPropertyMap::StaticClass, TEXT("/Script/TakesCore"), TEXT("UActorRecorderPropertyMap"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorRecorderPropertyMap);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
