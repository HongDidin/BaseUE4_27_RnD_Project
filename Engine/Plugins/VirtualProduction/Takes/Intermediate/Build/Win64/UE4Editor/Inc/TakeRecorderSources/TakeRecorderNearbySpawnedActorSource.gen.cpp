// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderNearbySpawnedActorSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderNearbySpawnedActorSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void UTakeRecorderNearbySpawnedActorSource::StaticRegisterNativesUTakeRecorderNearbySpawnedActorSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_NoRegister()
	{
		return UTakeRecorderNearbySpawnedActorSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Proximity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Proximity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFilterSpawnedActors_MetaData[];
#endif
		static void NewProp_bFilterSpawnedActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFilterSpawnedActors;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FilterTypes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterTypes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FilterTypes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Actors" },
		{ "Comment", "/** A recording source that detects actors spawned close to the current camera, and captures them as spawnables */" },
		{ "IncludePath", "TakeRecorderNearbySpawnedActorSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderNearbySpawnedActorSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Nearby Spawned Actors" },
		{ "ToolTip", "A recording source that detects actors spawned close to the current camera, and captures them as spawnables" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_Proximity_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** The proximity to the current camera that an actor must be spawned in order to be recorded as a spawnable. If 0, proximity is disregarded. */" },
		{ "DisplayName", "Spawn Proximity" },
		{ "ModuleRelativePath", "Private/TakeRecorderNearbySpawnedActorSource.h" },
		{ "ToolTip", "The proximity to the current camera that an actor must be spawned in order to be recorded as a spawnable. If 0, proximity is disregarded." },
		{ "units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_Proximity = { "Proximity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderNearbySpawnedActorSource, Proximity), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_Proximity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_Proximity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Should we only record actors that pass the filter list?*/" },
		{ "ModuleRelativePath", "Private/TakeRecorderNearbySpawnedActorSource.h" },
		{ "ToolTip", "Should we only record actors that pass the filter list?" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors_SetBit(void* Obj)
	{
		((UTakeRecorderNearbySpawnedActorSource*)Obj)->bFilterSpawnedActors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors = { "bFilterSpawnedActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderNearbySpawnedActorSource), &Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes_Inner = { "FilterTypes", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** A type filter to apply to spawned objects */" },
		{ "EditCondition", "bFilterSpawnedActors" },
		{ "ModuleRelativePath", "Private/TakeRecorderNearbySpawnedActorSource.h" },
		{ "ToolTip", "A type filter to apply to spawned objects" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes = { "FilterTypes", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderNearbySpawnedActorSource, FilterTypes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_Proximity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_bFilterSpawnedActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::NewProp_FilterTypes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderNearbySpawnedActorSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::ClassParams = {
		&UTakeRecorderNearbySpawnedActorSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderNearbySpawnedActorSource, 1025476089);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderNearbySpawnedActorSource>()
	{
		return UTakeRecorderNearbySpawnedActorSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderNearbySpawnedActorSource(Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource, &UTakeRecorderNearbySpawnedActorSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderNearbySpawnedActorSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderNearbySpawnedActorSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
