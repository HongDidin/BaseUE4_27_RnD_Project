// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderLevelVisibilitySource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderLevelVisibilitySource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySource();
// End Cross Module References
	void UTakeRecorderLevelVisibilitySourceSettings::StaticRegisterNativesUTakeRecorderLevelVisibilitySourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_NoRegister()
	{
		return UTakeRecorderLevelVisibilitySourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelVisibilityTrackName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_LevelVisibilityTrackName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A recording source that records level visibilitiy */" },
		{ "DisplayName", "Level Visibility Recorder Defaults" },
		{ "IncludePath", "TakeRecorderLevelVisibilitySource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderLevelVisibilitySource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A recording source that records level visibilitiy" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::NewProp_LevelVisibilityTrackName_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Name of the recorded level visibility track name */" },
		{ "ModuleRelativePath", "Private/TakeRecorderLevelVisibilitySource.h" },
		{ "ToolTip", "Name of the recorded level visibility track name" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::NewProp_LevelVisibilityTrackName = { "LevelVisibilityTrackName", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderLevelVisibilitySourceSettings, LevelVisibilityTrackName), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::NewProp_LevelVisibilityTrackName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::NewProp_LevelVisibilityTrackName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::NewProp_LevelVisibilityTrackName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderLevelVisibilitySourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::ClassParams = {
		&UTakeRecorderLevelVisibilitySourceSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::PropPointers),
		0,
		0x000000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderLevelVisibilitySourceSettings, 3156840641);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderLevelVisibilitySourceSettings>()
	{
		return UTakeRecorderLevelVisibilitySourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderLevelVisibilitySourceSettings(Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings, &UTakeRecorderLevelVisibilitySourceSettings::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderLevelVisibilitySourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderLevelVisibilitySourceSettings);
	void UTakeRecorderLevelVisibilitySource::StaticRegisterNativesUTakeRecorderLevelVisibilitySource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_NoRegister()
	{
		return UTakeRecorderLevelVisibilitySource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderLevelVisibilitySourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Other" },
		{ "Comment", "/** A recording source that records level visibility state */" },
		{ "IncludePath", "TakeRecorderLevelVisibilitySource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderLevelVisibilitySource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Level Visibility" },
		{ "ToolTip", "A recording source that records level visibility state" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderLevelVisibilitySource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::ClassParams = {
		&UTakeRecorderLevelVisibilitySource::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderLevelVisibilitySource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderLevelVisibilitySource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderLevelVisibilitySource, 755199830);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderLevelVisibilitySource>()
	{
		return UTakeRecorderLevelVisibilitySource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderLevelVisibilitySource(Z_Construct_UClass_UTakeRecorderLevelVisibilitySource, &UTakeRecorderLevelVisibilitySource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderLevelVisibilitySource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderLevelVisibilitySource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
