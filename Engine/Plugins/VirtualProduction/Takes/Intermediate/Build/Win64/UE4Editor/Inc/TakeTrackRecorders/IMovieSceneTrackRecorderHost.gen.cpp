// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeTrackRecorders/Public/TrackRecorders/IMovieSceneTrackRecorderHost.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIMovieSceneTrackRecorderHost() {}
// Cross Module References
	TAKETRACKRECORDERS_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderTrackSettings();
	UPackage* Z_Construct_UPackage__Script_TakeTrackRecorders();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
	TAKETRACKRECORDERS_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings();
// End Cross Module References
class UScriptStruct* FTakeRecorderTrackSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TAKETRACKRECORDERS_API uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings, Z_Construct_UPackage__Script_TakeTrackRecorders(), TEXT("TakeRecorderTrackSettings"), sizeof(FTakeRecorderTrackSettings), Get_Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Hash());
	}
	return Singleton;
}
template<> TAKETRACKRECORDERS_API UScriptStruct* StaticStruct<FTakeRecorderTrackSettings>()
{
	return FTakeRecorderTrackSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeRecorderTrackSettings(FTakeRecorderTrackSettings::StaticStruct, TEXT("/Script/TakeTrackRecorders"), TEXT("TakeRecorderTrackSettings"), false, nullptr, nullptr);
static struct FScriptStruct_TakeTrackRecorders_StaticRegisterNativesFTakeRecorderTrackSettings
{
	FScriptStruct_TakeTrackRecorders_StaticRegisterNativesFTakeRecorderTrackSettings()
	{
		UScriptStruct::DeferCppStructOps<FTakeRecorderTrackSettings>(FName(TEXT("TakeRecorderTrackSettings")));
	}
} ScriptStruct_TakeTrackRecorders_StaticRegisterNativesFTakeRecorderTrackSettings;
	struct Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatchingActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MatchingActorClass;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultPropertyTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultPropertyTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DefaultPropertyTracks;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludePropertyTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludePropertyTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludePropertyTracks;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTakeRecorderTrackSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_MatchingActorClass_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** The Actor class to create movie scene tracks for. */" },
		{ "MetaClass", "Actor" },
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
		{ "ToolTip", "The Actor class to create movie scene tracks for." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_MatchingActorClass = { "MatchingActorClass", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderTrackSettings, MatchingActorClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_MatchingActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_MatchingActorClass_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks_Inner = { "DefaultPropertyTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** List of property names for which movie scene tracks will be created automatically. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
		{ "ToolTip", "List of property names for which movie scene tracks will be created automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks = { "DefaultPropertyTracks", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderTrackSettings, DefaultPropertyTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks_Inner = { "ExcludePropertyTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** List of property names for which movie scene tracks will NOT be created automatically. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
		{ "ToolTip", "List of property names for which movie scene tracks will NOT be created automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks = { "ExcludePropertyTracks", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderTrackSettings, ExcludePropertyTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_MatchingActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_DefaultPropertyTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::NewProp_ExcludePropertyTracks,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TakeTrackRecorders,
		nullptr,
		&NewStructOps,
		"TakeRecorderTrackSettings",
		sizeof(FTakeRecorderTrackSettings),
		alignof(FTakeRecorderTrackSettings),
		Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderTrackSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TakeTrackRecorders();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeRecorderTrackSettings"), sizeof(FTakeRecorderTrackSettings), Get_Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderTrackSettings_Hash() { return 3247505454U; }
class UScriptStruct* FTakeRecorderPropertyTrackSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TAKETRACKRECORDERS_API uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings, Z_Construct_UPackage__Script_TakeTrackRecorders(), TEXT("TakeRecorderPropertyTrackSettings"), sizeof(FTakeRecorderPropertyTrackSettings), Get_Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Hash());
	}
	return Singleton;
}
template<> TAKETRACKRECORDERS_API UScriptStruct* StaticStruct<FTakeRecorderPropertyTrackSettings>()
{
	return FTakeRecorderPropertyTrackSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeRecorderPropertyTrackSettings(FTakeRecorderPropertyTrackSettings::StaticStruct, TEXT("/Script/TakeTrackRecorders"), TEXT("TakeRecorderPropertyTrackSettings"), false, nullptr, nullptr);
static struct FScriptStruct_TakeTrackRecorders_StaticRegisterNativesFTakeRecorderPropertyTrackSettings
{
	FScriptStruct_TakeTrackRecorders_StaticRegisterNativesFTakeRecorderPropertyTrackSettings()
	{
		UScriptStruct::DeferCppStructOps<FTakeRecorderPropertyTrackSettings>(FName(TEXT("TakeRecorderPropertyTrackSettings")));
	}
} ScriptStruct_TakeTrackRecorders_StaticRegisterNativesFTakeRecorderPropertyTrackSettings;
	struct Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ComponentPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PropertyPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTakeRecorderPropertyTrackSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_ComponentPath_MetaData[] = {
		{ "Category", "PropertyTrack" },
		{ "Comment", "/** Optional ActorComponent tag (when keying a component property). */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
		{ "ToolTip", "Optional ActorComponent tag (when keying a component property)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_ComponentPath = { "ComponentPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderPropertyTrackSettings, ComponentPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_ComponentPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_ComponentPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_PropertyPath_MetaData[] = {
		{ "Category", "PropertyTrack" },
		{ "Comment", "/** Path to the keyed property within the Actor or ActorComponent. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/IMovieSceneTrackRecorderHost.h" },
		{ "ToolTip", "Path to the keyed property within the Actor or ActorComponent." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_PropertyPath = { "PropertyPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderPropertyTrackSettings, PropertyPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_PropertyPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_PropertyPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_ComponentPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::NewProp_PropertyPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TakeTrackRecorders,
		nullptr,
		&NewStructOps,
		"TakeRecorderPropertyTrackSettings",
		sizeof(FTakeRecorderPropertyTrackSettings),
		alignof(FTakeRecorderPropertyTrackSettings),
		Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TakeTrackRecorders();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeRecorderPropertyTrackSettings"), sizeof(FTakeRecorderPropertyTrackSettings), Get_Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderPropertyTrackSettings_Hash() { return 2765960758U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
