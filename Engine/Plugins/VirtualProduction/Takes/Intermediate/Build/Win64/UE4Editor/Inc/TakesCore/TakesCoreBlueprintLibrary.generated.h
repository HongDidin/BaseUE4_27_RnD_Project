// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FAssetData;
#ifdef TAKESCORE_TakesCoreBlueprintLibrary_generated_h
#error "TakesCoreBlueprintLibrary.generated.h already included, missing '#pragma once' in TakesCoreBlueprintLibrary.h"
#endif
#define TAKESCORE_TakesCoreBlueprintLibrary_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_36_DELEGATE \
struct TakesCoreBlueprintLibrary_eventOnTakeRecorderTakeNumberChanged_Parms \
{ \
	int32 TakeNumber; \
}; \
static inline void FOnTakeRecorderTakeNumberChanged_DelegateWrapper(const FScriptDelegate& OnTakeRecorderTakeNumberChanged, int32 TakeNumber) \
{ \
	TakesCoreBlueprintLibrary_eventOnTakeRecorderTakeNumberChanged_Parms Parms; \
	Parms.TakeNumber=TakeNumber; \
	OnTakeRecorderTakeNumberChanged.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_35_DELEGATE \
struct TakesCoreBlueprintLibrary_eventOnTakeRecorderSlateChanged_Parms \
{ \
	FString Slate; \
}; \
static inline void FOnTakeRecorderSlateChanged_DelegateWrapper(const FScriptDelegate& OnTakeRecorderSlateChanged, const FString& Slate) \
{ \
	TakesCoreBlueprintLibrary_eventOnTakeRecorderSlateChanged_Parms Parms; \
	Parms.Slate=Slate; \
	OnTakeRecorderSlateChanged.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetOnTakeRecorderTakeNumberChanged); \
	DECLARE_FUNCTION(execSetOnTakeRecorderSlateChanged); \
	DECLARE_FUNCTION(execFindTakes); \
	DECLARE_FUNCTION(execComputeNextTakeNumber);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetOnTakeRecorderTakeNumberChanged); \
	DECLARE_FUNCTION(execSetOnTakeRecorderSlateChanged); \
	DECLARE_FUNCTION(execFindTakes); \
	DECLARE_FUNCTION(execComputeNextTakeNumber);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakesCoreBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UTakesCoreBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UTakesCoreBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakesCoreBlueprintLibrary)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUTakesCoreBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UTakesCoreBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UTakesCoreBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakesCoreBlueprintLibrary)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakesCoreBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakesCoreBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakesCoreBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakesCoreBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakesCoreBlueprintLibrary(UTakesCoreBlueprintLibrary&&); \
	NO_API UTakesCoreBlueprintLibrary(const UTakesCoreBlueprintLibrary&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakesCoreBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakesCoreBlueprintLibrary(UTakesCoreBlueprintLibrary&&); \
	NO_API UTakesCoreBlueprintLibrary(const UTakesCoreBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakesCoreBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakesCoreBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakesCoreBlueprintLibrary)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKESCORE_API UClass* StaticClass<class UTakesCoreBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakesCoreBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
