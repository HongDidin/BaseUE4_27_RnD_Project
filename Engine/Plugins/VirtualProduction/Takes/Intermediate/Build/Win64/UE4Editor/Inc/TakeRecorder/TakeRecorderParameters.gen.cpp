// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorder/Public/Recorder/TakeRecorderParameters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderParameters() {}
// Cross Module References
	TAKERECORDER_API UEnum* Z_Construct_UEnum_TakeRecorder_ETakeRecorderMode();
	UPackage* Z_Construct_UPackage__Script_TakeRecorder();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderParameters();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderUserParameters();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderProjectParameters();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	TAKETRACKRECORDERS_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderTrackSettings();
// End Cross Module References
	static UEnum* ETakeRecorderMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TakeRecorder_ETakeRecorderMode, Z_Construct_UPackage__Script_TakeRecorder(), TEXT("ETakeRecorderMode"));
		}
		return Singleton;
	}
	template<> TAKERECORDER_API UEnum* StaticEnum<ETakeRecorderMode>()
	{
		return ETakeRecorderMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETakeRecorderMode(ETakeRecorderMode_StaticEnum, TEXT("/Script/TakeRecorder"), TEXT("ETakeRecorderMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TakeRecorder_ETakeRecorderMode_Hash() { return 2055765743U; }
	UEnum* Z_Construct_UEnum_TakeRecorder_ETakeRecorderMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorder();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETakeRecorderMode"), 0, Get_Z_Construct_UEnum_TakeRecorder_ETakeRecorderMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETakeRecorderMode::RecordNewSequence", (int64)ETakeRecorderMode::RecordNewSequence },
				{ "ETakeRecorderMode::RecordIntoSequence", (int64)ETakeRecorderMode::RecordIntoSequence },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
				{ "RecordIntoSequence.Comment", "/* Record into an existing sequence */" },
				{ "RecordIntoSequence.Name", "ETakeRecorderMode::RecordIntoSequence" },
				{ "RecordIntoSequence.ToolTip", "Record into an existing sequence" },
				{ "RecordNewSequence.Comment", "/* Record into a new sequence */" },
				{ "RecordNewSequence.Name", "ETakeRecorderMode::RecordNewSequence" },
				{ "RecordNewSequence.ToolTip", "Record into a new sequence" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TakeRecorder,
				nullptr,
				"ETakeRecorderMode",
				"ETakeRecorderMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FTakeRecorderParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeRecorderParameters, Z_Construct_UPackage__Script_TakeRecorder(), TEXT("TakeRecorderParameters"), sizeof(FTakeRecorderParameters), Get_Z_Construct_UScriptStruct_FTakeRecorderParameters_Hash());
	}
	return Singleton;
}
template<> TAKERECORDER_API UScriptStruct* StaticStruct<FTakeRecorderParameters>()
{
	return FTakeRecorderParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeRecorderParameters(FTakeRecorderParameters::StaticStruct, TEXT("/Script/TakeRecorder"), TEXT("TakeRecorderParameters"), false, nullptr, nullptr);
static struct FScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderParameters
{
	FScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderParameters()
	{
		UScriptStruct::DeferCppStructOps<FTakeRecorderParameters>(FName(TEXT("TakeRecorderParameters")));
	}
} ScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderParameters;
	struct Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_User_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_User;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Project_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Project;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TakeRecorderMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeRecorderMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TakeRecorderMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableRecordingAndSave_MetaData[];
#endif
		static void NewProp_bDisableRecordingAndSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableRecordingAndSave;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Structure housing all configurable parameters for a take recorder instance\n */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Structure housing all configurable parameters for a take recorder instance" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTakeRecorderParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_User_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_User = { "User", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderParameters, User), Z_Construct_UScriptStruct_FTakeRecorderUserParameters, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_User_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_User_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_Project_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_Project = { "Project", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderParameters, Project), Z_Construct_UScriptStruct_FTakeRecorderProjectParameters, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_Project_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_Project_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode = { "TakeRecorderMode", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderParameters, TakeRecorderMode), Z_Construct_UEnum_TakeRecorder_ETakeRecorderMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave_MetaData[] = {
		{ "Comment", "/**\n\x09 * Option to disable recording and saving of data. This can be used in a scenario where multiple clients are running\n\x09 * take recorder, but only certain ones are set to process and save the data.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Option to disable recording and saving of data. This can be used in a scenario where multiple clients are running\ntake recorder, but only certain ones are set to process and save the data." },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave_SetBit(void* Obj)
	{
		((FTakeRecorderParameters*)Obj)->bDisableRecordingAndSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave = { "bDisableRecordingAndSave", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderParameters), &Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_User,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_Project,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_TakeRecorderMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::NewProp_bDisableRecordingAndSave,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
		nullptr,
		&NewStructOps,
		"TakeRecorderParameters",
		sizeof(FTakeRecorderParameters),
		alignof(FTakeRecorderParameters),
		Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeRecorderParameters"), sizeof(FTakeRecorderParameters), Get_Z_Construct_UScriptStruct_FTakeRecorderParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTakeRecorderParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderParameters_Hash() { return 4070599483U; }
class UScriptStruct* FTakeRecorderProjectParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters, Z_Construct_UPackage__Script_TakeRecorder(), TEXT("TakeRecorderProjectParameters"), sizeof(FTakeRecorderProjectParameters), Get_Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Hash());
	}
	return Singleton;
}
template<> TAKERECORDER_API UScriptStruct* StaticStruct<FTakeRecorderProjectParameters>()
{
	return FTakeRecorderProjectParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeRecorderProjectParameters(FTakeRecorderProjectParameters::StaticStruct, TEXT("/Script/TakeRecorder"), TEXT("TakeRecorderProjectParameters"), false, nullptr, nullptr);
static struct FScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderProjectParameters
{
	FScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderProjectParameters()
	{
		UScriptStruct::DeferCppStructOps<FTakeRecorderProjectParameters>(FName(TEXT("TakeRecorderProjectParameters")));
	}
} ScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderProjectParameters;
	struct Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootTakeSaveDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RootTakeSaveDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeSaveDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TakeSaveDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSlate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultSlate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bStartAtCurrentTimecode_MetaData[];
#endif
		static void NewProp_bStartAtCurrentTimecode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bStartAtCurrentTimecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordTimecode_MetaData[];
#endif
		static void NewProp_bRecordTimecode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordTimecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordSourcesIntoSubSequences_MetaData[];
#endif
		static void NewProp_bRecordSourcesIntoSubSequences_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordSourcesIntoSubSequences;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordToPossessable_MetaData[];
#endif
		static void NewProp_bRecordToPossessable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordToPossessable;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DefaultTracks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowNotifications_MetaData[];
#endif
		static void NewProp_bShowNotifications_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowNotifications;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTakeRecorderProjectParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_RootTakeSaveDir_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/** The root of the directory in which to save recorded takes. */" },
		{ "ContentDir", "" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "The root of the directory in which to save recorded takes." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_RootTakeSaveDir = { "RootTakeSaveDir", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderProjectParameters, RootTakeSaveDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_RootTakeSaveDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_RootTakeSaveDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_TakeSaveDir_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * The name of the directory in which to save recorded takes. Supports any of the following format specifiers that will be substituted when a take is recorded:\n\x09 * {day}       - The day of the timestamp for the start of the recording.\n\x09 * {month}     - The month of the timestamp for the start of the recording.\n\x09 * {year}      - The year of the timestamp for the start of the recording.\n\x09 * {hour}      - The hour of the timestamp for the start of the recording.\n\x09 * {minute}    - The minute of the timestamp for the start of the recording.\n\x09 * {second}    - The second of the timestamp for the start of the recording.\n\x09 * {take}      - The take number.\n\x09 * {slate}     - The slate string.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "The name of the directory in which to save recorded takes. Supports any of the following format specifiers that will be substituted when a take is recorded:\n{day}       - The day of the timestamp for the start of the recording.\n{month}     - The month of the timestamp for the start of the recording.\n{year}      - The year of the timestamp for the start of the recording.\n{hour}      - The hour of the timestamp for the start of the recording.\n{minute}    - The minute of the timestamp for the start of the recording.\n{second}    - The second of the timestamp for the start of the recording.\n{take}      - The take number.\n{slate}     - The slate string." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_TakeSaveDir = { "TakeSaveDir", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderProjectParameters, TakeSaveDir), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_TakeSaveDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_TakeSaveDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultSlate_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * The default name to use for the Slate information\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "The default name to use for the Slate information" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultSlate = { "DefaultSlate", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderProjectParameters, DefaultSlate), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultSlate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultSlate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * If enabled, track sections will start at the current timecode. Otherwise, 0.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "If enabled, track sections will start at the current timecode. Otherwise, 0." },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode_SetBit(void* Obj)
	{
		((FTakeRecorderProjectParameters*)Obj)->bStartAtCurrentTimecode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode = { "bStartAtCurrentTimecode", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderProjectParameters), &Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * If enabled, timecode will be recorded into each actor track\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "If enabled, timecode will be recorded into each actor track" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode_SetBit(void* Obj)
	{
		((FTakeRecorderProjectParameters*)Obj)->bRecordTimecode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode = { "bRecordTimecode", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderProjectParameters), &Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09* If enabled, each Source will be recorded into a separate Sequence and embedded in the Master Sequence will link to them via Subscenes track.\n\x09* If disabled, all Sources will be recorded into the Master Sequence, and you will not be able to swap between various takes of specific source\n\x09* using the Sequencer Take ui. This can still be done via copying and pasting between sequences if needed.\n\x09*/" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "If enabled, each Source will be recorded into a separate Sequence and embedded in the Master Sequence will link to them via Subscenes track.\nIf disabled, all Sources will be recorded into the Master Sequence, and you will not be able to swap between various takes of specific source\nusing the Sequencer Take ui. This can still be done via copying and pasting between sequences if needed." },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences_SetBit(void* Obj)
	{
		((FTakeRecorderProjectParameters*)Obj)->bRecordSourcesIntoSubSequences = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences = { "bRecordSourcesIntoSubSequences", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderProjectParameters), &Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/*\n\x09 * If enabled, all recorded actors will be recorded to possessable object bindings in Sequencer. If disabled, all recorded actors will be \n\x09 * recorded to spawnable object bindings in Sequencer. This can be overridden per actor source.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "* If enabled, all recorded actors will be recorded to possessable object bindings in Sequencer. If disabled, all recorded actors will be\n* recorded to spawnable object bindings in Sequencer. This can be overridden per actor source." },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable_SetBit(void* Obj)
	{
		((FTakeRecorderProjectParameters*)Obj)->bRecordToPossessable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable = { "bRecordToPossessable", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderProjectParameters), &Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks_Inner = { "DefaultTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTakeRecorderTrackSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/** List of property names for which movie scene tracks will always record. */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "List of property names for which movie scene tracks will always record." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks = { "DefaultTracks", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderProjectParameters, DefaultTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/** Whether to show notification windows or not when recording */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Whether to show notification windows or not when recording" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications_SetBit(void* Obj)
	{
		((FTakeRecorderProjectParameters*)Obj)->bShowNotifications = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications = { "bShowNotifications", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderProjectParameters), &Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_RootTakeSaveDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_TakeSaveDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultSlate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bStartAtCurrentTimecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordTimecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordSourcesIntoSubSequences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bRecordToPossessable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_DefaultTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::NewProp_bShowNotifications,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
		nullptr,
		&NewStructOps,
		"TakeRecorderProjectParameters",
		sizeof(FTakeRecorderProjectParameters),
		alignof(FTakeRecorderProjectParameters),
		Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderProjectParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeRecorderProjectParameters"), sizeof(FTakeRecorderProjectParameters), Get_Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderProjectParameters_Hash() { return 159261516U; }
class UScriptStruct* FTakeRecorderUserParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeRecorderUserParameters, Z_Construct_UPackage__Script_TakeRecorder(), TEXT("TakeRecorderUserParameters"), sizeof(FTakeRecorderUserParameters), Get_Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Hash());
	}
	return Singleton;
}
template<> TAKERECORDER_API UScriptStruct* StaticStruct<FTakeRecorderUserParameters>()
{
	return FTakeRecorderUserParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeRecorderUserParameters(FTakeRecorderUserParameters::StaticStruct, TEXT("/Script/TakeRecorder"), TEXT("TakeRecorderUserParameters"), false, nullptr, nullptr);
static struct FScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderUserParameters
{
	FScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderUserParameters()
	{
		UScriptStruct::DeferCppStructOps<FTakeRecorderUserParameters>(FName(TEXT("TakeRecorderUserParameters")));
	}
} ScriptStruct_TakeRecorder_StaticRegisterNativesFTakeRecorderUserParameters;
	struct Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMaximizeViewport_MetaData[];
#endif
		static void NewProp_bMaximizeViewport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMaximizeViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CountdownSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CountdownSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineTimeDilation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EngineTimeDilation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bStopAtPlaybackEnd_MetaData[];
#endif
		static void NewProp_bStopAtPlaybackEnd_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bStopAtPlaybackEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveRedundantTracks_MetaData[];
#endif
		static void NewProp_bRemoveRedundantTracks_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveRedundantTracks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReduceKeysTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReduceKeysTolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSaveRecordedAssets_MetaData[];
#endif
		static void NewProp_bSaveRecordedAssets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSaveRecordedAssets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoLock_MetaData[];
#endif
		static void NewProp_bAutoLock_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoLock;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoSerialize_MetaData[];
#endif
		static void NewProp_bAutoSerialize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoSerialize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTakeRecorderUserParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Whether to maximize the viewport (enter Immersive Mode) when recording */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Whether to maximize the viewport (enter Immersive Mode) when recording" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport_SetBit(void* Obj)
	{
		((FTakeRecorderUserParameters*)Obj)->bMaximizeViewport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport = { "bMaximizeViewport", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderUserParameters), &Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_CountdownSeconds_MetaData[] = {
		{ "Category", "User Settings" },
		{ "ClampMax", "60.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Delay that we will use before starting recording */" },
		{ "DisplayName", "Countdown" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Delay that we will use before starting recording" },
		{ "UIMax", "60.0" },
		{ "UIMin", "0.0" },
		{ "Units", "s" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_CountdownSeconds = { "CountdownSeconds", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderUserParameters, CountdownSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_CountdownSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_CountdownSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_EngineTimeDilation_MetaData[] = {
		{ "Category", "User Settings" },
		{ "ClampMin", "0.00001" },
		{ "Comment", "/** The engine time dilation to apply during the recording */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "The engine time dilation to apply during the recording" },
		{ "UIMin", "0.00001" },
		{ "Units", "Multiplier" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_EngineTimeDilation = { "EngineTimeDilation", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderUserParameters, EngineTimeDilation), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_EngineTimeDilation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_EngineTimeDilation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Automatically stop recording when reaching the end of the playback range */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Automatically stop recording when reaching the end of the playback range" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd_SetBit(void* Obj)
	{
		((FTakeRecorderUserParameters*)Obj)->bStopAtPlaybackEnd = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd = { "bStopAtPlaybackEnd", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderUserParameters), &Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Recommended for use with recorded spawnables. Beware that changes to actor instances in the map after recording may alter the recording when played back */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Recommended for use with recorded spawnables. Beware that changes to actor instances in the map after recording may alter the recording when played back" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks_SetBit(void* Obj)
	{
		((FTakeRecorderUserParameters*)Obj)->bRemoveRedundantTracks = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks = { "bRemoveRedundantTracks", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderUserParameters), &Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_ReduceKeysTolerance_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Tolerance to use when reducing keys */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Tolerance to use when reducing keys" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_ReduceKeysTolerance = { "ReduceKeysTolerance", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTakeRecorderUserParameters, ReduceKeysTolerance), METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_ReduceKeysTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_ReduceKeysTolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Whether to save recorded level sequences and assets when done recording */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Whether to save recorded level sequences and assets when done recording" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets_SetBit(void* Obj)
	{
		((FTakeRecorderUserParameters*)Obj)->bSaveRecordedAssets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets = { "bSaveRecordedAssets", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderUserParameters), &Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Whether to lock the level sequence when done recording */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Whether to lock the level sequence when done recording" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock_SetBit(void* Obj)
	{
		((FTakeRecorderUserParameters*)Obj)->bAutoLock = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock = { "bAutoLock", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderUserParameters), &Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize_MetaData[] = {
		{ "Category", "User Settings" },
		{ "Comment", "/** Whether to incrementally serialize and store some data while recording*/" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderParameters.h" },
		{ "ToolTip", "Whether to incrementally serialize and store some data while recording" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize_SetBit(void* Obj)
	{
		((FTakeRecorderUserParameters*)Obj)->bAutoSerialize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize = { "bAutoSerialize", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecorderUserParameters), &Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bMaximizeViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_CountdownSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_EngineTimeDilation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bStopAtPlaybackEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bRemoveRedundantTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_ReduceKeysTolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bSaveRecordedAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoLock,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::NewProp_bAutoSerialize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
		nullptr,
		&NewStructOps,
		"TakeRecorderUserParameters",
		sizeof(FTakeRecorderUserParameters),
		alignof(FTakeRecorderUserParameters),
		Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderUserParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeRecorderUserParameters"), sizeof(FTakeRecorderUserParameters), Get_Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeRecorderUserParameters_Hash() { return 191567800U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
