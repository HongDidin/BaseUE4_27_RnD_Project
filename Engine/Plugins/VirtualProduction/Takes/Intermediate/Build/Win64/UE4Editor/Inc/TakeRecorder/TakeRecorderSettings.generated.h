// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKERECORDER_TakeRecorderSettings_generated_h
#error "TakeRecorderSettings.generated.h already included, missing '#pragma once' in TakeRecorderSettings.h"
#endif
#define TAKERECORDER_TakeRecorderSettings_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderUserSettings(); \
	friend struct Z_Construct_UClass_UTakeRecorderUserSettings_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderUserSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TakeRecorder"), TAKERECORDER_API) \
	DECLARE_SERIALIZER(UTakeRecorderUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderUserSettings(); \
	friend struct Z_Construct_UClass_UTakeRecorderUserSettings_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderUserSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TakeRecorder"), TAKERECORDER_API) \
	DECLARE_SERIALIZER(UTakeRecorderUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TAKERECORDER_API UTakeRecorderUserSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderUserSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TAKERECORDER_API, UTakeRecorderUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderUserSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TAKERECORDER_API UTakeRecorderUserSettings(UTakeRecorderUserSettings&&); \
	TAKERECORDER_API UTakeRecorderUserSettings(const UTakeRecorderUserSettings&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TAKERECORDER_API UTakeRecorderUserSettings(UTakeRecorderUserSettings&&); \
	TAKERECORDER_API UTakeRecorderUserSettings(const UTakeRecorderUserSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TAKERECORDER_API, UTakeRecorderUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderUserSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTakeRecorderUserSettings)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDER_API UClass* StaticClass<class UTakeRecorderUserSettings>();

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderProjectSettings(); \
	friend struct Z_Construct_UClass_UTakeRecorderProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderProjectSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TakeRecorder"), TAKERECORDER_API) \
	DECLARE_SERIALIZER(UTakeRecorderProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderProjectSettings(); \
	friend struct Z_Construct_UClass_UTakeRecorderProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderProjectSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TakeRecorder"), TAKERECORDER_API) \
	DECLARE_SERIALIZER(UTakeRecorderProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \



#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TAKERECORDER_API UTakeRecorderProjectSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderProjectSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TAKERECORDER_API, UTakeRecorderProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderProjectSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TAKERECORDER_API UTakeRecorderProjectSettings(UTakeRecorderProjectSettings&&); \
	TAKERECORDER_API UTakeRecorderProjectSettings(const UTakeRecorderProjectSettings&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TAKERECORDER_API UTakeRecorderProjectSettings(UTakeRecorderProjectSettings&&); \
	TAKERECORDER_API UTakeRecorderProjectSettings(const UTakeRecorderProjectSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TAKERECORDER_API, UTakeRecorderProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderProjectSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTakeRecorderProjectSettings)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_48_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h_52_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDER_API UClass* StaticClass<class UTakeRecorderProjectSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
