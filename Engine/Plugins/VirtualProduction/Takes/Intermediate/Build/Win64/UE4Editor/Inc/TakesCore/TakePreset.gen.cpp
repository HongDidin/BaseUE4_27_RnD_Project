// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakesCore/Public/TakePreset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakePreset() {}
// Cross Module References
	TAKESCORE_API UClass* Z_Construct_UClass_UTakePreset_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakePreset();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_TakesCore();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
// End Cross Module References
	void UTakePreset::StaticRegisterNativesUTakePreset()
	{
	}
	UClass* Z_Construct_UClass_UTakePreset_NoRegister()
	{
		return UTakePreset::StaticClass();
	}
	struct Z_Construct_UClass_UTakePreset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakePreset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakesCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakePreset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Take preset that is stored as an asset comprising a ULevelSequence, and a set of actor recording sources\n */" },
		{ "IncludePath", "TakePreset.h" },
		{ "ModuleRelativePath", "Public/TakePreset.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Take preset that is stored as an asset comprising a ULevelSequence, and a set of actor recording sources" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakePreset_Statics::NewProp_LevelSequence_MetaData[] = {
		{ "Comment", "/** Instanced level sequence template that is used to define a starting point for a new take recording */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TakePreset.h" },
		{ "ToolTip", "Instanced level sequence template that is used to define a starting point for a new take recording" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakePreset_Statics::NewProp_LevelSequence = { "LevelSequence", nullptr, (EPropertyFlags)0x0042000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakePreset, LevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakePreset_Statics::NewProp_LevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakePreset_Statics::NewProp_LevelSequence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakePreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakePreset_Statics::NewProp_LevelSequence,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakePreset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakePreset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakePreset_Statics::ClassParams = {
		&UTakePreset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakePreset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakePreset_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakePreset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakePreset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakePreset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakePreset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakePreset, 2573473802);
	template<> TAKESCORE_API UClass* StaticClass<UTakePreset>()
	{
		return UTakePreset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakePreset(Z_Construct_UClass_UTakePreset, &UTakePreset::StaticClass, TEXT("/Script/TakesCore"), TEXT("UTakePreset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakePreset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
