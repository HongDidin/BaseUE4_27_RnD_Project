// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKETRACKRECORDERS_MovieSceneVisibilityTrackRecorder_generated_h
#error "MovieSceneVisibilityTrackRecorder.generated.h already included, missing '#pragma once' in MovieSceneVisibilityTrackRecorder.h"
#endif
#define TAKETRACKRECORDERS_MovieSceneVisibilityTrackRecorder_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneVisibilityTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneVisibilityTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneVisibilityTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TakeTrackRecorders"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneVisibilityTrackRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneVisibilityTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneVisibilityTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneVisibilityTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TakeTrackRecorders"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneVisibilityTrackRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneVisibilityTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneVisibilityTrackRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneVisibilityTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneVisibilityTrackRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneVisibilityTrackRecorder(UMovieSceneVisibilityTrackRecorder&&); \
	NO_API UMovieSceneVisibilityTrackRecorder(const UMovieSceneVisibilityTrackRecorder&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneVisibilityTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneVisibilityTrackRecorder(UMovieSceneVisibilityTrackRecorder&&); \
	NO_API UMovieSceneVisibilityTrackRecorder(const UMovieSceneVisibilityTrackRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneVisibilityTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneVisibilityTrackRecorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneVisibilityTrackRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_30_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKETRACKRECORDERS_API UClass* StaticClass<class UMovieSceneVisibilityTrackRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeTrackRecorders_Public_TrackRecorders_MovieSceneVisibilityTrackRecorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
