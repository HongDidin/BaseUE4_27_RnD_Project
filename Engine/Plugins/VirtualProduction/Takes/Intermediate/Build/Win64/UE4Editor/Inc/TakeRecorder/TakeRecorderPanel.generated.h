// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTakeRecorderSources;
struct FFrameRate;
class UTakeMetaData;
class ULevelSequence;
class UTakePreset;
enum class ETakeRecorderPanelMode : uint8;
#ifdef TAKERECORDER_TakeRecorderPanel_generated_h
#error "TakeRecorderPanel.generated.h already included, missing '#pragma once' in TakeRecorderPanel.h"
#endif
#define TAKERECORDER_TakeRecorderPanel_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCanStartRecording); \
	DECLARE_FUNCTION(execStopRecording); \
	DECLARE_FUNCTION(execStartRecording); \
	DECLARE_FUNCTION(execGetSources); \
	DECLARE_FUNCTION(execSetFrameRateFromTimecode); \
	DECLARE_FUNCTION(execSetFrameRate); \
	DECLARE_FUNCTION(execGetFrameRate); \
	DECLARE_FUNCTION(execGetTakeMetaData); \
	DECLARE_FUNCTION(execGetLastRecordedLevelSequence); \
	DECLARE_FUNCTION(execGetLevelSequence); \
	DECLARE_FUNCTION(execClearPendingTake); \
	DECLARE_FUNCTION(execNewTake); \
	DECLARE_FUNCTION(execSetupForViewing); \
	DECLARE_FUNCTION(execSetupForEditing); \
	DECLARE_FUNCTION(execSetupForRecordingInto_LevelSequence); \
	DECLARE_FUNCTION(execSetupForRecording_LevelSequence); \
	DECLARE_FUNCTION(execSetupForRecording_TakePreset); \
	DECLARE_FUNCTION(execGetMode);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCanStartRecording); \
	DECLARE_FUNCTION(execStopRecording); \
	DECLARE_FUNCTION(execStartRecording); \
	DECLARE_FUNCTION(execGetSources); \
	DECLARE_FUNCTION(execSetFrameRateFromTimecode); \
	DECLARE_FUNCTION(execSetFrameRate); \
	DECLARE_FUNCTION(execGetFrameRate); \
	DECLARE_FUNCTION(execGetTakeMetaData); \
	DECLARE_FUNCTION(execGetLastRecordedLevelSequence); \
	DECLARE_FUNCTION(execGetLevelSequence); \
	DECLARE_FUNCTION(execClearPendingTake); \
	DECLARE_FUNCTION(execNewTake); \
	DECLARE_FUNCTION(execSetupForViewing); \
	DECLARE_FUNCTION(execSetupForEditing); \
	DECLARE_FUNCTION(execSetupForRecordingInto_LevelSequence); \
	DECLARE_FUNCTION(execSetupForRecording_LevelSequence); \
	DECLARE_FUNCTION(execSetupForRecording_TakePreset); \
	DECLARE_FUNCTION(execGetMode);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderPanel(); \
	friend struct Z_Construct_UClass_UTakeRecorderPanel_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderPanel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderPanel)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderPanel(); \
	friend struct Z_Construct_UClass_UTakeRecorderPanel_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderPanel, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderPanel)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderPanel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderPanel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderPanel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderPanel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderPanel(UTakeRecorderPanel&&); \
	NO_API UTakeRecorderPanel(const UTakeRecorderPanel&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderPanel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderPanel(UTakeRecorderPanel&&); \
	NO_API UTakeRecorderPanel(const UTakeRecorderPanel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderPanel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderPanel); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderPanel)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_31_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h_36_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDER_API UClass* StaticClass<class UTakeRecorderPanel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderPanel_h


#define FOREACH_ENUM_ETAKERECORDERPANELMODE(op) \
	op(ETakeRecorderPanelMode::NewRecording) \
	op(ETakeRecorderPanelMode::RecordingInto) \
	op(ETakeRecorderPanelMode::EditingPreset) \
	op(ETakeRecorderPanelMode::ReviewingRecording) 

enum class ETakeRecorderPanelMode : uint8;
template<> TAKERECORDER_API UEnum* StaticEnum<ETakeRecorderPanelMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
