// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorder/Public/Recorder/TakeRecorder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorder() {}
// Cross Module References
	TAKERECORDER_API UEnum* Z_Construct_UEnum_TakeRecorder_ETakeRecorderState();
	UPackage* Z_Construct_UPackage__Script_TakeRecorder();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorder_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorder();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderOverlayWidget_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderParameters();
// End Cross Module References
	static UEnum* ETakeRecorderState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TakeRecorder_ETakeRecorderState, Z_Construct_UPackage__Script_TakeRecorder(), TEXT("ETakeRecorderState"));
		}
		return Singleton;
	}
	template<> TAKERECORDER_API UEnum* StaticEnum<ETakeRecorderState>()
	{
		return ETakeRecorderState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETakeRecorderState(ETakeRecorderState_StaticEnum, TEXT("/Script/TakeRecorder"), TEXT("ETakeRecorderState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TakeRecorder_ETakeRecorderState_Hash() { return 3037495477U; }
	UEnum* Z_Construct_UEnum_TakeRecorder_ETakeRecorderState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorder();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETakeRecorderState"), 0, Get_Z_Construct_UEnum_TakeRecorder_ETakeRecorderState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETakeRecorderState::CountingDown", (int64)ETakeRecorderState::CountingDown },
				{ "ETakeRecorderState::PreRecord", (int64)ETakeRecorderState::PreRecord },
				{ "ETakeRecorderState::TickingAfterPre", (int64)ETakeRecorderState::TickingAfterPre },
				{ "ETakeRecorderState::Started", (int64)ETakeRecorderState::Started },
				{ "ETakeRecorderState::Stopped", (int64)ETakeRecorderState::Stopped },
				{ "ETakeRecorderState::Cancelled", (int64)ETakeRecorderState::Cancelled },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Cancelled.Name", "ETakeRecorderState::Cancelled" },
				{ "CountingDown.Name", "ETakeRecorderState::CountingDown" },
				{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
				{ "PreRecord.Name", "ETakeRecorderState::PreRecord" },
				{ "Started.Name", "ETakeRecorderState::Started" },
				{ "Stopped.Name", "ETakeRecorderState::Stopped" },
				{ "TickingAfterPre.Name", "ETakeRecorderState::TickingAfterPre" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TakeRecorder,
				nullptr,
				"ETakeRecorderState",
				"ETakeRecorderState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UTakeRecorder::execGetState)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ETakeRecorderState*)Z_Param__Result=P_THIS->GetState();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorder::execGetSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSequence**)Z_Param__Result=P_THIS->GetSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorder::execGetCountdownSeconds)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetCountdownSeconds();
		P_NATIVE_END;
	}
	void UTakeRecorder::StaticRegisterNativesUTakeRecorder()
	{
		UClass* Class = UTakeRecorder::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCountdownSeconds", &UTakeRecorder::execGetCountdownSeconds },
			{ "GetSequence", &UTakeRecorder::execGetSequence },
			{ "GetState", &UTakeRecorder::execGetState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics
	{
		struct TakeRecorder_eventGetCountdownSeconds_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorder_eventGetCountdownSeconds_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * Access the number of seconds remaining before this recording will start\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "Access the number of seconds remaining before this recording will start" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorder, nullptr, "GetCountdownSeconds", nullptr, nullptr, sizeof(TakeRecorder_eventGetCountdownSeconds_Parms), Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics
	{
		struct TakeRecorder_eventGetSequence_Parms
		{
			ULevelSequence* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorder_eventGetSequence_Parms, ReturnValue), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * Access the sequence asset that this recorder is recording into\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "Access the sequence asset that this recorder is recording into" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorder, nullptr, "GetSequence", nullptr, nullptr, sizeof(TakeRecorder_eventGetSequence_Parms), Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorder_GetSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorder_GetSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorder_GetState_Statics
	{
		struct TakeRecorder_eventGetState_Parms
		{
			ETakeRecorderState ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTakeRecorder_GetState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTakeRecorder_GetState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorder_eventGetState_Parms, ReturnValue), Z_Construct_UEnum_TakeRecorder_ETakeRecorderState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorder_GetState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorder_GetState_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorder_GetState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorder_GetState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/**\n\x09 * Get the current state of this recorder\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "Get the current state of this recorder" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorder_GetState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorder, nullptr, "GetState", nullptr, nullptr, sizeof(TakeRecorder_eventGetState_Parms), Z_Construct_UFunction_UTakeRecorder_GetState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorder_GetState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorder_GetState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorder_GetState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorder_GetState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorder_GetState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTakeRecorder_NoRegister()
	{
		return UTakeRecorder::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorder_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequenceAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SequenceAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlayWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlayWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeakWorld_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_WeakWorld;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTakeRecorder_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTakeRecorder_GetCountdownSeconds, "GetCountdownSeconds" }, // 486672758
		{ &Z_Construct_UFunction_UTakeRecorder_GetSequence, "GetSequence" }, // 1930507179
		{ &Z_Construct_UFunction_UTakeRecorder_GetState, "GetState" }, // 3650808382
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Recorder/TakeRecorder.h" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorder_Statics::NewProp_SequenceAsset_MetaData[] = {
		{ "Comment", "/** The asset that we should output recorded data into */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "The asset that we should output recorded data into" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorder_Statics::NewProp_SequenceAsset = { "SequenceAsset", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorder, SequenceAsset), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_SequenceAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_SequenceAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorder_Statics::NewProp_OverlayWidget_MetaData[] = {
		{ "Comment", "/** The overlay widget for this recording */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "The overlay widget for this recording" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorder_Statics::NewProp_OverlayWidget = { "OverlayWidget", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorder, OverlayWidget), Z_Construct_UClass_UTakeRecorderOverlayWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_OverlayWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_OverlayWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorder_Statics::NewProp_WeakWorld_MetaData[] = {
		{ "Comment", "/** The world that we are recording within */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "The world that we are recording within" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UTakeRecorder_Statics::NewProp_WeakWorld = { "WeakWorld", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorder, WeakWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_WeakWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_WeakWorld_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorder_Statics::NewProp_Parameters_MetaData[] = {
		{ "Comment", "/** Parameters for the recorder - marked up as a uproperty to support reference collection */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorder.h" },
		{ "ToolTip", "Parameters for the recorder - marked up as a uproperty to support reference collection" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorder_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorder, Parameters), Z_Construct_UScriptStruct_FTakeRecorderParameters, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorder_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorder_Statics::NewProp_SequenceAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorder_Statics::NewProp_OverlayWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorder_Statics::NewProp_WeakWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorder_Statics::NewProp_Parameters,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorder_Statics::ClassParams = {
		&UTakeRecorder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTakeRecorder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorder_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorder, 3783049942);
	template<> TAKERECORDER_API UClass* StaticClass<UTakeRecorder>()
	{
		return UTakeRecorder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorder(Z_Construct_UClass_UTakeRecorder, &UTakeRecorder::StaticClass, TEXT("/Script/TakeRecorder"), TEXT("UTakeRecorder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
