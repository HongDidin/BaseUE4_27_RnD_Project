// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakesCore/Public/TakeRecorderSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderSource() {}
// Cross Module References
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_TakesCore();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
// End Cross Module References
	void UTakeRecorderSource::StaticRegisterNativesUTakeRecorderSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderSource_NoRegister()
	{
		return UTakeRecorderSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TakeNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackTint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TrackTint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakesCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderSource_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base class for all sources that can be recorded with the Take Recorder. Custom recording sources can\n * be created by inheriting from this class and implementing the Start/Tick/Stop recording functions. \n * The level sequence that the recording is being placed into is provided so that the take can decide\n * to store the data directly in the resulting level sequence, but sources are not limited to generating\n * data in the specified Level Sequence. The source should be registered with the ITakeRecorderModule for\n * it to show up in the Take Recorder UI. If creating a recording setup via code you can just add instances\n * of your Source to the UTakeRecorderSources instance you're using to record and skip registering them with\n * the module.\n *\n * Sources should reset their state before recording as there is not a guarantee that the object will be newly\n * created for each recording.\n */" },
		{ "IncludePath", "TakeRecorderSource.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/TakeRecorderSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for all sources that can be recorded with the Take Recorder. Custom recording sources can\nbe created by inheriting from this class and implementing the Start/Tick/Stop recording functions.\nThe level sequence that the recording is being placed into is provided so that the take can decide\nto store the data directly in the resulting level sequence, but sources are not limited to generating\ndata in the specified Level Sequence. The source should be registered with the ITakeRecorderModule for\nit to show up in the Take Recorder UI. If creating a recording setup via code you can just add instances\nof your Source to the UTakeRecorderSources instance you're using to record and skip registering them with\nthe module.\n\nSources should reset their state before recording as there is not a guarantee that the object will be newly\ncreated for each recording." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** True if this source is cued for recording or not */" },
		{ "ModuleRelativePath", "Public/TakeRecorderSource.h" },
		{ "ToolTip", "True if this source is cued for recording or not" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((UTakeRecorderSource*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderSource), &Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TakeNumber_MetaData[] = {
		{ "Category", "Source" },
		{ "ModuleRelativePath", "Public/TakeRecorderSource.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TakeNumber = { "TakeNumber", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderSource, TakeNumber), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TakeNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TakeNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TrackTint_MetaData[] = {
		{ "Category", "Source" },
		{ "ModuleRelativePath", "Public/TakeRecorderSource.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TrackTint = { "TrackTint", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderSource, TrackTint), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TrackTint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TrackTint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TakeNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderSource_Statics::NewProp_TrackTint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderSource_Statics::ClassParams = {
		&UTakeRecorderSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderSource_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderSource, 3047281174);
	template<> TAKESCORE_API UClass* StaticClass<UTakeRecorderSource>()
	{
		return UTakeRecorderSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderSource(Z_Construct_UClass_UTakeRecorderSource, &UTakeRecorderSource::StaticClass, TEXT("/Script/TakesCore"), TEXT("UTakeRecorderSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
