// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKESCORE_TakePreset_generated_h
#error "TakePreset.generated.h already included, missing '#pragma once' in TakePreset.h"
#endif
#define TAKESCORE_TakePreset_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakePreset(); \
	friend struct Z_Construct_UClass_UTakePreset_Statics; \
public: \
	DECLARE_CLASS(UTakePreset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakePreset)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUTakePreset(); \
	friend struct Z_Construct_UClass_UTakePreset_Statics; \
public: \
	DECLARE_CLASS(UTakePreset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakePreset)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakePreset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakePreset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakePreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakePreset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakePreset(UTakePreset&&); \
	NO_API UTakePreset(const UTakePreset&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakePreset(UTakePreset&&); \
	NO_API UTakePreset(const UTakePreset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakePreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakePreset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakePreset)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LevelSequence() { return STRUCT_OFFSET(UTakePreset, LevelSequence); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKESCORE_API UClass* StaticClass<class UTakePreset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakePreset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
