// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeTrackRecorders/Public/TrackRecorders/MovieSceneParticleTrackRecorder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneParticleTrackRecorder() {}
// Cross Module References
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneParticleTrackRecorder_NoRegister();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneParticleTrackRecorder();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneTrackRecorder();
	UPackage* Z_Construct_UPackage__Script_TakeTrackRecorders();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMovieSceneParticleTrackRecorder::execOnTriggered)
	{
		P_GET_OBJECT(UParticleSystemComponent,Z_Param_Component);
		P_GET_UBOOL(Z_Param_bActivating);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnTriggered(Z_Param_Component,Z_Param_bActivating);
		P_NATIVE_END;
	}
	void UMovieSceneParticleTrackRecorder::StaticRegisterNativesUMovieSceneParticleTrackRecorder()
	{
		UClass* Class = UMovieSceneParticleTrackRecorder::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnTriggered", &UMovieSceneParticleTrackRecorder::execOnTriggered },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics
	{
		struct MovieSceneParticleTrackRecorder_eventOnTriggered_Parms
		{
			UParticleSystemComponent* Component;
			bool bActivating;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Component_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Component;
		static void NewProp_bActivating_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActivating;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_Component_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_Component = { "Component", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneParticleTrackRecorder_eventOnTriggered_Parms, Component), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_Component_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_Component_MetaData)) };
	void Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_bActivating_SetBit(void* Obj)
	{
		((MovieSceneParticleTrackRecorder_eventOnTriggered_Parms*)Obj)->bActivating = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_bActivating = { "bActivating", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MovieSceneParticleTrackRecorder_eventOnTriggered_Parms), &Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_bActivating_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_Component,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::NewProp_bActivating,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneParticleTrackRecorder.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneParticleTrackRecorder, nullptr, "OnTriggered", nullptr, nullptr, sizeof(MovieSceneParticleTrackRecorder_eventOnTriggered_Parms), Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMovieSceneParticleTrackRecorder_NoRegister()
	{
		return UMovieSceneParticleTrackRecorder::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneTrackRecorder,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeTrackRecorders,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMovieSceneParticleTrackRecorder_OnTriggered, "OnTriggered" }, // 1087806291
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "TrackRecorders/MovieSceneParticleTrackRecorder.h" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneParticleTrackRecorder.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneParticleTrackRecorder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::ClassParams = {
		&UMovieSceneParticleTrackRecorder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneParticleTrackRecorder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneParticleTrackRecorder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneParticleTrackRecorder, 3350713989);
	template<> TAKETRACKRECORDERS_API UClass* StaticClass<UMovieSceneParticleTrackRecorder>()
	{
		return UMovieSceneParticleTrackRecorder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneParticleTrackRecorder(Z_Construct_UClass_UMovieSceneParticleTrackRecorder, &UMovieSceneParticleTrackRecorder::StaticClass, TEXT("/Script/TakeTrackRecorders"), TEXT("UMovieSceneParticleTrackRecorder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneParticleTrackRecorder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
