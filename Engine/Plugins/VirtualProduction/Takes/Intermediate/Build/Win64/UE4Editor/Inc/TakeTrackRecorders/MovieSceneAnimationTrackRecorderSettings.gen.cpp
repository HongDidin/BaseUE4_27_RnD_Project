// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeTrackRecorders/Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneAnimationTrackRecorderSettings() {}
// Cross Module References
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_NoRegister();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneTrackRecorderSettings();
	UPackage* Z_Construct_UPackage__Script_TakeTrackRecorders();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ERichCurveInterpMode();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ERichCurveTangentMode();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_NoRegister();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings();
// End Cross Module References
	void UMovieSceneAnimationTrackRecorderEditorSettings::StaticRegisterNativesUMovieSceneAnimationTrackRecorderEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_NoRegister()
	{
		return UMovieSceneAnimationTrackRecorderEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnimationTrackName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_AnimationTrackName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnimationAssetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AnimationAssetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnimationSubDirectory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AnimationSubDirectory;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TangentMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TangentMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveRootAnimation_MetaData[];
#endif
		static void NewProp_bRemoveRootAnimation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveRootAnimation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneTrackRecorderSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeTrackRecorders,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "Animation Recorder" },
		{ "IncludePath", "TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationTrackName_MetaData[] = {
		{ "Category", "Animation Recorder Settings" },
		{ "Comment", "/** Name of the recorded animation track. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ToolTip", "Name of the recorded animation track." },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationTrackName = { "AnimationTrackName", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneAnimationTrackRecorderEditorSettings, AnimationTrackName), METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationTrackName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationTrackName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationAssetName_MetaData[] = {
		{ "Category", "Animation Recorder Settings" },
		{ "Comment", "/** The name of the animation asset. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ToolTip", "The name of the animation asset." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationAssetName = { "AnimationAssetName", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneAnimationTrackRecorderEditorSettings, AnimationAssetName), METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationAssetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationAssetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationSubDirectory_MetaData[] = {
		{ "Category", "Animation Recorder Settings" },
		{ "Comment", "/** The name of the subdirectory animations will be placed in. Leave this empty to place into the same directory as the sequence base path. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ToolTip", "The name of the subdirectory animations will be placed in. Leave this empty to place into the same directory as the sequence base path." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationSubDirectory = { "AnimationSubDirectory", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneAnimationTrackRecorderEditorSettings, AnimationSubDirectory), METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationSubDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationSubDirectory_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_InterpMode_MetaData[] = {
		{ "Category", "Animation Recorder Settings" },
		{ "Comment", "/** Interpolation mode for the recorded keys. */" },
		{ "DisplayName", "Interpolation Mode" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ToolTip", "Interpolation mode for the recorded keys." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_InterpMode = { "InterpMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneAnimationTrackRecorderEditorSettings, InterpMode), Z_Construct_UEnum_Engine_ERichCurveInterpMode, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_InterpMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_InterpMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_TangentMode_MetaData[] = {
		{ "Category", "Animation Recorder Settings" },
		{ "Comment", "/** Tangent mode for the recorded keys. */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ToolTip", "Tangent mode for the recorded keys." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_TangentMode = { "TangentMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneAnimationTrackRecorderEditorSettings, TangentMode), Z_Construct_UEnum_Engine_ERichCurveTangentMode, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_TangentMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_TangentMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation_MetaData[] = {
		{ "Comment", "/** The following parameter is dynamically set based upon whether or not the animation was spawned dynamically via a blueprint or not, if so set to false, otherwise true */" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ToolTip", "The following parameter is dynamically set based upon whether or not the animation was spawned dynamically via a blueprint or not, if so set to false, otherwise true" },
	};
#endif
	void Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation_SetBit(void* Obj)
	{
		((UMovieSceneAnimationTrackRecorderEditorSettings*)Obj)->bRemoveRootAnimation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation = { "bRemoveRootAnimation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMovieSceneAnimationTrackRecorderEditorSettings), &Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationTrackName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationAssetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_AnimationSubDirectory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_InterpMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_TangentMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::NewProp_bRemoveRootAnimation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneAnimationTrackRecorderEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::ClassParams = {
		&UMovieSceneAnimationTrackRecorderEditorSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::PropPointers),
		0,
		0x001000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneAnimationTrackRecorderEditorSettings, 2425119698);
	template<> TAKETRACKRECORDERS_API UClass* StaticClass<UMovieSceneAnimationTrackRecorderEditorSettings>()
	{
		return UMovieSceneAnimationTrackRecorderEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneAnimationTrackRecorderEditorSettings(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings, &UMovieSceneAnimationTrackRecorderEditorSettings::StaticClass, TEXT("/Script/TakeTrackRecorders"), TEXT("UMovieSceneAnimationTrackRecorderEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneAnimationTrackRecorderEditorSettings);
	void UMovieSceneAnimationTrackRecorderSettings::StaticRegisterNativesUMovieSceneAnimationTrackRecorderSettings()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_NoRegister()
	{
		return UMovieSceneAnimationTrackRecorderSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneAnimationTrackRecorderEditorSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeTrackRecorders,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "Animation Recorder Settings" },
		{ "IncludePath", "TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ModuleRelativePath", "Public/TrackRecorders/MovieSceneAnimationTrackRecorderSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneAnimationTrackRecorderSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::ClassParams = {
		&UMovieSceneAnimationTrackRecorderSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneAnimationTrackRecorderSettings, 1558213468);
	template<> TAKETRACKRECORDERS_API UClass* StaticClass<UMovieSceneAnimationTrackRecorderSettings>()
	{
		return UMovieSceneAnimationTrackRecorderSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneAnimationTrackRecorderSettings(Z_Construct_UClass_UMovieSceneAnimationTrackRecorderSettings, &UMovieSceneAnimationTrackRecorderSettings::StaticClass, TEXT("/Script/TakeTrackRecorders"), TEXT("UMovieSceneAnimationTrackRecorderSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneAnimationTrackRecorderSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
