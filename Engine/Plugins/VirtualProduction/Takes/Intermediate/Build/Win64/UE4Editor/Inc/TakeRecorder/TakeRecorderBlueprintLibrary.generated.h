// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMovieSceneMarkedFrame;
class ULevelSequence;
class UTakeRecorderPanel;
class UTakeRecorder;
struct FTakeRecorderParameters;
class UTakeRecorderSources;
class UTakeMetaData;
#ifdef TAKERECORDER_TakeRecorderBlueprintLibrary_generated_h
#error "TakeRecorderBlueprintLibrary.generated.h already included, missing '#pragma once' in TakeRecorderBlueprintLibrary.h"
#endif
#define TAKERECORDER_TakeRecorderBlueprintLibrary_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_89_DELEGATE \
struct TakeRecorderBlueprintLibrary_eventOnTakeRecorderMarkedFrameAdded_Parms \
{ \
	FMovieSceneMarkedFrame MarkedFrame; \
}; \
static inline void FOnTakeRecorderMarkedFrameAdded_DelegateWrapper(const FScriptDelegate& OnTakeRecorderMarkedFrameAdded, FMovieSceneMarkedFrame const& MarkedFrame) \
{ \
	TakeRecorderBlueprintLibrary_eventOnTakeRecorderMarkedFrameAdded_Parms Parms; \
	Parms.MarkedFrame=MarkedFrame; \
	OnTakeRecorderMarkedFrameAdded.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_88_DELEGATE \
static inline void FOnTakeRecorderCancelled_DelegateWrapper(const FScriptDelegate& OnTakeRecorderCancelled) \
{ \
	OnTakeRecorderCancelled.ProcessDelegate<UObject>(NULL); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_87_DELEGATE \
struct TakeRecorderBlueprintLibrary_eventOnTakeRecorderFinished_Parms \
{ \
	ULevelSequence* SequenceAsset; \
}; \
static inline void FOnTakeRecorderFinished_DelegateWrapper(const FScriptDelegate& OnTakeRecorderFinished, ULevelSequence* SequenceAsset) \
{ \
	TakeRecorderBlueprintLibrary_eventOnTakeRecorderFinished_Parms Parms; \
	Parms.SequenceAsset=SequenceAsset; \
	OnTakeRecorderFinished.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_86_DELEGATE \
static inline void FOnTakeRecorderStopped_DelegateWrapper(const FScriptDelegate& OnTakeRecorderStopped) \
{ \
	OnTakeRecorderStopped.ProcessDelegate<UObject>(NULL); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_85_DELEGATE \
static inline void FOnTakeRecorderStarted_DelegateWrapper(const FScriptDelegate& OnTakeRecorderStarted) \
{ \
	OnTakeRecorderStarted.ProcessDelegate<UObject>(NULL); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_84_DELEGATE \
static inline void FOnTakeRecorderPreInitialize_DelegateWrapper(const FScriptDelegate& OnTakeRecorderPreInitialize) \
{ \
	OnTakeRecorderPreInitialize.ProcessDelegate<UObject>(NULL); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_83_DELEGATE \
static inline void FOnTakeRecorderPanelChanged_DelegateWrapper(const FScriptDelegate& OnTakeRecorderPanelChanged) \
{ \
	OnTakeRecorderPanelChanged.ProcessDelegate<UObject>(NULL); \
}


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetOnTakeRecorderMarkedFrameAdded); \
	DECLARE_FUNCTION(execSetOnTakeRecorderCancelled); \
	DECLARE_FUNCTION(execSetOnTakeRecorderFinished); \
	DECLARE_FUNCTION(execSetOnTakeRecorderStopped); \
	DECLARE_FUNCTION(execSetOnTakeRecorderStarted); \
	DECLARE_FUNCTION(execSetOnTakeRecorderPreInitialize); \
	DECLARE_FUNCTION(execSetOnTakeRecorderPanelChanged); \
	DECLARE_FUNCTION(execOpenTakeRecorderPanel); \
	DECLARE_FUNCTION(execGetTakeRecorderPanel); \
	DECLARE_FUNCTION(execStopRecording); \
	DECLARE_FUNCTION(execGetActiveRecorder); \
	DECLARE_FUNCTION(execIsRecording); \
	DECLARE_FUNCTION(execGetDefaultParameters); \
	DECLARE_FUNCTION(execStartRecording); \
	DECLARE_FUNCTION(execIsTakeRecorderEnabled);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetOnTakeRecorderMarkedFrameAdded); \
	DECLARE_FUNCTION(execSetOnTakeRecorderCancelled); \
	DECLARE_FUNCTION(execSetOnTakeRecorderFinished); \
	DECLARE_FUNCTION(execSetOnTakeRecorderStopped); \
	DECLARE_FUNCTION(execSetOnTakeRecorderStarted); \
	DECLARE_FUNCTION(execSetOnTakeRecorderPreInitialize); \
	DECLARE_FUNCTION(execSetOnTakeRecorderPanelChanged); \
	DECLARE_FUNCTION(execOpenTakeRecorderPanel); \
	DECLARE_FUNCTION(execGetTakeRecorderPanel); \
	DECLARE_FUNCTION(execStopRecording); \
	DECLARE_FUNCTION(execGetActiveRecorder); \
	DECLARE_FUNCTION(execIsRecording); \
	DECLARE_FUNCTION(execGetDefaultParameters); \
	DECLARE_FUNCTION(execStartRecording); \
	DECLARE_FUNCTION(execIsTakeRecorderEnabled);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UTakeRecorderBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderBlueprintLibrary)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UTakeRecorderBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderBlueprintLibrary)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderBlueprintLibrary(UTakeRecorderBlueprintLibrary&&); \
	NO_API UTakeRecorderBlueprintLibrary(const UTakeRecorderBlueprintLibrary&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderBlueprintLibrary(UTakeRecorderBlueprintLibrary&&); \
	NO_API UTakeRecorderBlueprintLibrary(const UTakeRecorderBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderBlueprintLibrary)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDER_API UClass* StaticClass<class UTakeRecorderBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorderBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
