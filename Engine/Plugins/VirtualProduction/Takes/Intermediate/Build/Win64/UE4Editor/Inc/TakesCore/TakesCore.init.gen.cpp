// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakesCore_init() {}
	TAKESCORE_API UFunction* Z_Construct_UDelegateFunction_UTakesCoreBlueprintLibrary_OnTakeRecorderSlateChanged__DelegateSignature();
	TAKESCORE_API UFunction* Z_Construct_UDelegateFunction_UTakesCoreBlueprintLibrary_OnTakeRecorderTakeNumberChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TakesCore()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakesCoreBlueprintLibrary_OnTakeRecorderSlateChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakesCoreBlueprintLibrary_OnTakeRecorderTakeNumberChanged__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/TakesCore",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000100,
				0x0C6BB893,
				0x4A001440,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
