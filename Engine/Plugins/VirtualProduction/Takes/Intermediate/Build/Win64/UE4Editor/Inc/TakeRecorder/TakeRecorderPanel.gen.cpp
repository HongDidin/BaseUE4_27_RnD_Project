// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorder/Public/Recorder/TakeRecorderPanel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderPanel() {}
// Cross Module References
	TAKERECORDER_API UEnum* Z_Construct_UEnum_TakeRecorder_ETakeRecorderPanelMode();
	UPackage* Z_Construct_UPackage__Script_TakeRecorder();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderPanel_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderPanel();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSources_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeMetaData_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakePreset_NoRegister();
// End Cross Module References
	static UEnum* ETakeRecorderPanelMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TakeRecorder_ETakeRecorderPanelMode, Z_Construct_UPackage__Script_TakeRecorder(), TEXT("ETakeRecorderPanelMode"));
		}
		return Singleton;
	}
	template<> TAKERECORDER_API UEnum* StaticEnum<ETakeRecorderPanelMode>()
	{
		return ETakeRecorderPanelMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETakeRecorderPanelMode(ETakeRecorderPanelMode_StaticEnum, TEXT("/Script/TakeRecorder"), TEXT("ETakeRecorderPanelMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TakeRecorder_ETakeRecorderPanelMode_Hash() { return 4199230173U; }
	UEnum* Z_Construct_UEnum_TakeRecorder_ETakeRecorderPanelMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorder();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETakeRecorderPanelMode"), 0, Get_Z_Construct_UEnum_TakeRecorder_ETakeRecorderPanelMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETakeRecorderPanelMode::NewRecording", (int64)ETakeRecorderPanelMode::NewRecording },
				{ "ETakeRecorderPanelMode::RecordingInto", (int64)ETakeRecorderPanelMode::RecordingInto },
				{ "ETakeRecorderPanelMode::EditingPreset", (int64)ETakeRecorderPanelMode::EditingPreset },
				{ "ETakeRecorderPanelMode::ReviewingRecording", (int64)ETakeRecorderPanelMode::ReviewingRecording },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "EditingPreset.Comment", "/** The panel is editing a Take Preset asset */" },
				{ "EditingPreset.Name", "ETakeRecorderPanelMode::EditingPreset" },
				{ "EditingPreset.ToolTip", "The panel is editing a Take Preset asset" },
				{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
				{ "NewRecording.Comment", "/** The panel is setting up a new recording */" },
				{ "NewRecording.Name", "ETakeRecorderPanelMode::NewRecording" },
				{ "NewRecording.ToolTip", "The panel is setting up a new recording" },
				{ "RecordingInto.Comment", "/** The panel is setting up recording into an existing level sequence */" },
				{ "RecordingInto.Name", "ETakeRecorderPanelMode::RecordingInto" },
				{ "RecordingInto.ToolTip", "The panel is setting up recording into an existing level sequence" },
				{ "ReviewingRecording.Comment", "/** The panel is reviewing a previously recorded take */" },
				{ "ReviewingRecording.Name", "ETakeRecorderPanelMode::ReviewingRecording" },
				{ "ReviewingRecording.ToolTip", "The panel is reviewing a previously recorded take" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TakeRecorder,
				nullptr,
				"ETakeRecorderPanelMode",
				"ETakeRecorderPanelMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execCanStartRecording)
	{
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutErrorText);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanStartRecording(Z_Param_Out_OutErrorText);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execStopRecording)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopRecording();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execStartRecording)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartRecording();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execGetSources)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTakeRecorderSources**)Z_Param__Result=P_THIS->GetSources();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetFrameRateFromTimecode)
	{
		P_GET_UBOOL(Z_Param_bInFromTimecode);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFrameRateFromTimecode(Z_Param_bInFromTimecode);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetFrameRate)
	{
		P_GET_STRUCT(FFrameRate,Z_Param_InFrameRate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFrameRate(Z_Param_InFrameRate);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execGetFrameRate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FFrameRate*)Z_Param__Result=P_THIS->GetFrameRate();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execGetTakeMetaData)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTakeMetaData**)Z_Param__Result=P_THIS->GetTakeMetaData();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execGetLastRecordedLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSequence**)Z_Param__Result=P_THIS->GetLastRecordedLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execGetLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSequence**)Z_Param__Result=P_THIS->GetLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execClearPendingTake)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearPendingTake();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execNewTake)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NewTake();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetupForViewing)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequenceAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupForViewing(Z_Param_LevelSequenceAsset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetupForEditing)
	{
		P_GET_OBJECT(UTakePreset,Z_Param_TakePreset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupForEditing(Z_Param_TakePreset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetupForRecordingInto_LevelSequence)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequenceAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupForRecordingInto_LevelSequence(Z_Param_LevelSequenceAsset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetupForRecording_LevelSequence)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequenceAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupForRecording_LevelSequence(Z_Param_LevelSequenceAsset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execSetupForRecording_TakePreset)
	{
		P_GET_OBJECT(UTakePreset,Z_Param_TakePresetAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupForRecording_TakePreset(Z_Param_TakePresetAsset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderPanel::execGetMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ETakeRecorderPanelMode*)Z_Param__Result=P_THIS->GetMode();
		P_NATIVE_END;
	}
	void UTakeRecorderPanel::StaticRegisterNativesUTakeRecorderPanel()
	{
		UClass* Class = UTakeRecorderPanel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CanStartRecording", &UTakeRecorderPanel::execCanStartRecording },
			{ "ClearPendingTake", &UTakeRecorderPanel::execClearPendingTake },
			{ "GetFrameRate", &UTakeRecorderPanel::execGetFrameRate },
			{ "GetLastRecordedLevelSequence", &UTakeRecorderPanel::execGetLastRecordedLevelSequence },
			{ "GetLevelSequence", &UTakeRecorderPanel::execGetLevelSequence },
			{ "GetMode", &UTakeRecorderPanel::execGetMode },
			{ "GetSources", &UTakeRecorderPanel::execGetSources },
			{ "GetTakeMetaData", &UTakeRecorderPanel::execGetTakeMetaData },
			{ "NewTake", &UTakeRecorderPanel::execNewTake },
			{ "SetFrameRate", &UTakeRecorderPanel::execSetFrameRate },
			{ "SetFrameRateFromTimecode", &UTakeRecorderPanel::execSetFrameRateFromTimecode },
			{ "SetupForEditing", &UTakeRecorderPanel::execSetupForEditing },
			{ "SetupForRecording_LevelSequence", &UTakeRecorderPanel::execSetupForRecording_LevelSequence },
			{ "SetupForRecording_TakePreset", &UTakeRecorderPanel::execSetupForRecording_TakePreset },
			{ "SetupForRecordingInto_LevelSequence", &UTakeRecorderPanel::execSetupForRecordingInto_LevelSequence },
			{ "SetupForViewing", &UTakeRecorderPanel::execSetupForViewing },
			{ "StartRecording", &UTakeRecorderPanel::execStartRecording },
			{ "StopRecording", &UTakeRecorderPanel::execStopRecording },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics
	{
		struct TakeRecorderPanel_eventCanStartRecording_Parms
		{
			FText OutErrorText;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutErrorText;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::NewProp_OutErrorText = { "OutErrorText", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventCanStartRecording_Parms, OutErrorText), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TakeRecorderPanel_eventCanStartRecording_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TakeRecorderPanel_eventCanStartRecording_Parms), &Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::NewProp_OutErrorText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Whether the panel is ready to start recording\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Whether the panel is ready to start recording" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "CanStartRecording", nullptr, nullptr, sizeof(TakeRecorderPanel_eventCanStartRecording_Parms), Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/*\n\x09 * Clear the pending take level sequence\n\x09 */" },
		{ "DisplayName", "Clear Pending Take" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "* Clear the pending take level sequence" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "ClearPendingTake", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics
	{
		struct FFrameRate
		{
			int32 Numerator;
			int32 Denominator;
		};

		struct TakeRecorderPanel_eventGetFrameRate_Parms
		{
			FFrameRate ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventGetFrameRate_Parms, ReturnValue), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Access the frame rate for this take\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Access the frame rate for this take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "GetFrameRate", nullptr, nullptr, sizeof(TakeRecorderPanel_eventGetFrameRate_Parms), Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics
	{
		struct TakeRecorderPanel_eventGetLastRecordedLevelSequence_Parms
		{
			ULevelSequence* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventGetLastRecordedLevelSequence_Parms, ReturnValue), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Access the last level sequence that was recorded\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Access the last level sequence that was recorded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "GetLastRecordedLevelSequence", nullptr, nullptr, sizeof(TakeRecorderPanel_eventGetLastRecordedLevelSequence_Parms), Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics
	{
		struct TakeRecorderPanel_eventGetLevelSequence_Parms
		{
			ULevelSequence* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventGetLevelSequence_Parms, ReturnValue), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Access the level sequence for this take\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Access the level sequence for this take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "GetLevelSequence", nullptr, nullptr, sizeof(TakeRecorderPanel_eventGetLevelSequence_Parms), Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics
	{
		struct TakeRecorderPanel_eventGetMode_Parms
		{
			ETakeRecorderPanelMode ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventGetMode_Parms, ReturnValue), Z_Construct_UEnum_TakeRecorder_ETakeRecorderPanelMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Get the mode that the panel is currently in\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Get the mode that the panel is currently in" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "GetMode", nullptr, nullptr, sizeof(TakeRecorderPanel_eventGetMode_Parms), Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_GetMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_GetMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics
	{
		struct TakeRecorderPanel_eventGetSources_Parms
		{
			UTakeRecorderSources* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventGetSources_Parms, ReturnValue), Z_Construct_UClass_UTakeRecorderSources_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Access the sources that are to be (or were) used for recording this take\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Access the sources that are to be (or were) used for recording this take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "GetSources", nullptr, nullptr, sizeof(TakeRecorderPanel_eventGetSources_Parms), Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_GetSources()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_GetSources_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics
	{
		struct TakeRecorderPanel_eventGetTakeMetaData_Parms
		{
			UTakeMetaData* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventGetTakeMetaData_Parms, ReturnValue), Z_Construct_UClass_UTakeMetaData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Access take meta data for this take\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Access take meta data for this take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "GetTakeMetaData", nullptr, nullptr, sizeof(TakeRecorderPanel_eventGetTakeMetaData_Parms), Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_NewTake_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_NewTake_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Please use ClearPendingTake instead" },
		{ "DisplayName", "New Take" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_NewTake_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "NewTake", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_NewTake_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_NewTake_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_NewTake()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_NewTake_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics
	{
		struct FFrameRate
		{
			int32 Numerator;
			int32 Denominator;
		};

		struct TakeRecorderPanel_eventSetFrameRate_Parms
		{
			FFrameRate InFrameRate;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFrameRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::NewProp_InFrameRate = { "InFrameRate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventSetFrameRate_Parms, InFrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::NewProp_InFrameRate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09* Set the frame rate for this take\n\x09*/" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Set the frame rate for this take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetFrameRate", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetFrameRate_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics
	{
		struct TakeRecorderPanel_eventSetFrameRateFromTimecode_Parms
		{
			bool bInFromTimecode;
		};
		static void NewProp_bInFromTimecode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInFromTimecode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::NewProp_bInFromTimecode_SetBit(void* Obj)
	{
		((TakeRecorderPanel_eventSetFrameRateFromTimecode_Parms*)Obj)->bInFromTimecode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::NewProp_bInFromTimecode = { "bInFromTimecode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TakeRecorderPanel_eventSetFrameRateFromTimecode_Parms), &Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::NewProp_bInFromTimecode_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::NewProp_bInFromTimecode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09* Set if the frame rate is set from the Timecode frame rate\n\x09*/" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Set if the frame rate is set from the Timecode frame rate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetFrameRateFromTimecode", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetFrameRateFromTimecode_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics
	{
		struct TakeRecorderPanel_eventSetupForEditing_Parms
		{
			UTakePreset* TakePreset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TakePreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::NewProp_TakePreset = { "TakePreset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventSetupForEditing_Parms, TakePreset), Z_Construct_UClass_UTakePreset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::NewProp_TakePreset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Setup this panel as an editor for the specified take preset asset.\n\x09 */" },
		{ "DisplayName", "Set Mode (Editing Take Preset)" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Setup this panel as an editor for the specified take preset asset." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetupForEditing", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetupForEditing_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics
	{
		struct TakeRecorderPanel_eventSetupForRecording_LevelSequence_Parms
		{
			ULevelSequence* LevelSequenceAsset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequenceAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::NewProp_LevelSequenceAsset = { "LevelSequenceAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventSetupForRecording_LevelSequence_Parms, LevelSequenceAsset), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::NewProp_LevelSequenceAsset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Setup this panel such that it is ready to start recording using the specified\n\x09 * level sequence asset as a template for the recording.\n\x09 */" },
		{ "DisplayName", "Set Mode (Recording w/ Level Sequence)" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Setup this panel such that it is ready to start recording using the specified\nlevel sequence asset as a template for the recording." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetupForRecording_LevelSequence", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetupForRecording_LevelSequence_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics
	{
		struct TakeRecorderPanel_eventSetupForRecording_TakePreset_Parms
		{
			UTakePreset* TakePresetAsset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TakePresetAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::NewProp_TakePresetAsset = { "TakePresetAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventSetupForRecording_TakePreset_Parms, TakePresetAsset), Z_Construct_UClass_UTakePreset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::NewProp_TakePresetAsset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Setup this panel such that it is ready to start recording using the specified\n\x09 * take preset as a template for the recording.\n\x09 */" },
		{ "DisplayName", "Set Mode (Recording w/ Take Preset)" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Setup this panel such that it is ready to start recording using the specified\ntake preset as a template for the recording." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetupForRecording_TakePreset", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetupForRecording_TakePreset_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics
	{
		struct TakeRecorderPanel_eventSetupForRecordingInto_LevelSequence_Parms
		{
			ULevelSequence* LevelSequenceAsset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequenceAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::NewProp_LevelSequenceAsset = { "LevelSequenceAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventSetupForRecordingInto_LevelSequence_Parms, LevelSequenceAsset), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::NewProp_LevelSequenceAsset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Setup this panel such that it is ready to start recording using the specified\n\x09 * level sequence asset to record into.\n\x09 */" },
		{ "DisplayName", "Set Mode (Recording into Level Sequence)" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Setup this panel such that it is ready to start recording using the specified\nlevel sequence asset to record into." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetupForRecordingInto_LevelSequence", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetupForRecordingInto_LevelSequence_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics
	{
		struct TakeRecorderPanel_eventSetupForViewing_Parms
		{
			ULevelSequence* LevelSequenceAsset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequenceAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::NewProp_LevelSequenceAsset = { "LevelSequenceAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderPanel_eventSetupForViewing_Parms, LevelSequenceAsset), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::NewProp_LevelSequenceAsset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Setup this panel as a viewer for a previously recorded take.\n\x09 */" },
		{ "DisplayName", "Set Mode (Read-Only Level Sequence)" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Setup this panel as a viewer for a previously recorded take." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "SetupForViewing", nullptr, nullptr, sizeof(TakeRecorderPanel_eventSetupForViewing_Parms), Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_StartRecording_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_StartRecording_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Start recording with the current take\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Start recording with the current take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_StartRecording_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "StartRecording", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_StartRecording_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_StartRecording_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_StartRecording()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_StartRecording_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderPanel_StopRecording_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderPanel_StopRecording_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder|Panel" },
		{ "Comment", "/**\n\x09 * Stop recording with the current take\n\x09 */" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Stop recording with the current take" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderPanel_StopRecording_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderPanel, nullptr, "StopRecording", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderPanel_StopRecording_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderPanel_StopRecording_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderPanel_StopRecording()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderPanel_StopRecording_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTakeRecorderPanel_NoRegister()
	{
		return UTakeRecorderPanel::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderPanel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderPanel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTakeRecorderPanel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTakeRecorderPanel_CanStartRecording, "CanStartRecording" }, // 1649558311
		{ &Z_Construct_UFunction_UTakeRecorderPanel_ClearPendingTake, "ClearPendingTake" }, // 1148539470
		{ &Z_Construct_UFunction_UTakeRecorderPanel_GetFrameRate, "GetFrameRate" }, // 357073707
		{ &Z_Construct_UFunction_UTakeRecorderPanel_GetLastRecordedLevelSequence, "GetLastRecordedLevelSequence" }, // 2853402595
		{ &Z_Construct_UFunction_UTakeRecorderPanel_GetLevelSequence, "GetLevelSequence" }, // 1676065136
		{ &Z_Construct_UFunction_UTakeRecorderPanel_GetMode, "GetMode" }, // 144590633
		{ &Z_Construct_UFunction_UTakeRecorderPanel_GetSources, "GetSources" }, // 112675782
		{ &Z_Construct_UFunction_UTakeRecorderPanel_GetTakeMetaData, "GetTakeMetaData" }, // 1205987111
		{ &Z_Construct_UFunction_UTakeRecorderPanel_NewTake, "NewTake" }, // 2268698444
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRate, "SetFrameRate" }, // 362664178
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetFrameRateFromTimecode, "SetFrameRateFromTimecode" }, // 3216249978
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetupForEditing, "SetupForEditing" }, // 3577569719
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_LevelSequence, "SetupForRecording_LevelSequence" }, // 1427801169
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecording_TakePreset, "SetupForRecording_TakePreset" }, // 3273654871
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetupForRecordingInto_LevelSequence, "SetupForRecordingInto_LevelSequence" }, // 690952848
		{ &Z_Construct_UFunction_UTakeRecorderPanel_SetupForViewing, "SetupForViewing" }, // 2404908079
		{ &Z_Construct_UFunction_UTakeRecorderPanel_StartRecording, "StartRecording" }, // 1602292687
		{ &Z_Construct_UFunction_UTakeRecorderPanel_StopRecording, "StopRecording" }, // 534057125
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderPanel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Take recorder UI panel interop object\n */" },
		{ "IncludePath", "Recorder/TakeRecorderPanel.h" },
		{ "ModuleRelativePath", "Public/Recorder/TakeRecorderPanel.h" },
		{ "ToolTip", "Take recorder UI panel interop object" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderPanel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderPanel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderPanel_Statics::ClassParams = {
		&UTakeRecorderPanel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderPanel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderPanel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderPanel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderPanel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderPanel, 2345729248);
	template<> TAKERECORDER_API UClass* StaticClass<UTakeRecorderPanel>()
	{
		return UTakeRecorderPanel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderPanel(Z_Construct_UClass_UTakeRecorderPanel, &UTakeRecorderPanel::StaticClass, TEXT("/Script/TakeRecorder"), TEXT("UTakeRecorderPanel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderPanel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
