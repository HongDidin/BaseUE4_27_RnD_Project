// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorder/Public/TakeRecorderOverlayWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderOverlayWidget() {}
// Cross Module References
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderOverlayWidget_NoRegister();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorderOverlayWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_TakeRecorder();
	TAKERECORDER_API UClass* Z_Construct_UClass_UTakeRecorder_NoRegister();
// End Cross Module References
	void UTakeRecorderOverlayWidget::StaticRegisterNativesUTakeRecorderOverlayWidget()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderOverlayWidget_NoRegister()
	{
		return UTakeRecorderOverlayWidget::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Recorder_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Recorder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorder,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "TakeRecorderOverlayWidget.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/TakeRecorderOverlayWidget.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::NewProp_Recorder_MetaData[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/** The recorder that this overlay is reflecting */" },
		{ "ModuleRelativePath", "Public/TakeRecorderOverlayWidget.h" },
		{ "ToolTip", "The recorder that this overlay is reflecting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::NewProp_Recorder = { "Recorder", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderOverlayWidget, Recorder), Z_Construct_UClass_UTakeRecorder_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::NewProp_Recorder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::NewProp_Recorder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::NewProp_Recorder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderOverlayWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::ClassParams = {
		&UTakeRecorderOverlayWidget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderOverlayWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderOverlayWidget, 2750087440);
	template<> TAKERECORDER_API UClass* StaticClass<UTakeRecorderOverlayWidget>()
	{
		return UTakeRecorderOverlayWidget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderOverlayWidget(Z_Construct_UClass_UTakeRecorderOverlayWidget, &UTakeRecorderOverlayWidget::StaticClass, TEXT("/Script/TakeRecorder"), TEXT("UTakeRecorderOverlayWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderOverlayWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
