// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKERECORDER_TakeRecorderOverlayWidget_generated_h
#error "TakeRecorderOverlayWidget.generated.h already included, missing '#pragma once' in TakeRecorderOverlayWidget.h"
#endif
#define TAKERECORDER_TakeRecorderOverlayWidget_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderOverlayWidget(); \
	friend struct Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderOverlayWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderOverlayWidget)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderOverlayWidget(); \
	friend struct Z_Construct_UClass_UTakeRecorderOverlayWidget_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderOverlayWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderOverlayWidget)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderOverlayWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderOverlayWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderOverlayWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderOverlayWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderOverlayWidget(UTakeRecorderOverlayWidget&&); \
	NO_API UTakeRecorderOverlayWidget(const UTakeRecorderOverlayWidget&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderOverlayWidget(UTakeRecorderOverlayWidget&&); \
	NO_API UTakeRecorderOverlayWidget(const UTakeRecorderOverlayWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderOverlayWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderOverlayWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderOverlayWidget)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Recorder() { return STRUCT_OFFSET(UTakeRecorderOverlayWidget, Recorder); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDER_API UClass* StaticClass<class UTakeRecorderOverlayWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_TakeRecorderOverlayWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
