// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderPlayerSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderPlayerSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderPlayerSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderPlayerSource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
// End Cross Module References
	void UTakeRecorderPlayerSource::StaticRegisterNativesUTakeRecorderPlayerSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderPlayerSource_NoRegister()
	{
		return UTakeRecorderPlayerSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderPlayerSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Actors" },
		{ "Comment", "/** A recording source that records the current player */" },
		{ "IncludePath", "TakeRecorderPlayerSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderPlayerSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Player" },
		{ "ToolTip", "A recording source that records the current player" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderPlayerSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::ClassParams = {
		&UTakeRecorderPlayerSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderPlayerSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderPlayerSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderPlayerSource, 2403222030);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderPlayerSource>()
	{
		return UTakeRecorderPlayerSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderPlayerSource(Z_Construct_UClass_UTakeRecorderPlayerSource, &UTakeRecorderPlayerSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderPlayerSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderPlayerSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
