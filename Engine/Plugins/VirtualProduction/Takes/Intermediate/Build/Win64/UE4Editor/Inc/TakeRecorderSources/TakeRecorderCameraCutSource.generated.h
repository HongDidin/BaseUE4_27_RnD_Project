// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKERECORDERSOURCES_TakeRecorderCameraCutSource_generated_h
#error "TakeRecorderCameraCutSource.generated.h already included, missing '#pragma once' in TakeRecorderCameraCutSource.h"
#endif
#define TAKERECORDERSOURCES_TakeRecorderCameraCutSource_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderCameraCutSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderCameraCutSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderCameraCutSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderCameraCutSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderCameraCutSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderCameraCutSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderCameraCutSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderCameraCutSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderCameraCutSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderCameraCutSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderCameraCutSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderCameraCutSource(UTakeRecorderCameraCutSource&&); \
	NO_API UTakeRecorderCameraCutSource(const UTakeRecorderCameraCutSource&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderCameraCutSource(UTakeRecorderCameraCutSource&&); \
	NO_API UTakeRecorderCameraCutSource(const UTakeRecorderCameraCutSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderCameraCutSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderCameraCutSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderCameraCutSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__World() { return STRUCT_OFFSET(UTakeRecorderCameraCutSource, World); } \
	FORCEINLINE static uint32 __PPO__MasterLevelSequence() { return STRUCT_OFFSET(UTakeRecorderCameraCutSource, MasterLevelSequence); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_17_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h_21_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDERSOURCES_API UClass* StaticClass<class UTakeRecorderCameraCutSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderCameraCutSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
