// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ETakeRecorderState : uint8;
class ULevelSequence;
#ifdef TAKERECORDER_TakeRecorder_generated_h
#error "TakeRecorder.generated.h already included, missing '#pragma once' in TakeRecorder.h"
#endif
#define TAKERECORDER_TakeRecorder_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetState); \
	DECLARE_FUNCTION(execGetSequence); \
	DECLARE_FUNCTION(execGetCountdownSeconds);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetState); \
	DECLARE_FUNCTION(execGetSequence); \
	DECLARE_FUNCTION(execGetCountdownSeconds);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorder(); \
	friend struct Z_Construct_UClass_UTakeRecorder_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorder(); \
	friend struct Z_Construct_UClass_UTakeRecorder_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorder(UTakeRecorder&&); \
	NO_API UTakeRecorder(const UTakeRecorder&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorder(UTakeRecorder&&); \
	NO_API UTakeRecorder(const UTakeRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorder)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SequenceAsset() { return STRUCT_OFFSET(UTakeRecorder, SequenceAsset); } \
	FORCEINLINE static uint32 __PPO__OverlayWidget() { return STRUCT_OFFSET(UTakeRecorder, OverlayWidget); } \
	FORCEINLINE static uint32 __PPO__WeakWorld() { return STRUCT_OFFSET(UTakeRecorder, WeakWorld); } \
	FORCEINLINE static uint32 __PPO__Parameters() { return STRUCT_OFFSET(UTakeRecorder, Parameters); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_52_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h_57_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDER_API UClass* StaticClass<class UTakeRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorder_Public_Recorder_TakeRecorder_h


#define FOREACH_ENUM_ETAKERECORDERSTATE(op) \
	op(ETakeRecorderState::CountingDown) \
	op(ETakeRecorderState::PreRecord) \
	op(ETakeRecorderState::TickingAfterPre) \
	op(ETakeRecorderState::Started) \
	op(ETakeRecorderState::Stopped) \
	op(ETakeRecorderState::Cancelled) 

enum class ETakeRecorderState : uint8;
template<> TAKERECORDER_API UEnum* StaticEnum<ETakeRecorderState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
