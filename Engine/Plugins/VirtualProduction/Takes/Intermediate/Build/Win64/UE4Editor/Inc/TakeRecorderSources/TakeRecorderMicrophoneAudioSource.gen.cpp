// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderMicrophoneAudioSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderMicrophoneAudioSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource();
// End Cross Module References
	void UTakeRecorderMicrophoneAudioSourceSettings::StaticRegisterNativesUTakeRecorderMicrophoneAudioSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_NoRegister()
	{
		return UTakeRecorderMicrophoneAudioSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioTrackName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_AudioTrackName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioSubDirectory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AudioSubDirectory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A recording source that records microphone audio */" },
		{ "DisplayName", "Microphone Audio Recorder" },
		{ "IncludePath", "TakeRecorderMicrophoneAudioSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A recording source that records microphone audio" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioTrackName_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Name of the recorded audio track name */" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ToolTip", "Name of the recorded audio track name" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioTrackName = { "AudioTrackName", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderMicrophoneAudioSourceSettings, AudioTrackName), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioTrackName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioTrackName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioSubDirectory_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** The name of the subdirectory audio will be placed in. Leave this empty to place into the same directory as the sequence base path */" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ToolTip", "The name of the subdirectory audio will be placed in. Leave this empty to place into the same directory as the sequence base path" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioSubDirectory = { "AudioSubDirectory", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderMicrophoneAudioSourceSettings, AudioSubDirectory), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioSubDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioSubDirectory_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioTrackName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::NewProp_AudioSubDirectory,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderMicrophoneAudioSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::ClassParams = {
		&UTakeRecorderMicrophoneAudioSourceSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::PropPointers),
		0,
		0x000000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderMicrophoneAudioSourceSettings, 2964508629);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderMicrophoneAudioSourceSettings>()
	{
		return UTakeRecorderMicrophoneAudioSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderMicrophoneAudioSourceSettings(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings, &UTakeRecorderMicrophoneAudioSourceSettings::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderMicrophoneAudioSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderMicrophoneAudioSourceSettings);
	void UTakeRecorderMicrophoneAudioSource::StaticRegisterNativesUTakeRecorderMicrophoneAudioSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_NoRegister()
	{
		return UTakeRecorderMicrophoneAudioSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioGain_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AudioGain;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSplitAudioChannelsIntoSeparateTracks_MetaData[];
#endif
		static void NewProp_bSplitAudioChannelsIntoSeparateTracks_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSplitAudioChannelsIntoSeparateTracks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReplaceRecordedAudio_MetaData[];
#endif
		static void NewProp_bReplaceRecordedAudio_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReplaceRecordedAudio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderMicrophoneAudioSourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** A recording source that records microphone audio */" },
		{ "IncludePath", "TakeRecorderMicrophoneAudioSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Microphone Audio" },
		{ "ToolTip", "A recording source that records microphone audio" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_AudioGain_MetaData[] = {
		{ "Category", "Source" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Gain in decibels to apply to recorded audio */" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ToolTip", "Gain in decibels to apply to recorded audio" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_AudioGain = { "AudioGain", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderMicrophoneAudioSource, AudioGain), METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_AudioGain_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_AudioGain_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Whether or not to split mic channels into separate audio tracks. If not true, a max of 2 input channels is supported. */" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ToolTip", "Whether or not to split mic channels into separate audio tracks. If not true, a max of 2 input channels is supported." },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks_SetBit(void* Obj)
	{
		((UTakeRecorderMicrophoneAudioSource*)Obj)->bSplitAudioChannelsIntoSeparateTracks = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks = { "bSplitAudioChannelsIntoSeparateTracks", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderMicrophoneAudioSource), &Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Replace existing recorded audio with any newly recorded audio */" },
		{ "ModuleRelativePath", "Private/TakeRecorderMicrophoneAudioSource.h" },
		{ "ToolTip", "Replace existing recorded audio with any newly recorded audio" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio_SetBit(void* Obj)
	{
		((UTakeRecorderMicrophoneAudioSource*)Obj)->bReplaceRecordedAudio = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio = { "bReplaceRecordedAudio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderMicrophoneAudioSource), &Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_AudioGain,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bSplitAudioChannelsIntoSeparateTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::NewProp_bReplaceRecordedAudio,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderMicrophoneAudioSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::ClassParams = {
		&UTakeRecorderMicrophoneAudioSource::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderMicrophoneAudioSource, 990591284);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderMicrophoneAudioSource>()
	{
		return UTakeRecorderMicrophoneAudioSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderMicrophoneAudioSource(Z_Construct_UClass_UTakeRecorderMicrophoneAudioSource, &UTakeRecorderMicrophoneAudioSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderMicrophoneAudioSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderMicrophoneAudioSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
