// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderActorSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderActorSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UEnum* Z_Construct_UEnum_TakeRecorderSources_ETakeRecorderActorRecordType();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderActorSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderActorSource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSources_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource_NoRegister();
	TAKESCORE_API UClass* Z_Construct_UClass_UActorRecorderPropertyMap_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneTrackRecorder_NoRegister();
// End Cross Module References
	static UEnum* ETakeRecorderActorRecordType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TakeRecorderSources_ETakeRecorderActorRecordType, Z_Construct_UPackage__Script_TakeRecorderSources(), TEXT("ETakeRecorderActorRecordType"));
		}
		return Singleton;
	}
	template<> TAKERECORDERSOURCES_API UEnum* StaticEnum<ETakeRecorderActorRecordType>()
	{
		return ETakeRecorderActorRecordType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETakeRecorderActorRecordType(ETakeRecorderActorRecordType_StaticEnum, TEXT("/Script/TakeRecorderSources"), TEXT("ETakeRecorderActorRecordType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TakeRecorderSources_ETakeRecorderActorRecordType_Hash() { return 1616361029U; }
	UEnum* Z_Construct_UEnum_TakeRecorderSources_ETakeRecorderActorRecordType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TakeRecorderSources();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETakeRecorderActorRecordType"), 0, Get_Z_Construct_UEnum_TakeRecorderSources_ETakeRecorderActorRecordType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETakeRecorderActorRecordType::Possessable", (int64)ETakeRecorderActorRecordType::Possessable },
				{ "ETakeRecorderActorRecordType::Spawnable", (int64)ETakeRecorderActorRecordType::Spawnable },
				{ "ETakeRecorderActorRecordType::ProjectDefault", (int64)ETakeRecorderActorRecordType::ProjectDefault },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
				{ "Possessable.Name", "ETakeRecorderActorRecordType::Possessable" },
				{ "ProjectDefault.Name", "ETakeRecorderActorRecordType::ProjectDefault" },
				{ "Spawnable.Name", "ETakeRecorderActorRecordType::Spawnable" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TakeRecorderSources,
				nullptr,
				"ETakeRecorderActorRecordType",
				"ETakeRecorderActorRecordType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UTakeRecorderActorSource::execGetSourceActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSoftObjectPtr<AActor>*)Z_Param__Result=P_THIS->GetSourceActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderActorSource::execSetSourceActor)
	{
		P_GET_SOFTOBJECT(TSoftObjectPtr<AActor>,Z_Param_InTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSourceActor(Z_Param_InTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderActorSource::execRemoveActorFromSources)
	{
		P_GET_OBJECT(AActor,Z_Param_InActor);
		P_GET_OBJECT(UTakeRecorderSources,Z_Param_InSources);
		P_FINISH;
		P_NATIVE_BEGIN;
		UTakeRecorderActorSource::RemoveActorFromSources(Z_Param_InActor,Z_Param_InSources);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTakeRecorderActorSource::execAddSourceForActor)
	{
		P_GET_OBJECT(AActor,Z_Param_InActor);
		P_GET_OBJECT(UTakeRecorderSources,Z_Param_InSources);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTakeRecorderSource**)Z_Param__Result=UTakeRecorderActorSource::AddSourceForActor(Z_Param_InActor,Z_Param_InSources);
		P_NATIVE_END;
	}
	void UTakeRecorderActorSource::StaticRegisterNativesUTakeRecorderActorSource()
	{
		UClass* Class = UTakeRecorderActorSource::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddSourceForActor", &UTakeRecorderActorSource::execAddSourceForActor },
			{ "GetSourceActor", &UTakeRecorderActorSource::execGetSourceActor },
			{ "RemoveActorFromSources", &UTakeRecorderActorSource::execRemoveActorFromSources },
			{ "SetSourceActor", &UTakeRecorderActorSource::execSetSourceActor },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics
	{
		struct TakeRecorderActorSource_eventAddSourceForActor_Parms
		{
			AActor* InActor;
			UTakeRecorderSources* InSources;
			UTakeRecorderSource* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSources;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::NewProp_InActor = { "InActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventAddSourceForActor_Parms, InActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::NewProp_InSources = { "InSources", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventAddSourceForActor_Parms, InSources), Z_Construct_UClass_UTakeRecorderSources_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventAddSourceForActor_Parms, ReturnValue), Z_Construct_UClass_UTakeRecorderSource_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::NewProp_InActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::NewProp_InSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/*\n\x09 * Add a take recorder source for the given actor. \n\x09 *\n\x09 * @param InActor The actor to add a source for\n\x09 * @param InSources The sources to add the actor to\n\x09 * @return The added source or the source already present with the same actor\n\x09 */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "* Add a take recorder source for the given actor.\n*\n* @param InActor The actor to add a source for\n* @param InSources The sources to add the actor to\n* @return The added source or the source already present with the same actor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderActorSource, nullptr, "AddSourceForActor", nullptr, nullptr, sizeof(TakeRecorderActorSource_eventAddSourceForActor_Parms), Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics
	{
		struct TakeRecorderActorSource_eventGetSourceActor_Parms
		{
			TSoftObjectPtr<AActor> ReturnValue;
		};
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventGetSourceActor_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder Actor Source" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderActorSource, nullptr, "GetSourceActor", nullptr, nullptr, sizeof(TakeRecorderActorSource_eventGetSourceActor_Parms), Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics
	{
		struct TakeRecorderActorSource_eventRemoveActorFromSources_Parms
		{
			AActor* InActor;
			UTakeRecorderSources* InSources;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSources;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::NewProp_InActor = { "InActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventRemoveActorFromSources_Parms, InActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::NewProp_InSources = { "InSources", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventRemoveActorFromSources_Parms, InSources), Z_Construct_UClass_UTakeRecorderSources_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::NewProp_InActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::NewProp_InSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder" },
		{ "Comment", "/*\n\x09 * Remove the given actor from TakeRecorderSources.\n\x09 *\n\x09 * @param InActor The actor to remove from the sources\n\x09 * @param InSources The sources from where to remove the actor\n\x09 */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "* Remove the given actor from TakeRecorderSources.\n*\n* @param InActor The actor to remove from the sources\n* @param InSources The sources from where to remove the actor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderActorSource, nullptr, "RemoveActorFromSources", nullptr, nullptr, sizeof(TakeRecorderActorSource_eventRemoveActorFromSources_Parms), Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics
	{
		struct TakeRecorderActorSource_eventSetSourceActor_Parms
		{
			TSoftObjectPtr<AActor> InTarget;
		};
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_InTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::NewProp_InTarget = { "InTarget", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TakeRecorderActorSource_eventSetSourceActor_Parms, InTarget), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::NewProp_InTarget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Take Recorder Actor Source" },
		{ "Comment", "/** Set the Target actor that we are going to record. Will reset the Recorded Property Map to defaults. */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "Set the Target actor that we are going to record. Will reset the Recorded Property Map to defaults." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderActorSource, nullptr, "SetSourceActor", nullptr, nullptr, sizeof(TakeRecorderActorSource_eventSetSourceActor_Parms), Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTakeRecorderActorSource_NoRegister()
	{
		return UTakeRecorderActorSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderActorSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_Target;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RecordType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecordType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RecordType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordParentHierarchy_MetaData[];
#endif
		static void NewProp_bRecordParentHierarchy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordParentHierarchy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReduceKeys_MetaData[];
#endif
		static void NewProp_bReduceKeys_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReduceKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecordedProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RecordedProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetLevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetLevelSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterLevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MasterLevelSequence;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FactorySettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FactorySettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FactorySettings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackRecorders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackRecorders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TrackRecorders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderActorSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTakeRecorderActorSource_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTakeRecorderActorSource_AddSourceForActor, "AddSourceForActor" }, // 3694205814
		{ &Z_Construct_UFunction_UTakeRecorderActorSource_GetSourceActor, "GetSourceActor" }, // 2325772792
		{ &Z_Construct_UFunction_UTakeRecorderActorSource_RemoveActorFromSources, "RemoveActorFromSources" }, // 1365235625
		{ &Z_Construct_UFunction_UTakeRecorderActorSource_SetSourceActor, "SetSourceActor" }, // 2260329174
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Actors" },
		{ "Comment", "/**\n* This Take Recorder Source can record an actor from the World's properties.\n* Records the properties of the actor and the components on the actor and safely\n* handles new components being spawned at runtime and the actor being destroyed.\n*/" },
		{ "IncludePath", "TakeRecorderActorSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Any Actor" },
		{ "ToolTip", "This Take Recorder Source can record an actor from the World's properties.\nRecords the properties of the actor and the components on the actor and safely\nhandles new components being spawned at runtime and the actor being destroyed." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_Target_MetaData[] = {
		{ "Category", "Actor Source" },
		{ "Comment", "/** Reference to the actor in the world that should have it's properties recorded. */" },
		{ "DisplayName", "Source Actor" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "Reference to the actor in the world that should have it's properties recorded." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_Target_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType_MetaData[] = {
		{ "Category", "Actor Source" },
		{ "Comment", "/**\n\x09 * Should this actor be recorded as a Possessable in Sequencer? If so the resulting Object Binding\x09\n\x09 * will not create a Spawnable copy of this object and instead will possess this object in the level.\n\x09 */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "Should this actor be recorded as a Possessable in Sequencer? If so the resulting Object Binding\nwill not create a Spawnable copy of this object and instead will possess this object in the level." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType = { "RecordType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, RecordType), Z_Construct_UEnum_TakeRecorderSources_ETakeRecorderActorRecordType, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy_MetaData[] = {
		{ "Category", "Actor Source" },
		{ "Comment", "/** \n\x09 * Whether to ensure that the parent hierarchy is also recorded. If recording to possessable and the parent is not recorded, \n\x09 * the recorded transforms will be in local space since the child will still be attached to the parent in the level after \n\x09 * recording.  If recording to spawnable and the parent is not recorded, the recorded transforms will be in global space \n\x09 * since the child will not be attached to the parent in the level.\n\x09 */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "Whether to ensure that the parent hierarchy is also recorded. If recording to possessable and the parent is not recorded,\nthe recorded transforms will be in local space since the child will still be attached to the parent in the level after\nrecording.  If recording to spawnable and the parent is not recorded, the recorded transforms will be in global space\nsince the child will not be attached to the parent in the level." },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy_SetBit(void* Obj)
	{
		((UTakeRecorderActorSource*)Obj)->bRecordParentHierarchy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy = { "bRecordParentHierarchy", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderActorSource), &Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys_MetaData[] = {
		{ "Category", "Actor Source" },
		{ "Comment", "/** Whether to perform key-reduction algorithms as part of the recording */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "Whether to perform key-reduction algorithms as part of the recording" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys_SetBit(void* Obj)
	{
		((UTakeRecorderActorSource*)Obj)->bReduceKeys = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys = { "bReduceKeys", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderActorSource), &Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordedProperties_MetaData[] = {
		{ "Category", "Actor Source" },
		{ "Comment", "/**\n\x09 * Lists the properties and components on the current actor and whether or not each property will be\n\x09 * recorded into a track in the resulting Level Sequence. \n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ShowInnerProperties", "" },
		{ "ToolTip", "Lists the properties and components on the current actor and whether or not each property will be\nrecorded into a track in the resulting Level Sequence." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordedProperties = { "RecordedProperties", nullptr, (EPropertyFlags)0x001200000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, RecordedProperties), Z_Construct_UClass_UActorRecorderPropertyMap_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordedProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordedProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TargetLevelSequence_MetaData[] = {
		{ "Comment", "/** The level sequence that this source is being recorded into. Set during PreRecording, null after PostRecording. */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "The level sequence that this source is being recorded into. Set during PreRecording, null after PostRecording." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TargetLevelSequence = { "TargetLevelSequence", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, TargetLevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TargetLevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TargetLevelSequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_MasterLevelSequence_MetaData[] = {
		{ "Comment", "/** The master or uppermost level sequence that this source is being recorded into. Set during PreRecording, null after PostRecording. */" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "The master or uppermost level sequence that this source is being recorded into. Set during PreRecording, null after PostRecording." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_MasterLevelSequence = { "MasterLevelSequence", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, MasterLevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_MasterLevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_MasterLevelSequence_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings_Inner = { "FactorySettings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings_MetaData[] = {
		{ "Comment", "/**\n\x09* Dynamically created list of settings objects for the different factories that are recording something \n\x09* on this actor. If a Factory has no properties it can record the settings objects will not get created.\n\x09* Only one instance of this object exists for a factory and the factory recorder will be passed the shared \n\x09* instance.\n\x09*/" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "Dynamically created list of settings objects for the different factories that are recording something\non this actor. If a Factory has no properties it can record the settings objects will not get created.\nOnly one instance of this object exists for a factory and the factory recorder will be passed the shared\ninstance." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings = { "FactorySettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, FactorySettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders_Inner = { "TrackRecorders", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneTrackRecorder_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders_MetaData[] = {
		{ "Comment", "/**\n\x09* An array of section recorders created during the recording process that are capturing data about the actor/components.\n\x09* Will be an empty list when a recording is not in progress.\n\x09*/" },
		{ "ModuleRelativePath", "Private/TakeRecorderActorSource.h" },
		{ "ToolTip", "An array of section recorders created during the recording process that are capturing data about the actor/components.\nWill be an empty list when a recording is not in progress." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders = { "TrackRecorders", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderActorSource, TrackRecorders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderActorSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bRecordParentHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_bReduceKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_RecordedProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TargetLevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_MasterLevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_FactorySettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderActorSource_Statics::NewProp_TrackRecorders,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderActorSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderActorSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderActorSource_Statics::ClassParams = {
		&UTakeRecorderActorSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTakeRecorderActorSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderActorSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderActorSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderActorSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderActorSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderActorSource, 2774971004);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderActorSource>()
	{
		return UTakeRecorderActorSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderActorSource(Z_Construct_UClass_UTakeRecorderActorSource, &UTakeRecorderActorSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderActorSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderActorSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
