// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderLevelSequenceSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderLevelSequenceSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderLevelSequenceSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderLevelSequenceSource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
// End Cross Module References
	void UTakeRecorderLevelSequenceSource::StaticRegisterNativesUTakeRecorderLevelSequenceSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderLevelSequenceSource_NoRegister()
	{
		return UTakeRecorderLevelSequenceSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequencesToTrigger_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSequencesToTrigger_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LevelSequencesToTrigger;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Other" },
		{ "Comment", "/** Plays level sequence actors when recording starts */" },
		{ "IncludePath", "TakeRecorderLevelSequenceSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderLevelSequenceSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "Level Sequence" },
		{ "ToolTip", "Plays level sequence actors when recording starts" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger_Inner = { "LevelSequencesToTrigger", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger_MetaData[] = {
		{ "Category", "Source" },
		{ "ModuleRelativePath", "Private/TakeRecorderLevelSequenceSource.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger = { "LevelSequencesToTrigger", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderLevelSequenceSource, LevelSequencesToTrigger), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::NewProp_LevelSequencesToTrigger,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderLevelSequenceSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::ClassParams = {
		&UTakeRecorderLevelSequenceSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderLevelSequenceSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderLevelSequenceSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderLevelSequenceSource, 3067257632);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderLevelSequenceSource>()
	{
		return UTakeRecorderLevelSequenceSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderLevelSequenceSource(Z_Construct_UClass_UTakeRecorderLevelSequenceSource, &UTakeRecorderLevelSequenceSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderLevelSequenceSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderLevelSequenceSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
