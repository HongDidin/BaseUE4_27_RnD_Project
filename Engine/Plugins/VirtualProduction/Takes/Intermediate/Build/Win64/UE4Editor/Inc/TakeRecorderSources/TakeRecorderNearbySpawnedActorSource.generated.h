// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKERECORDERSOURCES_TakeRecorderNearbySpawnedActorSource_generated_h
#error "TakeRecorderNearbySpawnedActorSource.generated.h already included, missing '#pragma once' in TakeRecorderNearbySpawnedActorSource.h"
#endif
#define TAKERECORDERSOURCES_TakeRecorderNearbySpawnedActorSource_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderNearbySpawnedActorSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderNearbySpawnedActorSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderNearbySpawnedActorSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderNearbySpawnedActorSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderNearbySpawnedActorSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderNearbySpawnedActorSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderNearbySpawnedActorSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderNearbySpawnedActorSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderNearbySpawnedActorSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderNearbySpawnedActorSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderNearbySpawnedActorSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderNearbySpawnedActorSource(UTakeRecorderNearbySpawnedActorSource&&); \
	NO_API UTakeRecorderNearbySpawnedActorSource(const UTakeRecorderNearbySpawnedActorSource&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderNearbySpawnedActorSource(UTakeRecorderNearbySpawnedActorSource&&); \
	NO_API UTakeRecorderNearbySpawnedActorSource(const UTakeRecorderNearbySpawnedActorSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderNearbySpawnedActorSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderNearbySpawnedActorSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderNearbySpawnedActorSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDERSOURCES_API UClass* StaticClass<class UTakeRecorderNearbySpawnedActorSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderNearbySpawnedActorSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
