// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ULevel;
class UTakePreset;
struct FFrameRate;
struct FFrameTime;
struct FTimecode;
struct FDateTime;
#ifdef TAKESCORE_TakeMetaData_generated_h
#error "TakeMetaData.generated.h already included, missing '#pragma once' in TakeMetaData.h"
#endif
#define TAKESCORE_TakeMetaData_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetFrameRateFromTimecode); \
	DECLARE_FUNCTION(execSetLevelOrigin); \
	DECLARE_FUNCTION(execSetPresetOrigin); \
	DECLARE_FUNCTION(execSetDescription); \
	DECLARE_FUNCTION(execSetFrameRate); \
	DECLARE_FUNCTION(execSetDuration); \
	DECLARE_FUNCTION(execSetTimecodeOut); \
	DECLARE_FUNCTION(execSetTimecodeIn); \
	DECLARE_FUNCTION(execSetTimestamp); \
	DECLARE_FUNCTION(execSetTakeNumber); \
	DECLARE_FUNCTION(execSetSlate); \
	DECLARE_FUNCTION(execGenerateAssetPath); \
	DECLARE_FUNCTION(execUnlock); \
	DECLARE_FUNCTION(execLock); \
	DECLARE_FUNCTION(execGetFrameRateFromTimecode); \
	DECLARE_FUNCTION(execGetLevelOrigin); \
	DECLARE_FUNCTION(execGetLevelPath); \
	DECLARE_FUNCTION(execGetPresetOrigin); \
	DECLARE_FUNCTION(execGetDescription); \
	DECLARE_FUNCTION(execGetFrameRate); \
	DECLARE_FUNCTION(execGetDuration); \
	DECLARE_FUNCTION(execGetTimecodeOut); \
	DECLARE_FUNCTION(execGetTimecodeIn); \
	DECLARE_FUNCTION(execGetTimestamp); \
	DECLARE_FUNCTION(execGetTakeNumber); \
	DECLARE_FUNCTION(execGetSlate); \
	DECLARE_FUNCTION(execRecorded); \
	DECLARE_FUNCTION(execIsLocked);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetFrameRateFromTimecode); \
	DECLARE_FUNCTION(execSetLevelOrigin); \
	DECLARE_FUNCTION(execSetPresetOrigin); \
	DECLARE_FUNCTION(execSetDescription); \
	DECLARE_FUNCTION(execSetFrameRate); \
	DECLARE_FUNCTION(execSetDuration); \
	DECLARE_FUNCTION(execSetTimecodeOut); \
	DECLARE_FUNCTION(execSetTimecodeIn); \
	DECLARE_FUNCTION(execSetTimestamp); \
	DECLARE_FUNCTION(execSetTakeNumber); \
	DECLARE_FUNCTION(execSetSlate); \
	DECLARE_FUNCTION(execGenerateAssetPath); \
	DECLARE_FUNCTION(execUnlock); \
	DECLARE_FUNCTION(execLock); \
	DECLARE_FUNCTION(execGetFrameRateFromTimecode); \
	DECLARE_FUNCTION(execGetLevelOrigin); \
	DECLARE_FUNCTION(execGetLevelPath); \
	DECLARE_FUNCTION(execGetPresetOrigin); \
	DECLARE_FUNCTION(execGetDescription); \
	DECLARE_FUNCTION(execGetFrameRate); \
	DECLARE_FUNCTION(execGetDuration); \
	DECLARE_FUNCTION(execGetTimecodeOut); \
	DECLARE_FUNCTION(execGetTimecodeIn); \
	DECLARE_FUNCTION(execGetTimestamp); \
	DECLARE_FUNCTION(execGetTakeNumber); \
	DECLARE_FUNCTION(execGetSlate); \
	DECLARE_FUNCTION(execRecorded); \
	DECLARE_FUNCTION(execIsLocked);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeMetaData(); \
	friend struct Z_Construct_UClass_UTakeMetaData_Statics; \
public: \
	DECLARE_CLASS(UTakeMetaData, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakeMetaData) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \
 \
	virtual UObject* _getUObject() const override { return const_cast<UTakeMetaData*>(this); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUTakeMetaData(); \
	friend struct Z_Construct_UClass_UTakeMetaData_Statics; \
public: \
	DECLARE_CLASS(UTakeMetaData, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakeMetaData) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorSettings");} \
 \
	virtual UObject* _getUObject() const override { return const_cast<UTakeMetaData*>(this); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeMetaData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeMetaData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeMetaData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeMetaData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeMetaData(UTakeMetaData&&); \
	NO_API UTakeMetaData(const UTakeMetaData&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeMetaData(UTakeMetaData&&); \
	NO_API UTakeMetaData(const UTakeMetaData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeMetaData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeMetaData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeMetaData)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsLocked() { return STRUCT_OFFSET(UTakeMetaData, bIsLocked); } \
	FORCEINLINE static uint32 __PPO__Slate() { return STRUCT_OFFSET(UTakeMetaData, Slate); } \
	FORCEINLINE static uint32 __PPO__TakeNumber() { return STRUCT_OFFSET(UTakeMetaData, TakeNumber); } \
	FORCEINLINE static uint32 __PPO__Timestamp() { return STRUCT_OFFSET(UTakeMetaData, Timestamp); } \
	FORCEINLINE static uint32 __PPO__TimecodeIn() { return STRUCT_OFFSET(UTakeMetaData, TimecodeIn); } \
	FORCEINLINE static uint32 __PPO__TimecodeOut() { return STRUCT_OFFSET(UTakeMetaData, TimecodeOut); } \
	FORCEINLINE static uint32 __PPO__Duration() { return STRUCT_OFFSET(UTakeMetaData, Duration); } \
	FORCEINLINE static uint32 __PPO__FrameRate() { return STRUCT_OFFSET(UTakeMetaData, FrameRate); } \
	FORCEINLINE static uint32 __PPO__Description() { return STRUCT_OFFSET(UTakeMetaData, Description); } \
	FORCEINLINE static uint32 __PPO__PresetOrigin() { return STRUCT_OFFSET(UTakeMetaData, PresetOrigin); } \
	FORCEINLINE static uint32 __PPO__LevelOrigin() { return STRUCT_OFFSET(UTakeMetaData, LevelOrigin); } \
	FORCEINLINE static uint32 __PPO__bFrameRateFromTimecode() { return STRUCT_OFFSET(UTakeMetaData, bFrameRateFromTimecode); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_18_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h_22_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKESCORE_API UClass* StaticClass<class UTakeMetaData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeMetaData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
