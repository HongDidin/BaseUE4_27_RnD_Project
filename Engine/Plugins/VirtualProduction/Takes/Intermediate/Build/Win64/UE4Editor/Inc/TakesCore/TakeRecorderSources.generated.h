// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTakeRecorderSource;
struct FQualifiedFrameTime;
#ifdef TAKESCORE_TakeRecorderSources_generated_h
#error "TakeRecorderSources.generated.h already included, missing '#pragma once' in TakeRecorderSources.h"
#endif
#define TAKESCORE_TakeRecorderSources_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStartRecordingSource); \
	DECLARE_FUNCTION(execGetSourcesCopy); \
	DECLARE_FUNCTION(execRemoveSource); \
	DECLARE_FUNCTION(execAddSource);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStartRecordingSource); \
	DECLARE_FUNCTION(execGetSourcesCopy); \
	DECLARE_FUNCTION(execRemoveSource); \
	DECLARE_FUNCTION(execAddSource);


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderSources(); \
	friend struct Z_Construct_UClass_UTakeRecorderSources_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderSources, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderSources)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderSources(); \
	friend struct Z_Construct_UClass_UTakeRecorderSources_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderSources, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderSources)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderSources(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderSources) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderSources); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderSources); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderSources(UTakeRecorderSources&&); \
	NO_API UTakeRecorderSources(const UTakeRecorderSources&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderSources(UTakeRecorderSources&&); \
	NO_API UTakeRecorderSources(const UTakeRecorderSources&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderSources); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderSources); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderSources)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sources() { return STRUCT_OFFSET(UTakeRecorderSources, Sources); } \
	FORCEINLINE static uint32 __PPO__SourceSubSequenceMap() { return STRUCT_OFFSET(UTakeRecorderSources, SourceSubSequenceMap); } \
	FORCEINLINE static uint32 __PPO__ActiveSubSections() { return STRUCT_OFFSET(UTakeRecorderSources, ActiveSubSections); }


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_32_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h_36_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKESCORE_API UClass* StaticClass<class UTakeRecorderSources>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSources_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
