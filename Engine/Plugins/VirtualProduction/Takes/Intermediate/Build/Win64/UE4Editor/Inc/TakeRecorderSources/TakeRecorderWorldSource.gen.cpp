// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TakeRecorderSources/Private/TakeRecorderWorldSource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderWorldSource() {}
// Cross Module References
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderWorldSourceSettings_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderWorldSourceSettings();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	UPackage* Z_Construct_UPackage__Script_TakeRecorderSources();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderWorldSource_NoRegister();
	TAKERECORDERSOURCES_API UClass* Z_Construct_UClass_UTakeRecorderWorldSource();
// End Cross Module References
	void UTakeRecorderWorldSourceSettings::StaticRegisterNativesUTakeRecorderWorldSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderWorldSourceSettings_NoRegister()
	{
		return UTakeRecorderWorldSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordWorldSettings_MetaData[];
#endif
		static void NewProp_bRecordWorldSettings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordWorldSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutotrackActors_MetaData[];
#endif
		static void NewProp_bAutotrackActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutotrackActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A recording source that records world state */" },
		{ "DisplayName", "World Recorder" },
		{ "IncludePath", "TakeRecorderWorldSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderWorldSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A recording source that records world state" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Record world settings */" },
		{ "ModuleRelativePath", "Private/TakeRecorderWorldSource.h" },
		{ "ToolTip", "Record world settings" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings_SetBit(void* Obj)
	{
		((UTakeRecorderWorldSourceSettings*)Obj)->bRecordWorldSettings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings = { "bRecordWorldSettings", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderWorldSourceSettings), &Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Add a binding and track for all actors that aren't explicitly being recorded */" },
		{ "ModuleRelativePath", "Private/TakeRecorderWorldSource.h" },
		{ "ToolTip", "Add a binding and track for all actors that aren't explicitly being recorded" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors_SetBit(void* Obj)
	{
		((UTakeRecorderWorldSourceSettings*)Obj)->bAutotrackActors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors = { "bAutotrackActors", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderWorldSourceSettings), &Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bRecordWorldSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::NewProp_bAutotrackActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderWorldSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::ClassParams = {
		&UTakeRecorderWorldSourceSettings::StaticClass,
		"EditorSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::PropPointers),
		0,
		0x000000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderWorldSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderWorldSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderWorldSourceSettings, 2171686429);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderWorldSourceSettings>()
	{
		return UTakeRecorderWorldSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderWorldSourceSettings(Z_Construct_UClass_UTakeRecorderWorldSourceSettings, &UTakeRecorderWorldSourceSettings::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderWorldSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderWorldSourceSettings);
	void UTakeRecorderWorldSource::StaticRegisterNativesUTakeRecorderWorldSource()
	{
	}
	UClass* Z_Construct_UClass_UTakeRecorderWorldSource_NoRegister()
	{
		return UTakeRecorderWorldSource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderWorldSource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderWorldSource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderWorldSourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_TakeRecorderSources,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderWorldSource_Statics::Class_MetaDataParams[] = {
		{ "Category", "Actors" },
		{ "Comment", "/** A recording source that records world state */" },
		{ "IncludePath", "TakeRecorderWorldSource.h" },
		{ "ModuleRelativePath", "Private/TakeRecorderWorldSource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "World" },
		{ "ToolTip", "A recording source that records world state" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderWorldSource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderWorldSource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderWorldSource_Statics::ClassParams = {
		&UTakeRecorderWorldSource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderWorldSource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderWorldSource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderWorldSource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderWorldSource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderWorldSource, 2061578761);
	template<> TAKERECORDERSOURCES_API UClass* StaticClass<UTakeRecorderWorldSource>()
	{
		return UTakeRecorderWorldSource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderWorldSource(Z_Construct_UClass_UTakeRecorderWorldSource, &UTakeRecorderWorldSource::StaticClass, TEXT("/Script/TakeRecorderSources"), TEXT("UTakeRecorderWorldSource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderWorldSource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
