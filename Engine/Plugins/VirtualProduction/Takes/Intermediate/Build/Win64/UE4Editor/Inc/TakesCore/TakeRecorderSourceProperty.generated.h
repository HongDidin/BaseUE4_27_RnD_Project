// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKESCORE_TakeRecorderSourceProperty_generated_h
#error "TakeRecorderSourceProperty.generated.h already included, missing '#pragma once' in TakeRecorderSourceProperty.h"
#endif
#define TAKESCORE_TakeRecorderSourceProperty_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActorRecordedProperty_Statics; \
	static class UScriptStruct* StaticStruct();


template<> TAKESCORE_API UScriptStruct* StaticStruct<struct FActorRecordedProperty>();

#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorRecorderPropertyMap(); \
	friend struct Z_Construct_UClass_UActorRecorderPropertyMap_Statics; \
public: \
	DECLARE_CLASS(UActorRecorderPropertyMap, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UActorRecorderPropertyMap)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUActorRecorderPropertyMap(); \
	friend struct Z_Construct_UClass_UActorRecorderPropertyMap_Statics; \
public: \
	DECLARE_CLASS(UActorRecorderPropertyMap, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakesCore"), NO_API) \
	DECLARE_SERIALIZER(UActorRecorderPropertyMap)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorRecorderPropertyMap(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorRecorderPropertyMap) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorRecorderPropertyMap); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorRecorderPropertyMap); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorRecorderPropertyMap(UActorRecorderPropertyMap&&); \
	NO_API UActorRecorderPropertyMap(const UActorRecorderPropertyMap&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorRecorderPropertyMap(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorRecorderPropertyMap(UActorRecorderPropertyMap&&); \
	NO_API UActorRecorderPropertyMap(const UActorRecorderPropertyMap&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorRecorderPropertyMap); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorRecorderPropertyMap); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorRecorderPropertyMap)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_44_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKESCORE_API UClass* StaticClass<class UActorRecorderPropertyMap>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakesCore_Public_TakeRecorderSourceProperty_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
