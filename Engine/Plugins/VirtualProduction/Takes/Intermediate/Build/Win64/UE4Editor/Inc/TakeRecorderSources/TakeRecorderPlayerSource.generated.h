// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TAKERECORDERSOURCES_TakeRecorderPlayerSource_generated_h
#error "TakeRecorderPlayerSource.generated.h already included, missing '#pragma once' in TakeRecorderPlayerSource.h"
#endif
#define TAKERECORDERSOURCES_TakeRecorderPlayerSource_generated_h

#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTakeRecorderPlayerSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderPlayerSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderPlayerSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderPlayerSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTakeRecorderPlayerSource(); \
	friend struct Z_Construct_UClass_UTakeRecorderPlayerSource_Statics; \
public: \
	DECLARE_CLASS(UTakeRecorderPlayerSource, UTakeRecorderSource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TakeRecorderSources"), NO_API) \
	DECLARE_SERIALIZER(UTakeRecorderPlayerSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTakeRecorderPlayerSource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderPlayerSource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderPlayerSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderPlayerSource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderPlayerSource(UTakeRecorderPlayerSource&&); \
	NO_API UTakeRecorderPlayerSource(const UTakeRecorderPlayerSource&); \
public:


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTakeRecorderPlayerSource(UTakeRecorderPlayerSource&&); \
	NO_API UTakeRecorderPlayerSource(const UTakeRecorderPlayerSource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTakeRecorderPlayerSource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTakeRecorderPlayerSource); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTakeRecorderPlayerSource)


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TAKERECORDERSOURCES_API UClass* StaticClass<class UTakeRecorderPlayerSource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_Takes_Source_TakeRecorderSources_Private_TakeRecorderPlayerSource_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
