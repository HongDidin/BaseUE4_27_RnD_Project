// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorder_init() {}
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderPanelChanged__DelegateSignature();
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderPreInitialize__DelegateSignature();
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderStarted__DelegateSignature();
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderStopped__DelegateSignature();
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderFinished__DelegateSignature();
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderCancelled__DelegateSignature();
	TAKERECORDER_API UFunction* Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderMarkedFrameAdded__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_TakeRecorder()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderPanelChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderPreInitialize__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderStarted__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderStopped__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderFinished__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderCancelled__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_UTakeRecorderBlueprintLibrary_OnTakeRecorderMarkedFrameAdded__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/TakeRecorder",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000100,
				0x69628296,
				0xBC81BB64,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
