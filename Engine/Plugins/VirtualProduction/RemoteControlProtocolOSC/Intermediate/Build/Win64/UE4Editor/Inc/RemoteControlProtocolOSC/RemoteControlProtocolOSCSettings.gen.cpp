// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlProtocolOSC/Private/RemoteControlProtocolOSCSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolOSCSettings() {}
// Cross Module References
	REMOTECONTROLPROTOCOLOSC_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings();
	UPackage* Z_Construct_UPackage__Script_RemoteControlProtocolOSC();
	REMOTECONTROLPROTOCOLOSC_API UClass* Z_Construct_UClass_URemoteControlProtocolOSCSettings_NoRegister();
	REMOTECONTROLPROTOCOLOSC_API UClass* Z_Construct_UClass_URemoteControlProtocolOSCSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FRemoteControlOSCServerSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLPROTOCOLOSC_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings, Z_Construct_UPackage__Script_RemoteControlProtocolOSC(), TEXT("RemoteControlOSCServerSettings"), sizeof(FRemoteControlOSCServerSettings), Get_Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLPROTOCOLOSC_API UScriptStruct* StaticStruct<FRemoteControlOSCServerSettings>()
{
	return FRemoteControlOSCServerSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlOSCServerSettings(FRemoteControlOSCServerSettings::StaticStruct, TEXT("/Script/RemoteControlProtocolOSC"), TEXT("RemoteControlOSCServerSettings"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlProtocolOSC_StaticRegisterNativesFRemoteControlOSCServerSettings
{
	FScriptStruct_RemoteControlProtocolOSC_StaticRegisterNativesFRemoteControlOSCServerSettings()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlOSCServerSettings>(FName(TEXT("RemoteControlOSCServerSettings")));
	}
} ScriptStruct_RemoteControlProtocolOSC_StaticRegisterNativesFRemoteControlOSCServerSettings;
	struct Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerAddress;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * OSC Remote Control server settings\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSCSettings.h" },
		{ "ToolTip", "OSC Remote Control server settings" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlOSCServerSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::NewProp_ServerAddress_MetaData[] = {
		{ "Category", "OSC" },
		{ "Comment", "/**\n\x09* OSC server IP address\n\x09* \n\x09* The format is IP_ADDRESS:PORT_NUMBER.\n\x09*/" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSCSettings.h" },
		{ "ToolTip", "OSC server IP address\n\nThe format is IP_ADDRESS:PORT_NUMBER." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::NewProp_ServerAddress = { "ServerAddress", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlOSCServerSettings, ServerAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::NewProp_ServerAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::NewProp_ServerAddress_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::NewProp_ServerAddress,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolOSC,
		nullptr,
		&NewStructOps,
		"RemoteControlOSCServerSettings",
		sizeof(FRemoteControlOSCServerSettings),
		alignof(FRemoteControlOSCServerSettings),
		Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolOSC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlOSCServerSettings"), sizeof(FRemoteControlOSCServerSettings), Get_Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Hash() { return 945358240U; }
	void URemoteControlProtocolOSCSettings::StaticRegisterNativesURemoteControlProtocolOSCSettings()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlProtocolOSCSettings_NoRegister()
	{
		return URemoteControlProtocolOSCSettings::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ServersSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServersSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ServersSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolOSC,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * OSC Remote Control settings\n */" },
		{ "IncludePath", "RemoteControlProtocolOSCSettings.h" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSCSettings.h" },
		{ "ToolTip", "OSC Remote Control settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings_Inner = { "ServersSettings", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** OSC server pair of server ip and server port */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSCSettings.h" },
		{ "ToolTip", "OSC server pair of server ip and server port" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings = { "ServersSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlProtocolOSCSettings, ServersSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::NewProp_ServersSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlProtocolOSCSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::ClassParams = {
		&URemoteControlProtocolOSCSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlProtocolOSCSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlProtocolOSCSettings, 2749800509);
	template<> REMOTECONTROLPROTOCOLOSC_API UClass* StaticClass<URemoteControlProtocolOSCSettings>()
	{
		return URemoteControlProtocolOSCSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlProtocolOSCSettings(Z_Construct_UClass_URemoteControlProtocolOSCSettings, &URemoteControlProtocolOSCSettings::StaticClass, TEXT("/Script/RemoteControlProtocolOSC"), TEXT("URemoteControlProtocolOSCSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlProtocolOSCSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
