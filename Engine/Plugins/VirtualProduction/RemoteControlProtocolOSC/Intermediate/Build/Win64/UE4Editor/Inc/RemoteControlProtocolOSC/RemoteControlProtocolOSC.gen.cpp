// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlProtocolOSC/Private/RemoteControlProtocolOSC.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolOSC() {}
// Cross Module References
	REMOTECONTROLPROTOCOLOSC_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity();
	UPackage* Z_Construct_UPackage__Script_RemoteControlProtocolOSC();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolEntity();
// End Cross Module References

static_assert(std::is_polymorphic<FRemoteControlOSCProtocolEntity>() == std::is_polymorphic<FRemoteControlProtocolEntity>(), "USTRUCT FRemoteControlOSCProtocolEntity cannot be polymorphic unless super FRemoteControlProtocolEntity is polymorphic");

class UScriptStruct* FRemoteControlOSCProtocolEntity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLPROTOCOLOSC_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity, Z_Construct_UPackage__Script_RemoteControlProtocolOSC(), TEXT("RemoteControlOSCProtocolEntity"), sizeof(FRemoteControlOSCProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLPROTOCOLOSC_API UScriptStruct* StaticStruct<FRemoteControlOSCProtocolEntity>()
{
	return FRemoteControlOSCProtocolEntity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlOSCProtocolEntity(FRemoteControlOSCProtocolEntity::StaticStruct, TEXT("/Script/RemoteControlProtocolOSC"), TEXT("RemoteControlOSCProtocolEntity"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlProtocolOSC_StaticRegisterNativesFRemoteControlOSCProtocolEntity
{
	FScriptStruct_RemoteControlProtocolOSC_StaticRegisterNativesFRemoteControlOSCProtocolEntity()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlOSCProtocolEntity>(FName(TEXT("RemoteControlOSCProtocolEntity")));
	}
} ScriptStruct_RemoteControlProtocolOSC_StaticRegisterNativesFRemoteControlOSCProtocolEntity;
	struct Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PathName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeInputTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RangeInputTemplate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * OSC protocol entity for remote control binding\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSC.h" },
		{ "ToolTip", "OSC protocol entity for remote control binding" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlOSCProtocolEntity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_PathName_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** OSC address in the form '/Container1/Container2/Method' */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSC.h" },
		{ "ToolTip", "OSC address in the form '/Container1/Container2/Method'" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_PathName = { "PathName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlOSCProtocolEntity, PathName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_PathName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_PathName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData[] = {
		{ "ClampMax", "1.000000" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** OSC range input property template, used for binding. */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolOSC.h" },
		{ "ToolTip", "OSC range input property template, used for binding." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_RangeInputTemplate = { "RangeInputTemplate", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlOSCProtocolEntity, RangeInputTemplate), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_PathName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::NewProp_RangeInputTemplate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolOSC,
		Z_Construct_UScriptStruct_FRemoteControlProtocolEntity,
		&NewStructOps,
		"RemoteControlOSCProtocolEntity",
		sizeof(FRemoteControlOSCProtocolEntity),
		alignof(FRemoteControlOSCProtocolEntity),
		Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolOSC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlOSCProtocolEntity"), sizeof(FRemoteControlOSCProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOSCProtocolEntity_Hash() { return 1940271146U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
