// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLPROTOCOLOSC_RemoteControlProtocolOSCSettings_generated_h
#error "RemoteControlProtocolOSCSettings.generated.h already included, missing '#pragma once' in RemoteControlProtocolOSCSettings.h"
#endif
#define REMOTECONTROLPROTOCOLOSC_RemoteControlProtocolOSCSettings_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlOSCServerSettings_Statics; \
	REMOTECONTROLPROTOCOLOSC_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROLPROTOCOLOSC_API UScriptStruct* StaticStruct<struct FRemoteControlOSCServerSettings>();

#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlProtocolOSCSettings(); \
	friend struct Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics; \
public: \
	DECLARE_CLASS(URemoteControlProtocolOSCSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlProtocolOSC"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlProtocolOSCSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlProtocolOSCSettings(); \
	friend struct Z_Construct_UClass_URemoteControlProtocolOSCSettings_Statics; \
public: \
	DECLARE_CLASS(URemoteControlProtocolOSCSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlProtocolOSC"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlProtocolOSCSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlProtocolOSCSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlProtocolOSCSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlProtocolOSCSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlProtocolOSCSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlProtocolOSCSettings(URemoteControlProtocolOSCSettings&&); \
	NO_API URemoteControlProtocolOSCSettings(const URemoteControlProtocolOSCSettings&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlProtocolOSCSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlProtocolOSCSettings(URemoteControlProtocolOSCSettings&&); \
	NO_API URemoteControlProtocolOSCSettings(const URemoteControlProtocolOSCSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlProtocolOSCSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlProtocolOSCSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlProtocolOSCSettings)


#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_41_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLPROTOCOLOSC_API UClass* StaticClass<class URemoteControlProtocolOSCSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControlProtocolOSC_Source_RemoteControlProtocolOSC_Private_RemoteControlProtocolOSCSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
