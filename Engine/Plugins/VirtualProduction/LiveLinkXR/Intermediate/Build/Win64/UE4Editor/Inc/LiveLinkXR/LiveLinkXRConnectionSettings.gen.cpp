// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkXR/Public/LiveLinkXRConnectionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkXRConnectionSettings() {}
// Cross Module References
	LIVELINKXR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkXR();
// End Cross Module References
class UScriptStruct* FLiveLinkXRConnectionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKXR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings, Z_Construct_UPackage__Script_LiveLinkXR(), TEXT("LiveLinkXRConnectionSettings"), sizeof(FLiveLinkXRConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Hash());
	}
	return Singleton;
}
template<> LIVELINKXR_API UScriptStruct* StaticStruct<FLiveLinkXRConnectionSettings>()
{
	return FLiveLinkXRConnectionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkXRConnectionSettings(FLiveLinkXRConnectionSettings::StaticStruct, TEXT("/Script/LiveLinkXR"), TEXT("LiveLinkXRConnectionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkXR_StaticRegisterNativesFLiveLinkXRConnectionSettings
{
	FScriptStruct_LiveLinkXR_StaticRegisterNativesFLiveLinkXRConnectionSettings()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkXRConnectionSettings>(FName(TEXT("LiveLinkXRConnectionSettings")));
	}
} ScriptStruct_LiveLinkXR_StaticRegisterNativesFLiveLinkXRConnectionSettings;
	struct Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTrackTrackers_MetaData[];
#endif
		static void NewProp_bTrackTrackers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrackTrackers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTrackControllers_MetaData[];
#endif
		static void NewProp_bTrackControllers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrackControllers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTrackHMDs_MetaData[];
#endif
		static void NewProp_bTrackHMDs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTrackHMDs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalUpdateRateInHz_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_LocalUpdateRateInHz;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LiveLinkXRConnectionSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkXRConnectionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** Track all SteamVR tracker pucks */" },
		{ "ModuleRelativePath", "Public/LiveLinkXRConnectionSettings.h" },
		{ "ToolTip", "Track all SteamVR tracker pucks" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers_SetBit(void* Obj)
	{
		((FLiveLinkXRConnectionSettings*)Obj)->bTrackTrackers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers = { "bTrackTrackers", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkXRConnectionSettings), &Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** Track all controllers */" },
		{ "ModuleRelativePath", "Public/LiveLinkXRConnectionSettings.h" },
		{ "ToolTip", "Track all controllers" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers_SetBit(void* Obj)
	{
		((FLiveLinkXRConnectionSettings*)Obj)->bTrackControllers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers = { "bTrackControllers", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkXRConnectionSettings), &Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** Track all HMDs */" },
		{ "ModuleRelativePath", "Public/LiveLinkXRConnectionSettings.h" },
		{ "ToolTip", "Track all HMDs" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs_SetBit(void* Obj)
	{
		((FLiveLinkXRConnectionSettings*)Obj)->bTrackHMDs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs = { "bTrackHMDs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkXRConnectionSettings), &Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_LocalUpdateRateInHz_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Update rate (in Hz) at which to read the tracking data for each device */" },
		{ "ModuleRelativePath", "Public/LiveLinkXRConnectionSettings.h" },
		{ "ToolTip", "Update rate (in Hz) at which to read the tracking data for each device" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_LocalUpdateRateInHz = { "LocalUpdateRateInHz", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkXRConnectionSettings, LocalUpdateRateInHz), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_LocalUpdateRateInHz_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_LocalUpdateRateInHz_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackTrackers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackControllers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_bTrackHMDs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::NewProp_LocalUpdateRateInHz,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkXR,
		nullptr,
		&NewStructOps,
		"LiveLinkXRConnectionSettings",
		sizeof(FLiveLinkXRConnectionSettings),
		alignof(FLiveLinkXRConnectionSettings),
		Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkXR();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkXRConnectionSettings"), sizeof(FLiveLinkXRConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Hash() { return 1799571155U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
