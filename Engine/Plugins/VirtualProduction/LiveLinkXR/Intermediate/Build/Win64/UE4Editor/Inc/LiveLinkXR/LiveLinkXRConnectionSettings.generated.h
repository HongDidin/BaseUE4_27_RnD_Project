// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKXR_LiveLinkXRConnectionSettings_generated_h
#error "LiveLinkXRConnectionSettings.generated.h already included, missing '#pragma once' in LiveLinkXRConnectionSettings.h"
#endif
#define LIVELINKXR_LiveLinkXRConnectionSettings_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRConnectionSettings_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLiveLinkXRConnectionSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LIVELINKXR_API UScriptStruct* StaticStruct<struct FLiveLinkXRConnectionSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRConnectionSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
