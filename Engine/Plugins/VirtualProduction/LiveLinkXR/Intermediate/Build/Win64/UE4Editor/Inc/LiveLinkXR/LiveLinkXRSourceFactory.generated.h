// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKXR_LiveLinkXRSourceFactory_generated_h
#error "LiveLinkXRSourceFactory.generated.h already included, missing '#pragma once' in LiveLinkXRSourceFactory.h"
#endif
#define LIVELINKXR_LiveLinkXRSourceFactory_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkXRSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkXRSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkXRSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkXR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkXRSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkXRSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkXRSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkXRSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkXR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkXRSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkXRSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkXRSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkXRSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkXRSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkXRSourceFactory(ULiveLinkXRSourceFactory&&); \
	NO_API ULiveLinkXRSourceFactory(const ULiveLinkXRSourceFactory&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkXRSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkXRSourceFactory(ULiveLinkXRSourceFactory&&); \
	NO_API ULiveLinkXRSourceFactory(const ULiveLinkXRSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkXRSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkXRSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkXRSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_9_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h_13_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKXR_API UClass* StaticClass<class ULiveLinkXRSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkXR_Source_LiveLinkXR_Public_LiveLinkXRSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
