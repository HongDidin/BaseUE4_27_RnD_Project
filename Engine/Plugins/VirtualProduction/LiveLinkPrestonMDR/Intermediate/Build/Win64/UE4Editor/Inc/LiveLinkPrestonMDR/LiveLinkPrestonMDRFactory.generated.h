// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKPRESTONMDR_LiveLinkPrestonMDRFactory_generated_h
#error "LiveLinkPrestonMDRFactory.generated.h already included, missing '#pragma once' in LiveLinkPrestonMDRFactory.h"
#endif
#define LIVELINKPRESTONMDR_LiveLinkPrestonMDRFactory_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkPrestonMDRSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkPrestonMDRSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPrestonMDRSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkPrestonMDR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPrestonMDRSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkPrestonMDRSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkPrestonMDRSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPrestonMDRSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkPrestonMDR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPrestonMDRSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPrestonMDRSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPrestonMDRSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPrestonMDRSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPrestonMDRSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPrestonMDRSourceFactory(ULiveLinkPrestonMDRSourceFactory&&); \
	NO_API ULiveLinkPrestonMDRSourceFactory(const ULiveLinkPrestonMDRSourceFactory&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPrestonMDRSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPrestonMDRSourceFactory(ULiveLinkPrestonMDRSourceFactory&&); \
	NO_API ULiveLinkPrestonMDRSourceFactory(const ULiveLinkPrestonMDRSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPrestonMDRSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPrestonMDRSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPrestonMDRSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h_20_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKPRESTONMDR_API UClass* StaticClass<class ULiveLinkPrestonMDRSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Public_LiveLinkPrestonMDRFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
