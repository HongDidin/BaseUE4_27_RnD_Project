// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKPRESTONMDR_LiveLinkPrestonMDRRole_generated_h
#error "LiveLinkPrestonMDRRole.generated.h already included, missing '#pragma once' in LiveLinkPrestonMDRRole.h"
#endif
#define LIVELINKPRESTONMDR_LiveLinkPrestonMDRRole_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkPrestonMDRRole(); \
	friend struct Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPrestonMDRRole, ULiveLinkCameraRole, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkPrestonMDR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPrestonMDRRole)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkPrestonMDRRole(); \
	friend struct Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPrestonMDRRole, ULiveLinkCameraRole, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkPrestonMDR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPrestonMDRRole)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPrestonMDRRole(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPrestonMDRRole) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPrestonMDRRole); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPrestonMDRRole); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPrestonMDRRole(ULiveLinkPrestonMDRRole&&); \
	NO_API ULiveLinkPrestonMDRRole(const ULiveLinkPrestonMDRRole&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPrestonMDRRole(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPrestonMDRRole(ULiveLinkPrestonMDRRole&&); \
	NO_API ULiveLinkPrestonMDRRole(const ULiveLinkPrestonMDRRole&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPrestonMDRRole); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPrestonMDRRole); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPrestonMDRRole)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKPRESTONMDR_API UClass* StaticClass<class ULiveLinkPrestonMDRRole>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRRole_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
