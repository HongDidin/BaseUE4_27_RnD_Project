// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKPRESTONMDR_LiveLinkPrestonMDRSourceSettings_generated_h
#error "LiveLinkPrestonMDRSourceSettings.generated.h already included, missing '#pragma once' in LiveLinkPrestonMDRSourceSettings.h"
#endif
#define LIVELINKPRESTONMDR_LiveLinkPrestonMDRSourceSettings_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEncoderRange_Statics; \
	LIVELINKPRESTONMDR_API static class UScriptStruct* StaticStruct();


template<> LIVELINKPRESTONMDR_API UScriptStruct* StaticStruct<struct FEncoderRange>();

#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkPrestonMDRSourceSettings(); \
	friend struct Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPrestonMDRSourceSettings, ULiveLinkSourceSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkPrestonMDR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPrestonMDRSourceSettings)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkPrestonMDRSourceSettings(); \
	friend struct Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkPrestonMDRSourceSettings, ULiveLinkSourceSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkPrestonMDR"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkPrestonMDRSourceSettings)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPrestonMDRSourceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPrestonMDRSourceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPrestonMDRSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPrestonMDRSourceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPrestonMDRSourceSettings(ULiveLinkPrestonMDRSourceSettings&&); \
	NO_API ULiveLinkPrestonMDRSourceSettings(const ULiveLinkPrestonMDRSourceSettings&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkPrestonMDRSourceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkPrestonMDRSourceSettings(ULiveLinkPrestonMDRSourceSettings&&); \
	NO_API ULiveLinkPrestonMDRSourceSettings(const ULiveLinkPrestonMDRSourceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkPrestonMDRSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkPrestonMDRSourceSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkPrestonMDRSourceSettings)


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_33_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h_37_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKPRESTONMDR_API UClass* StaticClass<class ULiveLinkPrestonMDRSourceSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkPrestonMDR_Source_LiveLinkPrestonMDR_Private_LiveLinkPrestonMDRSourceSettings_h


#define FOREACH_ENUM_EFIZDATAMODE(op) \
	op(EFIZDataMode::EncoderData) \
	op(EFIZDataMode::CalibratedData) 

enum class EFIZDataMode : uint8;
template<> LIVELINKPRESTONMDR_API UEnum* StaticEnum<EFIZDataMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
