// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkPrestonMDR/Private/LiveLinkPrestonMDRSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkPrestonMDRSourceSettings() {}
// Cross Module References
	LIVELINKPRESTONMDR_API UEnum* Z_Construct_UEnum_LiveLinkPrestonMDR_EFIZDataMode();
	UPackage* Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
	LIVELINKPRESTONMDR_API UScriptStruct* Z_Construct_UScriptStruct_FEncoderRange();
	LIVELINKPRESTONMDR_API UClass* Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_NoRegister();
	LIVELINKPRESTONMDR_API UClass* Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkSourceSettings();
// End Cross Module References
	static UEnum* EFIZDataMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LiveLinkPrestonMDR_EFIZDataMode, Z_Construct_UPackage__Script_LiveLinkPrestonMDR(), TEXT("EFIZDataMode"));
		}
		return Singleton;
	}
	template<> LIVELINKPRESTONMDR_API UEnum* StaticEnum<EFIZDataMode>()
	{
		return EFIZDataMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFIZDataMode(EFIZDataMode_StaticEnum, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("EFIZDataMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LiveLinkPrestonMDR_EFIZDataMode_Hash() { return 3082704022U; }
	UEnum* Z_Construct_UEnum_LiveLinkPrestonMDR_EFIZDataMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFIZDataMode"), 0, Get_Z_Construct_UEnum_LiveLinkPrestonMDR_EFIZDataMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFIZDataMode::EncoderData", (int64)EFIZDataMode::EncoderData },
				{ "EFIZDataMode::CalibratedData", (int64)EFIZDataMode::CalibratedData },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CalibratedData.Name", "EFIZDataMode::CalibratedData" },
				{ "EncoderData.Name", "EFIZDataMode::EncoderData" },
				{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
				nullptr,
				"EFIZDataMode",
				"EFIZDataMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FEncoderRange::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKPRESTONMDR_API uint32 Get_Z_Construct_UScriptStruct_FEncoderRange_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEncoderRange, Z_Construct_UPackage__Script_LiveLinkPrestonMDR(), TEXT("EncoderRange"), sizeof(FEncoderRange), Get_Z_Construct_UScriptStruct_FEncoderRange_Hash());
	}
	return Singleton;
}
template<> LIVELINKPRESTONMDR_API UScriptStruct* StaticStruct<FEncoderRange>()
{
	return FEncoderRange::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEncoderRange(FEncoderRange::StaticStruct, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("EncoderRange"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFEncoderRange
{
	FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFEncoderRange()
	{
		UScriptStruct::DeferCppStructOps<FEncoderRange>(FName(TEXT("EncoderRange")));
	}
} ScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFEncoderRange;
	struct Z_Construct_UScriptStruct_FEncoderRange_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Max;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEncoderRange_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEncoderRange_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEncoderRange>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "Comment", "/** Minimum raw encoder value */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
		{ "ToolTip", "Minimum raw encoder value" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEncoderRange, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "Comment", "/** Maximum raw encoder value */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
		{ "ToolTip", "Maximum raw encoder value" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEncoderRange, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Max_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEncoderRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEncoderRange_Statics::NewProp_Max,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEncoderRange_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
		nullptr,
		&NewStructOps,
		"EncoderRange",
		sizeof(FEncoderRange),
		alignof(FEncoderRange),
		Z_Construct_UScriptStruct_FEncoderRange_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncoderRange_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEncoderRange_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEncoderRange_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEncoderRange()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEncoderRange_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EncoderRange"), sizeof(FEncoderRange), Get_Z_Construct_UScriptStruct_FEncoderRange_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEncoderRange_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEncoderRange_Hash() { return 3290825261U; }
	void ULiveLinkPrestonMDRSourceSettings::StaticRegisterNativesULiveLinkPrestonMDRSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_NoRegister()
	{
		return ULiveLinkPrestonMDRSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IncomingDataMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IncomingDataMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IncomingDataMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusEncoderRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusEncoderRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IrisEncoderRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IrisEncoderRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZoomEncoderRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ZoomEncoderRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkSourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkPrestonMDRSourceSettings.h" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** The mode in which the Preston MDR is configured to send FIZ data (pre-calibrated or raw encoder positions) */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
		{ "ToolTip", "The mode in which the Preston MDR is configured to send FIZ data (pre-calibrated or raw encoder positions)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode = { "IncomingDataMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPrestonMDRSourceSettings, IncomingDataMode), Z_Construct_UEnum_LiveLinkPrestonMDR_EFIZDataMode, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_FocusEncoderRange_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Raw focus encoder range */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
		{ "ToolTip", "Raw focus encoder range" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_FocusEncoderRange = { "FocusEncoderRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPrestonMDRSourceSettings, FocusEncoderRange), Z_Construct_UScriptStruct_FEncoderRange, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_FocusEncoderRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_FocusEncoderRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IrisEncoderRange_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Raw iris encoder range */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
		{ "ToolTip", "Raw iris encoder range" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IrisEncoderRange = { "IrisEncoderRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPrestonMDRSourceSettings, IrisEncoderRange), Z_Construct_UScriptStruct_FEncoderRange, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IrisEncoderRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IrisEncoderRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_ZoomEncoderRange_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Raw zoom encoder range */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRSourceSettings.h" },
		{ "ToolTip", "Raw zoom encoder range" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_ZoomEncoderRange = { "ZoomEncoderRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkPrestonMDRSourceSettings, ZoomEncoderRange), Z_Construct_UScriptStruct_FEncoderRange, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_ZoomEncoderRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_ZoomEncoderRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IncomingDataMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_FocusEncoderRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_IrisEncoderRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::NewProp_ZoomEncoderRange,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkPrestonMDRSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::ClassParams = {
		&ULiveLinkPrestonMDRSourceSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkPrestonMDRSourceSettings, 1212807779);
	template<> LIVELINKPRESTONMDR_API UClass* StaticClass<ULiveLinkPrestonMDRSourceSettings>()
	{
		return ULiveLinkPrestonMDRSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkPrestonMDRSourceSettings(Z_Construct_UClass_ULiveLinkPrestonMDRSourceSettings, &ULiveLinkPrestonMDRSourceSettings::StaticClass, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("ULiveLinkPrestonMDRSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkPrestonMDRSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
