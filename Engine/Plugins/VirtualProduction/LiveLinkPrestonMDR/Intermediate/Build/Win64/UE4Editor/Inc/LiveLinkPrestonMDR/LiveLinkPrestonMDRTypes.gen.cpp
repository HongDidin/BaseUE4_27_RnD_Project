// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkPrestonMDR/Private/LiveLinkPrestonMDRTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkPrestonMDRTypes() {}
// Cross Module References
	LIVELINKPRESTONMDR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData();
	UPackage* Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkBaseBlueprintData();
	LIVELINKPRESTONMDR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData();
	LIVELINKPRESTONMDR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraFrameData();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraStaticData();
// End Cross Module References

static_assert(std::is_polymorphic<FLiveLinkPrestonMDRBlueprintData>() == std::is_polymorphic<FLiveLinkBaseBlueprintData>(), "USTRUCT FLiveLinkPrestonMDRBlueprintData cannot be polymorphic unless super FLiveLinkBaseBlueprintData is polymorphic");

class UScriptStruct* FLiveLinkPrestonMDRBlueprintData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKPRESTONMDR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData, Z_Construct_UPackage__Script_LiveLinkPrestonMDR(), TEXT("LiveLinkPrestonMDRBlueprintData"), sizeof(FLiveLinkPrestonMDRBlueprintData), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Hash());
	}
	return Singleton;
}
template<> LIVELINKPRESTONMDR_API UScriptStruct* StaticStruct<FLiveLinkPrestonMDRBlueprintData>()
{
	return FLiveLinkPrestonMDRBlueprintData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData(FLiveLinkPrestonMDRBlueprintData::StaticStruct, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("LiveLinkPrestonMDRBlueprintData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRBlueprintData
{
	FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRBlueprintData()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkPrestonMDRBlueprintData>(FName(TEXT("LiveLinkPrestonMDRBlueprintData")));
	}
} ScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRBlueprintData;
	struct Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StaticData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrameData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Facility structure to handle Preston MDR data in blueprint\n */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Facility structure to handle Preston MDR data in blueprint" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkPrestonMDRBlueprintData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_StaticData_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Static data that should not change every frame */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Static data that should not change every frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_StaticData = { "StaticData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRBlueprintData, StaticData), Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_StaticData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_StaticData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_FrameData_MetaData[] = {
		{ "Category", "LiveLink" },
		{ "Comment", "/** Dynamic data that can change every frame  */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Dynamic data that can change every frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_FrameData = { "FrameData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRBlueprintData, FrameData), Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_FrameData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_FrameData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_StaticData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::NewProp_FrameData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
		Z_Construct_UScriptStruct_FLiveLinkBaseBlueprintData,
		&NewStructOps,
		"LiveLinkPrestonMDRBlueprintData",
		sizeof(FLiveLinkPrestonMDRBlueprintData),
		alignof(FLiveLinkPrestonMDRBlueprintData),
		Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkPrestonMDRBlueprintData"), sizeof(FLiveLinkPrestonMDRBlueprintData), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRBlueprintData_Hash() { return 2472247776U; }

static_assert(std::is_polymorphic<FLiveLinkPrestonMDRFrameData>() == std::is_polymorphic<FLiveLinkCameraFrameData>(), "USTRUCT FLiveLinkPrestonMDRFrameData cannot be polymorphic unless super FLiveLinkCameraFrameData is polymorphic");

class UScriptStruct* FLiveLinkPrestonMDRFrameData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKPRESTONMDR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData, Z_Construct_UPackage__Script_LiveLinkPrestonMDR(), TEXT("LiveLinkPrestonMDRFrameData"), sizeof(FLiveLinkPrestonMDRFrameData), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Hash());
	}
	return Singleton;
}
template<> LIVELINKPRESTONMDR_API UScriptStruct* StaticStruct<FLiveLinkPrestonMDRFrameData>()
{
	return FLiveLinkPrestonMDRFrameData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkPrestonMDRFrameData(FLiveLinkPrestonMDRFrameData::StaticStruct, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("LiveLinkPrestonMDRFrameData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRFrameData
{
	FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRFrameData()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkPrestonMDRFrameData>(FName(TEXT("LiveLinkPrestonMDRFrameData")));
	}
} ScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRFrameData;
	struct Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RawFocusEncoderValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_RawFocusEncoderValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RawIrisEncoderValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_RawIrisEncoderValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RawZoomEncoderValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_RawZoomEncoderValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Struct for dynamic (per-frame) Preston MDR data\n */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Struct for dynamic (per-frame) Preston MDR data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkPrestonMDRFrameData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawFocusEncoderValue_MetaData[] = {
		{ "Category", "Raw Encoder Values" },
		{ "Comment", "/** Raw encoder value for focus motor */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Raw encoder value for focus motor" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawFocusEncoderValue = { "RawFocusEncoderValue", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRFrameData, RawFocusEncoderValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawFocusEncoderValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawFocusEncoderValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawIrisEncoderValue_MetaData[] = {
		{ "Category", "Raw Encoder Values" },
		{ "Comment", "/** Raw encoder value for iris motor */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Raw encoder value for iris motor" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawIrisEncoderValue = { "RawIrisEncoderValue", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRFrameData, RawIrisEncoderValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawIrisEncoderValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawIrisEncoderValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawZoomEncoderValue_MetaData[] = {
		{ "Category", "Raw Encoder Values" },
		{ "Comment", "/** Raw encoder value for zoom motor */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Raw encoder value for zoom motor" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawZoomEncoderValue = { "RawZoomEncoderValue", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRFrameData, RawZoomEncoderValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawZoomEncoderValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawZoomEncoderValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawFocusEncoderValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawIrisEncoderValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::NewProp_RawZoomEncoderValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
		Z_Construct_UScriptStruct_FLiveLinkCameraFrameData,
		&NewStructOps,
		"LiveLinkPrestonMDRFrameData",
		sizeof(FLiveLinkPrestonMDRFrameData),
		alignof(FLiveLinkPrestonMDRFrameData),
		Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkPrestonMDRFrameData"), sizeof(FLiveLinkPrestonMDRFrameData), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRFrameData_Hash() { return 1546813302U; }

static_assert(std::is_polymorphic<FLiveLinkPrestonMDRStaticData>() == std::is_polymorphic<FLiveLinkCameraStaticData>(), "USTRUCT FLiveLinkPrestonMDRStaticData cannot be polymorphic unless super FLiveLinkCameraStaticData is polymorphic");

class UScriptStruct* FLiveLinkPrestonMDRStaticData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKPRESTONMDR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData, Z_Construct_UPackage__Script_LiveLinkPrestonMDR(), TEXT("LiveLinkPrestonMDRStaticData"), sizeof(FLiveLinkPrestonMDRStaticData), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Hash());
	}
	return Singleton;
}
template<> LIVELINKPRESTONMDR_API UScriptStruct* StaticStruct<FLiveLinkPrestonMDRStaticData>()
{
	return FLiveLinkPrestonMDRStaticData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkPrestonMDRStaticData(FLiveLinkPrestonMDRStaticData::StaticStruct, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("LiveLinkPrestonMDRStaticData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRStaticData
{
	FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRStaticData()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkPrestonMDRStaticData>(FName(TEXT("LiveLinkPrestonMDRStaticData")));
	}
} ScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRStaticData;
	struct Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Struct for static Preston MDR data\n */" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRTypes.h" },
		{ "ToolTip", "Struct for static Preston MDR data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkPrestonMDRStaticData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
		Z_Construct_UScriptStruct_FLiveLinkCameraStaticData,
		&NewStructOps,
		"LiveLinkPrestonMDRStaticData",
		sizeof(FLiveLinkPrestonMDRStaticData),
		alignof(FLiveLinkPrestonMDRStaticData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkPrestonMDRStaticData"), sizeof(FLiveLinkPrestonMDRStaticData), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRStaticData_Hash() { return 772724659U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
