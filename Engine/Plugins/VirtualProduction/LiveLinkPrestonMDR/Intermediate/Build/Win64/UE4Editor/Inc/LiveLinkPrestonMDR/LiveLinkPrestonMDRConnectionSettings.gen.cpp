// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkPrestonMDR/Public/LiveLinkPrestonMDRConnectionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkPrestonMDRConnectionSettings() {}
// Cross Module References
	LIVELINKPRESTONMDR_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
// End Cross Module References
class UScriptStruct* FLiveLinkPrestonMDRConnectionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKPRESTONMDR_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings, Z_Construct_UPackage__Script_LiveLinkPrestonMDR(), TEXT("LiveLinkPrestonMDRConnectionSettings"), sizeof(FLiveLinkPrestonMDRConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Hash());
	}
	return Singleton;
}
template<> LIVELINKPRESTONMDR_API UScriptStruct* StaticStruct<FLiveLinkPrestonMDRConnectionSettings>()
{
	return FLiveLinkPrestonMDRConnectionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings(FLiveLinkPrestonMDRConnectionSettings::StaticStruct, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("LiveLinkPrestonMDRConnectionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRConnectionSettings
{
	FScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRConnectionSettings()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkPrestonMDRConnectionSettings>(FName(TEXT("LiveLinkPrestonMDRConnectionSettings")));
	}
} ScriptStruct_LiveLinkPrestonMDR_StaticRegisterNativesFLiveLinkPrestonMDRConnectionSettings;
	struct Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IPAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IPAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_PortNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubjectName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LiveLinkPrestonMDRConnectionSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkPrestonMDRConnectionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_IPAddress_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/LiveLinkPrestonMDRConnectionSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_IPAddress = { "IPAddress", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRConnectionSettings, IPAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_IPAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_IPAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_PortNumber_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/LiveLinkPrestonMDRConnectionSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_PortNumber = { "PortNumber", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRConnectionSettings, PortNumber), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_PortNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_PortNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/LiveLinkPrestonMDRConnectionSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkPrestonMDRConnectionSettings, SubjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_SubjectName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_IPAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_PortNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::NewProp_SubjectName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
		nullptr,
		&NewStructOps,
		"LiveLinkPrestonMDRConnectionSettings",
		sizeof(FLiveLinkPrestonMDRConnectionSettings),
		alignof(FLiveLinkPrestonMDRConnectionSettings),
		Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkPrestonMDRConnectionSettings"), sizeof(FLiveLinkPrestonMDRConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkPrestonMDRConnectionSettings_Hash() { return 3474123596U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
