// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkPrestonMDR/Private/LiveLinkPrestonMDRRole.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkPrestonMDRRole() {}
// Cross Module References
	LIVELINKPRESTONMDR_API UClass* Z_Construct_UClass_ULiveLinkPrestonMDRRole_NoRegister();
	LIVELINKPRESTONMDR_API UClass* Z_Construct_UClass_ULiveLinkPrestonMDRRole();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkCameraRole();
	UPackage* Z_Construct_UPackage__Script_LiveLinkPrestonMDR();
// End Cross Module References
	void ULiveLinkPrestonMDRRole::StaticRegisterNativesULiveLinkPrestonMDRRole()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkPrestonMDRRole_NoRegister()
	{
		return ULiveLinkPrestonMDRRole::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkCameraRole,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkPrestonMDR,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Role associated with Preston MDR data\n */" },
		{ "DisplayName", "Preston MDR Role" },
		{ "IncludePath", "LiveLinkPrestonMDRRole.h" },
		{ "ModuleRelativePath", "Private/LiveLinkPrestonMDRRole.h" },
		{ "ToolTip", "Role associated with Preston MDR data" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkPrestonMDRRole>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::ClassParams = {
		&ULiveLinkPrestonMDRRole::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkPrestonMDRRole()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkPrestonMDRRole_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkPrestonMDRRole, 1249633008);
	template<> LIVELINKPRESTONMDR_API UClass* StaticClass<ULiveLinkPrestonMDRRole>()
	{
		return ULiveLinkPrestonMDRRole::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkPrestonMDRRole(Z_Construct_UClass_ULiveLinkPrestonMDRRole, &ULiveLinkPrestonMDRRole::StaticClass, TEXT("/Script/LiveLinkPrestonMDR"), TEXT("ULiveLinkPrestonMDRRole"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkPrestonMDRRole);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
