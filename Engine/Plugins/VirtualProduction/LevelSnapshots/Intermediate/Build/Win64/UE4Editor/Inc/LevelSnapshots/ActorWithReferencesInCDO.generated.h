// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_ActorWithReferencesInCDO_generated_h
#error "ActorWithReferencesInCDO.generated.h already included, missing '#pragma once' in ActorWithReferencesInCDO.h"
#endif
#define LEVELSNAPSHOTS_ActorWithReferencesInCDO_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics; \
	LEVELSNAPSHOTS_API static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FExternalReferenceDummy>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAActorWithReferencesInCDO(); \
	friend struct Z_Construct_UClass_AActorWithReferencesInCDO_Statics; \
public: \
	DECLARE_CLASS(AActorWithReferencesInCDO, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(AActorWithReferencesInCDO)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_INCLASS \
private: \
	static void StaticRegisterNativesAActorWithReferencesInCDO(); \
	friend struct Z_Construct_UClass_AActorWithReferencesInCDO_Statics; \
public: \
	DECLARE_CLASS(AActorWithReferencesInCDO, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(AActorWithReferencesInCDO)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AActorWithReferencesInCDO(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AActorWithReferencesInCDO) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorWithReferencesInCDO); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorWithReferencesInCDO); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorWithReferencesInCDO(AActorWithReferencesInCDO&&); \
	NO_API AActorWithReferencesInCDO(const AActorWithReferencesInCDO&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorWithReferencesInCDO(AActorWithReferencesInCDO&&); \
	NO_API AActorWithReferencesInCDO(const AActorWithReferencesInCDO&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorWithReferencesInCDO); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorWithReferencesInCDO); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AActorWithReferencesInCDO)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_36_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class AActorWithReferencesInCDO>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_Types_ActorWithReferencesInCDO_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
