// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/LevelSnapshotsEditorData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotsEditorData() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_ULevelSnapshotsEditorData_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_ULevelSnapshotsEditorData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFavoriteFilterContainer_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_ULevelSnapshotsFilterPreset_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFilterLoader_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFilteredResults_NoRegister();
// End Cross Module References
	void ULevelSnapshotsEditorData::StaticRegisterNativesULevelSnapshotsEditorData()
	{
	}
	UClass* Z_Construct_UClass_ULevelSnapshotsEditorData_NoRegister()
	{
		return ULevelSnapshotsEditorData::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotsEditorData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FavoriteFilters_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FavoriteFilters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserDefinedFilters_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UserDefinedFilters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterLoader_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FilterLoader;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsFilterDirty_MetaData[];
#endif
		static void NewProp_bIsFilterDirty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsFilterDirty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterResults_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FilterResults;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* Stores all data shared across the editor's UI. */" },
		{ "IncludePath", "Data/LevelSnapshotsEditorData.h" },
		{ "ModuleRelativePath", "Private/Data/LevelSnapshotsEditorData.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Stores all data shared across the editor's UI." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FavoriteFilters_MetaData[] = {
		{ "ModuleRelativePath", "Private/Data/LevelSnapshotsEditorData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FavoriteFilters = { "FavoriteFilters", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorData, FavoriteFilters), Z_Construct_UClass_UFavoriteFilterContainer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FavoriteFilters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FavoriteFilters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_UserDefinedFilters_MetaData[] = {
		{ "Comment", "/* Stores user-defined filters in chain of ORs of ANDs. */" },
		{ "ModuleRelativePath", "Private/Data/LevelSnapshotsEditorData.h" },
		{ "ToolTip", "Stores user-defined filters in chain of ORs of ANDs." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_UserDefinedFilters = { "UserDefinedFilters", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorData, UserDefinedFilters), Z_Construct_UClass_ULevelSnapshotsFilterPreset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_UserDefinedFilters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_UserDefinedFilters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterLoader_MetaData[] = {
		{ "Comment", "/* Handles save & load requests for exchanging UserDefinedFilters. */" },
		{ "ModuleRelativePath", "Private/Data/LevelSnapshotsEditorData.h" },
		{ "ToolTip", "Handles save & load requests for exchanging UserDefinedFilters." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterLoader = { "FilterLoader", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorData, FilterLoader), Z_Construct_UClass_UFilterLoader_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterLoader_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterLoader_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty_MetaData[] = {
		{ "Comment", "/* Used for determining whether the filter state has changed since it was last refreshed. */" },
		{ "ModuleRelativePath", "Private/Data/LevelSnapshotsEditorData.h" },
		{ "ToolTip", "Used for determining whether the filter state has changed since it was last refreshed." },
	};
#endif
	void Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty_SetBit(void* Obj)
	{
		((ULevelSnapshotsEditorData*)Obj)->bIsFilterDirty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty = { "bIsFilterDirty", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSnapshotsEditorData), &Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterResults_MetaData[] = {
		{ "Comment", "/* Converts UserDefinedFilters into ULevelSnapshotsSelectionSet display in results view. */" },
		{ "ModuleRelativePath", "Private/Data/LevelSnapshotsEditorData.h" },
		{ "ToolTip", "Converts UserDefinedFilters into ULevelSnapshotsSelectionSet display in results view." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterResults = { "FilterResults", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorData, FilterResults), Z_Construct_UClass_UFilteredResults_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterResults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterResults_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FavoriteFilters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_UserDefinedFilters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterLoader,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_bIsFilterDirty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::NewProp_FilterResults,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotsEditorData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::ClassParams = {
		&ULevelSnapshotsEditorData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotsEditorData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotsEditorData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotsEditorData, 4111239987);
	template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<ULevelSnapshotsEditorData>()
	{
		return ULevelSnapshotsEditorData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotsEditorData(Z_Construct_UClass_ULevelSnapshotsEditorData, &ULevelSnapshotsEditorData::StaticClass, TEXT("/Script/LevelSnapshotsEditor"), TEXT("ULevelSnapshotsEditorData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotsEditorData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
