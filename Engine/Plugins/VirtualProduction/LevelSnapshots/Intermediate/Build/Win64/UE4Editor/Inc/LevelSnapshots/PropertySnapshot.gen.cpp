// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/PropertySnapshot.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePropertySnapshot() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FLevelSnapshot_Property();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
// End Cross Module References
class UScriptStruct* FLevelSnapshot_Property::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FLevelSnapshot_Property_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLevelSnapshot_Property, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("LevelSnapshot_Property"), sizeof(FLevelSnapshot_Property), Get_Z_Construct_UScriptStruct_FLevelSnapshot_Property_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FLevelSnapshot_Property>()
{
	return FLevelSnapshot_Property::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLevelSnapshot_Property(FLevelSnapshot_Property::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("LevelSnapshot_Property"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFLevelSnapshot_Property
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFLevelSnapshot_Property()
	{
		UScriptStruct::DeferCppStructOps<FLevelSnapshot_Property>(FName(TEXT("LevelSnapshot_Property")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFLevelSnapshot_Property;
	struct Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_PropertyPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyFlags_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_PropertyFlags;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_PropertyDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_DataOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_DataSize;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ReferencedNamesOffsetToIndex_ValueProp;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ReferencedNamesOffsetToIndex_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferencedNamesOffsetToIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReferencedNamesOffsetToIndex;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ReferencedObjectOffsetToIndex_ValueProp;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ReferencedObjectOffsetToIndex_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferencedObjectOffsetToIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReferencedObjectOffsetToIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLevelSnapshot_Property>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyPath_MetaData[] = {
		{ "Category", "Snapshot" },
		{ "Comment", "/** Base information about this property scope. */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Base information about this property scope." },
	};
#endif
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyPath = { "PropertyPath", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, PropertyPath), &FProperty::StaticClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyFlags_MetaData[] = {
		{ "Category", "Snapshot" },
		{ "Comment", "/** Property flags ie. Transient, NonTransactional, etc. */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Property flags ie. Transient, NonTransactional, etc." },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyFlags = { "PropertyFlags", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, PropertyFlags), METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyFlags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyFlags_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyDepth_MetaData[] = {
		{ "Category", "Snapshot" },
		{ "Comment", "/** Property depth from the recorded snapshot (i.e. 0 -> Root Property) */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Property depth from the recorded snapshot (i.e. 0 -> Root Property)" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyDepth = { "PropertyDepth", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, PropertyDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataOffset_MetaData[] = {
		{ "Category", "Snapshot" },
		{ "Comment", "/** Recorded DataOffset of this property scope in the FObjectSnapshot data buffer. */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Recorded DataOffset of this property scope in the FObjectSnapshot data buffer." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataOffset = { "DataOffset", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, DataOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataSize_MetaData[] = {
		{ "Category", "Snapshot" },
		{ "Comment", "/** Recorded DataSize of this property scope in the FObjectSnapshot data buffer. */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Recorded DataSize of this property scope in the FObjectSnapshot data buffer." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataSize = { "DataSize", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, DataSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataSize_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_ValueProp = { "ReferencedNamesOffsetToIndex", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_Key_KeyProp = { "ReferencedNamesOffsetToIndex_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_MetaData[] = {
		{ "Comment", "/** Referenced Names Offset to their NameIndex in the FObjectSnapshot::ReferencedNames array. */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Referenced Names Offset to their NameIndex in the FObjectSnapshot::ReferencedNames array." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex = { "ReferencedNamesOffsetToIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, ReferencedNamesOffsetToIndex), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_ValueProp = { "ReferencedObjectOffsetToIndex", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_Key_KeyProp = { "ReferencedObjectOffsetToIndex_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_MetaData[] = {
		{ "Comment", "/** Referenced Objects Offset to their ObjectIndex in the FObjectSnapshot::ReferencedObjects. */" },
		{ "ModuleRelativePath", "Public/Data/PropertySnapshot.h" },
		{ "ToolTip", "Referenced Objects Offset to their ObjectIndex in the FObjectSnapshot::ReferencedObjects." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex = { "ReferencedObjectOffsetToIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSnapshot_Property, ReferencedObjectOffsetToIndex), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyFlags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_PropertyDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_DataSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedNamesOffsetToIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::NewProp_ReferencedObjectOffsetToIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"LevelSnapshot_Property",
		sizeof(FLevelSnapshot_Property),
		alignof(FLevelSnapshot_Property),
		Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLevelSnapshot_Property()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLevelSnapshot_Property_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LevelSnapshot_Property"), sizeof(FLevelSnapshot_Property), Get_Z_Construct_UScriptStruct_FLevelSnapshot_Property_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLevelSnapshot_Property_Hash() { return 2742530210U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
