// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Private/Tests/Types/ActorWithReferencesInCDO.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorWithReferencesInCDO() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FExternalReferenceDummy();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_AActorWithReferencesInCDO_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_AActorWithReferencesInCDO();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
class UScriptStruct* FExternalReferenceDummy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FExternalReferenceDummy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FExternalReferenceDummy, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("ExternalReferenceDummy"), sizeof(FExternalReferenceDummy), Get_Z_Construct_UScriptStruct_FExternalReferenceDummy_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FExternalReferenceDummy>()
{
	return FExternalReferenceDummy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FExternalReferenceDummy(FExternalReferenceDummy::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("ExternalReferenceDummy"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFExternalReferenceDummy
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFExternalReferenceDummy()
	{
		UScriptStruct::DeferCppStructOps<FExternalReferenceDummy>(FName(TEXT("ExternalReferenceDummy")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFExternalReferenceDummy;
	struct Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Object_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Object;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FExternalReferenceDummy>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::NewProp_Object_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::NewProp_Object = { "Object", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FExternalReferenceDummy, Object), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::NewProp_Object_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::NewProp_Object_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::NewProp_Object,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"ExternalReferenceDummy",
		sizeof(FExternalReferenceDummy),
		alignof(FExternalReferenceDummy),
		Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FExternalReferenceDummy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FExternalReferenceDummy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ExternalReferenceDummy"), sizeof(FExternalReferenceDummy), Get_Z_Construct_UScriptStruct_FExternalReferenceDummy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FExternalReferenceDummy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FExternalReferenceDummy_Hash() { return 3756352602U; }
	void AActorWithReferencesInCDO::StaticRegisterNativesAActorWithReferencesInCDO()
	{
	}
	UClass* Z_Construct_UClass_AActorWithReferencesInCDO_NoRegister()
	{
		return AActorWithReferencesInCDO::StaticClass();
	}
	struct Z_Construct_UClass_AActorWithReferencesInCDO_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Array_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Array_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Array;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Set_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Set_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_Set;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IntKeyMap_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntKeyMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntKeyMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_IntKeyMap;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntValueMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IntValueMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntValueMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_IntValueMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Struct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Struct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CubeMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CylinderMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CylinderMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AActorWithReferencesInCDO_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/Types/ActorWithReferencesInCDO.h" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array_Inner = { "Array", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FExternalReferenceDummy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** Properties  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
		{ "ToolTip", "***************** Properties  *******************" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array = { "Array", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, Array), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set_ElementProp = { "Set", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FExternalReferenceDummy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set = { "Set", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, Set), METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_ValueProp = { "IntKeyMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FExternalReferenceDummy, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_Key_KeyProp = { "IntKeyMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap = { "IntKeyMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, IntKeyMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_ValueProp = { "IntValueMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_Key_KeyProp = { "IntValueMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FExternalReferenceDummy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap = { "IntValueMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, IntValueMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Struct_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Struct = { "Struct", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, Struct), Z_Construct_UScriptStruct_FExternalReferenceDummy, METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Struct_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Struct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CubeMesh_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** External references  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
		{ "ToolTip", "***************** External references  *******************" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CubeMesh = { "CubeMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, CubeMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CubeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CubeMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CylinderMesh_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/Types/ActorWithReferencesInCDO.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CylinderMesh = { "CylinderMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AActorWithReferencesInCDO, CylinderMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CylinderMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CylinderMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AActorWithReferencesInCDO_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Array,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Set,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntKeyMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_IntValueMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_Struct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CubeMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AActorWithReferencesInCDO_Statics::NewProp_CylinderMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AActorWithReferencesInCDO_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AActorWithReferencesInCDO>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AActorWithReferencesInCDO_Statics::ClassParams = {
		&AActorWithReferencesInCDO::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AActorWithReferencesInCDO_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AActorWithReferencesInCDO_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AActorWithReferencesInCDO()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AActorWithReferencesInCDO_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AActorWithReferencesInCDO, 1136009099);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<AActorWithReferencesInCDO>()
	{
		return AActorWithReferencesInCDO::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AActorWithReferencesInCDO(Z_Construct_UClass_AActorWithReferencesInCDO, &AActorWithReferencesInCDO::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("AActorWithReferencesInCDO"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AActorWithReferencesInCDO);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
