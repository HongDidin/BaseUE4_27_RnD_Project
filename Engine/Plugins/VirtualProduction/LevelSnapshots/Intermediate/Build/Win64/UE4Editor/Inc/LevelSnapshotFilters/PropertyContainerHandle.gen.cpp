// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/PropertyContainerHandle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePropertyContainerHandle() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FPropertyContainerHandle();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
// End Cross Module References
class UScriptStruct* FPropertyContainerHandle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTFILTERS_API uint32 Get_Z_Construct_UScriptStruct_FPropertyContainerHandle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPropertyContainerHandle, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("PropertyContainerHandle"), sizeof(FPropertyContainerHandle), Get_Z_Construct_UScriptStruct_FPropertyContainerHandle_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<FPropertyContainerHandle>()
{
	return FPropertyContainerHandle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPropertyContainerHandle(FPropertyContainerHandle::StaticStruct, TEXT("/Script/LevelSnapshotFilters"), TEXT("PropertyContainerHandle"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFPropertyContainerHandle
{
	FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFPropertyContainerHandle()
	{
		UScriptStruct::DeferCppStructOps<FPropertyContainerHandle>(FName(TEXT("PropertyContainerHandle")));
	}
} ScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFPropertyContainerHandle;
	struct Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* Blueprint wrapper for FProperty containers */" },
		{ "ModuleRelativePath", "Public/PropertyContainerHandle.h" },
		{ "ToolTip", "Blueprint wrapper for FProperty containers" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPropertyContainerHandle>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
		nullptr,
		&NewStructOps,
		"PropertyContainerHandle",
		sizeof(FPropertyContainerHandle),
		alignof(FPropertyContainerHandle),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPropertyContainerHandle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPropertyContainerHandle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PropertyContainerHandle"), sizeof(FPropertyContainerHandle), Get_Z_Construct_UScriptStruct_FPropertyContainerHandle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPropertyContainerHandle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPropertyContainerHandle_Hash() { return 1145567931U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
