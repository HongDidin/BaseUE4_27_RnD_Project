// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/BlueprintOnly/OrFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOrFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UOrFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UOrFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UParentFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
// End Cross Module References
	void UOrFilter::StaticRegisterNativesUOrFilter()
	{
	}
	UClass* Z_Construct_UClass_UOrFilter_NoRegister()
	{
		return UOrFilter::StaticClass();
	}
	struct Z_Construct_UClass_UOrFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOrFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UParentFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Builtin/BlueprintOnly/OrFilter.h" },
		{ "InternalSnapshotFilter", "" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/OrFilter.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOrFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOrFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOrFilter_Statics::ClassParams = {
		&UOrFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOrFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOrFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOrFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOrFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOrFilter, 2071862494);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UOrFilter>()
	{
		return UOrFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOrFilter(Z_Construct_UClass_UOrFilter, &UOrFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UOrFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOrFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
