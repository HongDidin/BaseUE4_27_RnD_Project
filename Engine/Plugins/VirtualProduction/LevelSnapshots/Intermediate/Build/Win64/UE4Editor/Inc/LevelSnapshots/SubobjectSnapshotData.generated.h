// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_SubobjectSnapshotData_generated_h
#error "SubobjectSnapshotData.generated.h already included, missing '#pragma once' in SubobjectSnapshotData.h"
#endif
#define LEVELSNAPSHOTS_SubobjectSnapshotData_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SubobjectSnapshotData_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FObjectSnapshotData Super;


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FSubobjectSnapshotData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SubobjectSnapshotData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
