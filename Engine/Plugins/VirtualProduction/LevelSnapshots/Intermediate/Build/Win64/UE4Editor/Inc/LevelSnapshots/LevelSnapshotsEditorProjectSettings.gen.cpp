// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Settings/LevelSnapshotsEditorProjectSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotsEditorProjectSettings() {}
// Cross Module References
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FRestorationBlacklist();
// End Cross Module References
	void ULevelSnapshotsEditorProjectSettings::StaticRegisterNativesULevelSnapshotsEditorProjectSettings()
	{
	}
	UClass* Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_NoRegister()
	{
		return ULevelSnapshotsEditorProjectSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blacklist_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Blacklist;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableLevelSnapshotsToolbarButton_MetaData[];
#endif
		static void NewProp_bEnableLevelSnapshotsToolbarButton_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableLevelSnapshotsToolbarButton;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCreationForm_MetaData[];
#endif
		static void NewProp_bUseCreationForm_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCreationForm;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClickActorGroupToSelectActorInScene_MetaData[];
#endif
		static void NewProp_bClickActorGroupToSelectActorInScene_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClickActorGroupToSelectActorInScene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreferredCreationFormWindowWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreferredCreationFormWindowWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreferredCreationFormWindowHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreferredCreationFormWindowHeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Settings/LevelSnapshotsEditorProjectSettings.h" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_Blacklist_MetaData[] = {
		{ "Category", "Level Snapshots|Behavior" },
		{ "Comment", "/* Specifies classes and properties that should never be captured nor restored. */" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
		{ "ToolTip", "Specifies classes and properties that should never be captured nor restored." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_Blacklist = { "Blacklist", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorProjectSettings, Blacklist), Z_Construct_UScriptStruct_FRestorationBlacklist, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_Blacklist_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_Blacklist_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton_MetaData[] = {
		{ "Category", "Level Snapshots|Editor" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
	};
#endif
	void Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton_SetBit(void* Obj)
	{
		((ULevelSnapshotsEditorProjectSettings*)Obj)->bEnableLevelSnapshotsToolbarButton = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton = { "bEnableLevelSnapshotsToolbarButton", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSnapshotsEditorProjectSettings), &Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm_MetaData[] = {
		{ "Category", "Level Snapshots|Editor" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
	};
#endif
	void Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm_SetBit(void* Obj)
	{
		((ULevelSnapshotsEditorProjectSettings*)Obj)->bUseCreationForm = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm = { "bUseCreationForm", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSnapshotsEditorProjectSettings), &Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene_MetaData[] = {
		{ "Category", "Level Snapshots|Editor" },
		{ "Comment", "/* If true, clicking on an actor group under 'Modified Actors' will select the actor in the scene. The previous selection will be deselected. */" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
		{ "ToolTip", "If true, clicking on an actor group under 'Modified Actors' will select the actor in the scene. The previous selection will be deselected." },
	};
#endif
	void Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene_SetBit(void* Obj)
	{
		((ULevelSnapshotsEditorProjectSettings*)Obj)->bClickActorGroupToSelectActorInScene = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene = { "bClickActorGroupToSelectActorInScene", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSnapshotsEditorProjectSettings), &Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowWidth_MetaData[] = {
		{ "Category", "Level Snapshots|Editor" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowWidth = { "PreferredCreationFormWindowWidth", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorProjectSettings, PreferredCreationFormWindowWidth), METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowHeight_MetaData[] = {
		{ "Category", "Level Snapshots|Editor" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowHeight = { "PreferredCreationFormWindowHeight", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorProjectSettings, PreferredCreationFormWindowHeight), METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowHeight_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_Blacklist,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bEnableLevelSnapshotsToolbarButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bUseCreationForm,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_bClickActorGroupToSelectActorInScene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::NewProp_PreferredCreationFormWindowHeight,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotsEditorProjectSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::ClassParams = {
		&ULevelSnapshotsEditorProjectSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotsEditorProjectSettings, 4068657754);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<ULevelSnapshotsEditorProjectSettings>()
	{
		return ULevelSnapshotsEditorProjectSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotsEditorProjectSettings(Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings, &ULevelSnapshotsEditorProjectSettings::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("ULevelSnapshotsEditorProjectSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotsEditorProjectSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
