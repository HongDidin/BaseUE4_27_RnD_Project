// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_FilterLoader_generated_h
#error "FilterLoader.generated.h already included, missing '#pragma once' in FilterLoader.h"
#endif
#define LEVELSNAPSHOTSEDITOR_FilterLoader_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFilterLoader(); \
	friend struct Z_Construct_UClass_UFilterLoader_Statics; \
public: \
	DECLARE_CLASS(UFilterLoader, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(UFilterLoader)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUFilterLoader(); \
	friend struct Z_Construct_UClass_UFilterLoader_Statics; \
public: \
	DECLARE_CLASS(UFilterLoader, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(UFilterLoader)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFilterLoader(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFilterLoader) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFilterLoader); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFilterLoader); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFilterLoader(UFilterLoader&&); \
	NO_API UFilterLoader(const UFilterLoader&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFilterLoader(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFilterLoader(UFilterLoader&&); \
	NO_API UFilterLoader(const UFilterLoader&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFilterLoader); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFilterLoader); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFilterLoader)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AssetLastSavedOrLoaded() { return STRUCT_OFFSET(UFilterLoader, AssetLastSavedOrLoaded); } \
	FORCEINLINE static uint32 __PPO__AssetBeingEdited() { return STRUCT_OFFSET(UFilterLoader, AssetBeingEdited); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class UFilterLoader>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterLoader_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
