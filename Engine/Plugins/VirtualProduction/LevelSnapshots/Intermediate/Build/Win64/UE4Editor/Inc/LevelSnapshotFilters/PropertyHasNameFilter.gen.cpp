// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/PropertyHasNameFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePropertyHasNameFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_ENameMatchingRule();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UPropertyHasNameFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UPropertyHasNameFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UPropertySelectorFilter();
// End Cross Module References
	static UEnum* ENameMatchingRule_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LevelSnapshotFilters_ENameMatchingRule, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("ENameMatchingRule"));
		}
		return Singleton;
	}
	template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<ENameMatchingRule::Type>()
	{
		return ENameMatchingRule_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENameMatchingRule(ENameMatchingRule_StaticEnum, TEXT("/Script/LevelSnapshotFilters"), TEXT("ENameMatchingRule"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LevelSnapshotFilters_ENameMatchingRule_Hash() { return 3285747509U; }
	UEnum* Z_Construct_UEnum_LevelSnapshotFilters_ENameMatchingRule()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENameMatchingRule"), 0, Get_Z_Construct_UEnum_LevelSnapshotFilters_ENameMatchingRule_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENameMatchingRule::MatchesExactly", (int64)ENameMatchingRule::MatchesExactly },
				{ "ENameMatchingRule::MatchesIgnoreCase", (int64)ENameMatchingRule::MatchesIgnoreCase },
				{ "ENameMatchingRule::ContainsExactly", (int64)ENameMatchingRule::ContainsExactly },
				{ "ENameMatchingRule::ContainsIgnoreCase", (int64)ENameMatchingRule::ContainsIgnoreCase },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ContainsExactly.Comment", "/* The name must contains the following substring (case sensitive) */" },
				{ "ContainsExactly.Name", "ENameMatchingRule::ContainsExactly" },
				{ "ContainsExactly.ToolTip", "The name must contains the following substring (case sensitive)" },
				{ "ContainsIgnoreCase.Comment", "/* The name must contains the following substring (case insensitive) */" },
				{ "ContainsIgnoreCase.Name", "ENameMatchingRule::ContainsIgnoreCase" },
				{ "ContainsIgnoreCase.ToolTip", "The name must contains the following substring (case insensitive)" },
				{ "MatchesExactly.Comment", "/* The property name must match the given name exactly. */" },
				{ "MatchesExactly.Name", "ENameMatchingRule::MatchesExactly" },
				{ "MatchesExactly.ToolTip", "The property name must match the given name exactly." },
				{ "MatchesIgnoreCase.Comment", "/* The name must match the given name but ignores the case */" },
				{ "MatchesIgnoreCase.Name", "ENameMatchingRule::MatchesIgnoreCase" },
				{ "MatchesIgnoreCase.ToolTip", "The name must match the given name but ignores the case" },
				{ "ModuleRelativePath", "Public/Builtin/PropertyHasNameFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
				nullptr,
				"ENameMatchingRule",
				"ENameMatchingRule::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UPropertyHasNameFilter::StaticRegisterNativesUPropertyHasNameFilter()
	{
	}
	UClass* Z_Construct_UClass_UPropertyHasNameFilter_NoRegister()
	{
		return UPropertyHasNameFilter::StaticClass();
	}
	struct Z_Construct_UClass_UPropertyHasNameFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameMatchingRule_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NameMatchingRule;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AllowedNames_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllowedNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_AllowedNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPropertyHasNameFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPropertySelectorFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyHasNameFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Allows a property when is has a certain name\n * Use case: You only want to allow properties named \"MyPropertyName\"\n */" },
		{ "CommonSnapshotFilter", "" },
		{ "IncludePath", "Builtin/PropertyHasNameFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/PropertyHasNameFilter.h" },
		{ "ToolTip", "Allows a property when is has a certain name\nUse case: You only want to allow properties named \"MyPropertyName\"" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_NameMatchingRule_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* How to compare the property name to AllowedNames */" },
		{ "ModuleRelativePath", "Public/Builtin/PropertyHasNameFilter.h" },
		{ "ToolTip", "How to compare the property name to AllowedNames" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_NameMatchingRule = { "NameMatchingRule", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyHasNameFilter, NameMatchingRule), Z_Construct_UEnum_LevelSnapshotFilters_ENameMatchingRule, METADATA_PARAMS(Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_NameMatchingRule_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_NameMatchingRule_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames_ElementProp = { "AllowedNames", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* The names to match the property name against. */" },
		{ "ModuleRelativePath", "Public/Builtin/PropertyHasNameFilter.h" },
		{ "ToolTip", "The names to match the property name against." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames = { "AllowedNames", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPropertyHasNameFilter, AllowedNames), METADATA_PARAMS(Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPropertyHasNameFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_NameMatchingRule,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPropertyHasNameFilter_Statics::NewProp_AllowedNames,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPropertyHasNameFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPropertyHasNameFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPropertyHasNameFilter_Statics::ClassParams = {
		&UPropertyHasNameFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPropertyHasNameFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyHasNameFilter_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPropertyHasNameFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyHasNameFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPropertyHasNameFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPropertyHasNameFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPropertyHasNameFilter, 417920003);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UPropertyHasNameFilter>()
	{
		return UPropertyHasNameFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPropertyHasNameFilter(Z_Construct_UClass_UPropertyHasNameFilter, &UPropertyHasNameFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UPropertyHasNameFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPropertyHasNameFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
