// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDateTime;
struct FSoftObjectPath;
#ifdef LEVELSNAPSHOTS_LevelSnapshot_generated_h
#error "LevelSnapshot.generated.h already included, missing '#pragma once' in LevelSnapshot.h"
#endif
#define LEVELSNAPSHOTS_LevelSnapshot_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSnapshotDescription); \
	DECLARE_FUNCTION(execGetSnapshotName); \
	DECLARE_FUNCTION(execGetCaptureTime); \
	DECLARE_FUNCTION(execGetMapPath); \
	DECLARE_FUNCTION(execSetSnapshotDescription); \
	DECLARE_FUNCTION(execSetSnapshotName);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSnapshotDescription); \
	DECLARE_FUNCTION(execGetSnapshotName); \
	DECLARE_FUNCTION(execGetCaptureTime); \
	DECLARE_FUNCTION(execGetMapPath); \
	DECLARE_FUNCTION(execSetSnapshotDescription); \
	DECLARE_FUNCTION(execSetSnapshotName);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshot(); \
	friend struct Z_Construct_UClass_ULevelSnapshot_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshot, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshot)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshot(); \
	friend struct Z_Construct_UClass_ULevelSnapshot_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshot, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshot)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshot(ULevelSnapshot&&); \
	NO_API ULevelSnapshot(const ULevelSnapshot&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshot(ULevelSnapshot&&); \
	NO_API ULevelSnapshot(const ULevelSnapshot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshot); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshot)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SnapshotContainerWorld() { return STRUCT_OFFSET(ULevelSnapshot, SnapshotContainerWorld); } \
	FORCEINLINE static uint32 __PPO__SerializedData() { return STRUCT_OFFSET(ULevelSnapshot, SerializedData); } \
	FORCEINLINE static uint32 __PPO__MapPath() { return STRUCT_OFFSET(ULevelSnapshot, MapPath); } \
	FORCEINLINE static uint32 __PPO__CaptureTime() { return STRUCT_OFFSET(ULevelSnapshot, CaptureTime); } \
	FORCEINLINE static uint32 __PPO__SnapshotName() { return STRUCT_OFFSET(ULevelSnapshot, SnapshotName); } \
	FORCEINLINE static uint32 __PPO__SnapshotDescription() { return STRUCT_OFFSET(ULevelSnapshot, SnapshotDescription); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class ULevelSnapshot>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_LevelSnapshot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
