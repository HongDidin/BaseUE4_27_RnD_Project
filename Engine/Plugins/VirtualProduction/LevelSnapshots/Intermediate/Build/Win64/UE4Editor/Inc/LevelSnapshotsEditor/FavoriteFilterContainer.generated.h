// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_FavoriteFilterContainer_generated_h
#error "FavoriteFilterContainer.generated.h already included, missing '#pragma once' in FavoriteFilterContainer.h"
#endif
#define LEVELSNAPSHOTSEDITOR_FavoriteFilterContainer_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFavoriteFilterContainer(); \
	friend struct Z_Construct_UClass_UFavoriteFilterContainer_Statics; \
public: \
	DECLARE_CLASS(UFavoriteFilterContainer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(UFavoriteFilterContainer)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUFavoriteFilterContainer(); \
	friend struct Z_Construct_UClass_UFavoriteFilterContainer_Statics; \
public: \
	DECLARE_CLASS(UFavoriteFilterContainer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(UFavoriteFilterContainer)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFavoriteFilterContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFavoriteFilterContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFavoriteFilterContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFavoriteFilterContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFavoriteFilterContainer(UFavoriteFilterContainer&&); \
	NO_API UFavoriteFilterContainer(const UFavoriteFilterContainer&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFavoriteFilterContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFavoriteFilterContainer(UFavoriteFilterContainer&&); \
	NO_API UFavoriteFilterContainer(const UFavoriteFilterContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFavoriteFilterContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFavoriteFilterContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFavoriteFilterContainer)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Favorites() { return STRUCT_OFFSET(UFavoriteFilterContainer, Favorites); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class UFavoriteFilterContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FavoriteFilterContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
