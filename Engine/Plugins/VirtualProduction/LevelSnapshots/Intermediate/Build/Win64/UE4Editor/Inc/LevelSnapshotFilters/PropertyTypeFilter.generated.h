// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_PropertyTypeFilter_generated_h
#error "PropertyTypeFilter.generated.h already included, missing '#pragma once' in PropertyTypeFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_PropertyTypeFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPropertyTypeFilter(); \
	friend struct Z_Construct_UClass_UPropertyTypeFilter_Statics; \
public: \
	DECLARE_CLASS(UPropertyTypeFilter, UPropertySelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UPropertyTypeFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_INCLASS \
private: \
	static void StaticRegisterNativesUPropertyTypeFilter(); \
	friend struct Z_Construct_UClass_UPropertyTypeFilter_Statics; \
public: \
	DECLARE_CLASS(UPropertyTypeFilter, UPropertySelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UPropertyTypeFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPropertyTypeFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPropertyTypeFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPropertyTypeFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPropertyTypeFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPropertyTypeFilter(UPropertyTypeFilter&&); \
	NO_API UPropertyTypeFilter(const UPropertyTypeFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPropertyTypeFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPropertyTypeFilter(UPropertyTypeFilter&&); \
	NO_API UPropertyTypeFilter(const UPropertyTypeFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPropertyTypeFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPropertyTypeFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPropertyTypeFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AllowedTypes() { return STRUCT_OFFSET(UPropertyTypeFilter, AllowedTypes); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_60_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h_63_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UPropertyTypeFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyTypeFilter_h


#define FOREACH_ENUM_EBLUEPRINTPROPERTYTYPE(op) \
	op(EBlueprintPropertyType::Byte) \
	op(EBlueprintPropertyType::Int) \
	op(EBlueprintPropertyType::Int64) \
	op(EBlueprintPropertyType::Bool) \
	op(EBlueprintPropertyType::Float) \
	op(EBlueprintPropertyType::ObjectReference) \
	op(EBlueprintPropertyType::Name) \
	op(EBlueprintPropertyType::Interface) \
	op(EBlueprintPropertyType::Struct) \
	op(EBlueprintPropertyType::String) \
	op(EBlueprintPropertyType::Text) \
	op(EBlueprintPropertyType::WeakObjectReference) \
	op(EBlueprintPropertyType::SoftObjectReference) \
	op(EBlueprintPropertyType::Double) \
	op(EBlueprintPropertyType::Array) \
	op(EBlueprintPropertyType::Map) \
	op(EBlueprintPropertyType::Set) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
