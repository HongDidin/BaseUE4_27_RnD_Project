// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_RestorationBlacklist_generated_h
#error "RestorationBlacklist.generated.h already included, missing '#pragma once' in RestorationBlacklist.h"
#endif
#define LEVELSNAPSHOTS_RestorationBlacklist_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_RestorationBlacklist_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRestorationBlacklist_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FRestorationBlacklist>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_RestorationBlacklist_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
