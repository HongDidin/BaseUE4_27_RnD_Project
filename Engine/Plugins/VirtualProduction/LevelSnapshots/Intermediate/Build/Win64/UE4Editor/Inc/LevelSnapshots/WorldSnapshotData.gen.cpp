// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/WorldSnapshotData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWorldSnapshotData() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FWorldSnapshotData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSnapshotVersionInfo();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FActorSnapshotData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSubobjectSnapshotData();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FCustomSerializationData();
// End Cross Module References
class UScriptStruct* FWorldSnapshotData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FWorldSnapshotData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWorldSnapshotData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("WorldSnapshotData"), sizeof(FWorldSnapshotData), Get_Z_Construct_UScriptStruct_FWorldSnapshotData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FWorldSnapshotData>()
{
	return FWorldSnapshotData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWorldSnapshotData(FWorldSnapshotData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("WorldSnapshotData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFWorldSnapshotData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFWorldSnapshotData()
	{
		UScriptStruct::DeferCppStructOps<FWorldSnapshotData>(FName(TEXT("WorldSnapshotData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFWorldSnapshotData;
	struct Z_Construct_UScriptStruct_FWorldSnapshotData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotVersionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SnapshotVersionInfo;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClassDefaults_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClassDefaults_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClassDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ClassDefaults;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActorData_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActorData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ActorData;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SerializedNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SerializedNames;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedObjectReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedObjectReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SerializedObjectReferences;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Subobjects_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Subobjects_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subobjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Subobjects;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomSubobjectSerializationData_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CustomSubobjectSerializationData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomSubobjectSerializationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CustomSubobjectSerializationData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/* Holds saved world data and handles all logic related to writing to the existing world. */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Holds saved world data and handles all logic related to writing to the existing world." },
	};
#endif
	void* Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWorldSnapshotData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SnapshotVersionInfo_MetaData[] = {
		{ "Comment", "/**\n\x09 * Stores versioning information we inject into archives.\n\x09 * This is to support asset migration, like FArchive::UsingCustomVersion.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Stores versioning information we inject into archives.\nThis is to support asset migration, like FArchive::UsingCustomVersion." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SnapshotVersionInfo = { "SnapshotVersionInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, SnapshotVersionInfo), Z_Construct_UScriptStruct_FSnapshotVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SnapshotVersionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SnapshotVersionInfo_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_ValueProp = { "ClassDefaults", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_Key_KeyProp = { "ClassDefaults_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_MetaData[] = {
		{ "Comment", "/**\n\x09 * We only save properties with values different from their CDO counterpart.\n\x09 * Because of this, we need to save class defaults in the snapshot.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "We only save properties with values different from their CDO counterpart.\nBecause of this, we need to save class defaults in the snapshot." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults = { "ClassDefaults", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, ClassDefaults), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_ValueProp = { "ActorData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FActorSnapshotData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_Key_KeyProp = { "ActorData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_MetaData[] = {
		{ "Comment", "/**\n\x09 * Holds serialized actor data.\n\x09 * Maps the original actor's path to its serialized data.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Holds serialized actor data.\nMaps the original actor's path to its serialized data." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData = { "ActorData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, ActorData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames_Inner = { "SerializedNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames_MetaData[] = {
		{ "Comment", "/** Whenever an object needs to serialize a name, we add it to this array and serialize an index to this array. */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Whenever an object needs to serialize a name, we add it to this array and serialize an index to this array." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames = { "SerializedNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, SerializedNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences_Inner = { "SerializedObjectReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whenever an object needs to serialize an object reference, we keep the object path here and serialize an index to this array.\n\x09 * \n\x09 * External references, e.g. UDataAssets or UMaterials, are easily handled.\n\x09 * Example: UStaticMesh /Game/Australia/StaticMeshes/MegaScans/Nature_Rock_vbhtdixga/vbhtdixga_LOD0.vbhtdixga_LOD0\n\x09 * \n\x09 * Internal references, e.g. to subobjects and to other actors in the world, are a bit tricky.\n\x09 * For internal references, we need to do some translation using TranslateOriginalToSnapshotPath:\n\x09 * Example original: UStaticMeshActor::StaticMeshComponent /Game/MapName.MapName:PersistentLevel.StaticMeshActor_42.StaticMeshComponent\n\x09 * Example translated: UStaticMeshActor::StaticMeshComponent /Engine/Transient.World_21:PersistentLevel.StaticMeshActor_42.StaticMeshComponent\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Whenever an object needs to serialize an object reference, we keep the object path here and serialize an index to this array.\n\nExternal references, e.g. UDataAssets or UMaterials, are easily handled.\nExample: UStaticMesh /Game/Australia/StaticMeshes/MegaScans/Nature_Rock_vbhtdixga/vbhtdixga_LOD0.vbhtdixga_LOD0\n\nInternal references, e.g. to subobjects and to other actors in the world, are a bit tricky.\nFor internal references, we need to do some translation using TranslateOriginalToSnapshotPath:\nExample original: UStaticMeshActor::StaticMeshComponent /Game/MapName.MapName:PersistentLevel.StaticMeshActor_42.StaticMeshComponent\nExample translated: UStaticMeshActor::StaticMeshComponent /Engine/Transient.World_21:PersistentLevel.StaticMeshActor_42.StaticMeshComponent" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences = { "SerializedObjectReferences", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, SerializedObjectReferences), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_ValueProp = { "Subobjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FSubobjectSnapshotData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_Key_KeyProp = { "Subobjects_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_MetaData[] = {
		{ "Comment", "/**\n\x09 * Key: A valid index in to SerializedObjectReferences.\n\x09 * Value: Subobject information for the associated entry in SerializedObjectReferences.\n\x09 * There is only an entry if the associated object is in fact a subobject. Actors and assets in particular do not get any entry.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Key: A valid index in to SerializedObjectReferences.\nValue: Subobject information for the associated entry in SerializedObjectReferences.\nThere is only an entry if the associated object is in fact a subobject. Actors and assets in particular do not get any entry." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects = { "Subobjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, Subobjects), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_ValueProp = { "CustomSubobjectSerializationData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCustomSerializationData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_Key_KeyProp = { "CustomSubobjectSerializationData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_MetaData[] = {
		{ "Comment", "/**\n\x09 * Key: A valid index in to SerializedObjectReferences\n\x09 * Value: Data that was generated by some ICustomObjectSnapshotSerializer.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/WorldSnapshotData.h" },
		{ "ToolTip", "Key: A valid index in to SerializedObjectReferences\nValue: Data that was generated by some ICustomObjectSnapshotSerializer." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData = { "CustomSubobjectSerializationData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldSnapshotData, CustomSubobjectSerializationData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SnapshotVersionInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ClassDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_ActorData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_SerializedObjectReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_Subobjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::NewProp_CustomSubobjectSerializationData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"WorldSnapshotData",
		sizeof(FWorldSnapshotData),
		alignof(FWorldSnapshotData),
		Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWorldSnapshotData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWorldSnapshotData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WorldSnapshotData"), sizeof(FWorldSnapshotData), Get_Z_Construct_UScriptStruct_FWorldSnapshotData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWorldSnapshotData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWorldSnapshotData_Hash() { return 2050967919U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
