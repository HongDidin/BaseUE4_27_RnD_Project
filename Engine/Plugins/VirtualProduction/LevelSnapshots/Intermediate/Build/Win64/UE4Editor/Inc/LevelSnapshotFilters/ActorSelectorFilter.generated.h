// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_ActorSelectorFilter_generated_h
#error "ActorSelectorFilter.generated.h already included, missing '#pragma once' in ActorSelectorFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_ActorSelectorFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorSelectorFilter(); \
	friend struct Z_Construct_UClass_UActorSelectorFilter_Statics; \
public: \
	DECLARE_CLASS(UActorSelectorFilter, ULevelSnapshotBlueprintFilter, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UActorSelectorFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUActorSelectorFilter(); \
	friend struct Z_Construct_UClass_UActorSelectorFilter_Statics; \
public: \
	DECLARE_CLASS(UActorSelectorFilter, ULevelSnapshotBlueprintFilter, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UActorSelectorFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorSelectorFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorSelectorFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorSelectorFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorSelectorFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorSelectorFilter(UActorSelectorFilter&&); \
	NO_API UActorSelectorFilter(const UActorSelectorFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorSelectorFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorSelectorFilter(UActorSelectorFilter&&); \
	NO_API UActorSelectorFilter(const UActorSelectorFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorSelectorFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorSelectorFilter); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorSelectorFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UActorSelectorFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorSelector_ActorSelectorFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
