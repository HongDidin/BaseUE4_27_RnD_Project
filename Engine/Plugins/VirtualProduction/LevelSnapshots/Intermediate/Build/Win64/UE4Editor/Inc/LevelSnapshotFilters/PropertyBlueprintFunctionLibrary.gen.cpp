// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/PropertyBlueprintFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePropertyBlueprintFunctionLibrary() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UPropertyBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsDeletedActorValidParams();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UPropertyBlueprintFunctionLibrary::execLoadSnapshotActor)
	{
		P_GET_STRUCT_REF(FIsDeletedActorValidParams,Z_Param_Out_Params);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=P_THIS->LoadSnapshotActor(Z_Param_Out_Params);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPropertyBlueprintFunctionLibrary::execGetPropertyName)
	{
		P_GET_STRUCT_REF(TFieldPath<FProperty>,Z_Param_Out_Property);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UPropertyBlueprintFunctionLibrary::GetPropertyName(Z_Param_Out_Property);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPropertyBlueprintFunctionLibrary::execGetPropertyOriginPath)
	{
		P_GET_STRUCT_REF(TFieldPath<FProperty>,Z_Param_Out_Property);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UPropertyBlueprintFunctionLibrary::GetPropertyOriginPath(Z_Param_Out_Property);
		P_NATIVE_END;
	}
	void UPropertyBlueprintFunctionLibrary::StaticRegisterNativesUPropertyBlueprintFunctionLibrary()
	{
		UClass* Class = UPropertyBlueprintFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPropertyName", &UPropertyBlueprintFunctionLibrary::execGetPropertyName },
			{ "GetPropertyOriginPath", &UPropertyBlueprintFunctionLibrary::execGetPropertyOriginPath },
			{ "LoadSnapshotActor", &UPropertyBlueprintFunctionLibrary::execLoadSnapshotActor },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics
	{
		struct PropertyBlueprintFunctionLibrary_eventGetPropertyName_Parms
		{
			TFieldPath<FProperty> Property;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_Property;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_Property_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_Property = { "Property", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PropertyBlueprintFunctionLibrary_eventGetPropertyName_Parms, Property), &FProperty::StaticClass, METADATA_PARAMS(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_Property_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PropertyBlueprintFunctionLibrary_eventGetPropertyName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Gets only the property name of a property. */" },
		{ "ModuleRelativePath", "Public/PropertyBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Gets only the property name of a property." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPropertyBlueprintFunctionLibrary, nullptr, "GetPropertyName", nullptr, nullptr, sizeof(PropertyBlueprintFunctionLibrary_eventGetPropertyName_Parms), Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics
	{
		struct PropertyBlueprintFunctionLibrary_eventGetPropertyOriginPath_Parms
		{
			TFieldPath<FProperty> Property;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_Property;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_Property_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_Property = { "Property", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PropertyBlueprintFunctionLibrary_eventGetPropertyOriginPath_Parms, Property), &FProperty::StaticClass, METADATA_PARAMS(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_Property_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PropertyBlueprintFunctionLibrary_eventGetPropertyOriginPath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Returns a path containing information which class declare the property.*/" },
		{ "ModuleRelativePath", "Public/PropertyBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Returns a path containing information which class declare the property." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPropertyBlueprintFunctionLibrary, nullptr, "GetPropertyOriginPath", nullptr, nullptr, sizeof(PropertyBlueprintFunctionLibrary_eventGetPropertyOriginPath_Parms), Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics
	{
		struct PropertyBlueprintFunctionLibrary_eventLoadSnapshotActor_Parms
		{
			FIsDeletedActorValidParams Params;
			AActor* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_Params_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PropertyBlueprintFunctionLibrary_eventLoadSnapshotActor_Parms, Params), Z_Construct_UScriptStruct_FIsDeletedActorValidParams, METADATA_PARAMS(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PropertyBlueprintFunctionLibrary_eventLoadSnapshotActor_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_Params,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Loads the actor identified by Params. You can use this for advanced filter queries.*/" },
		{ "ModuleRelativePath", "Public/PropertyBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Loads the actor identified by Params. You can use this for advanced filter queries." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPropertyBlueprintFunctionLibrary, nullptr, "LoadSnapshotActor", nullptr, nullptr, sizeof(PropertyBlueprintFunctionLibrary_eventLoadSnapshotActor_Parms), Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_NoRegister()
	{
		return UPropertyBlueprintFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyName, "GetPropertyName" }, // 2874398924
		{ &Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_GetPropertyOriginPath, "GetPropertyOriginPath" }, // 3359670279
		{ &Z_Construct_UFunction_UPropertyBlueprintFunctionLibrary_LoadSnapshotActor, "LoadSnapshotActor" }, // 1529247141
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PropertyBlueprintFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/PropertyBlueprintFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPropertyBlueprintFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::ClassParams = {
		&UPropertyBlueprintFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPropertyBlueprintFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPropertyBlueprintFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPropertyBlueprintFunctionLibrary, 82079031);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UPropertyBlueprintFunctionLibrary>()
	{
		return UPropertyBlueprintFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPropertyBlueprintFunctionLibrary(Z_Construct_UClass_UPropertyBlueprintFunctionLibrary, &UPropertyBlueprintFunctionLibrary::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UPropertyBlueprintFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPropertyBlueprintFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
