// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_PropertySelectionMap_generated_h
#error "PropertySelectionMap.generated.h already included, missing '#pragma once' in PropertySelectionMap.h"
#endif
#define LEVELSNAPSHOTS_PropertySelectionMap_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_PropertySelectionMap_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPropertySelectionMap_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__DeletedActorsToRespawn() { return STRUCT_OFFSET(FPropertySelectionMap, DeletedActorsToRespawn); } \
	FORCEINLINE static uint32 __PPO__NewActorsToDespawn() { return STRUCT_OFFSET(FPropertySelectionMap, NewActorsToDespawn); }


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FPropertySelectionMap>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_PropertySelectionMap_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
