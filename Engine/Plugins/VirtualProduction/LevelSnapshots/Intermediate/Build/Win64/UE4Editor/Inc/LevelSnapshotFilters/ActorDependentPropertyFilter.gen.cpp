// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/ActorDependentPropertyFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorDependentPropertyFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EDoNotCareHandling();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorDependentPropertyFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorDependentPropertyFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister();
// End Cross Module References
	static UEnum* EDoNotCareHandling_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LevelSnapshotFilters_EDoNotCareHandling, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("EDoNotCareHandling"));
		}
		return Singleton;
	}
	template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<EDoNotCareHandling>()
	{
		return EDoNotCareHandling_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDoNotCareHandling(EDoNotCareHandling_StaticEnum, TEXT("/Script/LevelSnapshotFilters"), TEXT("EDoNotCareHandling"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LevelSnapshotFilters_EDoNotCareHandling_Hash() { return 1231418653U; }
	UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EDoNotCareHandling()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDoNotCareHandling"), 0, Get_Z_Construct_UEnum_LevelSnapshotFilters_EDoNotCareHandling_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDoNotCareHandling::UseIncludeFilter", (int64)EDoNotCareHandling::UseIncludeFilter },
				{ "EDoNotCareHandling::UseExcludeFilter", (int64)EDoNotCareHandling::UseExcludeFilter },
				{ "EDoNotCareHandling::UseDoNotCareFilter", (int64)EDoNotCareHandling::UseDoNotCareFilter },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
				{ "UseDoNotCareFilter.Comment", "/* When IsActorValid returns DoNotCare, use RunOnDoNotCareActorFilter. */" },
				{ "UseDoNotCareFilter.Name", "EDoNotCareHandling::UseDoNotCareFilter" },
				{ "UseDoNotCareFilter.ToolTip", "When IsActorValid returns DoNotCare, use RunOnDoNotCareActorFilter." },
				{ "UseExcludeFilter.Comment", "/* When IsActorValid returns Exclude, use RunOnExcludedActorFilter. */" },
				{ "UseExcludeFilter.Name", "EDoNotCareHandling::UseExcludeFilter" },
				{ "UseExcludeFilter.ToolTip", "When IsActorValid returns Exclude, use RunOnExcludedActorFilter." },
				{ "UseIncludeFilter.Comment", "/* When IsActorValid returns Include, use RunOnIncludedActorFilter. */" },
				{ "UseIncludeFilter.Name", "EDoNotCareHandling::UseIncludeFilter" },
				{ "UseIncludeFilter.ToolTip", "When IsActorValid returns Include, use RunOnIncludedActorFilter." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
				nullptr,
				"EDoNotCareHandling",
				"EDoNotCareHandling",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UActorDependentPropertyFilter::StaticRegisterNativesUActorDependentPropertyFilter()
	{
	}
	UClass* Z_Construct_UClass_UActorDependentPropertyFilter_NoRegister()
	{
		return UActorDependentPropertyFilter::StaticClass();
	}
	struct Z_Construct_UClass_UActorDependentPropertyFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IncludePropertyFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IncludePropertyFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludePropertyFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExcludePropertyFilter;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_DoNotCareHandling_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoNotCareHandling_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DoNotCareHandling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoNotCarePropertyFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DoNotCarePropertyFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorDependentPropertyFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorDependentPropertyFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements IsActorValid and IsPropertyValid as follows:\n *\x09- IsActorValid returns ActorFilter->IsActorValid\n *\x09- IsPropertyValid runs ActorFilter->IsActorValid. Depending on its results it runs\n *\x09\x09- IncludePropertyFilter\n *\x09\x09- ExcludePropertyFilter\n *\x09\x09- DoNotCarePropertyFilter\n *\n * Use case: You want to allow certain properties when another filters would include the actor and allow different properties when excluded.\n */" },
		{ "IncludePath", "Builtin/ActorDependentPropertyFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
		{ "ToolTip", "Implements IsActorValid and IsPropertyValid as follows:\n    - IsActorValid returns ActorFilter->IsActorValid\n    - IsPropertyValid runs ActorFilter->IsActorValid. Depending on its results it runs\n            - IncludePropertyFilter\n            - ExcludePropertyFilter\n            - DoNotCarePropertyFilter\n\nUse case: You want to allow certain properties when another filters would include the actor and allow different properties when excluded." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ActorFilter_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* We run IsActorValid on this filter. IsPropertyValid uses one of the below filters depending on this filter. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
		{ "ToolTip", "We run IsActorValid on this filter. IsPropertyValid uses one of the below filters depending on this filter." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ActorFilter = { "ActorFilter", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorDependentPropertyFilter, ActorFilter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ActorFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ActorFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_IncludePropertyFilter_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* Used by IsPropertyValid when ActorFilter->IsActorValid returns Include */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
		{ "ToolTip", "Used by IsPropertyValid when ActorFilter->IsActorValid returns Include" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_IncludePropertyFilter = { "IncludePropertyFilter", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorDependentPropertyFilter, IncludePropertyFilter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_IncludePropertyFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_IncludePropertyFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ExcludePropertyFilter_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* Used by IsPropertyValid when ActorFilter->IsActorValid returns Exclude */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
		{ "ToolTip", "Used by IsPropertyValid when ActorFilter->IsActorValid returns Exclude" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ExcludePropertyFilter = { "ExcludePropertyFilter", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorDependentPropertyFilter, ExcludePropertyFilter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ExcludePropertyFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ExcludePropertyFilter_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* Determines what filter IsPropertyValid is supposed to use when IsActorValid returns DoNotCare. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
		{ "ToolTip", "Determines what filter IsPropertyValid is supposed to use when IsActorValid returns DoNotCare." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling = { "DoNotCareHandling", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorDependentPropertyFilter, DoNotCareHandling), Z_Construct_UEnum_LevelSnapshotFilters_EDoNotCareHandling, METADATA_PARAMS(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCarePropertyFilter_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* Used by IsPropertyValid when ActorFilter->IsActorValid returns DoNotCare and DoNotCareHandling == UseDoNotCareFilter. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Builtin/ActorDependentPropertyFilter.h" },
		{ "ToolTip", "Used by IsPropertyValid when ActorFilter->IsActorValid returns DoNotCare and DoNotCareHandling == UseDoNotCareFilter." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCarePropertyFilter = { "DoNotCarePropertyFilter", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorDependentPropertyFilter, DoNotCarePropertyFilter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCarePropertyFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCarePropertyFilter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActorDependentPropertyFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ActorFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_IncludePropertyFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_ExcludePropertyFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCareHandling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorDependentPropertyFilter_Statics::NewProp_DoNotCarePropertyFilter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorDependentPropertyFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorDependentPropertyFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorDependentPropertyFilter_Statics::ClassParams = {
		&UActorDependentPropertyFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UActorDependentPropertyFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorDependentPropertyFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorDependentPropertyFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorDependentPropertyFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorDependentPropertyFilter, 144584058);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UActorDependentPropertyFilter>()
	{
		return UActorDependentPropertyFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorDependentPropertyFilter(Z_Construct_UClass_UActorDependentPropertyFilter, &UActorDependentPropertyFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UActorDependentPropertyFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorDependentPropertyFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
