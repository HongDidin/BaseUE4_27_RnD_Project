// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FIsAddedActorValidParams;
struct FIsDeletedActorValidParams;
struct FIsPropertyValidParams;
struct FIsActorValidParams;
#ifdef LEVELSNAPSHOTFILTERS_LevelSnapshotFilters_generated_h
#error "LevelSnapshotFilters.generated.h already included, missing '#pragma once' in LevelSnapshotFilters.h"
#endif
#define LEVELSNAPSHOTFILTERS_LevelSnapshotFilters_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotFilter(); \
	friend struct Z_Construct_UClass_ULevelSnapshotFilter_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotFilter, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotFilter(); \
	friend struct Z_Construct_UClass_ULevelSnapshotFilter_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotFilter, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotFilter(ULevelSnapshotFilter&&); \
	NO_API ULevelSnapshotFilter(const ULevelSnapshotFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotFilter(ULevelSnapshotFilter&&); \
	NO_API ULevelSnapshotFilter(const ULevelSnapshotFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotFilter); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_62_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_65_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class ULevelSnapshotFilter>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_RPC_WRAPPERS \
	virtual EFilterResult::Type IsAddedActorValid_Implementation(FIsAddedActorValidParams const& Params) const; \
	virtual EFilterResult::Type IsDeletedActorValid_Implementation(FIsDeletedActorValidParams const& Params) const; \
	virtual EFilterResult::Type IsPropertyValid_Implementation(FIsPropertyValidParams const& Params) const; \
	virtual EFilterResult::Type IsActorValid_Implementation(FIsActorValidParams const& Params) const; \
 \
	DECLARE_FUNCTION(execIsAddedActorValid); \
	DECLARE_FUNCTION(execIsDeletedActorValid); \
	DECLARE_FUNCTION(execIsPropertyValid); \
	DECLARE_FUNCTION(execIsActorValid);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual EFilterResult::Type IsAddedActorValid_Implementation(FIsAddedActorValidParams const& Params) const; \
	virtual EFilterResult::Type IsDeletedActorValid_Implementation(FIsDeletedActorValidParams const& Params) const; \
	virtual EFilterResult::Type IsPropertyValid_Implementation(FIsPropertyValidParams const& Params) const; \
	virtual EFilterResult::Type IsActorValid_Implementation(FIsActorValidParams const& Params) const; \
 \
	DECLARE_FUNCTION(execIsAddedActorValid); \
	DECLARE_FUNCTION(execIsDeletedActorValid); \
	DECLARE_FUNCTION(execIsPropertyValid); \
	DECLARE_FUNCTION(execIsActorValid);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_EVENT_PARMS \
	struct LevelSnapshotBlueprintFilter_eventIsActorValid_Parms \
	{ \
		FIsActorValidParams Params; \
		TEnumAsByte<EFilterResult::Type> ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		LevelSnapshotBlueprintFilter_eventIsActorValid_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct LevelSnapshotBlueprintFilter_eventIsAddedActorValid_Parms \
	{ \
		FIsAddedActorValidParams Params; \
		TEnumAsByte<EFilterResult::Type> ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		LevelSnapshotBlueprintFilter_eventIsAddedActorValid_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct LevelSnapshotBlueprintFilter_eventIsDeletedActorValid_Parms \
	{ \
		FIsDeletedActorValidParams Params; \
		TEnumAsByte<EFilterResult::Type> ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		LevelSnapshotBlueprintFilter_eventIsDeletedActorValid_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct LevelSnapshotBlueprintFilter_eventIsPropertyValid_Parms \
	{ \
		FIsPropertyValidParams Params; \
		TEnumAsByte<EFilterResult::Type> ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		LevelSnapshotBlueprintFilter_eventIsPropertyValid_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	};


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_CALLBACK_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotBlueprintFilter(); \
	friend struct Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotBlueprintFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotBlueprintFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotBlueprintFilter(); \
	friend struct Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotBlueprintFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotBlueprintFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotBlueprintFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotBlueprintFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotBlueprintFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotBlueprintFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotBlueprintFilter(ULevelSnapshotBlueprintFilter&&); \
	NO_API ULevelSnapshotBlueprintFilter(const ULevelSnapshotBlueprintFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotBlueprintFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotBlueprintFilter(ULevelSnapshotBlueprintFilter&&); \
	NO_API ULevelSnapshotBlueprintFilter(const ULevelSnapshotBlueprintFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotBlueprintFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotBlueprintFilter); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotBlueprintFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_90_PROLOG \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_EVENT_PARMS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h_93_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class ULevelSnapshotBlueprintFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilters_h


#define FOREACH_ENUM_EFILTERRESULT(op) \
	op(EFilterResult::Include) \
	op(EFilterResult::Exclude) \
	op(EFilterResult::DoNotCare) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
