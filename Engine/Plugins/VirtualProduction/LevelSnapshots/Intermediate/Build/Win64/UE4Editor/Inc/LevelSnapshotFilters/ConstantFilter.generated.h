// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_ConstantFilter_generated_h
#error "ConstantFilter.generated.h already included, missing '#pragma once' in ConstantFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_ConstantFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConstantFilter(); \
	friend struct Z_Construct_UClass_UConstantFilter_Statics; \
public: \
	DECLARE_CLASS(UConstantFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UConstantFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUConstantFilter(); \
	friend struct Z_Construct_UClass_UConstantFilter_Statics; \
public: \
	DECLARE_CLASS(UConstantFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UConstantFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConstantFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConstantFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConstantFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConstantFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConstantFilter(UConstantFilter&&); \
	NO_API UConstantFilter(const UConstantFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConstantFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConstantFilter(UConstantFilter&&); \
	NO_API UConstantFilter(const UConstantFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConstantFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConstantFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConstantFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UConstantFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ConstantFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
