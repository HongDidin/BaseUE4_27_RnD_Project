// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/FilteredResults.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFilteredResults() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFilteredResults_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFilteredResults();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FPropertySelectionMap();
// End Cross Module References
	void UFilteredResults::StaticRegisterNativesUFilteredResults()
	{
	}
	UClass* Z_Construct_UClass_UFilteredResults_NoRegister()
	{
		return UFilteredResults::StaticClass();
	}
	struct Z_Construct_UClass_UFilteredResults_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertiesToRollback_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PropertiesToRollback;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFilteredResults_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFilteredResults_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* Processes user defined filters into a selection set, which the user inspect in the results tab. */" },
		{ "IncludePath", "Data/FilteredResults.h" },
		{ "ModuleRelativePath", "Private/Data/FilteredResults.h" },
		{ "ToolTip", "Processes user defined filters into a selection set, which the user inspect in the results tab." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFilteredResults_Statics::NewProp_PropertiesToRollback_MetaData[] = {
		{ "Comment", "/* Null until UpdatePropertiesToRollback is called. */" },
		{ "ModuleRelativePath", "Private/Data/FilteredResults.h" },
		{ "ToolTip", "Null until UpdatePropertiesToRollback is called." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFilteredResults_Statics::NewProp_PropertiesToRollback = { "PropertiesToRollback", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFilteredResults, PropertiesToRollback), Z_Construct_UScriptStruct_FPropertySelectionMap, METADATA_PARAMS(Z_Construct_UClass_UFilteredResults_Statics::NewProp_PropertiesToRollback_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFilteredResults_Statics::NewProp_PropertiesToRollback_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFilteredResults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFilteredResults_Statics::NewProp_PropertiesToRollback,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFilteredResults_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFilteredResults>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFilteredResults_Statics::ClassParams = {
		&UFilteredResults::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFilteredResults_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFilteredResults_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFilteredResults_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFilteredResults_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFilteredResults()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFilteredResults_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFilteredResults, 402845314);
	template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<UFilteredResults>()
	{
		return UFilteredResults::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFilteredResults(Z_Construct_UClass_UFilteredResults, &UFilteredResults::StaticClass, TEXT("/Script/LevelSnapshotsEditor"), TEXT("UFilteredResults"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFilteredResults);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
