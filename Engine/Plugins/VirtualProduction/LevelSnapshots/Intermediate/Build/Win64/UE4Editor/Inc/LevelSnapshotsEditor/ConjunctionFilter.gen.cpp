// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/Filters/ConjunctionFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConjunctionFilter() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UConjunctionFilter_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UConjunctionFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UNegatableFilter_NoRegister();
// End Cross Module References
	void UConjunctionFilter::StaticRegisterNativesUConjunctionFilter()
	{
	}
	UClass* Z_Construct_UClass_UConjunctionFilter_NoRegister()
	{
		return UConjunctionFilter::StaticClass();
	}
	struct Z_Construct_UClass_UConjunctionFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Children;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreFilter_MetaData[];
#endif
		static void NewProp_bIgnoreFilter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConjunctionFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConjunctionFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Returns the result of and-ing all child filters.\n * It is valid to have no children: in this case, this filter return false.\n */" },
		{ "IncludePath", "Data/Filters/ConjunctionFilter.h" },
		{ "InternalSnapshotFilter", "" },
		{ "ModuleRelativePath", "Private/Data/Filters/ConjunctionFilter.h" },
		{ "ToolTip", "* Returns the result of and-ing all child filters.\n* It is valid to have no children: in this case, this filter return false." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_Inner_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Data/Filters/ConjunctionFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_Inner = { "Children", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNegatableFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Data/Filters/ConjunctionFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConjunctionFilter, Children), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter_MetaData[] = {
		{ "ModuleRelativePath", "Private/Data/Filters/ConjunctionFilter.h" },
	};
#endif
	void Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter_SetBit(void* Obj)
	{
		((UConjunctionFilter*)Obj)->bIgnoreFilter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter = { "bIgnoreFilter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConjunctionFilter), &Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConjunctionFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_Children,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConjunctionFilter_Statics::NewProp_bIgnoreFilter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConjunctionFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConjunctionFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConjunctionFilter_Statics::ClassParams = {
		&UConjunctionFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConjunctionFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConjunctionFilter_Statics::PropPointers),
		0,
		0x008010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConjunctionFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConjunctionFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConjunctionFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConjunctionFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConjunctionFilter, 740063430);
	template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<UConjunctionFilter>()
	{
		return UConjunctionFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConjunctionFilter(Z_Construct_UClass_UConjunctionFilter, &UConjunctionFilter::StaticClass, TEXT("/Script/LevelSnapshotsEditor"), TEXT("UConjunctionFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConjunctionFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
