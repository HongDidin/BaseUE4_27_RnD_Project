// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/ActorSnapshotData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorSnapshotData() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FActorSnapshotData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FObjectSnapshotData();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FCustomSerializationData();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FComponentSnapshotData();
// End Cross Module References
class UScriptStruct* FActorSnapshotData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FActorSnapshotData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FActorSnapshotData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("ActorSnapshotData"), sizeof(FActorSnapshotData), Get_Z_Construct_UScriptStruct_FActorSnapshotData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FActorSnapshotData>()
{
	return FActorSnapshotData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FActorSnapshotData(FActorSnapshotData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("ActorSnapshotData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFActorSnapshotData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFActorSnapshotData()
	{
		UScriptStruct::DeferCppStructOps<FActorSnapshotData>(FName(TEXT("ActorSnapshotData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFActorSnapshotData;
	struct Z_Construct_UScriptStruct_FActorSnapshotData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedSnapshotActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_CachedSnapshotActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReceivedSerialisation_MetaData[];
#endif
		static void NewProp_bReceivedSerialisation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReceivedSerialisation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedActorData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedActorData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomActorSerializationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomActorSerializationData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ComponentData_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ComponentData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ComponentData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// TODO: Move these functions into static helper functions in private part of module.\n" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "TODO: Move these functions into static helper functions in private part of module." },
	};
#endif
	void* Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FActorSnapshotData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CachedSnapshotActor_MetaData[] = {
		{ "Comment", "/* We cache the actor to avoid recreating it all the time */" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "We cache the actor to avoid recreating it all the time" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CachedSnapshotActor = { "CachedSnapshotActor", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorSnapshotData, CachedSnapshotActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CachedSnapshotActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CachedSnapshotActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation_MetaData[] = {
		{ "Comment", "/* Whether we already serialised the snapshot data into the actor.\n\x09 * \n\x09 * This exists because sometimes we need to preallocate an actor without serialisation.\n\x09 * Example: When serializing another actor which referenced this actor.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "Whether we already serialised the snapshot data into the actor.\n       *\n       * This exists because sometimes we need to preallocate an actor without serialisation.\n       * Example: When serializing another actor which referenced this actor." },
	};
#endif
	void Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation_SetBit(void* Obj)
	{
		((FActorSnapshotData*)Obj)->bReceivedSerialisation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation = { "bReceivedSerialisation", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FActorSnapshotData), &Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ActorClass_MetaData[] = {
		{ "Comment", "/** The class the actor had when it was saved. */" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "The class the actor had when it was saved." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ActorClass = { "ActorClass", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorSnapshotData, ActorClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_SerializedActorData_MetaData[] = {
		{ "Comment", "/** The actor's serialized data */" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "The actor's serialized data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_SerializedActorData = { "SerializedActorData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorSnapshotData, SerializedActorData), Z_Construct_UScriptStruct_FObjectSnapshotData, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_SerializedActorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_SerializedActorData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CustomActorSerializationData_MetaData[] = {
		{ "Comment", "/** Data that was generated by some ICustomObjectSnapshotSerializer. Needed to restore custom subobjects. */" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "Data that was generated by some ICustomObjectSnapshotSerializer. Needed to restore custom subobjects." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CustomActorSerializationData = { "CustomActorSerializationData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorSnapshotData, CustomActorSerializationData), Z_Construct_UScriptStruct_FCustomSerializationData, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CustomActorSerializationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CustomActorSerializationData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_ValueProp = { "ComponentData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FComponentSnapshotData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_Key_KeyProp = { "ComponentData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_MetaData[] = {
		{ "Comment", "/** Additional component recreation info */" },
		{ "ModuleRelativePath", "Public/Data/ActorSnapshotData.h" },
		{ "ToolTip", "Additional component recreation info" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData = { "ComponentData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorSnapshotData, ComponentData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FActorSnapshotData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CachedSnapshotActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_bReceivedSerialisation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_SerializedActorData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_CustomActorSerializationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorSnapshotData_Statics::NewProp_ComponentData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FActorSnapshotData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"ActorSnapshotData",
		sizeof(FActorSnapshotData),
		alignof(FActorSnapshotData),
		Z_Construct_UScriptStruct_FActorSnapshotData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorSnapshotData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FActorSnapshotData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FActorSnapshotData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ActorSnapshotData"), sizeof(FActorSnapshotData), Get_Z_Construct_UScriptStruct_FActorSnapshotData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FActorSnapshotData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FActorSnapshotData_Hash() { return 1599606327U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
