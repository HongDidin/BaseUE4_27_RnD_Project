// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/ComponentSnapshotData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComponentSnapshotData() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FComponentSnapshotData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EComponentCreationMethod();
// End Cross Module References
class UScriptStruct* FComponentSnapshotData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FComponentSnapshotData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FComponentSnapshotData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("ComponentSnapshotData"), sizeof(FComponentSnapshotData), Get_Z_Construct_UScriptStruct_FComponentSnapshotData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FComponentSnapshotData>()
{
	return FComponentSnapshotData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FComponentSnapshotData(FComponentSnapshotData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("ComponentSnapshotData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFComponentSnapshotData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFComponentSnapshotData()
	{
		UScriptStruct::DeferCppStructOps<FComponentSnapshotData>(FName(TEXT("ComponentSnapshotData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFComponentSnapshotData;
	struct Z_Construct_UScriptStruct_FComponentSnapshotData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CreationMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreationMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CreationMethod;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Data/ComponentSnapshotData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FComponentSnapshotData>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod_MetaData[] = {
		{ "Comment", "/* Describes how the component was created */" },
		{ "ModuleRelativePath", "Public/Data/ComponentSnapshotData.h" },
		{ "ToolTip", "Describes how the component was created" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod = { "CreationMethod", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComponentSnapshotData, CreationMethod), Z_Construct_UEnum_Engine_EComponentCreationMethod, METADATA_PARAMS(Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::NewProp_CreationMethod,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"ComponentSnapshotData",
		sizeof(FComponentSnapshotData),
		alignof(FComponentSnapshotData),
		Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FComponentSnapshotData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FComponentSnapshotData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ComponentSnapshotData"), sizeof(FComponentSnapshotData), Get_Z_Construct_UScriptStruct_FComponentSnapshotData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FComponentSnapshotData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FComponentSnapshotData_Hash() { return 40519732U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
