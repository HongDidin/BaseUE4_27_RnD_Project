// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_TransformPropertyFilter_generated_h
#error "TransformPropertyFilter.generated.h already included, missing '#pragma once' in TransformPropertyFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_TransformPropertyFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTransformPropertyFilter(); \
	friend struct Z_Construct_UClass_UTransformPropertyFilter_Statics; \
public: \
	DECLARE_CLASS(UTransformPropertyFilter, UPropertySelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UTransformPropertyFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUTransformPropertyFilter(); \
	friend struct Z_Construct_UClass_UTransformPropertyFilter_Statics; \
public: \
	DECLARE_CLASS(UTransformPropertyFilter, UPropertySelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UTransformPropertyFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformPropertyFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformPropertyFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformPropertyFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformPropertyFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformPropertyFilter(UTransformPropertyFilter&&); \
	NO_API UTransformPropertyFilter(const UTransformPropertyFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTransformPropertyFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTransformPropertyFilter(UTransformPropertyFilter&&); \
	NO_API UTransformPropertyFilter(const UTransformPropertyFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTransformPropertyFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTransformPropertyFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTransformPropertyFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UTransformPropertyFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertySelector_TransformPropertyFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
