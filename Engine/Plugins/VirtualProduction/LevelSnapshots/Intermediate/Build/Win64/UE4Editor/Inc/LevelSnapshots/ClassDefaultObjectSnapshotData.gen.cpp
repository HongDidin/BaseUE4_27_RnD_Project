// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/ClassDefaultObjectSnapshotData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeClassDefaultObjectSnapshotData() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FObjectSnapshotData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FClassDefaultObjectSnapshotData>() == std::is_polymorphic<FObjectSnapshotData>(), "USTRUCT FClassDefaultObjectSnapshotData cannot be polymorphic unless super FObjectSnapshotData is polymorphic");

class UScriptStruct* FClassDefaultObjectSnapshotData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("ClassDefaultObjectSnapshotData"), sizeof(FClassDefaultObjectSnapshotData), Get_Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FClassDefaultObjectSnapshotData>()
{
	return FClassDefaultObjectSnapshotData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FClassDefaultObjectSnapshotData(FClassDefaultObjectSnapshotData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("ClassDefaultObjectSnapshotData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFClassDefaultObjectSnapshotData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFClassDefaultObjectSnapshotData()
	{
		UScriptStruct::DeferCppStructOps<FClassDefaultObjectSnapshotData>(FName(TEXT("ClassDefaultObjectSnapshotData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFClassDefaultObjectSnapshotData;
	struct Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedLoadedClassDefault_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedLoadedClassDefault;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Data/ClassDefaultObjectSnapshotData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FClassDefaultObjectSnapshotData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::NewProp_CachedLoadedClassDefault_MetaData[] = {
		{ "Comment", "/* Holds a value if the value was already loaded from the snapshot. */" },
		{ "ModuleRelativePath", "Public/Data/ClassDefaultObjectSnapshotData.h" },
		{ "ToolTip", "Holds a value if the value was already loaded from the snapshot." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::NewProp_CachedLoadedClassDefault = { "CachedLoadedClassDefault", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClassDefaultObjectSnapshotData, CachedLoadedClassDefault), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::NewProp_CachedLoadedClassDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::NewProp_CachedLoadedClassDefault_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::NewProp_CachedLoadedClassDefault,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		Z_Construct_UScriptStruct_FObjectSnapshotData,
		&NewStructOps,
		"ClassDefaultObjectSnapshotData",
		sizeof(FClassDefaultObjectSnapshotData),
		alignof(FClassDefaultObjectSnapshotData),
		Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ClassDefaultObjectSnapshotData"), sizeof(FClassDefaultObjectSnapshotData), Get_Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FClassDefaultObjectSnapshotData_Hash() { return 2720320856U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
