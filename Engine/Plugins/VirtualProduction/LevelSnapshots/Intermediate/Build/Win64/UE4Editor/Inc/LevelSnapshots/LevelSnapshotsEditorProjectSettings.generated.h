// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_LevelSnapshotsEditorProjectSettings_generated_h
#error "LevelSnapshotsEditorProjectSettings.generated.h already included, missing '#pragma once' in LevelSnapshotsEditorProjectSettings.h"
#endif
#define LEVELSNAPSHOTS_LevelSnapshotsEditorProjectSettings_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorProjectSettings(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorProjectSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorProjectSettings(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorProjectSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorProjectSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotsEditorProjectSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorProjectSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorProjectSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorProjectSettings(ULevelSnapshotsEditorProjectSettings&&); \
	NO_API ULevelSnapshotsEditorProjectSettings(const ULevelSnapshotsEditorProjectSettings&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorProjectSettings(ULevelSnapshotsEditorProjectSettings&&); \
	NO_API ULevelSnapshotsEditorProjectSettings(const ULevelSnapshotsEditorProjectSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorProjectSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorProjectSettings)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_8_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class ULevelSnapshotsEditorProjectSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorProjectSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
