// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_NegatableFilter_generated_h
#error "NegatableFilter.generated.h already included, missing '#pragma once' in NegatableFilter.h"
#endif
#define LEVELSNAPSHOTSEDITOR_NegatableFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNegatableFilter(); \
	friend struct Z_Construct_UClass_UNegatableFilter_Statics; \
public: \
	DECLARE_CLASS(UNegatableFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(UNegatableFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUNegatableFilter(); \
	friend struct Z_Construct_UClass_UNegatableFilter_Statics; \
public: \
	DECLARE_CLASS(UNegatableFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(UNegatableFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNegatableFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNegatableFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNegatableFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNegatableFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNegatableFilter(UNegatableFilter&&); \
	NO_API UNegatableFilter(const UNegatableFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNegatableFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNegatableFilter(UNegatableFilter&&); \
	NO_API UNegatableFilter(const UNegatableFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNegatableFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNegatableFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNegatableFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FilterBehavior() { return STRUCT_OFFSET(UNegatableFilter, FilterBehavior); } \
	FORCEINLINE static uint32 __PPO__bIgnoreFilter() { return STRUCT_OFFSET(UNegatableFilter, bIgnoreFilter); } \
	FORCEINLINE static uint32 __PPO__ChildFilter() { return STRUCT_OFFSET(UNegatableFilter, ChildFilter); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_21_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class UNegatableFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_Filters_NegatableFilter_h


#define FOREACH_ENUM_EFILTERBEHAVIOR(op) \
	op(EFilterBehavior::DoNotNegate) \
	op(EFilterBehavior::Negate) 

enum class EFilterBehavior : uint8;
template<> LEVELSNAPSHOTSEDITOR_API UEnum* StaticEnum<EFilterBehavior>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
