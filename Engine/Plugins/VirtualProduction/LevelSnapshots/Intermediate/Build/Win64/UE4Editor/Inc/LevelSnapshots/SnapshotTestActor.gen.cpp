// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Private/Tests/SnapshotTestActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnapshotTestActor() {}
// Cross Module References
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_USubSubobject_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_USubSubobject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_USubobject_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_USubobject();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_USnapshotTestComponent_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_USnapshotTestComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ASnapshotTestActor_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ASnapshotTestActor();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPointLightComponent_NoRegister();
// End Cross Module References
	void USubSubobject::StaticRegisterNativesUSubSubobject()
	{
	}
	UClass* Z_Construct_UClass_USubSubobject_NoRegister()
	{
		return USubSubobject::StaticClass();
	}
	struct Z_Construct_UClass_USubSubobject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProperty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USubSubobject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubSubobject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/SnapshotTestActor.h" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubSubobject_Statics::NewProp_IntProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USubSubobject_Statics::NewProp_IntProperty = { "IntProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubSubobject, IntProperty), METADATA_PARAMS(Z_Construct_UClass_USubSubobject_Statics::NewProp_IntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubSubobject_Statics::NewProp_IntProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubSubobject_Statics::NewProp_FloatProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USubSubobject_Statics::NewProp_FloatProperty = { "FloatProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubSubobject, FloatProperty), METADATA_PARAMS(Z_Construct_UClass_USubSubobject_Statics::NewProp_FloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubSubobject_Statics::NewProp_FloatProperty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USubSubobject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubSubobject_Statics::NewProp_IntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubSubobject_Statics::NewProp_FloatProperty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USubSubobject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USubSubobject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USubSubobject_Statics::ClassParams = {
		&USubSubobject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USubSubobject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USubSubobject_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USubSubobject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USubSubobject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USubSubobject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USubSubobject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USubSubobject, 956772404);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<USubSubobject>()
	{
		return USubSubobject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USubSubobject(Z_Construct_UClass_USubSubobject, &USubSubobject::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("USubSubobject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USubSubobject);
	void USubobject::StaticRegisterNativesUSubobject()
	{
	}
	UClass* Z_Construct_UClass_USubobject_NoRegister()
	{
		return USubobject::StaticClass();
	}
	struct Z_Construct_UClass_USubobject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NestedChild_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NestedChild;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USubobject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubobject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/SnapshotTestActor.h" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubobject_Statics::NewProp_IntProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USubobject_Statics::NewProp_IntProperty = { "IntProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubobject, IntProperty), METADATA_PARAMS(Z_Construct_UClass_USubobject_Statics::NewProp_IntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubobject_Statics::NewProp_IntProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubobject_Statics::NewProp_FloatProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USubobject_Statics::NewProp_FloatProperty = { "FloatProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubobject, FloatProperty), METADATA_PARAMS(Z_Construct_UClass_USubobject_Statics::NewProp_FloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubobject_Statics::NewProp_FloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubobject_Statics::NewProp_NestedChild_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USubobject_Statics::NewProp_NestedChild = { "NestedChild", nullptr, (EPropertyFlags)0x0012000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubobject, NestedChild), Z_Construct_UClass_USubSubobject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USubobject_Statics::NewProp_NestedChild_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubobject_Statics::NewProp_NestedChild_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USubobject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubobject_Statics::NewProp_IntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubobject_Statics::NewProp_FloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubobject_Statics::NewProp_NestedChild,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USubobject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USubobject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USubobject_Statics::ClassParams = {
		&USubobject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USubobject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USubobject_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USubobject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USubobject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USubobject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USubobject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USubobject, 1823252535);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<USubobject>()
	{
		return USubobject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USubobject(Z_Construct_UClass_USubobject, &USubobject::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("USubobject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USubobject);
	void USnapshotTestComponent::StaticRegisterNativesUSnapshotTestComponent()
	{
	}
	UClass* Z_Construct_UClass_USnapshotTestComponent_NoRegister()
	{
		return USnapshotTestComponent::StaticClass();
	}
	struct Z_Construct_UClass_USnapshotTestComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subobject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Subobject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USnapshotTestComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnapshotTestComponent_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/SnapshotTestActor.h" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_IntProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_IntProperty = { "IntProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USnapshotTestComponent, IntProperty), METADATA_PARAMS(Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_IntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_IntProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_FloatProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_FloatProperty = { "FloatProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USnapshotTestComponent, FloatProperty), METADATA_PARAMS(Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_FloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_FloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_Subobject_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_Subobject = { "Subobject", nullptr, (EPropertyFlags)0x0012000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USnapshotTestComponent, Subobject), Z_Construct_UClass_USubobject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_Subobject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_Subobject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USnapshotTestComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_IntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_FloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USnapshotTestComponent_Statics::NewProp_Subobject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USnapshotTestComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USnapshotTestComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USnapshotTestComponent_Statics::ClassParams = {
		&USnapshotTestComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USnapshotTestComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USnapshotTestComponent_Statics::PropPointers),
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USnapshotTestComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USnapshotTestComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USnapshotTestComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USnapshotTestComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USnapshotTestComponent, 1073380140);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<USnapshotTestComponent>()
	{
		return USnapshotTestComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USnapshotTestComponent(Z_Construct_UClass_USnapshotTestComponent, &USnapshotTestComponent::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("USnapshotTestComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USnapshotTestComponent);
	void ASnapshotTestActor::StaticRegisterNativesASnapshotTestActor()
	{
	}
	UClass* Z_Construct_UClass_ASnapshotTestActor_NoRegister()
	{
		return ASnapshotTestActor::StaticClass();
	}
	struct Z_Construct_UClass_ASnapshotTestActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeprecatedProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeprecatedProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransientProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TransientProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectReference_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectReference;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ObjectSet;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ObjectMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ObjectMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SoftPath;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SoftPathArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftPathArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SoftPathArray;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SoftPathSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftPathSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_SoftPathSet;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SoftPathMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SoftPathMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftPathMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SoftPathMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftObjectPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SoftObjectPtr;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SoftObjectPtrArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftObjectPtrArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SoftObjectPtrArray;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SoftObjectPtrSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftObjectPtrSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_SoftObjectPtrSet;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SoftObjectPtrMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SoftObjectPtrMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftObjectPtrMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SoftObjectPtrMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeakObjectPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_WeakObjectPtr;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_WeakObjectPtrArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeakObjectPtrArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WeakObjectPtrArray;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_WeakObjectPtrSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeakObjectPtrSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_WeakObjectPtrSet;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_WeakObjectPtrMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_WeakObjectPtrMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeakObjectPtrMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_WeakObjectPtrMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalComponentReference_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExternalComponentReference;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalComponentReferenceAsUObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExternalComponentReferenceAsUObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GradientLinearMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GradientLinearMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GradientRadialMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GradientRadialMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CubeMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CylinderMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CylinderMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstancedMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstancedMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointLightComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointLightComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TestComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TestComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditableInstancedSubobject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EditableInstancedSubobject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstancedSubobject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstancedSubobject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NakedSubobject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NakedSubobject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnapshotTestActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Tests/SnapshotTestActor.h" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_DeprecatedProperty_MetaData[] = {
		{ "Comment", "/******************** Skipped properties  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** Skipped properties  *******************" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_DeprecatedProperty = { "DeprecatedProperty", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, DeprecatedProperty_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_DeprecatedProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_DeprecatedProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TransientProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TransientProperty = { "TransientProperty", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, TransientProperty), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TransientProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TransientProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_IntProperty_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_IntProperty = { "IntProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, IntProperty), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_IntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_IntProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectReference_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** Raw references  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** Raw references  *******************" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectReference = { "ObjectReference", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, ObjectReference), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectReference_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectReference_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray_Inner = { "ObjectArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray = { "ObjectArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, ObjectArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet_ElementProp = { "ObjectSet", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet = { "ObjectSet", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, ObjectSet), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_ValueProp = { "ObjectMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_Key_KeyProp = { "ObjectMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap = { "ObjectMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, ObjectMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPath_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** FSoftObjectPath  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** FSoftObjectPath  *******************" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPath = { "SoftPath", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftPath), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPath_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray_Inner = { "SoftPathArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray = { "SoftPathArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftPathArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet_ElementProp = { "SoftPathSet", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet = { "SoftPathSet", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftPathSet), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_ValueProp = { "SoftPathMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_Key_KeyProp = { "SoftPathMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap = { "SoftPathMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftPathMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtr_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** TSoftObjectPtr  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** TSoftObjectPtr  *******************" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtr = { "SoftObjectPtr", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftObjectPtr), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtr_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray_Inner = { "SoftObjectPtrArray", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray = { "SoftObjectPtrArray", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftObjectPtrArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet_ElementProp = { "SoftObjectPtrSet", nullptr, (EPropertyFlags)0x0004000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet = { "SoftObjectPtrSet", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftObjectPtrSet), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_ValueProp = { "SoftObjectPtrMap", nullptr, (EPropertyFlags)0x0004000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_Key_KeyProp = { "SoftObjectPtrMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap = { "SoftObjectPtrMap", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, SoftObjectPtrMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtr_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** TWeakObjectPtr  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** TWeakObjectPtr  *******************" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtr = { "WeakObjectPtr", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, WeakObjectPtr), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtr_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtr_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray_Inner = { "WeakObjectPtrArray", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray = { "WeakObjectPtrArray", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, WeakObjectPtrArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet_ElementProp = { "WeakObjectPtrSet", nullptr, (EPropertyFlags)0x0004000000000001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet = { "WeakObjectPtrSet", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, WeakObjectPtrSet), METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_ValueProp = { "WeakObjectPtrMap", nullptr, (EPropertyFlags)0x0004000000000001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_Key_KeyProp = { "WeakObjectPtrMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap = { "WeakObjectPtrMap", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, WeakObjectPtrMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReference_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** External component references  ********************/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** External component references  *******************" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReference = { "ExternalComponentReference", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, ExternalComponentReference), Z_Construct_UClass_UActorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReference_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReference_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReferenceAsUObject_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReferenceAsUObject = { "ExternalComponentReferenceAsUObject", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, ExternalComponentReferenceAsUObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReferenceAsUObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReferenceAsUObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientLinearMaterial_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** External references  ********************/" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** External references  *******************" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientLinearMaterial = { "GradientLinearMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, GradientLinearMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientLinearMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientLinearMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientRadialMaterial_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientRadialMaterial = { "GradientRadialMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, GradientRadialMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientRadialMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientRadialMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CubeMesh_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CubeMesh = { "CubeMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, CubeMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CubeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CubeMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CylinderMesh_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CylinderMesh = { "CylinderMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, CylinderMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CylinderMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CylinderMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedMeshComponent_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** Subobject Component references  ********************/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** Subobject Component references  *******************" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedMeshComponent = { "InstancedMeshComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, InstancedMeshComponent), Z_Construct_UClass_UInstancedStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_PointLightComponent_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_PointLightComponent = { "PointLightComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, PointLightComponent), Z_Construct_UClass_UPointLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_PointLightComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_PointLightComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TestComponent_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TestComponent = { "TestComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, TestComponent), Z_Construct_UClass_USnapshotTestComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TestComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TestComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_EditableInstancedSubobject_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/******************** Subobject references  ********************/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
		{ "ToolTip", "***************** Subobject references  *******************" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_EditableInstancedSubobject = { "EditableInstancedSubobject", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, EditableInstancedSubobject), Z_Construct_UClass_USubobject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_EditableInstancedSubobject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_EditableInstancedSubobject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedSubobject_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedSubobject = { "InstancedSubobject", nullptr, (EPropertyFlags)0x0012000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, InstancedSubobject), Z_Construct_UClass_USubobject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedSubobject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedSubobject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_NakedSubobject_MetaData[] = {
		{ "ModuleRelativePath", "Private/Tests/SnapshotTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_NakedSubobject = { "NakedSubobject", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnapshotTestActor, NakedSubobject), Z_Construct_UClass_USubobject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_NakedSubobject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_NakedSubobject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASnapshotTestActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_DeprecatedProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TransientProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_IntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectReference,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ObjectMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftPathMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_SoftObjectPtrMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_WeakObjectPtrMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReference,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_ExternalComponentReferenceAsUObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientLinearMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_GradientRadialMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CubeMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_CylinderMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_PointLightComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_TestComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_EditableInstancedSubobject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_InstancedSubobject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnapshotTestActor_Statics::NewProp_NakedSubobject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnapshotTestActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnapshotTestActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnapshotTestActor_Statics::ClassParams = {
		&ASnapshotTestActor::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASnapshotTestActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASnapshotTestActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnapshotTestActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnapshotTestActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnapshotTestActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnapshotTestActor, 352704787);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<ASnapshotTestActor>()
	{
		return ASnapshotTestActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnapshotTestActor(Z_Construct_UClass_ASnapshotTestActor, &ASnapshotTestActor::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("ASnapshotTestActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnapshotTestActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
