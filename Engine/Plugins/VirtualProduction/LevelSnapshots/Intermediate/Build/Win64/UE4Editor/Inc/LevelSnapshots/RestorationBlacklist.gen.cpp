// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Settings/RestorationBlacklist.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRestorationBlacklist() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FRestorationBlacklist();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent_NoRegister();
// End Cross Module References
class UScriptStruct* FRestorationBlacklist::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FRestorationBlacklist_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRestorationBlacklist, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("RestorationBlacklist"), sizeof(FRestorationBlacklist), Get_Z_Construct_UScriptStruct_FRestorationBlacklist_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FRestorationBlacklist>()
{
	return FRestorationBlacklist::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRestorationBlacklist(FRestorationBlacklist::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("RestorationBlacklist"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFRestorationBlacklist
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFRestorationBlacklist()
	{
		UScriptStruct::DeferCppStructOps<FRestorationBlacklist>(FName(TEXT("RestorationBlacklist")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFRestorationBlacklist;
	struct Z_Construct_UScriptStruct_FRestorationBlacklist_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClasses_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ActorClasses;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ComponentClasses_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ComponentClasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Settings/RestorationBlacklist.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRestorationBlacklist>();
	}
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses_ElementProp = { "ActorClasses", nullptr, (EPropertyFlags)0x0004000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* These actor classes are not allowed. Child classes are included. */" },
		{ "ModuleRelativePath", "Public/Settings/RestorationBlacklist.h" },
		{ "ToolTip", "These actor classes are not allowed. Child classes are included." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses = { "ActorClasses", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRestorationBlacklist, ActorClasses), METADATA_PARAMS(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses_ElementProp = { "ComponentClasses", nullptr, (EPropertyFlags)0x0004000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UActorComponent_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* These component classes are not allowed. Child classes are included. */" },
		{ "ModuleRelativePath", "Public/Settings/RestorationBlacklist.h" },
		{ "ToolTip", "These component classes are not allowed. Child classes are included." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses = { "ComponentClasses", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRestorationBlacklist, ComponentClasses), METADATA_PARAMS(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ActorClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::NewProp_ComponentClasses,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"RestorationBlacklist",
		sizeof(FRestorationBlacklist),
		alignof(FRestorationBlacklist),
		Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRestorationBlacklist()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRestorationBlacklist_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RestorationBlacklist"), sizeof(FRestorationBlacklist), Get_Z_Construct_UScriptStruct_FRestorationBlacklist_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRestorationBlacklist_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRestorationBlacklist_Hash() { return 3189640394U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
