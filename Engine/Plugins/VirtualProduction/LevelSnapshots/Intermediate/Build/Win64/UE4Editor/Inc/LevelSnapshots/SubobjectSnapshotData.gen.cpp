// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/SubobjectSnapshotData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSubobjectSnapshotData() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSubobjectSnapshotData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FObjectSnapshotData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References

static_assert(std::is_polymorphic<FSubobjectSnapshotData>() == std::is_polymorphic<FObjectSnapshotData>(), "USTRUCT FSubobjectSnapshotData cannot be polymorphic unless super FObjectSnapshotData is polymorphic");

class UScriptStruct* FSubobjectSnapshotData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FSubobjectSnapshotData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSubobjectSnapshotData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("SubobjectSnapshotData"), sizeof(FSubobjectSnapshotData), Get_Z_Construct_UScriptStruct_FSubobjectSnapshotData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FSubobjectSnapshotData>()
{
	return FSubobjectSnapshotData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSubobjectSnapshotData(FSubobjectSnapshotData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("SubobjectSnapshotData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFSubobjectSnapshotData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFSubobjectSnapshotData()
	{
		UScriptStruct::DeferCppStructOps<FSubobjectSnapshotData>(FName(TEXT("SubobjectSnapshotData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFSubobjectSnapshotData;
	struct Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OuterIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OuterIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Data/SubobjectSnapshotData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSubobjectSnapshotData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_OuterIndex_MetaData[] = {
		{ "Comment", "/* Index to FWorldSnapshotData::SerializedObjectReferences */" },
		{ "ModuleRelativePath", "Public/Data/SubobjectSnapshotData.h" },
		{ "ToolTip", "Index to FWorldSnapshotData::SerializedObjectReferences" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_OuterIndex = { "OuterIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSubobjectSnapshotData, OuterIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_OuterIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_OuterIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_Class_MetaData[] = {
		{ "ModuleRelativePath", "Public/Data/SubobjectSnapshotData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSubobjectSnapshotData, Class), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_Class_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_OuterIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::NewProp_Class,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		Z_Construct_UScriptStruct_FObjectSnapshotData,
		&NewStructOps,
		"SubobjectSnapshotData",
		sizeof(FSubobjectSnapshotData),
		alignof(FSubobjectSnapshotData),
		Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSubobjectSnapshotData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSubobjectSnapshotData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SubobjectSnapshotData"), sizeof(FSubobjectSnapshotData), Get_Z_Construct_UScriptStruct_FSubobjectSnapshotData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSubobjectSnapshotData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSubobjectSnapshotData_Hash() { return 1498302932U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
