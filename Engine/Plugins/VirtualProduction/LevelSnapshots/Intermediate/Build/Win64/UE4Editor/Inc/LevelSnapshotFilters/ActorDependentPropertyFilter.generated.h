// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_ActorDependentPropertyFilter_generated_h
#error "ActorDependentPropertyFilter.generated.h already included, missing '#pragma once' in ActorDependentPropertyFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_ActorDependentPropertyFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorDependentPropertyFilter(); \
	friend struct Z_Construct_UClass_UActorDependentPropertyFilter_Statics; \
public: \
	DECLARE_CLASS(UActorDependentPropertyFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UActorDependentPropertyFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUActorDependentPropertyFilter(); \
	friend struct Z_Construct_UClass_UActorDependentPropertyFilter_Statics; \
public: \
	DECLARE_CLASS(UActorDependentPropertyFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UActorDependentPropertyFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorDependentPropertyFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorDependentPropertyFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorDependentPropertyFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorDependentPropertyFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorDependentPropertyFilter(UActorDependentPropertyFilter&&); \
	NO_API UActorDependentPropertyFilter(const UActorDependentPropertyFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorDependentPropertyFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorDependentPropertyFilter(UActorDependentPropertyFilter&&); \
	NO_API UActorDependentPropertyFilter(const UActorDependentPropertyFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorDependentPropertyFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorDependentPropertyFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorDependentPropertyFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_30_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UActorDependentPropertyFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorDependentPropertyFilter_h


#define FOREACH_ENUM_EDONOTCAREHANDLING(op) \
	op(EDoNotCareHandling::UseIncludeFilter) \
	op(EDoNotCareHandling::UseExcludeFilter) \
	op(EDoNotCareHandling::UseDoNotCareFilter) 

enum class EDoNotCareHandling;
template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<EDoNotCareHandling>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
