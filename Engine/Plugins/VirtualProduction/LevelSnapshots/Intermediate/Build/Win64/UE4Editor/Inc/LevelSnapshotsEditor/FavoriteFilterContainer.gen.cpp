// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/FavoriteFilterContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFavoriteFilterContainer() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFavoriteFilterContainer_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFavoriteFilterContainer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister();
// End Cross Module References
	void UFavoriteFilterContainer::StaticRegisterNativesUFavoriteFilterContainer()
	{
	}
	UClass* Z_Construct_UClass_UFavoriteFilterContainer_NoRegister()
	{
		return UFavoriteFilterContainer::StaticClass();
	}
	struct Z_Construct_UClass_UFavoriteFilterContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Favorites_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Favorites_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Favorites;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFavoriteFilterContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFavoriteFilterContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* Keeps track of selected favorite filters. */" },
		{ "IncludePath", "Data/FavoriteFilterContainer.h" },
		{ "ModuleRelativePath", "Private/Data/FavoriteFilterContainer.h" },
		{ "ToolTip", "Keeps track of selected favorite filters." },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites_Inner = { "Favorites", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites_MetaData[] = {
		{ "Comment", "/* The filters the user selected to use. */" },
		{ "ModuleRelativePath", "Private/Data/FavoriteFilterContainer.h" },
		{ "ToolTip", "The filters the user selected to use." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites = { "Favorites", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFavoriteFilterContainer, Favorites), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFavoriteFilterContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFavoriteFilterContainer_Statics::NewProp_Favorites,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFavoriteFilterContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFavoriteFilterContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFavoriteFilterContainer_Statics::ClassParams = {
		&UFavoriteFilterContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFavoriteFilterContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFavoriteFilterContainer_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFavoriteFilterContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFavoriteFilterContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFavoriteFilterContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFavoriteFilterContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFavoriteFilterContainer, 1915210505);
	template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<UFavoriteFilterContainer>()
	{
		return UFavoriteFilterContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFavoriteFilterContainer(Z_Construct_UClass_UFavoriteFilterContainer, &UFavoriteFilterContainer::StaticClass, TEXT("/Script/LevelSnapshotsEditor"), TEXT("UFavoriteFilterContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFavoriteFilterContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
