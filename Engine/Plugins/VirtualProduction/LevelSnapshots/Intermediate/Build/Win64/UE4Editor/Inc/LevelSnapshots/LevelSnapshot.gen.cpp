// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/LevelSnapshot.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshot() {}
// Cross Module References
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshot_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshot();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FWorldSnapshotData();
// End Cross Module References
	DEFINE_FUNCTION(ULevelSnapshot::execGetSnapshotDescription)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetSnapshotDescription();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshot::execGetSnapshotName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=P_THIS->GetSnapshotName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshot::execGetCaptureTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDateTime*)Z_Param__Result=P_THIS->GetCaptureTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshot::execGetMapPath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSoftObjectPath*)Z_Param__Result=P_THIS->GetMapPath();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshot::execSetSnapshotDescription)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InSnapshotDescription);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSnapshotDescription(Z_Param_InSnapshotDescription);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshot::execSetSnapshotName)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_InSnapshotName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSnapshotName(Z_Param_Out_InSnapshotName);
		P_NATIVE_END;
	}
	void ULevelSnapshot::StaticRegisterNativesULevelSnapshot()
	{
		UClass* Class = ULevelSnapshot::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCaptureTime", &ULevelSnapshot::execGetCaptureTime },
			{ "GetMapPath", &ULevelSnapshot::execGetMapPath },
			{ "GetSnapshotDescription", &ULevelSnapshot::execGetSnapshotDescription },
			{ "GetSnapshotName", &ULevelSnapshot::execGetSnapshotName },
			{ "SetSnapshotDescription", &ULevelSnapshot::execSetSnapshotDescription },
			{ "SetSnapshotName", &ULevelSnapshot::execSetSnapshotName },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics
	{
		struct LevelSnapshot_eventGetCaptureTime_Parms
		{
			FDateTime ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshot_eventGetCaptureTime_Parms, ReturnValue), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshot, nullptr, "GetCaptureTime", nullptr, nullptr, sizeof(LevelSnapshot_eventGetCaptureTime_Parms), Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics
	{
		struct LevelSnapshot_eventGetMapPath_Parms
		{
			FSoftObjectPath ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshot_eventGetMapPath_Parms, ReturnValue), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshot, nullptr, "GetMapPath", nullptr, nullptr, sizeof(LevelSnapshot_eventGetMapPath_Parms), Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshot_GetMapPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshot_GetMapPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics
	{
		struct LevelSnapshot_eventGetSnapshotDescription_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshot_eventGetSnapshotDescription_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshot, nullptr, "GetSnapshotDescription", nullptr, nullptr, sizeof(LevelSnapshot_eventGetSnapshotDescription_Parms), Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics
	{
		struct LevelSnapshot_eventGetSnapshotName_Parms
		{
			FName ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshot_eventGetSnapshotName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshot, nullptr, "GetSnapshotName", nullptr, nullptr, sizeof(LevelSnapshot_eventGetSnapshotName_Parms), Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics
	{
		struct LevelSnapshot_eventSetSnapshotDescription_Parms
		{
			FString InSnapshotDescription;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSnapshotDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InSnapshotDescription;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::NewProp_InSnapshotDescription_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::NewProp_InSnapshotDescription = { "InSnapshotDescription", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshot_eventSetSnapshotDescription_Parms, InSnapshotDescription), METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::NewProp_InSnapshotDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::NewProp_InSnapshotDescription_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::NewProp_InSnapshotDescription,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshot, nullptr, "SetSnapshotDescription", nullptr, nullptr, sizeof(LevelSnapshot_eventSetSnapshotDescription_Parms), Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics
	{
		struct LevelSnapshot_eventSetSnapshotName_Parms
		{
			FName InSnapshotName;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSnapshotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InSnapshotName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::NewProp_InSnapshotName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::NewProp_InSnapshotName = { "InSnapshotName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshot_eventSetSnapshotName_Parms, InSnapshotName), METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::NewProp_InSnapshotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::NewProp_InSnapshotName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::NewProp_InSnapshotName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Sets the name of this snapshot. */" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "Sets the name of this snapshot." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshot, nullptr, "SetSnapshotName", nullptr, nullptr, sizeof(LevelSnapshot_eventSetSnapshotName_Parms), Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULevelSnapshot_NoRegister()
	{
		return ULevelSnapshot::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshot_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotContainerWorld_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnapshotContainerWorld;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MapPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SnapshotName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotDescription_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SnapshotDescription;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshot_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULevelSnapshot_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULevelSnapshot_GetCaptureTime, "GetCaptureTime" }, // 334218949
		{ &Z_Construct_UFunction_ULevelSnapshot_GetMapPath, "GetMapPath" }, // 2233825987
		{ &Z_Construct_UFunction_ULevelSnapshot_GetSnapshotDescription, "GetSnapshotDescription" }, // 999407880
		{ &Z_Construct_UFunction_ULevelSnapshot_GetSnapshotName, "GetSnapshotName" }, // 3408483577
		{ &Z_Construct_UFunction_ULevelSnapshot_SetSnapshotDescription, "SetSnapshotDescription" }, // 357961273
		{ &Z_Construct_UFunction_ULevelSnapshot_SetSnapshotName, "SetSnapshotName" }, // 1278252167
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* Holds the state of a world at a given time. This asset can be used to rollback certain properties in a UWorld. */" },
		{ "IncludePath", "Data/LevelSnapshot.h" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "Holds the state of a world at a given time. This asset can be used to rollback certain properties in a UWorld." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotContainerWorld_MetaData[] = {
		{ "Comment", "/* The world we will be adding temporary actors to */" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "The world we will be adding temporary actors to" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotContainerWorld = { "SnapshotContainerWorld", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshot, SnapshotContainerWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotContainerWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotContainerWorld_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SerializedData_MetaData[] = {
		{ "Category", "Snapshot" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SerializedData = { "SerializedData", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshot, SerializedData), Z_Construct_UScriptStruct_FWorldSnapshotData, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SerializedData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SerializedData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_MapPath_MetaData[] = {
		{ "BlueprintGetter", "GetMapPath" },
		{ "Category", "Snapshot" },
		{ "Comment", "/* Path of the map that the snapshot was taken in */" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "Path of the map that the snapshot was taken in" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_MapPath = { "MapPath", nullptr, (EPropertyFlags)0x0040010000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshot, MapPath), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_MapPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_MapPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_CaptureTime_MetaData[] = {
		{ "BlueprintGetter", "GetCaptureTime" },
		{ "Category", "Snapshot" },
		{ "Comment", "/* UTC Time that the snapshot was taken */" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "UTC Time that the snapshot was taken" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_CaptureTime = { "CaptureTime", nullptr, (EPropertyFlags)0x0040010000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshot, CaptureTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_CaptureTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_CaptureTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotName_MetaData[] = {
		{ "BlueprintGetter", "GetSnapshotName" },
		{ "Category", "Snapshot" },
		{ "Comment", "/* User defined name for the snapshot, can differ from the actual asset name. */" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "User defined name for the snapshot, can differ from the actual asset name." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotName = { "SnapshotName", nullptr, (EPropertyFlags)0x0040010000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshot, SnapshotName), METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotDescription_MetaData[] = {
		{ "BlueprintGetter", "GetSnapshotDescription" },
		{ "Category", "Snapshot" },
		{ "Comment", "/* User defined description of the snapshot */" },
		{ "ModuleRelativePath", "Public/Data/LevelSnapshot.h" },
		{ "ToolTip", "User defined description of the snapshot" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotDescription = { "SnapshotDescription", nullptr, (EPropertyFlags)0x0040010000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshot, SnapshotDescription), METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotDescription_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotDescription_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSnapshot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotContainerWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SerializedData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_MapPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_CaptureTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshot_Statics::NewProp_SnapshotDescription,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshot_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshot>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshot_Statics::ClassParams = {
		&ULevelSnapshot::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULevelSnapshot_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshot_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshot_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshot()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshot_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshot, 779686629);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<ULevelSnapshot>()
	{
		return ULevelSnapshot::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshot(Z_Construct_UClass_ULevelSnapshot, &ULevelSnapshot::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("ULevelSnapshot"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshot);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
