// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_ActorSnapshotData_generated_h
#error "ActorSnapshotData.generated.h already included, missing '#pragma once' in ActorSnapshotData.h"
#endif
#define LEVELSNAPSHOTS_ActorSnapshotData_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_ActorSnapshotData_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActorSnapshotData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__CachedSnapshotActor() { return STRUCT_OFFSET(FActorSnapshotData, CachedSnapshotActor); } \
	FORCEINLINE static uint32 __PPO__bReceivedSerialisation() { return STRUCT_OFFSET(FActorSnapshotData, bReceivedSerialisation); } \
	FORCEINLINE static uint32 __PPO__ActorClass() { return STRUCT_OFFSET(FActorSnapshotData, ActorClass); } \
	FORCEINLINE static uint32 __PPO__SerializedActorData() { return STRUCT_OFFSET(FActorSnapshotData, SerializedActorData); } \
	FORCEINLINE static uint32 __PPO__CustomActorSerializationData() { return STRUCT_OFFSET(FActorSnapshotData, CustomActorSerializationData); } \
	FORCEINLINE static uint32 __PPO__ComponentData() { return STRUCT_OFFSET(FActorSnapshotData, ComponentData); }


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FActorSnapshotData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_ActorSnapshotData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
