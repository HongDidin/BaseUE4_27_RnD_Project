// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_LevelSnapshotsUserSettings_generated_h
#error "LevelSnapshotsUserSettings.generated.h already included, missing '#pragma once' in LevelSnapshotsUserSettings.h"
#endif
#define LEVELSNAPSHOTSEDITOR_LevelSnapshotsUserSettings_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotsUserSettings(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsUserSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsUserSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), LEVELSNAPSHOTSEDITOR_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotsUserSettings(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsUserSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsUserSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), LEVELSNAPSHOTSEDITOR_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LEVELSNAPSHOTSEDITOR_API ULevelSnapshotsUserSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsUserSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LEVELSNAPSHOTSEDITOR_API, ULevelSnapshotsUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsUserSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LEVELSNAPSHOTSEDITOR_API ULevelSnapshotsUserSettings(ULevelSnapshotsUserSettings&&); \
	LEVELSNAPSHOTSEDITOR_API ULevelSnapshotsUserSettings(const ULevelSnapshotsUserSettings&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LEVELSNAPSHOTSEDITOR_API ULevelSnapshotsUserSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LEVELSNAPSHOTSEDITOR_API ULevelSnapshotsUserSettings(ULevelSnapshotsUserSettings&&); \
	LEVELSNAPSHOTSEDITOR_API ULevelSnapshotsUserSettings(const ULevelSnapshotsUserSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LEVELSNAPSHOTSEDITOR_API, ULevelSnapshotsUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsUserSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsUserSettings)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h_14_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class ULevelSnapshotsUserSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_LevelSnapshotsUserSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
