// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/LevelSnapshotsFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotsFunctionLibrary() {}
// Cross Module References
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshotsFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshot_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ULevelSnapshotsFunctionLibrary::execApplySnapshotToWorld)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(ULevelSnapshot,Z_Param_Snapshot);
		P_GET_OBJECT(ULevelSnapshotFilter,Z_Param_OptionalFilter);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSnapshotsFunctionLibrary::ApplySnapshotToWorld(Z_Param_WorldContextObject,Z_Param_Snapshot,Z_Param_OptionalFilter);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshotsFunctionLibrary::execTakeLevelSnapshot)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FNameProperty,Z_Param_NewSnapshotName);
		P_GET_PROPERTY(FStrProperty,Z_Param_Description);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSnapshot**)Z_Param__Result=ULevelSnapshotsFunctionLibrary::TakeLevelSnapshot(Z_Param_WorldContextObject,Z_Param_NewSnapshotName,Z_Param_Description);
		P_NATIVE_END;
	}
	void ULevelSnapshotsFunctionLibrary::StaticRegisterNativesULevelSnapshotsFunctionLibrary()
	{
		UClass* Class = ULevelSnapshotsFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplySnapshotToWorld", &ULevelSnapshotsFunctionLibrary::execApplySnapshotToWorld },
			{ "TakeLevelSnapshot", &ULevelSnapshotsFunctionLibrary::execTakeLevelSnapshot },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics
	{
		struct LevelSnapshotsFunctionLibrary_eventApplySnapshotToWorld_Parms
		{
			const UObject* WorldContextObject;
			ULevelSnapshot* Snapshot;
			ULevelSnapshotFilter* OptionalFilter;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Snapshot;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OptionalFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventApplySnapshotToWorld_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_Snapshot = { "Snapshot", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventApplySnapshotToWorld_Parms, Snapshot), Z_Construct_UClass_ULevelSnapshot_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_OptionalFilter = { "OptionalFilter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventApplySnapshotToWorld_Parms, OptionalFilter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_Snapshot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::NewProp_OptionalFilter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::Function_MetaDataParams[] = {
		{ "Category", "LevelSnapshots" },
		{ "Comment", "/* Applies the snapshot to the world. If no filter is specified, the entire snapshot is applied. */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotsFunctionLibrary.h" },
		{ "ToolTip", "Applies the snapshot to the world. If no filter is specified, the entire snapshot is applied." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotsFunctionLibrary, nullptr, "ApplySnapshotToWorld", nullptr, nullptr, sizeof(LevelSnapshotsFunctionLibrary_eventApplySnapshotToWorld_Parms), Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics
	{
		struct LevelSnapshotsFunctionLibrary_eventTakeLevelSnapshot_Parms
		{
			const UObject* WorldContextObject;
			FName NewSnapshotName;
			FString Description;
			ULevelSnapshot* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewSnapshotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewSnapshotName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventTakeLevelSnapshot_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_WorldContextObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_NewSnapshotName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_NewSnapshotName = { "NewSnapshotName", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventTakeLevelSnapshot_Parms, NewSnapshotName), METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_NewSnapshotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_NewSnapshotName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_Description_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventTakeLevelSnapshot_Parms, Description), METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_Description_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsFunctionLibrary_eventTakeLevelSnapshot_Parms, ReturnValue), Z_Construct_UClass_ULevelSnapshot_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_NewSnapshotName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::Function_MetaDataParams[] = {
		{ "Category", "LevelSnapshots" },
		{ "CPP_Default_Description", "" },
		{ "CPP_Default_NewSnapshotName", "NewLevelSnapshot" },
		{ "ModuleRelativePath", "Public/LevelSnapshotsFunctionLibrary.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotsFunctionLibrary, nullptr, "TakeLevelSnapshot", nullptr, nullptr, sizeof(LevelSnapshotsFunctionLibrary_eventTakeLevelSnapshot_Parms), Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_NoRegister()
	{
		return ULevelSnapshotsFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_ApplySnapshotToWorld, "ApplySnapshotToWorld" }, // 1623831179
		{ &Z_Construct_UFunction_ULevelSnapshotsFunctionLibrary_TakeLevelSnapshot, "TakeLevelSnapshot" }, // 2526948991
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LevelSnapshotsFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/LevelSnapshotsFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotsFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::ClassParams = {
		&ULevelSnapshotsFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotsFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotsFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotsFunctionLibrary, 334316598);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<ULevelSnapshotsFunctionLibrary>()
	{
		return ULevelSnapshotsFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotsFunctionLibrary(Z_Construct_UClass_ULevelSnapshotsFunctionLibrary, &ULevelSnapshotsFunctionLibrary::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("ULevelSnapshotsFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotsFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
