// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/Filters/LevelSnapshotsFilterPreset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotsFilterPreset() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_ULevelSnapshotsFilterPreset_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_ULevelSnapshotsFilterPreset();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UConjunctionFilter_NoRegister();
// End Cross Module References
	void ULevelSnapshotsFilterPreset::StaticRegisterNativesULevelSnapshotsFilterPreset()
	{
	}
	UClass* Z_Construct_UClass_ULevelSnapshotsFilterPreset_NoRegister()
	{
		return ULevelSnapshotsFilterPreset::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Children;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/*\n * Manages logic for combining filters in the editor.\n * This filter may have no children: in this case, the filter returns true.\n *\n * Disjunctive normal form = ORs of ANDs. Example: (a && !b) || (c && d) || e\n */" },
		{ "IncludePath", "Data/Filters/LevelSnapshotsFilterPreset.h" },
		{ "InternalSnapshotFilter", "" },
		{ "ModuleRelativePath", "Private/Data/Filters/LevelSnapshotsFilterPreset.h" },
		{ "ToolTip", "* Manages logic for combining filters in the editor.\n* This filter may have no children: in this case, the filter returns true.\n*\n* Disjunctive normal form = ORs of ANDs. Example: (a && !b) || (c && d) || e" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_Inner_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Data/Filters/LevelSnapshotsFilterPreset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_Inner = { "Children", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UConjunctionFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/Data/Filters/LevelSnapshotsFilterPreset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsFilterPreset, Children), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::NewProp_Children,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotsFilterPreset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::ClassParams = {
		&ULevelSnapshotsFilterPreset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::PropPointers),
		0,
		0x008010A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotsFilterPreset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotsFilterPreset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotsFilterPreset, 1474765816);
	template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<ULevelSnapshotsFilterPreset>()
	{
		return ULevelSnapshotsFilterPreset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotsFilterPreset(Z_Construct_UClass_ULevelSnapshotsFilterPreset, &ULevelSnapshotsFilterPreset::StaticClass, TEXT("/Script/LevelSnapshotsEditor"), TEXT("ULevelSnapshotsFilterPreset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotsFilterPreset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
