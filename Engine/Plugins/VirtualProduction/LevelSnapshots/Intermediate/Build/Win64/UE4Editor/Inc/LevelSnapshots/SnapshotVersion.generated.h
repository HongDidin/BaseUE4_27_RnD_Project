// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_SnapshotVersion_generated_h
#error "SnapshotVersion.generated.h already included, missing '#pragma once' in SnapshotVersion.h"
#endif
#define LEVELSNAPSHOTS_SnapshotVersion_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SnapshotVersion_h_82_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FSnapshotVersionInfo>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SnapshotVersion_h_60_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FSnapshotCustomVersionInfo>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SnapshotVersion_h_34_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FSnapshotEngineVersionInfo>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SnapshotVersion_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FSnapshotFileVersionInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_SnapshotVersion_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
