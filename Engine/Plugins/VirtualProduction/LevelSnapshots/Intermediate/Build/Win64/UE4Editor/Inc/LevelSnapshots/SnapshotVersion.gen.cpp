// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/SnapshotVersion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnapshotVersion() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSnapshotVersionInfo();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSnapshotFileVersionInfo();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FSnapshotVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FSnapshotVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSnapshotVersionInfo, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("SnapshotVersionInfo"), sizeof(FSnapshotVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotVersionInfo_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FSnapshotVersionInfo>()
{
	return FSnapshotVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSnapshotVersionInfo(FSnapshotVersionInfo::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("SnapshotVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotVersionInfo
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FSnapshotVersionInfo>(FName(TEXT("SnapshotVersionInfo")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotVersionInfo;
	struct Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FileVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineVersion;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomVersions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomVersions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CustomVersions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds version information for a session */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Holds version information for a session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSnapshotVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_FileVersion_MetaData[] = {
		{ "Comment", "/** File version info */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "File version info" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_FileVersion = { "FileVersion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotVersionInfo, FileVersion), Z_Construct_UScriptStruct_FSnapshotFileVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_FileVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_FileVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_EngineVersion_MetaData[] = {
		{ "Comment", "/** Engine version info */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Engine version info" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_EngineVersion = { "EngineVersion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotVersionInfo, EngineVersion), Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_EngineVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_EngineVersion_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions_Inner = { "CustomVersions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions_MetaData[] = {
		{ "Comment", "/** Custom version info */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Custom version info" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions = { "CustomVersions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotVersionInfo, CustomVersions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_FileVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_EngineVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::NewProp_CustomVersions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"SnapshotVersionInfo",
		sizeof(FSnapshotVersionInfo),
		alignof(FSnapshotVersionInfo),
		Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSnapshotVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSnapshotVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SnapshotVersionInfo"), sizeof(FSnapshotVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSnapshotVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSnapshotVersionInfo_Hash() { return 2108670662U; }
class UScriptStruct* FSnapshotCustomVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("SnapshotCustomVersionInfo"), sizeof(FSnapshotCustomVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FSnapshotCustomVersionInfo>()
{
	return FSnapshotCustomVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSnapshotCustomVersionInfo(FSnapshotCustomVersionInfo::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("SnapshotCustomVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotCustomVersionInfo
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotCustomVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FSnapshotCustomVersionInfo>(FName(TEXT("SnapshotCustomVersionInfo")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotCustomVersionInfo;
	struct Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FriendlyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FriendlyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds custom version information */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Holds custom version information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSnapshotCustomVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_FriendlyName_MetaData[] = {
		{ "Comment", "/** Friendly name of the version */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Friendly name of the version" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_FriendlyName = { "FriendlyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotCustomVersionInfo, FriendlyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_FriendlyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_FriendlyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Key_MetaData[] = {
		{ "Comment", "/** Unique custom key */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Unique custom key" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotCustomVersionInfo, Key), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Version_MetaData[] = {
		{ "Comment", "/** Custom version */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Custom version" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotCustomVersionInfo, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Version_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_FriendlyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::NewProp_Version,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"SnapshotCustomVersionInfo",
		sizeof(FSnapshotCustomVersionInfo),
		alignof(FSnapshotCustomVersionInfo),
		Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SnapshotCustomVersionInfo"), sizeof(FSnapshotCustomVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSnapshotCustomVersionInfo_Hash() { return 2238178720U; }
class UScriptStruct* FSnapshotEngineVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("SnapshotEngineVersionInfo"), sizeof(FSnapshotEngineVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FSnapshotEngineVersionInfo>()
{
	return FSnapshotEngineVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSnapshotEngineVersionInfo(FSnapshotEngineVersionInfo::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("SnapshotEngineVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotEngineVersionInfo
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotEngineVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FSnapshotEngineVersionInfo>(FName(TEXT("SnapshotEngineVersionInfo")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotEngineVersionInfo;
	struct Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Major_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Major;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Minor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Minor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Patch_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Patch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Changelist_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Changelist;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds engine version information */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Holds engine version information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSnapshotEngineVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Major_MetaData[] = {
		{ "Comment", "/** Major version number */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Major version number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Major = { "Major", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotEngineVersionInfo, Major), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Major_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Major_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Minor_MetaData[] = {
		{ "Comment", "/** Minor version number */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Minor version number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Minor = { "Minor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotEngineVersionInfo, Minor), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Minor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Minor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Patch_MetaData[] = {
		{ "Comment", "/** Patch version number */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Patch version number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Patch = { "Patch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotEngineVersionInfo, Patch), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Patch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Patch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Changelist_MetaData[] = {
		{ "Comment", "/** Changelist number. This is used to arbitrate when Major/Minor/Patch version numbers match */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Changelist number. This is used to arbitrate when Major/Minor/Patch version numbers match" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Changelist = { "Changelist", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotEngineVersionInfo, Changelist), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Changelist_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Changelist_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Major,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Minor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Patch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::NewProp_Changelist,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"SnapshotEngineVersionInfo",
		sizeof(FSnapshotEngineVersionInfo),
		alignof(FSnapshotEngineVersionInfo),
		Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SnapshotEngineVersionInfo"), sizeof(FSnapshotEngineVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSnapshotEngineVersionInfo_Hash() { return 1170758616U; }
class UScriptStruct* FSnapshotFileVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("SnapshotFileVersionInfo"), sizeof(FSnapshotFileVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FSnapshotFileVersionInfo>()
{
	return FSnapshotFileVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSnapshotFileVersionInfo(FSnapshotFileVersionInfo::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("SnapshotFileVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotFileVersionInfo
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotFileVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FSnapshotFileVersionInfo>(FName(TEXT("SnapshotFileVersionInfo")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFSnapshotFileVersionInfo;
	struct Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersionUE4_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FileVersionUE4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersionLicenseeUE4_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FileVersionLicenseeUE4;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds file version information */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Holds file version information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSnapshotFileVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionUE4_MetaData[] = {
		{ "Comment", "/* UE4 file version */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "UE4 file version" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionUE4 = { "FileVersionUE4", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotFileVersionInfo, FileVersionUE4), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionUE4_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionUE4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4_MetaData[] = {
		{ "Comment", "/* Licensee file version */" },
		{ "ModuleRelativePath", "Public/Data/SnapshotVersion.h" },
		{ "ToolTip", "Licensee file version" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4 = { "FileVersionLicenseeUE4", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSnapshotFileVersionInfo, FileVersionLicenseeUE4), METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionUE4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"SnapshotFileVersionInfo",
		sizeof(FSnapshotFileVersionInfo),
		alignof(FSnapshotFileVersionInfo),
		Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSnapshotFileVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SnapshotFileVersionInfo"), sizeof(FSnapshotFileVersionInfo), Get_Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSnapshotFileVersionInfo_Hash() { return 1365019583U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
