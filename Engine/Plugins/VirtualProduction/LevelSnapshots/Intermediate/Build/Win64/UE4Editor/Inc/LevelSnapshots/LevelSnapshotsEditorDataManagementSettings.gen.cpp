// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Settings/LevelSnapshotsEditorDataManagementSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotsEditorDataManagementSettings() {}
// Cross Module References
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_NoRegister();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	DEFINE_FUNCTION(ULevelSnapshotsEditorDataManagementSettings::execParseLevelSnapshotsTokensInText)
	{
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_InTextToParse);
		P_GET_PROPERTY(FStrProperty,Z_Param_InWorldName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=ULevelSnapshotsEditorDataManagementSettings::ParseLevelSnapshotsTokensInText(Z_Param_Out_InTextToParse,Z_Param_InWorldName);
		P_NATIVE_END;
	}
	void ULevelSnapshotsEditorDataManagementSettings::StaticRegisterNativesULevelSnapshotsEditorDataManagementSettings()
	{
		UClass* Class = ULevelSnapshotsEditorDataManagementSettings::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ParseLevelSnapshotsTokensInText", &ULevelSnapshotsEditorDataManagementSettings::execParseLevelSnapshotsTokensInText },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics
	{
		struct LevelSnapshotsEditorDataManagementSettings_eventParseLevelSnapshotsTokensInText_Parms
		{
			FText InTextToParse;
			FString InWorldName;
			FText ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTextToParse_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_InTextToParse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InWorldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InWorldName;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InTextToParse_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InTextToParse = { "InTextToParse", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsEditorDataManagementSettings_eventParseLevelSnapshotsTokensInText_Parms, InTextToParse), METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InTextToParse_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InTextToParse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InWorldName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InWorldName = { "InWorldName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsEditorDataManagementSettings_eventParseLevelSnapshotsTokensInText_Parms, InWorldName), METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InWorldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InWorldName_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotsEditorDataManagementSettings_eventParseLevelSnapshotsTokensInText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InTextToParse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_InWorldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorDataManagementSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings, nullptr, "ParseLevelSnapshotsTokensInText", nullptr, nullptr, sizeof(LevelSnapshotsEditorDataManagementSettings_eventParseLevelSnapshotsTokensInText_Parms), Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_NoRegister()
	{
		return ULevelSnapshotsEditorDataManagementSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootLevelSnapshotSaveDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RootLevelSnapshotSaveDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSnapshotSaveDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LevelSnapshotSaveDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultLevelSnapshotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultLevelSnapshotName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULevelSnapshotsEditorDataManagementSettings_ParseLevelSnapshotsTokensInText, "ParseLevelSnapshotsTokensInText" }, // 3045361384
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Settings/LevelSnapshotsEditorDataManagementSettings.h" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorDataManagementSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_RootLevelSnapshotSaveDir_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "// Must be a directory in the Game Content folder (\"/Game/\"). For best results, use the picker.  \n" },
		{ "ContentDir", "" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorDataManagementSettings.h" },
		{ "RelativeToGameContentDir", "" },
		{ "ToolTip", "Must be a directory in the Game Content folder (\"/Game/\"). For best results, use the picker." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_RootLevelSnapshotSaveDir = { "RootLevelSnapshotSaveDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorDataManagementSettings, RootLevelSnapshotSaveDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_RootLevelSnapshotSaveDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_RootLevelSnapshotSaveDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_LevelSnapshotSaveDir_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/** The format to use for the resulting filename. Extension will be added automatically. Any tokens of the form {token} will be replaced with the corresponding value:\n\x09 * {map}\x09\x09- The name of the captured map or level\n\x09 * {user}\x09\x09- The current OS user account name\n\x09 * {year}       - The current year\n\x09 * {month}      - The current month\n\x09 * {day}        - The current day\n\x09 * {date}       - The current date from the local computer in the format of {year}-{month}-{day}\n\x09 * {time}       - The current time from the local computer in the format of hours-minutes-seconds\n\x09 */" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorDataManagementSettings.h" },
		{ "ToolTip", "The format to use for the resulting filename. Extension will be added automatically. Any tokens of the form {token} will be replaced with the corresponding value:\n{map}                - The name of the captured map or level\n{user}               - The current OS user account name\n{year}       - The current year\n{month}      - The current month\n{day}        - The current day\n{date}       - The current date from the local computer in the format of {year}-{month}-{day}\n{time}       - The current time from the local computer in the format of hours-minutes-seconds" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_LevelSnapshotSaveDir = { "LevelSnapshotSaveDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorDataManagementSettings, LevelSnapshotSaveDir), METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_LevelSnapshotSaveDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_LevelSnapshotSaveDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_DefaultLevelSnapshotName_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/** The format to use for the resulting filename. Extension will be added automatically. Any tokens of the form {token} will be replaced with the corresponding value:\n\x09 * {map}\x09\x09- The name of the captured map or level\n\x09 * {user}\x09\x09- The current OS user account name\n\x09 * {year}       - The current year\n\x09 * {month}      - The current month\n\x09 * {day}        - The current day\n\x09 * {date}       - The current date from the local computer in the format of {year}-{month}-{day}\n\x09 * {time}       - The current time from the local computer in the format of hours-minutes-seconds\n\x09 */" },
		{ "ModuleRelativePath", "Public/Settings/LevelSnapshotsEditorDataManagementSettings.h" },
		{ "ToolTip", "The format to use for the resulting filename. Extension will be added automatically. Any tokens of the form {token} will be replaced with the corresponding value:\n{map}                - The name of the captured map or level\n{user}               - The current OS user account name\n{year}       - The current year\n{month}      - The current month\n{day}        - The current day\n{date}       - The current date from the local computer in the format of {year}-{month}-{day}\n{time}       - The current time from the local computer in the format of hours-minutes-seconds" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_DefaultLevelSnapshotName = { "DefaultLevelSnapshotName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSnapshotsEditorDataManagementSettings, DefaultLevelSnapshotName), METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_DefaultLevelSnapshotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_DefaultLevelSnapshotName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_RootLevelSnapshotSaveDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_LevelSnapshotSaveDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::NewProp_DefaultLevelSnapshotName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotsEditorDataManagementSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::ClassParams = {
		&ULevelSnapshotsEditorDataManagementSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotsEditorDataManagementSettings, 3455447119);
	template<> LEVELSNAPSHOTS_API UClass* StaticClass<ULevelSnapshotsEditorDataManagementSettings>()
	{
		return ULevelSnapshotsEditorDataManagementSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotsEditorDataManagementSettings(Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings, &ULevelSnapshotsEditorDataManagementSettings::StaticClass, TEXT("/Script/LevelSnapshots"), TEXT("ULevelSnapshotsEditorDataManagementSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotsEditorDataManagementSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
