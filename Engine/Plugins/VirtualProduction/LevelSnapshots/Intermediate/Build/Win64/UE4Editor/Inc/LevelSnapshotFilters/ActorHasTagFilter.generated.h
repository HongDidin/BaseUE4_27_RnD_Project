// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_ActorHasTagFilter_generated_h
#error "ActorHasTagFilter.generated.h already included, missing '#pragma once' in ActorHasTagFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_ActorHasTagFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorHasTagFilter(); \
	friend struct Z_Construct_UClass_UActorHasTagFilter_Statics; \
public: \
	DECLARE_CLASS(UActorHasTagFilter, UActorSelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UActorHasTagFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_INCLASS \
private: \
	static void StaticRegisterNativesUActorHasTagFilter(); \
	friend struct Z_Construct_UClass_UActorHasTagFilter_Statics; \
public: \
	DECLARE_CLASS(UActorHasTagFilter, UActorSelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UActorHasTagFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorHasTagFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorHasTagFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorHasTagFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorHasTagFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorHasTagFilter(UActorHasTagFilter&&); \
	NO_API UActorHasTagFilter(const UActorHasTagFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorHasTagFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorHasTagFilter(UActorHasTagFilter&&); \
	NO_API UActorHasTagFilter(const UActorHasTagFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorHasTagFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorHasTagFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorHasTagFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TagCheckingBehavior() { return STRUCT_OFFSET(UActorHasTagFilter, TagCheckingBehavior); } \
	FORCEINLINE static uint32 __PPO__AllowedTags() { return STRUCT_OFFSET(UActorHasTagFilter, AllowedTags); } \
	FORCEINLINE static uint32 __PPO__ActorToCheck() { return STRUCT_OFFSET(UActorHasTagFilter, ActorToCheck); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_36_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UActorHasTagFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_ActorHasTagFilter_h


#define FOREACH_ENUM_ETAGCHECKINGBEHAVIOR(op) \
	op(ETagCheckingBehavior::HasAllTags) \
	op(ETagCheckingBehavior::HasAnyTag) 
#define FOREACH_ENUM_EACTORTOCHECK(op) \
	op(EActorToCheck::WorldActor) \
	op(EActorToCheck::SnapshotActor) \
	op(EActorToCheck::Both) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
