// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/ActorHasTagFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorHasTagFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_ETagCheckingBehavior();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EActorToCheck();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorHasTagFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorHasTagFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorSelectorFilter();
// End Cross Module References
	static UEnum* ETagCheckingBehavior_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LevelSnapshotFilters_ETagCheckingBehavior, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("ETagCheckingBehavior"));
		}
		return Singleton;
	}
	template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<ETagCheckingBehavior::Type>()
	{
		return ETagCheckingBehavior_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETagCheckingBehavior(ETagCheckingBehavior_StaticEnum, TEXT("/Script/LevelSnapshotFilters"), TEXT("ETagCheckingBehavior"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LevelSnapshotFilters_ETagCheckingBehavior_Hash() { return 1586030759U; }
	UEnum* Z_Construct_UEnum_LevelSnapshotFilters_ETagCheckingBehavior()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETagCheckingBehavior"), 0, Get_Z_Construct_UEnum_LevelSnapshotFilters_ETagCheckingBehavior_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETagCheckingBehavior::HasAllTags", (int64)ETagCheckingBehavior::HasAllTags },
				{ "ETagCheckingBehavior::HasAnyTag", (int64)ETagCheckingBehavior::HasAnyTag },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "HasAllTags.Comment", "/* Actor must have all tags to pass */" },
				{ "HasAllTags.Name", "ETagCheckingBehavior::HasAllTags" },
				{ "HasAllTags.ToolTip", "Actor must have all tags to pass" },
				{ "HasAnyTag.Comment", "/* Actor must have at least one of the tags */" },
				{ "HasAnyTag.Name", "ETagCheckingBehavior::HasAnyTag" },
				{ "HasAnyTag.ToolTip", "Actor must have at least one of the tags" },
				{ "ModuleRelativePath", "Public/Builtin/ActorHasTagFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
				nullptr,
				"ETagCheckingBehavior",
				"ETagCheckingBehavior::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EActorToCheck_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LevelSnapshotFilters_EActorToCheck, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("EActorToCheck"));
		}
		return Singleton;
	}
	template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<EActorToCheck::Type>()
	{
		return EActorToCheck_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EActorToCheck(EActorToCheck_StaticEnum, TEXT("/Script/LevelSnapshotFilters"), TEXT("EActorToCheck"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LevelSnapshotFilters_EActorToCheck_Hash() { return 4219805463U; }
	UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EActorToCheck()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EActorToCheck"), 0, Get_Z_Construct_UEnum_LevelSnapshotFilters_EActorToCheck_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EActorToCheck::WorldActor", (int64)EActorToCheck::WorldActor },
				{ "EActorToCheck::SnapshotActor", (int64)EActorToCheck::SnapshotActor },
				{ "EActorToCheck::Both", (int64)EActorToCheck::Both },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Both.Comment", "/* Checks the tags of both actors. */" },
				{ "Both.Name", "EActorToCheck::Both" },
				{ "Both.ToolTip", "Checks the tags of both actors." },
				{ "ModuleRelativePath", "Public/Builtin/ActorHasTagFilter.h" },
				{ "SnapshotActor.Comment", "/* Checks only the tags of the snapshot actor */" },
				{ "SnapshotActor.Name", "EActorToCheck::SnapshotActor" },
				{ "SnapshotActor.ToolTip", "Checks only the tags of the snapshot actor" },
				{ "WorldActor.Comment", "/* Checks only the tags of the world actor */" },
				{ "WorldActor.Name", "EActorToCheck::WorldActor" },
				{ "WorldActor.ToolTip", "Checks only the tags of the world actor" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
				nullptr,
				"EActorToCheck",
				"EActorToCheck::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UActorHasTagFilter::StaticRegisterNativesUActorHasTagFilter()
	{
	}
	UClass* Z_Construct_UClass_UActorHasTagFilter_NoRegister()
	{
		return UActorHasTagFilter::StaticClass();
	}
	struct Z_Construct_UClass_UActorHasTagFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TagCheckingBehavior_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TagCheckingBehavior;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AllowedTags_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllowedTags_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_AllowedTags;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorToCheck_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ActorToCheck;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorHasTagFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorSelectorFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorHasTagFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* Allows an actor if it has all or any of the specified tags. */" },
		{ "CommonSnapshotFilter", "" },
		{ "IncludePath", "Builtin/ActorHasTagFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/ActorHasTagFilter.h" },
		{ "ToolTip", "Allows an actor if it has all or any of the specified tags." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_TagCheckingBehavior_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* How to match AllowedTags in each actor. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorHasTagFilter.h" },
		{ "ToolTip", "How to match AllowedTags in each actor." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_TagCheckingBehavior = { "TagCheckingBehavior", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorHasTagFilter, TagCheckingBehavior), Z_Construct_UEnum_LevelSnapshotFilters_ETagCheckingBehavior, METADATA_PARAMS(Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_TagCheckingBehavior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_TagCheckingBehavior_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags_ElementProp = { "AllowedTags", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* The tags to check the actor for.  */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorHasTagFilter.h" },
		{ "ToolTip", "The tags to check the actor for." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags = { "AllowedTags", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorHasTagFilter, AllowedTags), METADATA_PARAMS(Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_ActorToCheck_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* Which of the actors we should check the tags on. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorHasTagFilter.h" },
		{ "ToolTip", "Which of the actors we should check the tags on." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_ActorToCheck = { "ActorToCheck", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorHasTagFilter, ActorToCheck), Z_Construct_UEnum_LevelSnapshotFilters_EActorToCheck, METADATA_PARAMS(Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_ActorToCheck_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_ActorToCheck_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActorHasTagFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_TagCheckingBehavior,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_AllowedTags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorHasTagFilter_Statics::NewProp_ActorToCheck,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorHasTagFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorHasTagFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorHasTagFilter_Statics::ClassParams = {
		&UActorHasTagFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UActorHasTagFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UActorHasTagFilter_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UActorHasTagFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorHasTagFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorHasTagFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorHasTagFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorHasTagFilter, 2256099195);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UActorHasTagFilter>()
	{
		return UActorHasTagFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorHasTagFilter(Z_Construct_UClass_UActorHasTagFilter, &UActorHasTagFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UActorHasTagFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorHasTagFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
