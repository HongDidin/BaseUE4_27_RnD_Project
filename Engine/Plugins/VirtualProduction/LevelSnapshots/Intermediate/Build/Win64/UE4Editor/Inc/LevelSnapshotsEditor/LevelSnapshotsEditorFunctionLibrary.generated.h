// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ULevelSnapshot;
class UObject;
#ifdef LEVELSNAPSHOTSEDITOR_LevelSnapshotsEditorFunctionLibrary_generated_h
#error "LevelSnapshotsEditorFunctionLibrary.generated.h already included, missing '#pragma once' in LevelSnapshotsEditorFunctionLibrary.h"
#endif
#define LEVELSNAPSHOTSEDITOR_LevelSnapshotsEditorFunctionLibrary_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGenerateThumbnailForSnapshotAsset); \
	DECLARE_FUNCTION(execTakeAndSaveLevelSnapshotEditorWorld); \
	DECLARE_FUNCTION(execTakeLevelSnapshotAndSaveToDisk);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGenerateThumbnailForSnapshotAsset); \
	DECLARE_FUNCTION(execTakeAndSaveLevelSnapshotEditorWorld); \
	DECLARE_FUNCTION(execTakeLevelSnapshotAndSaveToDisk);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorFunctionLibrary(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorFunctionLibrary)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorFunctionLibrary(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorFunctionLibrary)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotsEditorFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorFunctionLibrary(ULevelSnapshotsEditorFunctionLibrary&&); \
	NO_API ULevelSnapshotsEditorFunctionLibrary(const ULevelSnapshotsEditorFunctionLibrary&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotsEditorFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorFunctionLibrary(ULevelSnapshotsEditorFunctionLibrary&&); \
	NO_API ULevelSnapshotsEditorFunctionLibrary(const ULevelSnapshotsEditorFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorFunctionLibrary)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class ULevelSnapshotsEditorFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Public_LevelSnapshotsEditorFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
