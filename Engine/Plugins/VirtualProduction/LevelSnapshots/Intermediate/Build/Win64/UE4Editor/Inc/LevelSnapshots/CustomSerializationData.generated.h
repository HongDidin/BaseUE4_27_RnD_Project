// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_CustomSerializationData_generated_h
#error "CustomSerializationData.generated.h already included, missing '#pragma once' in CustomSerializationData.h"
#endif
#define LEVELSNAPSHOTS_CustomSerializationData_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_CustomSerializationData_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCustomSerializationData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FCustomSerializationData>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_CustomSerializationData_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FObjectSnapshotData Super;


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FCustomSubbjectSerializationData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_CustomSerializationData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
