// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/ActorSelector/ActorSelectorFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorSelectorFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorSelectorFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorSelectorFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotBlueprintFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult();
// End Cross Module References
	void UActorSelectorFilter::StaticRegisterNativesUActorSelectorFilter()
	{
	}
	UClass* Z_Construct_UClass_UActorSelectorFilter_NoRegister()
	{
		return UActorSelectorFilter::StaticClass();
	}
	struct Z_Construct_UClass_UActorSelectorFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorSelectorFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotBlueprintFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorSelectorFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for filters that only implement IsActorValid\n */" },
		{ "IncludePath", "Builtin/ActorSelector/ActorSelectorFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/ActorSelector/ActorSelectorFilter.h" },
		{ "ToolTip", "Base class for filters that only implement IsActorValid" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorSelectorFilter_Statics::NewProp_DefaultResult_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/**\n\x09 * What to return for IsPropertyValid, IsDeletedActorValid, and IsAddedActorValid\n\x09 */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorSelector/ActorSelectorFilter.h" },
		{ "ToolTip", "What to return for IsPropertyValid, IsDeletedActorValid, and IsAddedActorValid" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UActorSelectorFilter_Statics::NewProp_DefaultResult = { "DefaultResult", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorSelectorFilter, DefaultResult), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UActorSelectorFilter_Statics::NewProp_DefaultResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorSelectorFilter_Statics::NewProp_DefaultResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActorSelectorFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorSelectorFilter_Statics::NewProp_DefaultResult,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorSelectorFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorSelectorFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorSelectorFilter_Statics::ClassParams = {
		&UActorSelectorFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UActorSelectorFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UActorSelectorFilter_Statics::PropPointers),
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UActorSelectorFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorSelectorFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorSelectorFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorSelectorFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorSelectorFilter, 4158009189);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UActorSelectorFilter>()
	{
		return UActorSelectorFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorSelectorFilter(Z_Construct_UClass_UActorSelectorFilter, &UActorSelectorFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UActorSelectorFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorSelectorFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
