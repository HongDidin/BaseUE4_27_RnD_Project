// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_LevelSnapshotsEditorData_generated_h
#error "LevelSnapshotsEditorData.generated.h already included, missing '#pragma once' in LevelSnapshotsEditorData.h"
#endif
#define LEVELSNAPSHOTSEDITOR_LevelSnapshotsEditorData_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorData(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorData_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorData)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorData(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorData_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorData)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotsEditorData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorData(ULevelSnapshotsEditorData&&); \
	NO_API ULevelSnapshotsEditorData(const ULevelSnapshotsEditorData&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorData(ULevelSnapshotsEditorData&&); \
	NO_API ULevelSnapshotsEditorData(const ULevelSnapshotsEditorData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorData)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FavoriteFilters() { return STRUCT_OFFSET(ULevelSnapshotsEditorData, FavoriteFilters); } \
	FORCEINLINE static uint32 __PPO__UserDefinedFilters() { return STRUCT_OFFSET(ULevelSnapshotsEditorData, UserDefinedFilters); } \
	FORCEINLINE static uint32 __PPO__FilterLoader() { return STRUCT_OFFSET(ULevelSnapshotsEditorData, FilterLoader); } \
	FORCEINLINE static uint32 __PPO__bIsFilterDirty() { return STRUCT_OFFSET(ULevelSnapshotsEditorData, bIsFilterDirty); } \
	FORCEINLINE static uint32 __PPO__FilterResults() { return STRUCT_OFFSET(ULevelSnapshotsEditorData, FilterResults); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_20_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class ULevelSnapshotsEditorData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_LevelSnapshotsEditorData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
