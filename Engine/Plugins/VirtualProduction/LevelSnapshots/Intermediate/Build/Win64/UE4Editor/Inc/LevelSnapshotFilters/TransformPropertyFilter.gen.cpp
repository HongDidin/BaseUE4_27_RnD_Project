// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/PropertySelector/TransformPropertyFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTransformPropertyFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UTransformPropertyFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UTransformPropertyFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UPropertySelectorFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult();
// End Cross Module References
	void UTransformPropertyFilter::StaticRegisterNativesUTransformPropertyFilter()
	{
	}
	UClass* Z_Construct_UClass_UTransformPropertyFilter_NoRegister()
	{
		return UTransformPropertyFilter::StaticClass();
	}
	struct Z_Construct_UClass_UTransformPropertyFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTransformPropertyFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPropertySelectorFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformPropertyFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Allows you to filter location, rotation, and scale properties on scene components.\n * Use case: You want to restore the location but not rotation of an actor.\n */" },
		{ "IncludePath", "Builtin/PropertySelector/TransformPropertyFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/PropertySelector/TransformPropertyFilter.h" },
		{ "ToolTip", "Allows you to filter location, rotation, and scale properties on scene components.\nUse case: You want to restore the location but not rotation of an actor." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Location_MetaData[] = {
		{ "Category", "Transform" },
		{ "Comment", "/* Should the location property be restored? */" },
		{ "ModuleRelativePath", "Public/Builtin/PropertySelector/TransformPropertyFilter.h" },
		{ "ToolTip", "Should the location property be restored?" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformPropertyFilter, Location), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "Transform" },
		{ "Comment", "/* Should the rotation property be restored? */" },
		{ "ModuleRelativePath", "Public/Builtin/PropertySelector/TransformPropertyFilter.h" },
		{ "ToolTip", "Should the rotation property be restored?" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformPropertyFilter, Rotation), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "Transform" },
		{ "Comment", "/* Should the scale property be restored? */" },
		{ "ModuleRelativePath", "Public/Builtin/PropertySelector/TransformPropertyFilter.h" },
		{ "ToolTip", "Should the scale property be restored?" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTransformPropertyFilter, Scale), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTransformPropertyFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTransformPropertyFilter_Statics::NewProp_Scale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTransformPropertyFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTransformPropertyFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTransformPropertyFilter_Statics::ClassParams = {
		&UTransformPropertyFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTransformPropertyFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTransformPropertyFilter_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTransformPropertyFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTransformPropertyFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTransformPropertyFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTransformPropertyFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTransformPropertyFilter, 1289029066);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UTransformPropertyFilter>()
	{
		return UTransformPropertyFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTransformPropertyFilter(Z_Construct_UClass_UTransformPropertyFilter, &UTransformPropertyFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UTransformPropertyFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTransformPropertyFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
