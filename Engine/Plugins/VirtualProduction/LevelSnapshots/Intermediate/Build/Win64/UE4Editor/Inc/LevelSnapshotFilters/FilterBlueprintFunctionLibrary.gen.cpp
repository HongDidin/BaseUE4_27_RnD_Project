// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/FilterBlueprintFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFilterBlueprintFunctionLibrary() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UFilterBlueprintFunctionLibrary_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UFilterBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UFilterBlueprintFunctionLibrary::execCreateFilterByClass)
	{
		P_GET_OBJECT_REF_NO_PTR(TSubclassOf<ULevelSnapshotFilter> ,Z_Param_Out_Class);
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_OBJECT(UObject,Z_Param_Outer);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSnapshotFilter**)Z_Param__Result=UFilterBlueprintFunctionLibrary::CreateFilterByClass(Z_Param_Out_Class,Z_Param_Name,Z_Param_Outer);
		P_NATIVE_END;
	}
	void UFilterBlueprintFunctionLibrary::StaticRegisterNativesUFilterBlueprintFunctionLibrary()
	{
		UClass* Class = UFilterBlueprintFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateFilterByClass", &UFilterBlueprintFunctionLibrary::execCreateFilterByClass },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics
	{
		struct FilterBlueprintFunctionLibrary_eventCreateFilterByClass_Parms
		{
			const TSubclassOf<ULevelSnapshotFilter>  Class;
			FName Name;
			UObject* Outer;
			ULevelSnapshotFilter* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Outer;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Class_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0014000008000182, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FilterBlueprintFunctionLibrary_eventCreateFilterByClass_Parms, Class), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Class_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FilterBlueprintFunctionLibrary_eventCreateFilterByClass_Parms, Name), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Outer = { "Outer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FilterBlueprintFunctionLibrary_eventCreateFilterByClass_Parms, Outer), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FilterBlueprintFunctionLibrary_eventCreateFilterByClass_Parms, ReturnValue), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_Outer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "CPP_Default_Name", "None" },
		{ "CPP_Default_Outer", "None" },
		{ "ModuleRelativePath", "Public/FilterBlueprintFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UFilterBlueprintFunctionLibrary, nullptr, "CreateFilterByClass", nullptr, nullptr, sizeof(FilterBlueprintFunctionLibrary_eventCreateFilterByClass_Parms), Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UFilterBlueprintFunctionLibrary_NoRegister()
	{
		return UFilterBlueprintFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UFilterBlueprintFunctionLibrary_CreateFilterByClass, "CreateFilterByClass" }, // 1619537840
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FilterBlueprintFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/FilterBlueprintFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFilterBlueprintFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::ClassParams = {
		&UFilterBlueprintFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFilterBlueprintFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFilterBlueprintFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFilterBlueprintFunctionLibrary, 1036384463);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UFilterBlueprintFunctionLibrary>()
	{
		return UFilterBlueprintFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFilterBlueprintFunctionLibrary(Z_Construct_UClass_UFilterBlueprintFunctionLibrary, &UFilterBlueprintFunctionLibrary::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UFilterBlueprintFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFilterBlueprintFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
