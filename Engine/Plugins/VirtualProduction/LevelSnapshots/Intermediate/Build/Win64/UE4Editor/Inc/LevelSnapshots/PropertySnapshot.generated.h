// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_PropertySnapshot_generated_h
#error "PropertySnapshot.generated.h already included, missing '#pragma once' in PropertySnapshot.h"
#endif
#define LEVELSNAPSHOTS_PropertySnapshot_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_PropertySnapshot_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLevelSnapshot_Property_Statics; \
	LEVELSNAPSHOTS_API static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<struct FLevelSnapshot_Property>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Data_PropertySnapshot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
