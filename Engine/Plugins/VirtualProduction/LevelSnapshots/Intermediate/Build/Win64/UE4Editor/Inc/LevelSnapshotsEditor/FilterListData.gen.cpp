// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/FilterListData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFilterListData() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FFilterListData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	LEVELSNAPSHOTS_API UClass* Z_Construct_UClass_ULevelSnapshot_NoRegister();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FPropertySelectionMap();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References
class UScriptStruct* FFilterListData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTSEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FFilterListData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFilterListData, Z_Construct_UPackage__Script_LevelSnapshotsEditor(), TEXT("FilterListData"), sizeof(FFilterListData), Get_Z_Construct_UScriptStruct_FFilterListData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTSEDITOR_API UScriptStruct* StaticStruct<FFilterListData>()
{
	return FFilterListData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFilterListData(FFilterListData::StaticStruct, TEXT("/Script/LevelSnapshotsEditor"), TEXT("FilterListData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshotsEditor_StaticRegisterNativesFFilterListData
{
	FScriptStruct_LevelSnapshotsEditor_StaticRegisterNativesFFilterListData()
	{
		UScriptStruct::DeferCppStructOps<FFilterListData>(FName(TEXT("FilterListData")));
	}
} ScriptStruct_LevelSnapshotsEditor_StaticRegisterNativesFFilterListData;
	struct Z_Construct_UScriptStruct_FFilterListData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelatedSnapshot_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RelatedSnapshot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedActorsSelectedProperties_AllowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedActorsSelectedProperties_AllowedByFilter;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_ModifiedWorldActors_AllowedByFilter_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedWorldActors_AllowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ModifiedWorldActors_AllowedByFilter;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemovedOriginalActorPaths_AllowedByFilter_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemovedOriginalActorPaths_AllowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_RemovedOriginalActorPaths_AllowedByFilter;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_AddedWorldActors_AllowedByFilter_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddedWorldActors_AllowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_AddedWorldActors_AllowedByFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_ModifiedWorldActors_DisallowedByFilter_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedWorldActors_DisallowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ModifiedWorldActors_DisallowedByFilter;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemovedOriginalActorPaths_DisallowedByFilter_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemovedOriginalActorPaths_DisallowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_RemovedOriginalActorPaths_DisallowedByFilter;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_AddedWorldActors_DisallowedByFilter_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddedWorldActors_DisallowedByFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_AddedWorldActors_DisallowedByFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/* Contains all data required to display the filter results panel. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Contains all data required to display the filter results panel." },
	};
#endif
	void* Z_Construct_UScriptStruct_FFilterListData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFilterListData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RelatedSnapshot_MetaData[] = {
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RelatedSnapshot = { "RelatedSnapshot", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, RelatedSnapshot), Z_Construct_UClass_ULevelSnapshot_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RelatedSnapshot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RelatedSnapshot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_AllowedByFilter_MetaData[] = {
		{ "Comment", "/* Selected properties for actors allowed by filters. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Selected properties for actors allowed by filters." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_AllowedByFilter = { "ModifiedActorsSelectedProperties_AllowedByFilter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, ModifiedActorsSelectedProperties_AllowedByFilter), Z_Construct_UScriptStruct_FPropertySelectionMap, METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_AllowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_AllowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter_ElementProp = { "ModifiedWorldActors_AllowedByFilter", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter_MetaData[] = {
		{ "Comment", "/* Actors to show in filter results panel when \"ShowUnchanged = false\". */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Actors to show in filter results panel when \"ShowUnchanged = false\"." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter = { "ModifiedWorldActors_AllowedByFilter", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, ModifiedWorldActors_AllowedByFilter), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter_ElementProp = { "RemovedOriginalActorPaths_AllowedByFilter", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter_MetaData[] = {
		{ "Comment", "/* Actors which existed in snapshot but not in the world. Only contains entries that passed filters. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Actors which existed in snapshot but not in the world. Only contains entries that passed filters." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter = { "RemovedOriginalActorPaths_AllowedByFilter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, RemovedOriginalActorPaths_AllowedByFilter), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter_ElementProp = { "AddedWorldActors_AllowedByFilter", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter_MetaData[] = {
		{ "Comment", "/* Actors which existed in the world but not in the snapshot. Only contains entries that passed filters. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Actors which existed in the world but not in the snapshot. Only contains entries that passed filters." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter = { "AddedWorldActors_AllowedByFilter", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, AddedWorldActors_AllowedByFilter), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter_MetaData[] = {
		{ "Comment", "/* Selected properties for actors disallowed by filters. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Selected properties for actors disallowed by filters." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter = { "ModifiedActorsSelectedProperties_DisallowedByFilter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, ModifiedActorsSelectedProperties_DisallowedByFilter), Z_Construct_UScriptStruct_FPropertySelectionMap, METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter_ElementProp = { "ModifiedWorldActors_DisallowedByFilter", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter_MetaData[] = {
		{ "Comment", "/* Actors to show in filter results panel when \"ShowUnchanged = true\". */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Actors to show in filter results panel when \"ShowUnchanged = true\"." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter = { "ModifiedWorldActors_DisallowedByFilter", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, ModifiedWorldActors_DisallowedByFilter), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter_ElementProp = { "RemovedOriginalActorPaths_DisallowedByFilter", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter_MetaData[] = {
		{ "Comment", "/* Actors which existed in snapshot but not in the world. Only contains entries that did not pass filters. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Actors which existed in snapshot but not in the world. Only contains entries that did not pass filters." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter = { "RemovedOriginalActorPaths_DisallowedByFilter", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, RemovedOriginalActorPaths_DisallowedByFilter), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter_ElementProp = { "AddedWorldActors_DisallowedByFilter", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter_MetaData[] = {
		{ "Comment", "/* Actors which existed in the world but not in the snapshot. Only contains entries that did not pass filters. */" },
		{ "ModuleRelativePath", "Private/Data/FilterListData.h" },
		{ "ToolTip", "Actors which existed in the world but not in the snapshot. Only contains entries that did not pass filters." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter = { "AddedWorldActors_DisallowedByFilter", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterListData, AddedWorldActors_DisallowedByFilter), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFilterListData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RelatedSnapshot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_AllowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_AllowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_AllowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_AllowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedActorsSelectedProperties_DisallowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_ModifiedWorldActors_DisallowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_RemovedOriginalActorPaths_DisallowedByFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterListData_Statics::NewProp_AddedWorldActors_DisallowedByFilter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFilterListData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
		nullptr,
		&NewStructOps,
		"FilterListData",
		sizeof(FFilterListData),
		alignof(FFilterListData),
		Z_Construct_UScriptStruct_FFilterListData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterListData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterListData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFilterListData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFilterListData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotsEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FilterListData"), sizeof(FFilterListData), Get_Z_Construct_UScriptStruct_FFilterListData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFilterListData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFilterListData_Hash() { return 3305073694U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
