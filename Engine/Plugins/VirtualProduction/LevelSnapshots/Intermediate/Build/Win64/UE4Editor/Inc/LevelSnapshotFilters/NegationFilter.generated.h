// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ULevelSnapshotFilter;
#ifdef LEVELSNAPSHOTFILTERS_NegationFilter_generated_h
#error "NegationFilter.generated.h already included, missing '#pragma once' in NegationFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_NegationFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetChild); \
	DECLARE_FUNCTION(execSetExternalChild); \
	DECLARE_FUNCTION(execCreateChild);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetChild); \
	DECLARE_FUNCTION(execSetExternalChild); \
	DECLARE_FUNCTION(execCreateChild);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNegationFilter(); \
	friend struct Z_Construct_UClass_UNegationFilter_Statics; \
public: \
	DECLARE_CLASS(UNegationFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UNegationFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUNegationFilter(); \
	friend struct Z_Construct_UClass_UNegationFilter_Statics; \
public: \
	DECLARE_CLASS(UNegationFilter, ULevelSnapshotFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UNegationFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNegationFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNegationFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNegationFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNegationFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNegationFilter(UNegationFilter&&); \
	NO_API UNegationFilter(const UNegationFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNegationFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNegationFilter(UNegationFilter&&); \
	NO_API UNegationFilter(const UNegationFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNegationFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNegationFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNegationFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Child() { return STRUCT_OFFSET(UNegationFilter, Child); } \
	FORCEINLINE static uint32 __PPO__InstancedChild() { return STRUCT_OFFSET(UNegationFilter, InstancedChild); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_17_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UNegationFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_BlueprintOnly_NegationFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
