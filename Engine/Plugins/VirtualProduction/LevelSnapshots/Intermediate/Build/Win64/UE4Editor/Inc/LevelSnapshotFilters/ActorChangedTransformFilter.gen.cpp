// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/ActorChangedTransformFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorChangedTransformFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_ETransformReturnType();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorChangedTransformFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorChangedTransformFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UActorSelectorFilter();
// End Cross Module References
	static UEnum* ETransformReturnType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LevelSnapshotFilters_ETransformReturnType, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("ETransformReturnType"));
		}
		return Singleton;
	}
	template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<ETransformReturnType::Type>()
	{
		return ETransformReturnType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransformReturnType(ETransformReturnType_StaticEnum, TEXT("/Script/LevelSnapshotFilters"), TEXT("ETransformReturnType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LevelSnapshotFilters_ETransformReturnType_Hash() { return 640390847U; }
	UEnum* Z_Construct_UEnum_LevelSnapshotFilters_ETransformReturnType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransformReturnType"), 0, Get_Z_Construct_UEnum_LevelSnapshotFilters_ETransformReturnType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransformReturnType::IsValidWhenTransformChanged", (int64)ETransformReturnType::IsValidWhenTransformChanged },
				{ "ETransformReturnType::IsValidWhenTransformStayedSame", (int64)ETransformReturnType::IsValidWhenTransformStayedSame },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "IsValidWhenTransformChanged.Comment", "/* Return true if the snapshot and world actor have different transforms */" },
				{ "IsValidWhenTransformChanged.Name", "ETransformReturnType::IsValidWhenTransformChanged" },
				{ "IsValidWhenTransformChanged.ToolTip", "Return true if the snapshot and world actor have different transforms" },
				{ "IsValidWhenTransformStayedSame.Comment", "/* Returns true of the snapshot and world actor have the same transform */" },
				{ "IsValidWhenTransformStayedSame.Name", "ETransformReturnType::IsValidWhenTransformStayedSame" },
				{ "IsValidWhenTransformStayedSame.ToolTip", "Returns true of the snapshot and world actor have the same transform" },
				{ "ModuleRelativePath", "Public/Builtin/ActorChangedTransformFilter.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
				nullptr,
				"ETransformReturnType",
				"ETransformReturnType::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UActorChangedTransformFilter::StaticRegisterNativesUActorChangedTransformFilter()
	{
	}
	UClass* Z_Construct_UClass_UActorChangedTransformFilter_NoRegister()
	{
		return UActorChangedTransformFilter::StaticClass();
	}
	struct Z_Construct_UClass_UActorChangedTransformFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformCheckRule_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransformCheckRule;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreLocation_MetaData[];
#endif
		static void NewProp_bIgnoreLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreRotation_MetaData[];
#endif
		static void NewProp_bIgnoreRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreScale_MetaData[];
#endif
		static void NewProp_bIgnoreScale_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorChangedTransformFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorSelectorFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorChangedTransformFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Allows an actor depending on whether the actors' transforms have changed.\n * Use case: You want detect whether an actor has changed its transform.\n */" },
		{ "CommonSnapshotFilter", "" },
		{ "IncludePath", "Builtin/ActorChangedTransformFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/ActorChangedTransformFilter.h" },
		{ "ToolTip", "Allows an actor depending on whether the actors' transforms have changed.\nUse case: You want detect whether an actor has changed its transform." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_TransformCheckRule_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* Whether we allow actors that changed transform or that stayed at the same place. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorChangedTransformFilter.h" },
		{ "ToolTip", "Whether we allow actors that changed transform or that stayed at the same place." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_TransformCheckRule = { "TransformCheckRule", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorChangedTransformFilter, TransformCheckRule), Z_Construct_UEnum_LevelSnapshotFilters_ETransformReturnType, METADATA_PARAMS(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_TransformCheckRule_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_TransformCheckRule_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* If true, we do not compare the actors' locations. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorChangedTransformFilter.h" },
		{ "ToolTip", "If true, we do not compare the actors' locations." },
	};
#endif
	void Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation_SetBit(void* Obj)
	{
		((UActorChangedTransformFilter*)Obj)->bIgnoreLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation = { "bIgnoreLocation", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UActorChangedTransformFilter), &Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* If true, we do not compare the actors' rotations. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorChangedTransformFilter.h" },
		{ "ToolTip", "If true, we do not compare the actors' rotations." },
	};
#endif
	void Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation_SetBit(void* Obj)
	{
		((UActorChangedTransformFilter*)Obj)->bIgnoreRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation = { "bIgnoreRotation", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UActorChangedTransformFilter), &Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/* If true, we do not compare the actors' scales. */" },
		{ "ModuleRelativePath", "Public/Builtin/ActorChangedTransformFilter.h" },
		{ "ToolTip", "If true, we do not compare the actors' scales." },
	};
#endif
	void Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale_SetBit(void* Obj)
	{
		((UActorChangedTransformFilter*)Obj)->bIgnoreScale = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale = { "bIgnoreScale", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UActorChangedTransformFilter), &Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale_SetBit, METADATA_PARAMS(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActorChangedTransformFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_TransformCheckRule,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorChangedTransformFilter_Statics::NewProp_bIgnoreScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorChangedTransformFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorChangedTransformFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorChangedTransformFilter_Statics::ClassParams = {
		&UActorChangedTransformFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UActorChangedTransformFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UActorChangedTransformFilter_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UActorChangedTransformFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorChangedTransformFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorChangedTransformFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorChangedTransformFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorChangedTransformFilter, 2415245332);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UActorChangedTransformFilter>()
	{
		return UActorChangedTransformFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorChangedTransformFilter(Z_Construct_UClass_UActorChangedTransformFilter, &UActorChangedTransformFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UActorChangedTransformFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorChangedTransformFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
