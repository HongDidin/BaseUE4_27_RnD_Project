// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/LevelSnapshotFilters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotFilters() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotBlueprintFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotBlueprintFilter();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsActorValidParams();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsAddedActorValidParams();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsDeletedActorValidParams();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsPropertyValidParams();
// End Cross Module References
	static UEnum* EFilterResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("EFilterResult"));
		}
		return Singleton;
	}
	template<> LEVELSNAPSHOTFILTERS_API UEnum* StaticEnum<EFilterResult::Type>()
	{
		return EFilterResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFilterResult(EFilterResult_StaticEnum, TEXT("/Script/LevelSnapshotFilters"), TEXT("EFilterResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult_Hash() { return 35062161U; }
	UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFilterResult"), 0, Get_Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFilterResult::Include", (int64)EFilterResult::Include },
				{ "EFilterResult::Exclude", (int64)EFilterResult::Exclude },
				{ "EFilterResult::DoNotCare", (int64)EFilterResult::DoNotCare },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "DoNotCare.Comment", "/* The filter does not care what happens to this actor / property.\n\x09\x09 * Another filter will decide. If all filters don't care, actor / property is included.\n\x09\x09 *\n\x09\x09 * Use this for filters that only implement one function: IsActorValid or IsPropertyValid.\n\x09\x09 */" },
				{ "DoNotCare.Name", "EFilterResult::DoNotCare" },
				{ "DoNotCare.ToolTip", "The filter does not care what happens to this actor / property.\n               * Another filter will decide. If all filters don't care, actor / property is included.\n               *\n               * Use this for filters that only implement one function: IsActorValid or IsPropertyValid." },
				{ "Exclude.Comment", "/* This actor / property will be excluded.\n\x09\x09 */" },
				{ "Exclude.Name", "EFilterResult::Exclude" },
				{ "Exclude.ToolTip", "This actor / property will be excluded." },
				{ "Include.Comment", "/* This actor / property will be included. \n\x09\x09 */" },
				{ "Include.Name", "EFilterResult::Include" },
				{ "Include.ToolTip", "This actor / property will be included." },
				{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
				nullptr,
				"EFilterResult",
				"EFilterResult::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void ULevelSnapshotFilter::StaticRegisterNativesULevelSnapshotFilter()
	{
	}
	UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister()
	{
		return ULevelSnapshotFilter::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotFilter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base-class for filtering a level snapshot.\n * Native C++ classes should inherit directly from this class.\n */" },
		{ "IncludePath", "LevelSnapshotFilters.h" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
		{ "ToolTip", "Base-class for filtering a level snapshot.\nNative C++ classes should inherit directly from this class." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotFilter_Statics::ClassParams = {
		&ULevelSnapshotFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotFilter, 2881650419);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<ULevelSnapshotFilter>()
	{
		return ULevelSnapshotFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotFilter(Z_Construct_UClass_ULevelSnapshotFilter, &ULevelSnapshotFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("ULevelSnapshotFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotFilter);
	DEFINE_FUNCTION(ULevelSnapshotBlueprintFilter::execIsAddedActorValid)
	{
		P_GET_STRUCT_REF(FIsAddedActorValidParams,Z_Param_Out_Params);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<EFilterResult::Type>*)Z_Param__Result=P_THIS->IsAddedActorValid_Implementation(Z_Param_Out_Params);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshotBlueprintFilter::execIsDeletedActorValid)
	{
		P_GET_STRUCT_REF(FIsDeletedActorValidParams,Z_Param_Out_Params);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<EFilterResult::Type>*)Z_Param__Result=P_THIS->IsDeletedActorValid_Implementation(Z_Param_Out_Params);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshotBlueprintFilter::execIsPropertyValid)
	{
		P_GET_STRUCT_REF(FIsPropertyValidParams,Z_Param_Out_Params);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<EFilterResult::Type>*)Z_Param__Result=P_THIS->IsPropertyValid_Implementation(Z_Param_Out_Params);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSnapshotBlueprintFilter::execIsActorValid)
	{
		P_GET_STRUCT_REF(FIsActorValidParams,Z_Param_Out_Params);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<EFilterResult::Type>*)Z_Param__Result=P_THIS->IsActorValid_Implementation(Z_Param_Out_Params);
		P_NATIVE_END;
	}
	static FName NAME_ULevelSnapshotBlueprintFilter_IsActorValid = FName(TEXT("IsActorValid"));
	EFilterResult::Type ULevelSnapshotBlueprintFilter::IsActorValid(FIsActorValidParams const& Params) const
	{
		LevelSnapshotBlueprintFilter_eventIsActorValid_Parms Parms;
		Parms.Params=Params;
		const_cast<ULevelSnapshotBlueprintFilter*>(this)->ProcessEvent(FindFunctionChecked(NAME_ULevelSnapshotBlueprintFilter_IsActorValid),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_ULevelSnapshotBlueprintFilter_IsAddedActorValid = FName(TEXT("IsAddedActorValid"));
	EFilterResult::Type ULevelSnapshotBlueprintFilter::IsAddedActorValid(FIsAddedActorValidParams const& Params) const
	{
		LevelSnapshotBlueprintFilter_eventIsAddedActorValid_Parms Parms;
		Parms.Params=Params;
		const_cast<ULevelSnapshotBlueprintFilter*>(this)->ProcessEvent(FindFunctionChecked(NAME_ULevelSnapshotBlueprintFilter_IsAddedActorValid),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_ULevelSnapshotBlueprintFilter_IsDeletedActorValid = FName(TEXT("IsDeletedActorValid"));
	EFilterResult::Type ULevelSnapshotBlueprintFilter::IsDeletedActorValid(FIsDeletedActorValidParams const& Params) const
	{
		LevelSnapshotBlueprintFilter_eventIsDeletedActorValid_Parms Parms;
		Parms.Params=Params;
		const_cast<ULevelSnapshotBlueprintFilter*>(this)->ProcessEvent(FindFunctionChecked(NAME_ULevelSnapshotBlueprintFilter_IsDeletedActorValid),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_ULevelSnapshotBlueprintFilter_IsPropertyValid = FName(TEXT("IsPropertyValid"));
	EFilterResult::Type ULevelSnapshotBlueprintFilter::IsPropertyValid(FIsPropertyValidParams const& Params) const
	{
		LevelSnapshotBlueprintFilter_eventIsPropertyValid_Parms Parms;
		Parms.Params=Params;
		const_cast<ULevelSnapshotBlueprintFilter*>(this)->ProcessEvent(FindFunctionChecked(NAME_ULevelSnapshotBlueprintFilter_IsPropertyValid),&Parms);
		return Parms.ReturnValue;
	}
	void ULevelSnapshotBlueprintFilter::StaticRegisterNativesULevelSnapshotBlueprintFilter()
	{
		UClass* Class = ULevelSnapshotBlueprintFilter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsActorValid", &ULevelSnapshotBlueprintFilter::execIsActorValid },
			{ "IsAddedActorValid", &ULevelSnapshotBlueprintFilter::execIsAddedActorValid },
			{ "IsDeletedActorValid", &ULevelSnapshotBlueprintFilter::execIsDeletedActorValid },
			{ "IsPropertyValid", &ULevelSnapshotBlueprintFilter::execIsPropertyValid },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_Params_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsActorValid_Parms, Params), Z_Construct_UScriptStruct_FIsActorValidParams, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsActorValid_Parms, ReturnValue), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_Params,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/**\n\x09 * @return Whether the actor should be considered for the level snapshot.\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
		{ "ToolTip", "@return Whether the actor should be considered for the level snapshot." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotBlueprintFilter, nullptr, "IsActorValid", nullptr, nullptr, sizeof(LevelSnapshotBlueprintFilter_eventIsActorValid_Parms), Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_Params_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsAddedActorValid_Parms, Params), Z_Construct_UScriptStruct_FIsAddedActorValidParams, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsAddedActorValid_Parms, ReturnValue), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_Params,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/**\n\x09* This is called when an actor was added to the world since the snapshot had been taken. \n\x09* @return Whether to track the added actor\n\x09*/" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
		{ "ToolTip", "This is called when an actor was added to the world since the snapshot had been taken.\n@return Whether to track the added actor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotBlueprintFilter, nullptr, "IsAddedActorValid", nullptr, nullptr, sizeof(LevelSnapshotBlueprintFilter_eventIsAddedActorValid_Parms), Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_Params_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsDeletedActorValid_Parms, Params), Z_Construct_UScriptStruct_FIsDeletedActorValidParams, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsDeletedActorValid_Parms, ReturnValue), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_Params,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/**\n\x09* This is called when an actor was removed from the world since the snapshot had been taken.\n\x09* @return Whether to track the removed actor\n\x09*/" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
		{ "ToolTip", "This is called when an actor was removed from the world since the snapshot had been taken.\n@return Whether to track the removed actor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotBlueprintFilter, nullptr, "IsDeletedActorValid", nullptr, nullptr, sizeof(LevelSnapshotBlueprintFilter_eventIsDeletedActorValid_Parms), Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_Params_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsPropertyValid_Parms, Params), Z_Construct_UScriptStruct_FIsPropertyValidParams, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSnapshotBlueprintFilter_eventIsPropertyValid_Parms, ReturnValue), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_Params,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/**\n\x09 * @return Whether this property should be considered for rolling back to the version in the snapshot. \n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
		{ "ToolTip", "@return Whether this property should be considered for rolling back to the version in the snapshot." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSnapshotBlueprintFilter, nullptr, "IsPropertyValid", nullptr, nullptr, sizeof(LevelSnapshotBlueprintFilter_eventIsPropertyValid_Parms), Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULevelSnapshotBlueprintFilter_NoRegister()
	{
		return ULevelSnapshotBlueprintFilter::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsActorValid, "IsActorValid" }, // 2696243113
		{ &Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsAddedActorValid, "IsAddedActorValid" }, // 3557270998
		{ &Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsDeletedActorValid, "IsDeletedActorValid" }, // 2152273937
		{ &Z_Construct_UFunction_ULevelSnapshotBlueprintFilter_IsPropertyValid, "IsPropertyValid" }, // 2172665438
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base-class for filtering a level snapshot in Blueprints.\n */" },
		{ "IncludePath", "LevelSnapshotFilters.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilters.h" },
		{ "ToolTip", "Base-class for filtering a level snapshot in Blueprints." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSnapshotBlueprintFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::ClassParams = {
		&ULevelSnapshotBlueprintFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSnapshotBlueprintFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSnapshotBlueprintFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSnapshotBlueprintFilter, 1870304582);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<ULevelSnapshotBlueprintFilter>()
	{
		return ULevelSnapshotBlueprintFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSnapshotBlueprintFilter(Z_Construct_UClass_ULevelSnapshotBlueprintFilter, &ULevelSnapshotBlueprintFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("ULevelSnapshotBlueprintFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSnapshotBlueprintFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
