// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_LevelSnapshotFilterFactory_generated_h
#error "LevelSnapshotFilterFactory.generated.h already included, missing '#pragma once' in LevelSnapshotFilterFactory.h"
#endif
#define LEVELSNAPSHOTSEDITOR_LevelSnapshotFilterFactory_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotBlueprintFilterFactory(); \
	friend struct Z_Construct_UClass_ULevelSnapshotBlueprintFilterFactory_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotBlueprintFilterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotBlueprintFilterFactory)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotBlueprintFilterFactory(); \
	friend struct Z_Construct_UClass_ULevelSnapshotBlueprintFilterFactory_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotBlueprintFilterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotsEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotBlueprintFilterFactory)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotBlueprintFilterFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotBlueprintFilterFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotBlueprintFilterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotBlueprintFilterFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotBlueprintFilterFactory(ULevelSnapshotBlueprintFilterFactory&&); \
	NO_API ULevelSnapshotBlueprintFilterFactory(const ULevelSnapshotBlueprintFilterFactory&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotBlueprintFilterFactory(ULevelSnapshotBlueprintFilterFactory&&); \
	NO_API ULevelSnapshotBlueprintFilterFactory(const ULevelSnapshotBlueprintFilterFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotBlueprintFilterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotBlueprintFilterFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULevelSnapshotBlueprintFilterFactory)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<class ULevelSnapshotBlueprintFilterFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Factories_LevelSnapshotFilterFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
