// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/ConstantFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConstantFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UConstantFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UConstantFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UEnum* Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult();
// End Cross Module References
	void UConstantFilter::StaticRegisterNativesUConstantFilter()
	{
	}
	UClass* Z_Construct_UClass_UConstantFilter_NoRegister()
	{
		return UConstantFilter::StaticClass();
	}
	struct Z_Construct_UClass_UConstantFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsActorValidResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IsActorValidResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsPropertyValidResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IsPropertyValidResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsDeletedActorValidResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IsDeletedActorValidResult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsAddedActorValidResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IsAddedActorValidResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConstantFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstantFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* Filter which treats all actors the same. */" },
		{ "IncludePath", "Builtin/ConstantFilter.h" },
		{ "ModuleRelativePath", "Public/Builtin/ConstantFilter.h" },
		{ "ToolTip", "Filter which treats all actors the same." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsActorValidResult_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "//~ End ULevelSnapshotFilter Interface\n" },
		{ "ModuleRelativePath", "Public/Builtin/ConstantFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsActorValidResult = { "IsActorValidResult", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstantFilter, IsActorValidResult), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsActorValidResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsActorValidResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsPropertyValidResult_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/Builtin/ConstantFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsPropertyValidResult = { "IsPropertyValidResult", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstantFilter, IsPropertyValidResult), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsPropertyValidResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsPropertyValidResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsDeletedActorValidResult_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/Builtin/ConstantFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsDeletedActorValidResult = { "IsDeletedActorValidResult", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstantFilter, IsDeletedActorValidResult), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsDeletedActorValidResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsDeletedActorValidResult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsAddedActorValidResult_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/Builtin/ConstantFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsAddedActorValidResult = { "IsAddedActorValidResult", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstantFilter, IsAddedActorValidResult), Z_Construct_UEnum_LevelSnapshotFilters_EFilterResult, METADATA_PARAMS(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsAddedActorValidResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsAddedActorValidResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConstantFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsActorValidResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsPropertyValidResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsDeletedActorValidResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstantFilter_Statics::NewProp_IsAddedActorValidResult,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConstantFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConstantFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConstantFilter_Statics::ClassParams = {
		&UConstantFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConstantFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConstantFilter_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConstantFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConstantFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConstantFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConstantFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConstantFilter, 3203236486);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UConstantFilter>()
	{
		return UConstantFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConstantFilter(Z_Construct_UClass_UConstantFilter, &UConstantFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UConstantFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConstantFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
