// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/Builtin/BlueprintOnly/ParentFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeParentFilter() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UParentFilter_NoRegister();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_UParentFilter();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	LEVELSNAPSHOTFILTERS_API UClass* Z_Construct_UClass_ULevelSnapshotFilter_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	DEFINE_FUNCTION(UParentFilter::execGetChildren)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<ULevelSnapshotFilter*>*)Z_Param__Result=P_THIS->GetChildren();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UParentFilter::execCreateChild)
	{
		P_GET_OBJECT_REF_NO_PTR(TSubclassOf<ULevelSnapshotFilter> ,Z_Param_Out_Class);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSnapshotFilter**)Z_Param__Result=P_THIS->CreateChild(Z_Param_Out_Class);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UParentFilter::execRemovedChild)
	{
		P_GET_OBJECT(ULevelSnapshotFilter,Z_Param_Filter);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemovedChild(Z_Param_Filter);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UParentFilter::execAddChild)
	{
		P_GET_OBJECT(ULevelSnapshotFilter,Z_Param_Filter);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddChild(Z_Param_Filter);
		P_NATIVE_END;
	}
	void UParentFilter::StaticRegisterNativesUParentFilter()
	{
		UClass* Class = UParentFilter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddChild", &UParentFilter::execAddChild },
			{ "CreateChild", &UParentFilter::execCreateChild },
			{ "GetChildren", &UParentFilter::execGetChildren },
			{ "RemovedChild", &UParentFilter::execRemovedChild },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UParentFilter_AddChild_Statics
	{
		struct ParentFilter_eventAddChild_Parms
		{
			ULevelSnapshotFilter* Filter;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Filter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UParentFilter_AddChild_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ParentFilter_eventAddChild_Parms, Filter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UParentFilter_AddChild_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_AddChild_Statics::NewProp_Filter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UParentFilter_AddChild_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/**\n\x09 * Adds a child you already created to this filter\n\x09 *\n\x09 * If you intend to save your filter, add children using CreateChild.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
		{ "ToolTip", "Adds a child you already created to this filter\n\nIf you intend to save your filter, add children using CreateChild." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UParentFilter_AddChild_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UParentFilter, nullptr, "AddChild", nullptr, nullptr, sizeof(ParentFilter_eventAddChild_Parms), Z_Construct_UFunction_UParentFilter_AddChild_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_AddChild_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UParentFilter_AddChild_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_AddChild_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UParentFilter_AddChild()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UParentFilter_AddChild_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UParentFilter_CreateChild_Statics
	{
		struct ParentFilter_eventCreateChild_Parms
		{
			const TSubclassOf<ULevelSnapshotFilter>  Class;
			ULevelSnapshotFilter* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_Class_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0014000008000182, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ParentFilter_eventCreateChild_Parms, Class), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_Class_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ParentFilter_eventCreateChild_Parms, ReturnValue), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UParentFilter_CreateChild_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_CreateChild_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UParentFilter_CreateChild_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/**\n\x09 * Creates a child and adds it to this filter.\n\x09 * If you intend to save your filter, add children using this function.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
		{ "ToolTip", "Creates a child and adds it to this filter.\nIf you intend to save your filter, add children using this function." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UParentFilter_CreateChild_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UParentFilter, nullptr, "CreateChild", nullptr, nullptr, sizeof(ParentFilter_eventCreateChild_Parms), Z_Construct_UFunction_UParentFilter_CreateChild_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_CreateChild_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UParentFilter_CreateChild_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_CreateChild_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UParentFilter_CreateChild()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UParentFilter_CreateChild_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UParentFilter_GetChildren_Statics
	{
		struct ParentFilter_eventGetChildren_Parms
		{
			TArray<ULevelSnapshotFilter*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UParentFilter_GetChildren_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UParentFilter_GetChildren_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ParentFilter_eventGetChildren_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UParentFilter_GetChildren_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_GetChildren_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_GetChildren_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UParentFilter_GetChildren_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Gets the children in this filter */" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
		{ "ToolTip", "Gets the children in this filter" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UParentFilter_GetChildren_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UParentFilter, nullptr, "GetChildren", nullptr, nullptr, sizeof(ParentFilter_eventGetChildren_Parms), Z_Construct_UFunction_UParentFilter_GetChildren_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_GetChildren_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UParentFilter_GetChildren_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_GetChildren_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UParentFilter_GetChildren()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UParentFilter_GetChildren_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UParentFilter_RemovedChild_Statics
	{
		struct ParentFilter_eventRemovedChild_Parms
		{
			ULevelSnapshotFilter* Filter;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Filter;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ParentFilter_eventRemovedChild_Parms, Filter), Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ParentFilter_eventRemovedChild_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ParentFilter_eventRemovedChild_Parms), &Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::NewProp_Filter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Removes a child from this filter */" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
		{ "ToolTip", "Removes a child from this filter" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UParentFilter, nullptr, "RemovedChild", nullptr, nullptr, sizeof(ParentFilter_eventRemovedChild_Parms), Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UParentFilter_RemovedChild()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UParentFilter_RemovedChild_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UParentFilter_NoRegister()
	{
		return UParentFilter::StaticClass();
	}
	struct Z_Construct_UClass_UParentFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Children;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstancedChildren_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstancedChildren_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstancedChildren_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InstancedChildren;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UParentFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULevelSnapshotFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UParentFilter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UParentFilter_AddChild, "AddChild" }, // 2345309621
		{ &Z_Construct_UFunction_UParentFilter_CreateChild, "CreateChild" }, // 4085280412
		{ &Z_Construct_UFunction_UParentFilter_GetChildren, "GetChildren" }, // 2323156549
		{ &Z_Construct_UFunction_UParentFilter_RemovedChild, "RemovedChild" }, // 1450733610
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParentFilter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Builtin/BlueprintOnly/ParentFilter.h" },
		{ "InternalSnapshotFilter", "" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParentFilter_Statics::NewProp_Children_Inner = { "Children", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParentFilter_Statics::NewProp_Children_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UParentFilter_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParentFilter, Children), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UParentFilter_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParentFilter_Statics::NewProp_Children_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_Inner_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_Inner = { "InstancedChildren", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULevelSnapshotFilter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Builtin/BlueprintOnly/ParentFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren = { "InstancedChildren", nullptr, (EPropertyFlags)0x0040008000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UParentFilter, InstancedChildren), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UParentFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParentFilter_Statics::NewProp_Children_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParentFilter_Statics::NewProp_Children,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UParentFilter_Statics::NewProp_InstancedChildren,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UParentFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UParentFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UParentFilter_Statics::ClassParams = {
		&UParentFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UParentFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UParentFilter_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UParentFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UParentFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UParentFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UParentFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UParentFilter, 1875486575);
	template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<UParentFilter>()
	{
		return UParentFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UParentFilter(Z_Construct_UClass_UParentFilter, &UParentFilter::StaticClass, TEXT("/Script/LevelSnapshotFilters"), TEXT("UParentFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UParentFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
