// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelShapshotFilters/Public/LevelSnapshotFilterParams.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSnapshotFilterParams() {}
// Cross Module References
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsAddedActorValidParams();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotFilters();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsDeletedActorValidParams();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsPropertyValidParams();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FPropertyContainerHandle();
	LEVELSNAPSHOTFILTERS_API UScriptStruct* Z_Construct_UScriptStruct_FIsActorValidParams();
// End Cross Module References
class UScriptStruct* FIsAddedActorValidParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTFILTERS_API uint32 Get_Z_Construct_UScriptStruct_FIsAddedActorValidParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIsAddedActorValidParams, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("IsAddedActorValidParams"), sizeof(FIsAddedActorValidParams), Get_Z_Construct_UScriptStruct_FIsAddedActorValidParams_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<FIsAddedActorValidParams>()
{
	return FIsAddedActorValidParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIsAddedActorValidParams(FIsAddedActorValidParams::StaticStruct, TEXT("/Script/LevelSnapshotFilters"), TEXT("IsAddedActorValidParams"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsAddedActorValidParams
{
	FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsAddedActorValidParams()
	{
		UScriptStruct::DeferCppStructOps<FIsAddedActorValidParams>(FName(TEXT("IsAddedActorValidParams")));
	}
} ScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsAddedActorValidParams;
	struct Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FIsAddedActorValidParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::NewProp_NewActor_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* This actor was added to the level since the snapshot was taken. */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "This actor was added to the level since the snapshot was taken." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::NewProp_NewActor = { "NewActor", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsAddedActorValidParams, NewActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::NewProp_NewActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::NewProp_NewActor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::NewProp_NewActor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
		nullptr,
		&NewStructOps,
		"IsAddedActorValidParams",
		sizeof(FIsAddedActorValidParams),
		alignof(FIsAddedActorValidParams),
		Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FIsAddedActorValidParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FIsAddedActorValidParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IsAddedActorValidParams"), sizeof(FIsAddedActorValidParams), Get_Z_Construct_UScriptStruct_FIsAddedActorValidParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIsAddedActorValidParams_Hash() { return 3850091648U; }
class UScriptStruct* FIsDeletedActorValidParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTFILTERS_API uint32 Get_Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIsDeletedActorValidParams, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("IsDeletedActorValidParams"), sizeof(FIsDeletedActorValidParams), Get_Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<FIsDeletedActorValidParams>()
{
	return FIsDeletedActorValidParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIsDeletedActorValidParams(FIsDeletedActorValidParams::StaticStruct, TEXT("/Script/LevelSnapshotFilters"), TEXT("IsDeletedActorValidParams"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsDeletedActorValidParams
{
	FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsDeletedActorValidParams()
	{
		UScriptStruct::DeferCppStructOps<FIsDeletedActorValidParams>(FName(TEXT("IsDeletedActorValidParams")));
	}
} ScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsDeletedActorValidParams;
	struct Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SavedActorPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SavedActorPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FIsDeletedActorValidParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::NewProp_SavedActorPath_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Holds path info for an actor that was deleted since the snapshot was taken.\n\x09 * Contains the path to the original actor before it was deleted. */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "Holds path info for an actor that was deleted since the snapshot was taken.\n       * Contains the path to the original actor before it was deleted." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::NewProp_SavedActorPath = { "SavedActorPath", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsDeletedActorValidParams, SavedActorPath), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::NewProp_SavedActorPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::NewProp_SavedActorPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::NewProp_SavedActorPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
		nullptr,
		&NewStructOps,
		"IsDeletedActorValidParams",
		sizeof(FIsDeletedActorValidParams),
		alignof(FIsDeletedActorValidParams),
		Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FIsDeletedActorValidParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IsDeletedActorValidParams"), sizeof(FIsDeletedActorValidParams), Get_Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Hash() { return 2507579014U; }
class UScriptStruct* FIsPropertyValidParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTFILTERS_API uint32 Get_Z_Construct_UScriptStruct_FIsPropertyValidParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIsPropertyValidParams, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("IsPropertyValidParams"), sizeof(FIsPropertyValidParams), Get_Z_Construct_UScriptStruct_FIsPropertyValidParams_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<FIsPropertyValidParams>()
{
	return FIsPropertyValidParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIsPropertyValidParams(FIsPropertyValidParams::StaticStruct, TEXT("/Script/LevelSnapshotFilters"), TEXT("IsPropertyValidParams"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsPropertyValidParams
{
	FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsPropertyValidParams()
	{
		UScriptStruct::DeferCppStructOps<FIsPropertyValidParams>(FName(TEXT("IsPropertyValidParams")));
	}
} ScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsPropertyValidParams;
	struct Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnapshotActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotPropertyContainer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SnapshotPropertyContainer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelPropertyContainers_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LevelPropertyContainers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_Property;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PropertyPath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PropertyPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FIsPropertyValidParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotActor_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* The actor saved in the snapshot */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "The actor saved in the snapshot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotActor = { "SnapshotActor", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsPropertyValidParams, SnapshotActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelActor_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* The actor equivalent to LevelActor: it exists in the world */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "The actor equivalent to LevelActor: it exists in the world" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelActor = { "LevelActor", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsPropertyValidParams, LevelActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotPropertyContainer_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* For passing to FProperty::ContainerPtrToValuePtr. This is either SnapshotActor or a subobject thereof. */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "For passing to FProperty::ContainerPtrToValuePtr. This is either SnapshotActor or a subobject thereof." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotPropertyContainer = { "SnapshotPropertyContainer", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsPropertyValidParams, SnapshotPropertyContainer), Z_Construct_UScriptStruct_FPropertyContainerHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotPropertyContainer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotPropertyContainer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelPropertyContainers_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* For passing to FProperty::ContainerPtrToValuePtr. This is either LevelPropertyContainers or a subobject thereof. */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "For passing to FProperty::ContainerPtrToValuePtr. This is either LevelPropertyContainers or a subobject thereof." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelPropertyContainers = { "LevelPropertyContainers", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsPropertyValidParams, LevelPropertyContainers), Z_Construct_UScriptStruct_FPropertyContainerHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelPropertyContainers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelPropertyContainers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_Property_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* The property that we may want to rollback. */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "The property that we may want to rollback." },
	};
#endif
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_Property = { "Property", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsPropertyValidParams, Property), &FProperty::StaticClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_Property_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath_Inner = { "PropertyPath", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* Each elements is the name of a subobject name leading to this property. The last element is the property name.\n\x09 * The first element is either the name of a component or a struct/subobject in the root actor.\n\x09 *\n\x09 * Examples:\n\x09 *\x09- MyCustomComponent -> MyCustomStructPropertyName -> PropertyName\n\x09 *  - MyCustomComponent -> MyCustomStructPropertyName\n\x09 *\x09- StructPropertyNameInActor -> PropertyName\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "Each elements is the name of a subobject name leading to this property. The last element is the property name.\n       * The first element is either the name of a component or a struct/subobject in the root actor.\n       *\n       * Examples:\n       *      - MyCustomComponent -> MyCustomStructPropertyName -> PropertyName\n       *  - MyCustomComponent -> MyCustomStructPropertyName\n       *      - StructPropertyNameInActor -> PropertyName" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath = { "PropertyPath", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsPropertyValidParams, PropertyPath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_SnapshotPropertyContainer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_LevelPropertyContainers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::NewProp_PropertyPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
		nullptr,
		&NewStructOps,
		"IsPropertyValidParams",
		sizeof(FIsPropertyValidParams),
		alignof(FIsPropertyValidParams),
		Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FIsPropertyValidParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FIsPropertyValidParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IsPropertyValidParams"), sizeof(FIsPropertyValidParams), Get_Z_Construct_UScriptStruct_FIsPropertyValidParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIsPropertyValidParams_Hash() { return 3530328407U; }
class UScriptStruct* FIsActorValidParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTFILTERS_API uint32 Get_Z_Construct_UScriptStruct_FIsActorValidParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIsActorValidParams, Z_Construct_UPackage__Script_LevelSnapshotFilters(), TEXT("IsActorValidParams"), sizeof(FIsActorValidParams), Get_Z_Construct_UScriptStruct_FIsActorValidParams_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<FIsActorValidParams>()
{
	return FIsActorValidParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIsActorValidParams(FIsActorValidParams::StaticStruct, TEXT("/Script/LevelSnapshotFilters"), TEXT("IsActorValidParams"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsActorValidParams
{
	FScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsActorValidParams()
	{
		UScriptStruct::DeferCppStructOps<FIsActorValidParams>(FName(TEXT("IsActorValidParams")));
	}
} ScriptStruct_LevelSnapshotFilters_StaticRegisterNativesFIsActorValidParams;
	struct Z_Construct_UScriptStruct_FIsActorValidParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnapshotActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsActorValidParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FIsActorValidParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_SnapshotActor_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* The actor saved in the snapshot */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "The actor saved in the snapshot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_SnapshotActor = { "SnapshotActor", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsActorValidParams, SnapshotActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_SnapshotActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_SnapshotActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_LevelActor_MetaData[] = {
		{ "Category", "Level Snapshots" },
		{ "Comment", "/* The actor equivalent to LevelActor: it exists in the world */" },
		{ "ModuleRelativePath", "Public/LevelSnapshotFilterParams.h" },
		{ "ToolTip", "The actor equivalent to LevelActor: it exists in the world" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_LevelActor = { "LevelActor", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FIsActorValidParams, LevelActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_LevelActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_LevelActor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FIsActorValidParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_SnapshotActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FIsActorValidParams_Statics::NewProp_LevelActor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FIsActorValidParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotFilters,
		nullptr,
		&NewStructOps,
		"IsActorValidParams",
		sizeof(FIsActorValidParams),
		alignof(FIsActorValidParams),
		Z_Construct_UScriptStruct_FIsActorValidParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FIsActorValidParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FIsActorValidParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FIsActorValidParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshotFilters();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IsActorValidParams"), sizeof(FIsActorValidParams), Get_Z_Construct_UScriptStruct_FIsActorValidParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FIsActorValidParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIsActorValidParams_Hash() { return 2774176795U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
