// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_PropertyHasNameFilter_generated_h
#error "PropertyHasNameFilter.generated.h already included, missing '#pragma once' in PropertyHasNameFilter.h"
#endif
#define LEVELSNAPSHOTFILTERS_PropertyHasNameFilter_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPropertyHasNameFilter(); \
	friend struct Z_Construct_UClass_UPropertyHasNameFilter_Statics; \
public: \
	DECLARE_CLASS(UPropertyHasNameFilter, UPropertySelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UPropertyHasNameFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUPropertyHasNameFilter(); \
	friend struct Z_Construct_UClass_UPropertyHasNameFilter_Statics; \
public: \
	DECLARE_CLASS(UPropertyHasNameFilter, UPropertySelectorFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshotFilters"), NO_API) \
	DECLARE_SERIALIZER(UPropertyHasNameFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPropertyHasNameFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPropertyHasNameFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPropertyHasNameFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPropertyHasNameFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPropertyHasNameFilter(UPropertyHasNameFilter&&); \
	NO_API UPropertyHasNameFilter(const UPropertyHasNameFilter&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPropertyHasNameFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPropertyHasNameFilter(UPropertyHasNameFilter&&); \
	NO_API UPropertyHasNameFilter(const UPropertyHasNameFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPropertyHasNameFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPropertyHasNameFilter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPropertyHasNameFilter)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NameMatchingRule() { return STRUCT_OFFSET(UPropertyHasNameFilter, NameMatchingRule); } \
	FORCEINLINE static uint32 __PPO__AllowedNames() { return STRUCT_OFFSET(UPropertyHasNameFilter, AllowedNames); }


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_29_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTFILTERS_API UClass* StaticClass<class UPropertyHasNameFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_Builtin_PropertyHasNameFilter_h


#define FOREACH_ENUM_ENAMEMATCHINGRULE(op) \
	op(ENameMatchingRule::MatchesExactly) \
	op(ENameMatchingRule::MatchesIgnoreCase) \
	op(ENameMatchingRule::ContainsExactly) \
	op(ENameMatchingRule::ContainsIgnoreCase) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
