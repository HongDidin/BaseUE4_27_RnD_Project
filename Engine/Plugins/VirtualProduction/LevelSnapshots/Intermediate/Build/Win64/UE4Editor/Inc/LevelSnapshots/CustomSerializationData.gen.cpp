// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/Data/CustomSerializationData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCustomSerializationData() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FCustomSerializationData();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FCustomSubbjectSerializationData();
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FObjectSnapshotData();
// End Cross Module References
class UScriptStruct* FCustomSerializationData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FCustomSerializationData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCustomSerializationData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("CustomSerializationData"), sizeof(FCustomSerializationData), Get_Z_Construct_UScriptStruct_FCustomSerializationData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FCustomSerializationData>()
{
	return FCustomSerializationData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCustomSerializationData(FCustomSerializationData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("CustomSerializationData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFCustomSerializationData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFCustomSerializationData()
	{
		UScriptStruct::DeferCppStructOps<FCustomSerializationData>(FName(TEXT("CustomSerializationData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFCustomSerializationData;
	struct Z_Construct_UScriptStruct_FCustomSerializationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RootAnnotationData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootAnnotationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RootAnnotationData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Subobjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subobjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Subobjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomSerializationData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Data/CustomSerializationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCustomSerializationData>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData_Inner = { "RootAnnotationData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData_MetaData[] = {
		{ "Comment", "/* Additional custom data saved by ICustomSnapshotSerializationData */" },
		{ "ModuleRelativePath", "Public/Data/CustomSerializationData.h" },
		{ "ToolTip", "Additional custom data saved by ICustomSnapshotSerializationData" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData = { "RootAnnotationData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCustomSerializationData, RootAnnotationData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects_Inner = { "Subobjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCustomSubbjectSerializationData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects_MetaData[] = {
		{ "Comment", "/* Data for all subobjects */" },
		{ "ModuleRelativePath", "Public/Data/CustomSerializationData.h" },
		{ "ToolTip", "Data for all subobjects" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects = { "Subobjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCustomSerializationData, Subobjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCustomSerializationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_RootAnnotationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSerializationData_Statics::NewProp_Subobjects,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCustomSerializationData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"CustomSerializationData",
		sizeof(FCustomSerializationData),
		alignof(FCustomSerializationData),
		Z_Construct_UScriptStruct_FCustomSerializationData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSerializationData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCustomSerializationData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCustomSerializationData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CustomSerializationData"), sizeof(FCustomSerializationData), Get_Z_Construct_UScriptStruct_FCustomSerializationData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCustomSerializationData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCustomSerializationData_Hash() { return 468663694U; }

static_assert(std::is_polymorphic<FCustomSubbjectSerializationData>() == std::is_polymorphic<FObjectSnapshotData>(), "USTRUCT FCustomSubbjectSerializationData cannot be polymorphic unless super FObjectSnapshotData is polymorphic");

class UScriptStruct* FCustomSubbjectSerializationData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("CustomSubbjectSerializationData"), sizeof(FCustomSubbjectSerializationData), Get_Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FCustomSubbjectSerializationData>()
{
	return FCustomSubbjectSerializationData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCustomSubbjectSerializationData(FCustomSubbjectSerializationData::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("CustomSubbjectSerializationData"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFCustomSubbjectSerializationData
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFCustomSubbjectSerializationData()
	{
		UScriptStruct::DeferCppStructOps<FCustomSubbjectSerializationData>(FName(TEXT("CustomSubbjectSerializationData")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFCustomSubbjectSerializationData;
	struct Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPathIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ObjectPathIndex;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SubobjectAnnotationData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubobjectAnnotationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SubobjectAnnotationData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Data/CustomSerializationData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCustomSubbjectSerializationData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_ObjectPathIndex_MetaData[] = {
		{ "Comment", "/* Valid index to FWorldSnapshotData::SerializedObjectReferences */" },
		{ "ModuleRelativePath", "Public/Data/CustomSerializationData.h" },
		{ "ToolTip", "Valid index to FWorldSnapshotData::SerializedObjectReferences" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_ObjectPathIndex = { "ObjectPathIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCustomSubbjectSerializationData, ObjectPathIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_ObjectPathIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_ObjectPathIndex_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData_Inner = { "SubobjectAnnotationData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData_MetaData[] = {
		{ "Comment", "/* Additional custom data saved by ICustomSnapshotSerializationData */" },
		{ "ModuleRelativePath", "Public/Data/CustomSerializationData.h" },
		{ "ToolTip", "Additional custom data saved by ICustomSnapshotSerializationData" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData = { "SubobjectAnnotationData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCustomSubbjectSerializationData, SubobjectAnnotationData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_ObjectPathIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::NewProp_SubobjectAnnotationData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		Z_Construct_UScriptStruct_FObjectSnapshotData,
		&NewStructOps,
		"CustomSubbjectSerializationData",
		sizeof(FCustomSubbjectSerializationData),
		alignof(FCustomSubbjectSerializationData),
		Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCustomSubbjectSerializationData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CustomSubbjectSerializationData"), sizeof(FCustomSubbjectSerializationData), Get_Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCustomSubbjectSerializationData_Hash() { return 243716472U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
