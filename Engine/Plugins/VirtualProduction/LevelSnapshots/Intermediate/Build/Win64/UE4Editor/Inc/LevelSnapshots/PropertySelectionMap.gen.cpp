// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshots/Public/PropertySelectionMap.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePropertySelectionMap() {}
// Cross Module References
	LEVELSNAPSHOTS_API UScriptStruct* Z_Construct_UScriptStruct_FPropertySelectionMap();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshots();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
class UScriptStruct* FPropertySelectionMap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSNAPSHOTS_API uint32 Get_Z_Construct_UScriptStruct_FPropertySelectionMap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPropertySelectionMap, Z_Construct_UPackage__Script_LevelSnapshots(), TEXT("PropertySelectionMap"), sizeof(FPropertySelectionMap), Get_Z_Construct_UScriptStruct_FPropertySelectionMap_Hash());
	}
	return Singleton;
}
template<> LEVELSNAPSHOTS_API UScriptStruct* StaticStruct<FPropertySelectionMap>()
{
	return FPropertySelectionMap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPropertySelectionMap(FPropertySelectionMap::StaticStruct, TEXT("/Script/LevelSnapshots"), TEXT("PropertySelectionMap"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSnapshots_StaticRegisterNativesFPropertySelectionMap
{
	FScriptStruct_LevelSnapshots_StaticRegisterNativesFPropertySelectionMap()
	{
		UScriptStruct::DeferCppStructOps<FPropertySelectionMap>(FName(TEXT("PropertySelectionMap")));
	}
} ScriptStruct_LevelSnapshots_StaticRegisterNativesFPropertySelectionMap;
	struct Z_Construct_UScriptStruct_FPropertySelectionMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeletedActorsToRespawn_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeletedActorsToRespawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_DeletedActorsToRespawn;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_NewActorsToDespawn_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewActorsToDespawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_NewActorsToDespawn;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/* Binds an object to its selected properties */" },
		{ "ModuleRelativePath", "Public/PropertySelectionMap.h" },
		{ "ToolTip", "Binds an object to its selected properties" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPropertySelectionMap>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn_ElementProp = { "DeletedActorsToRespawn", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn_MetaData[] = {
		{ "Comment", "/* These actors were removed since the snapshot was taken. Re-create them.\n\x09 * This contains the original objects paths stored in the snapshot.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PropertySelectionMap.h" },
		{ "ToolTip", "These actors were removed since the snapshot was taken. Re-create them.\n       * This contains the original objects paths stored in the snapshot." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn = { "DeletedActorsToRespawn", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPropertySelectionMap, DeletedActorsToRespawn), METADATA_PARAMS(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn_MetaData)) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn_ElementProp = { "NewActorsToDespawn", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn_MetaData[] = {
		{ "Comment", "/* These actors were added since the snapshot was taken. Remove them. */" },
		{ "ModuleRelativePath", "Public/PropertySelectionMap.h" },
		{ "ToolTip", "These actors were added since the snapshot was taken. Remove them." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn = { "NewActorsToDespawn", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPropertySelectionMap, NewActorsToDespawn), METADATA_PARAMS(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_DeletedActorsToRespawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::NewProp_NewActorsToDespawn,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshots,
		nullptr,
		&NewStructOps,
		"PropertySelectionMap",
		sizeof(FPropertySelectionMap),
		alignof(FPropertySelectionMap),
		Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPropertySelectionMap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPropertySelectionMap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSnapshots();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PropertySelectionMap"), sizeof(FPropertySelectionMap), Get_Z_Construct_UScriptStruct_FPropertySelectionMap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPropertySelectionMap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPropertySelectionMap_Hash() { return 4187281674U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
