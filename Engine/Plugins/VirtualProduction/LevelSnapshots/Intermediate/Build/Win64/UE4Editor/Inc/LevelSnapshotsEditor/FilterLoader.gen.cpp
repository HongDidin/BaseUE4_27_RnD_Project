// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSnapshotsEditor/Private/Data/FilterLoader.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFilterLoader() {}
// Cross Module References
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFilterLoader_NoRegister();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_UFilterLoader();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LevelSnapshotsEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	LEVELSNAPSHOTSEDITOR_API UClass* Z_Construct_UClass_ULevelSnapshotsFilterPreset_NoRegister();
// End Cross Module References
	void UFilterLoader::StaticRegisterNativesUFilterLoader()
	{
	}
	UClass* Z_Construct_UClass_UFilterLoader_NoRegister()
	{
		return UFilterLoader::StaticClass();
	}
	struct Z_Construct_UClass_UFilterLoader_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetLastSavedOrLoaded_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssetLastSavedOrLoaded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetBeingEdited_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetBeingEdited;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFilterLoader_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSnapshotsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFilterLoader_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* Handles saving and loading of UDisjunctiveNormalFormFilter. */" },
		{ "IncludePath", "Data/FilterLoader.h" },
		{ "ModuleRelativePath", "Private/Data/FilterLoader.h" },
		{ "ToolTip", "Handles saving and loading of UDisjunctiveNormalFormFilter." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetLastSavedOrLoaded_MetaData[] = {
		{ "Comment", "/* Set once user either has used RequestSaveAs or SetPickedAsset. */" },
		{ "ModuleRelativePath", "Private/Data/FilterLoader.h" },
		{ "ToolTip", "Set once user either has used RequestSaveAs or SetPickedAsset." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetLastSavedOrLoaded = { "AssetLastSavedOrLoaded", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFilterLoader, AssetLastSavedOrLoaded), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetLastSavedOrLoaded_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetLastSavedOrLoaded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetBeingEdited_MetaData[] = {
		{ "ModuleRelativePath", "Private/Data/FilterLoader.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetBeingEdited = { "AssetBeingEdited", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFilterLoader, AssetBeingEdited), Z_Construct_UClass_ULevelSnapshotsFilterPreset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetBeingEdited_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetBeingEdited_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFilterLoader_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetLastSavedOrLoaded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFilterLoader_Statics::NewProp_AssetBeingEdited,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFilterLoader_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFilterLoader>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFilterLoader_Statics::ClassParams = {
		&UFilterLoader::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFilterLoader_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFilterLoader_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFilterLoader_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFilterLoader_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFilterLoader()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFilterLoader_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFilterLoader, 339885023);
	template<> LEVELSNAPSHOTSEDITOR_API UClass* StaticClass<UFilterLoader>()
	{
		return UFilterLoader::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFilterLoader(Z_Construct_UClass_UFilterLoader, &UFilterLoader::StaticClass, TEXT("/Script/LevelSnapshotsEditor"), TEXT("UFilterLoader"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFilterLoader);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
