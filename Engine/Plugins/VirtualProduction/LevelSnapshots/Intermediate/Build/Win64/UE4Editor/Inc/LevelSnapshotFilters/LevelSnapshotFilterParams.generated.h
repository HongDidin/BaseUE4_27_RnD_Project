// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTFILTERS_LevelSnapshotFilterParams_generated_h
#error "LevelSnapshotFilterParams.generated.h already included, missing '#pragma once' in LevelSnapshotFilterParams.h"
#endif
#define LEVELSNAPSHOTFILTERS_LevelSnapshotFilterParams_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilterParams_h_100_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FIsAddedActorValidParams_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<struct FIsAddedActorValidParams>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilterParams_h_79_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FIsDeletedActorValidParams_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<struct FIsDeletedActorValidParams>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilterParams_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FIsPropertyValidParams_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<struct FIsPropertyValidParams>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilterParams_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FIsActorValidParams_Statics; \
	static class UScriptStruct* StaticStruct();


template<> LEVELSNAPSHOTFILTERS_API UScriptStruct* StaticStruct<struct FIsActorValidParams>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelShapshotFilters_Public_LevelSnapshotFilterParams_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
