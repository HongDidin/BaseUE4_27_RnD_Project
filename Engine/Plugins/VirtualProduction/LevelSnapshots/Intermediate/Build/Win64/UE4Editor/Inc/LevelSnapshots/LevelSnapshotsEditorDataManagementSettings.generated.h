// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_LevelSnapshotsEditorDataManagementSettings_generated_h
#error "LevelSnapshotsEditorDataManagementSettings.generated.h already included, missing '#pragma once' in LevelSnapshotsEditorDataManagementSettings.h"
#endif
#define LEVELSNAPSHOTS_LevelSnapshotsEditorDataManagementSettings_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execParseLevelSnapshotsTokensInText);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execParseLevelSnapshotsTokensInText);


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorDataManagementSettings(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorDataManagementSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorDataManagementSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_INCLASS \
private: \
	static void StaticRegisterNativesULevelSnapshotsEditorDataManagementSettings(); \
	friend struct Z_Construct_UClass_ULevelSnapshotsEditorDataManagementSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSnapshotsEditorDataManagementSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ULevelSnapshotsEditorDataManagementSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSnapshotsEditorDataManagementSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorDataManagementSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorDataManagementSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorDataManagementSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorDataManagementSettings(ULevelSnapshotsEditorDataManagementSettings&&); \
	NO_API ULevelSnapshotsEditorDataManagementSettings(const ULevelSnapshotsEditorDataManagementSettings&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSnapshotsEditorDataManagementSettings(ULevelSnapshotsEditorDataManagementSettings&&); \
	NO_API ULevelSnapshotsEditorDataManagementSettings(const ULevelSnapshotsEditorDataManagementSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSnapshotsEditorDataManagementSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSnapshotsEditorDataManagementSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSnapshotsEditorDataManagementSettings)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_8_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class ULevelSnapshotsEditorDataManagementSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Public_Settings_LevelSnapshotsEditorDataManagementSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
