// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTS_SnapshotTestActor_generated_h
#error "SnapshotTestActor.generated.h already included, missing '#pragma once' in SnapshotTestActor.h"
#endif
#define LEVELSNAPSHOTS_SnapshotTestActor_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSubSubobject(); \
	friend struct Z_Construct_UClass_USubSubobject_Statics; \
public: \
	DECLARE_CLASS(USubSubobject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(USubSubobject)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSubSubobject(); \
	friend struct Z_Construct_UClass_USubSubobject_Statics; \
public: \
	DECLARE_CLASS(USubSubobject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(USubSubobject)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USubSubobject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USubSubobject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubSubobject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubSubobject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubSubobject(USubSubobject&&); \
	NO_API USubSubobject(const USubSubobject&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USubSubobject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubSubobject(USubSubobject&&); \
	NO_API USubSubobject(const USubSubobject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubSubobject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubSubobject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USubSubobject)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class USubSubobject>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSubobject(); \
	friend struct Z_Construct_UClass_USubobject_Statics; \
public: \
	DECLARE_CLASS(USubobject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(USubobject)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUSubobject(); \
	friend struct Z_Construct_UClass_USubobject_Statics; \
public: \
	DECLARE_CLASS(USubobject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(USubobject)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USubobject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USubobject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubobject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubobject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubobject(USubobject&&); \
	NO_API USubobject(const USubobject&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubobject(USubobject&&); \
	NO_API USubobject(const USubobject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubobject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubobject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USubobject)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_28_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class USubobject>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSnapshotTestComponent(); \
	friend struct Z_Construct_UClass_USnapshotTestComponent_Statics; \
public: \
	DECLARE_CLASS(USnapshotTestComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(USnapshotTestComponent)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUSnapshotTestComponent(); \
	friend struct Z_Construct_UClass_USnapshotTestComponent_Statics; \
public: \
	DECLARE_CLASS(USnapshotTestComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(USnapshotTestComponent)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USnapshotTestComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USnapshotTestComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USnapshotTestComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USnapshotTestComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USnapshotTestComponent(USnapshotTestComponent&&); \
	NO_API USnapshotTestComponent(const USnapshotTestComponent&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USnapshotTestComponent(USnapshotTestComponent&&); \
	NO_API USnapshotTestComponent(const USnapshotTestComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USnapshotTestComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USnapshotTestComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USnapshotTestComponent)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_46_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_49_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class USnapshotTestComponent>();

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnapshotTestActor(); \
	friend struct Z_Construct_UClass_ASnapshotTestActor_Statics; \
public: \
	DECLARE_CLASS(ASnapshotTestActor, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ASnapshotTestActor)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_INCLASS \
private: \
	static void StaticRegisterNativesASnapshotTestActor(); \
	friend struct Z_Construct_UClass_ASnapshotTestActor_Statics; \
public: \
	DECLARE_CLASS(ASnapshotTestActor, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSnapshots"), NO_API) \
	DECLARE_SERIALIZER(ASnapshotTestActor)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnapshotTestActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnapshotTestActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnapshotTestActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnapshotTestActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnapshotTestActor(ASnapshotTestActor&&); \
	NO_API ASnapshotTestActor(const ASnapshotTestActor&); \
public:


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnapshotTestActor(ASnapshotTestActor&&); \
	NO_API ASnapshotTestActor(const ASnapshotTestActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnapshotTestActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnapshotTestActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnapshotTestActor)


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_64_PROLOG
#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_INCLASS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h_67_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSNAPSHOTS_API UClass* StaticClass<class ASnapshotTestActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshots_Private_Tests_SnapshotTestActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
