// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSNAPSHOTSEDITOR_FilterListData_generated_h
#error "FilterListData.generated.h already included, missing '#pragma once' in FilterListData.h"
#endif
#define LEVELSNAPSHOTSEDITOR_FilterListData_generated_h

#define Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterListData_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFilterListData_Statics; \
	LEVELSNAPSHOTSEDITOR_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__RelatedSnapshot() { return STRUCT_OFFSET(FFilterListData, RelatedSnapshot); } \
	FORCEINLINE static uint32 __PPO__ModifiedActorsSelectedProperties_AllowedByFilter() { return STRUCT_OFFSET(FFilterListData, ModifiedActorsSelectedProperties_AllowedByFilter); } \
	FORCEINLINE static uint32 __PPO__ModifiedWorldActors_AllowedByFilter() { return STRUCT_OFFSET(FFilterListData, ModifiedWorldActors_AllowedByFilter); } \
	FORCEINLINE static uint32 __PPO__RemovedOriginalActorPaths_AllowedByFilter() { return STRUCT_OFFSET(FFilterListData, RemovedOriginalActorPaths_AllowedByFilter); } \
	FORCEINLINE static uint32 __PPO__AddedWorldActors_AllowedByFilter() { return STRUCT_OFFSET(FFilterListData, AddedWorldActors_AllowedByFilter); } \
	FORCEINLINE static uint32 __PPO__ModifiedActorsSelectedProperties_DisallowedByFilter() { return STRUCT_OFFSET(FFilterListData, ModifiedActorsSelectedProperties_DisallowedByFilter); } \
	FORCEINLINE static uint32 __PPO__ModifiedWorldActors_DisallowedByFilter() { return STRUCT_OFFSET(FFilterListData, ModifiedWorldActors_DisallowedByFilter); } \
	FORCEINLINE static uint32 __PPO__RemovedOriginalActorPaths_DisallowedByFilter() { return STRUCT_OFFSET(FFilterListData, RemovedOriginalActorPaths_DisallowedByFilter); } \
	FORCEINLINE static uint32 __PPO__AddedWorldActors_DisallowedByFilter() { return STRUCT_OFFSET(FFilterListData, AddedWorldActors_DisallowedByFilter); }


template<> LEVELSNAPSHOTSEDITOR_API UScriptStruct* StaticStruct<struct FFilterListData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LevelSnapshots_Source_LevelSnapshotsEditor_Private_Data_FilterListData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
