// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkVRPN/Public/LiveLinkVRPNConnectionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkVRPNConnectionSettings() {}
// Cross Module References
	LIVELINKVRPN_API UEnum* Z_Construct_UEnum_LiveLinkVRPN_EVRPNDeviceType();
	UPackage* Z_Construct_UPackage__Script_LiveLinkVRPN();
	LIVELINKVRPN_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings();
// End Cross Module References
	static UEnum* EVRPNDeviceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LiveLinkVRPN_EVRPNDeviceType, Z_Construct_UPackage__Script_LiveLinkVRPN(), TEXT("EVRPNDeviceType"));
		}
		return Singleton;
	}
	template<> LIVELINKVRPN_API UEnum* StaticEnum<EVRPNDeviceType>()
	{
		return EVRPNDeviceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVRPNDeviceType(EVRPNDeviceType_StaticEnum, TEXT("/Script/LiveLinkVRPN"), TEXT("EVRPNDeviceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LiveLinkVRPN_EVRPNDeviceType_Hash() { return 4030796269U; }
	UEnum* Z_Construct_UEnum_LiveLinkVRPN_EVRPNDeviceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkVRPN();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVRPNDeviceType"), 0, Get_Z_Construct_UEnum_LiveLinkVRPN_EVRPNDeviceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVRPNDeviceType::Analog", (int64)EVRPNDeviceType::Analog },
				{ "EVRPNDeviceType::Dial", (int64)EVRPNDeviceType::Dial },
				{ "EVRPNDeviceType::Button", (int64)EVRPNDeviceType::Button },
				{ "EVRPNDeviceType::Tracker", (int64)EVRPNDeviceType::Tracker },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Analog.Name", "EVRPNDeviceType::Analog" },
				{ "BlueprintType", "true" },
				{ "Button.Name", "EVRPNDeviceType::Button" },
				{ "Dial.Name", "EVRPNDeviceType::Dial" },
				{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
				{ "Tracker.Name", "EVRPNDeviceType::Tracker" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LiveLinkVRPN,
				nullptr,
				"EVRPNDeviceType",
				"EVRPNDeviceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FLiveLinkVRPNConnectionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKVRPN_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings, Z_Construct_UPackage__Script_LiveLinkVRPN(), TEXT("LiveLinkVRPNConnectionSettings"), sizeof(FLiveLinkVRPNConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Hash());
	}
	return Singleton;
}
template<> LIVELINKVRPN_API UScriptStruct* StaticStruct<FLiveLinkVRPNConnectionSettings>()
{
	return FLiveLinkVRPNConnectionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkVRPNConnectionSettings(FLiveLinkVRPNConnectionSettings::StaticStruct, TEXT("/Script/LiveLinkVRPN"), TEXT("LiveLinkVRPNConnectionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkVRPN_StaticRegisterNativesFLiveLinkVRPNConnectionSettings
{
	FScriptStruct_LiveLinkVRPN_StaticRegisterNativesFLiveLinkVRPNConnectionSettings()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkVRPNConnectionSettings>(FName(TEXT("LiveLinkVRPNConnectionSettings")));
	}
} ScriptStruct_LiveLinkVRPN_StaticRegisterNativesFLiveLinkVRPNConnectionSettings;
	struct Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IPAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IPAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalUpdateRateInHz_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_LocalUpdateRateInHz;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SubjectName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkVRPNConnectionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_IPAddress_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** IP address of the VRPN server */" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
		{ "ToolTip", "IP address of the VRPN server" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_IPAddress = { "IPAddress", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkVRPNConnectionSettings, IPAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_IPAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_IPAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_LocalUpdateRateInHz_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum rate (in Hz) at which to ask the VRPN server to update */" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
		{ "ToolTip", "Maximum rate (in Hz) at which to ask the VRPN server to update" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_LocalUpdateRateInHz = { "LocalUpdateRateInHz", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkVRPNConnectionSettings, LocalUpdateRateInHz), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_LocalUpdateRateInHz_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_LocalUpdateRateInHz_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** VRPN device name */" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
		{ "ToolTip", "VRPN device name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkVRPNConnectionSettings, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_DeviceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_SubjectName_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** LiveLink subject name */" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
		{ "ToolTip", "LiveLink subject name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_SubjectName = { "SubjectName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkVRPNConnectionSettings, SubjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_SubjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_SubjectName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** Device type */" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNConnectionSettings.h" },
		{ "ToolTip", "Device type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkVRPNConnectionSettings, Type), Z_Construct_UEnum_LiveLinkVRPN_EVRPNDeviceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_IPAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_LocalUpdateRateInHz,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_DeviceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_SubjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::NewProp_Type,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkVRPN,
		nullptr,
		&NewStructOps,
		"LiveLinkVRPNConnectionSettings",
		sizeof(FLiveLinkVRPNConnectionSettings),
		alignof(FLiveLinkVRPNConnectionSettings),
		Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkVRPN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkVRPNConnectionSettings"), sizeof(FLiveLinkVRPNConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkVRPNConnectionSettings_Hash() { return 2991237057U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
