// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkVRPN/Public/LiveLinkVRPNSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkVRPNSourceSettings() {}
// Cross Module References
	LIVELINKVRPN_API UClass* Z_Construct_UClass_ULiveLinkVRPNSourceSettings_NoRegister();
	LIVELINKVRPN_API UClass* Z_Construct_UClass_ULiveLinkVRPNSourceSettings();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkSourceSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkVRPN();
// End Cross Module References
	void ULiveLinkVRPNSourceSettings::StaticRegisterNativesULiveLinkVRPNSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkVRPNSourceSettings_NoRegister()
	{
		return ULiveLinkVRPNSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkSourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkVRPN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkVRPNSourceSettings.h" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNSourceSettings.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkVRPNSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::ClassParams = {
		&ULiveLinkVRPNSourceSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkVRPNSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkVRPNSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkVRPNSourceSettings, 3981444598);
	template<> LIVELINKVRPN_API UClass* StaticClass<ULiveLinkVRPNSourceSettings>()
	{
		return ULiveLinkVRPNSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkVRPNSourceSettings(Z_Construct_UClass_ULiveLinkVRPNSourceSettings, &ULiveLinkVRPNSourceSettings::StaticClass, TEXT("/Script/LiveLinkVRPN"), TEXT("ULiveLinkVRPNSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkVRPNSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
