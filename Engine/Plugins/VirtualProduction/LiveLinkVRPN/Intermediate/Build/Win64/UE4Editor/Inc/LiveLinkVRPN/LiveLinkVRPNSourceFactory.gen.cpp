// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkVRPN/Public/LiveLinkVRPNSourceFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkVRPNSourceFactory() {}
// Cross Module References
	LIVELINKVRPN_API UClass* Z_Construct_UClass_ULiveLinkVRPNSourceFactory_NoRegister();
	LIVELINKVRPN_API UClass* Z_Construct_UClass_ULiveLinkVRPNSourceFactory();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkSourceFactory();
	UPackage* Z_Construct_UPackage__Script_LiveLinkVRPN();
// End Cross Module References
	void ULiveLinkVRPNSourceFactory::StaticRegisterNativesULiveLinkVRPNSourceFactory()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkVRPNSourceFactory_NoRegister()
	{
		return ULiveLinkVRPNSourceFactory::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkSourceFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkVRPN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkVRPNSourceFactory.h" },
		{ "ModuleRelativePath", "Public/LiveLinkVRPNSourceFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkVRPNSourceFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::ClassParams = {
		&ULiveLinkVRPNSourceFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkVRPNSourceFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkVRPNSourceFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkVRPNSourceFactory, 729631236);
	template<> LIVELINKVRPN_API UClass* StaticClass<ULiveLinkVRPNSourceFactory>()
	{
		return ULiveLinkVRPNSourceFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkVRPNSourceFactory(Z_Construct_UClass_ULiveLinkVRPNSourceFactory, &ULiveLinkVRPNSourceFactory::StaticClass, TEXT("/Script/LiveLinkVRPN"), TEXT("ULiveLinkVRPNSourceFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkVRPNSourceFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
