// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTTAKERECORDER_ConcertTakeRecorderMessages_generated_h
#error "ConcertTakeRecorderMessages.generated.h already included, missing '#pragma once' in ConcertTakeRecorderMessages.h"
#endif
#define CONCERTTAKERECORDER_ConcertTakeRecorderMessages_generated_h

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_121_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FConcertRecordingCancelledEvent>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_112_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FConcertRecordingFinishedEvent>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_91_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FConcertTakeInitializedEvent>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_78_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FConcertMultiUserSyncChangeEvent>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_66_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FConcertRecordSettingsChangeEvent>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_38_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FConcertClientRecordSetting>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTakeRecordSettings_Statics; \
	CONCERTTAKERECORDER_API static class UScriptStruct* StaticStruct();


template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<struct FTakeRecordSettings>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertTakeSynchronization(); \
	friend struct Z_Construct_UClass_UConcertTakeSynchronization_Statics; \
public: \
	DECLARE_CLASS(UConcertTakeSynchronization, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertTakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UConcertTakeSynchronization) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUConcertTakeSynchronization(); \
	friend struct Z_Construct_UClass_UConcertTakeSynchronization_Statics; \
public: \
	DECLARE_CLASS(UConcertTakeSynchronization, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertTakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UConcertTakeSynchronization) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertTakeSynchronization(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertTakeSynchronization) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertTakeSynchronization); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertTakeSynchronization); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertTakeSynchronization(UConcertTakeSynchronization&&); \
	NO_API UConcertTakeSynchronization(const UConcertTakeSynchronization&); \
public:


#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertTakeSynchronization(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertTakeSynchronization(UConcertTakeSynchronization&&); \
	NO_API UConcertTakeSynchronization(const UConcertTakeSynchronization&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertTakeSynchronization); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertTakeSynchronization); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertTakeSynchronization)


#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTTAKERECORDER_API UClass* StaticClass<class UConcertTakeSynchronization>();

#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertSessionRecordSettings(); \
	friend struct Z_Construct_UClass_UConcertSessionRecordSettings_Statics; \
public: \
	DECLARE_CLASS(UConcertSessionRecordSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertTakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UConcertSessionRecordSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_INCLASS \
private: \
	static void StaticRegisterNativesUConcertSessionRecordSettings(); \
	friend struct Z_Construct_UClass_UConcertSessionRecordSettings_Statics; \
public: \
	DECLARE_CLASS(UConcertSessionRecordSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertTakeRecorder"), NO_API) \
	DECLARE_SERIALIZER(UConcertSessionRecordSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertSessionRecordSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertSessionRecordSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertSessionRecordSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertSessionRecordSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertSessionRecordSettings(UConcertSessionRecordSettings&&); \
	NO_API UConcertSessionRecordSettings(const UConcertSessionRecordSettings&); \
public:


#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertSessionRecordSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertSessionRecordSettings(UConcertSessionRecordSettings&&); \
	NO_API UConcertSessionRecordSettings(const UConcertSessionRecordSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertSessionRecordSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertSessionRecordSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertSessionRecordSettings)


#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_50_PROLOG
#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_INCLASS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h_54_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTTAKERECORDER_API UClass* StaticClass<class UConcertSessionRecordSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_MultiUserTakes_Source_ConcertTakeRecorder_Private_ConcertTakeRecorderMessages_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
