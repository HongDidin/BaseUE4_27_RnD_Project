// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertTakeRecorder/Private/ConcertTakeRecorderMessages.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertTakeRecorderMessages() {}
// Cross Module References
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent();
	UPackage* Z_Construct_UPackage__Script_ConcertTakeRecorder();
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent();
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FConcertTakeInitializedEvent();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertLocalIdentifierState();
	TAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecorderUserParameters();
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent();
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FTakeRecordSettings();
	CONCERTTAKERECORDER_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientRecordSetting();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionClientInfo();
	CONCERTTAKERECORDER_API UClass* Z_Construct_UClass_UConcertTakeSynchronization_NoRegister();
	CONCERTTAKERECORDER_API UClass* Z_Construct_UClass_UConcertTakeSynchronization();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	CONCERTTAKERECORDER_API UClass* Z_Construct_UClass_UConcertSessionRecordSettings_NoRegister();
	CONCERTTAKERECORDER_API UClass* Z_Construct_UClass_UConcertSessionRecordSettings();
// End Cross Module References
class UScriptStruct* FConcertRecordingCancelledEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("ConcertRecordingCancelledEvent"), sizeof(FConcertRecordingCancelledEvent), Get_Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FConcertRecordingCancelledEvent>()
{
	return FConcertRecordingCancelledEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertRecordingCancelledEvent(FConcertRecordingCancelledEvent::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("ConcertRecordingCancelledEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordingCancelledEvent
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordingCancelledEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertRecordingCancelledEvent>(FName(TEXT("ConcertRecordingCancelledEvent")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordingCancelledEvent;
	struct Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TakeName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertRecordingCancelledEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::NewProp_TakeName_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::NewProp_TakeName = { "TakeName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertRecordingCancelledEvent, TakeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::NewProp_TakeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::NewProp_TakeName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::NewProp_TakeName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"ConcertRecordingCancelledEvent",
		sizeof(FConcertRecordingCancelledEvent),
		alignof(FConcertRecordingCancelledEvent),
		Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertRecordingCancelledEvent"), sizeof(FConcertRecordingCancelledEvent), Get_Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertRecordingCancelledEvent_Hash() { return 145387670U; }
class UScriptStruct* FConcertRecordingFinishedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("ConcertRecordingFinishedEvent"), sizeof(FConcertRecordingFinishedEvent), Get_Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FConcertRecordingFinishedEvent>()
{
	return FConcertRecordingFinishedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertRecordingFinishedEvent(FConcertRecordingFinishedEvent::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("ConcertRecordingFinishedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordingFinishedEvent
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordingFinishedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertRecordingFinishedEvent>(FName(TEXT("ConcertRecordingFinishedEvent")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordingFinishedEvent;
	struct Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TakeName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertRecordingFinishedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::NewProp_TakeName_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::NewProp_TakeName = { "TakeName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertRecordingFinishedEvent, TakeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::NewProp_TakeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::NewProp_TakeName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::NewProp_TakeName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"ConcertRecordingFinishedEvent",
		sizeof(FConcertRecordingFinishedEvent),
		alignof(FConcertRecordingFinishedEvent),
		Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertRecordingFinishedEvent"), sizeof(FConcertRecordingFinishedEvent), Get_Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertRecordingFinishedEvent_Hash() { return 2331690809U; }
class UScriptStruct* FConcertTakeInitializedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("ConcertTakeInitializedEvent"), sizeof(FConcertTakeInitializedEvent), Get_Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FConcertTakeInitializedEvent>()
{
	return FConcertTakeInitializedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertTakeInitializedEvent(FConcertTakeInitializedEvent::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("ConcertTakeInitializedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertTakeInitializedEvent
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertTakeInitializedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertTakeInitializedEvent>(FName(TEXT("ConcertTakeInitializedEvent")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertTakeInitializedEvent;
	struct Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakePresetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TakePresetPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TakeName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TakeData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TakeData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TakeData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IdentifierState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IdentifierState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertTakeInitializedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakePresetPath_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakePresetPath = { "TakePresetPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTakeInitializedEvent, TakePresetPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakePresetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakePresetPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeName_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeName = { "TakeName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTakeInitializedEvent, TakeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData_Inner = { "TakeData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData = { "TakeData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTakeInitializedEvent, TakeData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_IdentifierState_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_IdentifierState = { "IdentifierState", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTakeInitializedEvent, IdentifierState), Z_Construct_UScriptStruct_FConcertLocalIdentifierState, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_IdentifierState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_IdentifierState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTakeInitializedEvent, Settings), Z_Construct_UScriptStruct_FTakeRecorderUserParameters, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakePresetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_TakeData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_IdentifierState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::NewProp_Settings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"ConcertTakeInitializedEvent",
		sizeof(FConcertTakeInitializedEvent),
		alignof(FConcertTakeInitializedEvent),
		Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertTakeInitializedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertTakeInitializedEvent"), sizeof(FConcertTakeInitializedEvent), Get_Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertTakeInitializedEvent_Hash() { return 4101651672U; }
class UScriptStruct* FConcertMultiUserSyncChangeEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("ConcertMultiUserSyncChangeEvent"), sizeof(FConcertMultiUserSyncChangeEvent), Get_Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FConcertMultiUserSyncChangeEvent>()
{
	return FConcertMultiUserSyncChangeEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertMultiUserSyncChangeEvent(FConcertMultiUserSyncChangeEvent::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("ConcertMultiUserSyncChangeEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertMultiUserSyncChangeEvent
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertMultiUserSyncChangeEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertMultiUserSyncChangeEvent>(FName(TEXT("ConcertMultiUserSyncChangeEvent")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertMultiUserSyncChangeEvent;
	struct Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSyncTakeRecordingTransactions_MetaData[];
#endif
		static void NewProp_bSyncTakeRecordingTransactions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSyncTakeRecordingTransactions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertMultiUserSyncChangeEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_EndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_EndpointId = { "EndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertMultiUserSyncChangeEvent, EndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_EndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_EndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions_SetBit(void* Obj)
	{
		((FConcertMultiUserSyncChangeEvent*)Obj)->bSyncTakeRecordingTransactions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions = { "bSyncTakeRecordingTransactions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertMultiUserSyncChangeEvent), &Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_EndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::NewProp_bSyncTakeRecordingTransactions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"ConcertMultiUserSyncChangeEvent",
		sizeof(FConcertMultiUserSyncChangeEvent),
		alignof(FConcertMultiUserSyncChangeEvent),
		Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertMultiUserSyncChangeEvent"), sizeof(FConcertMultiUserSyncChangeEvent), Get_Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertMultiUserSyncChangeEvent_Hash() { return 665571090U; }
class UScriptStruct* FConcertRecordSettingsChangeEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("ConcertRecordSettingsChangeEvent"), sizeof(FConcertRecordSettingsChangeEvent), Get_Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FConcertRecordSettingsChangeEvent>()
{
	return FConcertRecordSettingsChangeEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertRecordSettingsChangeEvent(FConcertRecordSettingsChangeEvent::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("ConcertRecordSettingsChangeEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordSettingsChangeEvent
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordSettingsChangeEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertRecordSettingsChangeEvent>(FName(TEXT("ConcertRecordSettingsChangeEvent")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertRecordSettingsChangeEvent;
	struct Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertRecordSettingsChangeEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_EndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_EndpointId = { "EndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertRecordSettingsChangeEvent, EndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_EndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_EndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertRecordSettingsChangeEvent, Settings), Z_Construct_UScriptStruct_FTakeRecordSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_EndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::NewProp_Settings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"ConcertRecordSettingsChangeEvent",
		sizeof(FConcertRecordSettingsChangeEvent),
		alignof(FConcertRecordSettingsChangeEvent),
		Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertRecordSettingsChangeEvent"), sizeof(FConcertRecordSettingsChangeEvent), Get_Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertRecordSettingsChangeEvent_Hash() { return 1439517996U; }
class UScriptStruct* FConcertClientRecordSetting::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientRecordSetting_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientRecordSetting, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("ConcertClientRecordSetting"), sizeof(FConcertClientRecordSetting), Get_Z_Construct_UScriptStruct_FConcertClientRecordSetting_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FConcertClientRecordSetting>()
{
	return FConcertClientRecordSetting::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientRecordSetting(FConcertClientRecordSetting::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("ConcertClientRecordSetting"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertClientRecordSetting
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertClientRecordSetting()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientRecordSetting>(FName(TEXT("ConcertClientRecordSetting")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFConcertClientRecordSetting;
	struct Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Details_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Details;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTakeSyncEnabled_MetaData[];
#endif
		static void NewProp_bTakeSyncEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTakeSyncEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientRecordSetting>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Details_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Details = { "Details", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientRecordSetting, Details), Z_Construct_UScriptStruct_FConcertSessionClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Details_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Details_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled_SetBit(void* Obj)
	{
		((FConcertClientRecordSetting*)Obj)->bTakeSyncEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled = { "bTakeSyncEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertClientRecordSetting), &Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientRecordSetting, Settings), Z_Construct_UScriptStruct_FTakeRecordSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Details,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_bTakeSyncEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::NewProp_Settings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"ConcertClientRecordSetting",
		sizeof(FConcertClientRecordSetting),
		alignof(FConcertClientRecordSetting),
		Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientRecordSetting()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientRecordSetting_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientRecordSetting"), sizeof(FConcertClientRecordSetting), Get_Z_Construct_UScriptStruct_FConcertClientRecordSetting_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientRecordSetting_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientRecordSetting_Hash() { return 327788905U; }
class UScriptStruct* FTakeRecordSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTAKERECORDER_API uint32 Get_Z_Construct_UScriptStruct_FTakeRecordSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeRecordSettings, Z_Construct_UPackage__Script_ConcertTakeRecorder(), TEXT("TakeRecordSettings"), sizeof(FTakeRecordSettings), Get_Z_Construct_UScriptStruct_FTakeRecordSettings_Hash());
	}
	return Singleton;
}
template<> CONCERTTAKERECORDER_API UScriptStruct* StaticStruct<FTakeRecordSettings>()
{
	return FTakeRecordSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeRecordSettings(FTakeRecordSettings::StaticStruct, TEXT("/Script/ConcertTakeRecorder"), TEXT("TakeRecordSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFTakeRecordSettings
{
	FScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFTakeRecordSettings()
	{
		UScriptStruct::DeferCppStructOps<FTakeRecordSettings>(FName(TEXT("TakeRecordSettings")));
	}
} ScriptStruct_ConcertTakeRecorder_StaticRegisterNativesFTakeRecordSettings;
	struct Z_Construct_UScriptStruct_FTakeRecordSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordOnClient_MetaData[];
#endif
		static void NewProp_bRecordOnClient_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordOnClient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTransactSources_MetaData[];
#endif
		static void NewProp_bTransactSources_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTransactSources;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTakeRecordSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient_MetaData[] = {
		{ "Category", "Multi-user Client Record Settings" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient_SetBit(void* Obj)
	{
		((FTakeRecordSettings*)Obj)->bRecordOnClient = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient = { "bRecordOnClient", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecordSettings), &Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources_MetaData[] = {
		{ "Category", "Multi-user Client Record Settings" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources_SetBit(void* Obj)
	{
		((FTakeRecordSettings*)Obj)->bTransactSources = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources = { "bTransactSources", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTakeRecordSettings), &Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bRecordOnClient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::NewProp_bTransactSources,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
		nullptr,
		&NewStructOps,
		"TakeRecordSettings",
		sizeof(FTakeRecordSettings),
		alignof(FTakeRecordSettings),
		Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTakeRecordSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeRecordSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTakeRecorder();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeRecordSettings"), sizeof(FTakeRecordSettings), Get_Z_Construct_UScriptStruct_FTakeRecordSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTakeRecordSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeRecordSettings_Hash() { return 1731144010U; }
	void UConcertTakeSynchronization::StaticRegisterNativesUConcertTakeSynchronization()
	{
	}
	UClass* Z_Construct_UClass_UConcertTakeSynchronization_NoRegister()
	{
		return UConcertTakeSynchronization::StaticClass();
	}
	struct Z_Construct_UClass_UConcertTakeSynchronization_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSyncTakeRecordingTransactions_MetaData[];
#endif
		static void NewProp_bSyncTakeRecordingTransactions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSyncTakeRecordingTransactions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertTakeSynchronization_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertTakeSynchronization_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Multi-user Take Synchronization" },
		{ "IncludePath", "ConcertTakeRecorderMessages.h" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions_MetaData[] = {
		{ "Category", "Multi-user Take Synchronization" },
		{ "DisplayName", "Synchronize Take Recorder Transactions" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	void Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions_SetBit(void* Obj)
	{
		((UConcertTakeSynchronization*)Obj)->bSyncTakeRecordingTransactions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions = { "bSyncTakeRecordingTransactions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertTakeSynchronization), &Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertTakeSynchronization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertTakeSynchronization_Statics::NewProp_bSyncTakeRecordingTransactions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertTakeSynchronization_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertTakeSynchronization>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertTakeSynchronization_Statics::ClassParams = {
		&UConcertTakeSynchronization::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertTakeSynchronization_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertTakeSynchronization_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertTakeSynchronization_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertTakeSynchronization_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertTakeSynchronization()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertTakeSynchronization_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertTakeSynchronization, 2264110997);
	template<> CONCERTTAKERECORDER_API UClass* StaticClass<UConcertTakeSynchronization>()
	{
		return UConcertTakeSynchronization::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertTakeSynchronization(Z_Construct_UClass_UConcertTakeSynchronization, &UConcertTakeSynchronization::StaticClass, TEXT("/Script/ConcertTakeRecorder"), TEXT("UConcertTakeSynchronization"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertTakeSynchronization);
	void UConcertSessionRecordSettings::StaticRegisterNativesUConcertSessionRecordSettings()
	{
	}
	UClass* Z_Construct_UClass_UConcertSessionRecordSettings_NoRegister()
	{
		return UConcertSessionRecordSettings::StaticClass();
	}
	struct Z_Construct_UClass_UConcertSessionRecordSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemoteSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RemoteSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertSessionRecordSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTakeRecorder,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionRecordSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Multi-user Client Record Settings" },
		{ "IncludePath", "ConcertTakeRecorderMessages.h" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_LocalSettings_MetaData[] = {
		{ "Category", "Multi-user Client Record Settings" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_LocalSettings = { "LocalSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSessionRecordSettings, LocalSettings), Z_Construct_UScriptStruct_FTakeRecordSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_LocalSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_LocalSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings_Inner = { "RemoteSettings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertClientRecordSetting, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings_MetaData[] = {
		{ "Category", "Multi-user Client Record Settings" },
		{ "ModuleRelativePath", "Private/ConcertTakeRecorderMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings = { "RemoteSettings", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSessionRecordSettings, RemoteSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertSessionRecordSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_LocalSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSessionRecordSettings_Statics::NewProp_RemoteSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertSessionRecordSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertSessionRecordSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertSessionRecordSettings_Statics::ClassParams = {
		&UConcertSessionRecordSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertSessionRecordSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionRecordSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertSessionRecordSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertSessionRecordSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertSessionRecordSettings, 1204823142);
	template<> CONCERTTAKERECORDER_API UClass* StaticClass<UConcertSessionRecordSettings>()
	{
		return UConcertSessionRecordSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertSessionRecordSettings(Z_Construct_UClass_UConcertSessionRecordSettings, &UConcertSessionRecordSettings::StaticClass, TEXT("/Script/ConcertTakeRecorder"), TEXT("UConcertSessionRecordSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertSessionRecordSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
