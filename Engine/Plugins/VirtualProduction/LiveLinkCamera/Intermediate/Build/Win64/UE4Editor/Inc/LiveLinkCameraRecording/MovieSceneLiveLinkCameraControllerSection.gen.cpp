// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkCameraRecording/Private/MovieSceneLiveLinkCameraControllerSection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneLiveLinkCameraControllerSection() {}
// Cross Module References
	LIVELINKCAMERARECORDING_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_NoRegister();
	LIVELINKCAMERARECORDING_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneHookSection();
	UPackage* Z_Construct_UPackage__Script_LiveLinkCameraRecording();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensFile_NoRegister();
// End Cross Module References
	void UMovieSceneLiveLinkCameraControllerSection::StaticRegisterNativesUMovieSceneLiveLinkCameraControllerSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_NoRegister()
	{
		return UMovieSceneLiveLinkCameraControllerSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedLensFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedLensFile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneHookSection,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCameraRecording,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Movie Scene section for LiveLink Camera Controller properties */" },
		{ "IncludePath", "MovieSceneLiveLinkCameraControllerSection.h" },
		{ "ModuleRelativePath", "Private/MovieSceneLiveLinkCameraControllerSection.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Movie Scene section for LiveLink Camera Controller properties" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::NewProp_CachedLensFile_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** Saved duplicate of the LensFile asset used by the recorded LiveLink Camera Controller at the time of recording */" },
		{ "ModuleRelativePath", "Private/MovieSceneLiveLinkCameraControllerSection.h" },
		{ "ToolTip", "Saved duplicate of the LensFile asset used by the recorded LiveLink Camera Controller at the time of recording" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::NewProp_CachedLensFile = { "CachedLensFile", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneLiveLinkCameraControllerSection, CachedLensFile), Z_Construct_UClass_ULensFile_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::NewProp_CachedLensFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::NewProp_CachedLensFile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::NewProp_CachedLensFile,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneLiveLinkCameraControllerSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::ClassParams = {
		&UMovieSceneLiveLinkCameraControllerSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::PropPointers),
		0,
		0x003000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneLiveLinkCameraControllerSection, 1912101206);
	template<> LIVELINKCAMERARECORDING_API UClass* StaticClass<UMovieSceneLiveLinkCameraControllerSection>()
	{
		return UMovieSceneLiveLinkCameraControllerSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneLiveLinkCameraControllerSection(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerSection, &UMovieSceneLiveLinkCameraControllerSection::StaticClass, TEXT("/Script/LiveLinkCameraRecording"), TEXT("UMovieSceneLiveLinkCameraControllerSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneLiveLinkCameraControllerSection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
