// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkCameraRecording/Private/MovieSceneLiveLinkCameraControllerTrackRecorder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneLiveLinkCameraControllerTrackRecorder() {}
// Cross Module References
	LIVELINKCAMERARECORDING_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_NoRegister();
	LIVELINKCAMERARECORDING_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder();
	LIVELINKSEQUENCER_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder();
	UPackage* Z_Construct_UPackage__Script_LiveLinkCameraRecording();
// End Cross Module References
	void UMovieSceneLiveLinkCameraControllerTrackRecorder::StaticRegisterNativesUMovieSceneLiveLinkCameraControllerTrackRecorder()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_NoRegister()
	{
		return UMovieSceneLiveLinkCameraControllerTrackRecorder::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneLiveLinkControllerTrackRecorder,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCameraRecording,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Movie Scene track recorder for LiveLink Camera Controller properties */" },
		{ "IncludePath", "MovieSceneLiveLinkCameraControllerTrackRecorder.h" },
		{ "ModuleRelativePath", "Private/MovieSceneLiveLinkCameraControllerTrackRecorder.h" },
		{ "ToolTip", "Movie Scene track recorder for LiveLink Camera Controller properties" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneLiveLinkCameraControllerTrackRecorder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::ClassParams = {
		&UMovieSceneLiveLinkCameraControllerTrackRecorder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneLiveLinkCameraControllerTrackRecorder, 935127783);
	template<> LIVELINKCAMERARECORDING_API UClass* StaticClass<UMovieSceneLiveLinkCameraControllerTrackRecorder>()
	{
		return UMovieSceneLiveLinkCameraControllerTrackRecorder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrackRecorder, &UMovieSceneLiveLinkCameraControllerTrackRecorder::StaticClass, TEXT("/Script/LiveLinkCameraRecording"), TEXT("UMovieSceneLiveLinkCameraControllerTrackRecorder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneLiveLinkCameraControllerTrackRecorder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
