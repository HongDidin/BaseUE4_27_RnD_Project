// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkCameraRecording/Private/MovieSceneLiveLinkCameraControllerTrack.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneLiveLinkCameraControllerTrack() {}
// Cross Module References
	LIVELINKCAMERARECORDING_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_NoRegister();
	LIVELINKCAMERARECORDING_API UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneNameableTrack();
	UPackage* Z_Construct_UPackage__Script_LiveLinkCameraRecording();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection_NoRegister();
// End Cross Module References
	void UMovieSceneLiveLinkCameraControllerTrack::StaticRegisterNativesUMovieSceneLiveLinkCameraControllerTrack()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_NoRegister()
	{
		return UMovieSceneLiveLinkCameraControllerTrack::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sections;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneNameableTrack,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCameraRecording,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Movie Scene track for LiveLink Camera Controller properties */" },
		{ "IncludePath", "MovieSceneLiveLinkCameraControllerTrack.h" },
		{ "ModuleRelativePath", "Private/MovieSceneLiveLinkCameraControllerTrack.h" },
		{ "ToolTip", "Movie Scene track for LiveLink Camera Controller properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections_Inner = { "Sections", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections_MetaData[] = {
		{ "Comment", "/** Array of movie scene sections managed by this track */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/MovieSceneLiveLinkCameraControllerTrack.h" },
		{ "ToolTip", "Array of movie scene sections managed by this track" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections = { "Sections", nullptr, (EPropertyFlags)0x0020088000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneLiveLinkCameraControllerTrack, Sections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::NewProp_Sections,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneLiveLinkCameraControllerTrack>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::ClassParams = {
		&UMovieSceneLiveLinkCameraControllerTrack::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneLiveLinkCameraControllerTrack, 4201677357);
	template<> LIVELINKCAMERARECORDING_API UClass* StaticClass<UMovieSceneLiveLinkCameraControllerTrack>()
	{
		return UMovieSceneLiveLinkCameraControllerTrack::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneLiveLinkCameraControllerTrack(Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack, &UMovieSceneLiveLinkCameraControllerTrack::StaticClass, TEXT("/Script/LiveLinkCameraRecording"), TEXT("UMovieSceneLiveLinkCameraControllerTrack"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneLiveLinkCameraControllerTrack);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
