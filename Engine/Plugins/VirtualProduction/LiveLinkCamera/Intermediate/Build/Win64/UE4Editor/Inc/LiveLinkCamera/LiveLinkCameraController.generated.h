// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKCAMERA_LiveLinkCameraController_generated_h
#error "LiveLinkCameraController.generated.h already included, missing '#pragma once' in LiveLinkCameraController.h"
#endif
#define LIVELINKCAMERA_LiveLinkCameraController_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics; \
	LIVELINKCAMERA_API static class UScriptStruct* StaticStruct();


template<> LIVELINKCAMERA_API UScriptStruct* StaticStruct<struct FLiveLinkCameraControllerUpdateFlags>();

#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkCameraController(); \
	friend struct Z_Construct_UClass_ULiveLinkCameraController_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkCameraController, ULiveLinkControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkCamera"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkCameraController)


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkCameraController(); \
	friend struct Z_Construct_UClass_ULiveLinkCameraController_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkCameraController, ULiveLinkControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkCamera"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkCameraController)


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkCameraController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkCameraController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkCameraController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkCameraController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkCameraController(ULiveLinkCameraController&&); \
	NO_API ULiveLinkCameraController(const ULiveLinkCameraController&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkCameraController(ULiveLinkCameraController&&); \
	NO_API ULiveLinkCameraController(const ULiveLinkCameraController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkCameraController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkCameraController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULiveLinkCameraController)


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LensDistortionHandler() { return STRUCT_OFFSET(ULiveLinkCameraController, LensDistortionHandler); } \
	FORCEINLINE static uint32 __PPO__DistortionProducerID() { return STRUCT_OFFSET(ULiveLinkCameraController, DistortionProducerID); } \
	FORCEINLINE static uint32 __PPO__OriginalCameraRotation() { return STRUCT_OFFSET(ULiveLinkCameraController, OriginalCameraRotation); } \
	FORCEINLINE static uint32 __PPO__OriginalCameraLocation() { return STRUCT_OFFSET(ULiveLinkCameraController, OriginalCameraLocation); } \
	FORCEINLINE static uint32 __PPO__UpdateFlags() { return STRUCT_OFFSET(ULiveLinkCameraController, UpdateFlags); }


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_56_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKCAMERA_API UClass* StaticClass<class ULiveLinkCameraController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCamera_Public_LiveLinkCameraController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
