// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkCamera/Public/LiveLinkCameraController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkCameraController() {}
// Cross Module References
	LIVELINKCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags();
	UPackage* Z_Construct_UPackage__Script_LiveLinkCamera();
	LIVELINKCAMERA_API UClass* Z_Construct_UClass_ULiveLinkCameraController_NoRegister();
	LIVELINKCAMERA_API UClass* Z_Construct_UClass_ULiveLinkCameraController();
	LIVELINKCOMPONENTS_API UClass* Z_Construct_UClass_ULiveLinkControllerBase();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FComponentReference();
	LIVELINKCOMPONENTS_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkTransformControllerData();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensFilePicker();
	CINEMATICCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FCameraFilmbackSettings();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
class UScriptStruct* FLiveLinkCameraControllerUpdateFlags::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags, Z_Construct_UPackage__Script_LiveLinkCamera(), TEXT("LiveLinkCameraControllerUpdateFlags"), sizeof(FLiveLinkCameraControllerUpdateFlags), Get_Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Hash());
	}
	return Singleton;
}
template<> LIVELINKCAMERA_API UScriptStruct* StaticStruct<FLiveLinkCameraControllerUpdateFlags>()
{
	return FLiveLinkCameraControllerUpdateFlags::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags(FLiveLinkCameraControllerUpdateFlags::StaticStruct, TEXT("/Script/LiveLinkCamera"), TEXT("LiveLinkCameraControllerUpdateFlags"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkCamera_StaticRegisterNativesFLiveLinkCameraControllerUpdateFlags
{
	FScriptStruct_LiveLinkCamera_StaticRegisterNativesFLiveLinkCameraControllerUpdateFlags()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkCameraControllerUpdateFlags>(FName(TEXT("LiveLinkCameraControllerUpdateFlags")));
	}
} ScriptStruct_LiveLinkCamera_StaticRegisterNativesFLiveLinkCameraControllerUpdateFlags;
	struct Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyFieldOfView_MetaData[];
#endif
		static void NewProp_bApplyFieldOfView_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyFieldOfView;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyAspectRatio_MetaData[];
#endif
		static void NewProp_bApplyAspectRatio_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyAspectRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyFocalLength_MetaData[];
#endif
		static void NewProp_bApplyFocalLength_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyFocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyProjectionMode_MetaData[];
#endif
		static void NewProp_bApplyProjectionMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyProjectionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyFilmBack_MetaData[];
#endif
		static void NewProp_bApplyFilmBack_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyFilmBack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyAperture_MetaData[];
#endif
		static void NewProp_bApplyAperture_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyAperture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyFocusDistance_MetaData[];
#endif
		static void NewProp_bApplyFocusDistance_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyFocusDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Flags to control whether incoming values from LiveLink Camera FrameData should be applied or not */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Flags to control whether incoming values from LiveLink Camera FrameData should be applied or not" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkCameraControllerUpdateFlags>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply FOV if it's available in LiveLink FrameData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply FOV if it's available in LiveLink FrameData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyFieldOfView = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView = { "bApplyFieldOfView", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply Aspect Ratio if it's available in LiveLink FrameData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply Aspect Ratio if it's available in LiveLink FrameData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyAspectRatio = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio = { "bApplyAspectRatio", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply Focal Length if it's available in LiveLink FrameData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply Focal Length if it's available in LiveLink FrameData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyFocalLength = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength = { "bApplyFocalLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply Projection Mode if it's available in LiveLink FrameData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply Projection Mode if it's available in LiveLink FrameData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyProjectionMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode = { "bApplyProjectionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply Filmback if it's available in LiveLink StaticData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply Filmback if it's available in LiveLink StaticData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyFilmBack = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack = { "bApplyFilmBack", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply Aperture if it's available in LiveLink FrameData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply Aperture if it's available in LiveLink FrameData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyAperture = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture = { "bApplyAperture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance_MetaData[] = {
		{ "Category", "Updates" },
		{ "Comment", "/** Whether to apply Focus Distance if it's available in LiveLink FrameData */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to apply Focus Distance if it's available in LiveLink FrameData" },
	};
#endif
	void Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance_SetBit(void* Obj)
	{
		((FLiveLinkCameraControllerUpdateFlags*)Obj)->bApplyFocusDistance = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance = { "bApplyFocusDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FLiveLinkCameraControllerUpdateFlags), &Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFieldOfView,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAspectRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyProjectionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFilmBack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyAperture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::NewProp_bApplyFocusDistance,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCamera,
		nullptr,
		&NewStructOps,
		"LiveLinkCameraControllerUpdateFlags",
		sizeof(FLiveLinkCameraControllerUpdateFlags),
		alignof(FLiveLinkCameraControllerUpdateFlags),
		Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkCameraControllerUpdateFlags"), sizeof(FLiveLinkCameraControllerUpdateFlags), Get_Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags_Hash() { return 875946642U; }
	void ULiveLinkCameraController::StaticRegisterNativesULiveLinkCameraController()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkCameraController_NoRegister()
	{
		return ULiveLinkCameraController::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkCameraController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentToControl_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ComponentToControl;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransformData;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCameraRange_MetaData[];
#endif
		static void NewProp_bUseCameraRange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCameraRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensFilePicker_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensFilePicker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCroppedFilmback_MetaData[];
#endif
		static void NewProp_bUseCroppedFilmback_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCroppedFilmback;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CroppedFilmback_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CroppedFilmback;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyNodalOffset_MetaData[];
#endif
		static void NewProp_bApplyNodalOffset_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyNodalOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bScaleOverscan_MetaData[];
#endif
		static void NewProp_bScaleOverscan_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bScaleOverscan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensDistortionHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LensDistortionHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionProducerID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionProducerID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalCameraRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginalCameraRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalCameraLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginalCameraLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateFlags_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UpdateFlags;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldUpdateVisualComponentOnChange_MetaData[];
#endif
		static void NewProp_bShouldUpdateVisualComponentOnChange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldUpdateVisualComponentOnChange;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkCameraController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkControllerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkCamera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n */" },
		{ "IncludePath", "LiveLinkCameraController.h" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_ComponentToControl_MetaData[] = {
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_ComponentToControl = { "ComponentToControl", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, ComponentToControl_DEPRECATED), Z_Construct_UScriptStruct_FComponentReference, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_ComponentToControl_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_ComponentToControl_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_TransformData_MetaData[] = {
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_TransformData = { "TransformData", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, TransformData_DEPRECATED), Z_Construct_UScriptStruct_FLiveLinkTransformControllerData, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_TransformData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_TransformData_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/**\n\x09 * Should LiveLink inputs be remapped (i.e normalized to physical units) using camera component range\n\x09 */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Should LiveLink inputs be remapped (i.e normalized to physical units) using camera component range" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange_SetBit(void* Obj)
	{
		((ULiveLinkCameraController*)Obj)->bUseCameraRange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange = { "bUseCameraRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkCameraController), &Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensFilePicker_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** Asset containing encoder and fiz mapping */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Asset containing encoder and fiz mapping" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensFilePicker = { "LensFilePicker", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, LensFilePicker), Z_Construct_UScriptStruct_FLensFilePicker, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensFilePicker_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensFilePicker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** Whether to use the cropped filmback setting to drive the filmback of the attached camera component */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to use the cropped filmback setting to drive the filmback of the attached camera component" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback_SetBit(void* Obj)
	{
		((ULiveLinkCameraController*)Obj)->bUseCroppedFilmback = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback = { "bUseCroppedFilmback", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkCameraController), &Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_CroppedFilmback_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** \n\x09 * If a LensFile is being evaluated, the filmback saved in that LensFile will drive the attached camera component to ensure correct calibration.\n\x09 * If bUseCroppedFilmback is true, this value will be applied to the camera component and used to evaluate the LensFile instead.\n\x09 */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "If a LensFile is being evaluated, the filmback saved in that LensFile will drive the attached camera component to ensure correct calibration.\nIf bUseCroppedFilmback is true, this value will be applied to the camera component and used to evaluate the LensFile instead." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_CroppedFilmback = { "CroppedFilmback", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, CroppedFilmback), Z_Construct_UScriptStruct_FCameraFilmbackSettings, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_CroppedFilmback_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_CroppedFilmback_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** Apply nodal offset from lens file if enabled */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Apply nodal offset from lens file if enabled" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset_SetBit(void* Obj)
	{
		((ULiveLinkCameraController*)Obj)->bApplyNodalOffset = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset = { "bApplyNodalOffset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkCameraController), &Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "Comment", "/** Whether to scale the computed overscan by the overscan percentage */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to scale the computed overscan by the overscan percentage" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan_SetBit(void* Obj)
	{
		((ULiveLinkCameraController*)Obj)->bScaleOverscan = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan = { "bScaleOverscan", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkCameraController), &Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OverscanMultiplier_MetaData[] = {
		{ "Category", "Camera Calibration" },
		{ "ClampMax", "2.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** The percentage of the computed overscan that should be applied to the target camera */" },
		{ "EditCondition", "bScaleOverscan" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "The percentage of the computed overscan that should be applied to the target camera" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OverscanMultiplier = { "OverscanMultiplier", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, OverscanMultiplier), METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OverscanMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OverscanMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensDistortionHandler_MetaData[] = {
		{ "Comment", "/** Cached distortion handler associated with attached camera component */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Cached distortion handler associated with attached camera component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensDistortionHandler = { "LensDistortionHandler", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, LensDistortionHandler), Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensDistortionHandler_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensDistortionHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_DistortionProducerID_MetaData[] = {
		{ "Comment", "/** Unique identifier representing the source of distortion data */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Unique identifier representing the source of distortion data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_DistortionProducerID = { "DistortionProducerID", nullptr, (EPropertyFlags)0x0020080000200000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, DistortionProducerID), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_DistortionProducerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_DistortionProducerID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraRotation_MetaData[] = {
		{ "Comment", "/** Original cinecamera component rotation that we set back on when nodal offset isn't applied anymore */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Original cinecamera component rotation that we set back on when nodal offset isn't applied anymore" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraRotation = { "OriginalCameraRotation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, OriginalCameraRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraLocation_MetaData[] = {
		{ "Comment", "/** Original cinecamera component location that we set back on when nodal offset isn't applied anymore */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Original cinecamera component location that we set back on when nodal offset isn't applied anymore" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraLocation = { "OriginalCameraLocation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, OriginalCameraLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_UpdateFlags_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Used to control which data from LiveLink is actually applied to camera */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Used to control which data from LiveLink is actually applied to camera" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_UpdateFlags = { "UpdateFlags", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkCameraController, UpdateFlags), Z_Construct_UScriptStruct_FLiveLinkCameraControllerUpdateFlags, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_UpdateFlags_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_UpdateFlags_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange_MetaData[] = {
		{ "Category", "Debug" },
		{ "Comment", "/** Whether to refresh frustum drawing on value change */" },
		{ "ModuleRelativePath", "Public/LiveLinkCameraController.h" },
		{ "ToolTip", "Whether to refresh frustum drawing on value change" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange_SetBit(void* Obj)
	{
		((ULiveLinkCameraController*)Obj)->bShouldUpdateVisualComponentOnChange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange = { "bShouldUpdateVisualComponentOnChange", nullptr, (EPropertyFlags)0x0020080800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkCameraController), &Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkCameraController_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_ComponentToControl,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_TransformData,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCameraRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensFilePicker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bUseCroppedFilmback,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_CroppedFilmback,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bApplyNodalOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bScaleOverscan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OverscanMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_LensDistortionHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_DistortionProducerID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_OriginalCameraLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_UpdateFlags,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkCameraController_Statics::NewProp_bShouldUpdateVisualComponentOnChange,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkCameraController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkCameraController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkCameraController_Statics::ClassParams = {
		&ULiveLinkCameraController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkCameraController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkCameraController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkCameraController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkCameraController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkCameraController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkCameraController, 2981278486);
	template<> LIVELINKCAMERA_API UClass* StaticClass<ULiveLinkCameraController>()
	{
		return ULiveLinkCameraController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkCameraController(Z_Construct_UClass_ULiveLinkCameraController, &ULiveLinkCameraController::StaticClass, TEXT("/Script/LiveLinkCamera"), TEXT("ULiveLinkCameraController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkCameraController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
