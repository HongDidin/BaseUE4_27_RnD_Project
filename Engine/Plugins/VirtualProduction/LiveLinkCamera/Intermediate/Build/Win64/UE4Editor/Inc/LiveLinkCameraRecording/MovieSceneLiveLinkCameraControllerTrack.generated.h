// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKCAMERARECORDING_MovieSceneLiveLinkCameraControllerTrack_generated_h
#error "MovieSceneLiveLinkCameraControllerTrack.generated.h already included, missing '#pragma once' in MovieSceneLiveLinkCameraControllerTrack.h"
#endif
#define LIVELINKCAMERARECORDING_MovieSceneLiveLinkCameraControllerTrack_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkCameraControllerTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkCameraControllerTrack, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkCameraRecording"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkCameraControllerTrack)


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneLiveLinkCameraControllerTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneLiveLinkCameraControllerTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneLiveLinkCameraControllerTrack, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkCameraRecording"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneLiveLinkCameraControllerTrack)


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkCameraControllerTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkCameraControllerTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkCameraControllerTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkCameraControllerTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkCameraControllerTrack(UMovieSceneLiveLinkCameraControllerTrack&&); \
	NO_API UMovieSceneLiveLinkCameraControllerTrack(const UMovieSceneLiveLinkCameraControllerTrack&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneLiveLinkCameraControllerTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneLiveLinkCameraControllerTrack(UMovieSceneLiveLinkCameraControllerTrack&&); \
	NO_API UMovieSceneLiveLinkCameraControllerTrack(const UMovieSceneLiveLinkCameraControllerTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneLiveLinkCameraControllerTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneLiveLinkCameraControllerTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneLiveLinkCameraControllerTrack)


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sections() { return STRUCT_OFFSET(UMovieSceneLiveLinkCameraControllerTrack, Sections); }


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKCAMERARECORDING_API UClass* StaticClass<class UMovieSceneLiveLinkCameraControllerTrack>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkCamera_Source_LiveLinkCameraRecording_Private_MovieSceneLiveLinkCameraControllerTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
