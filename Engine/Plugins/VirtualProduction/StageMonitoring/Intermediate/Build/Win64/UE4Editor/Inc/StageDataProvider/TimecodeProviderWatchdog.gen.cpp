// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageDataProvider/Private/TimecodeProviderWatchdog.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimecodeProviderWatchdog() {}
// Cross Module References
	STAGEDATAPROVIDER_API UScriptStruct* Z_Construct_UScriptStruct_FTimecodeProviderStateEvent();
	UPackage* Z_Construct_UPackage__Script_StageDataProvider();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageProviderEventMessage();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ETimecodeProviderSynchronizationState();
// End Cross Module References

static_assert(std::is_polymorphic<FTimecodeProviderStateEvent>() == std::is_polymorphic<FStageProviderEventMessage>(), "USTRUCT FTimecodeProviderStateEvent cannot be polymorphic unless super FStageProviderEventMessage is polymorphic");

class UScriptStruct* FTimecodeProviderStateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEDATAPROVIDER_API uint32 Get_Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent, Z_Construct_UPackage__Script_StageDataProvider(), TEXT("TimecodeProviderStateEvent"), sizeof(FTimecodeProviderStateEvent), Get_Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Hash());
	}
	return Singleton;
}
template<> STAGEDATAPROVIDER_API UScriptStruct* StaticStruct<FTimecodeProviderStateEvent>()
{
	return FTimecodeProviderStateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimecodeProviderStateEvent(FTimecodeProviderStateEvent::StaticStruct, TEXT("/Script/StageDataProvider"), TEXT("TimecodeProviderStateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_StageDataProvider_StaticRegisterNativesFTimecodeProviderStateEvent
{
	FScriptStruct_StageDataProvider_StaticRegisterNativesFTimecodeProviderStateEvent()
	{
		UScriptStruct::DeferCppStructOps<FTimecodeProviderStateEvent>(FName(TEXT("TimecodeProviderStateEvent")));
	}
} ScriptStruct_StageDataProvider_StaticRegisterNativesFTimecodeProviderStateEvent;
	struct Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProviderName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProviderType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrameRate;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NewState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Stage event to notify of TimecodeProvider state change\n */" },
		{ "ModuleRelativePath", "Private/TimecodeProviderWatchdog.h" },
		{ "ToolTip", "Stage event to notify of TimecodeProvider state change" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimecodeProviderStateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderName_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/** Name of the TimeodeProvider for this event */" },
		{ "ModuleRelativePath", "Private/TimecodeProviderWatchdog.h" },
		{ "ToolTip", "Name of the TimeodeProvider for this event" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderName = { "ProviderName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimecodeProviderStateEvent, ProviderName), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderType_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/** Type of the TimecodeProvider for this event */" },
		{ "ModuleRelativePath", "Private/TimecodeProviderWatchdog.h" },
		{ "ToolTip", "Type of the TimecodeProvider for this event" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderType = { "ProviderType", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimecodeProviderStateEvent, ProviderType), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_FrameRate_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/** FrameRate of the provider */" },
		{ "ModuleRelativePath", "Private/TimecodeProviderWatchdog.h" },
		{ "ToolTip", "FrameRate of the provider" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_FrameRate = { "FrameRate", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimecodeProviderStateEvent, FrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_FrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_FrameRate_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState_MetaData[] = {
		{ "Category", "Timecode" },
		{ "Comment", "/** New state of TimecodeProvider (i.e. Synchronized, Error, etc...) */" },
		{ "ModuleRelativePath", "Private/TimecodeProviderWatchdog.h" },
		{ "ToolTip", "New state of TimecodeProvider (i.e. Synchronized, Error, etc...)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState = { "NewState", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimecodeProviderStateEvent, NewState), Z_Construct_UEnum_Engine_ETimecodeProviderSynchronizationState, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_ProviderType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_FrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::NewProp_NewState,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageDataProvider,
		Z_Construct_UScriptStruct_FStageProviderEventMessage,
		&NewStructOps,
		"TimecodeProviderStateEvent",
		sizeof(FTimecodeProviderStateEvent),
		alignof(FTimecodeProviderStateEvent),
		Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimecodeProviderStateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageDataProvider();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimecodeProviderStateEvent"), sizeof(FTimecodeProviderStateEvent), Get_Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimecodeProviderStateEvent_Hash() { return 3289582331U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
