// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STAGEMONITORCOMMON_StageMonitoringSettings_generated_h
#error "StageMonitoringSettings.generated.h already included, missing '#pragma once' in StageMonitoringSettings.h"
#endif
#define STAGEMONITORCOMMON_StageMonitoringSettings_generated_h

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_142_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStageMonitorSettings_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bAutoStart() { return STRUCT_OFFSET(FStageMonitorSettings, bAutoStart); }


template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<struct FStageMonitorSettings>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_112_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<struct FStageDataProviderSettings>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<struct FStageHitchDetectionSettings>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<struct FStageFramePerformanceSettings>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_49_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStageDataExportSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<struct FStageDataExportSettings>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics; \
	static class UScriptStruct* StaticStruct();


template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<struct FStageMessageTypeWrapper>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStageMonitoringSettings(); \
	friend struct Z_Construct_UClass_UStageMonitoringSettings_Statics; \
public: \
	DECLARE_CLASS(UStageMonitoringSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StageMonitorCommon"), NO_API) \
	DECLARE_SERIALIZER(UStageMonitoringSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_INCLASS \
private: \
	static void StaticRegisterNativesUStageMonitoringSettings(); \
	friend struct Z_Construct_UClass_UStageMonitoringSettings_Statics; \
public: \
	DECLARE_CLASS(UStageMonitoringSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StageMonitorCommon"), NO_API) \
	DECLARE_SERIALIZER(UStageMonitoringSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStageMonitoringSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStageMonitoringSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStageMonitoringSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStageMonitoringSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStageMonitoringSettings(UStageMonitoringSettings&&); \
	NO_API UStageMonitoringSettings(const UStageMonitoringSettings&); \
public:


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStageMonitoringSettings(UStageMonitoringSettings&&); \
	NO_API UStageMonitoringSettings(const UStageMonitoringSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStageMonitoringSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStageMonitoringSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UStageMonitoringSettings)


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StageSessionId() { return STRUCT_OFFSET(UStageMonitoringSettings, StageSessionId); }


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_182_PROLOG
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_INCLASS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h_185_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STAGEMONITORCOMMON_API UClass* StaticClass<class UStageMonitoringSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorCommon_Public_StageMonitoringSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
