// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageMonitor/Public/IStageMonitorSession.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIStageMonitorSession() {}
// Cross Module References
	STAGEMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FStageSessionProviderEntry();
	UPackage* Z_Construct_UPackage__Script_StageMonitor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageInstanceDescriptor();
// End Cross Module References
class UScriptStruct* FStageSessionProviderEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FStageSessionProviderEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageSessionProviderEntry, Z_Construct_UPackage__Script_StageMonitor(), TEXT("StageSessionProviderEntry"), sizeof(FStageSessionProviderEntry), Get_Z_Construct_UScriptStruct_FStageSessionProviderEntry_Hash());
	}
	return Singleton;
}
template<> STAGEMONITOR_API UScriptStruct* StaticStruct<FStageSessionProviderEntry>()
{
	return FStageSessionProviderEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageSessionProviderEntry(FStageSessionProviderEntry::StaticStruct, TEXT("/Script/StageMonitor"), TEXT("StageSessionProviderEntry"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitor_StaticRegisterNativesFStageSessionProviderEntry
{
	FScriptStruct_StageMonitor_StaticRegisterNativesFStageSessionProviderEntry()
	{
		UScriptStruct::DeferCppStructOps<FStageSessionProviderEntry>(FName(TEXT("StageSessionProviderEntry")));
	}
} ScriptStruct_StageMonitor_StaticRegisterNativesFStageSessionProviderEntry;
	struct Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Identifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Identifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Descriptor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Descriptor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Entry corresponding to a provider we are monitoring\n * Contains information related to the provider so we can communicate with it\n * and more dynamic information like last communication received\n */" },
		{ "ModuleRelativePath", "Public/IStageMonitorSession.h" },
		{ "ToolTip", "Entry corresponding to a provider we are monitoring\nContains information related to the provider so we can communicate with it\nand more dynamic information like last communication received" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageSessionProviderEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Identifier_MetaData[] = {
		{ "Comment", "/** Identifier of this provider */" },
		{ "ModuleRelativePath", "Public/IStageMonitorSession.h" },
		{ "ToolTip", "Identifier of this provider" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Identifier = { "Identifier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageSessionProviderEntry, Identifier), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Identifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Identifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Descriptor_MetaData[] = {
		{ "Comment", "/** Detailed descriptor */" },
		{ "ModuleRelativePath", "Public/IStageMonitorSession.h" },
		{ "ToolTip", "Detailed descriptor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Descriptor = { "Descriptor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageSessionProviderEntry, Descriptor), Z_Construct_UScriptStruct_FStageInstanceDescriptor, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Descriptor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Descriptor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Identifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::NewProp_Descriptor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitor,
		nullptr,
		&NewStructOps,
		"StageSessionProviderEntry",
		sizeof(FStageSessionProviderEntry),
		alignof(FStageSessionProviderEntry),
		Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageSessionProviderEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageSessionProviderEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageSessionProviderEntry"), sizeof(FStageSessionProviderEntry), Get_Z_Construct_UScriptStruct_FStageSessionProviderEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageSessionProviderEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageSessionProviderEntry_Hash() { return 550430242U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
