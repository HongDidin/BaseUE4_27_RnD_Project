// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageMonitorEditor/Private/StageMonitorEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStageMonitorEditorSettings() {}
// Cross Module References
	STAGEMONITOREDITOR_API UClass* Z_Construct_UClass_UStageMonitorEditorSettings_NoRegister();
	STAGEMONITOREDITOR_API UClass* Z_Construct_UClass_UStageMonitorEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_StageMonitorEditor();
// End Cross Module References
	void UStageMonitorEditorSettings::StaticRegisterNativesUStageMonitorEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UStageMonitorEditorSettings_NoRegister()
	{
		return UStageMonitorEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UStageMonitorEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RefreshRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RefreshRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UStageMonitorEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitorEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the editor aspect of the StageMonitoring plugin modules. \n */" },
		{ "IncludePath", "StageMonitorEditorSettings.h" },
		{ "ModuleRelativePath", "Private/StageMonitorEditorSettings.h" },
		{ "ToolTip", "Settings for the editor aspect of the StageMonitoring plugin modules." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitorEditorSettings_Statics::NewProp_RefreshRate_MetaData[] = {
		{ "Category", "UI" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** Refresh rate in seconds for the StageMonitor panel. */" },
		{ "ModuleRelativePath", "Private/StageMonitorEditorSettings.h" },
		{ "ToolTip", "Refresh rate in seconds for the StageMonitor panel." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UStageMonitorEditorSettings_Statics::NewProp_RefreshRate = { "RefreshRate", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStageMonitorEditorSettings, RefreshRate), METADATA_PARAMS(Z_Construct_UClass_UStageMonitorEditorSettings_Statics::NewProp_RefreshRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitorEditorSettings_Statics::NewProp_RefreshRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UStageMonitorEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitorEditorSettings_Statics::NewProp_RefreshRate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UStageMonitorEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UStageMonitorEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UStageMonitorEditorSettings_Statics::ClassParams = {
		&UStageMonitorEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UStageMonitorEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitorEditorSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UStageMonitorEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitorEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UStageMonitorEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UStageMonitorEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStageMonitorEditorSettings, 2429599364);
	template<> STAGEMONITOREDITOR_API UClass* StaticClass<UStageMonitorEditorSettings>()
	{
		return UStageMonitorEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStageMonitorEditorSettings(Z_Construct_UClass_UStageMonitorEditorSettings, &UStageMonitorEditorSettings::StaticClass, TEXT("/Script/StageMonitorEditor"), TEXT("UStageMonitorEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStageMonitorEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
