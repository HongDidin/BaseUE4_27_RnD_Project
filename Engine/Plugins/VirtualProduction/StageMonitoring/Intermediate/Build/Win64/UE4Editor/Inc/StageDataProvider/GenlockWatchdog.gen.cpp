// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageDataProvider/Private/GenlockWatchdog.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGenlockWatchdog() {}
// Cross Module References
	STAGEDATAPROVIDER_API UScriptStruct* Z_Construct_UScriptStruct_FGenlockStateEvent();
	UPackage* Z_Construct_UPackage__Script_StageDataProvider();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageProviderEventMessage();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECustomTimeStepSynchronizationState();
	STAGEDATAPROVIDER_API UScriptStruct* Z_Construct_UScriptStruct_FGenlockHitchEvent();
// End Cross Module References

static_assert(std::is_polymorphic<FGenlockStateEvent>() == std::is_polymorphic<FStageProviderEventMessage>(), "USTRUCT FGenlockStateEvent cannot be polymorphic unless super FStageProviderEventMessage is polymorphic");

class UScriptStruct* FGenlockStateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEDATAPROVIDER_API uint32 Get_Z_Construct_UScriptStruct_FGenlockStateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGenlockStateEvent, Z_Construct_UPackage__Script_StageDataProvider(), TEXT("GenlockStateEvent"), sizeof(FGenlockStateEvent), Get_Z_Construct_UScriptStruct_FGenlockStateEvent_Hash());
	}
	return Singleton;
}
template<> STAGEDATAPROVIDER_API UScriptStruct* StaticStruct<FGenlockStateEvent>()
{
	return FGenlockStateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGenlockStateEvent(FGenlockStateEvent::StaticStruct, TEXT("/Script/StageDataProvider"), TEXT("GenlockStateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_StageDataProvider_StaticRegisterNativesFGenlockStateEvent
{
	FScriptStruct_StageDataProvider_StaticRegisterNativesFGenlockStateEvent()
	{
		UScriptStruct::DeferCppStructOps<FGenlockStateEvent>(FName(TEXT("GenlockStateEvent")));
	}
} ScriptStruct_StageDataProvider_StaticRegisterNativesFGenlockStateEvent;
	struct Z_Construct_UScriptStruct_FGenlockStateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NewState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Stage event to notify of genlock custom timestep state change\n */" },
		{ "ModuleRelativePath", "Private/GenlockWatchdog.h" },
		{ "ToolTip", "Stage event to notify of genlock custom timestep state change" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGenlockStateEvent>();
	}
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState_MetaData[] = {
		{ "Category", "GenlockState" },
		{ "Comment", "/** New state of genlock custom timestep (i.e. Synchronized, Error, etc...) */" },
		{ "ModuleRelativePath", "Private/GenlockWatchdog.h" },
		{ "ToolTip", "New state of genlock custom timestep (i.e. Synchronized, Error, etc...)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState = { "NewState", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGenlockStateEvent, NewState), Z_Construct_UEnum_Engine_ECustomTimeStepSynchronizationState, METADATA_PARAMS(Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::NewProp_NewState,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageDataProvider,
		Z_Construct_UScriptStruct_FStageProviderEventMessage,
		&NewStructOps,
		"GenlockStateEvent",
		sizeof(FGenlockStateEvent),
		alignof(FGenlockStateEvent),
		Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGenlockStateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGenlockStateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageDataProvider();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GenlockStateEvent"), sizeof(FGenlockStateEvent), Get_Z_Construct_UScriptStruct_FGenlockStateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGenlockStateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGenlockStateEvent_Hash() { return 2654107268U; }

static_assert(std::is_polymorphic<FGenlockHitchEvent>() == std::is_polymorphic<FStageProviderEventMessage>(), "USTRUCT FGenlockHitchEvent cannot be polymorphic unless super FStageProviderEventMessage is polymorphic");

class UScriptStruct* FGenlockHitchEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEDATAPROVIDER_API uint32 Get_Z_Construct_UScriptStruct_FGenlockHitchEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGenlockHitchEvent, Z_Construct_UPackage__Script_StageDataProvider(), TEXT("GenlockHitchEvent"), sizeof(FGenlockHitchEvent), Get_Z_Construct_UScriptStruct_FGenlockHitchEvent_Hash());
	}
	return Singleton;
}
template<> STAGEDATAPROVIDER_API UScriptStruct* StaticStruct<FGenlockHitchEvent>()
{
	return FGenlockHitchEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGenlockHitchEvent(FGenlockHitchEvent::StaticStruct, TEXT("/Script/StageDataProvider"), TEXT("GenlockHitchEvent"), false, nullptr, nullptr);
static struct FScriptStruct_StageDataProvider_StaticRegisterNativesFGenlockHitchEvent
{
	FScriptStruct_StageDataProvider_StaticRegisterNativesFGenlockHitchEvent()
	{
		UScriptStruct::DeferCppStructOps<FGenlockHitchEvent>(FName(TEXT("GenlockHitchEvent")));
	}
} ScriptStruct_StageDataProvider_StaticRegisterNativesFGenlockHitchEvent;
	struct Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MissedSyncSignals_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MissedSyncSignals;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Stage event to notify of missed sync (genlock) signals\n * When sync signal are lost, it means the engine is not running \n * fast enough to keep track of the genlock source. \n */" },
		{ "ModuleRelativePath", "Private/GenlockWatchdog.h" },
		{ "ToolTip", "Stage event to notify of missed sync (genlock) signals\nWhen sync signal are lost, it means the engine is not running\nfast enough to keep track of the genlock source." },
	};
#endif
	void* Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGenlockHitchEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::NewProp_MissedSyncSignals_MetaData[] = {
		{ "Category", "GenlockState" },
		{ "Comment", "/** Number of sync counts that were missed between tick */" },
		{ "ModuleRelativePath", "Private/GenlockWatchdog.h" },
		{ "ToolTip", "Number of sync counts that were missed between tick" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::NewProp_MissedSyncSignals = { "MissedSyncSignals", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGenlockHitchEvent, MissedSyncSignals), METADATA_PARAMS(Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::NewProp_MissedSyncSignals_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::NewProp_MissedSyncSignals_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::NewProp_MissedSyncSignals,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageDataProvider,
		Z_Construct_UScriptStruct_FStageProviderEventMessage,
		&NewStructOps,
		"GenlockHitchEvent",
		sizeof(FGenlockHitchEvent),
		alignof(FGenlockHitchEvent),
		Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGenlockHitchEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGenlockHitchEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageDataProvider();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GenlockHitchEvent"), sizeof(FGenlockHitchEvent), Get_Z_Construct_UScriptStruct_FGenlockHitchEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGenlockHitchEvent_Hash() { return 2669839220U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
