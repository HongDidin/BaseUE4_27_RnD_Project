// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageDataProvider/Private/FramePerformanceProvider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFramePerformanceProvider() {}
// Cross Module References
	STAGEDATAPROVIDER_API UScriptStruct* Z_Construct_UScriptStruct_FHitchDetectionMessage();
	UPackage* Z_Construct_UPackage__Script_StageDataProvider();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageProviderEventMessage();
// End Cross Module References

static_assert(std::is_polymorphic<FHitchDetectionMessage>() == std::is_polymorphic<FStageProviderEventMessage>(), "USTRUCT FHitchDetectionMessage cannot be polymorphic unless super FStageProviderEventMessage is polymorphic");

class UScriptStruct* FHitchDetectionMessage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEDATAPROVIDER_API uint32 Get_Z_Construct_UScriptStruct_FHitchDetectionMessage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHitchDetectionMessage, Z_Construct_UPackage__Script_StageDataProvider(), TEXT("HitchDetectionMessage"), sizeof(FHitchDetectionMessage), Get_Z_Construct_UScriptStruct_FHitchDetectionMessage_Hash());
	}
	return Singleton;
}
template<> STAGEDATAPROVIDER_API UScriptStruct* StaticStruct<FHitchDetectionMessage>()
{
	return FHitchDetectionMessage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHitchDetectionMessage(FHitchDetectionMessage::StaticStruct, TEXT("/Script/StageDataProvider"), TEXT("HitchDetectionMessage"), false, nullptr, nullptr);
static struct FScriptStruct_StageDataProvider_StaticRegisterNativesFHitchDetectionMessage
{
	FScriptStruct_StageDataProvider_StaticRegisterNativesFHitchDetectionMessage()
	{
		UScriptStruct::DeferCppStructOps<FHitchDetectionMessage>(FName(TEXT("HitchDetectionMessage")));
	}
} ScriptStruct_StageDataProvider_StaticRegisterNativesFHitchDetectionMessage;
	struct Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameThreadWithWaitsMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GameThreadWithWaitsMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderThreadWithWaitsMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderThreadWithWaitsMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameThreadMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GameThreadMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderThreadMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderThreadMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GPU_MS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GPU_MS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimingThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimingThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitchedTimeFPS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HitchedTimeFPS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AverageFPS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AverageFPS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Message sent when a hitch was detected on a provider machine.\n */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Message sent when a hitch was detected on a provider machine." },
	};
#endif
	void* Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHitchDetectionMessage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadWithWaitsMS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Current GameThread time including any waits read from StatsThread in milliseconds */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Current GameThread time including any waits read from StatsThread in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadWithWaitsMS = { "GameThreadWithWaitsMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, GameThreadWithWaitsMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadWithWaitsMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadWithWaitsMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadWithWaitsMS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Current RenderThread time including any waits read from StatsThread in milliseconds */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Current RenderThread time including any waits read from StatsThread in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadWithWaitsMS = { "RenderThreadWithWaitsMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, RenderThreadWithWaitsMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadWithWaitsMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadWithWaitsMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadMS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Current GameThread time read from GGameThreadTime in milliseconds */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Current GameThread time read from GGameThreadTime in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadMS = { "GameThreadMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, GameThreadMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadMS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Current RenderThread time read from GRenderThreadTime in milliseconds */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Current RenderThread time read from GRenderThreadTime in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadMS = { "RenderThreadMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, RenderThreadMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GPU_MS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Current GPU time read from GGPUFrameTime in milliseconds */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Current GPU time read from GGPUFrameTime in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GPU_MS = { "GPU_MS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, GPU_MS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GPU_MS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GPU_MS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_TimingThreshold_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Timing threshold that was crossed in milliseconds */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Timing threshold that was crossed in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_TimingThreshold = { "TimingThreshold", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, TimingThreshold), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_TimingThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_TimingThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_HitchedTimeFPS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** FPS correspondent to the timing that triggered the hitch (game or render thread) */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "FPS correspondent to the timing that triggered the hitch (game or render thread)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_HitchedTimeFPS = { "HitchedTimeFPS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, HitchedTimeFPS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_HitchedTimeFPS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_HitchedTimeFPS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_AverageFPS_MetaData[] = {
		{ "Category", "Hitch" },
		{ "Comment", "/** Average FPS when hitch occured */" },
		{ "ModuleRelativePath", "Private/FramePerformanceProvider.h" },
		{ "ToolTip", "Average FPS when hitch occured" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_AverageFPS = { "AverageFPS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FHitchDetectionMessage, AverageFPS), METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_AverageFPS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_AverageFPS_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadWithWaitsMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadWithWaitsMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GameThreadMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_RenderThreadMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_GPU_MS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_TimingThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_HitchedTimeFPS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::NewProp_AverageFPS,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageDataProvider,
		Z_Construct_UScriptStruct_FStageProviderEventMessage,
		&NewStructOps,
		"HitchDetectionMessage",
		sizeof(FHitchDetectionMessage),
		alignof(FHitchDetectionMessage),
		Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHitchDetectionMessage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHitchDetectionMessage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageDataProvider();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HitchDetectionMessage"), sizeof(FHitchDetectionMessage), Get_Z_Construct_UScriptStruct_FHitchDetectionMessage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHitchDetectionMessage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHitchDetectionMessage_Hash() { return 2154605384U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
