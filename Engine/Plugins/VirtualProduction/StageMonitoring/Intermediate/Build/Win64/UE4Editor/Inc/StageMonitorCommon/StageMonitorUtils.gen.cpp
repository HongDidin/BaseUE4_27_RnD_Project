// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageMonitorCommon/Public/StageMonitorUtils.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStageMonitorUtils() {}
// Cross Module References
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FFramePerformanceProviderMessage();
	UPackage* Z_Construct_UPackage__Script_StageMonitorCommon();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageProviderPeriodicMessage();
// End Cross Module References

static_assert(std::is_polymorphic<FFramePerformanceProviderMessage>() == std::is_polymorphic<FStageProviderPeriodicMessage>(), "USTRUCT FFramePerformanceProviderMessage cannot be polymorphic unless super FStageProviderPeriodicMessage is polymorphic");

class UScriptStruct* FFramePerformanceProviderMessage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("FramePerformanceProviderMessage"), sizeof(FFramePerformanceProviderMessage), Get_Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FFramePerformanceProviderMessage>()
{
	return FFramePerformanceProviderMessage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFramePerformanceProviderMessage(FFramePerformanceProviderMessage::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("FramePerformanceProviderMessage"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFFramePerformanceProviderMessage
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFFramePerformanceProviderMessage()
	{
		UScriptStruct::DeferCppStructOps<FFramePerformanceProviderMessage>(FName(TEXT("FramePerformanceProviderMessage")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFFramePerformanceProviderMessage;
	struct Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AverageFPS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AverageFPS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameThreadMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GameThreadMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderThreadMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderThreadMS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GPU_MS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GPU_MS;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IdleTimeMS_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_IdleTimeMS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Message containing information about frame timings.\n * Sent at regular intervals\n */" },
		{ "ModuleRelativePath", "Public/StageMonitorUtils.h" },
		{ "ToolTip", "Message containing information about frame timings.\nSent at regular intervals" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFramePerformanceProviderMessage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_AverageFPS_MetaData[] = {
		{ "Category", "Performance" },
		{ "Comment", "/** Average FrameRate read from GAverageFPS */" },
		{ "ModuleRelativePath", "Public/StageMonitorUtils.h" },
		{ "ToolTip", "Average FrameRate read from GAverageFPS" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_AverageFPS = { "AverageFPS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFramePerformanceProviderMessage, AverageFPS), METADATA_PARAMS(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_AverageFPS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_AverageFPS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GameThreadMS_MetaData[] = {
		{ "Category", "Performance" },
		{ "Comment", "/** Current GameThread time read from GGameThreadTime in milliseconds */" },
		{ "ModuleRelativePath", "Public/StageMonitorUtils.h" },
		{ "ToolTip", "Current GameThread time read from GGameThreadTime in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GameThreadMS = { "GameThreadMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFramePerformanceProviderMessage, GameThreadMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GameThreadMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GameThreadMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_RenderThreadMS_MetaData[] = {
		{ "Category", "Performance" },
		{ "Comment", "/** Current RenderThread time read from GRenderThreadTime in milliseconds */" },
		{ "ModuleRelativePath", "Public/StageMonitorUtils.h" },
		{ "ToolTip", "Current RenderThread time read from GRenderThreadTime in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_RenderThreadMS = { "RenderThreadMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFramePerformanceProviderMessage, RenderThreadMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_RenderThreadMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_RenderThreadMS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GPU_MS_MetaData[] = {
		{ "Category", "Performance" },
		{ "Comment", "/** Current GPU time read from GGPUFrameTime in milliseconds */" },
		{ "ModuleRelativePath", "Public/StageMonitorUtils.h" },
		{ "ToolTip", "Current GPU time read from GGPUFrameTime in milliseconds" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GPU_MS = { "GPU_MS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFramePerformanceProviderMessage, GPU_MS), METADATA_PARAMS(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GPU_MS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GPU_MS_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_IdleTimeMS_MetaData[] = {
		{ "Category", "Performance" },
		{ "Comment", "/** Idle time (slept) in milliseconds during last frame */" },
		{ "ModuleRelativePath", "Public/StageMonitorUtils.h" },
		{ "ToolTip", "Idle time (slept) in milliseconds during last frame" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_IdleTimeMS = { "IdleTimeMS", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFramePerformanceProviderMessage, IdleTimeMS), METADATA_PARAMS(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_IdleTimeMS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_IdleTimeMS_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_AverageFPS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GameThreadMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_RenderThreadMS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_GPU_MS,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::NewProp_IdleTimeMS,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		Z_Construct_UScriptStruct_FStageProviderPeriodicMessage,
		&NewStructOps,
		"FramePerformanceProviderMessage",
		sizeof(FFramePerformanceProviderMessage),
		alignof(FFramePerformanceProviderMessage),
		Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFramePerformanceProviderMessage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FramePerformanceProviderMessage"), sizeof(FFramePerformanceProviderMessage), Get_Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFramePerformanceProviderMessage_Hash() { return 3707690123U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
