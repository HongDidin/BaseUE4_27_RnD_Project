// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STAGEMONITOREDITOR_StageMonitorEditorSettings_generated_h
#error "StageMonitorEditorSettings.generated.h already included, missing '#pragma once' in StageMonitorEditorSettings.h"
#endif
#define STAGEMONITOREDITOR_StageMonitorEditorSettings_generated_h

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStageMonitorEditorSettings(); \
	friend struct Z_Construct_UClass_UStageMonitorEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UStageMonitorEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StageMonitorEditor"), STAGEMONITOREDITOR_API) \
	DECLARE_SERIALIZER(UStageMonitorEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUStageMonitorEditorSettings(); \
	friend struct Z_Construct_UClass_UStageMonitorEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UStageMonitorEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StageMonitorEditor"), STAGEMONITOREDITOR_API) \
	DECLARE_SERIALIZER(UStageMonitorEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STAGEMONITOREDITOR_API UStageMonitorEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStageMonitorEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STAGEMONITOREDITOR_API, UStageMonitorEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStageMonitorEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STAGEMONITOREDITOR_API UStageMonitorEditorSettings(UStageMonitorEditorSettings&&); \
	STAGEMONITOREDITOR_API UStageMonitorEditorSettings(const UStageMonitorEditorSettings&); \
public:


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STAGEMONITOREDITOR_API UStageMonitorEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STAGEMONITOREDITOR_API UStageMonitorEditorSettings(UStageMonitorEditorSettings&&); \
	STAGEMONITOREDITOR_API UStageMonitorEditorSettings(const UStageMonitorEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STAGEMONITOREDITOR_API, UStageMonitorEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStageMonitorEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStageMonitorEditorSettings)


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_INCLASS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STAGEMONITOREDITOR_API UClass* StaticClass<class UStageMonitorEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageMonitorEditor_Private_StageMonitorEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
