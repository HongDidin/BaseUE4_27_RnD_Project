// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STAGEDATAPROVIDER_GenlockWatchdog_generated_h
#error "GenlockWatchdog.generated.h already included, missing '#pragma once' in GenlockWatchdog.h"
#endif
#define STAGEDATAPROVIDER_GenlockWatchdog_generated_h

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageDataProvider_Private_GenlockWatchdog_h_42_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGenlockStateEvent_Statics; \
	STAGEDATAPROVIDER_API static class UScriptStruct* StaticStruct(); \
	typedef FStageProviderEventMessage Super;


template<> STAGEDATAPROVIDER_API UScriptStruct* StaticStruct<struct FGenlockStateEvent>();

#define Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageDataProvider_Private_GenlockWatchdog_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGenlockHitchEvent_Statics; \
	STAGEDATAPROVIDER_API static class UScriptStruct* StaticStruct(); \
	typedef FStageProviderEventMessage Super;


template<> STAGEDATAPROVIDER_API UScriptStruct* StaticStruct<struct FGenlockHitchEvent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_StageMonitoring_Source_StageDataProvider_Private_GenlockWatchdog_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
