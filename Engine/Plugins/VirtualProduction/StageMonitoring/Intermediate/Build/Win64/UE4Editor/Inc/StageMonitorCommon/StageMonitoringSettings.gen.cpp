// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageMonitorCommon/Public/StageMonitoringSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStageMonitoringSettings() {}
// Cross Module References
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FStageMonitorSettings();
	UPackage* Z_Construct_UPackage__Script_StageMonitorCommon();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTagContainer();
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FStageDataProviderSettings();
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FStageMessageTypeWrapper();
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FStageFramePerformanceSettings();
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FStageHitchDetectionSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	STAGEMONITORCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FStageDataExportSettings();
	STAGEMONITORCOMMON_API UClass* Z_Construct_UClass_UStageMonitoringSettings_NoRegister();
	STAGEMONITORCOMMON_API UClass* Z_Construct_UClass_UStageMonitoringSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
// End Cross Module References
class UScriptStruct* FStageMonitorSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FStageMonitorSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageMonitorSettings, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("StageMonitorSettings"), sizeof(FStageMonitorSettings), Get_Z_Construct_UScriptStruct_FStageMonitorSettings_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FStageMonitorSettings>()
{
	return FStageMonitorSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageMonitorSettings(FStageMonitorSettings::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("StageMonitorSettings"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageMonitorSettings
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageMonitorSettings()
	{
		UScriptStruct::DeferCppStructOps<FStageMonitorSettings>(FName(TEXT("StageMonitorSettings")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageMonitorSettings;
	struct Z_Construct_UScriptStruct_FStageMonitorSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseRoleFiltering_MetaData[];
#endif
		static void NewProp_bUseRoleFiltering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseRoleFiltering;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupportedRoles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SupportedRoles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DiscoveryMessageInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DiscoveryMessageInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoStart_MetaData[];
#endif
		static void NewProp_bAutoStart_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoStart;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for StageMonitor\n */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings for StageMonitor" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageMonitorSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** If true, Monitor will only start if machine has a role contained in SupportedRoles */" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "If true, Monitor will only start if machine has a role contained in SupportedRoles" },
	};
#endif
	void Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering_SetBit(void* Obj)
	{
		((FStageMonitorSettings*)Obj)->bUseRoleFiltering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering = { "bUseRoleFiltering", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FStageMonitorSettings), &Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_SupportedRoles_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** If checked, VP Role of this instance must be part of these roles to have the monitor operational */" },
		{ "EditCondition", "bUseRoleFiltering" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "If checked, VP Role of this instance must be part of these roles to have the monitor operational" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_SupportedRoles = { "SupportedRoles", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageMonitorSettings, SupportedRoles), Z_Construct_UScriptStruct_FGameplayTagContainer, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_SupportedRoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_SupportedRoles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_DiscoveryMessageInterval_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Interval between each discovery signal sent by Monitors */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Interval between each discovery signal sent by Monitors" },
		{ "Units", "s" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_DiscoveryMessageInterval = { "DiscoveryMessageInterval", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageMonitorSettings, DiscoveryMessageInterval), METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_DiscoveryMessageInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_DiscoveryMessageInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/**\n\x09 * Whether we should start monitoring on launch.\n\x09 * @note It may be overriden via the command line, \"-StageMonitorAutoStart=1 and via command line in editor\"\n\x09 */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Whether we should start monitoring on launch.\n@note It may be overriden via the command line, \"-StageMonitorAutoStart=1 and via command line in editor\"" },
	};
#endif
	void Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart_SetBit(void* Obj)
	{
		((FStageMonitorSettings*)Obj)->bAutoStart = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart = { "bAutoStart", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FStageMonitorSettings), &Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bUseRoleFiltering,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_SupportedRoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_DiscoveryMessageInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::NewProp_bAutoStart,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		nullptr,
		&NewStructOps,
		"StageMonitorSettings",
		sizeof(FStageMonitorSettings),
		alignof(FStageMonitorSettings),
		Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageMonitorSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageMonitorSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageMonitorSettings"), sizeof(FStageMonitorSettings), Get_Z_Construct_UScriptStruct_FStageMonitorSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageMonitorSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageMonitorSettings_Hash() { return 601963346U; }
class UScriptStruct* FStageDataProviderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FStageDataProviderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageDataProviderSettings, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("StageDataProviderSettings"), sizeof(FStageDataProviderSettings), Get_Z_Construct_UScriptStruct_FStageDataProviderSettings_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FStageDataProviderSettings>()
{
	return FStageDataProviderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageDataProviderSettings(FStageDataProviderSettings::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("StageDataProviderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageDataProviderSettings
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageDataProviderSettings()
	{
		UScriptStruct::DeferCppStructOps<FStageDataProviderSettings>(FName(TEXT("StageDataProviderSettings")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageDataProviderSettings;
	struct Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseRoleFiltering_MetaData[];
#endif
		static void NewProp_bUseRoleFiltering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseRoleFiltering;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupportedRoles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SupportedRoles;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MessageTypeRoleExclusion_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MessageTypeRoleExclusion_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageTypeRoleExclusion_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_MessageTypeRoleExclusion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FramePerformanceSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FramePerformanceSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HitchDetectionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HitchDetectionSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings associated to DataProviders\n */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings associated to DataProviders" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageDataProviderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** If true, DataProvider will only start if machine has a role contained in SupportedRoles */" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "If true, DataProvider will only start if machine has a role contained in SupportedRoles" },
	};
#endif
	void Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering_SetBit(void* Obj)
	{
		((FStageDataProviderSettings*)Obj)->bUseRoleFiltering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering = { "bUseRoleFiltering", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FStageDataProviderSettings), &Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_SupportedRoles_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** If checked, VP Role of this instance must be part of these roles to have the monitor operational */" },
		{ "EditCondition", "bUseRoleFiltering" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "If checked, VP Role of this instance must be part of these roles to have the monitor operational" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_SupportedRoles = { "SupportedRoles", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageDataProviderSettings, SupportedRoles), Z_Construct_UScriptStruct_FGameplayTagContainer, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_SupportedRoles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_SupportedRoles_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_ValueProp = { "MessageTypeRoleExclusion", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FGameplayTagContainer, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_Key_KeyProp = { "MessageTypeRoleExclusion_Key", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FStageMessageTypeWrapper, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion = { "MessageTypeRoleExclusion", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageDataProviderSettings, MessageTypeRoleExclusion), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_FramePerformanceSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Settings about Frame Performance messaging */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings about Frame Performance messaging" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_FramePerformanceSettings = { "FramePerformanceSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageDataProviderSettings, FramePerformanceSettings), Z_Construct_UScriptStruct_FStageFramePerformanceSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_FramePerformanceSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_FramePerformanceSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_HitchDetectionSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Settings about Hitch detection*/" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings about Hitch detection" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_HitchDetectionSettings = { "HitchDetectionSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageDataProviderSettings, HitchDetectionSettings), Z_Construct_UScriptStruct_FStageHitchDetectionSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_HitchDetectionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_HitchDetectionSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_bUseRoleFiltering,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_SupportedRoles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_MessageTypeRoleExclusion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_FramePerformanceSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::NewProp_HitchDetectionSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		nullptr,
		&NewStructOps,
		"StageDataProviderSettings",
		sizeof(FStageDataProviderSettings),
		alignof(FStageDataProviderSettings),
		Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageDataProviderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageDataProviderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageDataProviderSettings"), sizeof(FStageDataProviderSettings), Get_Z_Construct_UScriptStruct_FStageDataProviderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageDataProviderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageDataProviderSettings_Hash() { return 3988335912U; }
class UScriptStruct* FStageHitchDetectionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageHitchDetectionSettings, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("StageHitchDetectionSettings"), sizeof(FStageHitchDetectionSettings), Get_Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FStageHitchDetectionSettings>()
{
	return FStageHitchDetectionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageHitchDetectionSettings(FStageHitchDetectionSettings::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("StageHitchDetectionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageHitchDetectionSettings
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageHitchDetectionSettings()
	{
		UScriptStruct::DeferCppStructOps<FStageHitchDetectionSettings>(FName(TEXT("StageHitchDetectionSettings")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageHitchDetectionSettings;
	struct Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableHitchDetection_MetaData[];
#endif
		static void NewProp_bEnableHitchDetection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableHitchDetection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MinimumFrameRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings related to HitchDetection messages\n */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings related to HitchDetection messages" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageHitchDetectionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection_MetaData[] = {
		{ "Category", "Hitch Detection" },
		{ "Comment", "/** \n\x09 * Whether or not hitch detection should be enabled\n\x09 * @note: This uses stat data. To avoid having on-screen message\n\x09 * GAreScreenMessagesEnabled = false or -ExecCmds=\"DISABLEALLSCREENMESSAGES\" on command line\n\x09 * will turn them off.\n\x09 * For more accurate hitch detection, use genlock which\n\x09 * will have better missed frames information\n\x09 */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Whether or not hitch detection should be enabled\n@note: This uses stat data. To avoid having on-screen message\nGAreScreenMessagesEnabled = false or -ExecCmds=\"DISABLEALLSCREENMESSAGES\" on command line\nwill turn them off.\nFor more accurate hitch detection, use genlock which\nwill have better missed frames information" },
	};
#endif
	void Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection_SetBit(void* Obj)
	{
		((FStageHitchDetectionSettings*)Obj)->bEnableHitchDetection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection = { "bEnableHitchDetection", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FStageHitchDetectionSettings), &Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_MinimumFrameRate_MetaData[] = {
		{ "Category", "Hitch Detection" },
		{ "Comment", "/** Target FPS we're aiming for.  */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Target FPS we're aiming for." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_MinimumFrameRate = { "MinimumFrameRate", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageHitchDetectionSettings, MinimumFrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_MinimumFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_MinimumFrameRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_bEnableHitchDetection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::NewProp_MinimumFrameRate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		nullptr,
		&NewStructOps,
		"StageHitchDetectionSettings",
		sizeof(FStageHitchDetectionSettings),
		alignof(FStageHitchDetectionSettings),
		Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageHitchDetectionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageHitchDetectionSettings"), sizeof(FStageHitchDetectionSettings), Get_Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageHitchDetectionSettings_Hash() { return 1602774214U; }
class UScriptStruct* FStageFramePerformanceSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageFramePerformanceSettings, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("StageFramePerformanceSettings"), sizeof(FStageFramePerformanceSettings), Get_Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FStageFramePerformanceSettings>()
{
	return FStageFramePerformanceSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageFramePerformanceSettings(FStageFramePerformanceSettings::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("StageFramePerformanceSettings"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageFramePerformanceSettings
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageFramePerformanceSettings()
	{
		UScriptStruct::DeferCppStructOps<FStageFramePerformanceSettings>(FName(TEXT("StageFramePerformanceSettings")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageFramePerformanceSettings;
	struct Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpdateInterval;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings related to FramePerformance messages\n */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings related to FramePerformance messages" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageFramePerformanceSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::NewProp_UpdateInterval_MetaData[] = {
		{ "Category", "Frame Performance" },
		{ "Comment", "/** Target FPS we're aiming for. */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Target FPS we're aiming for." },
		{ "Unit", "s" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::NewProp_UpdateInterval = { "UpdateInterval", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageFramePerformanceSettings, UpdateInterval), METADATA_PARAMS(Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::NewProp_UpdateInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::NewProp_UpdateInterval_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::NewProp_UpdateInterval,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		nullptr,
		&NewStructOps,
		"StageFramePerformanceSettings",
		sizeof(FStageFramePerformanceSettings),
		alignof(FStageFramePerformanceSettings),
		Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageFramePerformanceSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageFramePerformanceSettings"), sizeof(FStageFramePerformanceSettings), Get_Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageFramePerformanceSettings_Hash() { return 3656736605U; }
class UScriptStruct* FStageDataExportSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FStageDataExportSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageDataExportSettings, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("StageDataExportSettings"), sizeof(FStageDataExportSettings), Get_Z_Construct_UScriptStruct_FStageDataExportSettings_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FStageDataExportSettings>()
{
	return FStageDataExportSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageDataExportSettings(FStageDataExportSettings::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("StageDataExportSettings"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageDataExportSettings
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageDataExportSettings()
	{
		UScriptStruct::DeferCppStructOps<FStageDataExportSettings>(FName(TEXT("StageDataExportSettings")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageDataExportSettings;
	struct Z_Construct_UScriptStruct_FStageDataExportSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bKeepOnlyLastPeriodMessage_MetaData[];
#endif
		static void NewProp_bKeepOnlyLastPeriodMessage_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bKeepOnlyLastPeriodMessage;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludedMessageTypes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludedMessageTypes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludedMessageTypes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings associated to file exporter\n */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings associated to file exporter" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageDataExportSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage_MetaData[] = {
		{ "Category", "Export" },
		{ "Comment", "/** \n\x09 * Save only the last instance of periodic message types\n\x09 * True by default to reduce file size\n\x09 */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Save only the last instance of periodic message types\nTrue by default to reduce file size" },
	};
#endif
	void Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage_SetBit(void* Obj)
	{
		((FStageDataExportSettings*)Obj)->bKeepOnlyLastPeriodMessage = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage = { "bKeepOnlyLastPeriodMessage", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FStageDataExportSettings), &Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes_Inner = { "ExcludedMessageTypes", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FStageMessageTypeWrapper, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes_MetaData[] = {
		{ "Category", "Export" },
		{ "Comment", "/** Message types to exclude from session when exporting */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Message types to exclude from session when exporting" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes = { "ExcludedMessageTypes", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageDataExportSettings, ExcludedMessageTypes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_bKeepOnlyLastPeriodMessage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::NewProp_ExcludedMessageTypes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		nullptr,
		&NewStructOps,
		"StageDataExportSettings",
		sizeof(FStageDataExportSettings),
		alignof(FStageDataExportSettings),
		Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageDataExportSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageDataExportSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageDataExportSettings"), sizeof(FStageDataExportSettings), Get_Z_Construct_UScriptStruct_FStageDataExportSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageDataExportSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageDataExportSettings_Hash() { return 2330775173U; }
class UScriptStruct* FStageMessageTypeWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITORCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FStageMessageTypeWrapper, Z_Construct_UPackage__Script_StageMonitorCommon(), TEXT("StageMessageTypeWrapper"), sizeof(FStageMessageTypeWrapper), Get_Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Hash());
	}
	return Singleton;
}
template<> STAGEMONITORCOMMON_API UScriptStruct* StaticStruct<FStageMessageTypeWrapper>()
{
	return FStageMessageTypeWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FStageMessageTypeWrapper(FStageMessageTypeWrapper::StaticStruct, TEXT("/Script/StageMonitorCommon"), TEXT("StageMessageTypeWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageMessageTypeWrapper
{
	FScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageMessageTypeWrapper()
	{
		UScriptStruct::DeferCppStructOps<FStageMessageTypeWrapper>(FName(TEXT("StageMessageTypeWrapper")));
	}
} ScriptStruct_StageMonitorCommon_StaticRegisterNativesFStageMessageTypeWrapper;
	struct Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageType_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MessageType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Wrapper structure holding a message type static struct.\n * Used with a customization to generate a filtered list\n */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Wrapper structure holding a message type static struct.\nUsed with a customization to generate a filtered list" },
	};
#endif
	void* Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FStageMessageTypeWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::NewProp_MessageType_MetaData[] = {
		{ "Category", "Stage Message" },
		{ "Comment", "/** Name of StaticStruct message type */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Name of StaticStruct message type" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::NewProp_MessageType = { "MessageType", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FStageMessageTypeWrapper, MessageType), METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::NewProp_MessageType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::NewProp_MessageType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::NewProp_MessageType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
		nullptr,
		&NewStructOps,
		"StageMessageTypeWrapper",
		sizeof(FStageMessageTypeWrapper),
		alignof(FStageMessageTypeWrapper),
		Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FStageMessageTypeWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitorCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("StageMessageTypeWrapper"), sizeof(FStageMessageTypeWrapper), Get_Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FStageMessageTypeWrapper_Hash() { return 1305702141U; }
	void UStageMonitoringSettings::StaticRegisterNativesUStageMonitoringSettings()
	{
	}
	UClass* Z_Construct_UClass_UStageMonitoringSettings_NoRegister()
	{
		return UStageMonitoringSettings::StaticClass();
	}
	struct Z_Construct_UClass_UStageMonitoringSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseSessionId_MetaData[];
#endif
		static void NewProp_bUseSessionId_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseSessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageSessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StageSessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeoutInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeoutInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MonitorSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MonitorSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProviderSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExportSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UStageMonitoringSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitorCommon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Settings for the StageMonitoring plugin modules. \n * Data Provider, Monitor and shared settings are contained here to centralize access through project settings\n */" },
		{ "IncludePath", "StageMonitoringSettings.h" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings for the StageMonitoring plugin modules.\nData Provider, Monitor and shared settings are contained here to centralize access through project settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** If true, Stage monitor will only listen to Stage Providers with same sessionId */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "If true, Stage monitor will only listen to Stage Providers with same sessionId" },
	};
#endif
	void Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId_SetBit(void* Obj)
	{
		((UStageMonitoringSettings*)Obj)->bUseSessionId = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId = { "bUseSessionId", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UStageMonitoringSettings), &Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId_SetBit, METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_StageSessionId_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/**\n\x09 * The projects Stage SessionId to differentiate data sent over network.\n\x09 * @note It may be overriden via the command line, \"-StageSessionId=1\"\n\x09 */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "The projects Stage SessionId to differentiate data sent over network.\n@note It may be overriden via the command line, \"-StageSessionId=1\"" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_StageSessionId = { "StageSessionId", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStageMonitoringSettings, StageSessionId), METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_StageSessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_StageSessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_TimeoutInterval_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Interval threshold between message reception before dropping out a provider or a monitor */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Interval threshold between message reception before dropping out a provider or a monitor" },
		{ "Unit", "s" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_TimeoutInterval = { "TimeoutInterval", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStageMonitoringSettings, TimeoutInterval), METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_TimeoutInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_TimeoutInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_MonitorSettings_MetaData[] = {
		{ "Category", "Monitor Settings" },
		{ "Comment", "/** Settings for monitors */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings for monitors" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_MonitorSettings = { "MonitorSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStageMonitoringSettings, MonitorSettings), Z_Construct_UScriptStruct_FStageMonitorSettings, METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_MonitorSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_MonitorSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ProviderSettings_MetaData[] = {
		{ "Category", "Provider Settings" },
		{ "Comment", "/** Settings for Data Providers */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings for Data Providers" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ProviderSettings = { "ProviderSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStageMonitoringSettings, ProviderSettings), Z_Construct_UScriptStruct_FStageDataProviderSettings, METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ProviderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ProviderSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ExportSettings_MetaData[] = {
		{ "Category", "Export Settings" },
		{ "Comment", "/** Settings for Data Providers */" },
		{ "ModuleRelativePath", "Public/StageMonitoringSettings.h" },
		{ "ToolTip", "Settings for Data Providers" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ExportSettings = { "ExportSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStageMonitoringSettings, ExportSettings), Z_Construct_UScriptStruct_FStageDataExportSettings, METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ExportSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ExportSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UStageMonitoringSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_bUseSessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_StageSessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_TimeoutInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_MonitorSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ProviderSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStageMonitoringSettings_Statics::NewProp_ExportSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UStageMonitoringSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UStageMonitoringSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UStageMonitoringSettings_Statics::ClassParams = {
		&UStageMonitoringSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UStageMonitoringSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UStageMonitoringSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UStageMonitoringSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UStageMonitoringSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UStageMonitoringSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStageMonitoringSettings, 129961933);
	template<> STAGEMONITORCOMMON_API UClass* StaticClass<UStageMonitoringSettings>()
	{
		return UStageMonitoringSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStageMonitoringSettings(Z_Construct_UClass_UStageMonitoringSettings, &UStageMonitoringSettings::StaticClass, TEXT("/Script/StageMonitorCommon"), TEXT("UStageMonitoringSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStageMonitoringSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
