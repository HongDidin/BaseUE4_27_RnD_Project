// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StageMonitor/Private/StageMonitorSessionManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStageMonitorSessionManager() {}
// Cross Module References
	STAGEMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FMonitorSessionInfo();
	UPackage* Z_Construct_UPackage__Script_StageMonitor();
// End Cross Module References
class UScriptStruct* FMonitorSessionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STAGEMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FMonitorSessionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMonitorSessionInfo, Z_Construct_UPackage__Script_StageMonitor(), TEXT("MonitorSessionInfo"), sizeof(FMonitorSessionInfo), Get_Z_Construct_UScriptStruct_FMonitorSessionInfo_Hash());
	}
	return Singleton;
}
template<> STAGEMONITOR_API UScriptStruct* StaticStruct<FMonitorSessionInfo>()
{
	return FMonitorSessionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMonitorSessionInfo(FMonitorSessionInfo::StaticStruct, TEXT("/Script/StageMonitor"), TEXT("MonitorSessionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_StageMonitor_StaticRegisterNativesFMonitorSessionInfo
{
	FScriptStruct_StageMonitor_StaticRegisterNativesFMonitorSessionInfo()
	{
		UScriptStruct::DeferCppStructOps<FMonitorSessionInfo>(FName(TEXT("MonitorSessionInfo")));
	}
} ScriptStruct_StageMonitor_StaticRegisterNativesFMonitorSessionInfo;
	struct Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Header written at beginning of json file to have an idea of what's in the file\n */" },
		{ "ModuleRelativePath", "Private/StageMonitorSessionManager.h" },
		{ "ToolTip", "Header written at beginning of json file to have an idea of what's in the file" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMonitorSessionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::NewProp_Version_MetaData[] = {
		{ "ModuleRelativePath", "Private/StageMonitorSessionManager.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMonitorSessionInfo, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::NewProp_Version_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::NewProp_Version,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_StageMonitor,
		nullptr,
		&NewStructOps,
		"MonitorSessionInfo",
		sizeof(FMonitorSessionInfo),
		alignof(FMonitorSessionInfo),
		Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMonitorSessionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMonitorSessionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_StageMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MonitorSessionInfo"), sizeof(FMonitorSessionInfo), Get_Z_Construct_UScriptStruct_FMonitorSessionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMonitorSessionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMonitorSessionInfo_Hash() { return 675325609U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
