// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLUI_RemoteControlPresetFactory_generated_h
#error "RemoteControlPresetFactory.generated.h already included, missing '#pragma once' in RemoteControlPresetFactory.h"
#endif
#define REMOTECONTROLUI_RemoteControlPresetFactory_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlPresetFactory(); \
	friend struct Z_Construct_UClass_URemoteControlPresetFactory_Statics; \
public: \
	DECLARE_CLASS(URemoteControlPresetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControlUI"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlPresetFactory)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlPresetFactory(); \
	friend struct Z_Construct_UClass_URemoteControlPresetFactory_Statics; \
public: \
	DECLARE_CLASS(URemoteControlPresetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControlUI"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlPresetFactory)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlPresetFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlPresetFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlPresetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlPresetFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlPresetFactory(URemoteControlPresetFactory&&); \
	NO_API URemoteControlPresetFactory(const URemoteControlPresetFactory&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlPresetFactory(URemoteControlPresetFactory&&); \
	NO_API URemoteControlPresetFactory(const URemoteControlPresetFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlPresetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlPresetFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URemoteControlPresetFactory)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLUI_API UClass* StaticClass<class URemoteControlPresetFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
