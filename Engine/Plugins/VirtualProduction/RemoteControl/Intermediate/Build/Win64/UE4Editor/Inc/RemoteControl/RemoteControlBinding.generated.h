// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlBinding_generated_h
#error "RemoteControlBinding.generated.h already included, missing '#pragma once' in RemoteControlBinding.h"
#endif
#define REMOTECONTROL_RemoteControlBinding_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlBinding(); \
	friend struct Z_Construct_UClass_URemoteControlBinding_Statics; \
public: \
	DECLARE_CLASS(URemoteControlBinding, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlBinding(); \
	friend struct Z_Construct_UClass_URemoteControlBinding_Statics; \
public: \
	DECLARE_CLASS(URemoteControlBinding, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlBinding(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlBinding) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlBinding); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlBinding); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlBinding(URemoteControlBinding&&); \
	NO_API URemoteControlBinding(const URemoteControlBinding&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlBinding(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlBinding(URemoteControlBinding&&); \
	NO_API URemoteControlBinding(const URemoteControlBinding&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlBinding); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlBinding); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlBinding>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlLevelIndependantBinding(); \
	friend struct Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics; \
public: \
	DECLARE_CLASS(URemoteControlLevelIndependantBinding, URemoteControlBinding, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlLevelIndependantBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlLevelIndependantBinding(); \
	friend struct Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics; \
public: \
	DECLARE_CLASS(URemoteControlLevelIndependantBinding, URemoteControlBinding, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlLevelIndependantBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlLevelIndependantBinding(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlLevelIndependantBinding) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlLevelIndependantBinding); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlLevelIndependantBinding); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlLevelIndependantBinding(URemoteControlLevelIndependantBinding&&); \
	NO_API URemoteControlLevelIndependantBinding(const URemoteControlLevelIndependantBinding&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlLevelIndependantBinding(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlLevelIndependantBinding(URemoteControlLevelIndependantBinding&&); \
	NO_API URemoteControlLevelIndependantBinding(const URemoteControlLevelIndependantBinding&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlLevelIndependantBinding); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlLevelIndependantBinding); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlLevelIndependantBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BoundObject() { return STRUCT_OFFSET(URemoteControlLevelIndependantBinding, BoundObject); }


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_55_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_59_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlLevelIndependantBinding>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlLevelDependantBinding(); \
	friend struct Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics; \
public: \
	DECLARE_CLASS(URemoteControlLevelDependantBinding, URemoteControlBinding, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlLevelDependantBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlLevelDependantBinding(); \
	friend struct Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics; \
public: \
	DECLARE_CLASS(URemoteControlLevelDependantBinding, URemoteControlBinding, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlLevelDependantBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlLevelDependantBinding(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlLevelDependantBinding) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlLevelDependantBinding); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlLevelDependantBinding); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlLevelDependantBinding(URemoteControlLevelDependantBinding&&); \
	NO_API URemoteControlLevelDependantBinding(const URemoteControlLevelDependantBinding&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlLevelDependantBinding(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlLevelDependantBinding(URemoteControlLevelDependantBinding&&); \
	NO_API URemoteControlLevelDependantBinding(const URemoteControlLevelDependantBinding&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlLevelDependantBinding); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlLevelDependantBinding); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlLevelDependantBinding)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BoundObjectMap() { return STRUCT_OFFSET(URemoteControlLevelDependantBinding, BoundObjectMap); } \
	FORCEINLINE static uint32 __PPO__LevelWithLastSuccessfulResolve() { return STRUCT_OFFSET(URemoteControlLevelDependantBinding, LevelWithLastSuccessfulResolve); }


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_75_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h_79_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlLevelDependantBinding>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlBinding_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
