// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlPreset.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlPreset() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTarget();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlFunction();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProperty();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlPreset_NoRegister();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlPresetLayout();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlPresetGroup();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCCachedFieldData();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlPreset();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlBinding_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlExposeRegistry_NoRegister();
// End Cross Module References
class UScriptStruct* FRemoteControlTarget::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTarget_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlTarget, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlTarget"), sizeof(FRemoteControlTarget), Get_Z_Construct_UScriptStruct_FRemoteControlTarget_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlTarget>()
{
	return FRemoteControlTarget::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlTarget(FRemoteControlTarget::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlTarget"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTarget
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTarget()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlTarget>(FName(TEXT("RemoteControlTarget")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTarget;
	struct Z_Construct_UScriptStruct_FRemoteControlTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposedFunctions_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposedFunctions_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ExposedFunctions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposedProperties_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposedProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ExposedProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Alias_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Alias;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Bindings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Owner_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Owner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlTarget>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Class_MetaData[] = {
		{ "Comment", "/**\n\x09 * The common class of the target's underlying objects.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The common class of the target's underlying objects." },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTarget, Class), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Class_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions_ElementProp = { "ExposedFunctions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlFunction, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions_MetaData[] = {
		{ "Comment", "/**\n\x09 * The target's exposed functions.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The target's exposed functions." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions = { "ExposedFunctions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTarget, ExposedFunctions), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties_ElementProp = { "ExposedProperties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlProperty, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties_MetaData[] = {
		{ "Comment", "/**\n\x09 * The target's exposed properties.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The target's exposed properties." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties = { "ExposedProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTarget, ExposedProperties), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Alias_MetaData[] = {
		{ "Comment", "/**\n\x09 * The alias for this target.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The alias for this target." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Alias = { "Alias", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTarget, Alias), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Alias_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Alias_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings_Inner = { "Bindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings_MetaData[] = {
		{ "Comment", "/**\n\x09 * The objects bound under the target's alias.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The objects bound under the target's alias." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings = { "Bindings", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTarget, Bindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Owner_MetaData[] = {
		{ "Comment", "/**\n\x09 * The preset that owns this target.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The preset that owns this target." },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Owner = { "Owner", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTarget, Owner), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Owner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Owner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedFunctions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_ExposedProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Alias,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Bindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::NewProp_Owner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlTarget",
		sizeof(FRemoteControlTarget),
		alignof(FRemoteControlTarget),
		Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTarget()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTarget_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlTarget"), sizeof(FRemoteControlTarget), Get_Z_Construct_UScriptStruct_FRemoteControlTarget_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlTarget_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTarget_Hash() { return 4056977365U; }
class UScriptStruct* FRemoteControlPresetLayout::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlPresetLayout, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlPresetLayout"), sizeof(FRemoteControlPresetLayout), Get_Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlPresetLayout>()
{
	return FRemoteControlPresetLayout::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlPresetLayout(FRemoteControlPresetLayout::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlPresetLayout"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlPresetLayout
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlPresetLayout()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlPresetLayout>(FName(TEXT("RemoteControlPresetLayout")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlPresetLayout;
	struct Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Groups_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Groups_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Groups;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Owner_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Owner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Layout that holds groups of fields. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Layout that holds groups of fields." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlPresetLayout>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups_Inner = { "Groups", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlPresetGroup, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups_MetaData[] = {
		{ "Comment", "/** The list of groups under this layout. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The list of groups under this layout." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups = { "Groups", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlPresetLayout, Groups), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Owner_MetaData[] = {
		{ "Comment", "/** The preset that owns this layout. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The preset that owns this layout." },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Owner = { "Owner", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlPresetLayout, Owner), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Owner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Owner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Groups,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::NewProp_Owner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlPresetLayout",
		sizeof(FRemoteControlPresetLayout),
		alignof(FRemoteControlPresetLayout),
		Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlPresetLayout()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlPresetLayout"), sizeof(FRemoteControlPresetLayout), Get_Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Hash() { return 3642153491U; }
class UScriptStruct* FRemoteControlPresetGroup::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlPresetGroup, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlPresetGroup"), sizeof(FRemoteControlPresetGroup), Get_Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlPresetGroup>()
{
	return FRemoteControlPresetGroup::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlPresetGroup(FRemoteControlPresetGroup::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlPresetGroup"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlPresetGroup
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlPresetGroup()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlPresetGroup>(FName(TEXT("RemoteControlPresetGroup")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlPresetGroup;
	struct Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Fields_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fields_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Fields;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Represents a group of field and offers operations to operate on the fields inside of that group.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Represents a group of field and offers operations to operate on the fields inside of that group." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlPresetGroup>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/** Name of this group. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Name of this group." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlPresetGroup, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Id_MetaData[] = {
		{ "Comment", "/** This group's ID. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "This group's ID." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlPresetGroup, Id), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields_Inner = { "Fields", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields_MetaData[] = {
		{ "Comment", "/** The list of exposed fields under this group. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The list of exposed fields under this group." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields = { "Fields", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlPresetGroup, Fields), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::NewProp_Fields,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlPresetGroup",
		sizeof(FRemoteControlPresetGroup),
		alignof(FRemoteControlPresetGroup),
		Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlPresetGroup()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlPresetGroup"), sizeof(FRemoteControlPresetGroup), Get_Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Hash() { return 1770309145U; }
class UScriptStruct* FRCCachedFieldData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCCachedFieldData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCCachedFieldData, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCCachedFieldData"), sizeof(FRCCachedFieldData), Get_Z_Construct_UScriptStruct_FRCCachedFieldData_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCCachedFieldData>()
{
	return FRCCachedFieldData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCCachedFieldData(FRCCachedFieldData::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCCachedFieldData"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCCachedFieldData
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCCachedFieldData()
	{
		UScriptStruct::DeferCppStructOps<FRCCachedFieldData>(FName(TEXT("RCCachedFieldData")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCCachedFieldData;
	struct Z_Construct_UScriptStruct_FRCCachedFieldData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayoutGroupId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LayoutGroupId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerObjectAlias_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_OwnerObjectAlias;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Data cached for every exposed field.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Data cached for every exposed field." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCCachedFieldData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_LayoutGroupId_MetaData[] = {
		{ "Comment", "/** The group the field is in. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The group the field is in." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_LayoutGroupId = { "LayoutGroupId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCCachedFieldData, LayoutGroupId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_LayoutGroupId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_LayoutGroupId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_OwnerObjectAlias_MetaData[] = {
		{ "Comment", "/** The target that owns this field. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The target that owns this field." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_OwnerObjectAlias = { "OwnerObjectAlias", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCCachedFieldData, OwnerObjectAlias), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_OwnerObjectAlias_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_OwnerObjectAlias_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_LayoutGroupId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::NewProp_OwnerObjectAlias,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RCCachedFieldData",
		sizeof(FRCCachedFieldData),
		alignof(FRCCachedFieldData),
		Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCCachedFieldData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCCachedFieldData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCCachedFieldData"), sizeof(FRCCachedFieldData), Get_Z_Construct_UScriptStruct_FRCCachedFieldData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCCachedFieldData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCCachedFieldData_Hash() { return 1210088588U; }
	void URemoteControlPreset::StaticRegisterNativesURemoteControlPreset()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlPreset_NoRegister()
	{
		return URemoteControlPreset::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlPreset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Layout_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Layout;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Bindings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PresetId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemoteControlTargets_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RemoteControlTargets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RemoteControlTargets;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FieldCache_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FieldCache_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FieldCache;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NameToGuidMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameToGuidMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameToGuidMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_NameToGuidMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Registry_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Registry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlPreset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Holds targets that contain exposed functions and properties.\n */" },
		{ "IncludePath", "RemoteControlPreset.h" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Holds targets that contain exposed functions and properties." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Layout_MetaData[] = {
		{ "Comment", "/** The visual layout for this preset. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The visual layout for this preset." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Layout = { "Layout", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, Layout), Z_Construct_UScriptStruct_FRemoteControlPresetLayout, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Layout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Layout_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_MetaData[] = {
		{ "Comment", "/** This preset's metadata. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "This preset's metadata." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings_Inner = { "Bindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_URemoteControlBinding_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings_MetaData[] = {
		{ "Category", "Remote Control Preset" },
		{ "Comment", "/** This preset's list of objects that are exposed or that have exposed fields. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "This preset's list of objects that are exposed or that have exposed fields." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings = { "Bindings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, Bindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_PresetId_MetaData[] = {
		{ "Comment", "/** Preset unique ID */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Preset unique ID" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_PresetId = { "PresetId", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, PresetId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_PresetId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_PresetId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_ValueProp = { "RemoteControlTargets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FRemoteControlTarget, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_Key_KeyProp = { "RemoteControlTargets_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_MetaData[] = {
		{ "Comment", "/** The mappings of alias to targets. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The mappings of alias to targets." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets = { "RemoteControlTargets", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, RemoteControlTargets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_ValueProp = { "FieldCache", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FRCCachedFieldData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_Key_KeyProp = { "FieldCache_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_MetaData[] = {
		{ "Comment", "/** The cache for information about an exposed field. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "The cache for information about an exposed field." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache = { "FieldCache", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, FieldCache), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_ValueProp = { "NameToGuidMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_Key_KeyProp = { "NameToGuidMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_MetaData[] = {
		{ "Comment", "/** Map of Field Name to GUID. */" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Map of Field Name to GUID." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap = { "NameToGuidMap", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, NameToGuidMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Registry_MetaData[] = {
		{ "Comment", "/** Holds exposed entities on the preset. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RemoteControlPreset.h" },
		{ "ToolTip", "Holds exposed entities on the preset." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Registry = { "Registry", nullptr, (EPropertyFlags)0x0042000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlPreset, Registry), Z_Construct_UClass_URemoteControlExposeRegistry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Registry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Registry_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlPreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Layout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Metadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Bindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_PresetId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_RemoteControlTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_FieldCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_NameToGuidMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlPreset_Statics::NewProp_Registry,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlPreset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlPreset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlPreset_Statics::ClassParams = {
		&URemoteControlPreset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlPreset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlPreset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPreset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlPreset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlPreset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlPreset, 965126713);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlPreset>()
	{
		return URemoteControlPreset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlPreset(Z_Construct_UClass_URemoteControlPreset, &URemoteControlPreset::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlPreset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlPreset);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(URemoteControlPreset)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
