// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLUI_RemoteControlPresetActorFactory_generated_h
#error "RemoteControlPresetActorFactory.generated.h already included, missing '#pragma once' in RemoteControlPresetActorFactory.h"
#endif
#define REMOTECONTROLUI_RemoteControlPresetActorFactory_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlPresetActorFactory(); \
	friend struct Z_Construct_UClass_URemoteControlPresetActorFactory_Statics; \
public: \
	DECLARE_CLASS(URemoteControlPresetActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlUI"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlPresetActorFactory)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlPresetActorFactory(); \
	friend struct Z_Construct_UClass_URemoteControlPresetActorFactory_Statics; \
public: \
	DECLARE_CLASS(URemoteControlPresetActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlUI"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlPresetActorFactory)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlPresetActorFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlPresetActorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlPresetActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlPresetActorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlPresetActorFactory(URemoteControlPresetActorFactory&&); \
	NO_API URemoteControlPresetActorFactory(const URemoteControlPresetActorFactory&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlPresetActorFactory(URemoteControlPresetActorFactory&&); \
	NO_API URemoteControlPresetActorFactory(const URemoteControlPresetActorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlPresetActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlPresetActorFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URemoteControlPresetActorFactory)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLUI_API UClass* StaticClass<class URemoteControlPresetActorFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlUI_Private_Factories_RemoteControlPresetActorFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
