// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlExposeRegistry_generated_h
#error "RemoteControlExposeRegistry.generated.h already included, missing '#pragma once' in RemoteControlExposeRegistry.h"
#endif
#define REMOTECONTROL_RemoteControlExposeRegistry_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCEntityWrapper_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCEntityWrapper>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlExposeRegistry(); \
	friend struct Z_Construct_UClass_URemoteControlExposeRegistry_Statics; \
public: \
	DECLARE_CLASS(URemoteControlExposeRegistry, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlExposeRegistry)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlExposeRegistry(); \
	friend struct Z_Construct_UClass_URemoteControlExposeRegistry_Statics; \
public: \
	DECLARE_CLASS(URemoteControlExposeRegistry, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlExposeRegistry)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlExposeRegistry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlExposeRegistry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlExposeRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlExposeRegistry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlExposeRegistry(URemoteControlExposeRegistry&&); \
	NO_API URemoteControlExposeRegistry(const URemoteControlExposeRegistry&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlExposeRegistry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlExposeRegistry(URemoteControlExposeRegistry&&); \
	NO_API URemoteControlExposeRegistry(const URemoteControlExposeRegistry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlExposeRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlExposeRegistry); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlExposeRegistry)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ExposedEntities() { return STRUCT_OFFSET(URemoteControlExposeRegistry, ExposedEntities); } \
	FORCEINLINE static uint32 __PPO__LabelToIdCache() { return STRUCT_OFFSET(URemoteControlExposeRegistry, LabelToIdCache); }


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_63_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h_67_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlExposeRegistry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_RemoteControlExposeRegistry_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
