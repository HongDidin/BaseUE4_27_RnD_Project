// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlFieldMetadata_generated_h
#error "RemoteControlFieldMetadata.generated.h already included, missing '#pragma once' in RemoteControlFieldMetadata.h"
#endif
#define REMOTECONTROL_RemoteControlFieldMetadata_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_226_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_FVector>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_217_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_bool_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_bool>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_208_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_UScriptStruct>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_199_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_UClass>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_190_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_UObject>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_181_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_FName_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_FName>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_172_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_FString_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_FString>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_157_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_double_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_double>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_142_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_float_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_float>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_127_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_int64_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_int64>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_112_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_int32_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_int32>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_97_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_int16_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_int16>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_82_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_int8_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_int8>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_67_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_uint64>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_52_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_uint32>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_uint16>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCMetadata_byte_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FRCFieldMetadata Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCMetadata_byte>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCFieldMetadata_Statics; \
	static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCFieldMetadata>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldMetadata_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
