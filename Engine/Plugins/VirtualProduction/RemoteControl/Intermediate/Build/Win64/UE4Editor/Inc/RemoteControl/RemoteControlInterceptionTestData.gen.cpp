// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Private/Tests/RemoteControlInterceptionTestData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlInterceptionTestData() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlInterceptionTestObject_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlInterceptionTestObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FRemoteControlInterceptionFunctionParamStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlInterceptionFunctionParamStruct"), sizeof(FRemoteControlInterceptionFunctionParamStruct), Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlInterceptionFunctionParamStruct>()
{
	return FRemoteControlInterceptionFunctionParamStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct(FRemoteControlInterceptionFunctionParamStruct::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlInterceptionFunctionParamStruct"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlInterceptionFunctionParamStruct
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlInterceptionFunctionParamStruct()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlInterceptionFunctionParamStruct>(FName(TEXT("RemoteControlInterceptionFunctionParamStruct")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlInterceptionFunctionParamStruct;
	struct Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Value;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IntString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlInterceptionFunctionParamStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_Int32Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_Int32Value = { "Int32Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlInterceptionFunctionParamStruct, Int32Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_Int32Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_Int32Value_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray_Inner = { "IntArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray = { "IntArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlInterceptionFunctionParamStruct, IntArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntString_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntString = { "IntString", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlInterceptionFunctionParamStruct, IntString), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntString_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_Int32Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::NewProp_IntString,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlInterceptionFunctionParamStruct",
		sizeof(FRemoteControlInterceptionFunctionParamStruct),
		alignof(FRemoteControlInterceptionFunctionParamStruct),
		Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlInterceptionFunctionParamStruct"), sizeof(FRemoteControlInterceptionFunctionParamStruct), Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Hash() { return 1925957091U; }
class UScriptStruct* FRemoteControlInterceptionTestStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlInterceptionTestStruct"), sizeof(FRemoteControlInterceptionTestStruct), Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlInterceptionTestStruct>()
{
	return FRemoteControlInterceptionTestStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlInterceptionTestStruct(FRemoteControlInterceptionTestStruct::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlInterceptionTestStruct"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlInterceptionTestStruct
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlInterceptionTestStruct()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlInterceptionTestStruct>(FName(TEXT("RemoteControlInterceptionTestStruct")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlInterceptionTestStruct;
	struct Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlInterceptionTestStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::NewProp_Int32Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::NewProp_Int32Value = { "Int32Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlInterceptionTestStruct, Int32Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::NewProp_Int32Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::NewProp_Int32Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::NewProp_Int32Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlInterceptionTestStruct",
		sizeof(FRemoteControlInterceptionTestStruct),
		alignof(FRemoteControlInterceptionTestStruct),
		Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlInterceptionTestStruct"), sizeof(FRemoteControlInterceptionTestStruct), Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Hash() { return 3733215183U; }
	DEFINE_FUNCTION(URemoteControlInterceptionTestObject::execTestFunction)
	{
		P_GET_STRUCT_REF(FRemoteControlInterceptionFunctionParamStruct,Z_Param_Out_InStruct);
		P_GET_PROPERTY(FIntProperty,Z_Param_InTestFactor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FRemoteControlInterceptionFunctionParamStruct*)Z_Param__Result=P_THIS->TestFunction(Z_Param_Out_InStruct,Z_Param_InTestFactor);
		P_NATIVE_END;
	}
	void URemoteControlInterceptionTestObject::StaticRegisterNativesURemoteControlInterceptionTestObject()
	{
		UClass* Class = URemoteControlInterceptionTestObject::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TestFunction", &URemoteControlInterceptionTestObject::execTestFunction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics
	{
		struct RemoteControlInterceptionTestObject_eventTestFunction_Parms
		{
			FRemoteControlInterceptionFunctionParamStruct InStruct;
			int32 InTestFactor;
			FRemoteControlInterceptionFunctionParamStruct ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InTestFactor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlInterceptionTestObject_eventTestFunction_Parms, InStruct), Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct, METADATA_PARAMS(Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InStruct_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InTestFactor = { "InTestFactor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlInterceptionTestObject_eventTestFunction_Parms, InTestFactor), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlInterceptionTestObject_eventTestFunction_Parms, ReturnValue), Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_InTestFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::Function_MetaDataParams[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URemoteControlInterceptionTestObject, nullptr, "TestFunction", nullptr, nullptr, sizeof(RemoteControlInterceptionTestObject_eventTestFunction_Parms), Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URemoteControlInterceptionTestObject_NoRegister()
	{
		return URemoteControlInterceptionTestObject::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionParamStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionParamStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URemoteControlInterceptionTestObject_TestFunction, "TestFunction" }, // 3971839721
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/RemoteControlInterceptionTestData.h" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_CustomStruct_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_CustomStruct = { "CustomStruct", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlInterceptionTestObject, CustomStruct), Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct, METADATA_PARAMS(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_CustomStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_CustomStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_FunctionParamStruct_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlInterceptionTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_FunctionParamStruct = { "FunctionParamStruct", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlInterceptionTestObject, FunctionParamStruct), Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct, METADATA_PARAMS(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_FunctionParamStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_FunctionParamStruct_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_CustomStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::NewProp_FunctionParamStruct,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlInterceptionTestObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::ClassParams = {
		&URemoteControlInterceptionTestObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlInterceptionTestObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlInterceptionTestObject, 444398260);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlInterceptionTestObject>()
	{
		return URemoteControlInterceptionTestObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlInterceptionTestObject(Z_Construct_UClass_URemoteControlInterceptionTestObject, &URemoteControlInterceptionTestObject::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlInterceptionTestObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlInterceptionTestObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
