// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlPreset_generated_h
#error "RemoteControlPreset.generated.h already included, missing '#pragma once' in RemoteControlPreset.h"
#endif
#define REMOTECONTROL_RemoteControlPreset_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_277_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlTarget_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Bindings() { return STRUCT_OFFSET(FRemoteControlTarget, Bindings); } \
	FORCEINLINE static uint32 __PPO__Owner() { return STRUCT_OFFSET(FRemoteControlTarget, Owner); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlTarget>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_134_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlPresetLayout_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Groups() { return STRUCT_OFFSET(FRemoteControlPresetLayout, Groups); } \
	FORCEINLINE static uint32 __PPO__Owner() { return STRUCT_OFFSET(FRemoteControlPresetLayout, Owner); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlPresetLayout>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_95_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlPresetGroup_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Fields() { return STRUCT_OFFSET(FRemoteControlPresetGroup, Fields); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlPresetGroup>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCCachedFieldData_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCCachedFieldData>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(URemoteControlPreset, NO_API)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlPreset(); \
	friend struct Z_Construct_UClass_URemoteControlPreset_Statics; \
public: \
	DECLARE_CLASS(URemoteControlPreset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlPreset) \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_ARCHIVESERIALIZER


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlPreset(); \
	friend struct Z_Construct_UClass_URemoteControlPreset_Statics; \
public: \
	DECLARE_CLASS(URemoteControlPreset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlPreset) \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_ARCHIVESERIALIZER


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlPreset(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlPreset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlPreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlPreset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlPreset(URemoteControlPreset&&); \
	NO_API URemoteControlPreset(const URemoteControlPreset&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlPreset(URemoteControlPreset&&); \
	NO_API URemoteControlPreset(const URemoteControlPreset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlPreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlPreset); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URemoteControlPreset)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PresetId() { return STRUCT_OFFSET(URemoteControlPreset, PresetId); } \
	FORCEINLINE static uint32 __PPO__RemoteControlTargets() { return STRUCT_OFFSET(URemoteControlPreset, RemoteControlTargets); } \
	FORCEINLINE static uint32 __PPO__FieldCache() { return STRUCT_OFFSET(URemoteControlPreset, FieldCache); } \
	FORCEINLINE static uint32 __PPO__NameToGuidMap() { return STRUCT_OFFSET(URemoteControlPreset, NameToGuidMap); } \
	FORCEINLINE static uint32 __PPO__Registry() { return STRUCT_OFFSET(URemoteControlPreset, Registry); }


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_445_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h_449_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlPreset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPreset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
