// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Private/Tests/RemoteControlPropertyHandleTestData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlPropertyHandleTestData() {}
// Cross Module References
	REMOTECONTROL_API UEnum* Z_Construct_UEnum_RemoteControl_ERemoteControlEnum();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UEnum* Z_Construct_UEnum_RemoteControl_ERemoteControlEnumClass();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTestStructOuter();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTestStructInner();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlAPITestObject_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlAPITestObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
// End Cross Module References
	static UEnum* ERemoteControlEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteControl_ERemoteControlEnum, Z_Construct_UPackage__Script_RemoteControl(), TEXT("ERemoteControlEnum"));
		}
		return Singleton;
	}
	template<> REMOTECONTROL_API UEnum* StaticEnum<ERemoteControlEnum::Type>()
	{
		return ERemoteControlEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteControlEnum(ERemoteControlEnum_StaticEnum, TEXT("/Script/RemoteControl"), TEXT("ERemoteControlEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteControl_ERemoteControlEnum_Hash() { return 173556494U; }
	UEnum* Z_Construct_UEnum_RemoteControl_ERemoteControlEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteControlEnum"), 0, Get_Z_Construct_UEnum_RemoteControl_ERemoteControlEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteControlEnum::E_One", (int64)ERemoteControlEnum::E_One },
				{ "ERemoteControlEnum::E_Two", (int64)ERemoteControlEnum::E_Two },
				{ "ERemoteControlEnum::E_Three", (int64)ERemoteControlEnum::E_Three },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "E_One.Name", "ERemoteControlEnum::E_One" },
				{ "E_Three.Name", "ERemoteControlEnum::E_Three" },
				{ "E_Two.Name", "ERemoteControlEnum::E_Two" },
				{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteControl,
				nullptr,
				"ERemoteControlEnum",
				"ERemoteControlEnum::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERemoteControlEnumClass_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteControl_ERemoteControlEnumClass, Z_Construct_UPackage__Script_RemoteControl(), TEXT("ERemoteControlEnumClass"));
		}
		return Singleton;
	}
	template<> REMOTECONTROL_API UEnum* StaticEnum<ERemoteControlEnumClass>()
	{
		return ERemoteControlEnumClass_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteControlEnumClass(ERemoteControlEnumClass_StaticEnum, TEXT("/Script/RemoteControl"), TEXT("ERemoteControlEnumClass"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteControl_ERemoteControlEnumClass_Hash() { return 2189347807U; }
	UEnum* Z_Construct_UEnum_RemoteControl_ERemoteControlEnumClass()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteControlEnumClass"), 0, Get_Z_Construct_UEnum_RemoteControl_ERemoteControlEnumClass_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteControlEnumClass::E_One", (int64)ERemoteControlEnumClass::E_One },
				{ "ERemoteControlEnumClass::E_Two", (int64)ERemoteControlEnumClass::E_Two },
				{ "ERemoteControlEnumClass::E_Three", (int64)ERemoteControlEnumClass::E_Three },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "E_One.Name", "ERemoteControlEnumClass::E_One" },
				{ "E_Three.Name", "ERemoteControlEnumClass::E_Three" },
				{ "E_Two.Name", "ERemoteControlEnumClass::E_Two" },
				{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteControl,
				nullptr,
				"ERemoteControlEnumClass",
				"ERemoteControlEnumClass",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRemoteControlTestStructOuter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlTestStructOuter"), sizeof(FRemoteControlTestStructOuter), Get_Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlTestStructOuter>()
{
	return FRemoteControlTestStructOuter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlTestStructOuter(FRemoteControlTestStructOuter::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlTestStructOuter"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructOuter
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructOuter()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlTestStructOuter>(FName(TEXT("RemoteControlTestStructOuter")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructOuter;
	struct Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Value;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructInnerSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructInnerSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_StructInnerSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlTestStructInner_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemoteControlTestStructInner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlTestStructOuter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int8Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int8Value = { "Int8Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructOuter, Int8Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int8Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int8Value_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet_ElementProp = { "StructInnerSet", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlTestStructInner, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet = { "StructInnerSet", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructOuter, StructInnerSet), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int32Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int32Value = { "Int32Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructOuter, Int32Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int32Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int32Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_RemoteControlTestStructInner_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_RemoteControlTestStructInner = { "RemoteControlTestStructInner", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructOuter, RemoteControlTestStructInner), Z_Construct_UScriptStruct_FRemoteControlTestStructInner, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_RemoteControlTestStructInner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_RemoteControlTestStructInner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int8Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_StructInnerSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_Int32Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::NewProp_RemoteControlTestStructInner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlTestStructOuter",
		sizeof(FRemoteControlTestStructOuter),
		alignof(FRemoteControlTestStructOuter),
		Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTestStructOuter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlTestStructOuter"), sizeof(FRemoteControlTestStructOuter), Get_Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Hash() { return 3727552756U; }
class UScriptStruct* FRemoteControlTestStructInner::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlTestStructInner, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlTestStructInner"), sizeof(FRemoteControlTestStructInner), Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlTestStructInner>()
{
	return FRemoteControlTestStructInner::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlTestStructInner(FRemoteControlTestStructInner::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlTestStructInner"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructInner
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructInner()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlTestStructInner>(FName(TEXT("RemoteControlTestStructInner")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructInner;
	struct Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerSimle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InnerSimle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlTestStructInner>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int8Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int8Value = { "Int8Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructInner, Int8Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int8Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int8Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_InnerSimle_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_InnerSimle = { "InnerSimle", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructInner, InnerSimle), Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_InnerSimle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_InnerSimle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int32Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int32Value = { "Int32Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructInner, Int32Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int32Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int32Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int8Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_InnerSimle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::NewProp_Int32Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlTestStructInner",
		sizeof(FRemoteControlTestStructInner),
		alignof(FRemoteControlTestStructInner),
		Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTestStructInner()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlTestStructInner"), sizeof(FRemoteControlTestStructInner), Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Hash() { return 2133231718U; }
class UScriptStruct* FRemoteControlTestStructInnerSimle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlTestStructInnerSimle"), sizeof(FRemoteControlTestStructInnerSimle), Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlTestStructInnerSimle>()
{
	return FRemoteControlTestStructInnerSimle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlTestStructInnerSimle(FRemoteControlTestStructInnerSimle::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlTestStructInnerSimle"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructInnerSimle
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructInnerSimle()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlTestStructInnerSimle>(FName(TEXT("RemoteControlTestStructInnerSimle")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlTestStructInnerSimle;
	struct Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlTestStructInnerSimle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::NewProp_Int32Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::NewProp_Int32Value = { "Int32Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlTestStructInnerSimle, Int32Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::NewProp_Int32Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::NewProp_Int32Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::NewProp_Int32Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlTestStructInnerSimle",
		sizeof(FRemoteControlTestStructInnerSimle),
		alignof(FRemoteControlTestStructInnerSimle),
		Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlTestStructInnerSimle"), sizeof(FRemoteControlTestStructInnerSimle), Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Hash() { return 528584869U; }
	void URemoteControlAPITestObject::StaticRegisterNativesURemoteControlAPITestObject()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlAPITestObject_NoRegister()
	{
		return URemoteControlAPITestObject::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlAPITestObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CStyleIntArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CStyleIntArray;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntArray;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructOuterArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructOuterArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StructOuterArray;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_IntSet;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntMap_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_IntMap;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructOuterMap_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StructOuterMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructOuterMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_StructOuterMap;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StringColorMap_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringColorMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringColorMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_StringColorMap;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ArrayOfVectors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArrayOfVectors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ArrayOfVectors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int16Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Int16Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DoubleValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlTestStructOuter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemoteControlTestStructOuter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bValue_MetaData[];
#endif
		static void NewProp_bValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlEnumByteValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RemoteControlEnumByteValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RemoteControlEnumValue_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlEnumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RemoteControlEnumValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VectorValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VectorValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotatorValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotatorValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlAPITestObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tests/RemoteControlPropertyHandleTestData.h" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_CStyleIntArray_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_CStyleIntArray = { "CStyleIntArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(CStyleIntArray, URemoteControlAPITestObject), STRUCT_OFFSET(URemoteControlAPITestObject, CStyleIntArray), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_CStyleIntArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_CStyleIntArray_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray_Inner = { "IntArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray = { "IntArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, IntArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray_Inner = { "StructOuterArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlTestStructOuter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray = { "StructOuterArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, StructOuterArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet_ElementProp = { "IntSet", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet = { "IntSet", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, IntSet), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_ValueProp = { "IntMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_Key_KeyProp = { "IntMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap = { "IntMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, IntMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_ValueProp = { "StructOuterMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FRemoteControlTestStructOuter, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_Key_KeyProp = { "StructOuterMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap = { "StructOuterMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, StructOuterMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_ValueProp = { "StringColorMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_Key_KeyProp = { "StringColorMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap = { "StringColorMap", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, StringColorMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors_Inner = { "ArrayOfVectors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors = { "ArrayOfVectors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, ArrayOfVectors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int8Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int8Value = { "Int8Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, Int8Value), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int8Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int8Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int16Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int16Value = { "Int16Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, Int16Value), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int16Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int16Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int32Value_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int32Value = { "Int32Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, Int32Value), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int32Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int32Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_FloatValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_FloatValue = { "FloatValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, FloatValue), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_FloatValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_FloatValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_DoubleValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_DoubleValue = { "DoubleValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, DoubleValue), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_DoubleValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_DoubleValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlTestStructOuter_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlTestStructOuter = { "RemoteControlTestStructOuter", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, RemoteControlTestStructOuter), Z_Construct_UScriptStruct_FRemoteControlTestStructOuter, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlTestStructOuter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlTestStructOuter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringValue = { "StringValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, StringValue), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_NameValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_NameValue = { "NameValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, NameValue), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_NameValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_NameValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_TextValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_TextValue = { "TextValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, TextValue), METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_TextValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_TextValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	void Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue_SetBit(void* Obj)
	{
		((URemoteControlAPITestObject*)Obj)->bValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue = { "bValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteControlAPITestObject), &Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ByteValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ByteValue = { "ByteValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, ByteValue), nullptr, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ByteValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ByteValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumByteValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumByteValue = { "RemoteControlEnumByteValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, RemoteControlEnumByteValue), Z_Construct_UEnum_RemoteControl_ERemoteControlEnum, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumByteValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumByteValue_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue = { "RemoteControlEnumValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, RemoteControlEnumValue), Z_Construct_UEnum_RemoteControl_ERemoteControlEnumClass, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_VectorValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_VectorValue = { "VectorValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, VectorValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_VectorValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_VectorValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RotatorValue_MetaData[] = {
		{ "Category", "RC" },
		{ "ModuleRelativePath", "Private/Tests/RemoteControlPropertyHandleTestData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RotatorValue = { "RotatorValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlAPITestObject, RotatorValue), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RotatorValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RotatorValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlAPITestObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_CStyleIntArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_IntMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StructOuterMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringColorMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ArrayOfVectors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int8Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int16Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_Int32Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_FloatValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_DoubleValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlTestStructOuter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_StringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_NameValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_TextValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_bValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_ByteValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumByteValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RemoteControlEnumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_VectorValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlAPITestObject_Statics::NewProp_RotatorValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlAPITestObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlAPITestObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlAPITestObject_Statics::ClassParams = {
		&URemoteControlAPITestObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlAPITestObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlAPITestObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlAPITestObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlAPITestObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlAPITestObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlAPITestObject, 251702047);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlAPITestObject>()
	{
		return URemoteControlAPITestObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlAPITestObject(Z_Construct_UClass_URemoteControlAPITestObject, &URemoteControlAPITestObject::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlAPITestObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlAPITestObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
