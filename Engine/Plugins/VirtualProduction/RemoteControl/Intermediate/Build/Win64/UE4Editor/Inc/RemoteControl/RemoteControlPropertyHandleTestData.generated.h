// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlPropertyHandleTestData_generated_h
#error "RemoteControlPropertyHandleTestData.generated.h already included, missing '#pragma once' in RemoteControlPropertyHandleTestData.h"
#endif
#define REMOTECONTROL_RemoteControlPropertyHandleTestData_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_65_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlTestStructOuter_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlTestStructOuter>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_38_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlTestStructInner_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlTestStructInner>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_29_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlTestStructInnerSimle_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlTestStructInnerSimle>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlAPITestObject(); \
	friend struct Z_Construct_UClass_URemoteControlAPITestObject_Statics; \
public: \
	DECLARE_CLASS(URemoteControlAPITestObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlAPITestObject)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlAPITestObject(); \
	friend struct Z_Construct_UClass_URemoteControlAPITestObject_Statics; \
public: \
	DECLARE_CLASS(URemoteControlAPITestObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlAPITestObject)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlAPITestObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlAPITestObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlAPITestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlAPITestObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlAPITestObject(URemoteControlAPITestObject&&); \
	NO_API URemoteControlAPITestObject(const URemoteControlAPITestObject&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlAPITestObject(URemoteControlAPITestObject&&); \
	NO_API URemoteControlAPITestObject(const URemoteControlAPITestObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlAPITestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlAPITestObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URemoteControlAPITestObject)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_80_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h_84_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlAPITestObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlPropertyHandleTestData_h


#define FOREACH_ENUM_EREMOTECONTROLENUM(op) \
	op(ERemoteControlEnum::E_One) \
	op(ERemoteControlEnum::E_Two) \
	op(ERemoteControlEnum::E_Three) 
#define FOREACH_ENUM_EREMOTECONTROLENUMCLASS(op) \
	op(ERemoteControlEnumClass::E_One) \
	op(ERemoteControlEnumClass::E_Two) \
	op(ERemoteControlEnumClass::E_Three) 

enum class ERemoteControlEnumClass : uint8;
template<> REMOTECONTROL_API UEnum* StaticEnum<ERemoteControlEnumClass>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
