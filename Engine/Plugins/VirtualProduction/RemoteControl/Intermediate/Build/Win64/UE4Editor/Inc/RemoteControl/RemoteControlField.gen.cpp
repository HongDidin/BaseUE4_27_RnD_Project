// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlField.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlField() {}
// Cross Module References
	REMOTECONTROL_API UEnum* Z_Construct_UEnum_RemoteControl_EExposedFieldType();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlFunction();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlField();
	COREUOBJECT_API UClass* Z_Construct_UClass_UFunction();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProperty();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlEntity();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCFieldPathInfo();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolBinding();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	static UEnum* EExposedFieldType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteControl_EExposedFieldType, Z_Construct_UPackage__Script_RemoteControl(), TEXT("EExposedFieldType"));
		}
		return Singleton;
	}
	template<> REMOTECONTROL_API UEnum* StaticEnum<EExposedFieldType>()
	{
		return EExposedFieldType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExposedFieldType(EExposedFieldType_StaticEnum, TEXT("/Script/RemoteControl"), TEXT("EExposedFieldType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteControl_EExposedFieldType_Hash() { return 2673758419U; }
	UEnum* Z_Construct_UEnum_RemoteControl_EExposedFieldType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExposedFieldType"), 0, Get_Z_Construct_UEnum_RemoteControl_EExposedFieldType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExposedFieldType::Invalid", (int64)EExposedFieldType::Invalid },
				{ "EExposedFieldType::Property", (int64)EExposedFieldType::Property },
				{ "EExposedFieldType::Function", (int64)EExposedFieldType::Function },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * The type of the exposed field.\n */" },
				{ "Function.Name", "EExposedFieldType::Function" },
				{ "Invalid.Name", "EExposedFieldType::Invalid" },
				{ "ModuleRelativePath", "Public/RemoteControlField.h" },
				{ "Property.Name", "EExposedFieldType::Property" },
				{ "ToolTip", "The type of the exposed field." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteControl,
				nullptr,
				"EExposedFieldType",
				"EExposedFieldType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FRemoteControlFunction>() == std::is_polymorphic<FRemoteControlField>(), "USTRUCT FRemoteControlFunction cannot be polymorphic unless super FRemoteControlField is polymorphic");

class UScriptStruct* FRemoteControlFunction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlFunction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlFunction, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlFunction"), sizeof(FRemoteControlFunction), Get_Z_Construct_UScriptStruct_FRemoteControlFunction_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlFunction>()
{
	return FRemoteControlFunction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlFunction(FRemoteControlFunction::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlFunction"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlFunction
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlFunction()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlFunction>(FName(TEXT("RemoteControlFunction")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlFunction;
	struct Z_Construct_UScriptStruct_FRemoteControlFunction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Function_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Function;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCallableInPackaged_MetaData[];
#endif
		static void NewProp_bIsCallableInPackaged_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCallableInPackaged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents a function exposed to remote control.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Represents a function exposed to remote control." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlFunction>();
	}
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_Function_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_Function = { "Function", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlFunction, Function_DEPRECATED), Z_Construct_UClass_UFunction, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_Function_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_Function_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged_MetaData[] = {
		{ "Comment", "/** Whether the function is callable in a packaged build. */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Whether the function is callable in a packaged build." },
	};
#endif
	void Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged_SetBit(void* Obj)
	{
		((FRemoteControlFunction*)Obj)->bIsCallableInPackaged = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged = { "bIsCallableInPackaged", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRemoteControlFunction), &Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_FunctionPath_MetaData[] = {
		{ "Comment", "/** The exposed function. */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "The exposed function." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_FunctionPath = { "FunctionPath", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlFunction, FunctionPath), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_FunctionPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_FunctionPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_Function,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_bIsCallableInPackaged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::NewProp_FunctionPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRemoteControlField,
		&NewStructOps,
		"RemoteControlFunction",
		sizeof(FRemoteControlFunction),
		alignof(FRemoteControlFunction),
		Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlFunction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlFunction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlFunction"), sizeof(FRemoteControlFunction), Get_Z_Construct_UScriptStruct_FRemoteControlFunction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlFunction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlFunction_Hash() { return 2113115781U; }

static_assert(std::is_polymorphic<FRemoteControlProperty>() == std::is_polymorphic<FRemoteControlField>(), "USTRUCT FRemoteControlProperty cannot be polymorphic unless super FRemoteControlField is polymorphic");

class UScriptStruct* FRemoteControlProperty::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProperty_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlProperty, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlProperty"), sizeof(FRemoteControlProperty), Get_Z_Construct_UScriptStruct_FRemoteControlProperty_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlProperty>()
{
	return FRemoteControlProperty::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlProperty(FRemoteControlProperty::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlProperty"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProperty
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProperty()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlProperty>(FName(TEXT("RemoteControlProperty")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProperty;
	struct Z_Construct_UScriptStruct_FRemoteControlProperty_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEditableInPackaged_MetaData[];
#endif
		static void NewProp_bIsEditableInPackaged_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEditableInPackaged;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents a property exposed to remote control.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Represents a property exposed to remote control." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlProperty>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged_MetaData[] = {
		{ "Comment", "/** Whether the property is blueprint read only. */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Whether the property is blueprint read only." },
	};
#endif
	void Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged_SetBit(void* Obj)
	{
		((FRemoteControlProperty*)Obj)->bIsEditableInPackaged = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged = { "bIsEditableInPackaged", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRemoteControlProperty), &Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::NewProp_bIsEditableInPackaged,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRemoteControlField,
		&NewStructOps,
		"RemoteControlProperty",
		sizeof(FRemoteControlProperty),
		alignof(FRemoteControlProperty),
		Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProperty()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProperty_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlProperty"), sizeof(FRemoteControlProperty), Get_Z_Construct_UScriptStruct_FRemoteControlProperty_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlProperty_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProperty_Hash() { return 3331436357U; }

static_assert(std::is_polymorphic<FRemoteControlField>() == std::is_polymorphic<FRemoteControlEntity>(), "USTRUCT FRemoteControlField cannot be polymorphic unless super FRemoteControlEntity is polymorphic");

class UScriptStruct* FRemoteControlField::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlField_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlField, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlField"), sizeof(FRemoteControlField), Get_Z_Construct_UScriptStruct_FRemoteControlField_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlField>()
{
	return FRemoteControlField::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlField(FRemoteControlField::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlField"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlField
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlField()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlField>(FName(TEXT("RemoteControlField")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlField;
	struct Z_Construct_UScriptStruct_FRemoteControlField_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FieldType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FieldType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FieldName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldPathInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FieldPathInfo;
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ComponentHierarchy_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentHierarchy_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ComponentHierarchy;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProtocolBindings_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtocolBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ProtocolBindings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEditorOnly_MetaData[];
#endif
		static void NewProp_bIsEditorOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEditorOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents a property or function that has been exposed to remote control.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Represents a property or function that has been exposed to remote control." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlField>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType_MetaData[] = {
		{ "Category", "RemoteControlEntity" },
		{ "Comment", "/**\n\x09 * The field's type.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "The field's type." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType = { "FieldType", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlField, FieldType), Z_Construct_UEnum_RemoteControl_EExposedFieldType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldName_MetaData[] = {
		{ "Category", "RemoteControlEntity" },
		{ "Comment", "/**\n\x09 * The exposed field's name.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "The exposed field's name." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlField, FieldName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldPathInfo_MetaData[] = {
		{ "Comment", "/**\n\x09 * Path information pointing to this field\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Path information pointing to this field" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldPathInfo = { "FieldPathInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlField, FieldPathInfo), Z_Construct_UScriptStruct_FRCFieldPathInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldPathInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldPathInfo_MetaData)) };
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy_Inner = { "ComponentHierarchy", nullptr, (EPropertyFlags)0x0000000820000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy = { "ComponentHierarchy", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlField, ComponentHierarchy_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings_ElementProp = { "ProtocolBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlProtocolBinding, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings_MetaData[] = {
		{ "Comment", "/**\n\x09 * Stores the bound protocols for this exposed field\n\x09 * It could store any types of the implemented protocols such as DMX, OSC, MIDI, etc\n\x09 * The map holds protocol bindings stores the protocol mapping and protocol-specific mapping\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Stores the bound protocols for this exposed field\nIt could store any types of the implemented protocols such as DMX, OSC, MIDI, etc\nThe map holds protocol bindings stores the protocol mapping and protocol-specific mapping" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings = { "ProtocolBindings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlField, ProtocolBindings), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_OwnerClass_MetaData[] = {
		{ "Comment", "/**\n\x09 * The class of the object that can have this field.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "The class of the object that can have this field." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_OwnerClass = { "OwnerClass", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlField, OwnerClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_OwnerClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_OwnerClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly_MetaData[] = {
		{ "Comment", "/** Whether the field is only available in editor. */" },
		{ "ModuleRelativePath", "Public/RemoteControlField.h" },
		{ "ToolTip", "Whether the field is only available in editor." },
	};
#endif
	void Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly_SetBit(void* Obj)
	{
		((FRemoteControlField*)Obj)->bIsEditorOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly = { "bIsEditorOnly", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRemoteControlField), &Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_FieldPathInfo,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ComponentHierarchy,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_ProtocolBindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_OwnerClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlField_Statics::NewProp_bIsEditorOnly,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlField_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRemoteControlEntity,
		&NewStructOps,
		"RemoteControlField",
		sizeof(FRemoteControlField),
		alignof(FRemoteControlField),
		Z_Construct_UScriptStruct_FRemoteControlField_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlField_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlField_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlField()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlField_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlField"), sizeof(FRemoteControlField), Get_Z_Construct_UScriptStruct_FRemoteControlField_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlField_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlField_Hash() { return 1650904566U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
