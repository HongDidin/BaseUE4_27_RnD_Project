// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlUI/Private/Factories/RemoteControlPresetFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlPresetFactory() {}
// Cross Module References
	REMOTECONTROLUI_API UClass* Z_Construct_UClass_URemoteControlPresetFactory_NoRegister();
	REMOTECONTROLUI_API UClass* Z_Construct_UClass_URemoteControlPresetFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_RemoteControlUI();
// End Cross Module References
	void URemoteControlPresetFactory::StaticRegisterNativesURemoteControlPresetFactory()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlPresetFactory_NoRegister()
	{
		return URemoteControlPresetFactory::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlPresetFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlPresetFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPresetFactory_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Implements a factory for URemoteControlPreset objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/RemoteControlPresetFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/RemoteControlPresetFactory.h" },
		{ "ToolTip", "Implements a factory for URemoteControlPreset objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlPresetFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlPresetFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlPresetFactory_Statics::ClassParams = {
		&URemoteControlPresetFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlPresetFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPresetFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlPresetFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlPresetFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlPresetFactory, 1395678770);
	template<> REMOTECONTROLUI_API UClass* StaticClass<URemoteControlPresetFactory>()
	{
		return URemoteControlPresetFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlPresetFactory(Z_Construct_UClass_URemoteControlPresetFactory, &URemoteControlPresetFactory::StaticClass, TEXT("/Script/RemoteControlUI"), TEXT("URemoteControlPresetFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlPresetFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
