// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlFieldPath.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlFieldPath() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCFieldPathInfo();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCFieldPathSegment();
// End Cross Module References
class UScriptStruct* FRCFieldPathInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCFieldPathInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCFieldPathInfo, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCFieldPathInfo"), sizeof(FRCFieldPathInfo), Get_Z_Construct_UScriptStruct_FRCFieldPathInfo_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCFieldPathInfo>()
{
	return FRCFieldPathInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCFieldPathInfo(FRCFieldPathInfo::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCFieldPathInfo"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldPathInfo
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldPathInfo()
	{
		UScriptStruct::DeferCppStructOps<FRCFieldPathInfo>(FName(TEXT("RCFieldPathInfo")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldPathInfo;
	struct Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Segments_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Segments_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Segments;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathHash_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_PathHash;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a path from a UObject to a field.\n * Has facilities to resolve for a given owner.\n *\n * Example Usage Create a path to relative location's x value, then resolve it on an static mesh component.\n * FRCFieldPathInfo Path(\"RelativeLocation.X\"));\n * bool bResolved = Path.Resolve(MyStaticMeshComponent);\n * if (bResolved)\n * {\n *   FRCFieldResolvedData Data = Path.GetResolvedData();\n *   // Data.ContainerAddress corresponds to &MyStaticMeshComponent.RelativeLocation\n *   // Data.Field corresponds to FFloatProperty (X) \n *   // Data.Struct corresponds to FVector\n * }\n * \n * Other example paths:\n * \"MyStructProperty.NestedArrayProperty[3]\"\n * \"RelativeLocation\"\n * \"RelativeLocation.X\"\n * \n * Supports array/set/map indexing.\n * @Note Only String keys are currently supported for map key indexing.\n * ie. MyStructProperty.MyStringToVectorMap[\"MyKey\"].X\n * Be aware that MyStringToVectorMap[2] will not correspond to the key 2 in the map, but to the index 2 of the map.\n * This behaviour is intended to match PropertyHandle's GeneratePathToProperty method.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "Holds a path from a UObject to a field.\nHas facilities to resolve for a given owner.\n\nExample Usage Create a path to relative location's x value, then resolve it on an static mesh component.\nFRCFieldPathInfo Path(\"RelativeLocation.X\"));\nbool bResolved = Path.Resolve(MyStaticMeshComponent);\nif (bResolved)\n{\n  FRCFieldResolvedData Data = Path.GetResolvedData();\n  // Data.ContainerAddress corresponds to &MyStaticMeshComponent.RelativeLocation\n  // Data.Field corresponds to FFloatProperty (X)\n  // Data.Struct corresponds to FVector\n}\n\nOther example paths:\n\"MyStructProperty.NestedArrayProperty[3]\"\n\"RelativeLocation\"\n\"RelativeLocation.X\"\n\nSupports array/set/map indexing.\n@Note Only String keys are currently supported for map key indexing.\nie. MyStructProperty.MyStringToVectorMap[\"MyKey\"].X\nBe aware that MyStringToVectorMap[2] will not correspond to the key 2 in the map, but to the index 2 of the map.\nThis behaviour is intended to match PropertyHandle's GeneratePathToProperty method." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCFieldPathInfo>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments_Inner = { "Segments", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCFieldPathSegment, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments_MetaData[] = {
		{ "Comment", "/** List of segments to point to a given field */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "List of segments to point to a given field" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments = { "Segments", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFieldPathInfo, Segments), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_PathHash_MetaData[] = {
		{ "Comment", "/** Hash created from the string we were built from to quickly compare to paths */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "Hash created from the string we were built from to quickly compare to paths" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_PathHash = { "PathHash", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFieldPathInfo, PathHash), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_PathHash_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_PathHash_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_Segments,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::NewProp_PathHash,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RCFieldPathInfo",
		sizeof(FRCFieldPathInfo),
		alignof(FRCFieldPathInfo),
		Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCFieldPathInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCFieldPathInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCFieldPathInfo"), sizeof(FRCFieldPathInfo), Get_Z_Construct_UScriptStruct_FRCFieldPathInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCFieldPathInfo_Hash() { return 2104925611U; }
class UScriptStruct* FRCFieldPathSegment::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCFieldPathSegment_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCFieldPathSegment, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCFieldPathSegment"), sizeof(FRCFieldPathSegment), Get_Z_Construct_UScriptStruct_FRCFieldPathSegment_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCFieldPathSegment>()
{
	return FRCFieldPathSegment::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCFieldPathSegment(FRCFieldPathSegment::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCFieldPathSegment"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldPathSegment
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldPathSegment()
	{
		UScriptStruct::DeferCppStructOps<FRCFieldPathSegment>(FName(TEXT("RCFieldPathSegment")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldPathSegment;
	struct Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArrayIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ArrayIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValuePropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ValuePropertyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MapKey;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** RemoteControl Path segment holding a property layer */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "RemoteControl Path segment holding a property layer" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCFieldPathSegment>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/** Name of the segment */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "Name of the segment" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFieldPathSegment, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ArrayIndex_MetaData[] = {
		{ "Comment", "/** Container index if any. */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "Container index if any." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ArrayIndex = { "ArrayIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFieldPathSegment, ArrayIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ArrayIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ArrayIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ValuePropertyName_MetaData[] = {
		{ "Comment", "/**\n\x09 * Value property name, in case a map is being indexed.\n\x09 * ie. The path Var.Var_Value[0] will populate ValuePropertyName with \"Var\"\n\x09 * This is needed because sometimes the value's property name will differ from the\n\x09 * name before.\n\x09 * For example a map property named Test_1 will generate the following path: Test_1.Test_Value[0]\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "Value property name, in case a map is being indexed.\nie. The path Var.Var_Value[0] will populate ValuePropertyName with \"Var\"\nThis is needed because sometimes the value's property name will differ from the\nname before.\nFor example a map property named Test_1 will generate the following path: Test_1.Test_Value[0]" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ValuePropertyName = { "ValuePropertyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFieldPathSegment, ValuePropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ValuePropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ValuePropertyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_MapKey_MetaData[] = {
		{ "Comment", "/**\n\x09 * Holds the key in case of a path containing an indexed map.\n\x09 * ie. Field path MapProp[\"mykey\"] will fill MapKey with \"mykey\" \n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldPath.h" },
		{ "ToolTip", "Holds the key in case of a path containing an indexed map.\nie. Field path MapProp[\"mykey\"] will fill MapKey with \"mykey\"" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_MapKey = { "MapKey", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFieldPathSegment, MapKey), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_MapKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_MapKey_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ArrayIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_ValuePropertyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::NewProp_MapKey,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RCFieldPathSegment",
		sizeof(FRCFieldPathSegment),
		alignof(FRCFieldPathSegment),
		Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCFieldPathSegment()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCFieldPathSegment_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCFieldPathSegment"), sizeof(FRCFieldPathSegment), Get_Z_Construct_UScriptStruct_FRCFieldPathSegment_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCFieldPathSegment_Hash() { return 1294209120U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
