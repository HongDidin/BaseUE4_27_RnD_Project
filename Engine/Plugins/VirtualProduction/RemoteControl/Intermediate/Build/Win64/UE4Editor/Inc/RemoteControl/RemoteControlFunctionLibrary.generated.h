// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class URemoteControlPreset;
class AActor;
struct FRemoteControlOptionalExposeArgs;
class UObject;
#ifdef REMOTECONTROL_RemoteControlFunctionLibrary_generated_h
#error "RemoteControlFunctionLibrary.generated.h already included, missing '#pragma once' in RemoteControlFunctionLibrary.h"
#endif
#define REMOTECONTROL_RemoteControlFunctionLibrary_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlOptionalExposeArgs>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExposeActor); \
	DECLARE_FUNCTION(execExposeFunction); \
	DECLARE_FUNCTION(execExposeProperty);


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExposeActor); \
	DECLARE_FUNCTION(execExposeFunction); \
	DECLARE_FUNCTION(execExposeProperty);


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlFunctionLibrary(); \
	friend struct Z_Construct_UClass_URemoteControlFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URemoteControlFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlFunctionLibrary)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlFunctionLibrary(); \
	friend struct Z_Construct_UClass_URemoteControlFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URemoteControlFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlFunctionLibrary)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlFunctionLibrary(URemoteControlFunctionLibrary&&); \
	NO_API URemoteControlFunctionLibrary(const URemoteControlFunctionLibrary&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlFunctionLibrary(URemoteControlFunctionLibrary&&); \
	NO_API URemoteControlFunctionLibrary(const URemoteControlFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlFunctionLibrary)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_32_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
