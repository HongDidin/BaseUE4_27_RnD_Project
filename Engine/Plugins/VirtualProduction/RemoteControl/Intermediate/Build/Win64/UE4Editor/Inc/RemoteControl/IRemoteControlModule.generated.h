// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_IRemoteControlModule_generated_h
#error "IRemoteControlModule.generated.h already included, missing '#pragma once' in IRemoteControlModule.h"
#endif
#define REMOTECONTROL_IRemoteControlModule_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_IRemoteControlModule_h


#define FOREACH_ENUM_ERCACCESS(op) \
	op(ERCAccess::NO_ACCESS) \
	op(ERCAccess::READ_ACCESS) \
	op(ERCAccess::WRITE_ACCESS) \
	op(ERCAccess::WRITE_TRANSACTION_ACCESS) 

enum class ERCAccess : uint8;
template<> REMOTECONTROL_API UEnum* StaticEnum<ERCAccess>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
