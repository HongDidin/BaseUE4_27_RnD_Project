// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Private/RemoteControlExposeRegistry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlExposeRegistry() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCEntityWrapper();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlExposeRegistry_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlExposeRegistry();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FRCEntityWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCEntityWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCEntityWrapper, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCEntityWrapper"), sizeof(FRCEntityWrapper), Get_Z_Construct_UScriptStruct_FRCEntityWrapper_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCEntityWrapper>()
{
	return FRCEntityWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCEntityWrapper(FRCEntityWrapper::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCEntityWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCEntityWrapper
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCEntityWrapper()
	{
		UScriptStruct::DeferCppStructOps<FRCEntityWrapper>(FName(TEXT("RCEntityWrapper")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCEntityWrapper;
	struct Z_Construct_UScriptStruct_FRCEntityWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCEntityWrapper_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Wrapper class used to serialize exposable entities in a generic way. */" },
		{ "ModuleRelativePath", "Private/RemoteControlExposeRegistry.h" },
		{ "ToolTip", "Wrapper class used to serialize exposable entities in a generic way." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCEntityWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCEntityWrapper>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCEntityWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RCEntityWrapper",
		sizeof(FRCEntityWrapper),
		alignof(FRCEntityWrapper),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCEntityWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCEntityWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCEntityWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCEntityWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCEntityWrapper"), sizeof(FRCEntityWrapper), Get_Z_Construct_UScriptStruct_FRCEntityWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCEntityWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCEntityWrapper_Hash() { return 994542835U; }
	void URemoteControlExposeRegistry::StaticRegisterNativesURemoteControlExposeRegistry()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlExposeRegistry_NoRegister()
	{
		return URemoteControlExposeRegistry::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlExposeRegistry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposedEntities_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposedEntities_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ExposedEntities;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LabelToIdCache_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LabelToIdCache_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LabelToIdCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LabelToIdCache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlExposeRegistry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlExposeRegistry_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RemoteControlExposeRegistry.h" },
		{ "ModuleRelativePath", "Private/RemoteControlExposeRegistry.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities_ElementProp = { "ExposedEntities", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCEntityWrapper, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities_MetaData[] = {
		{ "Comment", "/** Holds the exposed entities. */" },
		{ "ModuleRelativePath", "Private/RemoteControlExposeRegistry.h" },
		{ "ToolTip", "Holds the exposed entities." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities = { "ExposedEntities", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlExposeRegistry, ExposedEntities), METADATA_PARAMS(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_ValueProp = { "LabelToIdCache", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_Key_KeyProp = { "LabelToIdCache_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_MetaData[] = {
		{ "Comment", "/** Cache of label to ids. */" },
		{ "ModuleRelativePath", "Private/RemoteControlExposeRegistry.h" },
		{ "ToolTip", "Cache of label to ids." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache = { "LabelToIdCache", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlExposeRegistry, LabelToIdCache), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlExposeRegistry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_ExposedEntities,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlExposeRegistry_Statics::NewProp_LabelToIdCache,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlExposeRegistry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlExposeRegistry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlExposeRegistry_Statics::ClassParams = {
		&URemoteControlExposeRegistry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlExposeRegistry_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlExposeRegistry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlExposeRegistry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlExposeRegistry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlExposeRegistry, 1871711895);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlExposeRegistry>()
	{
		return URemoteControlExposeRegistry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlExposeRegistry(Z_Construct_UClass_URemoteControlExposeRegistry, &URemoteControlExposeRegistry::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlExposeRegistry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlExposeRegistry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
