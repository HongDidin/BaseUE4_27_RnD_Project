// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBREMOTECONTROL_RemoteControlResponse_generated_h
#error "RemoteControlResponse.generated.h already included, missing '#pragma once' in RemoteControlResponse.h"
#endif
#define WEBREMOTECONTROL_RemoteControlResponse_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_348_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetEntitiesModifiedEvent>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_317_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetFieldsAddedEvent>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_285_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetFieldsRemovedEvent>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_265_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetLayoutModified>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_233_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetMetadataModified>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_205_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetFieldsRenamedEvent>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_188_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSetEntityLabelResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_173_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGetMetadataResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FGetMetadataResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_156_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FGetMetadataFieldResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_140_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSearchActorResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSearchActorResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_123_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSearchAssetResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSearchAssetResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_81_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FDescribeObjectResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_66_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGetPresetResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FGetPresetResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FListPresetsResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FListPresetsResponse>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAPIInfoResponse_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__HttpRoutes() { return STRUCT_OFFSET(FAPIInfoResponse, HttpRoutes); } \
	FORCEINLINE static uint32 __PPO__ActivePreset() { return STRUCT_OFFSET(FAPIInfoResponse, ActivePreset); }


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FAPIInfoResponse>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlResponse_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
