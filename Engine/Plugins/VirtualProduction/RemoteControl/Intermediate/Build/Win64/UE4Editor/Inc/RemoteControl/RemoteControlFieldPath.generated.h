// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlFieldPath_generated_h
#error "RemoteControlFieldPath.generated.h already included, missing '#pragma once' in RemoteControlFieldPath.h"
#endif
#define REMOTECONTROL_RemoteControlFieldPath_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldPath_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCFieldPathInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCFieldPathInfo>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldPath_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCFieldPathSegment_Statics; \
	static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCFieldPathSegment>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlFieldPath_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
