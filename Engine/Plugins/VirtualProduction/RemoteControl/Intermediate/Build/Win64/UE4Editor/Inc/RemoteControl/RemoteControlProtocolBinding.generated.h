// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlProtocolBinding_generated_h
#error "RemoteControlProtocolBinding.generated.h already included, missing '#pragma once' in RemoteControlProtocolBinding.h"
#endif
#define REMOTECONTROL_RemoteControlProtocolBinding_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlProtocolBinding_h_532_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Id() { return STRUCT_OFFSET(FRemoteControlProtocolBinding, Id); } \
	FORCEINLINE static uint32 __PPO__ProtocolName() { return STRUCT_OFFSET(FRemoteControlProtocolBinding, ProtocolName); } \
	FORCEINLINE static uint32 __PPO__PropertyId() { return STRUCT_OFFSET(FRemoteControlProtocolBinding, PropertyId); } \
	FORCEINLINE static uint32 __PPO__MappingPropertyName() { return STRUCT_OFFSET(FRemoteControlProtocolBinding, MappingPropertyName); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlProtocolBinding>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlProtocolBinding_h_361_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Owner() { return STRUCT_OFFSET(FRemoteControlProtocolEntity, Owner); } \
	FORCEINLINE static uint32 __PPO__PropertyId() { return STRUCT_OFFSET(FRemoteControlProtocolEntity, PropertyId); } \
	FORCEINLINE static uint32 __PPO__Mappings() { return STRUCT_OFFSET(FRemoteControlProtocolEntity, Mappings); } \
	FORCEINLINE static uint32 __PPO__BindingStatus() { return STRUCT_OFFSET(FRemoteControlProtocolEntity, BindingStatus); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlProtocolEntity>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlProtocolBinding_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Id() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, Id); } \
	FORCEINLINE static uint32 __PPO__InterpolationRangePropertyData() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationRangePropertyData); } \
	FORCEINLINE static uint32 __PPO__InterpolationMappingPropertyData() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationMappingPropertyData); } \
	FORCEINLINE static uint32 __PPO__InterpolationRangePropertyDataCache() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationRangePropertyDataCache); } \
	FORCEINLINE static uint32 __PPO__InterpolationMappingPropertyDataCache() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationMappingPropertyDataCache); } \
	FORCEINLINE static uint32 __PPO__InterpolationMappingPropertyElementNum() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationMappingPropertyElementNum); } \
	FORCEINLINE static uint32 __PPO__BoundPropertyPath() { return STRUCT_OFFSET(FRemoteControlProtocolMapping, BoundPropertyPath); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlProtocolMapping>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlProtocolBinding_h


#define FOREACH_ENUM_ERCBINDINGSTATUS(op) \
	op(ERCBindingStatus::Unassigned) \
	op(ERCBindingStatus::Awaiting) \
	op(ERCBindingStatus::Bound) 

enum class ERCBindingStatus : uint8;
template<> REMOTECONTROL_API UEnum* StaticEnum<ERCBindingStatus>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
