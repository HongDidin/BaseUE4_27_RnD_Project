// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FRemoteControlInterceptionFunctionParamStruct;
#ifdef REMOTECONTROL_RemoteControlInterceptionTestData_generated_h
#error "RemoteControlInterceptionTestData.generated.h already included, missing '#pragma once' in RemoteControlInterceptionTestData.h"
#endif
#define REMOTECONTROL_RemoteControlInterceptionTestData_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlInterceptionFunctionParamStruct_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlInterceptionFunctionParamStruct>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlInterceptionTestStruct_Statics; \
	REMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlInterceptionTestStruct>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTestFunction);


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTestFunction);


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlInterceptionTestObject(); \
	friend struct Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics; \
public: \
	DECLARE_CLASS(URemoteControlInterceptionTestObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlInterceptionTestObject)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlInterceptionTestObject(); \
	friend struct Z_Construct_UClass_URemoteControlInterceptionTestObject_Statics; \
public: \
	DECLARE_CLASS(URemoteControlInterceptionTestObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlInterceptionTestObject)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlInterceptionTestObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlInterceptionTestObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlInterceptionTestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlInterceptionTestObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlInterceptionTestObject(URemoteControlInterceptionTestObject&&); \
	NO_API URemoteControlInterceptionTestObject(const URemoteControlInterceptionTestObject&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlInterceptionTestObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlInterceptionTestObject(URemoteControlInterceptionTestObject&&); \
	NO_API URemoteControlInterceptionTestObject(const URemoteControlInterceptionTestObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlInterceptionTestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlInterceptionTestObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlInterceptionTestObject)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_34_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h_38_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class URemoteControlInterceptionTestObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Private_Tests_RemoteControlInterceptionTestData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
