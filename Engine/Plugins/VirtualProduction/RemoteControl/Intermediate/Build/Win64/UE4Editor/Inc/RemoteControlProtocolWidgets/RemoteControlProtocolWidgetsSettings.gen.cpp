// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlProtocolWidgets/Private/RemoteControlProtocolWidgetsSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolWidgetsSettings() {}
// Cross Module References
	REMOTECONTROLPROTOCOLWIDGETS_API UClass* Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_NoRegister();
	REMOTECONTROLPROTOCOLWIDGETS_API UClass* Z_Construct_UClass_URemoteControlProtocolWidgetsSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_RemoteControlProtocolWidgets();
// End Cross Module References
	void URemoteControlProtocolWidgetsSettings::StaticRegisterNativesURemoteControlProtocolWidgetsSettings()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_NoRegister()
	{
		return URemoteControlProtocolWidgetsSettings::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_HiddenProtocolTypeNames_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HiddenProtocolTypeNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_HiddenProtocolTypeNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreferredProtocol_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PreferredProtocol;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolWidgets,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Remote Control Protocol Widget Settings\n */" },
		{ "IncludePath", "RemoteControlProtocolWidgetsSettings.h" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolWidgetsSettings.h" },
		{ "ToolTip", "Remote Control Protocol Widget Settings" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames_ElementProp = { "HiddenProtocolTypeNames", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames_MetaData[] = {
		{ "Category", "Widgets" },
		{ "Comment", "/** Protocol types to be hidden in the list view. */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolWidgetsSettings.h" },
		{ "ToolTip", "Protocol types to be hidden in the list view." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames = { "HiddenProtocolTypeNames", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlProtocolWidgetsSettings, HiddenProtocolTypeNames), METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_PreferredProtocol_MetaData[] = {
		{ "Comment", "/** Last protocol added. Used as default in the binding list. */" },
		{ "ModuleRelativePath", "Private/RemoteControlProtocolWidgetsSettings.h" },
		{ "ToolTip", "Last protocol added. Used as default in the binding list." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_PreferredProtocol = { "PreferredProtocol", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlProtocolWidgetsSettings, PreferredProtocol), METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_PreferredProtocol_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_PreferredProtocol_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_HiddenProtocolTypeNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::NewProp_PreferredProtocol,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlProtocolWidgetsSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::ClassParams = {
		&URemoteControlProtocolWidgetsSettings::StaticClass,
		"RemoteControlProtocolWidgets",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlProtocolWidgetsSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlProtocolWidgetsSettings, 1014655632);
	template<> REMOTECONTROLPROTOCOLWIDGETS_API UClass* StaticClass<URemoteControlProtocolWidgetsSettings>()
	{
		return URemoteControlProtocolWidgetsSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlProtocolWidgetsSettings(Z_Construct_UClass_URemoteControlProtocolWidgetsSettings, &URemoteControlProtocolWidgetsSettings::StaticClass, TEXT("/Script/RemoteControlProtocolWidgets"), TEXT("URemoteControlProtocolWidgetsSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlProtocolWidgetsSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
