// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBREMOTECONTROL_RemoteControlRequest_generated_h
#error "RemoteControlRequest.generated.h already included, missing '#pragma once' in RemoteControlRequest.h"
#endif
#define WEBREMOTECONTROL_RemoteControlRequest_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_524_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCWebSocketPresetRegisterBody>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_493_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCWebSocketRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_476_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FGetObjectThumbnailRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_461_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSetEntityLabelRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_444_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSetEntityMetadataRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_427_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSetPresetMetadataRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_388_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSearchObjectRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSearchObjectRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_356_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSearchActorRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSearchActorRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_324_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSearchAssetRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FSearchAssetRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_305_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FDescribeObjectRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_280_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetCallRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_252_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetSetPropertyRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_175_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCObjectRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Access() { return STRUCT_OFFSET(FRCObjectRequest, Access); } \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCObjectRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_138_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCCallRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCCallRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_106_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlObjectEventHookRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_83_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCBatchRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCBatchRequest>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_54_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCRequestWrapper_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct(); \
	typedef FRCRequest Super;


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCRequestWrapper>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCRequest_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCRequest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlRequest_h


#define FOREACH_ENUM_EREMOTECONTROLEVENT(op) \
	op(ERemoteControlEvent::PreObjectPropertyChanged) \
	op(ERemoteControlEvent::ObjectPropertyChanged) \
	op(ERemoteControlEvent::EventCount) 

enum class ERemoteControlEvent : uint8;
template<> WEBREMOTECONTROL_API UEnum* StaticEnum<ERemoteControlEvent>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
