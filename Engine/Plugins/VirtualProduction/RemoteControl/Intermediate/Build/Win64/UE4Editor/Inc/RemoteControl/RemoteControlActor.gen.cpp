// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlActor() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlActor();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlEntity();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References

static_assert(std::is_polymorphic<FRemoteControlActor>() == std::is_polymorphic<FRemoteControlEntity>(), "USTRUCT FRemoteControlActor cannot be polymorphic unless super FRemoteControlEntity is polymorphic");

class UScriptStruct* FRemoteControlActor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlActor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlActor, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlActor"), sizeof(FRemoteControlActor), Get_Z_Construct_UScriptStruct_FRemoteControlActor_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlActor>()
{
	return FRemoteControlActor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlActor(FRemoteControlActor::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlActor"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlActor
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlActor()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlActor>(FName(TEXT("RemoteControlActor")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlActor;
	struct Z_Construct_UScriptStruct_FRemoteControlActor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlActor_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents an actor exposed in the panel.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlActor.h" },
		{ "ToolTip", "Represents an actor exposed in the panel." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlActor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlActor>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlActor_Statics::NewProp_Path_MetaData[] = {
		{ "Category", "RemoteControlEntity" },
		{ "Comment", "/**\n\x09 * Path to the exposed object.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlActor.h" },
		{ "ToolTip", "Path to the exposed object." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlActor_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlActor, Path), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlActor_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlActor_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlActor_Statics::NewProp_Path,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlActor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRemoteControlEntity,
		&NewStructOps,
		"RemoteControlActor",
		sizeof(FRemoteControlActor),
		alignof(FRemoteControlActor),
		Z_Construct_UScriptStruct_FRemoteControlActor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlActor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlActor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlActor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlActor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlActor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlActor"), sizeof(FRemoteControlActor), Get_Z_Construct_UScriptStruct_FRemoteControlActor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlActor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlActor_Hash() { return 492929890U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
