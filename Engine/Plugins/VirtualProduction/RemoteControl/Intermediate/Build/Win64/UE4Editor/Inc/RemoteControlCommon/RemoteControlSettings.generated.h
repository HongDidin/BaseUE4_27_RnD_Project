// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLCOMMON_RemoteControlSettings_generated_h
#error "RemoteControlSettings.generated.h already included, missing '#pragma once' in RemoteControlSettings.h"
#endif
#define REMOTECONTROLCOMMON_RemoteControlSettings_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlSettings(); \
	friend struct Z_Construct_UClass_URemoteControlSettings_Statics; \
public: \
	DECLARE_CLASS(URemoteControlSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlCommon"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("RemoteControl");} \



#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlSettings(); \
	friend struct Z_Construct_UClass_URemoteControlSettings_Statics; \
public: \
	DECLARE_CLASS(URemoteControlSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlCommon"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("RemoteControl");} \



#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlSettings(URemoteControlSettings&&); \
	NO_API URemoteControlSettings(const URemoteControlSettings&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlSettings(URemoteControlSettings&&); \
	NO_API URemoteControlSettings(const URemoteControlSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlSettings)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLCOMMON_API UClass* StaticClass<class URemoteControlSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RemoteControlSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
