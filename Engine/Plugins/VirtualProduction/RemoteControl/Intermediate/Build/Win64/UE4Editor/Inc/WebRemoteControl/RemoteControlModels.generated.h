// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WEBREMOTECONTROL_RemoteControlModels_generated_h
#error "RemoteControlModels.generated.h already included, missing '#pragma once' in RemoteControlModels.h"
#endif
#define WEBREMOTECONTROL_RemoteControlModels_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_626_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCAssetFilter_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCAssetFilter>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_606_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetFieldRenamed>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_579_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCAssetDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCAssetDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_525_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCShortPresetDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_482_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_425_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetModifiedEntitiesDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_352_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPresetLayoutGroupDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_321_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCExposedActorDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_289_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCExposedFunctionDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_252_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCExposedPropertyDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_216_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCFunctionDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCFunctionDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_109_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPropertyDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCPropertyDescription>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h_69_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCObjectDescription_Statics; \
	WEBREMOTECONTROL_API static class UScriptStruct* StaticStruct();


template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<struct FRCObjectDescription>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_WebRemoteControl_Private_RemoteControlModels_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
