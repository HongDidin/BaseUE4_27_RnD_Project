// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlPresetActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlPresetActor() {}
// Cross Module References
	REMOTECONTROL_API UClass* Z_Construct_UClass_ARemoteControlPresetActor_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_ARemoteControlPresetActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlPreset_NoRegister();
// End Cross Module References
	void ARemoteControlPresetActor::StaticRegisterNativesARemoteControlPresetActor()
	{
	}
	UClass* Z_Construct_UClass_ARemoteControlPresetActor_NoRegister()
	{
		return ARemoteControlPresetActor::StaticClass();
	}
	struct Z_Construct_UClass_ARemoteControlPresetActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARemoteControlPresetActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARemoteControlPresetActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Actor that wraps a remote control preset, allows linking a specific preset to a level.\n */" },
		{ "HideCategories", "Rendering Physics LOD Activation Input Replication Collision Actor Cooking" },
		{ "IncludePath", "RemoteControlPresetActor.h" },
		{ "ModuleRelativePath", "Public/RemoteControlPresetActor.h" },
		{ "ToolTip", "Actor that wraps a remote control preset, allows linking a specific preset to a level." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARemoteControlPresetActor_Statics::NewProp_Preset_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Public/RemoteControlPresetActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARemoteControlPresetActor_Statics::NewProp_Preset = { "Preset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARemoteControlPresetActor, Preset), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARemoteControlPresetActor_Statics::NewProp_Preset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARemoteControlPresetActor_Statics::NewProp_Preset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARemoteControlPresetActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARemoteControlPresetActor_Statics::NewProp_Preset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARemoteControlPresetActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARemoteControlPresetActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARemoteControlPresetActor_Statics::ClassParams = {
		&ARemoteControlPresetActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ARemoteControlPresetActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ARemoteControlPresetActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARemoteControlPresetActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARemoteControlPresetActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARemoteControlPresetActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARemoteControlPresetActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARemoteControlPresetActor, 240253799);
	template<> REMOTECONTROL_API UClass* StaticClass<ARemoteControlPresetActor>()
	{
		return ARemoteControlPresetActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARemoteControlPresetActor(Z_Construct_UClass_ARemoteControlPresetActor, &ARemoteControlPresetActor::StaticClass, TEXT("/Script/RemoteControl"), TEXT("ARemoteControlPresetActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARemoteControlPresetActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
