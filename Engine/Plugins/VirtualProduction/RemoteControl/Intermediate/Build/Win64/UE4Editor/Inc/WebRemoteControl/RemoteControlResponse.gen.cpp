// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WebRemoteControl/Private/RemoteControlResponse.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlResponse() {}
// Cross Module References
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent();
	UPackage* Z_Construct_UPackage__Script_WebRemoteControl();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetLayoutModified();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetMetadataModified();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldRenamed();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSetEntityLabelResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FGetMetadataResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FGetMetadataFieldResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSearchActorResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCObjectDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSearchAssetResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCAssetDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FDescribeObjectResponse();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPropertyDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCFunctionDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FGetPresetResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FListPresetsResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCShortPresetDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FAPIInfoResponse();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlRouteDescription();
// End Cross Module References
class UScriptStruct* FRCPresetEntitiesModifiedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetEntitiesModifiedEvent"), sizeof(FRCPresetEntitiesModifiedEvent), Get_Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetEntitiesModifiedEvent>()
{
	return FRCPresetEntitiesModifiedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetEntitiesModifiedEvent(FRCPresetEntitiesModifiedEvent::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetEntitiesModifiedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetEntitiesModifiedEvent
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetEntitiesModifiedEvent()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetEntitiesModifiedEvent>(FName(TEXT("RCPresetEntitiesModifiedEvent")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetEntitiesModifiedEvent;
	struct Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PresetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PresetId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedEntities_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedEntities;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Event triggered when an exposed entity struct is modified.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "Event triggered when an exposed entity struct is modified." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetEntitiesModifiedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_Type_MetaData[] = {
		{ "Comment", "/**\n\x09 * Type of the event.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "Type of the event." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetEntitiesModifiedEvent, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetName_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the preset which contains the modified entities.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "Name of the preset which contains the modified entities." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetName = { "PresetName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetEntitiesModifiedEvent, PresetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetId_MetaData[] = {
		{ "Comment", "/**\n\x09 * ID of the preset that contains the modified entities.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "ID of the preset that contains the modified entities." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetId = { "PresetId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetEntitiesModifiedEvent, PresetId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_ModifiedEntities_MetaData[] = {
		{ "Comment", "/**\n\x09 * The entities that were modified in the last frame.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "The entities that were modified in the last frame." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_ModifiedEntities = { "ModifiedEntities", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetEntitiesModifiedEvent, ModifiedEntities), Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_ModifiedEntities_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_ModifiedEntities_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_PresetId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::NewProp_ModifiedEntities,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetEntitiesModifiedEvent",
		sizeof(FRCPresetEntitiesModifiedEvent),
		alignof(FRCPresetEntitiesModifiedEvent),
		Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetEntitiesModifiedEvent"), sizeof(FRCPresetEntitiesModifiedEvent), Get_Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetEntitiesModifiedEvent_Hash() { return 1342565335U; }
class UScriptStruct* FRCPresetFieldsAddedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetFieldsAddedEvent"), sizeof(FRCPresetFieldsAddedEvent), Get_Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetFieldsAddedEvent>()
{
	return FRCPresetFieldsAddedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetFieldsAddedEvent(FRCPresetFieldsAddedEvent::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetFieldsAddedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsAddedEvent
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsAddedEvent()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetFieldsAddedEvent>(FName(TEXT("RCPresetFieldsAddedEvent")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsAddedEvent;
	struct Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PresetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PresetId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Description;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetFieldsAddedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsAddedEvent, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetName_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetName = { "PresetName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsAddedEvent, PresetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetId_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetId = { "PresetId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsAddedEvent, PresetId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Description_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsAddedEvent, Description), Z_Construct_UScriptStruct_FRCPresetDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Description_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_PresetId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::NewProp_Description,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetFieldsAddedEvent",
		sizeof(FRCPresetFieldsAddedEvent),
		alignof(FRCPresetFieldsAddedEvent),
		Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetFieldsAddedEvent"), sizeof(FRCPresetFieldsAddedEvent), Get_Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsAddedEvent_Hash() { return 1749382955U; }
class UScriptStruct* FRCPresetFieldsRemovedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetFieldsRemovedEvent"), sizeof(FRCPresetFieldsRemovedEvent), Get_Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetFieldsRemovedEvent>()
{
	return FRCPresetFieldsRemovedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetFieldsRemovedEvent(FRCPresetFieldsRemovedEvent::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetFieldsRemovedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsRemovedEvent
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsRemovedEvent()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetFieldsRemovedEvent>(FName(TEXT("RCPresetFieldsRemovedEvent")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsRemovedEvent;
	struct Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PresetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PresetId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RemovedFields_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemovedFields_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RemovedFields;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RemovedFieldIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemovedFieldIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RemovedFieldIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetFieldsRemovedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRemovedEvent, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetName_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetName = { "PresetName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRemovedEvent, PresetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetId_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetId = { "PresetId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRemovedEvent, PresetId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields_Inner = { "RemovedFields", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields = { "RemovedFields", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRemovedEvent, RemovedFields), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds_Inner = { "RemovedFieldIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds = { "RemovedFieldIds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRemovedEvent, RemovedFieldIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_PresetId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFields,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::NewProp_RemovedFieldIds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetFieldsRemovedEvent",
		sizeof(FRCPresetFieldsRemovedEvent),
		alignof(FRCPresetFieldsRemovedEvent),
		Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetFieldsRemovedEvent"), sizeof(FRCPresetFieldsRemovedEvent), Get_Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsRemovedEvent_Hash() { return 341259821U; }
class UScriptStruct* FRCPresetLayoutModified::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetLayoutModified_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetLayoutModified, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetLayoutModified"), sizeof(FRCPresetLayoutModified), Get_Z_Construct_UScriptStruct_FRCPresetLayoutModified_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetLayoutModified>()
{
	return FRCPresetLayoutModified::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetLayoutModified(FRCPresetLayoutModified::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetLayoutModified"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetLayoutModified
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetLayoutModified()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetLayoutModified>(FName(TEXT("RCPresetLayoutModified")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetLayoutModified;
	struct Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Preset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetLayoutModified>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetLayoutModified, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Preset_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Preset = { "Preset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetLayoutModified, Preset), Z_Construct_UScriptStruct_FRCPresetDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Preset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Preset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::NewProp_Preset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetLayoutModified",
		sizeof(FRCPresetLayoutModified),
		alignof(FRCPresetLayoutModified),
		Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetLayoutModified()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetLayoutModified_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetLayoutModified"), sizeof(FRCPresetLayoutModified), Get_Z_Construct_UScriptStruct_FRCPresetLayoutModified_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetLayoutModified_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetLayoutModified_Hash() { return 4237501628U; }
class UScriptStruct* FRCPresetMetadataModified::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetMetadataModified_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetMetadataModified, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetMetadataModified"), sizeof(FRCPresetMetadataModified), Get_Z_Construct_UScriptStruct_FRCPresetMetadataModified_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetMetadataModified>()
{
	return FRCPresetMetadataModified::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetMetadataModified(FRCPresetMetadataModified::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetMetadataModified"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetMetadataModified
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetMetadataModified()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetMetadataModified>(FName(TEXT("RCPresetMetadataModified")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetMetadataModified;
	struct Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PresetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PresetId;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetMetadataModified>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetMetadataModified, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetName_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetName = { "PresetName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetMetadataModified, PresetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetId_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetId = { "PresetId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetMetadataModified, PresetId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetId_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetMetadataModified, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_PresetId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::NewProp_Metadata,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetMetadataModified",
		sizeof(FRCPresetMetadataModified),
		alignof(FRCPresetMetadataModified),
		Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetMetadataModified()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetMetadataModified_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetMetadataModified"), sizeof(FRCPresetMetadataModified), Get_Z_Construct_UScriptStruct_FRCPresetMetadataModified_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetMetadataModified_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetMetadataModified_Hash() { return 654067302U; }
class UScriptStruct* FRCPresetFieldsRenamedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetFieldsRenamedEvent"), sizeof(FRCPresetFieldsRenamedEvent), Get_Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetFieldsRenamedEvent>()
{
	return FRCPresetFieldsRenamedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetFieldsRenamedEvent(FRCPresetFieldsRenamedEvent::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetFieldsRenamedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsRenamedEvent
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsRenamedEvent()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetFieldsRenamedEvent>(FName(TEXT("RCPresetFieldsRenamedEvent")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldsRenamedEvent;
	struct Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PresetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PresetId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenamedFields_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenamedFields_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RenamedFields;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetFieldsRenamedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRenamedEvent, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetName_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetName = { "PresetName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRenamedEvent, PresetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetId_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetId = { "PresetId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRenamedEvent, PresetId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields_Inner = { "RenamedFields", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCPresetFieldRenamed, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields = { "RenamedFields", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldsRenamedEvent, RenamedFields), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_PresetId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::NewProp_RenamedFields,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetFieldsRenamedEvent",
		sizeof(FRCPresetFieldsRenamedEvent),
		alignof(FRCPresetFieldsRenamedEvent),
		Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetFieldsRenamedEvent"), sizeof(FRCPresetFieldsRenamedEvent), Get_Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldsRenamedEvent_Hash() { return 2741208781U; }
class UScriptStruct* FSetEntityLabelResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSetEntityLabelResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSetEntityLabelResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SetEntityLabelResponse"), sizeof(FSetEntityLabelResponse), Get_Z_Construct_UScriptStruct_FSetEntityLabelResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSetEntityLabelResponse>()
{
	return FSetEntityLabelResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSetEntityLabelResponse(FSetEntityLabelResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SetEntityLabelResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityLabelResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityLabelResponse()
	{
		UScriptStruct::DeferCppStructOps<FSetEntityLabelResponse>(FName(TEXT("SetEntityLabelResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityLabelResponse;
	struct Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssignedLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssignedLabel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSetEntityLabelResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::NewProp_AssignedLabel_MetaData[] = {
		{ "Comment", "/** The label that was assigned when requesting to modify an entity's label. */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "The label that was assigned when requesting to modify an entity's label." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::NewProp_AssignedLabel = { "AssignedLabel", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSetEntityLabelResponse, AssignedLabel), METADATA_PARAMS(Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::NewProp_AssignedLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::NewProp_AssignedLabel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::NewProp_AssignedLabel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"SetEntityLabelResponse",
		sizeof(FSetEntityLabelResponse),
		alignof(FSetEntityLabelResponse),
		Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSetEntityLabelResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSetEntityLabelResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SetEntityLabelResponse"), sizeof(FSetEntityLabelResponse), Get_Z_Construct_UScriptStruct_FSetEntityLabelResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSetEntityLabelResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSetEntityLabelResponse_Hash() { return 2298761879U; }
class UScriptStruct* FGetMetadataResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FGetMetadataResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGetMetadataResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("GetMetadataResponse"), sizeof(FGetMetadataResponse), Get_Z_Construct_UScriptStruct_FGetMetadataResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FGetMetadataResponse>()
{
	return FGetMetadataResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGetMetadataResponse(FGetMetadataResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("GetMetadataResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetMetadataResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetMetadataResponse()
	{
		UScriptStruct::DeferCppStructOps<FGetMetadataResponse>(FName(TEXT("GetMetadataResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFGetMetadataResponse;
	struct Z_Construct_UScriptStruct_FGetMetadataResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGetMetadataResponse>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGetMetadataResponse, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::NewProp_Metadata,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"GetMetadataResponse",
		sizeof(FGetMetadataResponse),
		alignof(FGetMetadataResponse),
		Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGetMetadataResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGetMetadataResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GetMetadataResponse"), sizeof(FGetMetadataResponse), Get_Z_Construct_UScriptStruct_FGetMetadataResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGetMetadataResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGetMetadataResponse_Hash() { return 716915195U; }
class UScriptStruct* FGetMetadataFieldResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGetMetadataFieldResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("GetMetadataFieldResponse"), sizeof(FGetMetadataFieldResponse), Get_Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FGetMetadataFieldResponse>()
{
	return FGetMetadataFieldResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGetMetadataFieldResponse(FGetMetadataFieldResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("GetMetadataFieldResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetMetadataFieldResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetMetadataFieldResponse()
	{
		UScriptStruct::DeferCppStructOps<FGetMetadataFieldResponse>(FName(TEXT("GetMetadataFieldResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFGetMetadataFieldResponse;
	struct Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGetMetadataFieldResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::NewProp_Value_MetaData[] = {
		{ "Comment", "/** The metadata value for a given key. */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "The metadata value for a given key." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGetMetadataFieldResponse, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"GetMetadataFieldResponse",
		sizeof(FGetMetadataFieldResponse),
		alignof(FGetMetadataFieldResponse),
		Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGetMetadataFieldResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GetMetadataFieldResponse"), sizeof(FGetMetadataFieldResponse), Get_Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGetMetadataFieldResponse_Hash() { return 606573307U; }
class UScriptStruct* FSearchActorResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSearchActorResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSearchActorResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SearchActorResponse"), sizeof(FSearchActorResponse), Get_Z_Construct_UScriptStruct_FSearchActorResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSearchActorResponse>()
{
	return FSearchActorResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSearchActorResponse(FSearchActorResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SearchActorResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchActorResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchActorResponse()
	{
		UScriptStruct::DeferCppStructOps<FSearchActorResponse>(FName(TEXT("SearchActorResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchActorResponse;
	struct Z_Construct_UScriptStruct_FSearchActorResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchActorResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSearchActorResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCObjectDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchActorResponse, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSearchActorResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchActorResponse_Statics::NewProp_Actors,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSearchActorResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"SearchActorResponse",
		sizeof(FSearchActorResponse),
		alignof(FSearchActorResponse),
		Z_Construct_UScriptStruct_FSearchActorResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchActorResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSearchActorResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSearchActorResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SearchActorResponse"), sizeof(FSearchActorResponse), Get_Z_Construct_UScriptStruct_FSearchActorResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSearchActorResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSearchActorResponse_Hash() { return 933105967U; }
class UScriptStruct* FSearchAssetResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSearchAssetResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSearchAssetResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SearchAssetResponse"), sizeof(FSearchAssetResponse), Get_Z_Construct_UScriptStruct_FSearchAssetResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSearchAssetResponse>()
{
	return FSearchAssetResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSearchAssetResponse(FSearchAssetResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SearchAssetResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchAssetResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchAssetResponse()
	{
		UScriptStruct::DeferCppStructOps<FSearchAssetResponse>(FName(TEXT("SearchAssetResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchAssetResponse;
	struct Z_Construct_UScriptStruct_FSearchAssetResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Assets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Assets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Assets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSearchAssetResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets_Inner = { "Assets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCAssetDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets = { "Assets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchAssetResponse, Assets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::NewProp_Assets,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"SearchAssetResponse",
		sizeof(FSearchAssetResponse),
		alignof(FSearchAssetResponse),
		Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSearchAssetResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSearchAssetResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SearchAssetResponse"), sizeof(FSearchAssetResponse), Get_Z_Construct_UScriptStruct_FSearchAssetResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSearchAssetResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSearchAssetResponse_Hash() { return 3884134666U; }
class UScriptStruct* FDescribeObjectResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FDescribeObjectResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDescribeObjectResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("DescribeObjectResponse"), sizeof(FDescribeObjectResponse), Get_Z_Construct_UScriptStruct_FDescribeObjectResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FDescribeObjectResponse>()
{
	return FDescribeObjectResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDescribeObjectResponse(FDescribeObjectResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("DescribeObjectResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFDescribeObjectResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFDescribeObjectResponse()
	{
		UScriptStruct::DeferCppStructOps<FDescribeObjectResponse>(FName(TEXT("DescribeObjectResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFDescribeObjectResponse;
	struct Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Properties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Properties;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Functions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Functions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Functions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDescribeObjectResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDescribeObjectResponse, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Class_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDescribeObjectResponse, Class), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Class_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties_Inner = { "Properties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCPropertyDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDescribeObjectResponse, Properties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions_Inner = { "Functions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCFunctionDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions = { "Functions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDescribeObjectResponse, Functions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Properties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::NewProp_Functions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"DescribeObjectResponse",
		sizeof(FDescribeObjectResponse),
		alignof(FDescribeObjectResponse),
		Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDescribeObjectResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDescribeObjectResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DescribeObjectResponse"), sizeof(FDescribeObjectResponse), Get_Z_Construct_UScriptStruct_FDescribeObjectResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDescribeObjectResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDescribeObjectResponse_Hash() { return 2154319905U; }
class UScriptStruct* FGetPresetResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FGetPresetResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGetPresetResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("GetPresetResponse"), sizeof(FGetPresetResponse), Get_Z_Construct_UScriptStruct_FGetPresetResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FGetPresetResponse>()
{
	return FGetPresetResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGetPresetResponse(FGetPresetResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("GetPresetResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetPresetResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetPresetResponse()
	{
		UScriptStruct::DeferCppStructOps<FGetPresetResponse>(FName(TEXT("GetPresetResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFGetPresetResponse;
	struct Z_Construct_UScriptStruct_FGetPresetResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Preset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetPresetResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGetPresetResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGetPresetResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetPresetResponse_Statics::NewProp_Preset_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGetPresetResponse_Statics::NewProp_Preset = { "Preset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGetPresetResponse, Preset), Z_Construct_UScriptStruct_FRCPresetDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FGetPresetResponse_Statics::NewProp_Preset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetPresetResponse_Statics::NewProp_Preset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGetPresetResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGetPresetResponse_Statics::NewProp_Preset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGetPresetResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"GetPresetResponse",
		sizeof(FGetPresetResponse),
		alignof(FGetPresetResponse),
		Z_Construct_UScriptStruct_FGetPresetResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetPresetResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGetPresetResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetPresetResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGetPresetResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGetPresetResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GetPresetResponse"), sizeof(FGetPresetResponse), Get_Z_Construct_UScriptStruct_FGetPresetResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGetPresetResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGetPresetResponse_Hash() { return 4004470030U; }
class UScriptStruct* FListPresetsResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FListPresetsResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FListPresetsResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("ListPresetsResponse"), sizeof(FListPresetsResponse), Get_Z_Construct_UScriptStruct_FListPresetsResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FListPresetsResponse>()
{
	return FListPresetsResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FListPresetsResponse(FListPresetsResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("ListPresetsResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFListPresetsResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFListPresetsResponse()
	{
		UScriptStruct::DeferCppStructOps<FListPresetsResponse>(FName(TEXT("ListPresetsResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFListPresetsResponse;
	struct Z_Construct_UScriptStruct_FListPresetsResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Presets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Presets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Presets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FListPresetsResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FListPresetsResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets_Inner = { "Presets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCShortPresetDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets_MetaData[] = {
		{ "Comment", "/**\n\x09 * The list of available remote control presets. \n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "The list of available remote control presets." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets = { "Presets", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FListPresetsResponse, Presets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FListPresetsResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FListPresetsResponse_Statics::NewProp_Presets,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FListPresetsResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"ListPresetsResponse",
		sizeof(FListPresetsResponse),
		alignof(FListPresetsResponse),
		Z_Construct_UScriptStruct_FListPresetsResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FListPresetsResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FListPresetsResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FListPresetsResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FListPresetsResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FListPresetsResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ListPresetsResponse"), sizeof(FListPresetsResponse), Get_Z_Construct_UScriptStruct_FListPresetsResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FListPresetsResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FListPresetsResponse_Hash() { return 1291069019U; }
class UScriptStruct* FAPIInfoResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FAPIInfoResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAPIInfoResponse, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("APIInfoResponse"), sizeof(FAPIInfoResponse), Get_Z_Construct_UScriptStruct_FAPIInfoResponse_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FAPIInfoResponse>()
{
	return FAPIInfoResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAPIInfoResponse(FAPIInfoResponse::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("APIInfoResponse"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFAPIInfoResponse
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFAPIInfoResponse()
	{
		UScriptStruct::DeferCppStructOps<FAPIInfoResponse>(FName(TEXT("APIInfoResponse")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFAPIInfoResponse;
	struct Z_Construct_UScriptStruct_FAPIInfoResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HttpRoutes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HttpRoutes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_HttpRoutes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivePreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActivePreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAPIInfoResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes_Inner = { "HttpRoutes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlRouteDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes_MetaData[] = {
		{ "Comment", "/**\n\x09 * Descriptions for all the routes that make up the remote control API.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
		{ "ToolTip", "Descriptions for all the routes that make up the remote control API." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes = { "HttpRoutes", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPIInfoResponse, HttpRoutes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_ActivePreset_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlResponse.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_ActivePreset = { "ActivePreset", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPIInfoResponse, ActivePreset), Z_Construct_UScriptStruct_FRCShortPresetDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_ActivePreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_ActivePreset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_HttpRoutes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::NewProp_ActivePreset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"APIInfoResponse",
		sizeof(FAPIInfoResponse),
		alignof(FAPIInfoResponse),
		Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAPIInfoResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAPIInfoResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("APIInfoResponse"), sizeof(FAPIInfoResponse), Get_Z_Construct_UScriptStruct_FAPIInfoResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAPIInfoResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAPIInfoResponse_Hash() { return 1335264128U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
