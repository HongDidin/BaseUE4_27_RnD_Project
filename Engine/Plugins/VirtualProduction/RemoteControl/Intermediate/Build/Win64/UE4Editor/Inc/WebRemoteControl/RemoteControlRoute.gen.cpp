// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WebRemoteControl/Private/RemoteControlRoute.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlRoute() {}
// Cross Module References
	WEBREMOTECONTROL_API UEnum* Z_Construct_UEnum_WebRemoteControl_ERemoteControlHttpVerbs();
	UPackage* Z_Construct_UPackage__Script_WebRemoteControl();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlRouteDescription();
// End Cross Module References
	static UEnum* ERemoteControlHttpVerbs_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_WebRemoteControl_ERemoteControlHttpVerbs, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("ERemoteControlHttpVerbs"));
		}
		return Singleton;
	}
	template<> WEBREMOTECONTROL_API UEnum* StaticEnum<ERemoteControlHttpVerbs>()
	{
		return ERemoteControlHttpVerbs_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteControlHttpVerbs(ERemoteControlHttpVerbs_StaticEnum, TEXT("/Script/WebRemoteControl"), TEXT("ERemoteControlHttpVerbs"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_WebRemoteControl_ERemoteControlHttpVerbs_Hash() { return 1751960767U; }
	UEnum* Z_Construct_UEnum_WebRemoteControl_ERemoteControlHttpVerbs()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteControlHttpVerbs"), 0, Get_Z_Construct_UEnum_WebRemoteControl_ERemoteControlHttpVerbs_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteControlHttpVerbs::None", (int64)ERemoteControlHttpVerbs::None },
				{ "ERemoteControlHttpVerbs::Get", (int64)ERemoteControlHttpVerbs::Get },
				{ "ERemoteControlHttpVerbs::Post", (int64)ERemoteControlHttpVerbs::Post },
				{ "ERemoteControlHttpVerbs::Put", (int64)ERemoteControlHttpVerbs::Put },
				{ "ERemoteControlHttpVerbs::Patch", (int64)ERemoteControlHttpVerbs::Patch },
				{ "ERemoteControlHttpVerbs::Delete", (int64)ERemoteControlHttpVerbs::Delete },
				{ "ERemoteControlHttpVerbs::Options", (int64)ERemoteControlHttpVerbs::Options },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Delete.Name", "ERemoteControlHttpVerbs::Delete" },
				{ "Get.Name", "ERemoteControlHttpVerbs::Get" },
				{ "ModuleRelativePath", "Private/RemoteControlRoute.h" },
				{ "None.Name", "ERemoteControlHttpVerbs::None" },
				{ "Options.Name", "ERemoteControlHttpVerbs::Options" },
				{ "Patch.Name", "ERemoteControlHttpVerbs::Patch" },
				{ "Post.Name", "ERemoteControlHttpVerbs::Post" },
				{ "Put.Name", "ERemoteControlHttpVerbs::Put" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_WebRemoteControl,
				nullptr,
				"ERemoteControlHttpVerbs",
				"ERemoteControlHttpVerbs",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRemoteControlRouteDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlRouteDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RemoteControlRouteDescription"), sizeof(FRemoteControlRouteDescription), Get_Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlRouteDescription>()
{
	return FRemoteControlRouteDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlRouteDescription(FRemoteControlRouteDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RemoteControlRouteDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRemoteControlRouteDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRemoteControlRouteDescription()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlRouteDescription>(FName(TEXT("RemoteControlRouteDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRemoteControlRouteDescription;
	struct Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Verb_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Verb_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Verb;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Utility struct to create a textual representation of an http route.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRoute.h" },
		{ "ToolTip", "Utility struct to create a textual representation of an http route." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlRouteDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Path_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Private/RemoteControlRoute.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlRouteDescription, Path), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRoute.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb = { "Verb", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlRouteDescription, Verb), Z_Construct_UEnum_WebRemoteControl_ERemoteControlHttpVerbs, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Description_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRoute.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlRouteDescription, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Description_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Path,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Verb,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::NewProp_Description,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlRouteDescription",
		sizeof(FRemoteControlRouteDescription),
		alignof(FRemoteControlRouteDescription),
		Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlRouteDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlRouteDescription"), sizeof(FRemoteControlRouteDescription), Get_Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlRouteDescription_Hash() { return 1164420334U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
