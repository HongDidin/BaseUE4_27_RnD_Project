// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlUI/Private/Factories/RemoteControlPresetActorFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlPresetActorFactory() {}
// Cross Module References
	REMOTECONTROLUI_API UClass* Z_Construct_UClass_URemoteControlPresetActorFactory_NoRegister();
	REMOTECONTROLUI_API UClass* Z_Construct_UClass_URemoteControlPresetActorFactory();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_RemoteControlUI();
// End Cross Module References
	void URemoteControlPresetActorFactory::StaticRegisterNativesURemoteControlPresetActorFactory()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlPresetActorFactory_NoRegister()
	{
		return URemoteControlPresetActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlPresetActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlUI,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/RemoteControlPresetActorFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/RemoteControlPresetActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlPresetActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::ClassParams = {
		&URemoteControlPresetActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000030ACu,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlPresetActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlPresetActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlPresetActorFactory, 2692719616);
	template<> REMOTECONTROLUI_API UClass* StaticClass<URemoteControlPresetActorFactory>()
	{
		return URemoteControlPresetActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlPresetActorFactory(Z_Construct_UClass_URemoteControlPresetActorFactory, &URemoteControlPresetActorFactory::StaticClass, TEXT("/Script/RemoteControlUI"), TEXT("URemoteControlPresetActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlPresetActorFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
