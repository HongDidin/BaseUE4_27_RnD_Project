// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLCOMMON_RCPropertyContainer_generated_h
#error "RCPropertyContainer.generated.h already included, missing '#pragma once' in RCPropertyContainer.h"
#endif
#define REMOTECONTROLCOMMON_RCPropertyContainer_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics; \
	static class UScriptStruct* StaticStruct();


template<> REMOTECONTROLCOMMON_API UScriptStruct* StaticStruct<struct FRCPropertyContainerKey>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURCPropertyContainerBase(); \
	friend struct Z_Construct_UClass_URCPropertyContainerBase_Statics; \
public: \
	DECLARE_CLASS(URCPropertyContainerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/RemoteControlCommon"), NO_API) \
	DECLARE_SERIALIZER(URCPropertyContainerBase)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURCPropertyContainerBase(); \
	friend struct Z_Construct_UClass_URCPropertyContainerBase_Statics; \
public: \
	DECLARE_CLASS(URCPropertyContainerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient), CASTCLASS_None, TEXT("/Script/RemoteControlCommon"), NO_API) \
	DECLARE_SERIALIZER(URCPropertyContainerBase)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URCPropertyContainerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URCPropertyContainerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URCPropertyContainerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URCPropertyContainerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URCPropertyContainerBase(URCPropertyContainerBase&&); \
	NO_API URCPropertyContainerBase(const URCPropertyContainerBase&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URCPropertyContainerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URCPropertyContainerBase(URCPropertyContainerBase&&); \
	NO_API URCPropertyContainerBase(const URCPropertyContainerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URCPropertyContainerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URCPropertyContainerBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URCPropertyContainerBase)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLCOMMON_API UClass* StaticClass<class URCPropertyContainerBase>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURCPropertyContainerRegistry(); \
	friend struct Z_Construct_UClass_URCPropertyContainerRegistry_Statics; \
public: \
	DECLARE_CLASS(URCPropertyContainerRegistry, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControlCommon"), NO_API) \
	DECLARE_SERIALIZER(URCPropertyContainerRegistry)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_INCLASS \
private: \
	static void StaticRegisterNativesURCPropertyContainerRegistry(); \
	friend struct Z_Construct_UClass_URCPropertyContainerRegistry_Statics; \
public: \
	DECLARE_CLASS(URCPropertyContainerRegistry, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RemoteControlCommon"), NO_API) \
	DECLARE_SERIALIZER(URCPropertyContainerRegistry)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URCPropertyContainerRegistry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URCPropertyContainerRegistry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URCPropertyContainerRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URCPropertyContainerRegistry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URCPropertyContainerRegistry(URCPropertyContainerRegistry&&); \
	NO_API URCPropertyContainerRegistry(const URCPropertyContainerRegistry&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URCPropertyContainerRegistry() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URCPropertyContainerRegistry(URCPropertyContainerRegistry&&); \
	NO_API URCPropertyContainerRegistry(const URCPropertyContainerRegistry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URCPropertyContainerRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URCPropertyContainerRegistry); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URCPropertyContainerRegistry)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedContainerClasses() { return STRUCT_OFFSET(URCPropertyContainerRegistry, CachedContainerClasses); }


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_84_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h_87_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLCOMMON_API UClass* StaticClass<class URCPropertyContainerRegistry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlCommon_Public_RCPropertyContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
