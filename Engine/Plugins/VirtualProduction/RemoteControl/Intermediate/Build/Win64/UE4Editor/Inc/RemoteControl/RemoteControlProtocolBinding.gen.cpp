// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlProtocolBinding.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolBinding() {}
// Cross Module References
	REMOTECONTROL_API UEnum* Z_Construct_UEnum_RemoteControl_ERCBindingStatus();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolBinding();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolEntity();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlPreset_NoRegister();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolMapping();
// End Cross Module References
	static UEnum* ERCBindingStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_RemoteControl_ERCBindingStatus, Z_Construct_UPackage__Script_RemoteControl(), TEXT("ERCBindingStatus"));
		}
		return Singleton;
	}
	template<> REMOTECONTROL_API UEnum* StaticEnum<ERCBindingStatus>()
	{
		return ERCBindingStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERCBindingStatus(ERCBindingStatus_StaticEnum, TEXT("/Script/RemoteControl"), TEXT("ERCBindingStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_RemoteControl_ERCBindingStatus_Hash() { return 2029970431U; }
	UEnum* Z_Construct_UEnum_RemoteControl_ERCBindingStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERCBindingStatus"), 0, Get_Z_Construct_UEnum_RemoteControl_ERCBindingStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERCBindingStatus::Unassigned", (int64)ERCBindingStatus::Unassigned },
				{ "ERCBindingStatus::Awaiting", (int64)ERCBindingStatus::Awaiting },
				{ "ERCBindingStatus::Bound", (int64)ERCBindingStatus::Bound },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Awaiting.Name", "ERCBindingStatus::Awaiting" },
				{ "Bound.Name", "ERCBindingStatus::Bound" },
				{ "Comment", "/**\n * Status of the binding \n */" },
				{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
				{ "ToolTip", "Status of the binding" },
				{ "Unassigned.Name", "ERCBindingStatus::Unassigned" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_RemoteControl,
				nullptr,
				"ERCBindingStatus",
				"ERCBindingStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRemoteControlProtocolBinding::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlProtocolBinding"), sizeof(FRemoteControlProtocolBinding), Get_Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlProtocolBinding>()
{
	return FRemoteControlProtocolBinding::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlProtocolBinding(FRemoteControlProtocolBinding::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlProtocolBinding"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolBinding
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolBinding()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlProtocolBinding>(FName(TEXT("RemoteControlProtocolBinding")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolBinding;
	struct Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtocolName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ProtocolName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PropertyId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MappingPropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MappingPropertyName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Struct which holds the bound struct and serialized struct archive\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Struct which holds the bound struct and serialized struct archive" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlProtocolBinding>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_Id_MetaData[] = {
		{ "Comment", "/** Binding Id */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Binding Id" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolBinding, Id), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_ProtocolName_MetaData[] = {
		{ "Comment", "/** Protocol name which we using for binding */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Protocol name which we using for binding" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_ProtocolName = { "ProtocolName", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolBinding, ProtocolName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_ProtocolName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_ProtocolName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_PropertyId_MetaData[] = {
		{ "Comment", "/** Property Unique ID */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Property Unique ID" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_PropertyId = { "PropertyId", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolBinding, PropertyId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_PropertyId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_PropertyId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_MappingPropertyName_MetaData[] = {
		{ "Comment", "/** Property name which we using for protocol range mapping */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Property name which we using for protocol range mapping" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_MappingPropertyName = { "MappingPropertyName", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolBinding, MappingPropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_MappingPropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_MappingPropertyName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_ProtocolName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_PropertyId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::NewProp_MappingPropertyName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlProtocolBinding",
		sizeof(FRemoteControlProtocolBinding),
		alignof(FRemoteControlProtocolBinding),
		Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolBinding()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlProtocolBinding"), sizeof(FRemoteControlProtocolBinding), Get_Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolBinding_Hash() { return 1510036219U; }
class UScriptStruct* FRemoteControlProtocolEntity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlProtocolEntity"), sizeof(FRemoteControlProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlProtocolEntity>()
{
	return FRemoteControlProtocolEntity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlProtocolEntity(FRemoteControlProtocolEntity::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlProtocolEntity"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolEntity
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolEntity()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlProtocolEntity>(FName(TEXT("RemoteControlProtocolEntity")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolEntity;
	struct Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Owner_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Owner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PropertyId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Mappings_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mappings_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_Mappings;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BindingStatus_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BindingStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * These structures serve both as properties mapping as well as UI generation\n * Protocols should implement it based on the parameters they need.\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "These structures serve both as properties mapping as well as UI generation\nProtocols should implement it based on the parameters they need." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlProtocolEntity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Owner_MetaData[] = {
		{ "Comment", "/** The preset that owns this entity. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "The preset that owns this entity." },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Owner = { "Owner", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolEntity, Owner), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Owner_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Owner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_PropertyId_MetaData[] = {
		{ "Comment", "/** Exposed property Id */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Exposed property Id" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_PropertyId = { "PropertyId", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolEntity, PropertyId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_PropertyId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_PropertyId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings_ElementProp = { "Mappings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRemoteControlProtocolMapping, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings_MetaData[] = {
		{ "Comment", "/** \n\x09 * Property mapping ranges set\n\x09 * Stores protocol mapping for this protocol binding entity\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Property mapping ranges set\nStores protocol mapping for this protocol binding entity" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings = { "Mappings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolEntity, Mappings), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus_MetaData[] = {
		{ "Comment", "/** Binding status of this protocol entity */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Binding status of this protocol entity" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus = { "BindingStatus", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolEntity, BindingStatus), Z_Construct_UEnum_RemoteControl_ERCBindingStatus, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Owner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_PropertyId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_Mappings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::NewProp_BindingStatus,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlProtocolEntity",
		sizeof(FRemoteControlProtocolEntity),
		alignof(FRemoteControlProtocolEntity),
		Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolEntity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlProtocolEntity"), sizeof(FRemoteControlProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolEntity_Hash() { return 2589197952U; }
class UScriptStruct* FRemoteControlProtocolMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlProtocolMapping"), sizeof(FRemoteControlProtocolMapping), Get_Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlProtocolMapping>()
{
	return FRemoteControlProtocolMapping::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlProtocolMapping(FRemoteControlProtocolMapping::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlProtocolMapping"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolMapping
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolMapping()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlProtocolMapping>(FName(TEXT("RemoteControlProtocolMapping")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlProtocolMapping;
	struct Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationRangePropertyData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationRangePropertyData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InterpolationRangePropertyData;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationMappingPropertyData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationMappingPropertyData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InterpolationMappingPropertyData;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationRangePropertyDataCache_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationRangePropertyDataCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InterpolationRangePropertyDataCache;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationMappingPropertyDataCache_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationMappingPropertyDataCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InterpolationMappingPropertyDataCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationMappingPropertyElementNum_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InterpolationMappingPropertyElementNum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundPropertyPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_BoundPropertyPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/*\n * Mapping of the range of the values for the protocol\n * This class holds a generic range buffer.\n * For example, it could be FFloatProperty 4 bytes\n * Or it could be any UScripStruct, like FVector - 12 bytes\n * Or any custom struct, arrays, maps, sets, or primitive properties\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "* Mapping of the range of the values for the protocol\n* This class holds a generic range buffer.\n* For example, it could be FFloatProperty 4 bytes\n* Or it could be any UScripStruct, like FVector - 12 bytes\n* Or any custom struct, arrays, maps, sets, or primitive properties" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlProtocolMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_Id_MetaData[] = {
		{ "Comment", "/** Unique Id of the current binding */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Unique Id of the current binding" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, Id), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData_Inner = { "InterpolationRangePropertyData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData_MetaData[] = {
		{ "Comment", "/**\n\x09 * The current integer value of the mapping range. It could be a float, int32, uint8, etc.\n\x09 * That is based on protocol input data.\n\x09 * \n\x09 *  For example, it could be uint8 in the case of one-byte mapping. In this case, the range value could be from 0 up to 255, which bound to InterpolationMappingPropertyData\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "The current integer value of the mapping range. It could be a float, int32, uint8, etc.\nThat is based on protocol input data.\n\n For example, it could be uint8 in the case of one-byte mapping. In this case, the range value could be from 0 up to 255, which bound to InterpolationMappingPropertyData" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData = { "InterpolationRangePropertyData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationRangePropertyData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData_Inner = { "InterpolationMappingPropertyData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData_MetaData[] = {
		{ "Comment", "/** \n\x09 * Holds the serialized mapped property data. \n\x09 * It could be a primitive value for FNumericProperty, or a text representation for a struct.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Holds the serialized mapped property data.\nIt could be a primitive value for FNumericProperty, or a text representation for a struct." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData = { "InterpolationMappingPropertyData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationMappingPropertyData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache_Inner = { "InterpolationRangePropertyDataCache", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache_MetaData[] = {
		{ "Comment", "/** \n\x09* Holds the range property data buffer. \n\x09* This is the cached raw data as opposed to the serialized data.\n\x09*/" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Holds the range property data buffer.\nThis is the cached raw data as opposed to the serialized data." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache = { "InterpolationRangePropertyDataCache", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationRangePropertyDataCache), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache_Inner = { "InterpolationMappingPropertyDataCache", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache_MetaData[] = {
		{ "Comment", "/** \n\x09* Holds the mapped property data buffer. \n\x09* This is the cached raw data as opposed to the serialized data, so it doesn't contain type information.\n\x09*/" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Holds the mapped property data buffer.\nThis is the cached raw data as opposed to the serialized data, so it doesn't contain type information." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache = { "InterpolationMappingPropertyDataCache", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationMappingPropertyDataCache), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyElementNum_MetaData[] = {
		{ "Comment", "/** The element count if the Mapping is an array. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "The element count if the Mapping is an array." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyElementNum = { "InterpolationMappingPropertyElementNum", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, InterpolationMappingPropertyElementNum), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyElementNum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyElementNum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_BoundPropertyPath_MetaData[] = {
		{ "Comment", "/** Holds the bound property path */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolBinding.h" },
		{ "ToolTip", "Holds the bound property path" },
	};
#endif
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_BoundPropertyPath = { "BoundPropertyPath", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlProtocolMapping, BoundPropertyPath), &FProperty::StaticClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_BoundPropertyPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_BoundPropertyPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationRangePropertyDataCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyDataCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_InterpolationMappingPropertyElementNum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::NewProp_BoundPropertyPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlProtocolMapping",
		sizeof(FRemoteControlProtocolMapping),
		alignof(FRemoteControlProtocolMapping),
		Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlProtocolMapping"), sizeof(FRemoteControlProtocolMapping), Get_Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlProtocolMapping_Hash() { return 1712282504U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
