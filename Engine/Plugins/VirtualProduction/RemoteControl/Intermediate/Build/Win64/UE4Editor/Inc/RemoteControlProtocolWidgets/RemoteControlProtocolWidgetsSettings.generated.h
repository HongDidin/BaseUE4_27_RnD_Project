// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLPROTOCOLWIDGETS_RemoteControlProtocolWidgetsSettings_generated_h
#error "RemoteControlProtocolWidgetsSettings.generated.h already included, missing '#pragma once' in RemoteControlProtocolWidgetsSettings.h"
#endif
#define REMOTECONTROLPROTOCOLWIDGETS_RemoteControlProtocolWidgetsSettings_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemoteControlProtocolWidgetsSettings(); \
	friend struct Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics; \
public: \
	DECLARE_CLASS(URemoteControlProtocolWidgetsSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlProtocolWidgets"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlProtocolWidgetsSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("RemoteControlProtocolWidgets");} \



#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURemoteControlProtocolWidgetsSettings(); \
	friend struct Z_Construct_UClass_URemoteControlProtocolWidgetsSettings_Statics; \
public: \
	DECLARE_CLASS(URemoteControlProtocolWidgetsSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControlProtocolWidgets"), NO_API) \
	DECLARE_SERIALIZER(URemoteControlProtocolWidgetsSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("RemoteControlProtocolWidgets");} \



#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlProtocolWidgetsSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlProtocolWidgetsSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlProtocolWidgetsSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlProtocolWidgetsSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlProtocolWidgetsSettings(URemoteControlProtocolWidgetsSettings&&); \
	NO_API URemoteControlProtocolWidgetsSettings(const URemoteControlProtocolWidgetsSettings&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemoteControlProtocolWidgetsSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemoteControlProtocolWidgetsSettings(URemoteControlProtocolWidgetsSettings&&); \
	NO_API URemoteControlProtocolWidgetsSettings(const URemoteControlProtocolWidgetsSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemoteControlProtocolWidgetsSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemoteControlProtocolWidgetsSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemoteControlProtocolWidgetsSettings)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROLPROTOCOLWIDGETS_API UClass* StaticClass<class URemoteControlProtocolWidgetsSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControlProtocolWidgets_Private_RemoteControlProtocolWidgetsSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
