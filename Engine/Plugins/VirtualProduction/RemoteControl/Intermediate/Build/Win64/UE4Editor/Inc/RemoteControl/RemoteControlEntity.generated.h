// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlEntity_generated_h
#error "RemoteControlEntity.generated.h already included, missing '#pragma once' in RemoteControlEntity.h"
#endif
#define REMOTECONTROL_RemoteControlEntity_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlEntity_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlEntity_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__UserMetadata() { return STRUCT_OFFSET(FRemoteControlEntity, UserMetadata); } \
	FORCEINLINE static uint32 __PPO__Bindings() { return STRUCT_OFFSET(FRemoteControlEntity, Bindings); } \
	FORCEINLINE static uint32 __PPO__Owner() { return STRUCT_OFFSET(FRemoteControlEntity, Owner); } \
	FORCEINLINE static uint32 __PPO__Label() { return STRUCT_OFFSET(FRemoteControlEntity, Label); } \
	FORCEINLINE static uint32 __PPO__Id() { return STRUCT_OFFSET(FRemoteControlEntity, Id); }


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlEntity>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlEntity_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
