// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlField_generated_h
#error "RemoteControlField.generated.h already included, missing '#pragma once' in RemoteControlField.h"
#endif
#define REMOTECONTROL_RemoteControlField_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlField_h_182_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlFunction_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bIsCallableInPackaged() { return STRUCT_OFFSET(FRemoteControlFunction, bIsCallableInPackaged); } \
	FORCEINLINE static uint32 __PPO__FunctionPath() { return STRUCT_OFFSET(FRemoteControlFunction, FunctionPath); } \
	typedef FRemoteControlField Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlFunction>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlField_h_115_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlProperty_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bIsEditableInPackaged() { return STRUCT_OFFSET(FRemoteControlProperty, bIsEditableInPackaged); } \
	typedef FRemoteControlField Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlProperty>();

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlField_h_29_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlField_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__OwnerClass() { return STRUCT_OFFSET(FRemoteControlField, OwnerClass); } \
	FORCEINLINE static uint32 __PPO__bIsEditorOnly() { return STRUCT_OFFSET(FRemoteControlField, bIsEditorOnly); } \
	typedef FRemoteControlEntity Super;


template<> REMOTECONTROL_API UScriptStruct* StaticStruct<struct FRemoteControlField>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlField_h


#define FOREACH_ENUM_EEXPOSEDFIELDTYPE(op) \
	op(EExposedFieldType::Invalid) \
	op(EExposedFieldType::Property) \
	op(EExposedFieldType::Function) 

enum class EExposedFieldType : uint8;
template<> REMOTECONTROL_API UEnum* StaticEnum<EExposedFieldType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
