// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlCommon/Public/RemoteControlSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlSettings() {}
// Cross Module References
	REMOTECONTROLCOMMON_API UClass* Z_Construct_UClass_URemoteControlSettings_NoRegister();
	REMOTECONTROLCOMMON_API UClass* Z_Construct_UClass_URemoteControlSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_RemoteControlCommon();
// End Cross Module References
	void URemoteControlSettings::StaticRegisterNativesURemoteControlSettings()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlSettings_NoRegister()
	{
		return URemoteControlSettings::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bProtocolsGenerateTransactions_MetaData[];
#endif
		static void NewProp_bProtocolsGenerateTransactions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bProtocolsGenerateTransactions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlWebInterfacePort_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_RemoteControlWebInterfacePort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceWebAppBuildAtStartup_MetaData[];
#endif
		static void NewProp_bForceWebAppBuildAtStartup_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceWebAppBuildAtStartup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoStartWebServer_MetaData[];
#endif
		static void NewProp_bAutoStartWebServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoStartWebServer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoStartWebSocketServer_MetaData[];
#endif
		static void NewProp_bAutoStartWebSocketServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoStartWebSocketServer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlHttpServerPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_RemoteControlHttpServerPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteControlWebSocketServerPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_RemoteControlWebSocketServerPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayInEditorOnlyWarnings_MetaData[];
#endif
		static void NewProp_bDisplayInEditorOnlyWarnings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayInEditorOnlyWarnings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TreeBindingSplitRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TreeBindingSplitRatio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlCommon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Global remote control settings\n */" },
		{ "IncludePath", "RemoteControlSettings.h" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "Global remote control settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions_MetaData[] = {
		{ "Category", "RemoteControl" },
		{ "Comment", "/**\n\x09 * Should transactions be generated for events received through protocols (ie. MIDI, DMX etc.)\n\x09 * Disabling transactions improves performance but will prevent events from being transacted to Multi-User\n\x09 * unless using the Remote Control Interception feature.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "Should transactions be generated for events received through protocols (ie. MIDI, DMX etc.)\nDisabling transactions improves performance but will prevent events from being transacted to Multi-User\nunless using the Remote Control Interception feature." },
	};
#endif
	void Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions_SetBit(void* Obj)
	{
		((URemoteControlSettings*)Obj)->bProtocolsGenerateTransactions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions = { "bProtocolsGenerateTransactions", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteControlSettings), &Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebInterfacePort_MetaData[] = {
		{ "Category", "Remote Control Web Interface" },
		{ "Comment", "/** The remote control web app http port. */" },
		{ "DisplayName", "Remote Control Web Interface http Port" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "The remote control web app http port." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebInterfacePort = { "RemoteControlWebInterfacePort", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlSettings, RemoteControlWebInterfacePort), METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebInterfacePort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebInterfacePort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup_MetaData[] = {
		{ "Category", "Remote Control Web Interface" },
		{ "Comment", "/** Should force a build of the WebApp at startup. */" },
		{ "DisplayName", "Force WebApp build at startup" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "Should force a build of the WebApp at startup." },
	};
#endif
	void Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup_SetBit(void* Obj)
	{
		((URemoteControlSettings*)Obj)->bForceWebAppBuildAtStartup = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup = { "bForceWebAppBuildAtStartup", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteControlSettings), &Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer_MetaData[] = {
		{ "Category", "Remote Control Web Server" },
		{ "Comment", "/** Whether web server is started automatically. */" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "Whether web server is started automatically." },
	};
#endif
	void Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer_SetBit(void* Obj)
	{
		((URemoteControlSettings*)Obj)->bAutoStartWebServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer = { "bAutoStartWebServer", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteControlSettings), &Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer_MetaData[] = {
		{ "Category", "Remote Control Web Server" },
		{ "Comment", "/** Whether web socket server is started automatically. */" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "Whether web socket server is started automatically." },
	};
#endif
	void Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer_SetBit(void* Obj)
	{
		((URemoteControlSettings*)Obj)->bAutoStartWebSocketServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer = { "bAutoStartWebSocketServer", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteControlSettings), &Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlHttpServerPort_MetaData[] = {
		{ "Category", "Remote Control Web Server" },
		{ "Comment", "/** The web remote control HTTP server's port. */" },
		{ "DisplayName", "Remote Control HTTP Server Port" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "The web remote control HTTP server's port." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlHttpServerPort = { "RemoteControlHttpServerPort", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlSettings, RemoteControlHttpServerPort), METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlHttpServerPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlHttpServerPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebSocketServerPort_MetaData[] = {
		{ "Category", "Remote Control Web Server" },
		{ "Comment", "/** The web remote control WebSocket server's port. */" },
		{ "DisplayName", "Remote Control WebSocket Server Port" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "The web remote control WebSocket server's port." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebSocketServerPort = { "RemoteControlWebSocketServerPort", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlSettings, RemoteControlWebSocketServerPort), METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebSocketServerPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebSocketServerPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings_MetaData[] = {
		{ "Category", "Remote Control Preset" },
		{ "Comment", "/** Show a warning icon for exposed editor-only fields. */" },
		{ "DisplayName", "Show a warning when exposing editor-only entities." },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "Show a warning icon for exposed editor-only fields." },
	};
#endif
	void Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings_SetBit(void* Obj)
	{
		((URemoteControlSettings*)Obj)->bDisplayInEditorOnlyWarnings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings = { "bDisplayInEditorOnlyWarnings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoteControlSettings), &Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_TreeBindingSplitRatio_MetaData[] = {
		{ "Comment", "/** The split widget control ratio between entity tree and details/protocol binding list. */" },
		{ "ModuleRelativePath", "Public/RemoteControlSettings.h" },
		{ "ToolTip", "The split widget control ratio between entity tree and details/protocol binding list." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_TreeBindingSplitRatio = { "TreeBindingSplitRatio", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlSettings, TreeBindingSplitRatio), METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_TreeBindingSplitRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_TreeBindingSplitRatio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bProtocolsGenerateTransactions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebInterfacePort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bForceWebAppBuildAtStartup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bAutoStartWebSocketServer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlHttpServerPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_RemoteControlWebSocketServerPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_bDisplayInEditorOnlyWarnings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlSettings_Statics::NewProp_TreeBindingSplitRatio,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlSettings_Statics::ClassParams = {
		&URemoteControlSettings::StaticClass,
		"RemoteControl",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlSettings, 2809976910);
	template<> REMOTECONTROLCOMMON_API UClass* StaticClass<URemoteControlSettings>()
	{
		return URemoteControlSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlSettings(Z_Construct_UClass_URemoteControlSettings, &URemoteControlSettings::StaticClass, TEXT("/Script/RemoteControlCommon"), TEXT("URemoteControlSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
