// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WebRemoteControl/Private/RemoteControlRequest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlRequest() {}
// Cross Module References
	WEBREMOTECONTROL_API UEnum* Z_Construct_UEnum_WebRemoteControl_ERemoteControlEvent();
	UPackage* Z_Construct_UPackage__Script_WebRemoteControl();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCWebSocketRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FGetObjectThumbnailRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSetEntityLabelRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSetEntityMetadataRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSetPresetMetadataRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSearchObjectRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSearchActorRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FSearchAssetRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCAssetFilter();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FDescribeObjectRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetCallRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCObjectRequest();
	REMOTECONTROL_API UEnum* Z_Construct_UEnum_RemoteControl_ERCAccess();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCCallRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCBatchRequest();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCRequestWrapper();
// End Cross Module References
	static UEnum* ERemoteControlEvent_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_WebRemoteControl_ERemoteControlEvent, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("ERemoteControlEvent"));
		}
		return Singleton;
	}
	template<> WEBREMOTECONTROL_API UEnum* StaticEnum<ERemoteControlEvent>()
	{
		return ERemoteControlEvent_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERemoteControlEvent(ERemoteControlEvent_StaticEnum, TEXT("/Script/WebRemoteControl"), TEXT("ERemoteControlEvent"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_WebRemoteControl_ERemoteControlEvent_Hash() { return 2022980136U; }
	UEnum* Z_Construct_UEnum_WebRemoteControl_ERemoteControlEvent()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERemoteControlEvent"), 0, Get_Z_Construct_UEnum_WebRemoteControl_ERemoteControlEvent_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERemoteControlEvent::PreObjectPropertyChanged", (int64)ERemoteControlEvent::PreObjectPropertyChanged },
				{ "ERemoteControlEvent::ObjectPropertyChanged", (int64)ERemoteControlEvent::ObjectPropertyChanged },
				{ "ERemoteControlEvent::EventCount", (int64)ERemoteControlEvent::EventCount },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "EventCount.Name", "ERemoteControlEvent::EventCount" },
				{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
				{ "ObjectPropertyChanged.Name", "ERemoteControlEvent::ObjectPropertyChanged" },
				{ "PreObjectPropertyChanged.Name", "ERemoteControlEvent::PreObjectPropertyChanged" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_WebRemoteControl,
				nullptr,
				"ERemoteControlEvent",
				"ERemoteControlEvent",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FRCWebSocketPresetRegisterBody>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCWebSocketPresetRegisterBody cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCWebSocketPresetRegisterBody::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCWebSocketPresetRegisterBody"), sizeof(FRCWebSocketPresetRegisterBody), Get_Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCWebSocketPresetRegisterBody>()
{
	return FRCWebSocketPresetRegisterBody::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCWebSocketPresetRegisterBody(FRCWebSocketPresetRegisterBody::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCWebSocketPresetRegisterBody"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCWebSocketPresetRegisterBody
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCWebSocketPresetRegisterBody()
	{
		UScriptStruct::DeferCppStructOps<FRCWebSocketPresetRegisterBody>(FName(TEXT("RCWebSocketPresetRegisterBody")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCWebSocketPresetRegisterBody;
	struct Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PresetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IgnoreRemoteChanges_MetaData[];
#endif
		static void NewProp_IgnoreRemoteChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IgnoreRemoteChanges;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request made for web socket.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request made for web socket." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCWebSocketPresetRegisterBody>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_PresetName_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the preset its registering.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Name of the preset its registering." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_PresetName = { "PresetName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCWebSocketPresetRegisterBody, PresetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_PresetName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_PresetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges_MetaData[] = {
		{ "Comment", "/** Whether changes to properties triggered remotely should fire an event. */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Whether changes to properties triggered remotely should fire an event." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges_SetBit(void* Obj)
	{
		((FRCWebSocketPresetRegisterBody*)Obj)->IgnoreRemoteChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges = { "IgnoreRemoteChanges", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCWebSocketPresetRegisterBody), &Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_PresetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::NewProp_IgnoreRemoteChanges,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCWebSocketPresetRegisterBody",
		sizeof(FRCWebSocketPresetRegisterBody),
		alignof(FRCWebSocketPresetRegisterBody),
		Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCWebSocketPresetRegisterBody"), sizeof(FRCWebSocketPresetRegisterBody), Get_Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCWebSocketPresetRegisterBody_Hash() { return 3936020445U; }

static_assert(std::is_polymorphic<FRCWebSocketRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCWebSocketRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCWebSocketRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCWebSocketRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCWebSocketRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCWebSocketRequest"), sizeof(FRCWebSocketRequest), Get_Z_Construct_UScriptStruct_FRCWebSocketRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCWebSocketRequest>()
{
	return FRCWebSocketRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCWebSocketRequest(FRCWebSocketRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCWebSocketRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCWebSocketRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCWebSocketRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCWebSocketRequest>(FName(TEXT("RCWebSocketRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCWebSocketRequest;
	struct Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MessageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request made for web socket.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request made for web socket." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCWebSocketRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_MessageName_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the websocket message.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Name of the websocket message." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_MessageName = { "MessageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCWebSocketRequest, MessageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_MessageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_MessageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_Id_MetaData[] = {
		{ "Comment", "/**\n\x09 * (Optional) Id of the incoming message, used to identify a deferred response to the clients.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "(Optional) Id of the incoming message, used to identify a deferred response to the clients." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCWebSocketRequest, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_MessageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::NewProp_Id,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCWebSocketRequest",
		sizeof(FRCWebSocketRequest),
		alignof(FRCWebSocketRequest),
		Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCWebSocketRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCWebSocketRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCWebSocketRequest"), sizeof(FRCWebSocketRequest), Get_Z_Construct_UScriptStruct_FRCWebSocketRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCWebSocketRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCWebSocketRequest_Hash() { return 1037609708U; }

static_assert(std::is_polymorphic<FGetObjectThumbnailRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FGetObjectThumbnailRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FGetObjectThumbnailRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGetObjectThumbnailRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("GetObjectThumbnailRequest"), sizeof(FGetObjectThumbnailRequest), Get_Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FGetObjectThumbnailRequest>()
{
	return FGetObjectThumbnailRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGetObjectThumbnailRequest(FGetObjectThumbnailRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("GetObjectThumbnailRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetObjectThumbnailRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFGetObjectThumbnailRequest()
	{
		UScriptStruct::DeferCppStructOps<FGetObjectThumbnailRequest>(FName(TEXT("GetObjectThumbnailRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFGetObjectThumbnailRequest;
	struct Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to get an asset's thumbnail.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to get an asset's thumbnail." },
	};
#endif
	void* Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGetObjectThumbnailRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::NewProp_ObjectPath_MetaData[] = {
		{ "Comment", "/**\n\x09 * The target object's path.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The target object's path." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::NewProp_ObjectPath = { "ObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGetObjectThumbnailRequest, ObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::NewProp_ObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::NewProp_ObjectPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::NewProp_ObjectPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"GetObjectThumbnailRequest",
		sizeof(FGetObjectThumbnailRequest),
		alignof(FGetObjectThumbnailRequest),
		Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGetObjectThumbnailRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GetObjectThumbnailRequest"), sizeof(FGetObjectThumbnailRequest), Get_Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGetObjectThumbnailRequest_Hash() { return 551380077U; }

static_assert(std::is_polymorphic<FSetEntityLabelRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FSetEntityLabelRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FSetEntityLabelRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSetEntityLabelRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSetEntityLabelRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SetEntityLabelRequest"), sizeof(FSetEntityLabelRequest), Get_Z_Construct_UScriptStruct_FSetEntityLabelRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSetEntityLabelRequest>()
{
	return FSetEntityLabelRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSetEntityLabelRequest(FSetEntityLabelRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SetEntityLabelRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityLabelRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityLabelRequest()
	{
		UScriptStruct::DeferCppStructOps<FSetEntityLabelRequest>(FName(TEXT("SetEntityLabelRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityLabelRequest;
	struct Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewLabel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to set an entity's label.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to set an entity's label." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSetEntityLabelRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::NewProp_NewLabel_MetaData[] = {
		{ "Comment", "/**\n\x09 * The new label to assign.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The new label to assign." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::NewProp_NewLabel = { "NewLabel", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSetEntityLabelRequest, NewLabel), METADATA_PARAMS(Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::NewProp_NewLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::NewProp_NewLabel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::NewProp_NewLabel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"SetEntityLabelRequest",
		sizeof(FSetEntityLabelRequest),
		alignof(FSetEntityLabelRequest),
		Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSetEntityLabelRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSetEntityLabelRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SetEntityLabelRequest"), sizeof(FSetEntityLabelRequest), Get_Z_Construct_UScriptStruct_FSetEntityLabelRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSetEntityLabelRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSetEntityLabelRequest_Hash() { return 1235893370U; }

static_assert(std::is_polymorphic<FSetEntityMetadataRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FSetEntityMetadataRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FSetEntityMetadataRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSetEntityMetadataRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SetEntityMetadataRequest"), sizeof(FSetEntityMetadataRequest), Get_Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSetEntityMetadataRequest>()
{
	return FSetEntityMetadataRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSetEntityMetadataRequest(FSetEntityMetadataRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SetEntityMetadataRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityMetadataRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityMetadataRequest()
	{
		UScriptStruct::DeferCppStructOps<FSetEntityMetadataRequest>(FName(TEXT("SetEntityMetadataRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSetEntityMetadataRequest;
	struct Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to set a metadata field.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to set a metadata field." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSetEntityMetadataRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::NewProp_Value_MetaData[] = {
		{ "Comment", "/**\n\x09 * The new value for the metadata field.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The new value for the metadata field." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSetEntityMetadataRequest, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"SetEntityMetadataRequest",
		sizeof(FSetEntityMetadataRequest),
		alignof(FSetEntityMetadataRequest),
		Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSetEntityMetadataRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SetEntityMetadataRequest"), sizeof(FSetEntityMetadataRequest), Get_Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSetEntityMetadataRequest_Hash() { return 2502238952U; }

static_assert(std::is_polymorphic<FSetPresetMetadataRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FSetPresetMetadataRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FSetPresetMetadataRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSetPresetMetadataRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SetPresetMetadataRequest"), sizeof(FSetPresetMetadataRequest), Get_Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSetPresetMetadataRequest>()
{
	return FSetPresetMetadataRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSetPresetMetadataRequest(FSetPresetMetadataRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SetPresetMetadataRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetPresetMetadataRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSetPresetMetadataRequest()
	{
		UScriptStruct::DeferCppStructOps<FSetPresetMetadataRequest>(FName(TEXT("SetPresetMetadataRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSetPresetMetadataRequest;
	struct Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to set a metadata field.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to set a metadata field." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSetPresetMetadataRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::NewProp_Value_MetaData[] = {
		{ "Comment", "/**\n\x09 * The new value for the metadata field.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The new value for the metadata field." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSetPresetMetadataRequest, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"SetPresetMetadataRequest",
		sizeof(FSetPresetMetadataRequest),
		alignof(FSetPresetMetadataRequest),
		Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSetPresetMetadataRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SetPresetMetadataRequest"), sizeof(FSetPresetMetadataRequest), Get_Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSetPresetMetadataRequest_Hash() { return 3693083946U; }

static_assert(std::is_polymorphic<FSearchObjectRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FSearchObjectRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FSearchObjectRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSearchObjectRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSearchObjectRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SearchObjectRequest"), sizeof(FSearchObjectRequest), Get_Z_Construct_UScriptStruct_FSearchObjectRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSearchObjectRequest>()
{
	return FSearchObjectRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSearchObjectRequest(FSearchObjectRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SearchObjectRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchObjectRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchObjectRequest()
	{
		UScriptStruct::DeferCppStructOps<FSearchObjectRequest>(FName(TEXT("SearchObjectRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchObjectRequest;
	struct Z_Construct_UScriptStruct_FSearchObjectRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Query_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Query;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Class;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Outer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Outer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Limit_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Limit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to search for an asset.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to search for an asset." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSearchObjectRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Query_MetaData[] = {
		{ "Comment", "/*\n\x09 * The search query.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "* The search query." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Query = { "Query", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchObjectRequest, Query), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Query_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Query_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Class_MetaData[] = {
		{ "Comment", "/**\n\x09 * The target object's class.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The target object's class." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchObjectRequest, Class), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Class_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Outer_MetaData[] = {
		{ "Comment", "/**\n\x09 * The search target's outer object.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The search target's outer object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Outer = { "Outer", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchObjectRequest, Outer), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Outer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Outer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Limit_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum number of search results returned.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The maximum number of search results returned." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Limit = { "Limit", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchObjectRequest, Limit), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Limit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Limit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Query,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Outer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::NewProp_Limit,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"SearchObjectRequest",
		sizeof(FSearchObjectRequest),
		alignof(FSearchObjectRequest),
		Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSearchObjectRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSearchObjectRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SearchObjectRequest"), sizeof(FSearchObjectRequest), Get_Z_Construct_UScriptStruct_FSearchObjectRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSearchObjectRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSearchObjectRequest_Hash() { return 4010362548U; }

static_assert(std::is_polymorphic<FSearchActorRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FSearchActorRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FSearchActorRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSearchActorRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSearchActorRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SearchActorRequest"), sizeof(FSearchActorRequest), Get_Z_Construct_UScriptStruct_FSearchActorRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSearchActorRequest>()
{
	return FSearchActorRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSearchActorRequest(FSearchActorRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SearchActorRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchActorRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchActorRequest()
	{
		UScriptStruct::DeferCppStructOps<FSearchActorRequest>(FName(TEXT("SearchActorRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchActorRequest;
	struct Z_Construct_UScriptStruct_FSearchActorRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Query_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Query;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Class;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Limit_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Limit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchActorRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to search for an actor.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to search for an actor." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSearchActorRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Query_MetaData[] = {
		{ "Comment", "/*\n\x09 * The search query.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "* The search query." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Query = { "Query", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchActorRequest, Query), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Query_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Query_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Class_MetaData[] = {
		{ "Comment", "/**\n\x09 * The target actor's class. \n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The target actor's class." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchActorRequest, Class), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Class_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Limit_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum number of search results returned.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The maximum number of search results returned." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Limit = { "Limit", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchActorRequest, Limit), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Limit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Limit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSearchActorRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Query,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchActorRequest_Statics::NewProp_Limit,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSearchActorRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"SearchActorRequest",
		sizeof(FSearchActorRequest),
		alignof(FSearchActorRequest),
		Z_Construct_UScriptStruct_FSearchActorRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchActorRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSearchActorRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSearchActorRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SearchActorRequest"), sizeof(FSearchActorRequest), Get_Z_Construct_UScriptStruct_FSearchActorRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSearchActorRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSearchActorRequest_Hash() { return 1264147654U; }

static_assert(std::is_polymorphic<FSearchAssetRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FSearchAssetRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FSearchAssetRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FSearchAssetRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSearchAssetRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("SearchAssetRequest"), sizeof(FSearchAssetRequest), Get_Z_Construct_UScriptStruct_FSearchAssetRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FSearchAssetRequest>()
{
	return FSearchAssetRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSearchAssetRequest(FSearchAssetRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("SearchAssetRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchAssetRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchAssetRequest()
	{
		UScriptStruct::DeferCppStructOps<FSearchAssetRequest>(FName(TEXT("SearchAssetRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFSearchAssetRequest;
	struct Z_Construct_UScriptStruct_FSearchAssetRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Query_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Query;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Filter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Limit_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Limit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to search for an asset.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to search for an asset." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSearchAssetRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Query_MetaData[] = {
		{ "Comment", "/**\n\x09 * The search query which will be compared with the asset names.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The search query which will be compared with the asset names." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Query = { "Query", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchAssetRequest, Query), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Query_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Query_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Filter_MetaData[] = {
		{ "Comment", "/*\n\x09 * The filter applied to this search.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "* The filter applied to this search." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchAssetRequest, Filter), Z_Construct_UScriptStruct_FRCAssetFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Filter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Filter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Limit_MetaData[] = {
		{ "Comment", "/**\n\x09 * The maximum number of search results returned.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The maximum number of search results returned." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Limit = { "Limit", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSearchAssetRequest, Limit), METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Limit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Limit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Query,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Filter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::NewProp_Limit,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"SearchAssetRequest",
		sizeof(FSearchAssetRequest),
		alignof(FSearchAssetRequest),
		Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSearchAssetRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSearchAssetRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SearchAssetRequest"), sizeof(FSearchAssetRequest), Get_Z_Construct_UScriptStruct_FSearchAssetRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSearchAssetRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSearchAssetRequest_Hash() { return 2810313946U; }

static_assert(std::is_polymorphic<FDescribeObjectRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FDescribeObjectRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FDescribeObjectRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FDescribeObjectRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDescribeObjectRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("DescribeObjectRequest"), sizeof(FDescribeObjectRequest), Get_Z_Construct_UScriptStruct_FDescribeObjectRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FDescribeObjectRequest>()
{
	return FDescribeObjectRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDescribeObjectRequest(FDescribeObjectRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("DescribeObjectRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFDescribeObjectRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFDescribeObjectRequest()
	{
		UScriptStruct::DeferCppStructOps<FDescribeObjectRequest>(FName(TEXT("DescribeObjectRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFDescribeObjectRequest;
	struct Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to describe an object using its path.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to describe an object using its path." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDescribeObjectRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::NewProp_ObjectPath_MetaData[] = {
		{ "Comment", "/**\n\x09 * The target object's path.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The target object's path." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::NewProp_ObjectPath = { "ObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDescribeObjectRequest, ObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::NewProp_ObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::NewProp_ObjectPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::NewProp_ObjectPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"DescribeObjectRequest",
		sizeof(FDescribeObjectRequest),
		alignof(FDescribeObjectRequest),
		Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDescribeObjectRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDescribeObjectRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DescribeObjectRequest"), sizeof(FDescribeObjectRequest), Get_Z_Construct_UScriptStruct_FDescribeObjectRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDescribeObjectRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDescribeObjectRequest_Hash() { return 2741645633U; }

static_assert(std::is_polymorphic<FRCPresetCallRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCPresetCallRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCPresetCallRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetCallRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetCallRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetCallRequest"), sizeof(FRCPresetCallRequest), Get_Z_Construct_UScriptStruct_FRCPresetCallRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetCallRequest>()
{
	return FRCPresetCallRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetCallRequest(FRCPresetCallRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetCallRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetCallRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetCallRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetCallRequest>(FName(TEXT("RCPresetCallRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetCallRequest;
	struct Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateTransaction_MetaData[];
#endif
		static void NewProp_GenerateTransaction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GenerateTransaction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to call a function on a preset\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to call a function on a preset" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetCallRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether a transaction should be created for the call.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Whether a transaction should be created for the call." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction_SetBit(void* Obj)
	{
		((FRCPresetCallRequest*)Obj)->GenerateTransaction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction = { "GenerateTransaction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCPresetCallRequest), &Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::NewProp_GenerateTransaction,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCPresetCallRequest",
		sizeof(FRCPresetCallRequest),
		alignof(FRCPresetCallRequest),
		Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetCallRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetCallRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetCallRequest"), sizeof(FRCPresetCallRequest), Get_Z_Construct_UScriptStruct_FRCPresetCallRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetCallRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetCallRequest_Hash() { return 1277243597U; }

static_assert(std::is_polymorphic<FRCPresetSetPropertyRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCPresetSetPropertyRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCPresetSetPropertyRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetSetPropertyRequest"), sizeof(FRCPresetSetPropertyRequest), Get_Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetSetPropertyRequest>()
{
	return FRCPresetSetPropertyRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetSetPropertyRequest(FRCPresetSetPropertyRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetSetPropertyRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetSetPropertyRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetSetPropertyRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetSetPropertyRequest>(FName(TEXT("RCPresetSetPropertyRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetSetPropertyRequest;
	struct Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateTransaction_MetaData[];
#endif
		static void NewProp_GenerateTransaction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GenerateTransaction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResetToDefault_MetaData[];
#endif
		static void NewProp_ResetToDefault_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ResetToDefault;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to set a property on a preset\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to set a property on a preset" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetSetPropertyRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether a transaction should be created for the call.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Whether a transaction should be created for the call." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction_SetBit(void* Obj)
	{
		((FRCPresetSetPropertyRequest*)Obj)->GenerateTransaction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction = { "GenerateTransaction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCPresetSetPropertyRequest), &Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault_SetBit(void* Obj)
	{
		((FRCPresetSetPropertyRequest*)Obj)->ResetToDefault = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault = { "ResetToDefault", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCPresetSetPropertyRequest), &Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_GenerateTransaction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::NewProp_ResetToDefault,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCPresetSetPropertyRequest",
		sizeof(FRCPresetSetPropertyRequest),
		alignof(FRCPresetSetPropertyRequest),
		Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetSetPropertyRequest"), sizeof(FRCPresetSetPropertyRequest), Get_Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetSetPropertyRequest_Hash() { return 2532449628U; }

static_assert(std::is_polymorphic<FRCObjectRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCObjectRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCObjectRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCObjectRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCObjectRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCObjectRequest"), sizeof(FRCObjectRequest), Get_Z_Construct_UScriptStruct_FRCObjectRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCObjectRequest>()
{
	return FRCObjectRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCObjectRequest(FRCObjectRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCObjectRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCObjectRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCObjectRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCObjectRequest>(FName(TEXT("RCObjectRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCObjectRequest;
	struct Z_Construct_UScriptStruct_FRCObjectRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PropertyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResetToDefault_MetaData[];
#endif
		static void NewProp_ResetToDefault_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ResetToDefault;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateTransaction_MetaData[];
#endif
		static void NewProp_GenerateTransaction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GenerateTransaction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Access_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Access_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Access;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to access an object\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to access an object" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCObjectRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ObjectPath_MetaData[] = {
		{ "Comment", "/**\n\x09 * The path of the target object.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The path of the target object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ObjectPath = { "ObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCObjectRequest, ObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_PropertyName_MetaData[] = {
		{ "Comment", "/**\n\x09 * The property to read or modify.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The property to read or modify." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_PropertyName = { "PropertyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCObjectRequest, PropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_PropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_PropertyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether the property should be reset to default.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Whether the property should be reset to default." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault_SetBit(void* Obj)
	{
		((FRCObjectRequest*)Obj)->ResetToDefault = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault = { "ResetToDefault", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCObjectRequest), &Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether a transaction should be created for the call.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Whether a transaction should be created for the call." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction_SetBit(void* Obj)
	{
		((FRCObjectRequest*)Obj)->GenerateTransaction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction = { "GenerateTransaction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCObjectRequest), &Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access_MetaData[] = {
		{ "Comment", "/**\n\x09 * Indicates if the property should be read or written to.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Indicates if the property should be read or written to." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access = { "Access", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCObjectRequest, Access), Z_Construct_UEnum_RemoteControl_ERCAccess, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCObjectRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_PropertyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_ResetToDefault,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_GenerateTransaction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectRequest_Statics::NewProp_Access,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCObjectRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCObjectRequest",
		sizeof(FRCObjectRequest),
		alignof(FRCObjectRequest),
		Z_Construct_UScriptStruct_FRCObjectRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCObjectRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCObjectRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCObjectRequest"), sizeof(FRCObjectRequest), Get_Z_Construct_UScriptStruct_FRCObjectRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCObjectRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCObjectRequest_Hash() { return 2331812626U; }

static_assert(std::is_polymorphic<FRCCallRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCCallRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCCallRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCCallRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCCallRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCCallRequest"), sizeof(FRCCallRequest), Get_Z_Construct_UScriptStruct_FRCCallRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCCallRequest>()
{
	return FRCCallRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCCallRequest(FRCCallRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCCallRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCCallRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCCallRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCCallRequest>(FName(TEXT("RCCallRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCCallRequest;
	struct Z_Construct_UScriptStruct_FRCCallRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FunctionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateTransaction_MetaData[];
#endif
		static void NewProp_GenerateTransaction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GenerateTransaction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCallRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to call a function\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to call a function" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCCallRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_ObjectPath_MetaData[] = {
		{ "Comment", "/**\n\x09 * The path of the target object.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The path of the target object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_ObjectPath = { "ObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCCallRequest, ObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_ObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_ObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_FunctionName_MetaData[] = {
		{ "Comment", "/**\n\x09 * The name of the function to call.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The name of the function to call." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_FunctionName = { "FunctionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCCallRequest, FunctionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_FunctionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_FunctionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether a transaction should be created for the call.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Whether a transaction should be created for the call." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction_SetBit(void* Obj)
	{
		((FRCCallRequest*)Obj)->GenerateTransaction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction = { "GenerateTransaction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCCallRequest), &Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCCallRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_ObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_FunctionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCCallRequest_Statics::NewProp_GenerateTransaction,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCCallRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCCallRequest",
		sizeof(FRCCallRequest),
		alignof(FRCCallRequest),
		Z_Construct_UScriptStruct_FRCCallRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCallRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCCallRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCCallRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCCallRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCCallRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCCallRequest"), sizeof(FRCCallRequest), Get_Z_Construct_UScriptStruct_FRCCallRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCCallRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCCallRequest_Hash() { return 2886081737U; }

static_assert(std::is_polymorphic<FRemoteControlObjectEventHookRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRemoteControlObjectEventHookRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRemoteControlObjectEventHookRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RemoteControlObjectEventHookRequest"), sizeof(FRemoteControlObjectEventHookRequest), Get_Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlObjectEventHookRequest>()
{
	return FRemoteControlObjectEventHookRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlObjectEventHookRequest(FRemoteControlObjectEventHookRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RemoteControlObjectEventHookRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRemoteControlObjectEventHookRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRemoteControlObjectEventHookRequest()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlObjectEventHookRequest>(FName(TEXT("RemoteControlObjectEventHookRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRemoteControlObjectEventHookRequest;
	struct Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PropertyName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request to create an event hook.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request to create an event hook." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlObjectEventHookRequest>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType_MetaData[] = {
		{ "Comment", "/**\n\x09 * What type of event should be listened to.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "What type of event should be listened to." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType = { "EventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlObjectEventHookRequest, EventType), Z_Construct_UEnum_WebRemoteControl_ERemoteControlEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_ObjectPath_MetaData[] = {
		{ "Comment", "/**\n\x09 * The path of the target object.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The path of the target object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_ObjectPath = { "ObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlObjectEventHookRequest, ObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_ObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_ObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_PropertyName_MetaData[] = {
		{ "Comment", "/**\n\x09 * The name of the property to watch for changes.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The name of the property to watch for changes." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_PropertyName = { "PropertyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlObjectEventHookRequest, PropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_PropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_PropertyName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_EventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_ObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::NewProp_PropertyName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RemoteControlObjectEventHookRequest",
		sizeof(FRemoteControlObjectEventHookRequest),
		alignof(FRemoteControlObjectEventHookRequest),
		Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlObjectEventHookRequest"), sizeof(FRemoteControlObjectEventHookRequest), Get_Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlObjectEventHookRequest_Hash() { return 1941311775U; }

static_assert(std::is_polymorphic<FRCBatchRequest>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCBatchRequest cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCBatchRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCBatchRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCBatchRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCBatchRequest"), sizeof(FRCBatchRequest), Get_Z_Construct_UScriptStruct_FRCBatchRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCBatchRequest>()
{
	return FRCBatchRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCBatchRequest(FRCBatchRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCBatchRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCBatchRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCBatchRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCBatchRequest>(FName(TEXT("RCBatchRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCBatchRequest;
	struct Z_Construct_UScriptStruct_FRCBatchRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Requests_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Requests_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Requests;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCBatchRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request that wraps multiple requests..\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request that wraps multiple requests.." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCBatchRequest>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests_Inner = { "Requests", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCRequestWrapper, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests_MetaData[] = {
		{ "Comment", "/**\n\x09 * The list of batched requests.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "The list of batched requests." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests = { "Requests", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCBatchRequest, Requests), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCBatchRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCBatchRequest_Statics::NewProp_Requests,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCBatchRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCBatchRequest",
		sizeof(FRCBatchRequest),
		alignof(FRCBatchRequest),
		Z_Construct_UScriptStruct_FRCBatchRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCBatchRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCBatchRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCBatchRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCBatchRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCBatchRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCBatchRequest"), sizeof(FRCBatchRequest), Get_Z_Construct_UScriptStruct_FRCBatchRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCBatchRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCBatchRequest_Hash() { return 2067790628U; }

static_assert(std::is_polymorphic<FRCRequestWrapper>() == std::is_polymorphic<FRCRequest>(), "USTRUCT FRCRequestWrapper cannot be polymorphic unless super FRCRequest is polymorphic");

class UScriptStruct* FRCRequestWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCRequestWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCRequestWrapper, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCRequestWrapper"), sizeof(FRCRequestWrapper), Get_Z_Construct_UScriptStruct_FRCRequestWrapper_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCRequestWrapper>()
{
	return FRCRequestWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCRequestWrapper(FRCRequestWrapper::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCRequestWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCRequestWrapper
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCRequestWrapper()
	{
		UScriptStruct::DeferCppStructOps<FRCRequestWrapper>(FName(TEXT("RCRequestWrapper")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCRequestWrapper;
	struct Z_Construct_UScriptStruct_FRCRequestWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_URL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_URL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Verb_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Verb;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequestId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RequestId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCRequestWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_URL_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_URL = { "URL", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCRequestWrapper, URL), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_URL_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_URL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_Verb_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_Verb = { "Verb", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCRequestWrapper, Verb), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_Verb_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_Verb_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_RequestId_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_RequestId = { "RequestId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCRequestWrapper, RequestId), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_RequestId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_RequestId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_URL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_Verb,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::NewProp_RequestId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		Z_Construct_UScriptStruct_FRCRequest,
		&NewStructOps,
		"RCRequestWrapper",
		sizeof(FRCRequestWrapper),
		alignof(FRCRequestWrapper),
		Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCRequestWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCRequestWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCRequestWrapper"), sizeof(FRCRequestWrapper), Get_Z_Construct_UScriptStruct_FRCRequestWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCRequestWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCRequestWrapper_Hash() { return 1850785410U; }
class UScriptStruct* FRCRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCRequest, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCRequest"), sizeof(FRCRequest), Get_Z_Construct_UScriptStruct_FRCRequest_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCRequest>()
{
	return FRCRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCRequest(FRCRequest::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCRequest"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCRequest
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCRequest()
	{
		UScriptStruct::DeferCppStructOps<FRCRequest>(FName(TEXT("RCRequest")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCRequest;
	struct Z_Construct_UScriptStruct_FRCRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds a request made to the remote control server.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlRequest.h" },
		{ "ToolTip", "Holds a request made to the remote control server." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCRequest>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCRequest",
		sizeof(FRCRequest),
		alignof(FRCRequest),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCRequest"), sizeof(FRCRequest), Get_Z_Construct_UScriptStruct_FRCRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCRequest_Hash() { return 209760832U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
