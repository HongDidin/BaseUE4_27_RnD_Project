// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlBinding.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlBinding() {}
// Cross Module References
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlBinding_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlBinding();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlLevelIndependantBinding_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlLevelIndependantBinding();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlLevelDependantBinding_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlLevelDependantBinding();
	ENGINE_API UClass* Z_Construct_UClass_ULevel_NoRegister();
// End Cross Module References
	void URemoteControlBinding::StaticRegisterNativesURemoteControlBinding()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlBinding_NoRegister()
	{
		return URemoteControlBinding::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlBinding_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlBinding_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlBinding_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Acts as a bridge between an exposed property/function and an object to act on.\n */" },
		{ "IncludePath", "RemoteControlBinding.h" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
		{ "ToolTip", "Acts as a bridge between an exposed property/function and an object to act on." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlBinding_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/**\n\x09 * The name of this binding. Defaults to the bound object's name.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
		{ "ToolTip", "The name of this binding. Defaults to the bound object's name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URemoteControlBinding_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlBinding, Name), METADATA_PARAMS(Z_Construct_UClass_URemoteControlBinding_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlBinding_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlBinding_Statics::NewProp_Name,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlBinding_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlBinding>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlBinding_Statics::ClassParams = {
		&URemoteControlBinding::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlBinding_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlBinding_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlBinding_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlBinding_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlBinding()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlBinding_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlBinding, 3452624942);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlBinding>()
	{
		return URemoteControlBinding::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlBinding(Z_Construct_UClass_URemoteControlBinding, &URemoteControlBinding::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlBinding"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlBinding);
	void URemoteControlLevelIndependantBinding::StaticRegisterNativesURemoteControlLevelIndependantBinding()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlLevelIndependantBinding_NoRegister()
	{
		return URemoteControlLevelIndependantBinding::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_BoundObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URemoteControlBinding,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "RemoteControlBinding.h" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::NewProp_BoundObject_MetaData[] = {
		{ "Comment", "/**\n\x09 * Holds the bound object.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
		{ "ToolTip", "Holds the bound object." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::NewProp_BoundObject = { "BoundObject", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlLevelIndependantBinding, BoundObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::NewProp_BoundObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::NewProp_BoundObject_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::NewProp_BoundObject,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlLevelIndependantBinding>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::ClassParams = {
		&URemoteControlLevelIndependantBinding::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlLevelIndependantBinding()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlLevelIndependantBinding_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlLevelIndependantBinding, 1454924176);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlLevelIndependantBinding>()
	{
		return URemoteControlLevelIndependantBinding::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlLevelIndependantBinding(Z_Construct_UClass_URemoteControlLevelIndependantBinding, &URemoteControlLevelIndependantBinding::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlLevelIndependantBinding"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlLevelIndependantBinding);
	void URemoteControlLevelDependantBinding::StaticRegisterNativesURemoteControlLevelDependantBinding()
	{
	}
	UClass* Z_Construct_UClass_URemoteControlLevelDependantBinding_NoRegister()
	{
		return URemoteControlLevelDependantBinding::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_BoundObjectMap_ValueProp;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_BoundObjectMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundObjectMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_BoundObjectMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelWithLastSuccessfulResolve_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_LevelWithLastSuccessfulResolve;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URemoteControlBinding,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "RemoteControlBinding.h" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_ValueProp = { "BoundObjectMap", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_Key_KeyProp = { "BoundObjectMap_Key", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ULevel_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_MetaData[] = {
		{ "Comment", "/**\n\x09 *\x09The map bound objects with their level as key.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
		{ "ToolTip", "The map bound objects with their level as key." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap = { "BoundObjectMap", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlLevelDependantBinding, BoundObjectMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_LevelWithLastSuccessfulResolve_MetaData[] = {
		{ "Comment", "/**\n\x09 * Caches the last level that had a successful resolve.\n\x09 * Used to decide which level to use when reinitializing this binding in a new level.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlBinding.h" },
		{ "ToolTip", "Caches the last level that had a successful resolve.\nUsed to decide which level to use when reinitializing this binding in a new level." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_LevelWithLastSuccessfulResolve = { "LevelWithLastSuccessfulResolve", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoteControlLevelDependantBinding, LevelWithLastSuccessfulResolve), Z_Construct_UClass_ULevel_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_LevelWithLastSuccessfulResolve_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_LevelWithLastSuccessfulResolve_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_BoundObjectMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::NewProp_LevelWithLastSuccessfulResolve,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlLevelDependantBinding>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::ClassParams = {
		&URemoteControlLevelDependantBinding::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlLevelDependantBinding()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlLevelDependantBinding_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlLevelDependantBinding, 643432314);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlLevelDependantBinding>()
	{
		return URemoteControlLevelDependantBinding::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlLevelDependantBinding(Z_Construct_UClass_URemoteControlLevelDependantBinding, &URemoteControlLevelDependantBinding::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlLevelDependantBinding"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlLevelDependantBinding);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
