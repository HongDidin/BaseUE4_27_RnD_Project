// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlFunctionLibrary() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlFunctionLibrary_NoRegister();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	REMOTECONTROL_API UClass* Z_Construct_UClass_URemoteControlPreset_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
class UScriptStruct* FRemoteControlOptionalExposeArgs::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RemoteControlOptionalExposeArgs"), sizeof(FRemoteControlOptionalExposeArgs), Get_Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRemoteControlOptionalExposeArgs>()
{
	return FRemoteControlOptionalExposeArgs::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlOptionalExposeArgs(FRemoteControlOptionalExposeArgs::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RemoteControlOptionalExposeArgs"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlOptionalExposeArgs
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlOptionalExposeArgs()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlOptionalExposeArgs>(FName(TEXT("RemoteControlOptionalExposeArgs")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRemoteControlOptionalExposeArgs;
	struct Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_GroupName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlOptionalExposeArgs>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "RemoteControlPreset" },
		{ "Comment", "/**\n\x09 * The display name of the exposed entity in the panel.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
		{ "ToolTip", "The display name of the exposed entity in the panel." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlOptionalExposeArgs, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_GroupName_MetaData[] = {
		{ "Category", "RemoteControlPreset" },
		{ "Comment", "/**\n\x09 * The name of the group to expose the entity in.\n\x09 * If it does not exist, a group with that name will be created.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
		{ "ToolTip", "The name of the group to expose the entity in.\nIf it does not exist, a group with that name will be created." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_GroupName = { "GroupName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlOptionalExposeArgs, GroupName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_GroupName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_GroupName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::NewProp_GroupName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RemoteControlOptionalExposeArgs",
		sizeof(FRemoteControlOptionalExposeArgs),
		alignof(FRemoteControlOptionalExposeArgs),
		Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlOptionalExposeArgs"), sizeof(FRemoteControlOptionalExposeArgs), Get_Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs_Hash() { return 158208485U; }
	DEFINE_FUNCTION(URemoteControlFunctionLibrary::execExposeActor)
	{
		P_GET_OBJECT(URemoteControlPreset,Z_Param_Preset);
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_GET_STRUCT(FRemoteControlOptionalExposeArgs,Z_Param_Args);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=URemoteControlFunctionLibrary::ExposeActor(Z_Param_Preset,Z_Param_Actor,Z_Param_Args);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URemoteControlFunctionLibrary::execExposeFunction)
	{
		P_GET_OBJECT(URemoteControlPreset,Z_Param_Preset);
		P_GET_OBJECT(UObject,Z_Param_SourceObject);
		P_GET_PROPERTY(FStrProperty,Z_Param_Function);
		P_GET_STRUCT(FRemoteControlOptionalExposeArgs,Z_Param_Args);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=URemoteControlFunctionLibrary::ExposeFunction(Z_Param_Preset,Z_Param_SourceObject,Z_Param_Function,Z_Param_Args);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URemoteControlFunctionLibrary::execExposeProperty)
	{
		P_GET_OBJECT(URemoteControlPreset,Z_Param_Preset);
		P_GET_OBJECT(UObject,Z_Param_SourceObject);
		P_GET_PROPERTY(FStrProperty,Z_Param_Property);
		P_GET_STRUCT(FRemoteControlOptionalExposeArgs,Z_Param_Args);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=URemoteControlFunctionLibrary::ExposeProperty(Z_Param_Preset,Z_Param_SourceObject,Z_Param_Property,Z_Param_Args);
		P_NATIVE_END;
	}
	void URemoteControlFunctionLibrary::StaticRegisterNativesURemoteControlFunctionLibrary()
	{
		UClass* Class = URemoteControlFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ExposeActor", &URemoteControlFunctionLibrary::execExposeActor },
			{ "ExposeFunction", &URemoteControlFunctionLibrary::execExposeFunction },
			{ "ExposeProperty", &URemoteControlFunctionLibrary::execExposeProperty },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics
	{
		struct RemoteControlFunctionLibrary_eventExposeActor_Parms
		{
			URemoteControlPreset* Preset;
			AActor* Actor;
			FRemoteControlOptionalExposeArgs Args;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preset;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Args;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_Preset = { "Preset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeActor_Parms, Preset), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeActor_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_Args = { "Args", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeActor_Parms, Args), Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((RemoteControlFunctionLibrary_eventExposeActor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RemoteControlFunctionLibrary_eventExposeActor_Parms), &Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_Preset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_Args,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "RemoteControlPreset" },
		{ "Comment", "/**\n\x09 * Expose an actor in a remote control preset.\n\x09 * @param Preset the preset to expose the property in.\n\x09 * @param Actor the actor to expose.\n\x09 * @param Args optional arguments.\n\x09 * @return Whether the operation was successful.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
		{ "ToolTip", "Expose an actor in a remote control preset.\n@param Preset the preset to expose the property in.\n@param Actor the actor to expose.\n@param Args optional arguments.\n@return Whether the operation was successful." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URemoteControlFunctionLibrary, nullptr, "ExposeActor", nullptr, nullptr, sizeof(RemoteControlFunctionLibrary_eventExposeActor_Parms), Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics
	{
		struct RemoteControlFunctionLibrary_eventExposeFunction_Parms
		{
			URemoteControlPreset* Preset;
			UObject* SourceObject;
			FString Function;
			FRemoteControlOptionalExposeArgs Args;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preset;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Function_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Function;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Args;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Preset = { "Preset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeFunction_Parms, Preset), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_SourceObject = { "SourceObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeFunction_Parms, SourceObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Function_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Function = { "Function", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeFunction_Parms, Function), METADATA_PARAMS(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Function_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Function_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Args = { "Args", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeFunction_Parms, Args), Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((RemoteControlFunctionLibrary_eventExposeFunction_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RemoteControlFunctionLibrary_eventExposeFunction_Parms), &Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Preset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_SourceObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Function,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_Args,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::Function_MetaDataParams[] = {
		{ "Category", "RemoteControlPreset" },
		{ "Comment", "/**\n\x09 * Expose a function in a remote control preset.\n\x09 * @param Preset the preset to expose the property in.\n\x09 * @param SourceObject the object that contains the property to expose.\n\x09 * @param Function the name of the function to expose.\n\x09 * @param Args optional arguments.\n\x09 * @return Whether the operation was successful.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
		{ "ToolTip", "Expose a function in a remote control preset.\n@param Preset the preset to expose the property in.\n@param SourceObject the object that contains the property to expose.\n@param Function the name of the function to expose.\n@param Args optional arguments.\n@return Whether the operation was successful." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URemoteControlFunctionLibrary, nullptr, "ExposeFunction", nullptr, nullptr, sizeof(RemoteControlFunctionLibrary_eventExposeFunction_Parms), Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics
	{
		struct RemoteControlFunctionLibrary_eventExposeProperty_Parms
		{
			URemoteControlPreset* Preset;
			UObject* SourceObject;
			FString Property;
			FRemoteControlOptionalExposeArgs Args;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preset;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Property;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Args;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Preset = { "Preset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeProperty_Parms, Preset), Z_Construct_UClass_URemoteControlPreset_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_SourceObject = { "SourceObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeProperty_Parms, SourceObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Property_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Property = { "Property", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeProperty_Parms, Property), METADATA_PARAMS(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Property_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Args = { "Args", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RemoteControlFunctionLibrary_eventExposeProperty_Parms, Args), Z_Construct_UScriptStruct_FRemoteControlOptionalExposeArgs, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((RemoteControlFunctionLibrary_eventExposeProperty_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RemoteControlFunctionLibrary_eventExposeProperty_Parms), &Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Preset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_SourceObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_Args,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::Function_MetaDataParams[] = {
		{ "Category", "RemoteControlPreset" },
		{ "Comment", "/**\n\x09 * Expose a property in a remote control preset.\n\x09 * @param Preset the preset to expose the property in.\n\x09 * @param SourceObject the object that contains the property to expose.\n\x09 * @param Property the name or path of the property to expose.\n\x09 * @param Args optional arguments.\n\x09 * @return Whether the operation was successful.\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
		{ "ToolTip", "Expose a property in a remote control preset.\n@param Preset the preset to expose the property in.\n@param SourceObject the object that contains the property to expose.\n@param Property the name or path of the property to expose.\n@param Args optional arguments.\n@return Whether the operation was successful." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URemoteControlFunctionLibrary, nullptr, "ExposeProperty", nullptr, nullptr, sizeof(RemoteControlFunctionLibrary_eventExposeProperty_Parms), Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URemoteControlFunctionLibrary_NoRegister()
	{
		return URemoteControlFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_URemoteControlFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeActor, "ExposeActor" }, // 868973240
		{ &Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeFunction, "ExposeFunction" }, // 3063218354
		{ &Z_Construct_UFunction_URemoteControlFunctionLibrary_ExposeProperty, "ExposeProperty" }, // 1236526627
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RemoteControlFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/RemoteControlFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoteControlFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::ClassParams = {
		&URemoteControlFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoteControlFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoteControlFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoteControlFunctionLibrary, 1453142923);
	template<> REMOTECONTROL_API UClass* StaticClass<URemoteControlFunctionLibrary>()
	{
		return URemoteControlFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoteControlFunctionLibrary(Z_Construct_UClass_URemoteControlFunctionLibrary, &URemoteControlFunctionLibrary::StaticClass, TEXT("/Script/RemoteControl"), TEXT("URemoteControlFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoteControlFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
