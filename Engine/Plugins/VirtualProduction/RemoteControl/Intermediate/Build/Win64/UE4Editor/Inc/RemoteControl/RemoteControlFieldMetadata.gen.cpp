// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControl/Public/RemoteControlFieldMetadata.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlFieldMetadata() {}
// Cross Module References
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_FVector();
	UPackage* Z_Construct_UPackage__Script_RemoteControl();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCFieldMetadata();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_bool();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct();
	COREUOBJECT_API UClass* Z_Construct_UClass_UScriptStruct();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_FName();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_FString();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_double();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_float();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int64();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int32();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int16();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int8();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_uint64();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_uint32();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_uint16();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_byte();
// End Cross Module References

static_assert(std::is_polymorphic<FRCMetadata_FVector>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_FVector cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_FVector::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FVector_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_FVector, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_FVector"), sizeof(FRCMetadata_FVector), Get_Z_Construct_UScriptStruct_FRCMetadata_FVector_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_FVector>()
{
	return FRCMetadata_FVector::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_FVector(FRCMetadata_FVector::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_FVector"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FVector
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FVector()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_FVector>(FName(TEXT("RCMetadata_FVector")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FVector;
	struct Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MinimumValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaximumValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_FVector>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_FVector, DefaultValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_DefaultValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MinimumValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MinimumValue = { "MinimumValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_FVector, MinimumValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MinimumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MinimumValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MaximumValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MaximumValue = { "MaximumValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_FVector, MaximumValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MaximumValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MaximumValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MinimumValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::NewProp_MaximumValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_FVector",
		sizeof(FRCMetadata_FVector),
		alignof(FRCMetadata_FVector),
		Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_FVector()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FVector_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_FVector"), sizeof(FRCMetadata_FVector), Get_Z_Construct_UScriptStruct_FRCMetadata_FVector_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_FVector_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FVector_Hash() { return 1290460464U; }

static_assert(std::is_polymorphic<FRCMetadata_bool>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_bool cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_bool::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_bool_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_bool, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_bool"), sizeof(FRCMetadata_bool), Get_Z_Construct_UScriptStruct_FRCMetadata_bool_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_bool>()
{
	return FRCMetadata_bool::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_bool(FRCMetadata_bool::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_bool"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_bool
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_bool()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_bool>(FName(TEXT("RCMetadata_bool")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_bool;
	struct Z_Construct_UScriptStruct_FRCMetadata_bool_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static void NewProp_DefaultValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_bool>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue_SetBit(void* Obj)
	{
		((FRCMetadata_bool*)Obj)->DefaultValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCMetadata_bool), &Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_bool",
		sizeof(FRCMetadata_bool),
		alignof(FRCMetadata_bool),
		Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_bool()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_bool_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_bool"), sizeof(FRCMetadata_bool), Get_Z_Construct_UScriptStruct_FRCMetadata_bool_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_bool_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_bool_Hash() { return 4093753828U; }

static_assert(std::is_polymorphic<FRCMetadata_UScriptStruct>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_UScriptStruct cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_UScriptStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_UScriptStruct"), sizeof(FRCMetadata_UScriptStruct), Get_Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_UScriptStruct>()
{
	return FRCMetadata_UScriptStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_UScriptStruct(FRCMetadata_UScriptStruct::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_UScriptStruct"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UScriptStruct
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UScriptStruct()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_UScriptStruct>(FName(TEXT("RCMetadata_UScriptStruct")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UScriptStruct;
	struct Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_UScriptStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_UScriptStruct, DefaultValue), Z_Construct_UClass_UScriptStruct, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_UScriptStruct",
		sizeof(FRCMetadata_UScriptStruct),
		alignof(FRCMetadata_UScriptStruct),
		Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_UScriptStruct"), sizeof(FRCMetadata_UScriptStruct), Get_Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UScriptStruct_Hash() { return 2793585356U; }

static_assert(std::is_polymorphic<FRCMetadata_UClass>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_UClass cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_UClass::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UClass_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_UClass, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_UClass"), sizeof(FRCMetadata_UClass), Get_Z_Construct_UScriptStruct_FRCMetadata_UClass_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_UClass>()
{
	return FRCMetadata_UClass::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_UClass(FRCMetadata_UClass::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_UClass"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UClass
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UClass()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_UClass>(FName(TEXT("RCMetadata_UClass")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UClass;
	struct Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_UClass>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_UClass, DefaultValue), Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_UClass",
		sizeof(FRCMetadata_UClass),
		alignof(FRCMetadata_UClass),
		Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_UClass()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UClass_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_UClass"), sizeof(FRCMetadata_UClass), Get_Z_Construct_UScriptStruct_FRCMetadata_UClass_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_UClass_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UClass_Hash() { return 1307036573U; }

static_assert(std::is_polymorphic<FRCMetadata_UObject>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_UObject cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_UObject::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UObject_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_UObject, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_UObject"), sizeof(FRCMetadata_UObject), Get_Z_Construct_UScriptStruct_FRCMetadata_UObject_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_UObject>()
{
	return FRCMetadata_UObject::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_UObject(FRCMetadata_UObject::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_UObject"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UObject
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UObject()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_UObject>(FName(TEXT("RCMetadata_UObject")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_UObject;
	struct Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_UObject>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_UObject, DefaultValue), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_UObject",
		sizeof(FRCMetadata_UObject),
		alignof(FRCMetadata_UObject),
		Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_UObject()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UObject_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_UObject"), sizeof(FRCMetadata_UObject), Get_Z_Construct_UScriptStruct_FRCMetadata_UObject_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_UObject_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_UObject_Hash() { return 2185735575U; }

static_assert(std::is_polymorphic<FRCMetadata_FName>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_FName cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_FName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_FName, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_FName"), sizeof(FRCMetadata_FName), Get_Z_Construct_UScriptStruct_FRCMetadata_FName_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_FName>()
{
	return FRCMetadata_FName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_FName(FRCMetadata_FName::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_FName"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FName
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FName()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_FName>(FName(TEXT("RCMetadata_FName")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FName;
	struct Z_Construct_UScriptStruct_FRCMetadata_FName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_FName>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_FName, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_FName",
		sizeof(FRCMetadata_FName),
		alignof(FRCMetadata_FName),
		Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_FName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_FName"), sizeof(FRCMetadata_FName), Get_Z_Construct_UScriptStruct_FRCMetadata_FName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_FName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FName_Hash() { return 879456354U; }

static_assert(std::is_polymorphic<FRCMetadata_FString>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_FString cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_FString::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FString_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_FString, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_FString"), sizeof(FRCMetadata_FString), Get_Z_Construct_UScriptStruct_FRCMetadata_FString_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_FString>()
{
	return FRCMetadata_FString::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_FString(FRCMetadata_FString::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_FString"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FString
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FString()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_FString>(FName(TEXT("RCMetadata_FString")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_FString;
	struct Z_Construct_UScriptStruct_FRCMetadata_FString_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_FString>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_FString, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_FString",
		sizeof(FRCMetadata_FString),
		alignof(FRCMetadata_FString),
		Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_FString()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FString_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_FString"), sizeof(FRCMetadata_FString), Get_Z_Construct_UScriptStruct_FRCMetadata_FString_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_FString_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_FString_Hash() { return 3232674608U; }

static_assert(std::is_polymorphic<FRCMetadata_double>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_double cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_double::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_double_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_double, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_double"), sizeof(FRCMetadata_double), Get_Z_Construct_UScriptStruct_FRCMetadata_double_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_double>()
{
	return FRCMetadata_double::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_double(FRCMetadata_double::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_double"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_double
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_double()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_double>(FName(TEXT("RCMetadata_double")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_double;
	struct Z_Construct_UScriptStruct_FRCMetadata_double_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_double_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_double>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_double, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_double, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_double, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_double_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_double_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_double_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_double",
		sizeof(FRCMetadata_double),
		alignof(FRCMetadata_double),
		Z_Construct_UScriptStruct_FRCMetadata_double_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_double_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_double()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_double_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_double"), sizeof(FRCMetadata_double), Get_Z_Construct_UScriptStruct_FRCMetadata_double_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_double_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_double_Hash() { return 2570776311U; }

static_assert(std::is_polymorphic<FRCMetadata_float>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_float cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_float::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_float_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_float, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_float"), sizeof(FRCMetadata_float), Get_Z_Construct_UScriptStruct_FRCMetadata_float_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_float>()
{
	return FRCMetadata_float::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_float(FRCMetadata_float::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_float"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_float
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_float()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_float>(FName(TEXT("RCMetadata_float")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_float;
	struct Z_Construct_UScriptStruct_FRCMetadata_float_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_float_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_float>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_float, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_float, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_float, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_float_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_float_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_float_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_float",
		sizeof(FRCMetadata_float),
		alignof(FRCMetadata_float),
		Z_Construct_UScriptStruct_FRCMetadata_float_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_float_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_float()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_float_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_float"), sizeof(FRCMetadata_float), Get_Z_Construct_UScriptStruct_FRCMetadata_float_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_float_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_float_Hash() { return 2382434870U; }

static_assert(std::is_polymorphic<FRCMetadata_int64>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_int64 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_int64::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int64_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_int64, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_int64"), sizeof(FRCMetadata_int64), Get_Z_Construct_UScriptStruct_FRCMetadata_int64_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_int64>()
{
	return FRCMetadata_int64::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_int64(FRCMetadata_int64::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_int64"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int64
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int64()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_int64>(FName(TEXT("RCMetadata_int64")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int64;
	struct Z_Construct_UScriptStruct_FRCMetadata_int64_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_int64>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int64, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int64, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int64, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_int64",
		sizeof(FRCMetadata_int64),
		alignof(FRCMetadata_int64),
		Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int64()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int64_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_int64"), sizeof(FRCMetadata_int64), Get_Z_Construct_UScriptStruct_FRCMetadata_int64_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_int64_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int64_Hash() { return 1380037536U; }

static_assert(std::is_polymorphic<FRCMetadata_int32>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_int32 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_int32::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int32_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_int32, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_int32"), sizeof(FRCMetadata_int32), Get_Z_Construct_UScriptStruct_FRCMetadata_int32_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_int32>()
{
	return FRCMetadata_int32::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_int32(FRCMetadata_int32::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_int32"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int32
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int32()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_int32>(FName(TEXT("RCMetadata_int32")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int32;
	struct Z_Construct_UScriptStruct_FRCMetadata_int32_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_int32>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int32, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int32, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int32, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_int32",
		sizeof(FRCMetadata_int32),
		alignof(FRCMetadata_int32),
		Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int32()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int32_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_int32"), sizeof(FRCMetadata_int32), Get_Z_Construct_UScriptStruct_FRCMetadata_int32_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_int32_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int32_Hash() { return 4118757368U; }

static_assert(std::is_polymorphic<FRCMetadata_int16>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_int16 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_int16::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int16_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_int16, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_int16"), sizeof(FRCMetadata_int16), Get_Z_Construct_UScriptStruct_FRCMetadata_int16_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_int16>()
{
	return FRCMetadata_int16::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_int16(FRCMetadata_int16::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_int16"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int16
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int16()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_int16>(FName(TEXT("RCMetadata_int16")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int16;
	struct Z_Construct_UScriptStruct_FRCMetadata_int16_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_int16>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int16, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int16, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int16, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_int16",
		sizeof(FRCMetadata_int16),
		alignof(FRCMetadata_int16),
		Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int16()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int16_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_int16"), sizeof(FRCMetadata_int16), Get_Z_Construct_UScriptStruct_FRCMetadata_int16_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_int16_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int16_Hash() { return 3679442021U; }

static_assert(std::is_polymorphic<FRCMetadata_int8>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_int8 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_int8::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int8_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_int8, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_int8"), sizeof(FRCMetadata_int8), Get_Z_Construct_UScriptStruct_FRCMetadata_int8_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_int8>()
{
	return FRCMetadata_int8::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_int8(FRCMetadata_int8::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_int8"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int8
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int8()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_int8>(FName(TEXT("RCMetadata_int8")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_int8;
	struct Z_Construct_UScriptStruct_FRCMetadata_int8_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_int8>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int8, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int8, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_int8, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_int8",
		sizeof(FRCMetadata_int8),
		alignof(FRCMetadata_int8),
		Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_int8()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int8_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_int8"), sizeof(FRCMetadata_int8), Get_Z_Construct_UScriptStruct_FRCMetadata_int8_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_int8_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_int8_Hash() { return 1048530473U; }

static_assert(std::is_polymorphic<FRCMetadata_uint64>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_uint64 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_uint64::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint64_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_uint64, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_uint64"), sizeof(FRCMetadata_uint64), Get_Z_Construct_UScriptStruct_FRCMetadata_uint64_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_uint64>()
{
	return FRCMetadata_uint64::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_uint64(FRCMetadata_uint64::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_uint64"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint64
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint64()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_uint64>(FName(TEXT("RCMetadata_uint64")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint64;
	struct Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_uint64>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint64, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint64, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint64, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_uint64",
		sizeof(FRCMetadata_uint64),
		alignof(FRCMetadata_uint64),
		Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_uint64()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint64_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_uint64"), sizeof(FRCMetadata_uint64), Get_Z_Construct_UScriptStruct_FRCMetadata_uint64_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_uint64_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint64_Hash() { return 475138416U; }

static_assert(std::is_polymorphic<FRCMetadata_uint32>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_uint32 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_uint32::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint32_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_uint32, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_uint32"), sizeof(FRCMetadata_uint32), Get_Z_Construct_UScriptStruct_FRCMetadata_uint32_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_uint32>()
{
	return FRCMetadata_uint32::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_uint32(FRCMetadata_uint32::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_uint32"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint32
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint32()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_uint32>(FName(TEXT("RCMetadata_uint32")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint32;
	struct Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_uint32>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint32, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint32, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint32, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_uint32",
		sizeof(FRCMetadata_uint32),
		alignof(FRCMetadata_uint32),
		Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_uint32()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint32_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_uint32"), sizeof(FRCMetadata_uint32), Get_Z_Construct_UScriptStruct_FRCMetadata_uint32_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_uint32_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint32_Hash() { return 756108327U; }

static_assert(std::is_polymorphic<FRCMetadata_uint16>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_uint16 cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_uint16::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint16_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_uint16, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_uint16"), sizeof(FRCMetadata_uint16), Get_Z_Construct_UScriptStruct_FRCMetadata_uint16_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_uint16>()
{
	return FRCMetadata_uint16::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_uint16(FRCMetadata_uint16::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_uint16"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint16
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint16()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_uint16>(FName(TEXT("RCMetadata_uint16")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_uint16;
	struct Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_uint16>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint16, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint16, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_uint16, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_uint16",
		sizeof(FRCMetadata_uint16),
		alignof(FRCMetadata_uint16),
		Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_uint16()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint16_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_uint16"), sizeof(FRCMetadata_uint16), Get_Z_Construct_UScriptStruct_FRCMetadata_uint16_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_uint16_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_uint16_Hash() { return 3962415011U; }

static_assert(std::is_polymorphic<FRCMetadata_byte>() == std::is_polymorphic<FRCFieldMetadata>(), "USTRUCT FRCMetadata_byte cannot be polymorphic unless super FRCFieldMetadata is polymorphic");

class UScriptStruct* FRCMetadata_byte::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_byte_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCMetadata_byte, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCMetadata_byte"), sizeof(FRCMetadata_byte), Get_Z_Construct_UScriptStruct_FRCMetadata_byte_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCMetadata_byte>()
{
	return FRCMetadata_byte::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCMetadata_byte(FRCMetadata_byte::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCMetadata_byte"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_byte
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_byte()
	{
		UScriptStruct::DeferCppStructOps<FRCMetadata_byte>(FName(TEXT("RCMetadata_byte")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCMetadata_byte;
	struct Z_Construct_UScriptStruct_FRCMetadata_byte_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCMetadata_byte>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_byte, Min), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_byte, Max), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "Default" },
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCMetadata_byte, DefaultValue), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		Z_Construct_UScriptStruct_FRCFieldMetadata,
		&NewStructOps,
		"RCMetadata_byte",
		sizeof(FRCMetadata_byte),
		alignof(FRCMetadata_byte),
		Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCMetadata_byte()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_byte_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCMetadata_byte"), sizeof(FRCMetadata_byte), Get_Z_Construct_UScriptStruct_FRCMetadata_byte_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCMetadata_byte_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCMetadata_byte_Hash() { return 499644665U; }
class UScriptStruct* FRCFieldMetadata::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCFieldMetadata_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCFieldMetadata, Z_Construct_UPackage__Script_RemoteControl(), TEXT("RCFieldMetadata"), sizeof(FRCFieldMetadata), Get_Z_Construct_UScriptStruct_FRCFieldMetadata_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROL_API UScriptStruct* StaticStruct<FRCFieldMetadata>()
{
	return FRCFieldMetadata::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCFieldMetadata(FRCFieldMetadata::StaticStruct, TEXT("/Script/RemoteControl"), TEXT("RCFieldMetadata"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldMetadata
{
	FScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldMetadata()
	{
		UScriptStruct::DeferCppStructOps<FRCFieldMetadata>(FName(TEXT("RCFieldMetadata")));
	}
} ScriptStruct_RemoteControl_StaticRegisterNativesFRCFieldMetadata;
	struct Z_Construct_UScriptStruct_FRCFieldMetadata_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFieldMetadata_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/RemoteControlFieldMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCFieldMetadata_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCFieldMetadata>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCFieldMetadata_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControl,
		nullptr,
		&NewStructOps,
		"RCFieldMetadata",
		sizeof(FRCFieldMetadata),
		alignof(FRCFieldMetadata),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFieldMetadata_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFieldMetadata_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCFieldMetadata()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCFieldMetadata_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCFieldMetadata"), sizeof(FRCFieldMetadata), Get_Z_Construct_UScriptStruct_FRCFieldMetadata_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCFieldMetadata_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCFieldMetadata_Hash() { return 1955968692U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
