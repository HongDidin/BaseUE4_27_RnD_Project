// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlCommon/Public/RCPropertyContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRCPropertyContainer() {}
// Cross Module References
	REMOTECONTROLCOMMON_API UScriptStruct* Z_Construct_UScriptStruct_FRCPropertyContainerKey();
	UPackage* Z_Construct_UPackage__Script_RemoteControlCommon();
	REMOTECONTROLCOMMON_API UClass* Z_Construct_UClass_URCPropertyContainerBase_NoRegister();
	REMOTECONTROLCOMMON_API UClass* Z_Construct_UClass_URCPropertyContainerBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	REMOTECONTROLCOMMON_API UClass* Z_Construct_UClass_URCPropertyContainerRegistry_NoRegister();
	REMOTECONTROLCOMMON_API UClass* Z_Construct_UClass_URCPropertyContainerRegistry();
	ENGINE_API UClass* Z_Construct_UClass_UEngineSubsystem();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
class UScriptStruct* FRCPropertyContainerKey::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLCOMMON_API uint32 Get_Z_Construct_UScriptStruct_FRCPropertyContainerKey_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPropertyContainerKey, Z_Construct_UPackage__Script_RemoteControlCommon(), TEXT("RCPropertyContainerKey"), sizeof(FRCPropertyContainerKey), Get_Z_Construct_UScriptStruct_FRCPropertyContainerKey_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLCOMMON_API UScriptStruct* StaticStruct<FRCPropertyContainerKey>()
{
	return FRCPropertyContainerKey::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPropertyContainerKey(FRCPropertyContainerKey::StaticStruct, TEXT("/Script/RemoteControlCommon"), TEXT("RCPropertyContainerKey"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlCommon_StaticRegisterNativesFRCPropertyContainerKey
{
	FScriptStruct_RemoteControlCommon_StaticRegisterNativesFRCPropertyContainerKey()
	{
		UScriptStruct::DeferCppStructOps<FRCPropertyContainerKey>(FName(TEXT("RCPropertyContainerKey")));
	}
} ScriptStruct_RemoteControlCommon_StaticRegisterNativesFRCPropertyContainerKey;
	struct Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueTypeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ValueTypeName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Minimal information needed to lookup a unique property container class */" },
		{ "ModuleRelativePath", "Public/RCPropertyContainer.h" },
		{ "ToolTip", "Minimal information needed to lookup a unique property container class" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPropertyContainerKey>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::NewProp_ValueTypeName_MetaData[] = {
		{ "ModuleRelativePath", "Public/RCPropertyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::NewProp_ValueTypeName = { "ValueTypeName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyContainerKey, ValueTypeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::NewProp_ValueTypeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::NewProp_ValueTypeName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::NewProp_ValueTypeName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlCommon,
		nullptr,
		&NewStructOps,
		"RCPropertyContainerKey",
		sizeof(FRCPropertyContainerKey),
		alignof(FRCPropertyContainerKey),
		Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPropertyContainerKey()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPropertyContainerKey_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlCommon();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPropertyContainerKey"), sizeof(FRCPropertyContainerKey), Get_Z_Construct_UScriptStruct_FRCPropertyContainerKey_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPropertyContainerKey_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPropertyContainerKey_Hash() { return 3032982524U; }
	void URCPropertyContainerBase::StaticRegisterNativesURCPropertyContainerBase()
	{
	}
	UClass* Z_Construct_UClass_URCPropertyContainerBase_NoRegister()
	{
		return URCPropertyContainerBase::StaticClass();
	}
	struct Z_Construct_UClass_URCPropertyContainerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URCPropertyContainerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlCommon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URCPropertyContainerBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RCPropertyContainer.h" },
		{ "ModuleRelativePath", "Public/RCPropertyContainer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URCPropertyContainerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URCPropertyContainerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URCPropertyContainerBase_Statics::ClassParams = {
		&URCPropertyContainerBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A9u,
		METADATA_PARAMS(Z_Construct_UClass_URCPropertyContainerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URCPropertyContainerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URCPropertyContainerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URCPropertyContainerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URCPropertyContainerBase, 68998130);
	template<> REMOTECONTROLCOMMON_API UClass* StaticClass<URCPropertyContainerBase>()
	{
		return URCPropertyContainerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URCPropertyContainerBase(Z_Construct_UClass_URCPropertyContainerBase, &URCPropertyContainerBase::StaticClass, TEXT("/Script/RemoteControlCommon"), TEXT("URCPropertyContainerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URCPropertyContainerBase);
	void URCPropertyContainerRegistry::StaticRegisterNativesURCPropertyContainerRegistry()
	{
	}
	UClass* Z_Construct_UClass_URCPropertyContainerRegistry_NoRegister()
	{
		return URCPropertyContainerRegistry::StaticClass();
	}
	struct Z_Construct_UClass_URCPropertyContainerRegistry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CachedContainerClasses_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CachedContainerClasses_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedContainerClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CachedContainerClasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URCPropertyContainerRegistry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlCommon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URCPropertyContainerRegistry_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A subsystem to provide and cache dynamically created PropertyContainer classes. */" },
		{ "IncludePath", "RCPropertyContainer.h" },
		{ "ModuleRelativePath", "Public/RCPropertyContainer.h" },
		{ "ToolTip", "A subsystem to provide and cache dynamically created PropertyContainer classes." },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_ValueProp = { "CachedContainerClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_URCPropertyContainerBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_Key_KeyProp = { "CachedContainerClasses_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCPropertyContainerKey, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_MetaData[] = {
		{ "ModuleRelativePath", "Public/RCPropertyContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses = { "CachedContainerClasses", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URCPropertyContainerRegistry, CachedContainerClasses), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URCPropertyContainerRegistry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URCPropertyContainerRegistry_Statics::NewProp_CachedContainerClasses,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URCPropertyContainerRegistry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URCPropertyContainerRegistry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URCPropertyContainerRegistry_Statics::ClassParams = {
		&URCPropertyContainerRegistry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URCPropertyContainerRegistry_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URCPropertyContainerRegistry_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URCPropertyContainerRegistry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URCPropertyContainerRegistry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URCPropertyContainerRegistry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URCPropertyContainerRegistry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URCPropertyContainerRegistry, 3648198876);
	template<> REMOTECONTROLCOMMON_API UClass* StaticClass<URCPropertyContainerRegistry>()
	{
		return URCPropertyContainerRegistry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URCPropertyContainerRegistry(Z_Construct_UClass_URCPropertyContainerRegistry, &URCPropertyContainerRegistry::StaticClass, TEXT("/Script/RemoteControlCommon"), TEXT("URCPropertyContainerRegistry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URCPropertyContainerRegistry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
