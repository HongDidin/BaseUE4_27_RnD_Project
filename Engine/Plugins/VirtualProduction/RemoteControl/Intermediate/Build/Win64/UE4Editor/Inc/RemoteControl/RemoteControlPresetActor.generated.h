// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROL_RemoteControlPresetActor_generated_h
#error "RemoteControlPresetActor.generated.h already included, missing '#pragma once' in RemoteControlPresetActor.h"
#endif
#define REMOTECONTROL_RemoteControlPresetActor_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARemoteControlPresetActor(); \
	friend struct Z_Construct_UClass_ARemoteControlPresetActor_Statics; \
public: \
	DECLARE_CLASS(ARemoteControlPresetActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(ARemoteControlPresetActor)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARemoteControlPresetActor(); \
	friend struct Z_Construct_UClass_ARemoteControlPresetActor_Statics; \
public: \
	DECLARE_CLASS(ARemoteControlPresetActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RemoteControl"), NO_API) \
	DECLARE_SERIALIZER(ARemoteControlPresetActor)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARemoteControlPresetActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARemoteControlPresetActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARemoteControlPresetActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARemoteControlPresetActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARemoteControlPresetActor(ARemoteControlPresetActor&&); \
	NO_API ARemoteControlPresetActor(const ARemoteControlPresetActor&); \
public:


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARemoteControlPresetActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARemoteControlPresetActor(ARemoteControlPresetActor&&); \
	NO_API ARemoteControlPresetActor(const ARemoteControlPresetActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARemoteControlPresetActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARemoteControlPresetActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARemoteControlPresetActor)


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REMOTECONTROL_API UClass* StaticClass<class ARemoteControlPresetActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControl_Source_RemoteControl_Public_RemoteControlPresetActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
