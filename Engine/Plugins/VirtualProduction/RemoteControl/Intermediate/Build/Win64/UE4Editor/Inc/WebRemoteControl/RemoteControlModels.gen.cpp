// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "WebRemoteControl/Private/RemoteControlModels.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlModels() {}
// Cross Module References
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCAssetFilter();
	UPackage* Z_Construct_UPackage__Script_WebRemoteControl();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldRenamed();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCAssetDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCShortPresetDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCExposedPropertyDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCExposedFunctionDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCExposedActorDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCObjectDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCFunctionDescription();
	WEBREMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRCPropertyDescription();
// End Cross Module References
class UScriptStruct* FRCAssetFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCAssetFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCAssetFilter, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCAssetFilter"), sizeof(FRCAssetFilter), Get_Z_Construct_UScriptStruct_FRCAssetFilter_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCAssetFilter>()
{
	return FRCAssetFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCAssetFilter(FRCAssetFilter::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCAssetFilter"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCAssetFilter
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCAssetFilter()
	{
		UScriptStruct::DeferCppStructOps<FRCAssetFilter>(FName(TEXT("RCAssetFilter")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCAssetFilter;
	struct Z_Construct_UScriptStruct_FRCAssetFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PackageNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PackageNames;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PackagePaths_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackagePaths_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PackagePaths;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ClassNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClassNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ClassNames;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RecursiveClassesExclusionSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecursiveClassesExclusionSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_RecursiveClassesExclusionSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecursiveClasses_MetaData[];
#endif
		static void NewProp_RecursiveClasses_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RecursiveClasses;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecursivePaths_MetaData[];
#endif
		static void NewProp_RecursivePaths_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RecursivePaths;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCAssetFilter>();
	}
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames_Inner = { "PackageNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames_MetaData[] = {
		{ "Comment", "/** The filter component for package names */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The filter component for package names" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames = { "PackageNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetFilter, PackageNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths_Inner = { "PackagePaths", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths_MetaData[] = {
		{ "Comment", "/** The filter component for package paths */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The filter component for package paths" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths = { "PackagePaths", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetFilter, PackagePaths), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames_Inner = { "ClassNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames_MetaData[] = {
		{ "Comment", "/** The filter component for class names. Instances of the specified classes, but not subclasses (by default), will be included. Derived classes will be included only if bRecursiveClasses is true. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The filter component for class names. Instances of the specified classes, but not subclasses (by default), will be included. Derived classes will be included only if bRecursiveClasses is true." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames = { "ClassNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetFilter, ClassNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet_ElementProp = { "RecursiveClassesExclusionSet", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet_MetaData[] = {
		{ "Comment", "/** Only if bRecursiveClasses is true, the results will exclude classes (and subclasses) in this list */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Only if bRecursiveClasses is true, the results will exclude classes (and subclasses) in this list" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet = { "RecursiveClassesExclusionSet", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetFilter, RecursiveClassesExclusionSet), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses_MetaData[] = {
		{ "Comment", "/** If true, subclasses of ClassNames will also be included and RecursiveClassesExclusionSet will be excluded. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "If true, subclasses of ClassNames will also be included and RecursiveClassesExclusionSet will be excluded." },
	};
#endif
	void Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses_SetBit(void* Obj)
	{
		((FRCAssetFilter*)Obj)->RecursiveClasses = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses = { "RecursiveClasses", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCAssetFilter), &Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths_MetaData[] = {
		{ "Comment", "/** If true, PackagePath components will be recursive */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "If true, PackagePath components will be recursive" },
	};
#endif
	void Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths_SetBit(void* Obj)
	{
		((FRCAssetFilter*)Obj)->RecursivePaths = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths = { "RecursivePaths", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRCAssetFilter), &Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCAssetFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackageNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_PackagePaths,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_ClassNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClassesExclusionSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursiveClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetFilter_Statics::NewProp_RecursivePaths,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCAssetFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCAssetFilter",
		sizeof(FRCAssetFilter),
		alignof(FRCAssetFilter),
		Z_Construct_UScriptStruct_FRCAssetFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCAssetFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCAssetFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCAssetFilter"), sizeof(FRCAssetFilter), Get_Z_Construct_UScriptStruct_FRCAssetFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCAssetFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCAssetFilter_Hash() { return 3380822154U; }
class UScriptStruct* FRCPresetFieldRenamed::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetFieldRenamed, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetFieldRenamed"), sizeof(FRCPresetFieldRenamed), Get_Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetFieldRenamed>()
{
	return FRCPresetFieldRenamed::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetFieldRenamed(FRCPresetFieldRenamed::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetFieldRenamed"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldRenamed
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldRenamed()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetFieldRenamed>(FName(TEXT("RCPresetFieldRenamed")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetFieldRenamed;
	struct Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OldFieldLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_OldFieldLabel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFieldLabel_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewFieldLabel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetFieldRenamed>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_OldFieldLabel_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_OldFieldLabel = { "OldFieldLabel", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldRenamed, OldFieldLabel), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_OldFieldLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_OldFieldLabel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_NewFieldLabel_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_NewFieldLabel = { "NewFieldLabel", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetFieldRenamed, NewFieldLabel), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_NewFieldLabel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_NewFieldLabel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_OldFieldLabel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::NewProp_NewFieldLabel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetFieldRenamed",
		sizeof(FRCPresetFieldRenamed),
		alignof(FRCPresetFieldRenamed),
		Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetFieldRenamed()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetFieldRenamed"), sizeof(FRCPresetFieldRenamed), Get_Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetFieldRenamed_Hash() { return 2837940775U; }
class UScriptStruct* FRCAssetDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCAssetDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCAssetDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCAssetDescription"), sizeof(FRCAssetDescription), Get_Z_Construct_UScriptStruct_FRCAssetDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCAssetDescription>()
{
	return FRCAssetDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCAssetDescription(FRCAssetDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCAssetDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCAssetDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCAssetDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCAssetDescription>(FName(TEXT("RCAssetDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCAssetDescription;
	struct Z_Construct_UScriptStruct_FRCAssetDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Class;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCAssetDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Class_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetDescription, Class), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Class_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Path_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetDescription, Path), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCAssetDescription, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCAssetDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Path,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCAssetDescription_Statics::NewProp_Metadata,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCAssetDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCAssetDescription",
		sizeof(FRCAssetDescription),
		alignof(FRCAssetDescription),
		Z_Construct_UScriptStruct_FRCAssetDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCAssetDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCAssetDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCAssetDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCAssetDescription"), sizeof(FRCAssetDescription), Get_Z_Construct_UScriptStruct_FRCAssetDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCAssetDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCAssetDescription_Hash() { return 1321900389U; }
class UScriptStruct* FRCShortPresetDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCShortPresetDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCShortPresetDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCShortPresetDescription"), sizeof(FRCShortPresetDescription), Get_Z_Construct_UScriptStruct_FRCShortPresetDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCShortPresetDescription>()
{
	return FRCShortPresetDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCShortPresetDescription(FRCShortPresetDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCShortPresetDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCShortPresetDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCShortPresetDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCShortPresetDescription>(FName(TEXT("RCShortPresetDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCShortPresetDescription;
	struct Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCShortPresetDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the preset.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Name of the preset." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCShortPresetDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_ID_MetaData[] = {
		{ "Comment", "/**\n\x09 * Unique identifier for the preset, can be used to make requests to the API.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Unique identifier for the preset, can be used to make requests to the API." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCShortPresetDescription, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_ID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Path_MetaData[] = {
		{ "Comment", "/**\n\x09 * Object path of the preset.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Object path of the preset." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCShortPresetDescription, Path), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_ID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::NewProp_Path,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCShortPresetDescription",
		sizeof(FRCShortPresetDescription),
		alignof(FRCShortPresetDescription),
		Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCShortPresetDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCShortPresetDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCShortPresetDescription"), sizeof(FRCShortPresetDescription), Get_Z_Construct_UScriptStruct_FRCShortPresetDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCShortPresetDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCShortPresetDescription_Hash() { return 368133160U; }
class UScriptStruct* FRCPresetDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetDescription"), sizeof(FRCPresetDescription), Get_Z_Construct_UScriptStruct_FRCPresetDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetDescription>()
{
	return FRCPresetDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetDescription(FRCPresetDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetDescription>(FName(TEXT("RCPresetDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetDescription;
	struct Z_Construct_UScriptStruct_FRCPresetDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ID;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Groups_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Groups_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Groups;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the preset.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Name of the preset." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Path_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the preset.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Name of the preset." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetDescription, Path), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Path_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_ID_MetaData[] = {
		{ "Comment", "/**\n\x09 * Unique identifier for the preset, can be used to make requests to the API.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Unique identifier for the preset, can be used to make requests to the API." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetDescription, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_ID_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups_Inner = { "Groups", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups_MetaData[] = {
		{ "Comment", "/**\n\x09 * The groups containing exposed entities.\n\x09 */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The groups containing exposed entities." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups = { "Groups", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetDescription, Groups), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Path,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_ID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetDescription_Statics::NewProp_Groups,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetDescription",
		sizeof(FRCPresetDescription),
		alignof(FRCPresetDescription),
		Z_Construct_UScriptStruct_FRCPresetDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetDescription"), sizeof(FRCPresetDescription), Get_Z_Construct_UScriptStruct_FRCPresetDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetDescription_Hash() { return 2967474793U; }
class UScriptStruct* FRCPresetModifiedEntitiesDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetModifiedEntitiesDescription"), sizeof(FRCPresetModifiedEntitiesDescription), Get_Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetModifiedEntitiesDescription>()
{
	return FRCPresetModifiedEntitiesDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetModifiedEntitiesDescription(FRCPresetModifiedEntitiesDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetModifiedEntitiesDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetModifiedEntitiesDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetModifiedEntitiesDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetModifiedEntitiesDescription>(FName(TEXT("RCPresetModifiedEntitiesDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetModifiedEntitiesDescription;
	struct Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedRCProperties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedRCProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModifiedRCProperties;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedRCFunctions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedRCFunctions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModifiedRCFunctions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedRCActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedRCActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModifiedRCActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Holds lists of modified RC entities.\n * @Note that this does not mean the underlying property/function/actor was modified,\n * but rather that the entity structure itself was modified in some way.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Holds lists of modified RC entities.\n@Note that this does not mean the underlying property/function/actor was modified,\nbut rather that the entity structure itself was modified in some way." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetModifiedEntitiesDescription>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties_Inner = { "ModifiedRCProperties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCExposedPropertyDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties_MetaData[] = {
		{ "Comment", "/** The list of modified RC properties. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The list of modified RC properties." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties = { "ModifiedRCProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetModifiedEntitiesDescription, ModifiedRCProperties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions_Inner = { "ModifiedRCFunctions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCExposedFunctionDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions_MetaData[] = {
		{ "Comment", "/** The list of modified RC functions. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The list of modified RC functions." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions = { "ModifiedRCFunctions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetModifiedEntitiesDescription, ModifiedRCFunctions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors_Inner = { "ModifiedRCActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCExposedActorDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors_MetaData[] = {
		{ "Comment", "/** The list of modified RC actors. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The list of modified RC actors." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors = { "ModifiedRCActors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetModifiedEntitiesDescription, ModifiedRCActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCFunctions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::NewProp_ModifiedRCActors,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetModifiedEntitiesDescription",
		sizeof(FRCPresetModifiedEntitiesDescription),
		alignof(FRCPresetModifiedEntitiesDescription),
		Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetModifiedEntitiesDescription"), sizeof(FRCPresetModifiedEntitiesDescription), Get_Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetModifiedEntitiesDescription_Hash() { return 2842045609U; }
class UScriptStruct* FRCPresetLayoutGroupDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPresetLayoutGroupDescription"), sizeof(FRCPresetLayoutGroupDescription), Get_Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPresetLayoutGroupDescription>()
{
	return FRCPresetLayoutGroupDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPresetLayoutGroupDescription(FRCPresetLayoutGroupDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPresetLayoutGroupDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetLayoutGroupDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetLayoutGroupDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCPresetLayoutGroupDescription>(FName(TEXT("RCPresetLayoutGroupDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPresetLayoutGroupDescription;
	struct Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposedProperties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposedProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExposedProperties;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposedFunctions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposedFunctions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExposedFunctions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExposedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExposedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExposedActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPresetLayoutGroupDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetLayoutGroupDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties_Inner = { "ExposedProperties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCExposedPropertyDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties = { "ExposedProperties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetLayoutGroupDescription, ExposedProperties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions_Inner = { "ExposedFunctions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCExposedFunctionDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions = { "ExposedFunctions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetLayoutGroupDescription, ExposedFunctions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors_Inner = { "ExposedActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCExposedActorDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors_MetaData[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors = { "ExposedActors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPresetLayoutGroupDescription, ExposedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedFunctions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::NewProp_ExposedActors,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPresetLayoutGroupDescription",
		sizeof(FRCPresetLayoutGroupDescription),
		alignof(FRCPresetLayoutGroupDescription),
		Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPresetLayoutGroupDescription"), sizeof(FRCPresetLayoutGroupDescription), Get_Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPresetLayoutGroupDescription_Hash() { return 4082388451U; }
class UScriptStruct* FRCExposedActorDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCExposedActorDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCExposedActorDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCExposedActorDescription"), sizeof(FRCExposedActorDescription), Get_Z_Construct_UScriptStruct_FRCExposedActorDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCExposedActorDescription>()
{
	return FRCExposedActorDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCExposedActorDescription(FRCExposedActorDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCExposedActorDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedActorDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedActorDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCExposedActorDescription>(FName(TEXT("RCExposedActorDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedActorDescription;
	struct Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderlyingActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UnderlyingActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCExposedActorDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Comment", "/** The label displayed in the remote control panel for this exposed actor. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The label displayed in the remote control panel for this exposed actor." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedActorDescription, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_ID_MetaData[] = {
		{ "Comment", "/** Unique identifier for the exposed actor. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Unique identifier for the exposed actor." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedActorDescription, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_ID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_UnderlyingActor_MetaData[] = {
		{ "Comment", "/** The underlying exposed actor. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The underlying exposed actor." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_UnderlyingActor = { "UnderlyingActor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedActorDescription, UnderlyingActor), Z_Construct_UScriptStruct_FRCObjectDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_UnderlyingActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_UnderlyingActor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_ID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::NewProp_UnderlyingActor,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCExposedActorDescription",
		sizeof(FRCExposedActorDescription),
		alignof(FRCExposedActorDescription),
		Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCExposedActorDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCExposedActorDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCExposedActorDescription"), sizeof(FRCExposedActorDescription), Get_Z_Construct_UScriptStruct_FRCExposedActorDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCExposedActorDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCExposedActorDescription_Hash() { return 913155781U; }
class UScriptStruct* FRCExposedFunctionDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCExposedFunctionDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCExposedFunctionDescription"), sizeof(FRCExposedFunctionDescription), Get_Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCExposedFunctionDescription>()
{
	return FRCExposedFunctionDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCExposedFunctionDescription(FRCExposedFunctionDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCExposedFunctionDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedFunctionDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedFunctionDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCExposedFunctionDescription>(FName(TEXT("RCExposedFunctionDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedFunctionDescription;
	struct Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderlyingFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UnderlyingFunction;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OwnerObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCExposedFunctionDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Comment", "/** The label displayed in the remote control panel for this exposed property. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The label displayed in the remote control panel for this exposed property." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedFunctionDescription, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_ID_MetaData[] = {
		{ "Comment", "/** Unique identifier for the exposed function. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Unique identifier for the exposed function." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedFunctionDescription, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_ID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_UnderlyingFunction_MetaData[] = {
		{ "Comment", "/** The underlying exposed function. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The underlying exposed function." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_UnderlyingFunction = { "UnderlyingFunction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedFunctionDescription, UnderlyingFunction), Z_Construct_UScriptStruct_FRCFunctionDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_UnderlyingFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_UnderlyingFunction_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects_Inner = { "OwnerObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCObjectDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects_MetaData[] = {
		{ "Comment", "/** The objects that own the underlying function. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The objects that own the underlying function." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects = { "OwnerObjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedFunctionDescription, OwnerObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_ID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_UnderlyingFunction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::NewProp_OwnerObjects,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCExposedFunctionDescription",
		sizeof(FRCExposedFunctionDescription),
		alignof(FRCExposedFunctionDescription),
		Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCExposedFunctionDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCExposedFunctionDescription"), sizeof(FRCExposedFunctionDescription), Get_Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCExposedFunctionDescription_Hash() { return 476420275U; }
class UScriptStruct* FRCExposedPropertyDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCExposedPropertyDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCExposedPropertyDescription"), sizeof(FRCExposedPropertyDescription), Get_Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCExposedPropertyDescription>()
{
	return FRCExposedPropertyDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCExposedPropertyDescription(FRCExposedPropertyDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCExposedPropertyDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedPropertyDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedPropertyDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCExposedPropertyDescription>(FName(TEXT("RCExposedPropertyDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCExposedPropertyDescription;
	struct Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderlyingProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UnderlyingProperty;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OwnerObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCExposedPropertyDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Comment", "/** The label displayed in the remote control panel for this exposed property. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The label displayed in the remote control panel for this exposed property." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedPropertyDescription, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_ID_MetaData[] = {
		{ "Comment", "/** Unique identifier for the exposed property. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Unique identifier for the exposed property." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedPropertyDescription, ID), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_ID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_UnderlyingProperty_MetaData[] = {
		{ "Comment", "/** The underlying exposed property. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The underlying exposed property." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_UnderlyingProperty = { "UnderlyingProperty", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedPropertyDescription, UnderlyingProperty), Z_Construct_UScriptStruct_FRCPropertyDescription, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_UnderlyingProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_UnderlyingProperty_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_MetaData[] = {
		{ "Comment", "/** Metadata specific to this exposed property. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Metadata specific to this exposed property." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedPropertyDescription, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects_Inner = { "OwnerObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCObjectDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects_MetaData[] = {
		{ "Comment", "/** The objects that own the underlying property. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The objects that own the underlying property." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects = { "OwnerObjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCExposedPropertyDescription, OwnerObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_ID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_UnderlyingProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_Metadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::NewProp_OwnerObjects,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCExposedPropertyDescription",
		sizeof(FRCExposedPropertyDescription),
		alignof(FRCExposedPropertyDescription),
		Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCExposedPropertyDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCExposedPropertyDescription"), sizeof(FRCExposedPropertyDescription), Get_Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCExposedPropertyDescription_Hash() { return 3209207362U; }
class UScriptStruct* FRCFunctionDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCFunctionDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCFunctionDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCFunctionDescription"), sizeof(FRCFunctionDescription), Get_Z_Construct_UScriptStruct_FRCFunctionDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCFunctionDescription>()
{
	return FRCFunctionDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCFunctionDescription(FRCFunctionDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCFunctionDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCFunctionDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCFunctionDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCFunctionDescription>(FName(TEXT("RCFunctionDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCFunctionDescription;
	struct Z_Construct_UScriptStruct_FRCFunctionDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Arguments_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arguments_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Arguments;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCFunctionDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/** Name of the function. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Name of the function." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFunctionDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Description_MetaData[] = {
		{ "Comment", "/** Description for the function. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Description for the function." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFunctionDescription, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Description_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments_Inner = { "Arguments", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRCPropertyDescription, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments_MetaData[] = {
		{ "Comment", "/** The function's arguments. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "The function's arguments." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments = { "Arguments", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCFunctionDescription, Arguments), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::NewProp_Arguments,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCFunctionDescription",
		sizeof(FRCFunctionDescription),
		alignof(FRCFunctionDescription),
		Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCFunctionDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCFunctionDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCFunctionDescription"), sizeof(FRCFunctionDescription), Get_Z_Construct_UScriptStruct_FRCFunctionDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCFunctionDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCFunctionDescription_Hash() { return 3437885643U; }
class UScriptStruct* FRCPropertyDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCPropertyDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCPropertyDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCPropertyDescription"), sizeof(FRCPropertyDescription), Get_Z_Construct_UScriptStruct_FRCPropertyDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCPropertyDescription>()
{
	return FRCPropertyDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCPropertyDescription(FRCPropertyDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCPropertyDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPropertyDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPropertyDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCPropertyDescription>(FName(TEXT("RCPropertyDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCPropertyDescription;
	struct Z_Construct_UScriptStruct_FRCPropertyDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContainerType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ContainerType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_KeyType;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Metadata_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Metadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Metadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Metadata;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCPropertyDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/** Name of the exposed property */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Name of the exposed property" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Description_MetaData[] = {
		{ "Comment", "/** Description of the exposed property */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Description of the exposed property" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyDescription, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Type_MetaData[] = {
		{ "Comment", "/** Type of the property value (If an array, this will be the content of the array) */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Type of the property value (If an array, this will be the content of the array)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyDescription, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_ContainerType_MetaData[] = {
		{ "Comment", "/** Type of the container (TMap, TArray, CArray, TSet) or empty if none */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Type of the container (TMap, TArray, CArray, TSet) or empty if none" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_ContainerType = { "ContainerType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyDescription, ContainerType), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_ContainerType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_ContainerType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_KeyType_MetaData[] = {
		{ "Comment", "/** Key type if container is a map */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Key type if container is a map" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_KeyType = { "KeyType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyDescription, KeyType), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_KeyType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_KeyType_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_ValueProp = { "Metadata", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_Key_KeyProp = { "Metadata_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_MetaData[] = {
		{ "Comment", "/** Metadata for this exposed property */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Metadata for this exposed property" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata = { "Metadata", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCPropertyDescription, Metadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_ContainerType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_KeyType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::NewProp_Metadata,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCPropertyDescription",
		sizeof(FRCPropertyDescription),
		alignof(FRCPropertyDescription),
		Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCPropertyDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCPropertyDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCPropertyDescription"), sizeof(FRCPropertyDescription), Get_Z_Construct_UScriptStruct_FRCPropertyDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCPropertyDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCPropertyDescription_Hash() { return 2558775782U; }
class UScriptStruct* FRCObjectDescription::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WEBREMOTECONTROL_API uint32 Get_Z_Construct_UScriptStruct_FRCObjectDescription_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRCObjectDescription, Z_Construct_UPackage__Script_WebRemoteControl(), TEXT("RCObjectDescription"), sizeof(FRCObjectDescription), Get_Z_Construct_UScriptStruct_FRCObjectDescription_Hash());
	}
	return Singleton;
}
template<> WEBREMOTECONTROL_API UScriptStruct* StaticStruct<FRCObjectDescription>()
{
	return FRCObjectDescription::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRCObjectDescription(FRCObjectDescription::StaticStruct, TEXT("/Script/WebRemoteControl"), TEXT("RCObjectDescription"), false, nullptr, nullptr);
static struct FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCObjectDescription
{
	FScriptStruct_WebRemoteControl_StaticRegisterNativesFRCObjectDescription()
	{
		UScriptStruct::DeferCppStructOps<FRCObjectDescription>(FName(TEXT("RCObjectDescription")));
	}
} ScriptStruct_WebRemoteControl_StaticRegisterNativesFRCObjectDescription;
	struct Z_Construct_UScriptStruct_FRCObjectDescription_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Class;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectDescription_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Short description of an unreal object.\n */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Short description of an unreal object." },
	};
#endif
	void* Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRCObjectDescription>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Name_MetaData[] = {
		{ "Comment", "/** Name of the object. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Name of the object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCObjectDescription, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Class_MetaData[] = {
		{ "Comment", "/** Class of the object. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Class of the object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCObjectDescription, Class), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Class_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Path_MetaData[] = {
		{ "Comment", "/** Path of the object. */" },
		{ "ModuleRelativePath", "Private/RemoteControlModels.h" },
		{ "ToolTip", "Path of the object." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRCObjectDescription, Path), METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRCObjectDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRCObjectDescription_Statics::NewProp_Path,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRCObjectDescription_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WebRemoteControl,
		nullptr,
		&NewStructOps,
		"RCObjectDescription",
		sizeof(FRCObjectDescription),
		alignof(FRCObjectDescription),
		Z_Construct_UScriptStruct_FRCObjectDescription_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRCObjectDescription_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRCObjectDescription()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRCObjectDescription_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WebRemoteControl();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RCObjectDescription"), sizeof(FRCObjectDescription), Get_Z_Construct_UScriptStruct_FRCObjectDescription_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRCObjectDescription_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRCObjectDescription_Hash() { return 808744441U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
