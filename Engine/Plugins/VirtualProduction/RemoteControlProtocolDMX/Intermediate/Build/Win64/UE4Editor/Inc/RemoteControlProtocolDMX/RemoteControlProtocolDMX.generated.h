// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REMOTECONTROLPROTOCOLDMX_RemoteControlProtocolDMX_generated_h
#error "RemoteControlProtocolDMX.generated.h already included, missing '#pragma once' in RemoteControlProtocolDMX.h"
#endif
#define REMOTECONTROLPROTOCOLDMX_RemoteControlProtocolDMX_generated_h

#define Engine_Plugins_VirtualProduction_RemoteControlProtocolDMX_Source_RemoteControlProtocolDMX_Public_RemoteControlProtocolDMX_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics; \
	REMOTECONTROLPROTOCOLDMX_API static class UScriptStruct* StaticStruct(); \
	typedef FRemoteControlProtocolEntity Super;


template<> REMOTECONTROLPROTOCOLDMX_API UScriptStruct* StaticStruct<struct FRemoteControlDMXProtocolEntity>();

#define Engine_Plugins_VirtualProduction_RemoteControlProtocolDMX_Source_RemoteControlProtocolDMX_Public_RemoteControlProtocolDMX_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics; \
	REMOTECONTROLPROTOCOLDMX_API static class UScriptStruct* StaticStruct();


template<> REMOTECONTROLPROTOCOLDMX_API UScriptStruct* StaticStruct<struct FRemoteControlDMXProtocolEntityExtraSetting>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_RemoteControlProtocolDMX_Source_RemoteControlProtocolDMX_Public_RemoteControlProtocolDMX_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
