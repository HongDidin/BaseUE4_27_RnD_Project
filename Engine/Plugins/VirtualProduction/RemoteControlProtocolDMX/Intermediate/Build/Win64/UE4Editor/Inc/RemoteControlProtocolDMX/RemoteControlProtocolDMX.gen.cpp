// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RemoteControlProtocolDMX/Public/RemoteControlProtocolDMX.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoteControlProtocolDMX() {}
// Cross Module References
	REMOTECONTROLPROTOCOLDMX_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity();
	UPackage* Z_Construct_UPackage__Script_RemoteControlProtocolDMX();
	REMOTECONTROL_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlProtocolEntity();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat();
	REMOTECONTROLPROTOCOLDMX_API UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting();
// End Cross Module References

static_assert(std::is_polymorphic<FRemoteControlDMXProtocolEntity>() == std::is_polymorphic<FRemoteControlProtocolEntity>(), "USTRUCT FRemoteControlDMXProtocolEntity cannot be polymorphic unless super FRemoteControlProtocolEntity is polymorphic");

class UScriptStruct* FRemoteControlDMXProtocolEntity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLPROTOCOLDMX_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity, Z_Construct_UPackage__Script_RemoteControlProtocolDMX(), TEXT("RemoteControlDMXProtocolEntity"), sizeof(FRemoteControlDMXProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLPROTOCOLDMX_API UScriptStruct* StaticStruct<FRemoteControlDMXProtocolEntity>()
{
	return FRemoteControlDMXProtocolEntity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlDMXProtocolEntity(FRemoteControlDMXProtocolEntity::StaticStruct, TEXT("/Script/RemoteControlProtocolDMX"), TEXT("RemoteControlDMXProtocolEntity"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlProtocolDMX_StaticRegisterNativesFRemoteControlDMXProtocolEntity
{
	FScriptStruct_RemoteControlProtocolDMX_StaticRegisterNativesFRemoteControlDMXProtocolEntity()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlDMXProtocolEntity>(FName(TEXT("RemoteControlDMXProtocolEntity")));
	}
} ScriptStruct_RemoteControlProtocolDMX_StaticRegisterNativesFRemoteControlDMXProtocolEntity;
	struct Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Universe_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Universe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseLSB_MetaData[];
#endif
		static void NewProp_bUseLSB_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseLSB;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtraSetting_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExtraSetting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RangeInputTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_RangeInputTemplate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * DMX protocol entity for remote control binding\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "DMX protocol entity for remote control binding" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlDMXProtocolEntity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_Universe_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** DMX universe id */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "DMX universe id" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_Universe = { "Universe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlDMXProtocolEntity, Universe), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_Universe_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_Universe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/**\n\x09 * Least Significant Byte mode makes the individual bytes (channels) of the function be\n\x09 * interpreted with the first bytes being the lowest part of the number.\n\x09 * Most Fixtures use MSB (Most Significant Byte).\n\x09 */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "Least Significant Byte mode makes the individual bytes (channels) of the function be\ninterpreted with the first bytes being the lowest part of the number.\nMost Fixtures use MSB (Most Significant Byte)." },
	};
#endif
	void Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB_SetBit(void* Obj)
	{
		((FRemoteControlDMXProtocolEntity*)Obj)->bUseLSB = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB = { "bUseLSB", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRemoteControlDMXProtocolEntity), &Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Defines the used number of channels (bytes) */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "Defines the used number of channels (bytes)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType = { "DataType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlDMXProtocolEntity, DataType), Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_ExtraSetting_MetaData[] = {
		{ "Category", "Mapping" },
		{ "Comment", "/** Extra protocol settings. Primary using for customization */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Extra protocol settings. Primary using for customization" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_ExtraSetting = { "ExtraSetting", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlDMXProtocolEntity, ExtraSetting), Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting, METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_ExtraSetting_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_ExtraSetting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData[] = {
		{ "Comment", "/** DMX range input property template, used for binding. */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "DMX range input property template, used for binding." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_RangeInputTemplate = { "RangeInputTemplate", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlDMXProtocolEntity, RangeInputTemplate), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_RangeInputTemplate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_Universe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_bUseLSB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_DataType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_ExtraSetting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::NewProp_RangeInputTemplate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolDMX,
		Z_Construct_UScriptStruct_FRemoteControlProtocolEntity,
		&NewStructOps,
		"RemoteControlDMXProtocolEntity",
		sizeof(FRemoteControlDMXProtocolEntity),
		alignof(FRemoteControlDMXProtocolEntity),
		Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolDMX();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlDMXProtocolEntity"), sizeof(FRemoteControlDMXProtocolEntity), Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntity_Hash() { return 610701817U; }
class UScriptStruct* FRemoteControlDMXProtocolEntityExtraSetting::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern REMOTECONTROLPROTOCOLDMX_API uint32 Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting, Z_Construct_UPackage__Script_RemoteControlProtocolDMX(), TEXT("RemoteControlDMXProtocolEntityExtraSetting"), sizeof(FRemoteControlDMXProtocolEntityExtraSetting), Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Hash());
	}
	return Singleton;
}
template<> REMOTECONTROLPROTOCOLDMX_API UScriptStruct* StaticStruct<FRemoteControlDMXProtocolEntityExtraSetting>()
{
	return FRemoteControlDMXProtocolEntityExtraSetting::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting(FRemoteControlDMXProtocolEntityExtraSetting::StaticStruct, TEXT("/Script/RemoteControlProtocolDMX"), TEXT("RemoteControlDMXProtocolEntityExtraSetting"), false, nullptr, nullptr);
static struct FScriptStruct_RemoteControlProtocolDMX_StaticRegisterNativesFRemoteControlDMXProtocolEntityExtraSetting
{
	FScriptStruct_RemoteControlProtocolDMX_StaticRegisterNativesFRemoteControlDMXProtocolEntityExtraSetting()
	{
		UScriptStruct::DeferCppStructOps<FRemoteControlDMXProtocolEntityExtraSetting>(FName(TEXT("RemoteControlDMXProtocolEntityExtraSetting")));
	}
} ScriptStruct_RemoteControlProtocolDMX_StaticRegisterNativesFRemoteControlDMXProtocolEntityExtraSetting;
	struct Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartingChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StartingChannel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Using as an inner struct for details customization.\n * Useful to have type customization for the struct\n */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "Using as an inner struct for details customization.\nUseful to have type customization for the struct" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRemoteControlDMXProtocolEntityExtraSetting>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::NewProp_StartingChannel_MetaData[] = {
		{ "Category", "Mapping" },
		{ "ClampMax", "512" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Starting universe channel */" },
		{ "ModuleRelativePath", "Public/RemoteControlProtocolDMX.h" },
		{ "ToolTip", "Starting universe channel" },
		{ "UIMax", "512" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::NewProp_StartingChannel = { "StartingChannel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRemoteControlDMXProtocolEntityExtraSetting, StartingChannel), METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::NewProp_StartingChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::NewProp_StartingChannel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::NewProp_StartingChannel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RemoteControlProtocolDMX,
		nullptr,
		&NewStructOps,
		"RemoteControlDMXProtocolEntityExtraSetting",
		sizeof(FRemoteControlDMXProtocolEntityExtraSetting),
		alignof(FRemoteControlDMXProtocolEntityExtraSetting),
		Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RemoteControlProtocolDMX();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RemoteControlDMXProtocolEntityExtraSetting"), sizeof(FRemoteControlDMXProtocolEntityExtraSetting), Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRemoteControlDMXProtocolEntityExtraSetting_Hash() { return 4010923567U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
