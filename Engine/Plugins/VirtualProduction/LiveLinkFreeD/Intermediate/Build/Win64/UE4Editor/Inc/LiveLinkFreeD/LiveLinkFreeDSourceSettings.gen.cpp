// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkFreeD/Public/LiveLinkFreeDSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkFreeDSourceSettings() {}
// Cross Module References
	LIVELINKFREED_API UEnum* Z_Construct_UEnum_LiveLinkFreeD_EFreeDDefaultConfigs();
	UPackage* Z_Construct_UPackage__Script_LiveLinkFreeD();
	LIVELINKFREED_API UScriptStruct* Z_Construct_UScriptStruct_FFreeDEncoderData();
	LIVELINKFREED_API UClass* Z_Construct_UClass_ULiveLinkFreeDSourceSettings_NoRegister();
	LIVELINKFREED_API UClass* Z_Construct_UClass_ULiveLinkFreeDSourceSettings();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkSourceSettings();
// End Cross Module References
	static UEnum* EFreeDDefaultConfigs_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LiveLinkFreeD_EFreeDDefaultConfigs, Z_Construct_UPackage__Script_LiveLinkFreeD(), TEXT("EFreeDDefaultConfigs"));
		}
		return Singleton;
	}
	template<> LIVELINKFREED_API UEnum* StaticEnum<EFreeDDefaultConfigs>()
	{
		return EFreeDDefaultConfigs_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFreeDDefaultConfigs(EFreeDDefaultConfigs_StaticEnum, TEXT("/Script/LiveLinkFreeD"), TEXT("EFreeDDefaultConfigs"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LiveLinkFreeD_EFreeDDefaultConfigs_Hash() { return 3110995394U; }
	UEnum* Z_Construct_UEnum_LiveLinkFreeD_EFreeDDefaultConfigs()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkFreeD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFreeDDefaultConfigs"), 0, Get_Z_Construct_UEnum_LiveLinkFreeD_EFreeDDefaultConfigs_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFreeDDefaultConfigs::Generic", (int64)EFreeDDefaultConfigs::Generic },
				{ "EFreeDDefaultConfigs::Panasonic", (int64)EFreeDDefaultConfigs::Panasonic },
				{ "EFreeDDefaultConfigs::Sony", (int64)EFreeDDefaultConfigs::Sony },
				{ "EFreeDDefaultConfigs::Stype", (int64)EFreeDDefaultConfigs::Stype },
				{ "EFreeDDefaultConfigs::Mosys", (int64)EFreeDDefaultConfigs::Mosys },
				{ "EFreeDDefaultConfigs::Ncam", (int64)EFreeDDefaultConfigs::Ncam },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Generic.Name", "EFreeDDefaultConfigs::Generic" },
				{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
				{ "Mosys.Name", "EFreeDDefaultConfigs::Mosys" },
				{ "Ncam.Name", "EFreeDDefaultConfigs::Ncam" },
				{ "Panasonic.Name", "EFreeDDefaultConfigs::Panasonic" },
				{ "Sony.Name", "EFreeDDefaultConfigs::Sony" },
				{ "Stype.DisplayName", "stYpe" },
				{ "Stype.Name", "EFreeDDefaultConfigs::Stype" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LiveLinkFreeD,
				nullptr,
				"EFreeDDefaultConfigs",
				"EFreeDDefaultConfigs",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FFreeDEncoderData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKFREED_API uint32 Get_Z_Construct_UScriptStruct_FFreeDEncoderData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFreeDEncoderData, Z_Construct_UPackage__Script_LiveLinkFreeD(), TEXT("FreeDEncoderData"), sizeof(FFreeDEncoderData), Get_Z_Construct_UScriptStruct_FFreeDEncoderData_Hash());
	}
	return Singleton;
}
template<> LIVELINKFREED_API UScriptStruct* StaticStruct<FFreeDEncoderData>()
{
	return FFreeDEncoderData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFreeDEncoderData(FFreeDEncoderData::StaticStruct, TEXT("/Script/LiveLinkFreeD"), TEXT("FreeDEncoderData"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkFreeD_StaticRegisterNativesFFreeDEncoderData
{
	FScriptStruct_LiveLinkFreeD_StaticRegisterNativesFFreeDEncoderData()
	{
		UScriptStruct::DeferCppStructOps<FFreeDEncoderData>(FName(TEXT("FreeDEncoderData")));
	}
} ScriptStruct_LiveLinkFreeD_StaticRegisterNativesFFreeDEncoderData;
	struct Z_Construct_UScriptStruct_FFreeDEncoderData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsValid_MetaData[];
#endif
		static void NewProp_bIsValid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsValid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertEncoder_MetaData[];
#endif
		static void NewProp_bInvertEncoder_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertEncoder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseManualRange_MetaData[];
#endif
		static void NewProp_bUseManualRange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseManualRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Min_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Min;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Max_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Max;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskBits_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaskBits;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFreeDEncoderData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "Comment", "/** Is this encoder data valid? */" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Is this encoder data valid?" },
	};
#endif
	void Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid_SetBit(void* Obj)
	{
		((FFreeDEncoderData*)Obj)->bIsValid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid = { "bIsValid", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFreeDEncoderData), &Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "Comment", "/** Invert the encoder input direction */" },
		{ "EditCondition", "bIsValid" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Invert the encoder input direction" },
	};
#endif
	void Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder_SetBit(void* Obj)
	{
		((FFreeDEncoderData*)Obj)->bInvertEncoder = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder = { "bInvertEncoder", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFreeDEncoderData), &Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "Comment", "/** Use manual Min/Max values for the encoder normalization (normally uses dynamic auto ranging based on inputs) */" },
		{ "EditCondition", "bIsValid" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Use manual Min/Max values for the encoder normalization (normally uses dynamic auto ranging based on inputs)" },
	};
#endif
	void Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange_SetBit(void* Obj)
	{
		((FFreeDEncoderData*)Obj)->bUseManualRange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange = { "bUseManualRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FFreeDEncoderData), &Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Min_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "ClampMax", "16777215" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Minimum raw encoder value */" },
		{ "EditCondition", "bIsValid && bUseManualRange" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Minimum raw encoder value" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Min = { "Min", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFreeDEncoderData, Min), METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Min_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Min_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Max_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "ClampMax", "16777215" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Maximum raw encoder value */" },
		{ "EditCondition", "bIsValid && bUseManualRange" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Maximum raw encoder value" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Max = { "Max", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFreeDEncoderData, Max), METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Max_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Max_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_MaskBits_MetaData[] = {
		{ "Category", "Encoder Data" },
		{ "ClampMax", "16777215" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Mask bits for raw encoder value */" },
		{ "EditCondition", "bIsValid" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Mask bits for raw encoder value" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_MaskBits = { "MaskBits", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFreeDEncoderData, MaskBits), METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_MaskBits_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_MaskBits_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bIsValid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bInvertEncoder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_bUseManualRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Min,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_Max,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::NewProp_MaskBits,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkFreeD,
		nullptr,
		&NewStructOps,
		"FreeDEncoderData",
		sizeof(FFreeDEncoderData),
		alignof(FFreeDEncoderData),
		Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFreeDEncoderData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFreeDEncoderData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkFreeD();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FreeDEncoderData"), sizeof(FFreeDEncoderData), Get_Z_Construct_UScriptStruct_FFreeDEncoderData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFreeDEncoderData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFreeDEncoderData_Hash() { return 4115188715U; }
	void ULiveLinkFreeDSourceSettings::StaticRegisterNativesULiveLinkFreeDSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkFreeDSourceSettings_NoRegister()
	{
		return ULiveLinkFreeDSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSendExtraMetaData_MetaData[];
#endif
		static void NewProp_bSendExtraMetaData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSendExtraMetaData;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DefaultConfig_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DefaultConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusDistanceEncoderData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusDistanceEncoderData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthEncoderData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocalLengthEncoderData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserDefinedEncoderData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UserDefinedEncoderData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkSourceSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkFreeD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkFreeDSourceSettings.h" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Send extra string meta data (Camera ID and FrameCounter) */" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Send extra string meta data (Camera ID and FrameCounter)" },
	};
#endif
	void Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData_SetBit(void* Obj)
	{
		((ULiveLinkFreeDSourceSettings*)Obj)->bSendExtraMetaData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData = { "bSendExtraMetaData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULiveLinkFreeDSourceSettings), &Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Default configurations for specific manufacturers */" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Default configurations for specific manufacturers" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig = { "DefaultConfig", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkFreeDSourceSettings, DefaultConfig), Z_Construct_UEnum_LiveLinkFreeD_EFreeDDefaultConfigs, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocusDistanceEncoderData_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Raw focus distance (in cm) encoder parameters for this camera - 24 bits max */" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Raw focus distance (in cm) encoder parameters for this camera - 24 bits max" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocusDistanceEncoderData = { "FocusDistanceEncoderData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkFreeDSourceSettings, FocusDistanceEncoderData), Z_Construct_UScriptStruct_FFreeDEncoderData, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocusDistanceEncoderData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocusDistanceEncoderData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocalLengthEncoderData_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Raw focal length/zoom (in mm) encoder parameters for this camera - 24 bits max */" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Raw focal length/zoom (in mm) encoder parameters for this camera - 24 bits max" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocalLengthEncoderData = { "FocalLengthEncoderData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkFreeDSourceSettings, FocalLengthEncoderData), Z_Construct_UScriptStruct_FFreeDEncoderData, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocalLengthEncoderData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocalLengthEncoderData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_UserDefinedEncoderData_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Raw user defined/spare data encoder (normally used for Aperture) parameters for this camera - 16 bits max */" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceSettings.h" },
		{ "ToolTip", "Raw user defined/spare data encoder (normally used for Aperture) parameters for this camera - 16 bits max" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_UserDefinedEncoderData = { "UserDefinedEncoderData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULiveLinkFreeDSourceSettings, UserDefinedEncoderData), Z_Construct_UScriptStruct_FFreeDEncoderData, METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_UserDefinedEncoderData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_UserDefinedEncoderData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_bSendExtraMetaData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_DefaultConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocusDistanceEncoderData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_FocalLengthEncoderData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::NewProp_UserDefinedEncoderData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkFreeDSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::ClassParams = {
		&ULiveLinkFreeDSourceSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkFreeDSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkFreeDSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkFreeDSourceSettings, 806603847);
	template<> LIVELINKFREED_API UClass* StaticClass<ULiveLinkFreeDSourceSettings>()
	{
		return ULiveLinkFreeDSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkFreeDSourceSettings(Z_Construct_UClass_ULiveLinkFreeDSourceSettings, &ULiveLinkFreeDSourceSettings::StaticClass, TEXT("/Script/LiveLinkFreeD"), TEXT("ULiveLinkFreeDSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkFreeDSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
