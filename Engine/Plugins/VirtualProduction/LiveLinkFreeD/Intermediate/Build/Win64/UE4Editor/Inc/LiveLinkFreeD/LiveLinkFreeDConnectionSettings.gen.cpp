// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkFreeD/Private/LiveLinkFreeDConnectionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkFreeDConnectionSettings() {}
// Cross Module References
	LIVELINKFREED_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings();
	UPackage* Z_Construct_UPackage__Script_LiveLinkFreeD();
// End Cross Module References
class UScriptStruct* FLiveLinkFreeDConnectionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LIVELINKFREED_API uint32 Get_Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings, Z_Construct_UPackage__Script_LiveLinkFreeD(), TEXT("LiveLinkFreeDConnectionSettings"), sizeof(FLiveLinkFreeDConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Hash());
	}
	return Singleton;
}
template<> LIVELINKFREED_API UScriptStruct* StaticStruct<FLiveLinkFreeDConnectionSettings>()
{
	return FLiveLinkFreeDConnectionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLiveLinkFreeDConnectionSettings(FLiveLinkFreeDConnectionSettings::StaticStruct, TEXT("/Script/LiveLinkFreeD"), TEXT("LiveLinkFreeDConnectionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LiveLinkFreeD_StaticRegisterNativesFLiveLinkFreeDConnectionSettings
{
	FScriptStruct_LiveLinkFreeD_StaticRegisterNativesFLiveLinkFreeDConnectionSettings()
	{
		UScriptStruct::DeferCppStructOps<FLiveLinkFreeDConnectionSettings>(FName(TEXT("LiveLinkFreeDConnectionSettings")));
	}
} ScriptStruct_LiveLinkFreeD_StaticRegisterNativesFLiveLinkFreeDConnectionSettings;
	struct Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IPAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IPAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UDPPortNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_UDPPortNumber;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/LiveLinkFreeDConnectionSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLiveLinkFreeDConnectionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_IPAddress_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** IP address of the free-d tracking source */" },
		{ "ModuleRelativePath", "Private/LiveLinkFreeDConnectionSettings.h" },
		{ "ToolTip", "IP address of the free-d tracking source" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_IPAddress = { "IPAddress", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkFreeDConnectionSettings, IPAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_IPAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_IPAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_UDPPortNumber_MetaData[] = {
		{ "Category", "Connection Settings" },
		{ "Comment", "/** UDP port number */" },
		{ "ModuleRelativePath", "Private/LiveLinkFreeDConnectionSettings.h" },
		{ "ToolTip", "UDP port number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_UDPPortNumber = { "UDPPortNumber", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLiveLinkFreeDConnectionSettings, UDPPortNumber), METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_UDPPortNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_UDPPortNumber_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_IPAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::NewProp_UDPPortNumber,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkFreeD,
		nullptr,
		&NewStructOps,
		"LiveLinkFreeDConnectionSettings",
		sizeof(FLiveLinkFreeDConnectionSettings),
		alignof(FLiveLinkFreeDConnectionSettings),
		Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LiveLinkFreeD();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LiveLinkFreeDConnectionSettings"), sizeof(FLiveLinkFreeDConnectionSettings), Get_Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLiveLinkFreeDConnectionSettings_Hash() { return 1313464016U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
