// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LIVELINKFREED_LiveLinkFreeDSourceFactory_generated_h
#error "LiveLinkFreeDSourceFactory.generated.h already included, missing '#pragma once' in LiveLinkFreeDSourceFactory.h"
#endif
#define LIVELINKFREED_LiveLinkFreeDSourceFactory_generated_h

#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULiveLinkFreeDSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkFreeDSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkFreeD"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkFreeDSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesULiveLinkFreeDSourceFactory(); \
	friend struct Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics; \
public: \
	DECLARE_CLASS(ULiveLinkFreeDSourceFactory, ULiveLinkSourceFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LiveLinkFreeD"), NO_API) \
	DECLARE_SERIALIZER(ULiveLinkFreeDSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkFreeDSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkFreeDSourceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkFreeDSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkFreeDSourceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkFreeDSourceFactory(ULiveLinkFreeDSourceFactory&&); \
	NO_API ULiveLinkFreeDSourceFactory(const ULiveLinkFreeDSourceFactory&); \
public:


#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULiveLinkFreeDSourceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULiveLinkFreeDSourceFactory(ULiveLinkFreeDSourceFactory&&); \
	NO_API ULiveLinkFreeDSourceFactory(const ULiveLinkFreeDSourceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULiveLinkFreeDSourceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULiveLinkFreeDSourceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULiveLinkFreeDSourceFactory)


#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_9_PROLOG
#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h_13_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LIVELINKFREED_API UClass* StaticClass<class ULiveLinkFreeDSourceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_LiveLinkFreeD_Source_LiveLinkFreeD_Public_LiveLinkFreeDSourceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
