// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LiveLinkFreeD/Public/LiveLinkFreeDSourceFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLiveLinkFreeDSourceFactory() {}
// Cross Module References
	LIVELINKFREED_API UClass* Z_Construct_UClass_ULiveLinkFreeDSourceFactory_NoRegister();
	LIVELINKFREED_API UClass* Z_Construct_UClass_ULiveLinkFreeDSourceFactory();
	LIVELINKINTERFACE_API UClass* Z_Construct_UClass_ULiveLinkSourceFactory();
	UPackage* Z_Construct_UPackage__Script_LiveLinkFreeD();
// End Cross Module References
	void ULiveLinkFreeDSourceFactory::StaticRegisterNativesULiveLinkFreeDSourceFactory()
	{
	}
	UClass* Z_Construct_UClass_ULiveLinkFreeDSourceFactory_NoRegister()
	{
		return ULiveLinkFreeDSourceFactory::StaticClass();
	}
	struct Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULiveLinkSourceFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LiveLinkFreeD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LiveLinkFreeDSourceFactory.h" },
		{ "ModuleRelativePath", "Public/LiveLinkFreeDSourceFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULiveLinkFreeDSourceFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::ClassParams = {
		&ULiveLinkFreeDSourceFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULiveLinkFreeDSourceFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULiveLinkFreeDSourceFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULiveLinkFreeDSourceFactory, 2019088089);
	template<> LIVELINKFREED_API UClass* StaticClass<ULiveLinkFreeDSourceFactory>()
	{
		return ULiveLinkFreeDSourceFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULiveLinkFreeDSourceFactory(Z_Construct_UClass_ULiveLinkFreeDSourceFactory, &ULiveLinkFreeDSourceFactory::StaticClass, TEXT("/Script/LiveLinkFreeD"), TEXT("ULiveLinkFreeDSourceFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULiveLinkFreeDSourceFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
