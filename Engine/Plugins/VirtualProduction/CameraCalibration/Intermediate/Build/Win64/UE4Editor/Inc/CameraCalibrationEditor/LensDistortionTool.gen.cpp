// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/AssetEditor/LensDistortionTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensDistortionTool() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_ULensDistortionTool_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_ULensDistortionTool();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationStep();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraLensDistortionAlgo_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void ULensDistortionTool::StaticRegisterNativesULensDistortionTool()
	{
	}
	UClass* Z_Construct_UClass_ULensDistortionTool_NoRegister()
	{
		return ULensDistortionTool::StaticClass();
	}
	struct Z_Construct_UClass_ULensDistortionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentAlgo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentAlgo;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_AlgosMap_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AlgosMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlgosMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AlgosMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensDistortionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraCalibrationStep,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ULensDistortionTool is the controller for the lens distortion panel.\n */" },
		{ "IncludePath", "AssetEditor/LensDistortionTool.h" },
		{ "ModuleRelativePath", "Private/AssetEditor/LensDistortionTool.h" },
		{ "ToolTip", "ULensDistortionTool is the controller for the lens distortion panel." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_CurrentAlgo_MetaData[] = {
		{ "Comment", "/** The currently selected algorithm */" },
		{ "ModuleRelativePath", "Private/AssetEditor/LensDistortionTool.h" },
		{ "ToolTip", "The currently selected algorithm" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_CurrentAlgo = { "CurrentAlgo", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionTool, CurrentAlgo), Z_Construct_UClass_UCameraLensDistortionAlgo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_CurrentAlgo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_CurrentAlgo_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_ValueProp = { "AlgosMap", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UCameraLensDistortionAlgo_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_Key_KeyProp = { "AlgosMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_MetaData[] = {
		{ "Comment", "/** Holds the registered camera nodal offset algos */" },
		{ "ModuleRelativePath", "Private/AssetEditor/LensDistortionTool.h" },
		{ "ToolTip", "Holds the registered camera nodal offset algos" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap = { "AlgosMap", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionTool, AlgosMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULensDistortionTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_CurrentAlgo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionTool_Statics::NewProp_AlgosMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensDistortionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensDistortionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensDistortionTool_Statics::ClassParams = {
		&ULensDistortionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULensDistortionTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionTool_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULensDistortionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensDistortionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensDistortionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensDistortionTool, 4157177364);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<ULensDistortionTool>()
	{
		return ULensDistortionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensDistortionTool(Z_Construct_UClass_ULensDistortionTool, &ULensDistortionTool::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("ULensDistortionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensDistortionTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
