// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATION_LensDistortionComponent_generated_h
#error "LensDistortionComponent.generated.h already included, missing '#pragma once' in LensDistortionComponent.h"
#endif
#define CAMERACALIBRATION_LensDistortionComponent_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULensDistortionComponent(); \
	friend struct Z_Construct_UClass_ULensDistortionComponent_Statics; \
public: \
	DECLARE_CLASS(ULensDistortionComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraCalibration"), NO_API) \
	DECLARE_SERIALIZER(ULensDistortionComponent)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesULensDistortionComponent(); \
	friend struct Z_Construct_UClass_ULensDistortionComponent_Statics; \
public: \
	DECLARE_CLASS(ULensDistortionComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CameraCalibration"), NO_API) \
	DECLARE_SERIALIZER(ULensDistortionComponent)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensDistortionComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensDistortionComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensDistortionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensDistortionComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensDistortionComponent(ULensDistortionComponent&&); \
	NO_API ULensDistortionComponent(const ULensDistortionComponent&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensDistortionComponent(ULensDistortionComponent&&); \
	NO_API ULensDistortionComponent(const ULensDistortionComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensDistortionComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensDistortionComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULensDistortionComponent)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TargetCameraComponent() { return STRUCT_OFFSET(ULensDistortionComponent, TargetCameraComponent); } \
	FORCEINLINE static uint32 __PPO__DistortionSource() { return STRUCT_OFFSET(ULensDistortionComponent, DistortionSource); } \
	FORCEINLINE static uint32 __PPO__bApplyDistortion() { return STRUCT_OFFSET(ULensDistortionComponent, bApplyDistortion); } \
	FORCEINLINE static uint32 __PPO__bIsDistortionSetup() { return STRUCT_OFFSET(ULensDistortionComponent, bIsDistortionSetup); } \
	FORCEINLINE static uint32 __PPO__OriginalFocalLength() { return STRUCT_OFFSET(ULensDistortionComponent, OriginalFocalLength); } \
	FORCEINLINE static uint32 __PPO__OriginalCameraRotation() { return STRUCT_OFFSET(ULensDistortionComponent, OriginalCameraRotation); } \
	FORCEINLINE static uint32 __PPO__OriginalCameraLocation() { return STRUCT_OFFSET(ULensDistortionComponent, OriginalCameraLocation); } \
	FORCEINLINE static uint32 __PPO__LastDistortionMID() { return STRUCT_OFFSET(ULensDistortionComponent, LastDistortionMID); } \
	FORCEINLINE static uint32 __PPO__LastCameraComponent() { return STRUCT_OFFSET(ULensDistortionComponent, LastCameraComponent); } \
	FORCEINLINE static uint32 __PPO__bEvaluateLensFileForDistortion() { return STRUCT_OFFSET(ULensDistortionComponent, bEvaluateLensFileForDistortion); } \
	FORCEINLINE static uint32 __PPO__bApplyNodalOffset() { return STRUCT_OFFSET(ULensDistortionComponent, bApplyNodalOffset); } \
	FORCEINLINE static uint32 __PPO__LensFilePicker() { return STRUCT_OFFSET(ULensDistortionComponent, LensFilePicker); } \
	FORCEINLINE static uint32 __PPO__bScaleOverscan() { return STRUCT_OFFSET(ULensDistortionComponent, bScaleOverscan); } \
	FORCEINLINE static uint32 __PPO__OverscanMultiplier() { return STRUCT_OFFSET(ULensDistortionComponent, OverscanMultiplier); } \
	FORCEINLINE static uint32 __PPO__ProducedLensDistortionHandler() { return STRUCT_OFFSET(ULensDistortionComponent, ProducedLensDistortionHandler); } \
	FORCEINLINE static uint32 __PPO__DistortionProducerID() { return STRUCT_OFFSET(ULensDistortionComponent, DistortionProducerID); }


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATION_API UClass* StaticClass<class ULensDistortionComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibration_Public_LensDistortionComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
