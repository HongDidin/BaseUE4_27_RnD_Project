// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/Calibrators/CameraNodalOffsetAlgoAruco.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraNodalOffsetAlgoAruco() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoAruco();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoPoints();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
// End Cross Module References
	void UCameraNodalOffsetAlgoAruco::StaticRegisterNativesUCameraNodalOffsetAlgoAruco()
	{
	}
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_NoRegister()
	{
		return UCameraNodalOffsetAlgoAruco::StaticClass();
	}
	struct Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraNodalOffsetAlgoPoints,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Implements a nodal offset calibration algorithm. It uses 3d points (UCalibrationPointComponent) \n * specified in a selectable calibrator that should be named after the correponding Aruco markers.\n */" },
		{ "IncludePath", "Calibrators/CameraNodalOffsetAlgoAruco.h" },
		{ "ModuleRelativePath", "Private/Calibrators/CameraNodalOffsetAlgoAruco.h" },
		{ "ToolTip", "Implements a nodal offset calibration algorithm. It uses 3d points (UCalibrationPointComponent)\nspecified in a selectable calibrator that should be named after the correponding Aruco markers." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraNodalOffsetAlgoAruco>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::ClassParams = {
		&UCameraNodalOffsetAlgoAruco::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoAruco()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraNodalOffsetAlgoAruco, 1475229380);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<UCameraNodalOffsetAlgoAruco>()
	{
		return UCameraNodalOffsetAlgoAruco::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraNodalOffsetAlgoAruco(Z_Construct_UClass_UCameraNodalOffsetAlgoAruco, &UCameraNodalOffsetAlgoAruco::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("UCameraNodalOffsetAlgoAruco"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraNodalOffsetAlgoAruco);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
