// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibration/Public/LensDistortionComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensDistortionComponent() {}
// Cross Module References
	CAMERACALIBRATION_API UClass* Z_Construct_UClass_ULensDistortionComponent_NoRegister();
	CAMERACALIBRATION_API UClass* Z_Construct_UClass_ULensDistortionComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_CameraCalibration();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FComponentReference();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionHandlerPicker();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent_NoRegister();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FLensFilePicker();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void ULensDistortionComponent::StaticRegisterNativesULensDistortionComponent()
	{
	}
	UClass* Z_Construct_UClass_ULensDistortionComponent_NoRegister()
	{
		return ULensDistortionComponent::StaticClass();
	}
	struct Z_Construct_UClass_ULensDistortionComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyDistortion_MetaData[];
#endif
		static void NewProp_bApplyDistortion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyDistortion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsDistortionSetup_MetaData[];
#endif
		static void NewProp_bIsDistortionSetup_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsDistortionSetup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalFocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OriginalFocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalCameraRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginalCameraRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalCameraLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginalCameraLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastDistortionMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastDistortionMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEvaluateLensFileForDistortion_MetaData[];
#endif
		static void NewProp_bEvaluateLensFileForDistortion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEvaluateLensFileForDistortion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyNodalOffset_MetaData[];
#endif
		static void NewProp_bApplyNodalOffset_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyNodalOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensFilePicker_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensFilePicker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bScaleOverscan_MetaData[];
#endif
		static void NewProp_bScaleOverscan_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bScaleOverscan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProducedLensDistortionHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProducedLensDistortionHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionProducerID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionProducerID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensDistortionComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "Comment", "/** Component for applying a post-process lens distortion effect to a CineCameraComponent on the same actor */" },
		{ "HideCategories", "Tags Activation Cooking AssetUserData Collision" },
		{ "IncludePath", "LensDistortionComponent.h" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Component for applying a post-process lens distortion effect to a CineCameraComponent on the same actor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_TargetCameraComponent_MetaData[] = {
		{ "AllowedClasses", "CineCameraComponent" },
		{ "Category", "Default" },
		{ "Comment", "/** The CineCameraComponent on which to apply the post-process distortion effect */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "The CineCameraComponent on which to apply the post-process distortion effect" },
		{ "UseComponentPicker", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_TargetCameraComponent = { "TargetCameraComponent", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, TargetCameraComponent), Z_Construct_UScriptStruct_FComponentReference, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_TargetCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_TargetCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionSource_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Structure used to query the camera calibration subsystem for a lens distortion model handler */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Structure used to query the camera calibration subsystem for a lens distortion model handler" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionSource = { "DistortionSource", nullptr, (EPropertyFlags)0x0020088000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, DistortionSource), Z_Construct_UScriptStruct_FDistortionHandlerPicker, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Whether or not to apply distortion to the target camera component */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Whether or not to apply distortion to the target camera component" },
	};
#endif
	void Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion_SetBit(void* Obj)
	{
		((ULensDistortionComponent*)Obj)->bApplyDistortion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion = { "bApplyDistortion", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULensDistortionComponent), &Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup_MetaData[] = {
		{ "Comment", "/** Whether a distortion effect is currently being applied to the target camera component */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Whether a distortion effect is currently being applied to the target camera component" },
	};
#endif
	void Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup_SetBit(void* Obj)
	{
		((ULensDistortionComponent*)Obj)->bIsDistortionSetup = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup = { "bIsDistortionSetup", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULensDistortionComponent), &Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalFocalLength_MetaData[] = {
		{ "Comment", "/** Focal length of the target camera before any overscan has been applied */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Focal length of the target camera before any overscan has been applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalFocalLength = { "OriginalFocalLength", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, OriginalFocalLength), METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalFocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraRotation_MetaData[] = {
		{ "Comment", "/** Original camera rotation to reset before/after applying nodal offset */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Original camera rotation to reset before/after applying nodal offset" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraRotation = { "OriginalCameraRotation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, OriginalCameraRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraLocation_MetaData[] = {
		{ "Comment", "/** Original camera location to reset before/after applying nodal offset */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Original camera location to reset before/after applying nodal offset" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraLocation = { "OriginalCameraLocation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, OriginalCameraLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastDistortionMID_MetaData[] = {
		{ "Comment", "/** Cached MID last applied to the target camera */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Cached MID last applied to the target camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastDistortionMID = { "LastDistortionMID", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, LastDistortionMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastDistortionMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastDistortionMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastCameraComponent_MetaData[] = {
		{ "Comment", "/** Cached most recent target camera, used to clean up the old camera when the user changes the target */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Cached most recent target camera, used to clean up the old camera when the user changes the target" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastCameraComponent = { "LastCameraComponent", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, LastCameraComponent), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** \n\x09 * Whether to use the specified lens file to drive distortion\n\x09 * Enabling this will create a new distortion source and automatically set this component to use it\n\x09 */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Whether to use the specified lens file to drive distortion\nEnabling this will create a new distortion source and automatically set this component to use it" },
	};
#endif
	void Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion_SetBit(void* Obj)
	{
		((ULensDistortionComponent*)Obj)->bEvaluateLensFileForDistortion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion = { "bEvaluateLensFileForDistortion", nullptr, (EPropertyFlags)0x00200c0000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULensDistortionComponent), &Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Whether to apply nodal offset to the target camera */" },
		{ "EditCondition", "bEvaluateLensFileForDistortion" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Whether to apply nodal offset to the target camera" },
	};
#endif
	void Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset_SetBit(void* Obj)
	{
		((ULensDistortionComponent*)Obj)->bApplyNodalOffset = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset = { "bApplyNodalOffset", nullptr, (EPropertyFlags)0x00200c0000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULensDistortionComponent), &Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LensFilePicker_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Lens File used to drive distortion with current camera settings */" },
		{ "EditCondition", "bEvaluateLensFileForDistortion" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Lens File used to drive distortion with current camera settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LensFilePicker = { "LensFilePicker", nullptr, (EPropertyFlags)0x00200c0000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, LensFilePicker), Z_Construct_UScriptStruct_FLensFilePicker, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LensFilePicker_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LensFilePicker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Whether to scale the computed overscan by the overscan percentage */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Whether to scale the computed overscan by the overscan percentage" },
	};
#endif
	void Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan_SetBit(void* Obj)
	{
		((ULensDistortionComponent*)Obj)->bScaleOverscan = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan = { "bScaleOverscan", nullptr, (EPropertyFlags)0x00200c0000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULensDistortionComponent), &Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OverscanMultiplier_MetaData[] = {
		{ "Category", "Default" },
		{ "ClampMax", "2.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** The percentage of the computed overscan that should be applied to the target camera */" },
		{ "EditCondition", "bScaleOverscan" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "The percentage of the computed overscan that should be applied to the target camera" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OverscanMultiplier = { "OverscanMultiplier", nullptr, (EPropertyFlags)0x00200c0000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, OverscanMultiplier), METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OverscanMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OverscanMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_ProducedLensDistortionHandler_MetaData[] = {
		{ "Comment", "/** Distortion handler produced by this component */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Distortion handler produced by this component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_ProducedLensDistortionHandler = { "ProducedLensDistortionHandler", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, ProducedLensDistortionHandler), Z_Construct_UClass_ULensDistortionModelHandlerBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_ProducedLensDistortionHandler_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_ProducedLensDistortionHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionProducerID_MetaData[] = {
		{ "Comment", "/** Unique identifier representing the source of distortion data */" },
		{ "ModuleRelativePath", "Public/LensDistortionComponent.h" },
		{ "ToolTip", "Unique identifier representing the source of distortion data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionProducerID = { "DistortionProducerID", nullptr, (EPropertyFlags)0x0020080000200000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULensDistortionComponent, DistortionProducerID), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionProducerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionProducerID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULensDistortionComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_TargetCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyDistortion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bIsDistortionSetup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalFocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OriginalCameraLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastDistortionMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LastCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bEvaluateLensFileForDistortion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bApplyNodalOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_LensFilePicker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_bScaleOverscan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_OverscanMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_ProducedLensDistortionHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULensDistortionComponent_Statics::NewProp_DistortionProducerID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensDistortionComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensDistortionComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensDistortionComponent_Statics::ClassParams = {
		&ULensDistortionComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULensDistortionComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULensDistortionComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensDistortionComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensDistortionComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensDistortionComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensDistortionComponent, 372313851);
	template<> CAMERACALIBRATION_API UClass* StaticClass<ULensDistortionComponent>()
	{
		return ULensDistortionComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensDistortionComponent(Z_Construct_UClass_ULensDistortionComponent, &ULensDistortionComponent::StaticClass, TEXT("/Script/CameraCalibration"), TEXT("ULensDistortionComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensDistortionComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
