// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONEDITOR_CameraNodalOffsetAlgoPoints_generated_h
#error "CameraNodalOffsetAlgoPoints.generated.h already included, missing '#pragma once' in CameraNodalOffsetAlgoPoints.h"
#endif
#define CAMERACALIBRATIONEDITOR_CameraNodalOffsetAlgoPoints_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraNodalOffsetAlgoPoints(); \
	friend struct Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics; \
public: \
	DECLARE_CLASS(UCameraNodalOffsetAlgoPoints, UCameraNodalOffsetAlgo, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(UCameraNodalOffsetAlgoPoints)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_INCLASS \
private: \
	static void StaticRegisterNativesUCameraNodalOffsetAlgoPoints(); \
	friend struct Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics; \
public: \
	DECLARE_CLASS(UCameraNodalOffsetAlgoPoints, UCameraNodalOffsetAlgo, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(UCameraNodalOffsetAlgoPoints)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraNodalOffsetAlgoPoints(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraNodalOffsetAlgoPoints) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraNodalOffsetAlgoPoints); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraNodalOffsetAlgoPoints); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraNodalOffsetAlgoPoints(UCameraNodalOffsetAlgoPoints&&); \
	NO_API UCameraNodalOffsetAlgoPoints(const UCameraNodalOffsetAlgoPoints&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraNodalOffsetAlgoPoints(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraNodalOffsetAlgoPoints(UCameraNodalOffsetAlgoPoints&&); \
	NO_API UCameraNodalOffsetAlgoPoints(const UCameraNodalOffsetAlgoPoints&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraNodalOffsetAlgoPoints); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraNodalOffsetAlgoPoints); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraNodalOffsetAlgoPoints)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_37_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h_40_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<class UCameraNodalOffsetAlgoPoints>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoPoints_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
