// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/Calibrators/CameraNodalOffsetAlgoCheckerboard.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraNodalOffsetAlgoCheckerboard() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoPoints();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
// End Cross Module References
	void UCameraNodalOffsetAlgoCheckerboard::StaticRegisterNativesUCameraNodalOffsetAlgoCheckerboard()
	{
	}
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_NoRegister()
	{
		return UCameraNodalOffsetAlgoCheckerboard::StaticClass();
	}
	struct Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraNodalOffsetAlgoPoints,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Implements a nodal offset calibration algorithm based on a checkerboard.\n * It requires the checkerboard to be upright in the image due to symmetry ambiguity.\n */" },
		{ "IncludePath", "Calibrators/CameraNodalOffsetAlgoCheckerboard.h" },
		{ "ModuleRelativePath", "Private/Calibrators/CameraNodalOffsetAlgoCheckerboard.h" },
		{ "ToolTip", "Implements a nodal offset calibration algorithm based on a checkerboard.\nIt requires the checkerboard to be upright in the image due to symmetry ambiguity." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraNodalOffsetAlgoCheckerboard>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::ClassParams = {
		&UCameraNodalOffsetAlgoCheckerboard::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraNodalOffsetAlgoCheckerboard, 4218811274);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<UCameraNodalOffsetAlgoCheckerboard>()
	{
		return UCameraNodalOffsetAlgoCheckerboard::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraNodalOffsetAlgoCheckerboard(Z_Construct_UClass_UCameraNodalOffsetAlgoCheckerboard, &UCameraNodalOffsetAlgoCheckerboard::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("UCameraNodalOffsetAlgoCheckerboard"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraNodalOffsetAlgoCheckerboard);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
