// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/Calibrators/CameraLensDistortionAlgoCheckerboard.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraLensDistortionAlgoCheckerboard() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraLensDistortionAlgo();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
// End Cross Module References
	void UCameraLensDistortionAlgoCheckerboard::StaticRegisterNativesUCameraLensDistortionAlgoCheckerboard()
	{
	}
	UClass* Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_NoRegister()
	{
		return UCameraLensDistortionAlgoCheckerboard::StaticClass();
	}
	struct Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraLensDistortionAlgo,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Implements a lens distortion calibration algorithm. It requires a checkerboard pattern\n */" },
		{ "IncludePath", "Calibrators/CameraLensDistortionAlgoCheckerboard.h" },
		{ "ModuleRelativePath", "Private/Calibrators/CameraLensDistortionAlgoCheckerboard.h" },
		{ "ToolTip", "Implements a lens distortion calibration algorithm. It requires a checkerboard pattern" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraLensDistortionAlgoCheckerboard>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::ClassParams = {
		&UCameraLensDistortionAlgoCheckerboard::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraLensDistortionAlgoCheckerboard, 2087622088);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<UCameraLensDistortionAlgoCheckerboard>()
	{
		return UCameraLensDistortionAlgoCheckerboard::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraLensDistortionAlgoCheckerboard(Z_Construct_UClass_UCameraLensDistortionAlgoCheckerboard, &UCameraLensDistortionAlgoCheckerboard::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("UCameraLensDistortionAlgoCheckerboard"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraLensDistortionAlgoCheckerboard);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
