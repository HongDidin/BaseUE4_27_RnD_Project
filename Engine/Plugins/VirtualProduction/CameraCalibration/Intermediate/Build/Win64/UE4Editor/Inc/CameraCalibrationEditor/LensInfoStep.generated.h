// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONEDITOR_LensInfoStep_generated_h
#error "LensInfoStep.generated.h already included, missing '#pragma once' in LensInfoStep.h"
#endif
#define CAMERACALIBRATIONEDITOR_LensInfoStep_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULensInfoStep(); \
	friend struct Z_Construct_UClass_ULensInfoStep_Statics; \
public: \
	DECLARE_CLASS(ULensInfoStep, UCameraCalibrationStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(ULensInfoStep)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_INCLASS \
private: \
	static void StaticRegisterNativesULensInfoStep(); \
	friend struct Z_Construct_UClass_ULensInfoStep_Statics; \
public: \
	DECLARE_CLASS(ULensInfoStep, UCameraCalibrationStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(ULensInfoStep)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensInfoStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensInfoStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensInfoStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensInfoStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensInfoStep(ULensInfoStep&&); \
	NO_API ULensInfoStep(const ULensInfoStep&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensInfoStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensInfoStep(ULensInfoStep&&); \
	NO_API ULensInfoStep(const ULensInfoStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensInfoStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensInfoStep); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensInfoStep)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<class ULensInfoStep>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensInfoStep_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
