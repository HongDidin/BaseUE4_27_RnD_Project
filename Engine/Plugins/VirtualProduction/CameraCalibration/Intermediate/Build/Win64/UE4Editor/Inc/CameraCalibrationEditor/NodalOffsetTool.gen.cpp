// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/AssetEditor/NodalOffsetTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNodalOffsetTool() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UNodalOffsetTool_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UNodalOffsetTool();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationStep();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgo_NoRegister();
// End Cross Module References
	void UNodalOffsetTool::StaticRegisterNativesUNodalOffsetTool()
	{
	}
	UClass* Z_Construct_UClass_UNodalOffsetTool_NoRegister()
	{
		return UNodalOffsetTool::StaticClass();
	}
	struct Z_Construct_UClass_UNodalOffsetTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodalOffsetAlgo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NodalOffsetAlgo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNodalOffsetTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraCalibrationStep,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNodalOffsetTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * FNodalOffsetTool is the controller for the nodal offset tool panel.\n * It has the logic to bridge user input like selection of nodal offset algorithm or CG camera\n * with the actions that follow. It houses convenience functions used to generate the data\n * of what is presented to the user, and holds pointers to the relevant objects and structures.\n */" },
		{ "IncludePath", "AssetEditor/NodalOffsetTool.h" },
		{ "ModuleRelativePath", "Private/AssetEditor/NodalOffsetTool.h" },
		{ "ToolTip", "FNodalOffsetTool is the controller for the nodal offset tool panel.\nIt has the logic to bridge user input like selection of nodal offset algorithm or CG camera\nwith the actions that follow. It houses convenience functions used to generate the data\nof what is presented to the user, and holds pointers to the relevant objects and structures." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNodalOffsetTool_Statics::NewProp_NodalOffsetAlgo_MetaData[] = {
		{ "Comment", "/** The currently selected nodal offset algorithm */" },
		{ "ModuleRelativePath", "Private/AssetEditor/NodalOffsetTool.h" },
		{ "ToolTip", "The currently selected nodal offset algorithm" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNodalOffsetTool_Statics::NewProp_NodalOffsetAlgo = { "NodalOffsetAlgo", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNodalOffsetTool, NodalOffsetAlgo), Z_Construct_UClass_UCameraNodalOffsetAlgo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNodalOffsetTool_Statics::NewProp_NodalOffsetAlgo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNodalOffsetTool_Statics::NewProp_NodalOffsetAlgo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNodalOffsetTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNodalOffsetTool_Statics::NewProp_NodalOffsetAlgo,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNodalOffsetTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNodalOffsetTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNodalOffsetTool_Statics::ClassParams = {
		&UNodalOffsetTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNodalOffsetTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNodalOffsetTool_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNodalOffsetTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNodalOffsetTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNodalOffsetTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNodalOffsetTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNodalOffsetTool, 1393230551);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<UNodalOffsetTool>()
	{
		return UNodalOffsetTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNodalOffsetTool(Z_Construct_UClass_UNodalOffsetTool, &UNodalOffsetTool::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("UNodalOffsetTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNodalOffsetTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
