// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/AssetEditor/CameraCalibrationEditorCommon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraCalibrationEditorCommon() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfoContainer();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfo();
// End Cross Module References
class UScriptStruct* FDistortionInfoContainer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CAMERACALIBRATIONEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FDistortionInfoContainer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDistortionInfoContainer, Z_Construct_UPackage__Script_CameraCalibrationEditor(), TEXT("DistortionInfoContainer"), sizeof(FDistortionInfoContainer), Get_Z_Construct_UScriptStruct_FDistortionInfoContainer_Hash());
	}
	return Singleton;
}
template<> CAMERACALIBRATIONEDITOR_API UScriptStruct* StaticStruct<FDistortionInfoContainer>()
{
	return FDistortionInfoContainer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDistortionInfoContainer(FDistortionInfoContainer::StaticStruct, TEXT("/Script/CameraCalibrationEditor"), TEXT("DistortionInfoContainer"), false, nullptr, nullptr);
static struct FScriptStruct_CameraCalibrationEditor_StaticRegisterNativesFDistortionInfoContainer
{
	FScriptStruct_CameraCalibrationEditor_StaticRegisterNativesFDistortionInfoContainer()
	{
		UScriptStruct::DeferCppStructOps<FDistortionInfoContainer>(FName(TEXT("DistortionInfoContainer")));
	}
} ScriptStruct_CameraCalibrationEditor_StaticRegisterNativesFDistortionInfoContainer;
	struct Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Container of distortion data to use instanced customization to show parameter struct defined by the model */" },
		{ "ModuleRelativePath", "Private/AssetEditor/CameraCalibrationEditorCommon.h" },
		{ "ToolTip", "Container of distortion data to use instanced customization to show parameter struct defined by the model" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDistortionInfoContainer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::NewProp_DistortionInfo_MetaData[] = {
		{ "Category", "Distortion" },
		{ "Comment", "/** Distortion parameters */" },
		{ "ModuleRelativePath", "Private/AssetEditor/CameraCalibrationEditorCommon.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Distortion parameters" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::NewProp_DistortionInfo = { "DistortionInfo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDistortionInfoContainer, DistortionInfo), Z_Construct_UScriptStruct_FDistortionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::NewProp_DistortionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::NewProp_DistortionInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::NewProp_DistortionInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
		nullptr,
		&NewStructOps,
		"DistortionInfoContainer",
		sizeof(FDistortionInfoContainer),
		alignof(FDistortionInfoContainer),
		Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDistortionInfoContainer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDistortionInfoContainer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CameraCalibrationEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DistortionInfoContainer"), sizeof(FDistortionInfoContainer), Get_Z_Construct_UScriptStruct_FDistortionInfoContainer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDistortionInfoContainer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDistortionInfoContainer_Hash() { return 4022676123U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
