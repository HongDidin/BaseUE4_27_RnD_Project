// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/AssetEditor/LensInfoStep.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensInfoStep() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_ULensInfoStep_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_ULensInfoStep();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraCalibrationStep();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
// End Cross Module References
	void ULensInfoStep::StaticRegisterNativesULensInfoStep()
	{
	}
	UClass* Z_Construct_UClass_ULensInfoStep_NoRegister()
	{
		return ULensInfoStep::StaticClass();
	}
	struct Z_Construct_UClass_ULensInfoStep_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensInfoStep_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraCalibrationStep,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensInfoStep_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * ULensInfoStep is used as the initial step to provide information about the lens you are going to calibrate\n */" },
		{ "IncludePath", "AssetEditor/LensInfoStep.h" },
		{ "ModuleRelativePath", "Private/AssetEditor/LensInfoStep.h" },
		{ "ToolTip", "ULensInfoStep is used as the initial step to provide information about the lens you are going to calibrate" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensInfoStep_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensInfoStep>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensInfoStep_Statics::ClassParams = {
		&ULensInfoStep::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULensInfoStep_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensInfoStep_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensInfoStep()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensInfoStep_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensInfoStep, 4258647719);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<ULensInfoStep>()
	{
		return ULensInfoStep::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensInfoStep(Z_Construct_UClass_ULensInfoStep, &ULensInfoStep::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("ULensInfoStep"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensInfoStep);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
