// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/Factories/LensFileFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLensFileFactoryNew() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_ULensFileFactoryNew_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_ULensFileFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
// End Cross Module References
	void ULensFileFactoryNew::StaticRegisterNativesULensFileFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_ULensFileFactoryNew_NoRegister()
	{
		return ULensFileFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_ULensFileFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULensFileFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULensFileFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for ULensFile objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/LensFileFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/LensFileFactoryNew.h" },
		{ "ToolTip", "Implements a factory for ULensFile objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULensFileFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULensFileFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULensFileFactoryNew_Statics::ClassParams = {
		&ULensFileFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULensFileFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULensFileFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULensFileFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULensFileFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULensFileFactoryNew, 2540522297);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<ULensFileFactoryNew>()
	{
		return ULensFileFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULensFileFactoryNew(Z_Construct_UClass_ULensFileFactoryNew, &ULensFileFactoryNew::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("ULensFileFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULensFileFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
