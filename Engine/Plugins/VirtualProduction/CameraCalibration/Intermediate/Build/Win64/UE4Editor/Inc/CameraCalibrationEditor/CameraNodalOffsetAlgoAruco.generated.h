// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONEDITOR_CameraNodalOffsetAlgoAruco_generated_h
#error "CameraNodalOffsetAlgoAruco.generated.h already included, missing '#pragma once' in CameraNodalOffsetAlgoAruco.h"
#endif
#define CAMERACALIBRATIONEDITOR_CameraNodalOffsetAlgoAruco_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraNodalOffsetAlgoAruco(); \
	friend struct Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics; \
public: \
	DECLARE_CLASS(UCameraNodalOffsetAlgoAruco, UCameraNodalOffsetAlgoPoints, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(UCameraNodalOffsetAlgoAruco)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUCameraNodalOffsetAlgoAruco(); \
	friend struct Z_Construct_UClass_UCameraNodalOffsetAlgoAruco_Statics; \
public: \
	DECLARE_CLASS(UCameraNodalOffsetAlgoAruco, UCameraNodalOffsetAlgoPoints, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(UCameraNodalOffsetAlgoAruco)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraNodalOffsetAlgoAruco(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraNodalOffsetAlgoAruco) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraNodalOffsetAlgoAruco); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraNodalOffsetAlgoAruco); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraNodalOffsetAlgoAruco(UCameraNodalOffsetAlgoAruco&&); \
	NO_API UCameraNodalOffsetAlgoAruco(const UCameraNodalOffsetAlgoAruco&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraNodalOffsetAlgoAruco(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraNodalOffsetAlgoAruco(UCameraNodalOffsetAlgoAruco&&); \
	NO_API UCameraNodalOffsetAlgoAruco(const UCameraNodalOffsetAlgoAruco&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraNodalOffsetAlgoAruco); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraNodalOffsetAlgoAruco); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraNodalOffsetAlgoAruco)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<class UCameraNodalOffsetAlgoAruco>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Calibrators_CameraNodalOffsetAlgoAruco_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
