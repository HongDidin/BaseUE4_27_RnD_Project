// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CameraCalibrationEditor/Private/Calibrators/CameraNodalOffsetAlgoPoints.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraNodalOffsetAlgoPoints() {}
// Cross Module References
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_NoRegister();
	CAMERACALIBRATIONEDITOR_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoPoints();
	CAMERACALIBRATIONCORE_API UClass* Z_Construct_UClass_UCameraNodalOffsetAlgo();
	UPackage* Z_Construct_UPackage__Script_CameraCalibrationEditor();
// End Cross Module References
	void UCameraNodalOffsetAlgoPoints::StaticRegisterNativesUCameraNodalOffsetAlgoPoints()
	{
	}
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_NoRegister()
	{
		return UCameraNodalOffsetAlgoPoints::StaticClass();
	}
	struct Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraNodalOffsetAlgo,
		(UObject* (*)())Z_Construct_UPackage__Script_CameraCalibrationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Implements a nodal offset calibration algorithm. It uses 3d points (UCalibrationPointComponent) \n * specified in a selectable calibrator with features that the user can identify by clicking on the \n * simulcam viewport, and after enough points have been identified, it can calculate the nodal offset\n * that needs to be applied to the associated camera in order to align its CG with the live action media plate.\n * \n * Limitations:\n * - Only supports Brown\xe2\x80\x93""Conrady model lens model (FSphericalDistortionParameters)\n * - The camera or the lens should not be moved during the calibration of each nodal offset sample.\n */" },
		{ "IncludePath", "Calibrators/CameraNodalOffsetAlgoPoints.h" },
		{ "ModuleRelativePath", "Private/Calibrators/CameraNodalOffsetAlgoPoints.h" },
		{ "ToolTip", "Implements a nodal offset calibration algorithm. It uses 3d points (UCalibrationPointComponent)\nspecified in a selectable calibrator with features that the user can identify by clicking on the\nsimulcam viewport, and after enough points have been identified, it can calculate the nodal offset\nthat needs to be applied to the associated camera in order to align its CG with the live action media plate.\n\nLimitations:\n- Only supports Brown\xe2\x80\x93""Conrady model lens model (FSphericalDistortionParameters)\n- The camera or the lens should not be moved during the calibration of each nodal offset sample." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraNodalOffsetAlgoPoints>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::ClassParams = {
		&UCameraNodalOffsetAlgoPoints::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraNodalOffsetAlgoPoints()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraNodalOffsetAlgoPoints_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraNodalOffsetAlgoPoints, 3466189633);
	template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<UCameraNodalOffsetAlgoPoints>()
	{
		return UCameraNodalOffsetAlgoPoints::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraNodalOffsetAlgoPoints(Z_Construct_UClass_UCameraNodalOffsetAlgoPoints, &UCameraNodalOffsetAlgoPoints::StaticClass, TEXT("/Script/CameraCalibrationEditor"), TEXT("UCameraNodalOffsetAlgoPoints"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraNodalOffsetAlgoPoints);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
