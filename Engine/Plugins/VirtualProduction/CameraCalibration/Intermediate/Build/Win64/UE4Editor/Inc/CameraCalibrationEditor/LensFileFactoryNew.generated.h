// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONEDITOR_LensFileFactoryNew_generated_h
#error "LensFileFactoryNew.generated.h already included, missing '#pragma once' in LensFileFactoryNew.h"
#endif
#define CAMERACALIBRATIONEDITOR_LensFileFactoryNew_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULensFileFactoryNew(); \
	friend struct Z_Construct_UClass_ULensFileFactoryNew_Statics; \
public: \
	DECLARE_CLASS(ULensFileFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(ULensFileFactoryNew)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_INCLASS \
private: \
	static void StaticRegisterNativesULensFileFactoryNew(); \
	friend struct Z_Construct_UClass_ULensFileFactoryNew_Statics; \
public: \
	DECLARE_CLASS(ULensFileFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(ULensFileFactoryNew)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensFileFactoryNew(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensFileFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensFileFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensFileFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensFileFactoryNew(ULensFileFactoryNew&&); \
	NO_API ULensFileFactoryNew(const ULensFileFactoryNew&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensFileFactoryNew(ULensFileFactoryNew&&); \
	NO_API ULensFileFactoryNew(const ULensFileFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensFileFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensFileFactoryNew); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULensFileFactoryNew)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<class ULensFileFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_Factories_LensFileFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
