// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONEDITOR_NodalOffsetTool_generated_h
#error "NodalOffsetTool.generated.h already included, missing '#pragma once' in NodalOffsetTool.h"
#endif
#define CAMERACALIBRATIONEDITOR_NodalOffsetTool_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNodalOffsetTool(); \
	friend struct Z_Construct_UClass_UNodalOffsetTool_Statics; \
public: \
	DECLARE_CLASS(UNodalOffsetTool, UCameraCalibrationStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(UNodalOffsetTool)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUNodalOffsetTool(); \
	friend struct Z_Construct_UClass_UNodalOffsetTool_Statics; \
public: \
	DECLARE_CLASS(UNodalOffsetTool, UCameraCalibrationStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(UNodalOffsetTool)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNodalOffsetTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNodalOffsetTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNodalOffsetTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNodalOffsetTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNodalOffsetTool(UNodalOffsetTool&&); \
	NO_API UNodalOffsetTool(const UNodalOffsetTool&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNodalOffsetTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNodalOffsetTool(UNodalOffsetTool&&); \
	NO_API UNodalOffsetTool(const UNodalOffsetTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNodalOffsetTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNodalOffsetTool); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNodalOffsetTool)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NodalOffsetAlgo() { return STRUCT_OFFSET(UNodalOffsetTool, NodalOffsetAlgo); }


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_24_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<class UNodalOffsetTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_NodalOffsetTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
