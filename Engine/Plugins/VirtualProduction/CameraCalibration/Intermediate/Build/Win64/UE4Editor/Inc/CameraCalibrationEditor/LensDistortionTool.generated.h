// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CAMERACALIBRATIONEDITOR_LensDistortionTool_generated_h
#error "LensDistortionTool.generated.h already included, missing '#pragma once' in LensDistortionTool.h"
#endif
#define CAMERACALIBRATIONEDITOR_LensDistortionTool_generated_h

#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULensDistortionTool(); \
	friend struct Z_Construct_UClass_ULensDistortionTool_Statics; \
public: \
	DECLARE_CLASS(ULensDistortionTool, UCameraCalibrationStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(ULensDistortionTool)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_INCLASS \
private: \
	static void StaticRegisterNativesULensDistortionTool(); \
	friend struct Z_Construct_UClass_ULensDistortionTool_Statics; \
public: \
	DECLARE_CLASS(ULensDistortionTool, UCameraCalibrationStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CameraCalibrationEditor"), NO_API) \
	DECLARE_SERIALIZER(ULensDistortionTool)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensDistortionTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensDistortionTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensDistortionTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensDistortionTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensDistortionTool(ULensDistortionTool&&); \
	NO_API ULensDistortionTool(const ULensDistortionTool&); \
public:


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULensDistortionTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULensDistortionTool(ULensDistortionTool&&); \
	NO_API ULensDistortionTool(const ULensDistortionTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULensDistortionTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULensDistortionTool); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULensDistortionTool)


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CurrentAlgo() { return STRUCT_OFFSET(ULensDistortionTool, CurrentAlgo); } \
	FORCEINLINE static uint32 __PPO__AlgosMap() { return STRUCT_OFFSET(ULensDistortionTool, AlgosMap); }


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_21_PROLOG
#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_INCLASS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CAMERACALIBRATIONEDITOR_API UClass* StaticClass<class ULensDistortionTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_CameraCalibration_Source_CameraCalibrationEditor_Private_AssetEditor_LensDistortionTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
