// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimedDataMonitor/Public/TimedDataMonitorTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimedDataMonitorTypes() {}
// Cross Module References
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier();
	UPackage* Z_Construct_UPackage__Script_TimedDataMonitor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier();
// End Cross Module References
class UScriptStruct* FTimedDataMonitorChannelIdentifier::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMEDDATAMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("TimedDataMonitorChannelIdentifier"), sizeof(FTimedDataMonitorChannelIdentifier), Get_Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Hash());
	}
	return Singleton;
}
template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<FTimedDataMonitorChannelIdentifier>()
{
	return FTimedDataMonitorChannelIdentifier::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimedDataMonitorChannelIdentifier(FTimedDataMonitorChannelIdentifier::StaticStruct, TEXT("/Script/TimedDataMonitor"), TEXT("TimedDataMonitorChannelIdentifier"), false, nullptr, nullptr);
static struct FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorChannelIdentifier
{
	FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorChannelIdentifier()
	{
		UScriptStruct::DeferCppStructOps<FTimedDataMonitorChannelIdentifier>(FName(TEXT("TimedDataMonitorChannelIdentifier")));
	}
} ScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorChannelIdentifier;
	struct Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Identifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Identifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimedDataMonitorChannelIdentifier>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::NewProp_Identifier_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimedDataMonitorTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::NewProp_Identifier = { "Identifier", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorChannelIdentifier, Identifier), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::NewProp_Identifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::NewProp_Identifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::NewProp_Identifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitor,
		nullptr,
		&NewStructOps,
		"TimedDataMonitorChannelIdentifier",
		sizeof(FTimedDataMonitorChannelIdentifier),
		alignof(FTimedDataMonitorChannelIdentifier),
		Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimedDataMonitorChannelIdentifier"), sizeof(FTimedDataMonitorChannelIdentifier), Get_Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Hash() { return 604442720U; }
class UScriptStruct* FTimedDataMonitorInputIdentifier::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMEDDATAMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("TimedDataMonitorInputIdentifier"), sizeof(FTimedDataMonitorInputIdentifier), Get_Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Hash());
	}
	return Singleton;
}
template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<FTimedDataMonitorInputIdentifier>()
{
	return FTimedDataMonitorInputIdentifier::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimedDataMonitorInputIdentifier(FTimedDataMonitorInputIdentifier::StaticStruct, TEXT("/Script/TimedDataMonitor"), TEXT("TimedDataMonitorInputIdentifier"), false, nullptr, nullptr);
static struct FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorInputIdentifier
{
	FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorInputIdentifier()
	{
		UScriptStruct::DeferCppStructOps<FTimedDataMonitorInputIdentifier>(FName(TEXT("TimedDataMonitorInputIdentifier")));
	}
} ScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorInputIdentifier;
	struct Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Identifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Identifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimedDataMonitorInputIdentifier>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::NewProp_Identifier_MetaData[] = {
		{ "ModuleRelativePath", "Public/TimedDataMonitorTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::NewProp_Identifier = { "Identifier", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorInputIdentifier, Identifier), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::NewProp_Identifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::NewProp_Identifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::NewProp_Identifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitor,
		nullptr,
		&NewStructOps,
		"TimedDataMonitorInputIdentifier",
		sizeof(FTimedDataMonitorInputIdentifier),
		alignof(FTimedDataMonitorInputIdentifier),
		Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimedDataMonitorInputIdentifier"), sizeof(FTimedDataMonitorInputIdentifier), Get_Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Hash() { return 968047895U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
