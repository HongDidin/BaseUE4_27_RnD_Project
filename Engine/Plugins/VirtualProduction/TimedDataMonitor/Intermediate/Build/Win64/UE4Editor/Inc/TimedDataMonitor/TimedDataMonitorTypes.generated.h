// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMEDDATAMONITOR_TimedDataMonitorTypes_generated_h
#error "TimedDataMonitorTypes.generated.h already included, missing '#pragma once' in TimedDataMonitorTypes.h"
#endif
#define TIMEDDATAMONITOR_TimedDataMonitorTypes_generated_h

#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitor_Public_TimedDataMonitorTypes_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Identifier() { return STRUCT_OFFSET(FTimedDataMonitorChannelIdentifier, Identifier); }


template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<struct FTimedDataMonitorChannelIdentifier>();

#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitor_Public_TimedDataMonitorTypes_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Identifier() { return STRUCT_OFFSET(FTimedDataMonitorInputIdentifier, Identifier); }


template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<struct FTimedDataMonitorInputIdentifier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitor_Public_TimedDataMonitorTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
