// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimedDataMonitor/Public/TimedDataMonitorCalibration.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimedDataMonitorCalibration() {}
// Cross Module References
	TIMEDDATAMONITOR_API UEnum* Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorTimeCorrectionReturnCode();
	UPackage* Z_Construct_UPackage__Script_TimedDataMonitor();
	TIMEDDATAMONITOR_API UEnum* Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorCalibrationReturnCode();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier();
// End Cross Module References
	static UEnum* ETimedDataMonitorTimeCorrectionReturnCode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorTimeCorrectionReturnCode, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("ETimedDataMonitorTimeCorrectionReturnCode"));
		}
		return Singleton;
	}
	template<> TIMEDDATAMONITOR_API UEnum* StaticEnum<ETimedDataMonitorTimeCorrectionReturnCode>()
	{
		return ETimedDataMonitorTimeCorrectionReturnCode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimedDataMonitorTimeCorrectionReturnCode(ETimedDataMonitorTimeCorrectionReturnCode_StaticEnum, TEXT("/Script/TimedDataMonitor"), TEXT("ETimedDataMonitorTimeCorrectionReturnCode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorTimeCorrectionReturnCode_Hash() { return 81445950U; }
	UEnum* Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorTimeCorrectionReturnCode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimedDataMonitorTimeCorrectionReturnCode"), 0, Get_Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorTimeCorrectionReturnCode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Succeeded", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Succeeded },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Failed_InvalidInput", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Failed_InvalidInput },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Failed_NoTimecode", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Failed_NoTimecode },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Failed_UnresponsiveInput", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Failed_UnresponsiveInput },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Failed_NoDataBuffered", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Failed_NoDataBuffered },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Failed_BufferCouldNotBeResize", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Failed_BufferCouldNotBeResize },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Retry_NotEnoughData", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Retry_NotEnoughData },
				{ "ETimedDataMonitorTimeCorrectionReturnCode::Retry_IncreaseBufferSize", (int64)ETimedDataMonitorTimeCorrectionReturnCode::Retry_IncreaseBufferSize },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Failed_BufferCouldNotBeResize.Comment", "/** Failed. A resize was requested but it was not able to do so. */" },
				{ "Failed_BufferCouldNotBeResize.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Failed_BufferCouldNotBeResize" },
				{ "Failed_BufferCouldNotBeResize.ToolTip", "Failed. A resize was requested but it was not able to do so." },
				{ "Failed_InvalidInput.Comment", "/** Failed. The provided input doesn't exist. */" },
				{ "Failed_InvalidInput.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Failed_InvalidInput" },
				{ "Failed_InvalidInput.ToolTip", "Failed. The provided input doesn't exist." },
				{ "Failed_NoDataBuffered.Comment", "/** Failed. The channel doesn't have any data in it's buffer to synchronized with. */" },
				{ "Failed_NoDataBuffered.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Failed_NoDataBuffered" },
				{ "Failed_NoDataBuffered.ToolTip", "Failed. The channel doesn't have any data in it's buffer to synchronized with." },
				{ "Failed_NoTimecode.Comment", "/** Failed. The timecode provider was not existing or not synchronized. */" },
				{ "Failed_NoTimecode.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Failed_NoTimecode" },
				{ "Failed_NoTimecode.ToolTip", "Failed. The timecode provider was not existing or not synchronized." },
				{ "Failed_UnresponsiveInput.Comment", "/** Failed. At least one channel is unresponsive. */" },
				{ "Failed_UnresponsiveInput.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Failed_UnresponsiveInput" },
				{ "Failed_UnresponsiveInput.ToolTip", "Failed. At least one channel is unresponsive." },
				{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
				{ "Retry_IncreaseBufferSize.Comment", "/** Retry. No interval could be found. Increase the buffer size. */" },
				{ "Retry_IncreaseBufferSize.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Retry_IncreaseBufferSize" },
				{ "Retry_IncreaseBufferSize.ToolTip", "Retry. No interval could be found. Increase the buffer size." },
				{ "Retry_NotEnoughData.Comment", "/** Retry. The buffer size is correct but they do not contain enough data to to the time correction. */" },
				{ "Retry_NotEnoughData.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Retry_NotEnoughData" },
				{ "Retry_NotEnoughData.ToolTip", "Retry. The buffer size is correct but they do not contain enough data to to the time correction." },
				{ "Succeeded.Comment", "/** Success. The values were synchronized. */" },
				{ "Succeeded.Name", "ETimedDataMonitorTimeCorrectionReturnCode::Succeeded" },
				{ "Succeeded.ToolTip", "Success. The values were synchronized." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimedDataMonitor,
				nullptr,
				"ETimedDataMonitorTimeCorrectionReturnCode",
				"ETimedDataMonitorTimeCorrectionReturnCode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETimedDataMonitorCalibrationReturnCode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorCalibrationReturnCode, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("ETimedDataMonitorCalibrationReturnCode"));
		}
		return Singleton;
	}
	template<> TIMEDDATAMONITOR_API UEnum* StaticEnum<ETimedDataMonitorCalibrationReturnCode>()
	{
		return ETimedDataMonitorCalibrationReturnCode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimedDataMonitorCalibrationReturnCode(ETimedDataMonitorCalibrationReturnCode_StaticEnum, TEXT("/Script/TimedDataMonitor"), TEXT("ETimedDataMonitorCalibrationReturnCode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorCalibrationReturnCode_Hash() { return 1509408184U; }
	UEnum* Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorCalibrationReturnCode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimedDataMonitorCalibrationReturnCode"), 0, Get_Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorCalibrationReturnCode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimedDataMonitorCalibrationReturnCode::Succeeded", (int64)ETimedDataMonitorCalibrationReturnCode::Succeeded },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_NoTimecode", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_NoTimecode },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_UnresponsiveInput", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_UnresponsiveInput },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_InvalidEvaluationType", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_InvalidEvaluationType },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_InvalidFrameRate", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_InvalidFrameRate },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_NoDataBuffered", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_NoDataBuffered },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_BufferCouldNotBeResize", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_BufferCouldNotBeResize },
				{ "ETimedDataMonitorCalibrationReturnCode::Failed_Reset", (int64)ETimedDataMonitorCalibrationReturnCode::Failed_Reset },
				{ "ETimedDataMonitorCalibrationReturnCode::Retry_NotEnoughData", (int64)ETimedDataMonitorCalibrationReturnCode::Retry_NotEnoughData },
				{ "ETimedDataMonitorCalibrationReturnCode::Retry_IncreaseBufferSize", (int64)ETimedDataMonitorCalibrationReturnCode::Retry_IncreaseBufferSize },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Failed_BufferCouldNotBeResize.Comment", "/** Failed. A resize was requested but it was not able to do so. */" },
				{ "Failed_BufferCouldNotBeResize.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_BufferCouldNotBeResize" },
				{ "Failed_BufferCouldNotBeResize.ToolTip", "Failed. A resize was requested but it was not able to do so." },
				{ "Failed_InvalidEvaluationType.Comment", "/** Failed. At least one input has an evaluation type that is not timecode. */" },
				{ "Failed_InvalidEvaluationType.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_InvalidEvaluationType" },
				{ "Failed_InvalidEvaluationType.ToolTip", "Failed. At least one input has an evaluation type that is not timecode." },
				{ "Failed_InvalidFrameRate.Comment", "/** Failed. At least one input doesn't have a defined frame rate. */" },
				{ "Failed_InvalidFrameRate.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_InvalidFrameRate" },
				{ "Failed_InvalidFrameRate.ToolTip", "Failed. At least one input doesn't have a defined frame rate." },
				{ "Failed_NoDataBuffered.Comment", "/** Failed. At least one input doesn't have data buffered. */" },
				{ "Failed_NoDataBuffered.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_NoDataBuffered" },
				{ "Failed_NoDataBuffered.ToolTip", "Failed. At least one input doesn't have data buffered." },
				{ "Failed_NoTimecode.Comment", "/** Failed. The timecode provider doesn't have a proper timecode value. */" },
				{ "Failed_NoTimecode.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_NoTimecode" },
				{ "Failed_NoTimecode.ToolTip", "Failed. The timecode provider doesn't have a proper timecode value." },
				{ "Failed_Reset.Comment", "/** Failed. The calibration was manually reset. */" },
				{ "Failed_Reset.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_Reset" },
				{ "Failed_Reset.ToolTip", "Failed. The calibration was manually reset." },
				{ "Failed_UnresponsiveInput.Comment", "/** Failed. At least one input is unresponsive. */" },
				{ "Failed_UnresponsiveInput.Name", "ETimedDataMonitorCalibrationReturnCode::Failed_UnresponsiveInput" },
				{ "Failed_UnresponsiveInput.ToolTip", "Failed. At least one input is unresponsive." },
				{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
				{ "Retry_IncreaseBufferSize.Comment", "/** Retry. No interval could be found. Increase the buffer size. */" },
				{ "Retry_IncreaseBufferSize.Name", "ETimedDataMonitorCalibrationReturnCode::Retry_IncreaseBufferSize" },
				{ "Retry_IncreaseBufferSize.ToolTip", "Retry. No interval could be found. Increase the buffer size." },
				{ "Retry_NotEnoughData.Comment", "/** Retry. The buffer size is correct but they do not contain enough data to calibrate. */" },
				{ "Retry_NotEnoughData.Name", "ETimedDataMonitorCalibrationReturnCode::Retry_NotEnoughData" },
				{ "Retry_NotEnoughData.ToolTip", "Retry. The buffer size is correct but they do not contain enough data to calibrate." },
				{ "Succeeded.Comment", "/** Success. The values were synchronized. */" },
				{ "Succeeded.Name", "ETimedDataMonitorCalibrationReturnCode::Succeeded" },
				{ "Succeeded.ToolTip", "Success. The values were synchronized." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimedDataMonitor,
				nullptr,
				"ETimedDataMonitorCalibrationReturnCode",
				"ETimedDataMonitorCalibrationReturnCode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FTimedDataMonitorTimeCorrectionParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMEDDATAMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("TimedDataMonitorTimeCorrectionParameters"), sizeof(FTimedDataMonitorTimeCorrectionParameters), Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Hash());
	}
	return Singleton;
}
template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<FTimedDataMonitorTimeCorrectionParameters>()
{
	return FTimedDataMonitorTimeCorrectionParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters(FTimedDataMonitorTimeCorrectionParameters::StaticStruct, TEXT("/Script/TimedDataMonitor"), TEXT("TimedDataMonitorTimeCorrectionParameters"), false, nullptr, nullptr);
static struct FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorTimeCorrectionParameters
{
	FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorTimeCorrectionParameters()
	{
		UScriptStruct::DeferCppStructOps<FTimedDataMonitorTimeCorrectionParameters>(FName(TEXT("TimedDataMonitorTimeCorrectionParameters")));
	}
} ScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorTimeCorrectionParameters;
	struct Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBufferResizeAllowed_MetaData[];
#endif
		static void NewProp_bBufferResizeAllowed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBufferResizeAllowed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBufferShrinkAllowed_MetaData[];
#endif
		static void NewProp_bBufferShrinkAllowed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBufferShrinkAllowed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFailedIfBufferCantBeResize_MetaData[];
#endif
		static void NewProp_bFailedIfBufferCantBeResize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFailedIfBufferCantBeResize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseStandardDeviation_MetaData[];
#endif
		static void NewProp_bUseStandardDeviation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseStandardDeviation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfStandardDeviation_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfStandardDeviation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimedDataMonitorTimeCorrectionParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** If no calibration is possible, are we allowed to increase the size of the buffer. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "If no calibration is possible, are we allowed to increase the size of the buffer." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed_SetBit(void* Obj)
	{
		((FTimedDataMonitorTimeCorrectionParameters*)Obj)->bBufferResizeAllowed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed = { "bBufferResizeAllowed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorTimeCorrectionParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** When resizing buffer, do we allow shrinking them. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When resizing buffer, do we allow shrinking them." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed_SetBit(void* Obj)
	{
		((FTimedDataMonitorTimeCorrectionParameters*)Obj)->bBufferShrinkAllowed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed = { "bBufferShrinkAllowed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorTimeCorrectionParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** When resizing buffer, failed the calibration if a buffer couldn't be resize. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When resizing buffer, failed the calibration if a buffer couldn't be resize." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize_SetBit(void* Obj)
	{
		((FTimedDataMonitorTimeCorrectionParameters*)Obj)->bFailedIfBufferCantBeResize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize = { "bFailedIfBufferCantBeResize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorTimeCorrectionParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** When calibrating, ensure that the evaluation is included inside the STD. */" },
		{ "InlineEditConditionToggle", "TRUE" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When calibrating, ensure that the evaluation is included inside the STD." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation_SetBit(void* Obj)
	{
		((FTimedDataMonitorTimeCorrectionParameters*)Obj)->bUseStandardDeviation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation = { "bUseStandardDeviation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorTimeCorrectionParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_NumberOfStandardDeviation_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "ClampMax", "5" },
		{ "ClampMin", "0" },
		{ "Comment", "/** When using STD, how many should we use. */" },
		{ "EditCondition", "bUseStandardDeviation" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When using STD, how many should we use." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_NumberOfStandardDeviation = { "NumberOfStandardDeviation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorTimeCorrectionParameters, NumberOfStandardDeviation), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_NumberOfStandardDeviation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_NumberOfStandardDeviation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferResizeAllowed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bBufferShrinkAllowed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bFailedIfBufferCantBeResize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_bUseStandardDeviation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::NewProp_NumberOfStandardDeviation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitor,
		nullptr,
		&NewStructOps,
		"TimedDataMonitorTimeCorrectionParameters",
		sizeof(FTimedDataMonitorTimeCorrectionParameters),
		alignof(FTimedDataMonitorTimeCorrectionParameters),
		Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimedDataMonitorTimeCorrectionParameters"), sizeof(FTimedDataMonitorTimeCorrectionParameters), Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters_Hash() { return 1988240081U; }
class UScriptStruct* FTimedDataMonitorTimeCorrectionResult::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMEDDATAMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("TimedDataMonitorTimeCorrectionResult"), sizeof(FTimedDataMonitorTimeCorrectionResult), Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Hash());
	}
	return Singleton;
}
template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<FTimedDataMonitorTimeCorrectionResult>()
{
	return FTimedDataMonitorTimeCorrectionResult::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult(FTimedDataMonitorTimeCorrectionResult::StaticStruct, TEXT("/Script/TimedDataMonitor"), TEXT("TimedDataMonitorTimeCorrectionResult"), false, nullptr, nullptr);
static struct FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorTimeCorrectionResult
{
	FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorTimeCorrectionResult()
	{
		UScriptStruct::DeferCppStructOps<FTimedDataMonitorTimeCorrectionResult>(FName(TEXT("TimedDataMonitorTimeCorrectionResult")));
	}
} ScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorTimeCorrectionResult;
	struct Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnCode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnCode;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FailureChannelIdentifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FailureChannelIdentifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FailureChannelIdentifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimedDataMonitorTimeCorrectionResult>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode_MetaData[] = {
		{ "Category", "Result" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode = { "ReturnCode", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorTimeCorrectionResult, ReturnCode), Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorTimeCorrectionReturnCode, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers_Inner = { "FailureChannelIdentifiers", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTimedDataMonitorChannelIdentifier, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers_MetaData[] = {
		{ "Category", "Result" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers = { "FailureChannelIdentifiers", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorTimeCorrectionResult, FailureChannelIdentifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_ReturnCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::NewProp_FailureChannelIdentifiers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitor,
		nullptr,
		&NewStructOps,
		"TimedDataMonitorTimeCorrectionResult",
		sizeof(FTimedDataMonitorTimeCorrectionResult),
		alignof(FTimedDataMonitorTimeCorrectionResult),
		Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimedDataMonitorTimeCorrectionResult"), sizeof(FTimedDataMonitorTimeCorrectionResult), Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionResult_Hash() { return 840312723U; }
class UScriptStruct* FTimedDataMonitorCalibrationParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMEDDATAMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("TimedDataMonitorCalibrationParameters"), sizeof(FTimedDataMonitorCalibrationParameters), Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Hash());
	}
	return Singleton;
}
template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<FTimedDataMonitorCalibrationParameters>()
{
	return FTimedDataMonitorCalibrationParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimedDataMonitorCalibrationParameters(FTimedDataMonitorCalibrationParameters::StaticStruct, TEXT("/Script/TimedDataMonitor"), TEXT("TimedDataMonitorCalibrationParameters"), false, nullptr, nullptr);
static struct FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorCalibrationParameters
{
	FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorCalibrationParameters()
	{
		UScriptStruct::DeferCppStructOps<FTimedDataMonitorCalibrationParameters>(FName(TEXT("TimedDataMonitorCalibrationParameters")));
	}
} ScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorCalibrationParameters;
	struct Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfRetries_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfRetries;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBufferResizeAllowed_MetaData[];
#endif
		static void NewProp_bBufferResizeAllowed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBufferResizeAllowed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBufferShrinkAllowed_MetaData[];
#endif
		static void NewProp_bBufferShrinkAllowed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBufferShrinkAllowed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFailedIfBufferCantBeResize_MetaData[];
#endif
		static void NewProp_bFailedIfBufferCantBeResize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFailedIfBufferCantBeResize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseStandardDeviation_MetaData[];
#endif
		static void NewProp_bUseStandardDeviation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseStandardDeviation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfStandardDeviation_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfStandardDeviation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetStatisticsBeforeUsingStandardDeviation_MetaData[];
#endif
		static void NewProp_bResetStatisticsBeforeUsingStandardDeviation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetStatisticsBeforeUsingStandardDeviation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmountOfSecondsToWaitAfterStatisticReset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AmountOfSecondsToWaitAfterStatisticReset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimedDataMonitorCalibrationParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfRetries_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "ClampMax", "100" },
		{ "ClampMin", "0" },
		{ "Comment", "/** When needed, how many retry is allowed. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When needed, how many retry is allowed." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfRetries = { "NumberOfRetries", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorCalibrationParameters, NumberOfRetries), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfRetries_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfRetries_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** If no calibration is possible, are we allowed to increase the size of the buffer. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "If no calibration is possible, are we allowed to increase the size of the buffer." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed_SetBit(void* Obj)
	{
		((FTimedDataMonitorCalibrationParameters*)Obj)->bBufferResizeAllowed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed = { "bBufferResizeAllowed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorCalibrationParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** When resizing buffer, do we allow shrinking them. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When resizing buffer, do we allow shrinking them." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed_SetBit(void* Obj)
	{
		((FTimedDataMonitorCalibrationParameters*)Obj)->bBufferShrinkAllowed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed = { "bBufferShrinkAllowed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorCalibrationParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** When resizing buffer, failed the calibration if a buffer couldn't be resize. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When resizing buffer, failed the calibration if a buffer couldn't be resize." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize_SetBit(void* Obj)
	{
		((FTimedDataMonitorCalibrationParameters*)Obj)->bFailedIfBufferCantBeResize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize = { "bFailedIfBufferCantBeResize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorCalibrationParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** When calibrating, ensure that the evaluation is included inside the STD. */" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When calibrating, ensure that the evaluation is included inside the STD." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation_SetBit(void* Obj)
	{
		((FTimedDataMonitorCalibrationParameters*)Obj)->bUseStandardDeviation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation = { "bUseStandardDeviation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorCalibrationParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfStandardDeviation_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "ClampMax", "5" },
		{ "ClampMin", "0" },
		{ "Comment", "/** When using STD, how many should we use. */" },
		{ "EditCondition", "bUseStandardDeviation" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "When using STD, how many should we use." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfStandardDeviation = { "NumberOfStandardDeviation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorCalibrationParameters, NumberOfStandardDeviation), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfStandardDeviation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfStandardDeviation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** Before calibration, allow to reset the statistics. */" },
		{ "EditCondition", "bUseStandardDeviation" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "Before calibration, allow to reset the statistics." },
	};
#endif
	void Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation_SetBit(void* Obj)
	{
		((FTimedDataMonitorCalibrationParameters*)Obj)->bResetStatisticsBeforeUsingStandardDeviation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation = { "bResetStatisticsBeforeUsingStandardDeviation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FTimedDataMonitorCalibrationParameters), &Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_AmountOfSecondsToWaitAfterStatisticReset_MetaData[] = {
		{ "Category", "Calibration Parameters" },
		{ "Comment", "/** Before calibration, allow to reset the statistics. */" },
		{ "EditCondition", "bUseStandardDeviation" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
		{ "ToolTip", "Before calibration, allow to reset the statistics." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_AmountOfSecondsToWaitAfterStatisticReset = { "AmountOfSecondsToWaitAfterStatisticReset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorCalibrationParameters, AmountOfSecondsToWaitAfterStatisticReset), METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_AmountOfSecondsToWaitAfterStatisticReset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_AmountOfSecondsToWaitAfterStatisticReset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfRetries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferResizeAllowed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bBufferShrinkAllowed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bFailedIfBufferCantBeResize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bUseStandardDeviation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_NumberOfStandardDeviation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_bResetStatisticsBeforeUsingStandardDeviation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::NewProp_AmountOfSecondsToWaitAfterStatisticReset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitor,
		nullptr,
		&NewStructOps,
		"TimedDataMonitorCalibrationParameters",
		sizeof(FTimedDataMonitorCalibrationParameters),
		alignof(FTimedDataMonitorCalibrationParameters),
		Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimedDataMonitorCalibrationParameters"), sizeof(FTimedDataMonitorCalibrationParameters), Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters_Hash() { return 2512657007U; }
class UScriptStruct* FTimedDataMonitorCalibrationResult::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TIMEDDATAMONITOR_API uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult, Z_Construct_UPackage__Script_TimedDataMonitor(), TEXT("TimedDataMonitorCalibrationResult"), sizeof(FTimedDataMonitorCalibrationResult), Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Hash());
	}
	return Singleton;
}
template<> TIMEDDATAMONITOR_API UScriptStruct* StaticStruct<FTimedDataMonitorCalibrationResult>()
{
	return FTimedDataMonitorCalibrationResult::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTimedDataMonitorCalibrationResult(FTimedDataMonitorCalibrationResult::StaticStruct, TEXT("/Script/TimedDataMonitor"), TEXT("TimedDataMonitorCalibrationResult"), false, nullptr, nullptr);
static struct FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorCalibrationResult
{
	FScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorCalibrationResult()
	{
		UScriptStruct::DeferCppStructOps<FTimedDataMonitorCalibrationResult>(FName(TEXT("TimedDataMonitorCalibrationResult")));
	}
} ScriptStruct_TimedDataMonitor_StaticRegisterNativesFTimedDataMonitorCalibrationResult;
	struct Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnCode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnCode;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FailureInputIdentifiers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FailureInputIdentifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FailureInputIdentifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTimedDataMonitorCalibrationResult>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode_MetaData[] = {
		{ "Category", "Result" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode = { "ReturnCode", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorCalibrationResult, ReturnCode), Z_Construct_UEnum_TimedDataMonitor_ETimedDataMonitorCalibrationReturnCode, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers_Inner = { "FailureInputIdentifiers", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTimedDataMonitorInputIdentifier, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers_MetaData[] = {
		{ "Category", "Result" },
		{ "ModuleRelativePath", "Public/TimedDataMonitorCalibration.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers = { "FailureInputIdentifiers", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTimedDataMonitorCalibrationResult, FailureInputIdentifiers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_ReturnCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::NewProp_FailureInputIdentifiers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitor,
		nullptr,
		&NewStructOps,
		"TimedDataMonitorCalibrationResult",
		sizeof(FTimedDataMonitorCalibrationResult),
		alignof(FTimedDataMonitorCalibrationResult),
		Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TimedDataMonitorCalibrationResult"), sizeof(FTimedDataMonitorCalibrationResult), Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationResult_Hash() { return 2413024060U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
