// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMEDDATAMONITOREDITOR_TimedDataMonitorEditorSettings_generated_h
#error "TimedDataMonitorEditorSettings.generated.h already included, missing '#pragma once' in TimedDataMonitorEditorSettings.h"
#endif
#define TIMEDDATAMONITOREDITOR_TimedDataMonitorEditorSettings_generated_h

#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTimedDataMonitorEditorSettings(); \
	friend struct Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTimedDataMonitorEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimedDataMonitorEditor"), TIMEDDATAMONITOREDITOR_API) \
	DECLARE_SERIALIZER(UTimedDataMonitorEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUTimedDataMonitorEditorSettings(); \
	friend struct Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTimedDataMonitorEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimedDataMonitorEditor"), TIMEDDATAMONITOREDITOR_API) \
	DECLARE_SERIALIZER(UTimedDataMonitorEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TIMEDDATAMONITOREDITOR_API UTimedDataMonitorEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimedDataMonitorEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TIMEDDATAMONITOREDITOR_API, UTimedDataMonitorEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimedDataMonitorEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TIMEDDATAMONITOREDITOR_API UTimedDataMonitorEditorSettings(UTimedDataMonitorEditorSettings&&); \
	TIMEDDATAMONITOREDITOR_API UTimedDataMonitorEditorSettings(const UTimedDataMonitorEditorSettings&); \
public:


#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TIMEDDATAMONITOREDITOR_API UTimedDataMonitorEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TIMEDDATAMONITOREDITOR_API UTimedDataMonitorEditorSettings(UTimedDataMonitorEditorSettings&&); \
	TIMEDDATAMONITOREDITOR_API UTimedDataMonitorEditorSettings(const UTimedDataMonitorEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TIMEDDATAMONITOREDITOR_API, UTimedDataMonitorEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTimedDataMonitorEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTimedDataMonitorEditorSettings)


#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_23_PROLOG
#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_INCLASS \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMEDDATAMONITOREDITOR_API UClass* StaticClass<class UTimedDataMonitorEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_TimedDataMonitor_Source_TimedDataMonitorEditor_Private_TimedDataMonitorEditorSettings_h


#define FOREACH_ENUM_ETIMEDDATAMONITOREDITORCALIBRATIONTYPE(op) \
	op(ETimedDataMonitorEditorCalibrationType::CalibrateWithTimecode) \
	op(ETimedDataMonitorEditorCalibrationType::TimeCorrection) \
	op(ETimedDataMonitorEditorCalibrationType::Max) 

enum class ETimedDataMonitorEditorCalibrationType;
template<> TIMEDDATAMONITOREDITOR_API UEnum* StaticEnum<ETimedDataMonitorEditorCalibrationType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
