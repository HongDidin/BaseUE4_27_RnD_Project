// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimedDataMonitorEditor/Private/TimedDataMonitorEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimedDataMonitorEditorSettings() {}
// Cross Module References
	TIMEDDATAMONITOREDITOR_API UEnum* Z_Construct_UEnum_TimedDataMonitorEditor_ETimedDataMonitorEditorCalibrationType();
	UPackage* Z_Construct_UPackage__Script_TimedDataMonitorEditor();
	TIMEDDATAMONITOREDITOR_API UClass* Z_Construct_UClass_UTimedDataMonitorEditorSettings_NoRegister();
	TIMEDDATAMONITOREDITOR_API UClass* Z_Construct_UClass_UTimedDataMonitorEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters();
	TIMEDDATAMONITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters();
// End Cross Module References
	static UEnum* ETimedDataMonitorEditorCalibrationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_TimedDataMonitorEditor_ETimedDataMonitorEditorCalibrationType, Z_Construct_UPackage__Script_TimedDataMonitorEditor(), TEXT("ETimedDataMonitorEditorCalibrationType"));
		}
		return Singleton;
	}
	template<> TIMEDDATAMONITOREDITOR_API UEnum* StaticEnum<ETimedDataMonitorEditorCalibrationType>()
	{
		return ETimedDataMonitorEditorCalibrationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETimedDataMonitorEditorCalibrationType(ETimedDataMonitorEditorCalibrationType_StaticEnum, TEXT("/Script/TimedDataMonitorEditor"), TEXT("ETimedDataMonitorEditorCalibrationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_TimedDataMonitorEditor_ETimedDataMonitorEditorCalibrationType_Hash() { return 815952651U; }
	UEnum* Z_Construct_UEnum_TimedDataMonitorEditor_ETimedDataMonitorEditorCalibrationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_TimedDataMonitorEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETimedDataMonitorEditorCalibrationType"), 0, Get_Z_Construct_UEnum_TimedDataMonitorEditor_ETimedDataMonitorEditorCalibrationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETimedDataMonitorEditorCalibrationType::CalibrateWithTimecode", (int64)ETimedDataMonitorEditorCalibrationType::CalibrateWithTimecode },
				{ "ETimedDataMonitorEditorCalibrationType::TimeCorrection", (int64)ETimedDataMonitorEditorCalibrationType::TimeCorrection },
				{ "ETimedDataMonitorEditorCalibrationType::Max", (int64)ETimedDataMonitorEditorCalibrationType::Max },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CalibrateWithTimecode.Name", "ETimedDataMonitorEditorCalibrationType::CalibrateWithTimecode" },
				{ "Max.Name", "ETimedDataMonitorEditorCalibrationType::Max" },
				{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
				{ "TimeCorrection.Name", "ETimedDataMonitorEditorCalibrationType::TimeCorrection" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_TimedDataMonitorEditor,
				nullptr,
				"ETimedDataMonitorEditorCalibrationType",
				"ETimedDataMonitorEditorCalibrationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UTimedDataMonitorEditorSettings::StaticRegisterNativesUTimedDataMonitorEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UTimedDataMonitorEditorSettings_NoRegister()
	{
		return UTimedDataMonitorEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalibrationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CalibrationSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeCorrectionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TimeCorrectionSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RefreshRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RefreshRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideNumberOfStandardDeviationToShow_MetaData[];
#endif
		static void NewProp_bOverrideNumberOfStandardDeviationToShow_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideNumberOfStandardDeviationToShow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideNumberOfStandardDeviationToShow_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OverrideNumberOfStandardDeviationToShow;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_LastCalibrationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCalibrationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LastCalibrationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetBufferStatEnabled_MetaData[];
#endif
		static void NewProp_bResetBufferStatEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetBufferStatEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClearMessageEnabled_MetaData[];
#endif
		static void NewProp_bClearMessageEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearMessageEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bResetEvaluationTimeEnabled_MetaData[];
#endif
		static void NewProp_bResetEvaluationTimeEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bResetEvaluationTimeEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TimedDataMonitorEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TimedDataMonitorEditorSettings.h" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_CalibrationSettings_MetaData[] = {
		{ "Category", "Calibration" },
		{ "Comment", "/** Option to use when calibrating from the UI. */" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "Option to use when calibrating from the UI." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_CalibrationSettings = { "CalibrationSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimedDataMonitorEditorSettings, CalibrationSettings), Z_Construct_UScriptStruct_FTimedDataMonitorCalibrationParameters, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_CalibrationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_CalibrationSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_TimeCorrectionSettings_MetaData[] = {
		{ "Category", "Calibration" },
		{ "Comment", "/** Option to use when calibrating from the UI. */" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "Option to use when calibrating from the UI." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_TimeCorrectionSettings = { "TimeCorrectionSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimedDataMonitorEditorSettings, TimeCorrectionSettings), Z_Construct_UScriptStruct_FTimedDataMonitorTimeCorrectionParameters, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_TimeCorrectionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_TimeCorrectionSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_RefreshRate_MetaData[] = {
		{ "Category", "UI" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** At which speed we should update the Timed Data Monitor UI. */" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "At which speed we should update the Timed Data Monitor UI." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_RefreshRate = { "RefreshRate", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimedDataMonitorEditorSettings, RefreshRate), METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_RefreshRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_RefreshRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow_MetaData[] = {
		{ "Category", "UI" },
		{ "InlineEditConditionToggle", "TRUE" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
	};
#endif
	void Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow_SetBit(void* Obj)
	{
		((UTimedDataMonitorEditorSettings*)Obj)->bOverrideNumberOfStandardDeviationToShow = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow = { "bOverrideNumberOfStandardDeviationToShow", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimedDataMonitorEditorSettings), &Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_OverrideNumberOfStandardDeviationToShow_MetaData[] = {
		{ "Category", "UI" },
		{ "ClampMax", "5" },
		{ "ClampMin", "0" },
		{ "Comment", "/**\n\x09 * When displaying the buffer widget, how many STD should we show.\n\x09 * By default, it will show the value used for Calibration or Time Correction.\n\x09 */" },
		{ "EditCondition", "bOverrideNumberOfStandardDeviationToShow" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "When displaying the buffer widget, how many STD should we show.\nBy default, it will show the value used for Calibration or Time Correction." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_OverrideNumberOfStandardDeviationToShow = { "OverrideNumberOfStandardDeviationToShow", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimedDataMonitorEditorSettings, OverrideNumberOfStandardDeviationToShow), METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_OverrideNumberOfStandardDeviationToShow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_OverrideNumberOfStandardDeviationToShow_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType_MetaData[] = {
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType = { "LastCalibrationType", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTimedDataMonitorEditorSettings, LastCalibrationType), Z_Construct_UEnum_TimedDataMonitorEditor_ETimedDataMonitorEditorCalibrationType, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled_MetaData[] = {
		{ "Category", "UI|Reset" },
		{ "Comment", "/** The Reset button will reset the buffer errors count. */" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "The Reset button will reset the buffer errors count." },
	};
#endif
	void Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled_SetBit(void* Obj)
	{
		((UTimedDataMonitorEditorSettings*)Obj)->bResetBufferStatEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled = { "bResetBufferStatEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimedDataMonitorEditorSettings), &Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled_MetaData[] = {
		{ "Category", "UI|Reset" },
		{ "Comment", "/** The Reset button will clear the messages list. */" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "The Reset button will clear the messages list." },
	};
#endif
	void Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled_SetBit(void* Obj)
	{
		((UTimedDataMonitorEditorSettings*)Obj)->bClearMessageEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled = { "bClearMessageEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimedDataMonitorEditorSettings), &Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled_MetaData[] = {
		{ "Category", "UI|Reset" },
		{ "Comment", "/** The Reset button will set the evaluation time of all input to 0. */" },
		{ "ModuleRelativePath", "Private/TimedDataMonitorEditorSettings.h" },
		{ "ToolTip", "The Reset button will set the evaluation time of all input to 0." },
	};
#endif
	void Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled_SetBit(void* Obj)
	{
		((UTimedDataMonitorEditorSettings*)Obj)->bResetEvaluationTimeEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled = { "bResetEvaluationTimeEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTimedDataMonitorEditorSettings), &Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_CalibrationSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_TimeCorrectionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_RefreshRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bOverrideNumberOfStandardDeviationToShow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_OverrideNumberOfStandardDeviationToShow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_LastCalibrationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetBufferStatEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bClearMessageEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::NewProp_bResetEvaluationTimeEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTimedDataMonitorEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::ClassParams = {
		&UTimedDataMonitorEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTimedDataMonitorEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTimedDataMonitorEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTimedDataMonitorEditorSettings, 631753959);
	template<> TIMEDDATAMONITOREDITOR_API UClass* StaticClass<UTimedDataMonitorEditorSettings>()
	{
		return UTimedDataMonitorEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTimedDataMonitorEditorSettings(Z_Construct_UClass_UTimedDataMonitorEditorSettings, &UTimedDataMonitorEditorSettings::StaticClass, TEXT("/Script/TimedDataMonitorEditor"), TEXT("UTimedDataMonitorEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTimedDataMonitorEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
