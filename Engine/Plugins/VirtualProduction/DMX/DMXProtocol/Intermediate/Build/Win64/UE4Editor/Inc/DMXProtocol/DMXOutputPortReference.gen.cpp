// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXProtocol/Public/IO/DMXOutputPortReference.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXOutputPortReference() {}
// Cross Module References
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputPortReference();
	UPackage* Z_Construct_UPackage__Script_DMXProtocol();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FDMXOutputPortReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXOutputPortReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXOutputPortReference, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXOutputPortReference"), sizeof(FDMXOutputPortReference), Get_Z_Construct_UScriptStruct_FDMXOutputPortReference_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXOutputPortReference>()
{
	return FDMXOutputPortReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXOutputPortReference(FDMXOutputPortReference::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXOutputPortReference"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXOutputPortReference
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXOutputPortReference()
	{
		UScriptStruct::DeferCppStructOps<FDMXOutputPortReference>(FName(TEXT("DMXOutputPortReference")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXOutputPortReference;
	struct Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PortGuid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabledFlag_MetaData[];
#endif
		static void NewProp_bEnabledFlag_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabledFlag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Reference of an input port */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortReference.h" },
		{ "ToolTip", "Reference of an input port" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXOutputPortReference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_PortGuid_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * Unique identifier shared with port config and port instance.\n\x09 * Note: This needs be BlueprintReadWrite to be accessible to property type customization, but is hidden by customization.\n\x09 */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortReference.h" },
		{ "ToolTip", "Unique identifier shared with port config and port instance.\nNote: This needs be BlueprintReadWrite to be accessible to property type customization, but is hidden by customization." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_PortGuid = { "PortGuid", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortReference, PortGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_PortGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_PortGuid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Optional flag for port references that can be enabled or disabled */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortReference.h" },
		{ "ToolTip", "Optional flag for port references that can be enabled or disabled" },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag_SetBit(void* Obj)
	{
		((FDMXOutputPortReference*)Obj)->bEnabledFlag = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag = { "bEnabledFlag", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXOutputPortReference), &Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_PortGuid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::NewProp_bEnabledFlag,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		nullptr,
		&NewStructOps,
		"DMXOutputPortReference",
		sizeof(FDMXOutputPortReference),
		alignof(FDMXOutputPortReference),
		Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputPortReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXOutputPortReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXOutputPortReference"), sizeof(FDMXOutputPortReference), Get_Z_Construct_UScriptStruct_FDMXOutputPortReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXOutputPortReference_Hash() { return 1141219523U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
