// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXProtocol/Public/DMXAttribute.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXAttribute() {}
// Cross Module References
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	UPackage* Z_Construct_UPackage__Script_DMXProtocol();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNameListItem();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttribute();
	DMXPROTOCOL_API UClass* Z_Construct_UClass_UDMXAttributeNameConversions_NoRegister();
	DMXPROTOCOL_API UClass* Z_Construct_UClass_UDMXAttributeNameConversions();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References

static_assert(std::is_polymorphic<FDMXAttributeName>() == std::is_polymorphic<FDMXNameListItem>(), "USTRUCT FDMXAttributeName cannot be polymorphic unless super FDMXNameListItem is polymorphic");

class UScriptStruct* FDMXAttributeName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXAttributeName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXAttributeName, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXAttributeName"), sizeof(FDMXAttributeName), Get_Z_Construct_UScriptStruct_FDMXAttributeName_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXAttributeName>()
{
	return FDMXAttributeName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXAttributeName(FDMXAttributeName::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXAttributeName"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXAttributeName
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXAttributeName()
	{
		UScriptStruct::DeferCppStructOps<FDMXAttributeName>(FName(TEXT("DMXAttributeName")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXAttributeName;
	struct Z_Construct_UScriptStruct_FDMXAttributeName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXAttributeName_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXAttributeName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXAttributeName>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXAttributeName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		Z_Construct_UScriptStruct_FDMXNameListItem,
		&NewStructOps,
		"DMXAttributeName",
		sizeof(FDMXAttributeName),
		alignof(FDMXAttributeName),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXAttributeName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXAttributeName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXAttributeName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXAttributeName"), sizeof(FDMXAttributeName), Get_Z_Construct_UScriptStruct_FDMXAttributeName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXAttributeName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXAttributeName_Hash() { return 4006307070U; }
class UScriptStruct* FDMXAttribute::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXAttribute_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXAttribute, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXAttribute"), sizeof(FDMXAttribute), Get_Z_Construct_UScriptStruct_FDMXAttribute_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXAttribute>()
{
	return FDMXAttribute::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXAttribute(FDMXAttribute::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXAttribute"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXAttribute
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXAttribute()
	{
		UScriptStruct::DeferCppStructOps<FDMXAttribute>(FName(TEXT("DMXAttribute")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXAttribute;
	struct Z_Construct_UScriptStruct_FDMXAttribute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Keywords_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Keywords;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXAttribute_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXAttribute>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Name of this Attribute, displayed on Attribute selectors */" },
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
		{ "ToolTip", "Name of this Attribute, displayed on Attribute selectors" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXAttribute, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Keywords_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * Keywords used when auto-mapping Fixture Functions from a GDTF file to\n\x09 * match Fixture Functions to existing Attributes.\n\x09 */" },
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
		{ "ToolTip", "Keywords used when auto-mapping Fixture Functions from a GDTF file to\nmatch Fixture Functions to existing Attributes." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Keywords = { "Keywords", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXAttribute, Keywords), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Keywords_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Keywords_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXAttribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXAttribute_Statics::NewProp_Keywords,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXAttribute_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		nullptr,
		&NewStructOps,
		"DMXAttribute",
		sizeof(FDMXAttribute),
		alignof(FDMXAttribute),
		Z_Construct_UScriptStruct_FDMXAttribute_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXAttribute_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXAttribute_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXAttribute_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXAttribute()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXAttribute_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXAttribute"), sizeof(FDMXAttribute), Get_Z_Construct_UScriptStruct_FDMXAttribute_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXAttribute_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXAttribute_Hash() { return 2397626934U; }
	DEFINE_FUNCTION(UDMXAttributeNameConversions::execConv_DMXAttributeToName)
	{
		P_GET_STRUCT_REF(FDMXAttributeName,Z_Param_Out_InAttribute);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=UDMXAttributeNameConversions::Conv_DMXAttributeToName(Z_Param_Out_InAttribute);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXAttributeNameConversions::execConv_DMXAttributeToString)
	{
		P_GET_STRUCT_REF(FDMXAttributeName,Z_Param_Out_InAttribute);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UDMXAttributeNameConversions::Conv_DMXAttributeToString(Z_Param_Out_InAttribute);
		P_NATIVE_END;
	}
	void UDMXAttributeNameConversions::StaticRegisterNativesUDMXAttributeNameConversions()
	{
		UClass* Class = UDMXAttributeNameConversions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Conv_DMXAttributeToName", &UDMXAttributeNameConversions::execConv_DMXAttributeToName },
			{ "Conv_DMXAttributeToString", &UDMXAttributeNameConversions::execConv_DMXAttributeToString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics
	{
		struct DMXAttributeNameConversions_eventConv_DMXAttributeToName_Parms
		{
			FDMXAttributeName InAttribute;
			FName ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InAttribute;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_InAttribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_InAttribute = { "InAttribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXAttributeNameConversions_eventConv_DMXAttributeToName_Parms, InAttribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_InAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_InAttribute_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXAttributeNameConversions_eventConv_DMXAttributeToName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_InAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToName (DMX Attribute)" },
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXAttributeNameConversions, nullptr, "Conv_DMXAttributeToName", nullptr, nullptr, sizeof(DMXAttributeNameConversions_eventConv_DMXAttributeToName_Parms), Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics
	{
		struct DMXAttributeNameConversions_eventConv_DMXAttributeToString_Parms
		{
			FDMXAttributeName InAttribute;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InAttribute;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_InAttribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_InAttribute = { "InAttribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXAttributeNameConversions_eventConv_DMXAttributeToString_Parms, InAttribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_InAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_InAttribute_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXAttributeNameConversions_eventConv_DMXAttributeToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_InAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToString (DMX Attribute)" },
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXAttributeNameConversions, nullptr, "Conv_DMXAttributeToString", nullptr, nullptr, sizeof(DMXAttributeNameConversions_eventConv_DMXAttributeToString_Parms), Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXAttributeNameConversions_NoRegister()
	{
		return UDMXAttributeNameConversions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXAttributeNameConversions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXAttributeNameConversions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXAttributeNameConversions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToName, "Conv_DMXAttributeToName" }, // 1702328178
		{ &Z_Construct_UFunction_UDMXAttributeNameConversions_Conv_DMXAttributeToString, "Conv_DMXAttributeToString" }, // 1501463609
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXAttributeNameConversions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DMXAttribute.h" },
		{ "ModuleRelativePath", "Public/DMXAttribute.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXAttributeNameConversions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXAttributeNameConversions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXAttributeNameConversions_Statics::ClassParams = {
		&UDMXAttributeNameConversions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXAttributeNameConversions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXAttributeNameConversions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXAttributeNameConversions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXAttributeNameConversions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXAttributeNameConversions, 2178395914);
	template<> DMXPROTOCOL_API UClass* StaticClass<UDMXAttributeNameConversions>()
	{
		return UDMXAttributeNameConversions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXAttributeNameConversions(Z_Construct_UClass_UDMXAttributeNameConversions, &UDMXAttributeNameConversions::StaticClass, TEXT("/Script/DMXProtocol"), TEXT("UDMXAttributeNameConversions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXAttributeNameConversions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
