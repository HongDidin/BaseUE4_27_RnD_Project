// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDMXFixtureCategory;
struct FDMXProtocolName;
#ifdef DMXPROTOCOL_DMXProtocolTypes_generated_h
#error "DMXProtocolTypes.generated.h already included, missing '#pragma once' in DMXProtocolTypes.h"
#endif
#define DMXPROTOCOL_DMXProtocolTypes_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_149_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDMXNameListItem Super;


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXFixtureCategory>();

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_105_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXProtocolName_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDMXNameListItem Super;


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXProtocolName>();

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConv_DMXFixtureCategoryToName); \
	DECLARE_FUNCTION(execConv_DMXFixtureCategoryToString); \
	DECLARE_FUNCTION(execConv_DMXProtocolNameToName); \
	DECLARE_FUNCTION(execConv_DMXProtocolNameToString);


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConv_DMXFixtureCategoryToName); \
	DECLARE_FUNCTION(execConv_DMXFixtureCategoryToString); \
	DECLARE_FUNCTION(execConv_DMXProtocolNameToName); \
	DECLARE_FUNCTION(execConv_DMXProtocolNameToString);


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXNameContainersConversions(); \
	friend struct Z_Construct_UClass_UDMXNameContainersConversions_Statics; \
public: \
	DECLARE_CLASS(UDMXNameContainersConversions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXProtocol"), NO_API) \
	DECLARE_SERIALIZER(UDMXNameContainersConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_INCLASS \
private: \
	static void StaticRegisterNativesUDMXNameContainersConversions(); \
	friend struct Z_Construct_UClass_UDMXNameContainersConversions_Statics; \
public: \
	DECLARE_CLASS(UDMXNameContainersConversions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXProtocol"), NO_API) \
	DECLARE_SERIALIZER(UDMXNameContainersConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXNameContainersConversions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXNameContainersConversions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXNameContainersConversions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXNameContainersConversions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXNameContainersConversions(UDMXNameContainersConversions&&); \
	NO_API UDMXNameContainersConversions(const UDMXNameContainersConversions&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXNameContainersConversions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXNameContainersConversions(UDMXNameContainersConversions&&); \
	NO_API UDMXNameContainersConversions(const UDMXNameContainersConversions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXNameContainersConversions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXNameContainersConversions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXNameContainersConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_171_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h_175_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPROTOCOL_API UClass* StaticClass<class UDMXNameContainersConversions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXProtocolTypes_h


#define FOREACH_ENUM_EDMXFIXTURESIGNALFORMAT(op) \
	op(EDMXFixtureSignalFormat::E8Bit) \
	op(EDMXFixtureSignalFormat::E16Bit) \
	op(EDMXFixtureSignalFormat::E24Bit) \
	op(EDMXFixtureSignalFormat::E32Bit) 

enum class EDMXFixtureSignalFormat : uint8;
template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXFixtureSignalFormat>();

#define FOREACH_ENUM_EDMXSENDRESULT(op) \
	op(EDMXSendResult::Success) \
	op(EDMXSendResult::ErrorGetUniverse) \
	op(EDMXSendResult::ErrorSetBuffer) \
	op(EDMXSendResult::ErrorSizeBuffer) \
	op(EDMXSendResult::ErrorEnqueuePackage) \
	op(EDMXSendResult::ErrorNoSenderInterface) 

enum class EDMXSendResult : uint8;
template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXSendResult>();

#define FOREACH_ENUM_EDMXCOMMUNICATIONTYPE(op) \
	op(EDMXCommunicationType::Broadcast) \
	op(EDMXCommunicationType::Unicast) \
	op(EDMXCommunicationType::Multicast) \
	op(EDMXCommunicationType::InternalOnly) 

enum class EDMXCommunicationType : uint8;
template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXCommunicationType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
