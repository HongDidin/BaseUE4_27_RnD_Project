// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXProtocol/Public/DMXNameListItem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXNameListItem() {}
// Cross Module References
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNameListItem();
	UPackage* Z_Construct_UPackage__Script_DMXProtocol();
// End Cross Module References
class UScriptStruct* FDMXNameListItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXNameListItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXNameListItem, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXNameListItem"), sizeof(FDMXNameListItem), Get_Z_Construct_UScriptStruct_FDMXNameListItem_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXNameListItem>()
{
	return FDMXNameListItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXNameListItem(FDMXNameListItem::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXNameListItem"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXNameListItem
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXNameListItem()
	{
		UScriptStruct::DeferCppStructOps<FDMXNameListItem>(FName(TEXT("DMXNameListItem")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXNameListItem;
	struct Z_Construct_UScriptStruct_FDMXNameListItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXNameListItem_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Types that represent names in a SNameListPicker dropdown must:\n * - inherit this struct and override the virtual functions.\n * - use the DMX_NAMELISTITEM_STATICS macros above to declare the expected statics.\n */" },
		{ "ModuleRelativePath", "Public/DMXNameListItem.h" },
		{ "ToolTip", "Types that represent names in a SNameListPicker dropdown must:\n- inherit this struct and override the virtual functions.\n- use the DMX_NAMELISTITEM_STATICS macros above to declare the expected statics." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXNameListItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXNameListItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXNameListItem_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Label of the item this struct represents */" },
		{ "ModuleRelativePath", "Public/DMXNameListItem.h" },
		{ "ToolTip", "Label of the item this struct represents" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXNameListItem_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXNameListItem, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXNameListItem_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXNameListItem_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXNameListItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXNameListItem_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXNameListItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		nullptr,
		&NewStructOps,
		"DMXNameListItem",
		sizeof(FDMXNameListItem),
		alignof(FDMXNameListItem),
		Z_Construct_UScriptStruct_FDMXNameListItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXNameListItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXNameListItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXNameListItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXNameListItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXNameListItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXNameListItem"), sizeof(FDMXNameListItem), Get_Z_Construct_UScriptStruct_FDMXNameListItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXNameListItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXNameListItem_Hash() { return 1427639445U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
