// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPROTOCOL_DMXOutputPortReference_generated_h
#error "DMXOutputPortReference.generated.h already included, missing '#pragma once' in DMXOutputPortReference.h"
#endif
#define DMXPROTOCOL_DMXOutputPortReference_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXOutputPortReference_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXOutputPortReference_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PortGuid() { return STRUCT_OFFSET(FDMXOutputPortReference, PortGuid); } \
	FORCEINLINE static uint32 __PPO__bEnabledFlag() { return STRUCT_OFFSET(FDMXOutputPortReference, bEnabledFlag); }


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXOutputPortReference>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXOutputPortReference_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
