// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPROTOCOL_DMXOutputPortConfig_generated_h
#error "DMXOutputPortConfig.generated.h already included, missing '#pragma once' in DMXOutputPortConfig.h"
#endif
#define DMXPROTOCOL_DMXOutputPortConfig_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXOutputPortConfig_h_45_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PortName() { return STRUCT_OFFSET(FDMXOutputPortConfig, PortName); } \
	FORCEINLINE static uint32 __PPO__ProtocolName() { return STRUCT_OFFSET(FDMXOutputPortConfig, ProtocolName); } \
	FORCEINLINE static uint32 __PPO__CommunicationType() { return STRUCT_OFFSET(FDMXOutputPortConfig, CommunicationType); } \
	FORCEINLINE static uint32 __PPO__DeviceAddress() { return STRUCT_OFFSET(FDMXOutputPortConfig, DeviceAddress); } \
	FORCEINLINE static uint32 __PPO__DestinationAddress() { return STRUCT_OFFSET(FDMXOutputPortConfig, DestinationAddress); } \
	FORCEINLINE static uint32 __PPO__bLoopbackToEngine() { return STRUCT_OFFSET(FDMXOutputPortConfig, bLoopbackToEngine); } \
	FORCEINLINE static uint32 __PPO__LocalUniverseStart() { return STRUCT_OFFSET(FDMXOutputPortConfig, LocalUniverseStart); } \
	FORCEINLINE static uint32 __PPO__NumUniverses() { return STRUCT_OFFSET(FDMXOutputPortConfig, NumUniverses); } \
	FORCEINLINE static uint32 __PPO__ExternUniverseStart() { return STRUCT_OFFSET(FDMXOutputPortConfig, ExternUniverseStart); } \
	FORCEINLINE static uint32 __PPO__Priority() { return STRUCT_OFFSET(FDMXOutputPortConfig, Priority); } \
	FORCEINLINE static uint32 __PPO__PortGuid() { return STRUCT_OFFSET(FDMXOutputPortConfig, PortGuid); }


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXOutputPortConfig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXOutputPortConfig_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
