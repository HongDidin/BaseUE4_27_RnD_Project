// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPROTOCOL_DMXInputPortReference_generated_h
#error "DMXInputPortReference.generated.h already included, missing '#pragma once' in DMXInputPortReference.h"
#endif
#define DMXPROTOCOL_DMXInputPortReference_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXInputPortReference_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXInputPortReference_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PortGuid() { return STRUCT_OFFSET(FDMXInputPortReference, PortGuid); }


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXInputPortReference>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXInputPortReference_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
