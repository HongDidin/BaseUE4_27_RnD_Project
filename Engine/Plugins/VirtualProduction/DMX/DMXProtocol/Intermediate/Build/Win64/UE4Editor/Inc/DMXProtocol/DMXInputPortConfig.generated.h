// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPROTOCOL_DMXInputPortConfig_generated_h
#error "DMXInputPortConfig.generated.h already included, missing '#pragma once' in DMXInputPortConfig.h"
#endif
#define DMXPROTOCOL_DMXInputPortConfig_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXInputPortConfig_h_63_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PortName() { return STRUCT_OFFSET(FDMXInputPortConfig, PortName); } \
	FORCEINLINE static uint32 __PPO__ProtocolName() { return STRUCT_OFFSET(FDMXInputPortConfig, ProtocolName); } \
	FORCEINLINE static uint32 __PPO__CommunicationType() { return STRUCT_OFFSET(FDMXInputPortConfig, CommunicationType); } \
	FORCEINLINE static uint32 __PPO__DeviceAddress() { return STRUCT_OFFSET(FDMXInputPortConfig, DeviceAddress); } \
	FORCEINLINE static uint32 __PPO__LocalUniverseStart() { return STRUCT_OFFSET(FDMXInputPortConfig, LocalUniverseStart); } \
	FORCEINLINE static uint32 __PPO__NumUniverses() { return STRUCT_OFFSET(FDMXInputPortConfig, NumUniverses); } \
	FORCEINLINE static uint32 __PPO__ExternUniverseStart() { return STRUCT_OFFSET(FDMXInputPortConfig, ExternUniverseStart); } \
	FORCEINLINE static uint32 __PPO__PriorityStrategy() { return STRUCT_OFFSET(FDMXInputPortConfig, PriorityStrategy); } \
	FORCEINLINE static uint32 __PPO__Priority() { return STRUCT_OFFSET(FDMXInputPortConfig, Priority); } \
	FORCEINLINE static uint32 __PPO__PortGuid() { return STRUCT_OFFSET(FDMXInputPortConfig, PortGuid); }


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXInputPortConfig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_IO_DMXInputPortConfig_h


#define FOREACH_ENUM_EDMXPORTPRIORITYSTRATEGY(op) \
	op(EDMXPortPriorityStrategy::None) \
	op(EDMXPortPriorityStrategy::Equal) \
	op(EDMXPortPriorityStrategy::HigherThan) \
	op(EDMXPortPriorityStrategy::LowerThan) \
	op(EDMXPortPriorityStrategy::Highest) \
	op(EDMXPortPriorityStrategy::Lowest) 

enum class EDMXPortPriorityStrategy : uint8;
template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXPortPriorityStrategy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
