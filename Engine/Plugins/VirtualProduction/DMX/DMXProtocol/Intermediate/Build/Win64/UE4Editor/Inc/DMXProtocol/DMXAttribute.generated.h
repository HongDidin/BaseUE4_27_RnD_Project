// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDMXAttributeName;
#ifdef DMXPROTOCOL_DMXAttribute_generated_h
#error "DMXAttribute.generated.h already included, missing '#pragma once' in DMXAttribute.h"
#endif
#define DMXPROTOCOL_DMXAttribute_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXAttributeName_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDMXNameListItem Super;


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXAttributeName>();

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXAttribute_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<struct FDMXAttribute>();

#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConv_DMXAttributeToName); \
	DECLARE_FUNCTION(execConv_DMXAttributeToString);


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConv_DMXAttributeToName); \
	DECLARE_FUNCTION(execConv_DMXAttributeToString);


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXAttributeNameConversions(); \
	friend struct Z_Construct_UClass_UDMXAttributeNameConversions_Statics; \
public: \
	DECLARE_CLASS(UDMXAttributeNameConversions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXProtocol"), NO_API) \
	DECLARE_SERIALIZER(UDMXAttributeNameConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_INCLASS \
private: \
	static void StaticRegisterNativesUDMXAttributeNameConversions(); \
	friend struct Z_Construct_UClass_UDMXAttributeNameConversions_Statics; \
public: \
	DECLARE_CLASS(UDMXAttributeNameConversions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXProtocol"), NO_API) \
	DECLARE_SERIALIZER(UDMXAttributeNameConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXAttributeNameConversions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXAttributeNameConversions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXAttributeNameConversions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXAttributeNameConversions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXAttributeNameConversions(UDMXAttributeNameConversions&&); \
	NO_API UDMXAttributeNameConversions(const UDMXAttributeNameConversions&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXAttributeNameConversions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXAttributeNameConversions(UDMXAttributeNameConversions&&); \
	NO_API UDMXAttributeNameConversions(const UDMXAttributeNameConversions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXAttributeNameConversions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXAttributeNameConversions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXAttributeNameConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_96_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h_100_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPROTOCOL_API UClass* StaticClass<class UDMXAttributeNameConversions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXProtocol_Source_DMXProtocol_Public_DMXAttribute_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
