// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXProtocol/Public/IO/DMXInputPortConfig.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXInputPortConfig() {}
// Cross Module References
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXPortPriorityStrategy();
	UPackage* Z_Construct_UPackage__Script_DMXProtocol();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXInputPortConfig();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	static UEnum* EDMXPortPriorityStrategy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXProtocol_EDMXPortPriorityStrategy, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("EDMXPortPriorityStrategy"));
		}
		return Singleton;
	}
	template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXPortPriorityStrategy>()
	{
		return EDMXPortPriorityStrategy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXPortPriorityStrategy(EDMXPortPriorityStrategy_StaticEnum, TEXT("/Script/DMXProtocol"), TEXT("EDMXPortPriorityStrategy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXProtocol_EDMXPortPriorityStrategy_Hash() { return 1138204641U; }
	UEnum* Z_Construct_UEnum_DMXProtocol_EDMXPortPriorityStrategy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXPortPriorityStrategy"), 0, Get_Z_Construct_UEnum_DMXProtocol_EDMXPortPriorityStrategy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXPortPriorityStrategy::None", (int64)EDMXPortPriorityStrategy::None },
				{ "EDMXPortPriorityStrategy::Equal", (int64)EDMXPortPriorityStrategy::Equal },
				{ "EDMXPortPriorityStrategy::HigherThan", (int64)EDMXPortPriorityStrategy::HigherThan },
				{ "EDMXPortPriorityStrategy::LowerThan", (int64)EDMXPortPriorityStrategy::LowerThan },
				{ "EDMXPortPriorityStrategy::Highest", (int64)EDMXPortPriorityStrategy::Highest },
				{ "EDMXPortPriorityStrategy::Lowest", (int64)EDMXPortPriorityStrategy::Lowest },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Strategy for priority system (when receiving packets)\n * \n * Not: Not all protocols have a use for this\n*/" },
				{ "Equal.Comment", "/** Manage the packet only if the priority is equal to the specified value */" },
				{ "Equal.Name", "EDMXPortPriorityStrategy::Equal" },
				{ "Equal.ToolTip", "Manage the packet only if the priority is equal to the specified value" },
				{ "HigherThan.Comment", "/** Manage the packet only if the priority is higher than the specified value */" },
				{ "HigherThan.Name", "EDMXPortPriorityStrategy::HigherThan" },
				{ "HigherThan.ToolTip", "Manage the packet only if the priority is higher than the specified value" },
				{ "Highest.Comment", "/** Manage the packet only if it matches the highest received priority */" },
				{ "Highest.Name", "EDMXPortPriorityStrategy::Highest" },
				{ "Highest.ToolTip", "Manage the packet only if it matches the highest received priority" },
				{ "LowerThan.Comment", "/** Manage the packet only if the priority is lower than the specified value */" },
				{ "LowerThan.Name", "EDMXPortPriorityStrategy::LowerThan" },
				{ "LowerThan.ToolTip", "Manage the packet only if the priority is lower than the specified value" },
				{ "Lowest.Comment", "/** Manage the packet only if it matches the lowest received priority */" },
				{ "Lowest.Name", "EDMXPortPriorityStrategy::Lowest" },
				{ "Lowest.ToolTip", "Manage the packet only if it matches the lowest received priority" },
				{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
				{ "None.Comment", "/** Always manage the packet */" },
				{ "None.Name", "EDMXPortPriorityStrategy::None" },
				{ "None.ToolTip", "Always manage the packet" },
				{ "ToolTip", "Strategy for priority system (when receiving packets)\n\nNot: Not all protocols have a use for this" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXProtocol,
				nullptr,
				"EDMXPortPriorityStrategy",
				"EDMXPortPriorityStrategy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FDMXInputPortConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXInputPortConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXInputPortConfig, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXInputPortConfig"), sizeof(FDMXInputPortConfig), Get_Z_Construct_UScriptStruct_FDMXInputPortConfig_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXInputPortConfig>()
{
	return FDMXInputPortConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXInputPortConfig(FDMXInputPortConfig::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXInputPortConfig"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXInputPortConfig
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXInputPortConfig()
	{
		UScriptStruct::DeferCppStructOps<FDMXInputPortConfig>(FName(TEXT("DMXInputPortConfig")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXInputPortConfig;
	struct Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PortName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtocolName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ProtocolName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CommunicationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommunicationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CommunicationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalUniverseStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LocalUniverseStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumUniverses_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumUniverses;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternUniverseStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ExternUniverseStart;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PriorityStrategy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PriorityStrategy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PriorityStrategy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Priority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Priority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PortGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** \n * Blueprint Configuration of a Port, used in DXM Settings to specify inputs and outputs.\n *\n * Property changes are handled in details customization consistently.\n */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "Blueprint Configuration of a Port, used in DXM Settings to specify inputs and outputs.\n\nProperty changes are handled in details customization consistently." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXInputPortConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortName_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** The name displayed wherever the port can be displayed */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "The name displayed wherever the port can be displayed" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortName = { "PortName", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, PortName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ProtocolName_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** DMX Protocol */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "DMX Protocol" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ProtocolName = { "ProtocolName", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, ProtocolName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ProtocolName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ProtocolName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** The type of communication used with this port */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "The type of communication used with this port" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType = { "CommunicationType", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, CommunicationType), Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_DeviceAddress_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** The Network Interface Card's IP Adress, over which DMX is received */" },
		{ "DisplayName", "Network Interface Card IP Address" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "The Network Interface Card's IP Adress, over which DMX is received" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_DeviceAddress = { "DeviceAddress", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, DeviceAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_DeviceAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_DeviceAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_LocalUniverseStart_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** Local Start Universe */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "Local Start Universe" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_LocalUniverseStart = { "LocalUniverseStart", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, LocalUniverseStart), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_LocalUniverseStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_LocalUniverseStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_NumUniverses_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** Number of Universes */" },
		{ "DisplayName", "Amount of Universes" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "Number of Universes" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_NumUniverses = { "NumUniverses", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, NumUniverses), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_NumUniverses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_NumUniverses_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ExternUniverseStart_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** \n\x09 * The start address this being transposed to. \n\x09 * E.g. if LocalUniverseStart is 1 and this is 100, Local Universe 1 is sent/received as Universe 100.\n\x09 */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "The start address this being transposed to.\nE.g. if LocalUniverseStart is 1 and this is 100, Local Universe 1 is sent/received as Universe 100." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ExternUniverseStart = { "ExternUniverseStart", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, ExternUniverseStart), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ExternUniverseStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ExternUniverseStart_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** How to deal with the priority value */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "How to deal with the priority value" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy = { "PriorityStrategy", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, PriorityStrategy), Z_Construct_UEnum_DMXProtocol_EDMXPortPriorityStrategy, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_Priority_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** Priority value, can act as a filter or a threshold */" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "Priority value, can act as a filter or a threshold" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, Priority), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_Priority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_Priority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortGuid_MetaData[] = {
		{ "Category", "Port Config Guid" },
		{ "Comment", "/** \n\x09 * Unique identifier, shared with the port instance.\n\x09 * Note: This needs be BlueprintReadWrite to be accessible to property type customization, but is hidden by customization.\n\x09 */" },
		{ "IgnoreForMemberInitializationTest", "" },
		{ "ModuleRelativePath", "Public/IO/DMXInputPortConfig.h" },
		{ "ToolTip", "Unique identifier, shared with the port instance.\nNote: This needs be BlueprintReadWrite to be accessible to property type customization, but is hidden by customization." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortGuid = { "PortGuid", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXInputPortConfig, PortGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ProtocolName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_CommunicationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_DeviceAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_LocalUniverseStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_NumUniverses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_ExternUniverseStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PriorityStrategy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_Priority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::NewProp_PortGuid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		nullptr,
		&NewStructOps,
		"DMXInputPortConfig",
		sizeof(FDMXInputPortConfig),
		alignof(FDMXInputPortConfig),
		Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXInputPortConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXInputPortConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXInputPortConfig"), sizeof(FDMXInputPortConfig), Get_Z_Construct_UScriptStruct_FDMXInputPortConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXInputPortConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXInputPortConfig_Hash() { return 836304677U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
