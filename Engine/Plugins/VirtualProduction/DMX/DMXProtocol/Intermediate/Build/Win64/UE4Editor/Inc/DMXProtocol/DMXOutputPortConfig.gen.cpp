// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXProtocol/Public/IO/DMXOutputPortConfig.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXOutputPortConfig() {}
// Cross Module References
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputPortConfig();
	UPackage* Z_Construct_UPackage__Script_DMXProtocol();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FDMXOutputPortConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXOutputPortConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXOutputPortConfig, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXOutputPortConfig"), sizeof(FDMXOutputPortConfig), Get_Z_Construct_UScriptStruct_FDMXOutputPortConfig_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXOutputPortConfig>()
{
	return FDMXOutputPortConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXOutputPortConfig(FDMXOutputPortConfig::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXOutputPortConfig"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXOutputPortConfig
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXOutputPortConfig()
	{
		UScriptStruct::DeferCppStructOps<FDMXOutputPortConfig>(FName(TEXT("DMXOutputPortConfig")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXOutputPortConfig;
	struct Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PortName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtocolName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ProtocolName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CommunicationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommunicationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CommunicationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DestinationAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLoopbackToEngine_MetaData[];
#endif
		static void NewProp_bLoopbackToEngine_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLoopbackToEngine;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalUniverseStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LocalUniverseStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumUniverses_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumUniverses;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternUniverseStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ExternUniverseStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Priority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Priority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PortGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** \n * Blueprint Configuration of a Port, used in DXM Settings to specify inputs and outputs.\n *\n * Property changes are handled in details customization consistently.\n */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "Blueprint Configuration of a Port, used in DXM Settings to specify inputs and outputs.\n\nProperty changes are handled in details customization consistently." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXOutputPortConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortName_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** The name displayed wherever the port can be displayed */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "The name displayed wherever the port can be displayed" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortName = { "PortName", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, PortName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ProtocolName_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** DMX Protocol */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "DMX Protocol" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ProtocolName = { "ProtocolName", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, ProtocolName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ProtocolName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ProtocolName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** The type of communication used with this port */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "The type of communication used with this port" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType = { "CommunicationType", nullptr, (EPropertyFlags)0x0020080000014005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, CommunicationType), Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DeviceAddress_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** The IP address of the network interface card over which outbound DMX is sent */" },
		{ "DisplayName", "Network Interface Card IP Address" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "The IP address of the network interface card over which outbound DMX is sent" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DeviceAddress = { "DeviceAddress", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, DeviceAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DeviceAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DeviceAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DestinationAddress_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** For Unicast, the IP address outbound DMX is sent to */" },
		{ "DisplayName", "Destination IP Address" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "For Unicast, the IP address outbound DMX is sent to" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DestinationAddress = { "DestinationAddress", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, DestinationAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DestinationAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DestinationAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** If true, the signals of output to this port is input into to the engine. It will still show only under output ports and is not visible in Monitors as Input. */" },
		{ "DisplayName", "Input into Engine" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "If true, the signals of output to this port is input into to the engine. It will still show only under output ports and is not visible in Monitors as Input." },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine_SetBit(void* Obj)
	{
		((FDMXOutputPortConfig*)Obj)->bLoopbackToEngine = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine = { "bLoopbackToEngine", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXOutputPortConfig), &Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_LocalUniverseStart_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** Local Start Universe */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "Local Start Universe" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_LocalUniverseStart = { "LocalUniverseStart", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, LocalUniverseStart), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_LocalUniverseStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_LocalUniverseStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_NumUniverses_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** Number of Universes */" },
		{ "DisplayName", "Amount of Universes" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "Number of Universes" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_NumUniverses = { "NumUniverses", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, NumUniverses), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_NumUniverses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_NumUniverses_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ExternUniverseStart_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** \n\x09 * The start address this being transposed to. \n\x09 * E.g. if LocalUniverseStart is 1 and this is 100, Local Universe 1 is sent/received as Universe 100.\n\x09 */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "The start address this being transposed to.\nE.g. if LocalUniverseStart is 1 and this is 100, Local Universe 1 is sent/received as Universe 100." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ExternUniverseStart = { "ExternUniverseStart", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, ExternUniverseStart), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ExternUniverseStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ExternUniverseStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_Priority_MetaData[] = {
		{ "Category", "Port Config" },
		{ "Comment", "/** Priority on which packets are being sent */" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "Priority on which packets are being sent" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, Priority), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_Priority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_Priority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortGuid_MetaData[] = {
		{ "Category", "Port Config Guid" },
		{ "Comment", "/** \n\x09 * Unique identifier, shared with the port instance.\n\x09 * Note: This needs be BlueprintReadWrite to be accessible to property type customization, but is hidden by customization.\n\x09 */" },
		{ "IgnoreForMemberInitializationTest", "" },
		{ "ModuleRelativePath", "Public/IO/DMXOutputPortConfig.h" },
		{ "ToolTip", "Unique identifier, shared with the port instance.\nNote: This needs be BlueprintReadWrite to be accessible to property type customization, but is hidden by customization." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortGuid = { "PortGuid", nullptr, (EPropertyFlags)0x0020080000014015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputPortConfig, PortGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ProtocolName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_CommunicationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DeviceAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_DestinationAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_bLoopbackToEngine,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_LocalUniverseStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_NumUniverses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_ExternUniverseStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_Priority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::NewProp_PortGuid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		nullptr,
		&NewStructOps,
		"DMXOutputPortConfig",
		sizeof(FDMXOutputPortConfig),
		alignof(FDMXOutputPortConfig),
		Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputPortConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXOutputPortConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXOutputPortConfig"), sizeof(FDMXOutputPortConfig), Get_Z_Construct_UScriptStruct_FDMXOutputPortConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXOutputPortConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXOutputPortConfig_Hash() { return 3652286846U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
