// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXProtocol/Public/DMXProtocolTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXProtocolTypes() {}
// Cross Module References
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat();
	UPackage* Z_Construct_UPackage__Script_DMXProtocol();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXSendResult();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureCategory();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNameListItem();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXProtocolName();
	DMXPROTOCOL_API UClass* Z_Construct_UClass_UDMXNameContainersConversions_NoRegister();
	DMXPROTOCOL_API UClass* Z_Construct_UClass_UDMXNameContainersConversions();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
	static UEnum* EDMXFixtureSignalFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("EDMXFixtureSignalFormat"));
		}
		return Singleton;
	}
	template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXFixtureSignalFormat>()
	{
		return EDMXFixtureSignalFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXFixtureSignalFormat(EDMXFixtureSignalFormat_StaticEnum, TEXT("/Script/DMXProtocol"), TEXT("EDMXFixtureSignalFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat_Hash() { return 196075430U; }
	UEnum* Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXFixtureSignalFormat"), 0, Get_Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXFixtureSignalFormat::E8Bit", (int64)EDMXFixtureSignalFormat::E8Bit },
				{ "EDMXFixtureSignalFormat::E16Bit", (int64)EDMXFixtureSignalFormat::E16Bit },
				{ "EDMXFixtureSignalFormat::E24Bit", (int64)EDMXFixtureSignalFormat::E24Bit },
				{ "EDMXFixtureSignalFormat::E32Bit", (int64)EDMXFixtureSignalFormat::E32Bit },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "DMX" },
				{ "E16Bit.Comment", "/** Uses 2 channels (bytes). Range: 0 to 65.535 */" },
				{ "E16Bit.DisplayName", "16 Bit" },
				{ "E16Bit.Name", "EDMXFixtureSignalFormat::E16Bit" },
				{ "E16Bit.ToolTip", "Uses 2 channels (bytes). Range: 0 to 65.535" },
				{ "E24Bit.Comment", "/** Uses 3 channels (bytes). Range: 0 to 16.777.215 */" },
				{ "E24Bit.DisplayName", "24 Bit" },
				{ "E24Bit.Name", "EDMXFixtureSignalFormat::E24Bit" },
				{ "E24Bit.ToolTip", "Uses 3 channels (bytes). Range: 0 to 16.777.215" },
				{ "E32Bit.Comment", "/** Uses 4 channels (bytes). Range: 0 to 4.294.967.295 */" },
				{ "E32Bit.DisplayName", "32 Bit" },
				{ "E32Bit.Name", "EDMXFixtureSignalFormat::E32Bit" },
				{ "E32Bit.ToolTip", "Uses 4 channels (bytes). Range: 0 to 4.294.967.295" },
				{ "E8Bit.Comment", "/** Uses 1 channel (byte). Range: 0 to 255 */" },
				{ "E8Bit.DisplayName", "8 Bit" },
				{ "E8Bit.Name", "EDMXFixtureSignalFormat::E8Bit" },
				{ "E8Bit.ToolTip", "Uses 1 channel (byte). Range: 0 to 255" },
				{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXProtocol,
				nullptr,
				"EDMXFixtureSignalFormat",
				"EDMXFixtureSignalFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXSendResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXProtocol_EDMXSendResult, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("EDMXSendResult"));
		}
		return Singleton;
	}
	template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXSendResult>()
	{
		return EDMXSendResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXSendResult(EDMXSendResult_StaticEnum, TEXT("/Script/DMXProtocol"), TEXT("EDMXSendResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXProtocol_EDMXSendResult_Hash() { return 1400836948U; }
	UEnum* Z_Construct_UEnum_DMXProtocol_EDMXSendResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXSendResult"), 0, Get_Z_Construct_UEnum_DMXProtocol_EDMXSendResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXSendResult::Success", (int64)EDMXSendResult::Success },
				{ "EDMXSendResult::ErrorGetUniverse", (int64)EDMXSendResult::ErrorGetUniverse },
				{ "EDMXSendResult::ErrorSetBuffer", (int64)EDMXSendResult::ErrorSetBuffer },
				{ "EDMXSendResult::ErrorSizeBuffer", (int64)EDMXSendResult::ErrorSizeBuffer },
				{ "EDMXSendResult::ErrorEnqueuePackage", (int64)EDMXSendResult::ErrorEnqueuePackage },
				{ "EDMXSendResult::ErrorNoSenderInterface", (int64)EDMXSendResult::ErrorNoSenderInterface },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "DMX" },
				{ "Comment", "/** Result when sending a DMX packet */" },
				{ "ErrorEnqueuePackage.DisplayName", "Error Enqueue Package" },
				{ "ErrorEnqueuePackage.Name", "EDMXSendResult::ErrorEnqueuePackage" },
				{ "ErrorGetUniverse.DisplayName", "Error Get Universe" },
				{ "ErrorGetUniverse.Name", "EDMXSendResult::ErrorGetUniverse" },
				{ "ErrorNoSenderInterface.DisplayName", "Error No Sending Interface" },
				{ "ErrorNoSenderInterface.Name", "EDMXSendResult::ErrorNoSenderInterface" },
				{ "ErrorSetBuffer.DisplayName", "Error Set Buffer" },
				{ "ErrorSetBuffer.Name", "EDMXSendResult::ErrorSetBuffer" },
				{ "ErrorSizeBuffer.DisplayName", "Error Size Buffer" },
				{ "ErrorSizeBuffer.Name", "EDMXSendResult::ErrorSizeBuffer" },
				{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
				{ "Success.DisplayName", "Successfully sent" },
				{ "Success.Name", "EDMXSendResult::Success" },
				{ "ToolTip", "Result when sending a DMX packet" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXProtocol,
				nullptr,
				"EDMXSendResult",
				"EDMXSendResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXCommunicationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("EDMXCommunicationType"));
		}
		return Singleton;
	}
	template<> DMXPROTOCOL_API UEnum* StaticEnum<EDMXCommunicationType>()
	{
		return EDMXCommunicationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXCommunicationType(EDMXCommunicationType_StaticEnum, TEXT("/Script/DMXProtocol"), TEXT("EDMXCommunicationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType_Hash() { return 3759476410U; }
	UEnum* Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXCommunicationType"), 0, Get_Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXCommunicationType::Broadcast", (int64)EDMXCommunicationType::Broadcast },
				{ "EDMXCommunicationType::Unicast", (int64)EDMXCommunicationType::Unicast },
				{ "EDMXCommunicationType::Multicast", (int64)EDMXCommunicationType::Multicast },
				{ "EDMXCommunicationType::InternalOnly", (int64)EDMXCommunicationType::InternalOnly },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Broadcast.Name", "EDMXCommunicationType::Broadcast" },
				{ "Comment", "/** Type of network communication */" },
				{ "InternalOnly.Name", "EDMXCommunicationType::InternalOnly" },
				{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
				{ "Multicast.Name", "EDMXCommunicationType::Multicast" },
				{ "ToolTip", "Type of network communication" },
				{ "Unicast.Name", "EDMXCommunicationType::Unicast" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXProtocol,
				nullptr,
				"EDMXCommunicationType",
				"EDMXCommunicationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FDMXFixtureCategory>() == std::is_polymorphic<FDMXNameListItem>(), "USTRUCT FDMXFixtureCategory cannot be polymorphic unless super FDMXNameListItem is polymorphic");

class UScriptStruct* FDMXFixtureCategory::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureCategory_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixtureCategory, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXFixtureCategory"), sizeof(FDMXFixtureCategory), Get_Z_Construct_UScriptStruct_FDMXFixtureCategory_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXFixtureCategory>()
{
	return FDMXFixtureCategory::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixtureCategory(FDMXFixtureCategory::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXFixtureCategory"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXFixtureCategory
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXFixtureCategory()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixtureCategory>(FName(TEXT("DMXFixtureCategory")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXFixtureCategory;
	struct Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "DMX" },
		{ "Comment", "/** Category of a fixture */" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
		{ "ToolTip", "Category of a fixture" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixtureCategory>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		Z_Construct_UScriptStruct_FDMXNameListItem,
		&NewStructOps,
		"DMXFixtureCategory",
		sizeof(FDMXFixtureCategory),
		alignof(FDMXFixtureCategory),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureCategory()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureCategory_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixtureCategory"), sizeof(FDMXFixtureCategory), Get_Z_Construct_UScriptStruct_FDMXFixtureCategory_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixtureCategory_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureCategory_Hash() { return 2839039049U; }

static_assert(std::is_polymorphic<FDMXProtocolName>() == std::is_polymorphic<FDMXNameListItem>(), "USTRUCT FDMXProtocolName cannot be polymorphic unless super FDMXNameListItem is polymorphic");

class UScriptStruct* FDMXProtocolName::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXPROTOCOL_API uint32 Get_Z_Construct_UScriptStruct_FDMXProtocolName_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXProtocolName, Z_Construct_UPackage__Script_DMXProtocol(), TEXT("DMXProtocolName"), sizeof(FDMXProtocolName), Get_Z_Construct_UScriptStruct_FDMXProtocolName_Hash());
	}
	return Singleton;
}
template<> DMXPROTOCOL_API UScriptStruct* StaticStruct<FDMXProtocolName>()
{
	return FDMXProtocolName::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXProtocolName(FDMXProtocolName::StaticStruct, TEXT("/Script/DMXProtocol"), TEXT("DMXProtocolName"), false, nullptr, nullptr);
static struct FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXProtocolName
{
	FScriptStruct_DMXProtocol_StaticRegisterNativesFDMXProtocolName()
	{
		UScriptStruct::DeferCppStructOps<FDMXProtocolName>(FName(TEXT("DMXProtocolName")));
	}
} ScriptStruct_DMXProtocol_StaticRegisterNativesFDMXProtocolName;
	struct Z_Construct_UScriptStruct_FDMXProtocolName_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXProtocolName_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "DMX" },
		{ "Comment", "/** A DMX protocol as a name that can be displayed in UI. The protocol is directly accessible via GetProtocol */" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
		{ "ToolTip", "A DMX protocol as a name that can be displayed in UI. The protocol is directly accessible via GetProtocol" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXProtocolName_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXProtocolName>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXProtocolName_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
		Z_Construct_UScriptStruct_FDMXNameListItem,
		&NewStructOps,
		"DMXProtocolName",
		sizeof(FDMXProtocolName),
		alignof(FDMXProtocolName),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXProtocolName_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXProtocolName_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXProtocolName()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXProtocolName_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXProtocol();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXProtocolName"), sizeof(FDMXProtocolName), Get_Z_Construct_UScriptStruct_FDMXProtocolName_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXProtocolName_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXProtocolName_Hash() { return 924455346U; }
	DEFINE_FUNCTION(UDMXNameContainersConversions::execConv_DMXFixtureCategoryToName)
	{
		P_GET_STRUCT_REF(FDMXFixtureCategory,Z_Param_Out_InFixtureCategory);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=UDMXNameContainersConversions::Conv_DMXFixtureCategoryToName(Z_Param_Out_InFixtureCategory);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXNameContainersConversions::execConv_DMXFixtureCategoryToString)
	{
		P_GET_STRUCT_REF(FDMXFixtureCategory,Z_Param_Out_InFixtureCategory);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UDMXNameContainersConversions::Conv_DMXFixtureCategoryToString(Z_Param_Out_InFixtureCategory);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXNameContainersConversions::execConv_DMXProtocolNameToName)
	{
		P_GET_STRUCT_REF(FDMXProtocolName,Z_Param_Out_InProtocolName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FName*)Z_Param__Result=UDMXNameContainersConversions::Conv_DMXProtocolNameToName(Z_Param_Out_InProtocolName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXNameContainersConversions::execConv_DMXProtocolNameToString)
	{
		P_GET_STRUCT_REF(FDMXProtocolName,Z_Param_Out_InProtocolName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UDMXNameContainersConversions::Conv_DMXProtocolNameToString(Z_Param_Out_InProtocolName);
		P_NATIVE_END;
	}
	void UDMXNameContainersConversions::StaticRegisterNativesUDMXNameContainersConversions()
	{
		UClass* Class = UDMXNameContainersConversions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Conv_DMXFixtureCategoryToName", &UDMXNameContainersConversions::execConv_DMXFixtureCategoryToName },
			{ "Conv_DMXFixtureCategoryToString", &UDMXNameContainersConversions::execConv_DMXFixtureCategoryToString },
			{ "Conv_DMXProtocolNameToName", &UDMXNameContainersConversions::execConv_DMXProtocolNameToName },
			{ "Conv_DMXProtocolNameToString", &UDMXNameContainersConversions::execConv_DMXProtocolNameToString },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics
	{
		struct DMXNameContainersConversions_eventConv_DMXFixtureCategoryToName_Parms
		{
			FDMXFixtureCategory InFixtureCategory;
			FName ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFixtureCategory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFixtureCategory;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_InFixtureCategory_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_InFixtureCategory = { "InFixtureCategory", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXFixtureCategoryToName_Parms, InFixtureCategory), Z_Construct_UScriptStruct_FDMXFixtureCategory, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_InFixtureCategory_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_InFixtureCategory_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXFixtureCategoryToName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_InFixtureCategory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToName (DMX Fixture Category)" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXNameContainersConversions, nullptr, "Conv_DMXFixtureCategoryToName", nullptr, nullptr, sizeof(DMXNameContainersConversions_eventConv_DMXFixtureCategoryToName_Parms), Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics
	{
		struct DMXNameContainersConversions_eventConv_DMXFixtureCategoryToString_Parms
		{
			FDMXFixtureCategory InFixtureCategory;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFixtureCategory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFixtureCategory;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_InFixtureCategory_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_InFixtureCategory = { "InFixtureCategory", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXFixtureCategoryToString_Parms, InFixtureCategory), Z_Construct_UScriptStruct_FDMXFixtureCategory, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_InFixtureCategory_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_InFixtureCategory_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXFixtureCategoryToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_InFixtureCategory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToString (DMX Fixture Category)" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXNameContainersConversions, nullptr, "Conv_DMXFixtureCategoryToString", nullptr, nullptr, sizeof(DMXNameContainersConversions_eventConv_DMXFixtureCategoryToString_Parms), Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics
	{
		struct DMXNameContainersConversions_eventConv_DMXProtocolNameToName_Parms
		{
			FDMXProtocolName InProtocolName;
			FName ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InProtocolName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InProtocolName;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_InProtocolName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_InProtocolName = { "InProtocolName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXProtocolNameToName_Parms, InProtocolName), Z_Construct_UScriptStruct_FDMXProtocolName, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_InProtocolName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_InProtocolName_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXProtocolNameToName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_InProtocolName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToName (DMX Protocol Name)" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXNameContainersConversions, nullptr, "Conv_DMXProtocolNameToName", nullptr, nullptr, sizeof(DMXNameContainersConversions_eventConv_DMXProtocolNameToName_Parms), Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics
	{
		struct DMXNameContainersConversions_eventConv_DMXProtocolNameToString_Parms
		{
			FDMXProtocolName InProtocolName;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InProtocolName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InProtocolName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_InProtocolName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_InProtocolName = { "InProtocolName", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXProtocolNameToString_Parms, InProtocolName), Z_Construct_UScriptStruct_FDMXProtocolName, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_InProtocolName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_InProtocolName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXNameContainersConversions_eventConv_DMXProtocolNameToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_InProtocolName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToString (DMX Protocol Name)" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXNameContainersConversions, nullptr, "Conv_DMXProtocolNameToString", nullptr, nullptr, sizeof(DMXNameContainersConversions_eventConv_DMXProtocolNameToString_Parms), Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXNameContainersConversions_NoRegister()
	{
		return UDMXNameContainersConversions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXNameContainersConversions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXNameContainersConversions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXProtocol,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXNameContainersConversions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToName, "Conv_DMXFixtureCategoryToName" }, // 3525749621
		{ &Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXFixtureCategoryToString, "Conv_DMXFixtureCategoryToString" }, // 4110364201
		{ &Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToName, "Conv_DMXProtocolNameToName" }, // 3856379563
		{ &Z_Construct_UFunction_UDMXNameContainersConversions_Conv_DMXProtocolNameToString, "Conv_DMXProtocolNameToString" }, // 1145282435
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXNameContainersConversions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DMXProtocolTypes.h" },
		{ "ModuleRelativePath", "Public/DMXProtocolTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXNameContainersConversions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXNameContainersConversions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXNameContainersConversions_Statics::ClassParams = {
		&UDMXNameContainersConversions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXNameContainersConversions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXNameContainersConversions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXNameContainersConversions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXNameContainersConversions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXNameContainersConversions, 1080502848);
	template<> DMXPROTOCOL_API UClass* StaticClass<UDMXNameContainersConversions>()
	{
		return UDMXNameContainersConversions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXNameContainersConversions(Z_Construct_UClass_UDMXNameContainersConversions, &UDMXNameContainersConversions::StaticClass, TEXT("/Script/DMXProtocol"), TEXT("UDMXNameContainersConversions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXNameContainersConversions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
