// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingMatrixComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingMatrixComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingMatrixComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingMatrixComponent();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
	DMXPIXELMAPPINGCORE_API UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution();
// End Cross Module References
	void UDMXPixelMappingMatrixComponent::StaticRegisterNativesUDMXPixelMappingMatrixComponent()
	{
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingMatrixComponent_NoRegister()
	{
		return UDMXPixelMappingMatrixComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchMatrixRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchMatrixRef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchRef;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeRExpose_MetaData[];
#endif
		static void NewProp_AttributeRExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeRExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeGExpose_MetaData[];
#endif
		static void NewProp_AttributeGExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeGExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeBExpose_MetaData[];
#endif
		static void NewProp_AttributeBExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeBExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMonochromeExpose_MetaData[];
#endif
		static void NewProp_bMonochromeExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMonochromeExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeRInvert_MetaData[];
#endif
		static void NewProp_AttributeRInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeRInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeGInvert_MetaData[];
#endif
		static void NewProp_AttributeGInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeGInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeBInvert_MetaData[];
#endif
		static void NewProp_AttributeBInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeBInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMonochromeInvert_MetaData[];
#endif
		static void NewProp_bMonochromeInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMonochromeInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeG_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeG;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeB_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MonochromeIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MonochromeIntensity;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ModulatorClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModulatorClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModulatorClasses;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modulators_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modulators_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modulators;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CoordinateGrid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CoordinateGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellSize;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Distribution_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distribution_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Distribution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXPixelMappingOutputComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * DMX Matrix group component\n */" },
		{ "IncludePath", "Components/DMXPixelMappingMatrixComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
		{ "ToolTip", "DMX Matrix group component" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchMatrixRef_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchMatrixRef = { "FixturePatchMatrixRef", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, FixturePatchMatrixRef_DEPRECATED), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchMatrixRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchMatrixRef_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchRef_MetaData[] = {
		{ "Category", "Matrix Settings" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchRef = { "FixturePatchRef", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, FixturePatchRef), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchRef_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode = { "ColorMode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, ColorMode), Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "R" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->AttributeRExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose = { "AttributeRExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "G" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->AttributeGExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose = { "AttributeGExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "B" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->AttributeBExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose = { "AttributeBExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Expose" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->bMonochromeExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose = { "bMonochromeExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert R" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->AttributeRInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert = { "AttributeRInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert G" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->AttributeGInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert = { "AttributeGInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert B" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->AttributeBInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert = { "AttributeBInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingMatrixComponent*)Obj)->bMonochromeInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert = { "bMonochromeInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingMatrixComponent), &Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeR_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "R Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeR = { "AttributeR", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, AttributeR), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeR_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeG_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "G Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeG = { "AttributeG", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, AttributeG), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeG_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeG_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeB_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "B Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeB = { "AttributeB", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, AttributeB), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeB_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_MonochromeIntensity_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Intensity Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_MonochromeIntensity = { "MonochromeIntensity", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, MonochromeIntensity), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_MonochromeIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_MonochromeIntensity_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses_Inner = { "ModulatorClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXModulator_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "Comment", "/** Modulators applied to the output before sending DMX */" },
		{ "DisplayName", "Output Modulators" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
		{ "ToolTip", "Modulators applied to the output before sending DMX" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses = { "ModulatorClasses", nullptr, (EPropertyFlags)0x0014000000002015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, ModulatorClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators_Inner = { "Modulators", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXModulator_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators_MetaData[] = {
		{ "Comment", "/** The actual modulator instances */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
		{ "ToolTip", "The actual modulator instances" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators = { "Modulators", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, Modulators), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CoordinateGrid_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CoordinateGrid = { "CoordinateGrid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, CoordinateGrid), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CoordinateGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CoordinateGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CellSize_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CellSize = { "CellSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, CellSize), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CellSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CellSize_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution = { "Distribution", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixComponent, Distribution), Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchMatrixRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_FixturePatchRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ColorMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeRInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeGInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeBInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_bMonochromeInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeG,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_AttributeB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_MonochromeIntensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_ModulatorClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Modulators,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CoordinateGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_CellSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::NewProp_Distribution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingMatrixComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::ClassParams = {
		&UDMXPixelMappingMatrixComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingMatrixComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingMatrixComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingMatrixComponent, 3413274137);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingMatrixComponent>()
	{
		return UDMXPixelMappingMatrixComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingMatrixComponent(Z_Construct_UClass_UDMXPixelMappingMatrixComponent, &UDMXPixelMappingMatrixComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingMatrixComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingMatrixComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDMXPixelMappingMatrixComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
