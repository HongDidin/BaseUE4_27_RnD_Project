// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingMatrixCellComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingMatrixCellComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputDMXComponent();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
// End Cross Module References
	void UDMXPixelMappingMatrixCellComponent::StaticRegisterNativesUDMXPixelMappingMatrixCellComponent()
	{
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_NoRegister()
	{
		return UDMXPixelMappingMatrixCellComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CellID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchMatrixRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchMatrixRef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXPixelMappingOutputDMXComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Matrix pixel component\n */" },
		{ "IncludePath", "Components/DMXPixelMappingMatrixCellComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixCellComponent.h" },
		{ "ToolTip", "Matrix pixel component" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellID_MetaData[] = {
		{ "Category", "Cell Settings" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixCellComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellID = { "CellID", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixCellComponent, CellID), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_FixturePatchMatrixRef_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixCellComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_FixturePatchMatrixRef = { "FixturePatchMatrixRef", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixCellComponent, FixturePatchMatrixRef_DEPRECATED), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_FixturePatchMatrixRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_FixturePatchMatrixRef_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingMatrixCellComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingMatrixCellComponent, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_FixturePatchMatrixRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::NewProp_CellCoordinate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingMatrixCellComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::ClassParams = {
		&UDMXPixelMappingMatrixCellComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingMatrixCellComponent, 3762738680);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingMatrixCellComponent>()
	{
		return UDMXPixelMappingMatrixCellComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingMatrixCellComponent(Z_Construct_UClass_UDMXPixelMappingMatrixCellComponent, &UDMXPixelMappingMatrixCellComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingMatrixCellComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingMatrixCellComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
