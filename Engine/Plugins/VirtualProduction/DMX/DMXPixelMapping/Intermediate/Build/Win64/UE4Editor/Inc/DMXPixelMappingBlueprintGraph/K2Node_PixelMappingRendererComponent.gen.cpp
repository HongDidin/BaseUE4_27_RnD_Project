// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingBlueprintGraph/Public/K2Node_PixelMappingRendererComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_PixelMappingRendererComponent() {}
// Cross Module References
	DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_NoRegister();
	DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_PixelMappingRendererComponent();
	DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_PixelMappingBaseComponent();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingBlueprintGraph();
// End Cross Module References
	void UK2Node_PixelMappingRendererComponent::StaticRegisterNativesUK2Node_PixelMappingRendererComponent()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_NoRegister()
	{
		return UK2Node_PixelMappingRendererComponent::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_PixelMappingBaseComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingBlueprintGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Node for getting Renderer Component from PixelMapping object and Renderer FName\n */" },
		{ "IncludePath", "K2Node_PixelMappingRendererComponent.h" },
		{ "ModuleRelativePath", "Public/K2Node_PixelMappingRendererComponent.h" },
		{ "ToolTip", "Node for getting Renderer Component from PixelMapping object and Renderer FName" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_PixelMappingRendererComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::ClassParams = {
		&UK2Node_PixelMappingRendererComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_PixelMappingRendererComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_PixelMappingRendererComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_PixelMappingRendererComponent, 2069802679);
	template<> DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* StaticClass<UK2Node_PixelMappingRendererComponent>()
	{
		return UK2Node_PixelMappingRendererComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_PixelMappingRendererComponent(Z_Construct_UClass_UK2Node_PixelMappingRendererComponent, &UK2Node_PixelMappingRendererComponent::StaticClass, TEXT("/Script/DMXPixelMappingBlueprintGraph"), TEXT("UK2Node_PixelMappingRendererComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_PixelMappingRendererComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
