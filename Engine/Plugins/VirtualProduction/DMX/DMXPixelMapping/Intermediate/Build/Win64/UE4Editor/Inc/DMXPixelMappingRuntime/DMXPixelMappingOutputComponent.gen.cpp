// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingOutputComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingOutputComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UEnum* Z_Construct_UEnum_DMXPixelMappingRuntime_EDMXPixelBlendingQuality();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingBaseComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
// End Cross Module References
	static UEnum* EDMXPixelBlendingQuality_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXPixelMappingRuntime_EDMXPixelBlendingQuality, Z_Construct_UPackage__Script_DMXPixelMappingRuntime(), TEXT("EDMXPixelBlendingQuality"));
		}
		return Singleton;
	}
	template<> DMXPIXELMAPPINGRUNTIME_API UEnum* StaticEnum<EDMXPixelBlendingQuality>()
	{
		return EDMXPixelBlendingQuality_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXPixelBlendingQuality(EDMXPixelBlendingQuality_StaticEnum, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("EDMXPixelBlendingQuality"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXPixelMappingRuntime_EDMXPixelBlendingQuality_Hash() { return 740719532U; }
	UEnum* Z_Construct_UEnum_DMXPixelMappingRuntime_EDMXPixelBlendingQuality()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXPixelBlendingQuality"), 0, Get_Z_Construct_UEnum_DMXPixelMappingRuntime_EDMXPixelBlendingQuality_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXPixelBlendingQuality::Low", (int64)EDMXPixelBlendingQuality::Low },
				{ "EDMXPixelBlendingQuality::Medium", (int64)EDMXPixelBlendingQuality::Medium },
				{ "EDMXPixelBlendingQuality::High", (int64)EDMXPixelBlendingQuality::High },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Enum that defines the quality of how pixels are rendered */" },
				{ "High.Comment", "/** 9 samples ( 3 x 3 ) */" },
				{ "High.Name", "EDMXPixelBlendingQuality::High" },
				{ "High.ToolTip", "9 samples ( 3 x 3 )" },
				{ "Low.Comment", "/** 1 sample */" },
				{ "Low.Name", "EDMXPixelBlendingQuality::Low" },
				{ "Low.ToolTip", "1 sample" },
				{ "Medium.Comment", "/** 5 samples ( 2 x 2 with center) */" },
				{ "Medium.Name", "EDMXPixelBlendingQuality::Medium" },
				{ "Medium.ToolTip", "5 samples ( 2 x 2 with center)" },
				{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
				{ "ToolTip", "Enum that defines the quality of how pixels are rendered" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
				nullptr,
				"EDMXPixelBlendingQuality",
				"EDMXPixelBlendingQuality",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDMXPixelMappingOutputComponent::StaticRegisterNativesUDMXPixelMappingOutputComponent()
	{
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent_NoRegister()
	{
		return UDMXPixelMappingOutputComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ZOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EditorColor;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CellBlendingQuality_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellBlendingQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CellBlendingQuality;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionX_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PositionX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionY_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PositionY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SizeX_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SizeX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SizeY_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SizeY;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLockInDesigner_MetaData[];
#endif
		static void NewProp_bLockInDesigner_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLockInDesigner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVisibleInDesigner_MetaData[];
#endif
		static void NewProp_bVisibleInDesigner_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisibleInDesigner;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXPixelMappingBaseComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for all Designer and configurable components\n */" },
		{ "IncludePath", "Components/DMXPixelMappingOutputComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
		{ "ToolTip", "Base class for all Designer and configurable components" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_ZOrder_MetaData[] = {
		{ "Comment", "/** ZOrder in the UI */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
		{ "ToolTip", "ZOrder in the UI" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_ZOrder = { "ZOrder", nullptr, (EPropertyFlags)0x0010000800000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, ZOrder), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_ZOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_ZOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_EditorColor_MetaData[] = {
		{ "Category", "Editor Settings" },
		{ "Comment", "/** The color displayed in editor */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
		{ "ToolTip", "The color displayed in editor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_EditorColor = { "EditorColor", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, EditorColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_EditorColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_EditorColor_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality_MetaData[] = {
		{ "Category", "Pixel Settings" },
		{ "Comment", "/** The quality level to use when averaging colors during downsampling. */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
		{ "ToolTip", "The quality level to use when averaging colors during downsampling." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality = { "CellBlendingQuality", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, CellBlendingQuality), Z_Construct_UEnum_DMXPixelMappingRuntime_EDMXPixelBlendingQuality, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionX_MetaData[] = {
		{ "Category", "Common Settings" },
		{ "EditCondition", "!bLockInDesigner" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionX = { "PositionX", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, PositionX), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionY_MetaData[] = {
		{ "Category", "Common Settings" },
		{ "EditCondition", "!bLockInDesigner" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionY = { "PositionY", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, PositionY), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeX_MetaData[] = {
		{ "Category", "Common Settings" },
		{ "ClampMin", "0" },
		{ "EditCondition", "!bLockInDesigner" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeX = { "SizeX", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, SizeX), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeY_MetaData[] = {
		{ "Category", "Common Settings" },
		{ "ClampMin", "0" },
		{ "EditCondition", "!bLockInDesigner" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeY = { "SizeY", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingOutputComponent, SizeY), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeY_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner_MetaData[] = {
		{ "Category", "Editor Settings" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner_SetBit(void* Obj)
	{
		((UDMXPixelMappingOutputComponent*)Obj)->bLockInDesigner = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner = { "bLockInDesigner", nullptr, (EPropertyFlags)0x0020080800000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingOutputComponent), &Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner_MetaData[] = {
		{ "Category", "Editor Settings" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingOutputComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner_SetBit(void* Obj)
	{
		((UDMXPixelMappingOutputComponent*)Obj)->bVisibleInDesigner = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner = { "bVisibleInDesigner", nullptr, (EPropertyFlags)0x0020080800000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingOutputComponent), &Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_ZOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_EditorColor,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_CellBlendingQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_PositionY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_SizeY,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bLockInDesigner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::NewProp_bVisibleInDesigner,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingOutputComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::ClassParams = {
		&UDMXPixelMappingOutputComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingOutputComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingOutputComponent, 773511906);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingOutputComponent>()
	{
		return UDMXPixelMappingOutputComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingOutputComponent(Z_Construct_UClass_UDMXPixelMappingOutputComponent, &UDMXPixelMappingOutputComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingOutputComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingOutputComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
