// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPIXELMAPPINGRUNTIME_DMXModulator_PixelMappingFrameDelay_generated_h
#error "DMXModulator_PixelMappingFrameDelay.generated.h already included, missing '#pragma once' in DMXModulator_PixelMappingFrameDelay.h"
#endif
#define DMXPIXELMAPPINGRUNTIME_DMXModulator_PixelMappingFrameDelay_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXModulator_PixelMappingFrameDelay(); \
	friend struct Z_Construct_UClass_UDMXModulator_PixelMappingFrameDelay_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator_PixelMappingFrameDelay, UDMXModulator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator_PixelMappingFrameDelay)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUDMXModulator_PixelMappingFrameDelay(); \
	friend struct Z_Construct_UClass_UDMXModulator_PixelMappingFrameDelay_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator_PixelMappingFrameDelay, UDMXModulator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator_PixelMappingFrameDelay)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXModulator_PixelMappingFrameDelay(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXModulator_PixelMappingFrameDelay) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator_PixelMappingFrameDelay); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator_PixelMappingFrameDelay); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator_PixelMappingFrameDelay(UDMXModulator_PixelMappingFrameDelay&&); \
	NO_API UDMXModulator_PixelMappingFrameDelay(const UDMXModulator_PixelMappingFrameDelay&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXModulator_PixelMappingFrameDelay(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator_PixelMappingFrameDelay(UDMXModulator_PixelMappingFrameDelay&&); \
	NO_API UDMXModulator_PixelMappingFrameDelay(const UDMXModulator_PixelMappingFrameDelay&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator_PixelMappingFrameDelay); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator_PixelMappingFrameDelay); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXModulator_PixelMappingFrameDelay)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<class UDMXModulator_PixelMappingFrameDelay>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Private_Modulators_DMXModulator_PixelMappingFrameDelay_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
