// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingEditor/Private/DMXPixelMappingThumbnailRendering.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingThumbnailRendering() {}
// Cross Module References
	DMXPIXELMAPPINGEDITOR_API UClass* Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_NoRegister();
	DMXPIXELMAPPINGEDITOR_API UClass* Z_Construct_UClass_UDMXPixelMappingThumbnailRendering();
	UNREALED_API UClass* Z_Construct_UClass_UTextureThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingEditor();
// End Cross Module References
	void UDMXPixelMappingThumbnailRendering::StaticRegisterNativesUDMXPixelMappingThumbnailRendering()
	{
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_NoRegister()
	{
		return UDMXPixelMappingThumbnailRendering::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTextureThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Dynamically  rendering thumbnail for Pixel Mapping asset\n */" },
		{ "IncludePath", "DMXPixelMappingThumbnailRendering.h" },
		{ "ModuleRelativePath", "Private/DMXPixelMappingThumbnailRendering.h" },
		{ "ToolTip", "Dynamically  rendering thumbnail for Pixel Mapping asset" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingThumbnailRendering>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::ClassParams = {
		&UDMXPixelMappingThumbnailRendering::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingThumbnailRendering()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingThumbnailRendering, 689152264);
	template<> DMXPIXELMAPPINGEDITOR_API UClass* StaticClass<UDMXPixelMappingThumbnailRendering>()
	{
		return UDMXPixelMappingThumbnailRendering::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingThumbnailRendering(Z_Construct_UClass_UDMXPixelMappingThumbnailRendering, &UDMXPixelMappingThumbnailRendering::StaticClass, TEXT("/Script/DMXPixelMappingEditor"), TEXT("UDMXPixelMappingThumbnailRendering"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingThumbnailRendering);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
