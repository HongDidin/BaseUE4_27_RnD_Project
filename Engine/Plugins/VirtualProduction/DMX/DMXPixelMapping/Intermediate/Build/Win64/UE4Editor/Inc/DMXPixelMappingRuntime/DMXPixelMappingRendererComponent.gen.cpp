// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingRendererComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingRendererComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingRendererComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingRendererComponent();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
	DMXPIXELMAPPINGCORE_API UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UDMXPixelMappingRendererComponent::execRendererInputTexture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RendererInputTexture();
		P_NATIVE_END;
	}
	void UDMXPixelMappingRendererComponent::StaticRegisterNativesUDMXPixelMappingRendererComponent()
	{
		UClass* Class = UDMXPixelMappingRendererComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "RendererInputTexture", &UDMXPixelMappingRendererComponent::execRendererInputTexture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|PixelMapping" },
		{ "Comment", "/** Render input texture for downsampling */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Render input texture for downsampling" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXPixelMappingRendererComponent, nullptr, "RendererInputTexture", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingRendererComponent_NoRegister()
	{
		return UDMXPixelMappingRendererComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RendererType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RendererType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RendererType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InputWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Brightness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Brightness;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewRenderTarget;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputRenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UserWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DownsampleBufferTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DownsampleBufferTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXPixelMappingOutputComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXPixelMappingRendererComponent_RendererInputTexture, "RendererInputTexture" }, // 3119145820
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Component for rendering input texture\n */" },
		{ "IncludePath", "Components/DMXPixelMappingRendererComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Component for rendering input texture" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType_MetaData[] = {
		{ "Category", "Render Settings" },
		{ "Comment", "/** Type of rendering, Texture, Material, UMG, etc... */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Type of rendering, Texture, Material, UMG, etc..." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType = { "RendererType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, RendererType), Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputTexture_MetaData[] = {
		{ "Category", "Render Settings" },
		{ "Comment", "/** Texture to Downsampling */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Texture to Downsampling" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputTexture = { "InputTexture", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, InputTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputMaterial_MetaData[] = {
		{ "Category", "Render Settings" },
		{ "Comment", "/** Material to Downsampling */" },
		{ "DisplayName", "User Interface Material" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Material to Downsampling" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputMaterial = { "InputMaterial", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, InputMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputWidget_MetaData[] = {
		{ "Category", "Render Settings" },
		{ "Comment", "/** UMG to Downsampling */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "UMG to Downsampling" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputWidget = { "InputWidget", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, InputWidget), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_Brightness_MetaData[] = {
		{ "Category", "Render Settings" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Master brightness of the renderer */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Master brightness of the renderer" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_Brightness = { "Brightness", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, Brightness), METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_Brightness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_Brightness_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_PreviewRenderTarget_MetaData[] = {
		{ "Comment", "/** Editor preview output target */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Editor preview output target" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_PreviewRenderTarget = { "PreviewRenderTarget", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, PreviewRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_PreviewRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_PreviewRenderTarget_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputRenderTarget_MetaData[] = {
		{ "Comment", "/** Material of UMG texture to downsample */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "Material of UMG texture to downsample" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputRenderTarget = { "InputRenderTarget", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, InputRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputRenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_UserWidget_MetaData[] = {
		{ "Comment", "/** UMG widget for downsampling */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "UMG widget for downsampling" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_UserWidget = { "UserWidget", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, UserWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_UserWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_UserWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_DownsampleBufferTarget_MetaData[] = {
		{ "Comment", "/** GPU downsample pixel buffer target texture */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingRendererComponent.h" },
		{ "ToolTip", "GPU downsample pixel buffer target texture" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_DownsampleBufferTarget = { "DownsampleBufferTarget", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingRendererComponent, DownsampleBufferTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_DownsampleBufferTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_DownsampleBufferTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_RendererType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_Brightness,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_PreviewRenderTarget,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_InputRenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_UserWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::NewProp_DownsampleBufferTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingRendererComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::ClassParams = {
		&UDMXPixelMappingRendererComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingRendererComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingRendererComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingRendererComponent, 3422899145);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingRendererComponent>()
	{
		return UDMXPixelMappingRendererComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingRendererComponent(Z_Construct_UClass_UDMXPixelMappingRendererComponent, &UDMXPixelMappingRendererComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingRendererComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingRendererComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDMXPixelMappingRendererComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
