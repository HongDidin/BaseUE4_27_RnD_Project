// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPIXELMAPPINGBLUEPRINTGRAPH_K2Node_PixelMappingBaseComponent_generated_h
#error "K2Node_PixelMappingBaseComponent.generated.h already included, missing '#pragma once' in K2Node_PixelMappingBaseComponent.h"
#endif
#define DMXPIXELMAPPINGBLUEPRINTGRAPH_K2Node_PixelMappingBaseComponent_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_PixelMappingBaseComponent(); \
	friend struct Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics; \
public: \
	DECLARE_CLASS(UK2Node_PixelMappingBaseComponent, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DMXPixelMappingBlueprintGraph"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_PixelMappingBaseComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_PixelMappingBaseComponent(); \
	friend struct Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics; \
public: \
	DECLARE_CLASS(UK2Node_PixelMappingBaseComponent, UK2Node, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DMXPixelMappingBlueprintGraph"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_PixelMappingBaseComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_PixelMappingBaseComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_PixelMappingBaseComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_PixelMappingBaseComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_PixelMappingBaseComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_PixelMappingBaseComponent(UK2Node_PixelMappingBaseComponent&&); \
	NO_API UK2Node_PixelMappingBaseComponent(const UK2Node_PixelMappingBaseComponent&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_PixelMappingBaseComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_PixelMappingBaseComponent(UK2Node_PixelMappingBaseComponent&&); \
	NO_API UK2Node_PixelMappingBaseComponent(const UK2Node_PixelMappingBaseComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_PixelMappingBaseComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_PixelMappingBaseComponent); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_PixelMappingBaseComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* StaticClass<class UK2Node_PixelMappingBaseComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingBlueprintGraph_Public_K2Node_PixelMappingBaseComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
