// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingBlueprintGraph/Public/K2Node_PixelMappingBaseComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_PixelMappingBaseComponent() {}
// Cross Module References
	DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_NoRegister();
	DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_PixelMappingBaseComponent();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingBlueprintGraph();
// End Cross Module References
	void UK2Node_PixelMappingBaseComponent::StaticRegisterNativesUK2Node_PixelMappingBaseComponent()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_NoRegister()
	{
		return UK2Node_PixelMappingBaseComponent::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingBlueprintGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base Pixel Mapping node. Never use directly\n */" },
		{ "IncludePath", "K2Node_PixelMappingBaseComponent.h" },
		{ "ModuleRelativePath", "Public/K2Node_PixelMappingBaseComponent.h" },
		{ "ToolTip", "Base Pixel Mapping node. Never use directly" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_PixelMappingBaseComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::ClassParams = {
		&UK2Node_PixelMappingBaseComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_PixelMappingBaseComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_PixelMappingBaseComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_PixelMappingBaseComponent, 3372052651);
	template<> DMXPIXELMAPPINGBLUEPRINTGRAPH_API UClass* StaticClass<UK2Node_PixelMappingBaseComponent>()
	{
		return UK2Node_PixelMappingBaseComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_PixelMappingBaseComponent(Z_Construct_UClass_UK2Node_PixelMappingBaseComponent, &UK2Node_PixelMappingBaseComponent::StaticClass, TEXT("/Script/DMXPixelMappingBlueprintGraph"), TEXT("UK2Node_PixelMappingBaseComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_PixelMappingBaseComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
