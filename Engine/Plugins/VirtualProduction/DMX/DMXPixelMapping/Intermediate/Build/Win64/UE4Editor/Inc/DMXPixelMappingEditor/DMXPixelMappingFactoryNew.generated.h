// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPIXELMAPPINGEDITOR_DMXPixelMappingFactoryNew_generated_h
#error "DMXPixelMappingFactoryNew.generated.h already included, missing '#pragma once' in DMXPixelMappingFactoryNew.h"
#endif
#define DMXPIXELMAPPINGEDITOR_DMXPixelMappingFactoryNew_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXPixelMappingFactoryNew(); \
	friend struct Z_Construct_UClass_UDMXPixelMappingFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMappingFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingEditor"), DMXPIXELMAPPINGEDITOR_API) \
	DECLARE_SERIALIZER(UDMXPixelMappingFactoryNew)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUDMXPixelMappingFactoryNew(); \
	friend struct Z_Construct_UClass_UDMXPixelMappingFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMappingFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingEditor"), DMXPIXELMAPPINGEDITOR_API) \
	DECLARE_SERIALIZER(UDMXPixelMappingFactoryNew)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DMXPIXELMAPPINGEDITOR_API UDMXPixelMappingFactoryNew(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXPixelMappingFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DMXPIXELMAPPINGEDITOR_API, UDMXPixelMappingFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMappingFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DMXPIXELMAPPINGEDITOR_API UDMXPixelMappingFactoryNew(UDMXPixelMappingFactoryNew&&); \
	DMXPIXELMAPPINGEDITOR_API UDMXPixelMappingFactoryNew(const UDMXPixelMappingFactoryNew&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DMXPIXELMAPPINGEDITOR_API UDMXPixelMappingFactoryNew(UDMXPixelMappingFactoryNew&&); \
	DMXPIXELMAPPINGEDITOR_API UDMXPixelMappingFactoryNew(const UDMXPixelMappingFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DMXPIXELMAPPINGEDITOR_API, UDMXPixelMappingFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMappingFactoryNew); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXPixelMappingFactoryNew)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_9_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPIXELMAPPINGEDITOR_API UClass* StaticClass<class UDMXPixelMappingFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_Factories_DMXPixelMappingFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
