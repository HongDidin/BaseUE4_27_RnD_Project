// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPIXELMAPPINGRUNTIME_DMXPixelMappingOutputDMXComponent_generated_h
#error "DMXPixelMappingOutputDMXComponent.generated.h already included, missing '#pragma once' in DMXPixelMappingOutputDMXComponent.h"
#endif
#define DMXPIXELMAPPINGRUNTIME_DMXPixelMappingOutputDMXComponent_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRenderWithInputAndSendDMX);


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRenderWithInputAndSendDMX);


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXPixelMappingOutputDMXComponent(); \
	friend struct Z_Construct_UClass_UDMXPixelMappingOutputDMXComponent_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMappingOutputDMXComponent, UDMXPixelMappingOutputComponent, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DMXPixelMappingRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXPixelMappingOutputDMXComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDMXPixelMappingOutputDMXComponent(); \
	friend struct Z_Construct_UClass_UDMXPixelMappingOutputDMXComponent_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMappingOutputDMXComponent, UDMXPixelMappingOutputComponent, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DMXPixelMappingRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXPixelMappingOutputDMXComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXPixelMappingOutputDMXComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXPixelMappingOutputDMXComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXPixelMappingOutputDMXComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMappingOutputDMXComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXPixelMappingOutputDMXComponent(UDMXPixelMappingOutputDMXComponent&&); \
	NO_API UDMXPixelMappingOutputDMXComponent(const UDMXPixelMappingOutputDMXComponent&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXPixelMappingOutputDMXComponent() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXPixelMappingOutputDMXComponent(UDMXPixelMappingOutputDMXComponent&&); \
	NO_API UDMXPixelMappingOutputDMXComponent(const UDMXPixelMappingOutputDMXComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXPixelMappingOutputDMXComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMappingOutputDMXComponent); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UDMXPixelMappingOutputDMXComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<class UDMXPixelMappingOutputDMXComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_Components_DMXPixelMappingOutputDMXComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
