// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingBaseComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingBaseComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingBaseComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingBaseComponent();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
// End Cross Module References
	DEFINE_FUNCTION(UDMXPixelMappingBaseComponent::execRenderAndSendDMX)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RenderAndSendDMX();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXPixelMappingBaseComponent::execRender)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Render();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXPixelMappingBaseComponent::execSendDMX)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendDMX();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXPixelMappingBaseComponent::execResetDMX)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetDMX();
		P_NATIVE_END;
	}
	void UDMXPixelMappingBaseComponent::StaticRegisterNativesUDMXPixelMappingBaseComponent()
	{
		UClass* Class = UDMXPixelMappingBaseComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Render", &UDMXPixelMappingBaseComponent::execRender },
			{ "RenderAndSendDMX", &UDMXPixelMappingBaseComponent::execRenderAndSendDMX },
			{ "ResetDMX", &UDMXPixelMappingBaseComponent::execResetDMX },
			{ "SendDMX", &UDMXPixelMappingBaseComponent::execSendDMX },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|PixelMapping" },
		{ "Comment", "/** Render downsample texture for this component and all children */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Render downsample texture for this component and all children" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXPixelMappingBaseComponent, nullptr, "Render", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|PixelMapping" },
		{ "Comment", "/** Render downsample texture and send DMX for this component and all children */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Render downsample texture and send DMX for this component and all children" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXPixelMappingBaseComponent, nullptr, "RenderAndSendDMX", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|PixelMapping" },
		{ "Comment", "/** Reset all sending DMX channels to 0 for this component and all children */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Reset all sending DMX channels to 0 for this component and all children" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXPixelMappingBaseComponent, nullptr, "ResetDMX", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|PixelMapping" },
		{ "Comment", "/** Send DMX values of this component and all children */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Send DMX values of this component and all children" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXPixelMappingBaseComponent, nullptr, "SendDMX", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingBaseComponent_NoRegister()
	{
		return UDMXPixelMappingBaseComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Children;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeakParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_WeakParent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXPixelMappingBaseComponent_Render, "Render" }, // 430941102
		{ &Z_Construct_UFunction_UDMXPixelMappingBaseComponent_RenderAndSendDMX, "RenderAndSendDMX" }, // 2711199011
		{ &Z_Construct_UFunction_UDMXPixelMappingBaseComponent_ResetDMX, "ResetDMX" }, // 3902445577
		{ &Z_Construct_UFunction_UDMXPixelMappingBaseComponent_SendDMX, "SendDMX" }, // 1821583875
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base class for all DMX Pixel Mapping components\n */" },
		{ "IncludePath", "Components/DMXPixelMappingBaseComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Base class for all DMX Pixel Mapping components" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children_Inner = { "Children", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXPixelMappingBaseComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children_MetaData[] = {
		{ "Comment", "/** Array of children belong to this component */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Array of children belong to this component" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingBaseComponent, Children), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Parent_MetaData[] = {
		{ "Comment", "/** Parent component */" },
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Leads to entangled references. Use GetParent() or WeakParent instead." },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Parent component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingBaseComponent, Parent_DEPRECATED), Z_Construct_UClass_UDMXPixelMappingBaseComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_WeakParent_MetaData[] = {
		{ "Comment", "/** Parent component */" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingBaseComponent.h" },
		{ "ToolTip", "Parent component" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_WeakParent = { "WeakParent", nullptr, (EPropertyFlags)0x0044000400000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingBaseComponent, WeakParent), Z_Construct_UClass_UDMXPixelMappingBaseComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_WeakParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_WeakParent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Children,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::NewProp_WeakParent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingBaseComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::ClassParams = {
		&UDMXPixelMappingBaseComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingBaseComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingBaseComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingBaseComponent, 2119284063);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingBaseComponent>()
	{
		return UDMXPixelMappingBaseComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingBaseComponent(Z_Construct_UClass_UDMXPixelMappingBaseComponent, &UDMXPixelMappingBaseComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingBaseComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingBaseComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDMXPixelMappingBaseComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
