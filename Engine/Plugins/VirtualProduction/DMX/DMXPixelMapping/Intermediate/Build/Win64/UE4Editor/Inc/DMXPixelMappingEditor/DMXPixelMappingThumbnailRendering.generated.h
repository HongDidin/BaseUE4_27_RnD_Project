// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXPIXELMAPPINGEDITOR_DMXPixelMappingThumbnailRendering_generated_h
#error "DMXPixelMappingThumbnailRendering.generated.h already included, missing '#pragma once' in DMXPixelMappingThumbnailRendering.h"
#endif
#define DMXPIXELMAPPINGEDITOR_DMXPixelMappingThumbnailRendering_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXPixelMappingThumbnailRendering(); \
	friend struct Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMappingThumbnailRendering, UTextureThumbnailRenderer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingEditor"), NO_API) \
	DECLARE_SERIALIZER(UDMXPixelMappingThumbnailRendering)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDMXPixelMappingThumbnailRendering(); \
	friend struct Z_Construct_UClass_UDMXPixelMappingThumbnailRendering_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMappingThumbnailRendering, UTextureThumbnailRenderer, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingEditor"), NO_API) \
	DECLARE_SERIALIZER(UDMXPixelMappingThumbnailRendering)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXPixelMappingThumbnailRendering(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXPixelMappingThumbnailRendering) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXPixelMappingThumbnailRendering); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMappingThumbnailRendering); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXPixelMappingThumbnailRendering(UDMXPixelMappingThumbnailRendering&&); \
	NO_API UDMXPixelMappingThumbnailRendering(const UDMXPixelMappingThumbnailRendering&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXPixelMappingThumbnailRendering(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXPixelMappingThumbnailRendering(UDMXPixelMappingThumbnailRendering&&); \
	NO_API UDMXPixelMappingThumbnailRendering(const UDMXPixelMappingThumbnailRendering&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXPixelMappingThumbnailRendering); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMappingThumbnailRendering); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXPixelMappingThumbnailRendering)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_13_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPIXELMAPPINGEDITOR_API UClass* StaticClass<class UDMXPixelMappingThumbnailRendering>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingEditor_Private_DMXPixelMappingThumbnailRendering_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
