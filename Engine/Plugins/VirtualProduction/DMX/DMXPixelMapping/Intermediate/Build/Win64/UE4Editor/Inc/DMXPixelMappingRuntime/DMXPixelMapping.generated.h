// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDMXPixelMappingRootComponent;
#ifdef DMXPIXELMAPPINGRUNTIME_DMXPixelMapping_generated_h
#error "DMXPixelMapping.generated.h already included, missing '#pragma once' in DMXPixelMapping.h"
#endif
#define DMXPIXELMAPPINGRUNTIME_DMXPixelMapping_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRootComponent);


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRootComponent);


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXPixelMapping(); \
	friend struct Z_Construct_UClass_UDMXPixelMapping_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMapping, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXPixelMapping)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUDMXPixelMapping(); \
	friend struct Z_Construct_UClass_UDMXPixelMapping_Statics; \
public: \
	DECLARE_CLASS(UDMXPixelMapping, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXPixelMappingRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXPixelMapping)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXPixelMapping(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXPixelMapping) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXPixelMapping); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMapping); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXPixelMapping(UDMXPixelMapping&&); \
	NO_API UDMXPixelMapping(const UDMXPixelMapping&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXPixelMapping(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXPixelMapping(UDMXPixelMapping&&); \
	NO_API UDMXPixelMapping(const UDMXPixelMapping&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXPixelMapping); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXPixelMapping); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXPixelMapping)


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_24_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<class UDMXPixelMapping>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXPixelMapping_Source_DMXPixelMappingRuntime_Public_DMXPixelMapping_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
