// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingFixtureGroupItemComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingFixtureGroupItemComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputDMXComponent();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
	DMXPIXELMAPPINGCORE_API UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_NoRegister();
// End Cross Module References
	void UDMXPixelMappingFixtureGroupItemComponent::StaticRegisterNativesUDMXPixelMappingFixtureGroupItemComponent()
	{
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_NoRegister()
	{
		return UDMXPixelMappingFixtureGroupItemComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchRef;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeRExpose_MetaData[];
#endif
		static void NewProp_AttributeRExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeRExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeGExpose_MetaData[];
#endif
		static void NewProp_AttributeGExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeGExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeBExpose_MetaData[];
#endif
		static void NewProp_AttributeBExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeBExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMonochromeExpose_MetaData[];
#endif
		static void NewProp_bMonochromeExpose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMonochromeExpose;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeRInvert_MetaData[];
#endif
		static void NewProp_AttributeRInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeRInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeGInvert_MetaData[];
#endif
		static void NewProp_AttributeGInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeGInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeBInvert_MetaData[];
#endif
		static void NewProp_AttributeBInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttributeBInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMonochromeInvert_MetaData[];
#endif
		static void NewProp_bMonochromeInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMonochromeInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeR_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeR;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeG_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeG;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeB_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeB;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MonochromeIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MonochromeIntensity;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ModulatorClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModulatorClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModulatorClasses;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modulators_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modulators_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modulators;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXPixelMappingOutputDMXComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "Output Settings" },
		{ "Comment", "/**\n * Fixture Item pixel component\n */" },
		{ "IncludePath", "Components/DMXPixelMappingFixtureGroupItemComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
		{ "ToolTip", "Fixture Item pixel component" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_FixturePatchRef_MetaData[] = {
		{ "Category", "Selected Patch" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_FixturePatchRef = { "FixturePatchRef", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, FixturePatchRef), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_FixturePatchRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_FixturePatchRef_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode = { "ColorMode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, ColorMode), Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "R" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->AttributeRExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose = { "AttributeRExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "G" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->AttributeGExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose = { "AttributeGExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "B" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->AttributeBExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose = { "AttributeBExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Expose" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->bMonochromeExpose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose = { "bMonochromeExpose", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert R" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->AttributeRInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert = { "AttributeRInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert G" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->AttributeGInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert = { "AttributeGInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert B" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->AttributeBInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert = { "AttributeBInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Invert" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert_SetBit(void* Obj)
	{
		((UDMXPixelMappingFixtureGroupItemComponent*)Obj)->bMonochromeInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert = { "bMonochromeInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXPixelMappingFixtureGroupItemComponent), &Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeR_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "R Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeR = { "AttributeR", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, AttributeR), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeR_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeR_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeG_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "G Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeG = { "AttributeG", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, AttributeG), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeG_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeG_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeB_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "B Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeB = { "AttributeB", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, AttributeB), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeB_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeB_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_MonochromeIntensity_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "DisplayName", "Intensity Attribute" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_MonochromeIntensity = { "MonochromeIntensity", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, MonochromeIntensity), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_MonochromeIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_MonochromeIntensity_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses_Inner = { "ModulatorClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXModulator_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses_MetaData[] = {
		{ "Category", "Output Settings" },
		{ "Comment", "/** Modulators applied to the output before sending DMX */" },
		{ "DisplayName", "Output Modulators" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
		{ "ToolTip", "Modulators applied to the output before sending DMX" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses = { "ModulatorClasses", nullptr, (EPropertyFlags)0x0014000000002015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, ModulatorClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators_Inner = { "Modulators", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXModulator_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupItemComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators = { "Modulators", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupItemComponent, Modulators), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_FixturePatchRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ColorMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeExpose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeRInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeGInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeBInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_bMonochromeInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeR,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeG,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_AttributeB,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_MonochromeIntensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_ModulatorClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::NewProp_Modulators,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingFixtureGroupItemComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::ClassParams = {
		&UDMXPixelMappingFixtureGroupItemComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingFixtureGroupItemComponent, 290199726);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingFixtureGroupItemComponent>()
	{
		return UDMXPixelMappingFixtureGroupItemComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingFixtureGroupItemComponent(Z_Construct_UClass_UDMXPixelMappingFixtureGroupItemComponent, &UDMXPixelMappingFixtureGroupItemComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingFixtureGroupItemComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingFixtureGroupItemComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
