// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingRuntime/Public/Components/DMXPixelMappingFixtureGroupComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingFixtureGroupComponent() {}
// Cross Module References
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_NoRegister();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent();
	DMXPIXELMAPPINGRUNTIME_API UClass* Z_Construct_UClass_UDMXPixelMappingOutputComponent();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingRuntime();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
// End Cross Module References
	void UDMXPixelMappingFixtureGroupComponent::StaticRegisterNativesUDMXPixelMappingFixtureGroupComponent()
	{
	}
	UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_NoRegister()
	{
		return UDMXPixelMappingFixtureGroupComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXLibrary;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXPixelMappingOutputComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXPixelMappingRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Container component for Fixture Items\n */" },
		{ "IncludePath", "Components/DMXPixelMappingFixtureGroupComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupComponent.h" },
		{ "ToolTip", "Container component for Fixture Items" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::NewProp_DMXLibrary_MetaData[] = {
		{ "Category", "Fixture List" },
		{ "Comment", "//~ End UDMXPixelMappingOutputComponent implementation\n" },
		{ "ModuleRelativePath", "Public/Components/DMXPixelMappingFixtureGroupComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::NewProp_DMXLibrary = { "DMXLibrary", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXPixelMappingFixtureGroupComponent, DMXLibrary), Z_Construct_UClass_UDMXLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::NewProp_DMXLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::NewProp_DMXLibrary_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::NewProp_DMXLibrary,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXPixelMappingFixtureGroupComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::ClassParams = {
		&UDMXPixelMappingFixtureGroupComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXPixelMappingFixtureGroupComponent, 2632122804);
	template<> DMXPIXELMAPPINGRUNTIME_API UClass* StaticClass<UDMXPixelMappingFixtureGroupComponent>()
	{
		return UDMXPixelMappingFixtureGroupComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXPixelMappingFixtureGroupComponent(Z_Construct_UClass_UDMXPixelMappingFixtureGroupComponent, &UDMXPixelMappingFixtureGroupComponent::StaticClass, TEXT("/Script/DMXPixelMappingRuntime"), TEXT("UDMXPixelMappingFixtureGroupComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXPixelMappingFixtureGroupComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
