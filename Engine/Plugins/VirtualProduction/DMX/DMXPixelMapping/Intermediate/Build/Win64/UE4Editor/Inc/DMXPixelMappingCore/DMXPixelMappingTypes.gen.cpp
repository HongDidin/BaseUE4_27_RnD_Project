// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXPixelMappingCore/Public/DMXPixelMappingTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXPixelMappingTypes() {}
// Cross Module References
	DMXPIXELMAPPINGCORE_API UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode();
	UPackage* Z_Construct_UPackage__Script_DMXPixelMappingCore();
	DMXPIXELMAPPINGCORE_API UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXCellFormat();
	DMXPIXELMAPPINGCORE_API UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType();
// End Cross Module References
	static UEnum* EDMXColorMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode, Z_Construct_UPackage__Script_DMXPixelMappingCore(), TEXT("EDMXColorMode"));
		}
		return Singleton;
	}
	template<> DMXPIXELMAPPINGCORE_API UEnum* StaticEnum<EDMXColorMode>()
	{
		return EDMXColorMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXColorMode(EDMXColorMode_StaticEnum, TEXT("/Script/DMXPixelMappingCore"), TEXT("EDMXColorMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode_Hash() { return 1174113510U; }
	UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXPixelMappingCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXColorMode"), 0, Get_Z_Construct_UEnum_DMXPixelMappingCore_EDMXColorMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXColorMode::CM_RGB", (int64)EDMXColorMode::CM_RGB },
				{ "EDMXColorMode::CM_Monochrome", (int64)EDMXColorMode::CM_Monochrome },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CM_Monochrome.DisplayName", "Monochrome" },
				{ "CM_Monochrome.Name", "EDMXColorMode::CM_Monochrome" },
				{ "CM_RGB.DisplayName", "RGB" },
				{ "CM_RGB.Name", "EDMXColorMode::CM_RGB" },
				{ "ModuleRelativePath", "Public/DMXPixelMappingTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXPixelMappingCore,
				nullptr,
				"EDMXColorMode",
				"EDMXColorMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXCellFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXPixelMappingCore_EDMXCellFormat, Z_Construct_UPackage__Script_DMXPixelMappingCore(), TEXT("EDMXCellFormat"));
		}
		return Singleton;
	}
	template<> DMXPIXELMAPPINGCORE_API UEnum* StaticEnum<EDMXCellFormat>()
	{
		return EDMXCellFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXCellFormat(EDMXCellFormat_StaticEnum, TEXT("/Script/DMXPixelMappingCore"), TEXT("EDMXCellFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXPixelMappingCore_EDMXCellFormat_Hash() { return 923072042U; }
	UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXCellFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXPixelMappingCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXCellFormat"), 0, Get_Z_Construct_UEnum_DMXPixelMappingCore_EDMXCellFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXCellFormat::PF_R", (int64)EDMXCellFormat::PF_R },
				{ "EDMXCellFormat::PF_G", (int64)EDMXCellFormat::PF_G },
				{ "EDMXCellFormat::PF_B", (int64)EDMXCellFormat::PF_B },
				{ "EDMXCellFormat::PF_RG", (int64)EDMXCellFormat::PF_RG },
				{ "EDMXCellFormat::PF_RB", (int64)EDMXCellFormat::PF_RB },
				{ "EDMXCellFormat::PF_GB", (int64)EDMXCellFormat::PF_GB },
				{ "EDMXCellFormat::PF_GR", (int64)EDMXCellFormat::PF_GR },
				{ "EDMXCellFormat::PF_BR", (int64)EDMXCellFormat::PF_BR },
				{ "EDMXCellFormat::PF_BG", (int64)EDMXCellFormat::PF_BG },
				{ "EDMXCellFormat::PF_RGB", (int64)EDMXCellFormat::PF_RGB },
				{ "EDMXCellFormat::PF_BRG", (int64)EDMXCellFormat::PF_BRG },
				{ "EDMXCellFormat::PF_GRB", (int64)EDMXCellFormat::PF_GRB },
				{ "EDMXCellFormat::PF_GBR", (int64)EDMXCellFormat::PF_GBR },
				{ "EDMXCellFormat::PF_RGBA", (int64)EDMXCellFormat::PF_RGBA },
				{ "EDMXCellFormat::PF_GBRA", (int64)EDMXCellFormat::PF_GBRA },
				{ "EDMXCellFormat::PF_BRGA", (int64)EDMXCellFormat::PF_BRGA },
				{ "EDMXCellFormat::PF_GRBA", (int64)EDMXCellFormat::PF_GRBA },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/DMXPixelMappingTypes.h" },
				{ "PF_B.DisplayName", "B" },
				{ "PF_B.Name", "EDMXCellFormat::PF_B" },
				{ "PF_BG.DisplayName", "BG" },
				{ "PF_BG.Name", "EDMXCellFormat::PF_BG" },
				{ "PF_BR.DisplayName", "BR" },
				{ "PF_BR.Name", "EDMXCellFormat::PF_BR" },
				{ "PF_BRG.DisplayName", "BRG" },
				{ "PF_BRG.Name", "EDMXCellFormat::PF_BRG" },
				{ "PF_BRGA.DisplayName", "BRGA" },
				{ "PF_BRGA.Name", "EDMXCellFormat::PF_BRGA" },
				{ "PF_G.DisplayName", "G" },
				{ "PF_G.Name", "EDMXCellFormat::PF_G" },
				{ "PF_GB.DisplayName", "GB" },
				{ "PF_GB.Name", "EDMXCellFormat::PF_GB" },
				{ "PF_GBR.DisplayName", "GBR" },
				{ "PF_GBR.Name", "EDMXCellFormat::PF_GBR" },
				{ "PF_GBRA.DisplayName", "GBRA" },
				{ "PF_GBRA.Name", "EDMXCellFormat::PF_GBRA" },
				{ "PF_GR.DisplayName", "GR" },
				{ "PF_GR.Name", "EDMXCellFormat::PF_GR" },
				{ "PF_GRB.DisplayName", "GRB" },
				{ "PF_GRB.Name", "EDMXCellFormat::PF_GRB" },
				{ "PF_GRBA.DisplayName", "GRBA" },
				{ "PF_GRBA.Name", "EDMXCellFormat::PF_GRBA" },
				{ "PF_R.DisplayName", "R" },
				{ "PF_R.Name", "EDMXCellFormat::PF_R" },
				{ "PF_RB.DisplayName", "RB" },
				{ "PF_RB.Name", "EDMXCellFormat::PF_RB" },
				{ "PF_RG.DisplayName", "RG" },
				{ "PF_RG.Name", "EDMXCellFormat::PF_RG" },
				{ "PF_RGB.DisplayName", "RGB" },
				{ "PF_RGB.Name", "EDMXCellFormat::PF_RGB" },
				{ "PF_RGBA.DisplayName", "RGBA" },
				{ "PF_RGBA.Name", "EDMXCellFormat::PF_RGBA" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXPixelMappingCore,
				nullptr,
				"EDMXCellFormat",
				"EDMXCellFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXPixelMappingRendererType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType, Z_Construct_UPackage__Script_DMXPixelMappingCore(), TEXT("EDMXPixelMappingRendererType"));
		}
		return Singleton;
	}
	template<> DMXPIXELMAPPINGCORE_API UEnum* StaticEnum<EDMXPixelMappingRendererType>()
	{
		return EDMXPixelMappingRendererType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXPixelMappingRendererType(EDMXPixelMappingRendererType_StaticEnum, TEXT("/Script/DMXPixelMappingCore"), TEXT("EDMXPixelMappingRendererType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType_Hash() { return 1689674450U; }
	UEnum* Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXPixelMappingCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXPixelMappingRendererType"), 0, Get_Z_Construct_UEnum_DMXPixelMappingCore_EDMXPixelMappingRendererType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXPixelMappingRendererType::Texture", (int64)EDMXPixelMappingRendererType::Texture },
				{ "EDMXPixelMappingRendererType::Material", (int64)EDMXPixelMappingRendererType::Material },
				{ "EDMXPixelMappingRendererType::UMG", (int64)EDMXPixelMappingRendererType::UMG },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Material.Name", "EDMXPixelMappingRendererType::Material" },
				{ "ModuleRelativePath", "Public/DMXPixelMappingTypes.h" },
				{ "Texture.Name", "EDMXPixelMappingRendererType::Texture" },
				{ "UMG.Name", "EDMXPixelMappingRendererType::UMG" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXPixelMappingCore,
				nullptr,
				"EDMXPixelMappingRendererType",
				"EDMXPixelMappingRendererType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
