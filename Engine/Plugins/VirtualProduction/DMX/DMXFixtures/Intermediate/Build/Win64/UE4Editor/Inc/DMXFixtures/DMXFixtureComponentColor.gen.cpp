// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXFixtures/Public/DMXFixtureComponentColor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXFixtureComponentColor() {}
// Cross Module References
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponentColor_NoRegister();
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponentColor();
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponent();
	UPackage* Z_Construct_UPackage__Script_DMXFixtures();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
// End Cross Module References
	static FName NAME_UDMXFixtureComponentColor_SetColorNoInterp = FName(TEXT("SetColorNoInterp"));
	void UDMXFixtureComponentColor::SetColorNoInterp(FLinearColor const& NewColor)
	{
		DMXFixtureComponentColor_eventSetColorNoInterp_Parms Parms;
		Parms.NewColor=NewColor;
		ProcessEvent(FindFunctionChecked(NAME_UDMXFixtureComponentColor_SetColorNoInterp),&Parms);
	}
	void UDMXFixtureComponentColor::StaticRegisterNativesUDMXFixtureComponentColor()
	{
	}
	struct Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::NewProp_NewColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::NewProp_NewColor = { "NewColor", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponentColor_eventSetColorNoInterp_Parms, NewColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::NewProp_NewColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::NewProp_NewColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::NewProp_NewColor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Sets the color of the component. Note DMX Fixture Component Color does not support interpolation */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentColor.h" },
		{ "ToolTip", "Sets the color of the component. Note DMX Fixture Component Color does not support interpolation" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponentColor, nullptr, "SetColorNoInterp", nullptr, nullptr, sizeof(DMXFixtureComponentColor_eventSetColorNoInterp_Parms), Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0CC20800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXFixtureComponentColor_NoRegister()
	{
		return UDMXFixtureComponentColor::StaticClass();
	}
	struct Z_Construct_UClass_UDMXFixtureComponentColor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChannel1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXChannel1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChannel2_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXChannel2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChannel3_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXChannel3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChannel4_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXChannel4;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXFixtureComponentColor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXFixtureComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXFixtures,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXFixtureComponentColor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXFixtureComponentColor_SetColorNoInterp, "SetColorNoInterp" }, // 3289119731
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentColor_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "DMXFixtureComponent" },
		{ "Comment", "/**\n * Specific class to handle color mixing using 4 channels (rgb, cmy, rgbw).\n * Note, the color values are never interpolated.\n */" },
		{ "IncludePath", "DMXFixtureComponentColor.h" },
		{ "IsBlueprintBase", "TRUE" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentColor.h" },
		{ "ToolTip", "Specific class to handle color mixing using 4 channels (rgb, cmy, rgbw).\nNote, the color values are never interpolated." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel1_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentColor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel1 = { "DMXChannel1", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponentColor, DMXChannel1), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel2_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentColor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel2 = { "DMXChannel2", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponentColor, DMXChannel2), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel3_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentColor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel3 = { "DMXChannel3", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponentColor, DMXChannel3), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel4_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentColor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel4 = { "DMXChannel4", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponentColor, DMXChannel4), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel4_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXFixtureComponentColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponentColor_Statics::NewProp_DMXChannel4,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXFixtureComponentColor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXFixtureComponentColor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXFixtureComponentColor_Statics::ClassParams = {
		&UDMXFixtureComponentColor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXFixtureComponentColor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentColor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXFixtureComponentColor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXFixtureComponentColor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXFixtureComponentColor, 1666776118);
	template<> DMXFIXTURES_API UClass* StaticClass<UDMXFixtureComponentColor>()
	{
		return UDMXFixtureComponentColor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXFixtureComponentColor(Z_Construct_UClass_UDMXFixtureComponentColor, &UDMXFixtureComponentColor::StaticClass, TEXT("/Script/DMXFixtures"), TEXT("UDMXFixtureComponentColor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXFixtureComponentColor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
