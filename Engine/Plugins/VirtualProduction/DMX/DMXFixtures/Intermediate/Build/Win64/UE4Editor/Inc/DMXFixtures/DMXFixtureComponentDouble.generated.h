// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXFIXTURES_DMXFixtureComponentDouble_generated_h
#error "DMXFixtureComponentDouble.generated.h already included, missing '#pragma once' in DMXFixtureComponentDouble.h"
#endif
#define DMXFIXTURES_DMXFixtureComponentDouble_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsDMXInterpolationDone); \
	DECLARE_FUNCTION(execGetDMXTargetValue); \
	DECLARE_FUNCTION(execGetDMXInterpolatedValue); \
	DECLARE_FUNCTION(execGetDMXInterpolatedStep);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsDMXInterpolationDone); \
	DECLARE_FUNCTION(execGetDMXTargetValue); \
	DECLARE_FUNCTION(execGetDMXInterpolatedValue); \
	DECLARE_FUNCTION(execGetDMXInterpolatedStep);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_EVENT_PARMS \
	struct DMXFixtureComponentDouble_eventSetChannel1ValueNoInterp_Parms \
	{ \
		float Channel1Value; \
	}; \
	struct DMXFixtureComponentDouble_eventSetChannel2ValueNoInterp_Parms \
	{ \
		float Channel2Value; \
	};


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_CALLBACK_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXFixtureComponentDouble(); \
	friend struct Z_Construct_UClass_UDMXFixtureComponentDouble_Statics; \
public: \
	DECLARE_CLASS(UDMXFixtureComponentDouble, UDMXFixtureComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(UDMXFixtureComponentDouble)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUDMXFixtureComponentDouble(); \
	friend struct Z_Construct_UClass_UDMXFixtureComponentDouble_Statics; \
public: \
	DECLARE_CLASS(UDMXFixtureComponentDouble, UDMXFixtureComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(UDMXFixtureComponentDouble)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXFixtureComponentDouble(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXFixtureComponentDouble) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXFixtureComponentDouble); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXFixtureComponentDouble); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXFixtureComponentDouble(UDMXFixtureComponentDouble&&); \
	NO_API UDMXFixtureComponentDouble(const UDMXFixtureComponentDouble&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXFixtureComponentDouble(UDMXFixtureComponentDouble&&); \
	NO_API UDMXFixtureComponentDouble(const UDMXFixtureComponentDouble&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXFixtureComponentDouble); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXFixtureComponentDouble); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXFixtureComponentDouble)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_10_PROLOG \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_EVENT_PARMS


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXFIXTURES_API UClass* StaticClass<class UDMXFixtureComponentDouble>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponentDouble_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
