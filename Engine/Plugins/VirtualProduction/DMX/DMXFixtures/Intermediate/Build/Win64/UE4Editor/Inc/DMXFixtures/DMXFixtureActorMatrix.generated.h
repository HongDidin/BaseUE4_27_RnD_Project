// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDMXCell;
#ifdef DMXFIXTURES_DMXFixtureActorMatrix_generated_h
#error "DMXFixtureActorMatrix.generated.h already included, missing '#pragma once' in DMXFixtureActorMatrix.h"
#endif
#define DMXFIXTURES_DMXFixtureActorMatrix_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGenerateEditorMatrixMesh); \
	DECLARE_FUNCTION(execInitializeMatrixFixture); \
	DECLARE_FUNCTION(execPushFixtureMatrixCellData);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGenerateEditorMatrixMesh); \
	DECLARE_FUNCTION(execInitializeMatrixFixture); \
	DECLARE_FUNCTION(execPushFixtureMatrixCellData);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADMXFixtureActorMatrix(); \
	friend struct Z_Construct_UClass_ADMXFixtureActorMatrix_Statics; \
public: \
	DECLARE_CLASS(ADMXFixtureActorMatrix, ADMXFixtureActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(ADMXFixtureActorMatrix)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_INCLASS \
private: \
	static void StaticRegisterNativesADMXFixtureActorMatrix(); \
	friend struct Z_Construct_UClass_ADMXFixtureActorMatrix_Statics; \
public: \
	DECLARE_CLASS(ADMXFixtureActorMatrix, ADMXFixtureActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(ADMXFixtureActorMatrix)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADMXFixtureActorMatrix(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADMXFixtureActorMatrix) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADMXFixtureActorMatrix); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADMXFixtureActorMatrix); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADMXFixtureActorMatrix(ADMXFixtureActorMatrix&&); \
	NO_API ADMXFixtureActorMatrix(const ADMXFixtureActorMatrix&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADMXFixtureActorMatrix(ADMXFixtureActorMatrix&&); \
	NO_API ADMXFixtureActorMatrix(const ADMXFixtureActorMatrix&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADMXFixtureActorMatrix); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADMXFixtureActorMatrix); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADMXFixtureActorMatrix)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_10_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXFIXTURES_API UClass* StaticClass<class ADMXFixtureActorMatrix>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActorMatrix_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
