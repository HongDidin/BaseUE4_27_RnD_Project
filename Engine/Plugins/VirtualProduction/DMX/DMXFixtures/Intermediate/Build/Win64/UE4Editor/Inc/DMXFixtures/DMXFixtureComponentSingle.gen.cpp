// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXFixtures/Public/DMXFixtureComponentSingle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXFixtureComponentSingle() {}
// Cross Module References
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponentSingle_NoRegister();
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponentSingle();
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponent();
	UPackage* Z_Construct_UPackage__Script_DMXFixtures();
	DMXFIXTURES_API UScriptStruct* Z_Construct_UScriptStruct_FDMXChannelData();
// End Cross Module References
	DEFINE_FUNCTION(UDMXFixtureComponentSingle::execIsDMXInterpolationDone)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsDMXInterpolationDone();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXFixtureComponentSingle::execGetDMXTargetValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetDMXTargetValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXFixtureComponentSingle::execGetDMXInterpolatedValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetDMXInterpolatedValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXFixtureComponentSingle::execGetDMXInterpolatedStep)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetDMXInterpolatedStep();
		P_NATIVE_END;
	}
	static FName NAME_UDMXFixtureComponentSingle_SetValueNoInterp = FName(TEXT("SetValueNoInterp"));
	void UDMXFixtureComponentSingle::SetValueNoInterp(float NewValue)
	{
		DMXFixtureComponentSingle_eventSetValueNoInterp_Parms Parms;
		Parms.NewValue=NewValue;
		ProcessEvent(FindFunctionChecked(NAME_UDMXFixtureComponentSingle_SetValueNoInterp),&Parms);
	}
	void UDMXFixtureComponentSingle::StaticRegisterNativesUDMXFixtureComponentSingle()
	{
		UClass* Class = UDMXFixtureComponentSingle::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDMXInterpolatedStep", &UDMXFixtureComponentSingle::execGetDMXInterpolatedStep },
			{ "GetDMXInterpolatedValue", &UDMXFixtureComponentSingle::execGetDMXInterpolatedValue },
			{ "GetDMXTargetValue", &UDMXFixtureComponentSingle::execGetDMXTargetValue },
			{ "IsDMXInterpolationDone", &UDMXFixtureComponentSingle::execIsDMXInterpolationDone },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics
	{
		struct DMXFixtureComponentSingle_eventGetDMXInterpolatedStep_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponentSingle_eventGetDMXInterpolatedStep_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Gets the interpolation delta value (step) for this frame */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
		{ "ToolTip", "Gets the interpolation delta value (step) for this frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponentSingle, nullptr, "GetDMXInterpolatedStep", nullptr, nullptr, sizeof(DMXFixtureComponentSingle_eventGetDMXInterpolatedStep_Parms), Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics
	{
		struct DMXFixtureComponentSingle_eventGetDMXInterpolatedValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponentSingle_eventGetDMXInterpolatedValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Gets the current interpolated value */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
		{ "ToolTip", "Gets the current interpolated value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponentSingle, nullptr, "GetDMXInterpolatedValue", nullptr, nullptr, sizeof(DMXFixtureComponentSingle_eventGetDMXInterpolatedValue_Parms), Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics
	{
		struct DMXFixtureComponentSingle_eventGetDMXTargetValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponentSingle_eventGetDMXTargetValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Gets the target value towards which the component interpolates */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
		{ "ToolTip", "Gets the target value towards which the component interpolates" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponentSingle, nullptr, "GetDMXTargetValue", nullptr, nullptr, sizeof(DMXFixtureComponentSingle_eventGetDMXTargetValue_Parms), Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics
	{
		struct DMXFixtureComponentSingle_eventIsDMXInterpolationDone_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXFixtureComponentSingle_eventIsDMXInterpolationDone_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXFixtureComponentSingle_eventIsDMXInterpolationDone_Parms), &Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** True if the target value is reached and no interpolation is required */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
		{ "ToolTip", "True if the target value is reached and no interpolation is required" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponentSingle, nullptr, "IsDMXInterpolationDone", nullptr, nullptr, sizeof(DMXFixtureComponentSingle_eventIsDMXInterpolationDone_Parms), Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NewValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::NewProp_NewValue = { "NewValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponentSingle_eventSetValueNoInterp_Parms, NewValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::NewProp_NewValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX Component" },
		{ "Comment", "/** Called to set the value. When interpolation is enabled this function is called by the plugin until the target value is reached, else just once. */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
		{ "ToolTip", "Called to set the value. When interpolation is enabled this function is called by the plugin until the target value is reached, else just once." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponentSingle, nullptr, "SetValueNoInterp", nullptr, nullptr, sizeof(DMXFixtureComponentSingle_eventSetValueNoInterp_Parms), Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXFixtureComponentSingle_NoRegister()
	{
		return UDMXFixtureComponentSingle::StaticClass();
	}
	struct Z_Construct_UClass_UDMXFixtureComponentSingle_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXChannel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXFixtureComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXFixtures,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedStep, "GetDMXInterpolatedStep" }, // 1693492297
		{ &Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXInterpolatedValue, "GetDMXInterpolatedValue" }, // 3959423385
		{ &Z_Construct_UFunction_UDMXFixtureComponentSingle_GetDMXTargetValue, "GetDMXTargetValue" }, // 1100456409
		{ &Z_Construct_UFunction_UDMXFixtureComponentSingle_IsDMXInterpolationDone, "IsDMXInterpolationDone" }, // 2990377185
		{ &Z_Construct_UFunction_UDMXFixtureComponentSingle_SetValueNoInterp, "SetValueNoInterp" }, // 3731076012
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "FixtureComponent" },
		{ "Comment", "// Component that uses 1 DMX channel\n" },
		{ "IncludePath", "DMXFixtureComponentSingle.h" },
		{ "IsBlueprintBase", "TRUE" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
		{ "ToolTip", "Component that uses 1 DMX channel" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::NewProp_DMXChannel_MetaData[] = {
		{ "Category", "DMX Channels" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponentSingle.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::NewProp_DMXChannel = { "DMXChannel", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponentSingle, DMXChannel), Z_Construct_UScriptStruct_FDMXChannelData, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::NewProp_DMXChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::NewProp_DMXChannel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::NewProp_DMXChannel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXFixtureComponentSingle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::ClassParams = {
		&UDMXFixtureComponentSingle::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXFixtureComponentSingle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXFixtureComponentSingle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXFixtureComponentSingle, 3977617418);
	template<> DMXFIXTURES_API UClass* StaticClass<UDMXFixtureComponentSingle>()
	{
		return UDMXFixtureComponentSingle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXFixtureComponentSingle(Z_Construct_UClass_UDMXFixtureComponentSingle, &UDMXFixtureComponentSingle::StaticClass, TEXT("/Script/DMXFixtures"), TEXT("UDMXFixtureComponentSingle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXFixtureComponentSingle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
