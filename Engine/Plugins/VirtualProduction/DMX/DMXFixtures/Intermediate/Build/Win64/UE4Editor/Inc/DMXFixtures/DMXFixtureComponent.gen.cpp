// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXFixtures/Public/DMXFixtureComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXFixtureComponent() {}
// Cross Module References
	DMXFIXTURES_API UScriptStruct* Z_Construct_UScriptStruct_FDMXChannelData();
	UPackage* Z_Construct_UPackage__Script_DMXFixtures();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponent_NoRegister();
	DMXFIXTURES_API UClass* Z_Construct_UClass_UDMXFixtureComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	DMXFIXTURES_API UClass* Z_Construct_UClass_ADMXFixtureActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
// End Cross Module References
class UScriptStruct* FDMXChannelData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXFIXTURES_API uint32 Get_Z_Construct_UScriptStruct_FDMXChannelData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXChannelData, Z_Construct_UPackage__Script_DMXFixtures(), TEXT("DMXChannelData"), sizeof(FDMXChannelData), Get_Z_Construct_UScriptStruct_FDMXChannelData_Hash());
	}
	return Singleton;
}
template<> DMXFIXTURES_API UScriptStruct* StaticStruct<FDMXChannelData>()
{
	return FDMXChannelData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXChannelData(FDMXChannelData::StaticStruct, TEXT("/Script/DMXFixtures"), TEXT("DMXChannelData"), false, nullptr, nullptr);
static struct FScriptStruct_DMXFixtures_StaticRegisterNativesFDMXChannelData
{
	FScriptStruct_DMXFixtures_StaticRegisterNativesFDMXChannelData()
	{
		UScriptStruct::DeferCppStructOps<FDMXChannelData>(FName(TEXT("DMXChannelData")));
	}
} ScriptStruct_DMXFixtures_StaticRegisterNativesFDMXChannelData;
	struct Z_Construct_UScriptStruct_FDMXChannelData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXChannelData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXChannelData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXChannelData, Name), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MinValue_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MinValue = { "MinValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXChannelData, MinValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MinValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MinValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MaxValue_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MaxValue = { "MaxValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXChannelData, MaxValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MaxValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MaxValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "DMX Channel" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXChannelData, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXChannelData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MinValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_MaxValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXChannelData_Statics::NewProp_DefaultValue,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXChannelData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXFixtures,
		nullptr,
		&NewStructOps,
		"DMXChannelData",
		sizeof(FDMXChannelData),
		alignof(FDMXChannelData),
		Z_Construct_UScriptStruct_FDMXChannelData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXChannelData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXChannelData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXChannelData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXChannelData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXChannelData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXFixtures();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXChannelData"), sizeof(FDMXChannelData), Get_Z_Construct_UScriptStruct_FDMXChannelData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXChannelData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXChannelData_Hash() { return 2367645219U; }
	DEFINE_FUNCTION(UDMXFixtureComponent::execGetTextureCenterColors)
	{
		P_GET_OBJECT(UTexture2D,Z_Param_TextureAtlas);
		P_GET_PROPERTY(FIntProperty,Z_Param_numTextures);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FLinearColor>*)Z_Param__Result=P_THIS->GetTextureCenterColors(Z_Param_TextureAtlas,Z_Param_numTextures);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXFixtureComponent::execGetParentFixtureActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ADMXFixtureActor**)Z_Param__Result=P_THIS->GetParentFixtureActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXFixtureComponent::execInitialize)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Initialize();
		P_NATIVE_END;
	}
	static FName NAME_UDMXFixtureComponent_InitializeComponent = FName(TEXT("InitializeComponent"));
	void UDMXFixtureComponent::InitializeComponent()
	{
		ProcessEvent(FindFunctionChecked(NAME_UDMXFixtureComponent_InitializeComponent),NULL);
	}
	static FName NAME_UDMXFixtureComponent_InterpolateComponent = FName(TEXT("InterpolateComponent"));
	void UDMXFixtureComponent::InterpolateComponent(float DeltaSeconds)
	{
		DMXFixtureComponent_eventInterpolateComponent_Parms Parms;
		Parms.DeltaSeconds=DeltaSeconds;
		ProcessEvent(FindFunctionChecked(NAME_UDMXFixtureComponent_InterpolateComponent),&Parms);
	}
	void UDMXFixtureComponent::StaticRegisterNativesUDMXFixtureComponent()
	{
		UClass* Class = UDMXFixtureComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetParentFixtureActor", &UDMXFixtureComponent::execGetParentFixtureActor },
			{ "GetTextureCenterColors", &UDMXFixtureComponent::execGetTextureCenterColors },
			{ "Initialize", &UDMXFixtureComponent::execInitialize },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics
	{
		struct DMXFixtureComponent_eventGetParentFixtureActor_Parms
		{
			ADMXFixtureActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponent_eventGetParentFixtureActor_Parms, ReturnValue), Z_Construct_UClass_ADMXFixtureActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** If attached to a DMX Fixture Actor, returns the parent fixture actor. */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "If attached to a DMX Fixture Actor, returns the parent fixture actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponent, nullptr, "GetParentFixtureActor", nullptr, nullptr, sizeof(DMXFixtureComponent_eventGetParentFixtureActor_Parms), Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics
	{
		struct DMXFixtureComponent_eventGetTextureCenterColors_Parms
		{
			UTexture2D* TextureAtlas;
			int32 numTextures;
			TArray<FLinearColor> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextureAtlas;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_numTextures;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_TextureAtlas = { "TextureAtlas", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponent_eventGetTextureCenterColors_Parms, TextureAtlas), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_numTextures = { "numTextures", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponent_eventGetTextureCenterColors_Parms, numTextures), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponent_eventGetTextureCenterColors_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_TextureAtlas,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_numTextures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Reads pixel color in the middle of each \"Texture\" and output linear colors */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "Reads pixel color in the middle of each \"Texture\" and output linear colors" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponent, nullptr, "GetTextureCenterColors", nullptr, nullptr, sizeof(DMXFixtureComponent_eventGetTextureCenterColors_Parms), Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponent_Initialize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponent_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Initializes the component */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "Initializes the component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponent_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponent, nullptr, "Initialize", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponent_Initialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponent_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponent_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Called to initialize the component in blueprints */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "Called to initialize the component in blueprints" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponent, nullptr, "InitializeComponent", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::NewProp_DeltaSeconds = { "DeltaSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureComponent_eventInterpolateComponent_Parms, DeltaSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::NewProp_DeltaSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Called each tick when interpolation is enabled, to calculate the next value */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "Called each tick when interpolation is enabled, to calculate the next value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXFixtureComponent, nullptr, "InterpolateComponent", nullptr, nullptr, sizeof(DMXFixtureComponent_eventInterpolateComponent_Parms), Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXFixtureComponent_NoRegister()
	{
		return UDMXFixtureComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXFixtureComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkipThreshold_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SkipThreshold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseInterpolation_MetaData[];
#endif
		static void NewProp_bUseInterpolation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseInterpolation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpolationScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsingMatrixData_MetaData[];
#endif
		static void NewProp_bUsingMatrixData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsingMatrixData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXFixtureComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXFixtures,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXFixtureComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXFixtureComponent_GetParentFixtureActor, "GetParentFixtureActor" }, // 2756878974
		{ &Z_Construct_UFunction_UDMXFixtureComponent_GetTextureCenterColors, "GetTextureCenterColors" }, // 149809306
		{ &Z_Construct_UFunction_UDMXFixtureComponent_Initialize, "Initialize" }, // 3033599624
		{ &Z_Construct_UFunction_UDMXFixtureComponent_InitializeComponent, "InitializeComponent" }, // 3402423731
		{ &Z_Construct_UFunction_UDMXFixtureComponent_InterpolateComponent, "InterpolateComponent" }, // 3982200502
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponent_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DMXFixtureComponent.h" },
		{ "IsBlueprintBase", "FALSE" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "DMX Parameters" },
		{ "Comment", "/** If used within a DMX Fixture Actor or Fixture Matrix Actor, the component only receives data when set to true. Else needs be implemented in blueprints. */" },
		{ "DisplayPriority", "0" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "If used within a DMX Fixture Actor or Fixture Matrix Actor, the component only receives data when set to true. Else needs be implemented in blueprints." },
	};
#endif
	void Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UDMXFixtureComponent*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXFixtureComponent), &Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_SkipThreshold_MetaData[] = {
		{ "Category", "DMX Parameters" },
		{ "Comment", "/** Value changes smaller than this threshold are ignored */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "Value changes smaller than this threshold are ignored" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_SkipThreshold = { "SkipThreshold", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponent, SkipThreshold), METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_SkipThreshold_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_SkipThreshold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation_MetaData[] = {
		{ "Category", "DMX Parameters" },
		{ "Comment", "/** If used within a DMX Fixture Actor or Fixture Matrix Actor, the plugin interpolates towards the last set value. */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "If used within a DMX Fixture Actor or Fixture Matrix Actor, the plugin interpolates towards the last set value." },
	};
#endif
	void Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation_SetBit(void* Obj)
	{
		((UDMXFixtureComponent*)Obj)->bUseInterpolation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation = { "bUseInterpolation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXFixtureComponent), &Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_InterpolationScale_MetaData[] = {
		{ "Category", "DMX Parameters" },
		{ "Comment", "/** The scale of the interpolation speed. Faster when > 1, slower when < 1 */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "The scale of the interpolation speed. Faster when > 1, slower when < 1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_InterpolationScale = { "InterpolationScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXFixtureComponent, InterpolationScale), METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_InterpolationScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_InterpolationScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData_MetaData[] = {
		{ "Comment", "/** True if the component is attached to a matrix fixture */" },
		{ "ModuleRelativePath", "Public/DMXFixtureComponent.h" },
		{ "ToolTip", "True if the component is attached to a matrix fixture" },
	};
#endif
	void Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData_SetBit(void* Obj)
	{
		((UDMXFixtureComponent*)Obj)->bUsingMatrixData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData = { "bUsingMatrixData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXFixtureComponent), &Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXFixtureComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_SkipThreshold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUseInterpolation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_InterpolationScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXFixtureComponent_Statics::NewProp_bUsingMatrixData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXFixtureComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXFixtureComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXFixtureComponent_Statics::ClassParams = {
		&UDMXFixtureComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXFixtureComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXFixtureComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXFixtureComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXFixtureComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXFixtureComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXFixtureComponent, 2865179142);
	template<> DMXFIXTURES_API UClass* StaticClass<UDMXFixtureComponent>()
	{
		return UDMXFixtureComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXFixtureComponent(Z_Construct_UClass_UDMXFixtureComponent, &UDMXFixtureComponent::StaticClass, TEXT("/Script/DMXFixtures"), TEXT("UDMXFixtureComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXFixtureComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
