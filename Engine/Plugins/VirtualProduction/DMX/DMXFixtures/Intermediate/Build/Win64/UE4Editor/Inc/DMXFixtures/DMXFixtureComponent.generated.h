// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture2D;
struct FLinearColor;
class ADMXFixtureActor;
#ifdef DMXFIXTURES_DMXFixtureComponent_generated_h
#error "DMXFixtureComponent.generated.h already included, missing '#pragma once' in DMXFixtureComponent.h"
#endif
#define DMXFIXTURES_DMXFixtureComponent_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXChannelData_Statics; \
	DMXFIXTURES_API static class UScriptStruct* StaticStruct();


template<> DMXFIXTURES_API UScriptStruct* StaticStruct<struct FDMXChannelData>();

#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetTextureCenterColors); \
	DECLARE_FUNCTION(execGetParentFixtureActor); \
	DECLARE_FUNCTION(execInitialize);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetTextureCenterColors); \
	DECLARE_FUNCTION(execGetParentFixtureActor); \
	DECLARE_FUNCTION(execInitialize);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_EVENT_PARMS \
	struct DMXFixtureComponent_eventInterpolateComponent_Parms \
	{ \
		float DeltaSeconds; \
	};


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_CALLBACK_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXFixtureComponent(); \
	friend struct Z_Construct_UClass_UDMXFixtureComponent_Statics; \
public: \
	DECLARE_CLASS(UDMXFixtureComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(UDMXFixtureComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUDMXFixtureComponent(); \
	friend struct Z_Construct_UClass_UDMXFixtureComponent_Statics; \
public: \
	DECLARE_CLASS(UDMXFixtureComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(UDMXFixtureComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXFixtureComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXFixtureComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXFixtureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXFixtureComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXFixtureComponent(UDMXFixtureComponent&&); \
	NO_API UDMXFixtureComponent(const UDMXFixtureComponent&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXFixtureComponent(UDMXFixtureComponent&&); \
	NO_API UDMXFixtureComponent(const UDMXFixtureComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXFixtureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXFixtureComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXFixtureComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_35_PROLOG \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_EVENT_PARMS


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXFIXTURES_API UClass* StaticClass<class UDMXFixtureComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
