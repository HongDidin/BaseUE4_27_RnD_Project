// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDMXNormalizedAttributeValueMap;
class UStaticMeshComponent;
#ifdef DMXFIXTURES_DMXFixtureActor_generated_h
#error "DMXFixtureActor.generated.h already included, missing '#pragma once' in DMXFixtureActor.h"
#endif
#define DMXFIXTURES_DMXFixtureActor_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInterpolateDMXComponents); \
	DECLARE_FUNCTION(execPushNormalizedValuesPerAttribute); \
	DECLARE_FUNCTION(execInitializeFixture);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInterpolateDMXComponents); \
	DECLARE_FUNCTION(execPushNormalizedValuesPerAttribute); \
	DECLARE_FUNCTION(execInitializeFixture);


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADMXFixtureActor(); \
	friend struct Z_Construct_UClass_ADMXFixtureActor_Statics; \
public: \
	DECLARE_CLASS(ADMXFixtureActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(ADMXFixtureActor)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_INCLASS \
private: \
	static void StaticRegisterNativesADMXFixtureActor(); \
	friend struct Z_Construct_UClass_ADMXFixtureActor_Statics; \
public: \
	DECLARE_CLASS(ADMXFixtureActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXFixtures"), NO_API) \
	DECLARE_SERIALIZER(ADMXFixtureActor)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADMXFixtureActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADMXFixtureActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADMXFixtureActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADMXFixtureActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADMXFixtureActor(ADMXFixtureActor&&); \
	NO_API ADMXFixtureActor(const ADMXFixtureActor&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADMXFixtureActor(ADMXFixtureActor&&); \
	NO_API ADMXFixtureActor(const ADMXFixtureActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADMXFixtureActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADMXFixtureActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADMXFixtureActor)


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_30_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXFIXTURES_API UClass* StaticClass<class ADMXFixtureActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXFixtures_Source_DMXFixtures_Public_DMXFixtureActor_h


#define FOREACH_ENUM_EDMXFIXTUREQUALITYLEVEL(op) \
	op(LowQuality) \
	op(MediumQuality) \
	op(HighQuality) \
	op(UltraQuality) \
	op(Custom) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
