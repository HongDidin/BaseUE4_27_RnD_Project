// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXFixtures/Public/DMXFixtureActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXFixtureActor() {}
// Cross Module References
	DMXFIXTURES_API UEnum* Z_Construct_UEnum_DMXFixtures_EDMXFixtureQualityLevel();
	UPackage* Z_Construct_UPackage__Script_DMXFixtures();
	DMXFIXTURES_API UClass* Z_Construct_UClass_ADMXFixtureActor_NoRegister();
	DMXFIXTURES_API UClass* Z_Construct_UClass_ADMXFixtureActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpotLightComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPointLightComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	static UEnum* EDMXFixtureQualityLevel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXFixtures_EDMXFixtureQualityLevel, Z_Construct_UPackage__Script_DMXFixtures(), TEXT("EDMXFixtureQualityLevel"));
		}
		return Singleton;
	}
	template<> DMXFIXTURES_API UEnum* StaticEnum<EDMXFixtureQualityLevel>()
	{
		return EDMXFixtureQualityLevel_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXFixtureQualityLevel(EDMXFixtureQualityLevel_StaticEnum, TEXT("/Script/DMXFixtures"), TEXT("EDMXFixtureQualityLevel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXFixtures_EDMXFixtureQualityLevel_Hash() { return 3189895861U; }
	UEnum* Z_Construct_UEnum_DMXFixtures_EDMXFixtureQualityLevel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXFixtures();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXFixtureQualityLevel"), 0, Get_Z_Construct_UEnum_DMXFixtures_EDMXFixtureQualityLevel_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "LowQuality", (int64)LowQuality },
				{ "MediumQuality", (int64)MediumQuality },
				{ "HighQuality", (int64)HighQuality },
				{ "UltraQuality", (int64)UltraQuality },
				{ "Custom", (int64)Custom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Custom.DisplayName", "Custom" },
				{ "Custom.Name", "Custom" },
				{ "HighQuality.DisplayName", "High" },
				{ "HighQuality.Name", "HighQuality" },
				{ "LowQuality.DisplayName", "Low" },
				{ "LowQuality.Name", "LowQuality" },
				{ "MediumQuality.DisplayName", "Medium" },
				{ "MediumQuality.Name", "MediumQuality" },
				{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
				{ "UltraQuality.DisplayName", "Ultra" },
				{ "UltraQuality.Name", "UltraQuality" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXFixtures,
				nullptr,
				"EDMXFixtureQualityLevel",
				"EDMXFixtureQualityLevel",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(ADMXFixtureActor::execInterpolateDMXComponents)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaSeconds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InterpolateDMXComponents(Z_Param_DeltaSeconds);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ADMXFixtureActor::execPushNormalizedValuesPerAttribute)
	{
		P_GET_STRUCT_REF(FDMXNormalizedAttributeValueMap,Z_Param_Out_ValuePerAttributeMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PushNormalizedValuesPerAttribute(Z_Param_Out_ValuePerAttributeMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ADMXFixtureActor::execInitializeFixture)
	{
		P_GET_OBJECT(UStaticMeshComponent,Z_Param_StaticMeshLens);
		P_GET_OBJECT(UStaticMeshComponent,Z_Param_StaticMeshBeam);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitializeFixture(Z_Param_StaticMeshLens,Z_Param_StaticMeshBeam);
		P_NATIVE_END;
	}
	void ADMXFixtureActor::StaticRegisterNativesADMXFixtureActor()
	{
		UClass* Class = ADMXFixtureActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InitializeFixture", &ADMXFixtureActor::execInitializeFixture },
			{ "InterpolateDMXComponents", &ADMXFixtureActor::execInterpolateDMXComponents },
			{ "PushNormalizedValuesPerAttribute", &ADMXFixtureActor::execPushNormalizedValuesPerAttribute },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics
	{
		struct DMXFixtureActor_eventInitializeFixture_Parms
		{
			UStaticMeshComponent* StaticMeshLens;
			UStaticMeshComponent* StaticMeshBeam;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshLens_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshLens;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshBeam_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshBeam;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshLens_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshLens = { "StaticMeshLens", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureActor_eventInitializeFixture_Parms, StaticMeshLens), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshLens_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshLens_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshBeam_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshBeam = { "StaticMeshBeam", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureActor_eventInitializeFixture_Parms, StaticMeshBeam), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshBeam_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshBeam_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshLens,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::NewProp_StaticMeshBeam,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX Fixture" },
		{ "Comment", "// FUNCTIONS---------------------------------\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "FUNCTIONS---------------------------------" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADMXFixtureActor, nullptr, "InitializeFixture", nullptr, nullptr, sizeof(DMXFixtureActor_eventInitializeFixture_Parms), Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics
	{
		struct DMXFixtureActor_eventInterpolateDMXComponents_Parms
		{
			float DeltaSeconds;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::NewProp_DeltaSeconds = { "DeltaSeconds", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureActor_eventInterpolateDMXComponents_Parms, DeltaSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::NewProp_DeltaSeconds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADMXFixtureActor, nullptr, "InterpolateDMXComponents", nullptr, nullptr, sizeof(DMXFixtureActor_eventInterpolateDMXComponents_Parms), Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics
	{
		struct DMXFixtureActor_eventPushNormalizedValuesPerAttribute_Parms
		{
			FDMXNormalizedAttributeValueMap ValuePerAttributeMap;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValuePerAttributeMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ValuePerAttributeMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::NewProp_ValuePerAttributeMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::NewProp_ValuePerAttributeMap = { "ValuePerAttributeMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXFixtureActor_eventPushNormalizedValuesPerAttribute_Parms, ValuePerAttributeMap), Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::NewProp_ValuePerAttributeMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::NewProp_ValuePerAttributeMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::NewProp_ValuePerAttributeMap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX Fixture" },
		{ "Comment", "/** Pushes DMX Values to the Fixture. Expects normalized values in the range of 0.0f - 1.0f */" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Pushes DMX Values to the Fixture. Expects normalized values in the range of 0.0f - 1.0f" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADMXFixtureActor, nullptr, "PushNormalizedValuesPerAttribute", nullptr, nullptr, sizeof(DMXFixtureActor_eventPushNormalizedValuesPerAttribute_Parms), Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADMXFixtureActor_NoRegister()
	{
		return ADMXFixtureActor::StaticClass();
	}
	struct Z_Construct_UClass_ADMXFixtureActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_QualityLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_QualityLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinQuality;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxQuality_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxQuality;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Base_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Base;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Yoke_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Yoke;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Head_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Head;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LightIntensityMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LightIntensityMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LightDistanceMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LightDistanceMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LightColorTemp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LightColorTemp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpotlightIntensityScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpotlightIntensityScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointlightIntensityScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PointlightIntensityScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LightCastShadow_MetaData[];
#endif
		static void NewProp_LightCastShadow_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_LightCastShadow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UseDynamicOcclusion_MetaData[];
#endif
		static void NewProp_UseDynamicOcclusion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UseDynamicOcclusion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMX;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StaticMeshComponents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpotLight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpotLight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointLight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointLight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OcclusionDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OcclusionDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LensMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeamMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BeamMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpotLightMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpotLightMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointLightMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointLightMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMaterialLens_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMaterialLens;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMaterialBeam_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMaterialBeam;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMaterialSpotLight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMaterialSpotLight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DynamicMaterialPointLight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DynamicMaterialPointLight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADMXFixtureActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXFixtures,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADMXFixtureActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ADMXFixtureActor_InitializeFixture, "InitializeFixture" }, // 1167277700
		{ &Z_Construct_UFunction_ADMXFixtureActor_InterpolateDMXComponents, "InterpolateDMXComponents" }, // 2068823791
		{ &Z_Construct_UFunction_ADMXFixtureActor_PushNormalizedValuesPerAttribute, "PushNormalizedValuesPerAttribute" }, // 2845168603
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DMXFixtureActor.h" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_QualityLevel_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Visual quality level that changes the number of samples in the volumetric beam\n" },
		{ "DisplayPriority", "0" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Visual quality level that changes the number of samples in the volumetric beam" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_QualityLevel = { "QualityLevel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, QualityLevel), Z_Construct_UEnum_DMXFixtures_EDMXFixtureQualityLevel, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_QualityLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_QualityLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MinQuality_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Visual quality when using smaller zoom angle (thin beam). Small value is visually better but cost more on GPU\n" },
		{ "EditCondition", "QualityLevel == EDMXFixtureQualityLevel::Custom" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Visual quality when using smaller zoom angle (thin beam). Small value is visually better but cost more on GPU" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MinQuality = { "MinQuality", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, MinQuality), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MinQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MinQuality_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MaxQuality_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Visual quality when using bigger zoom angle (wide beam). Small value is visually better but cost more on GPU\n" },
		{ "EditCondition", "QualityLevel == EDMXFixtureQualityLevel::Custom" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Visual quality when using bigger zoom angle (wide beam). Small value is visually better but cost more on GPU" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MaxQuality = { "MaxQuality", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, MaxQuality), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MaxQuality_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MaxQuality_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Base_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// HIERARCHY---------------------------------\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "HIERARCHY---------------------------------" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Base = { "Base", nullptr, (EPropertyFlags)0x001000000009001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, Base), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Base_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Base_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Yoke_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Yoke = { "Yoke", nullptr, (EPropertyFlags)0x001000000009001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, Yoke), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Yoke_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Yoke_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Head_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Head = { "Head", nullptr, (EPropertyFlags)0x001000000009001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, Head), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Head_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Head_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightIntensityMax_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Light intensity at 1 steradian (32.77deg half angle)\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Light intensity at 1 steradian (32.77deg half angle)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightIntensityMax = { "LightIntensityMax", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, LightIntensityMax), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightIntensityMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightIntensityMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightDistanceMax_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Sets Attenuation Radius on the spotlight and pointlight\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Sets Attenuation Radius on the spotlight and pointlight" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightDistanceMax = { "LightDistanceMax", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, LightDistanceMax), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightDistanceMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightDistanceMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightColorTemp_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Light color temperature on the spotlight and pointlight\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Light color temperature on the spotlight and pointlight" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightColorTemp = { "LightColorTemp", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, LightColorTemp), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightColorTemp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightColorTemp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotlightIntensityScale_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Scales spotlight intensity\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Scales spotlight intensity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotlightIntensityScale = { "SpotlightIntensityScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, SpotlightIntensityScale), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotlightIntensityScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotlightIntensityScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointlightIntensityScale_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Scales pointlight intensity\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Scales pointlight intensity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointlightIntensityScale = { "PointlightIntensityScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, PointlightIntensityScale), METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointlightIntensityScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointlightIntensityScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Enable/disable cast shadow on the spotlight and pointlight\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Enable/disable cast shadow on the spotlight and pointlight" },
	};
#endif
	void Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow_SetBit(void* Obj)
	{
		((ADMXFixtureActor*)Obj)->LightCastShadow = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow = { "LightCastShadow", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADMXFixtureActor), &Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// Simple solution useful for walls, 1 linetrace from the center\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "Simple solution useful for walls, 1 linetrace from the center" },
	};
#endif
	void Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion_SetBit(void* Obj)
	{
		((ADMXFixtureActor*)Obj)->UseDynamicOcclusion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion = { "UseDynamicOcclusion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADMXFixtureActor), &Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DMX_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// DMX COMPONENT -----------------------------\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "DMX COMPONENT -----------------------------" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DMX = { "DMX", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, DMX), Z_Construct_UClass_UDMXComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DMX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DMX_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents_Inner = { "StaticMeshComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// COMPONENTS ---------------------------------\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "COMPONENTS ---------------------------------" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents = { "StaticMeshComponents", nullptr, (EPropertyFlags)0x001000800000001c, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, StaticMeshComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLight_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLight = { "SpotLight", nullptr, (EPropertyFlags)0x001000000009001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, SpotLight), Z_Construct_UClass_USpotLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLight_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLight = { "PointLight", nullptr, (EPropertyFlags)0x001000000009001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, PointLight), Z_Construct_UClass_UPointLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_OcclusionDirection_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_OcclusionDirection = { "OcclusionDirection", nullptr, (EPropertyFlags)0x001000000009001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, OcclusionDirection), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_OcclusionDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_OcclusionDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LensMaterialInstance_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "Comment", "// MATERIALS ---------------------------------\n" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
		{ "ToolTip", "MATERIALS ---------------------------------" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LensMaterialInstance = { "LensMaterialInstance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, LensMaterialInstance), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LensMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LensMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_BeamMaterialInstance_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_BeamMaterialInstance = { "BeamMaterialInstance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, BeamMaterialInstance), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_BeamMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_BeamMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLightMaterialInstance_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLightMaterialInstance = { "SpotLightMaterialInstance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, SpotLightMaterialInstance), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLightMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLightMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLightMaterialInstance_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLightMaterialInstance = { "PointLightMaterialInstance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, PointLightMaterialInstance), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLightMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLightMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialLens_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialLens = { "DynamicMaterialLens", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, DynamicMaterialLens), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialLens_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialLens_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialBeam_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialBeam = { "DynamicMaterialBeam", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, DynamicMaterialBeam), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialBeam_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialBeam_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialSpotLight_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialSpotLight = { "DynamicMaterialSpotLight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, DynamicMaterialSpotLight), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialSpotLight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialSpotLight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialPointLight_MetaData[] = {
		{ "Category", "DMX Light Fixture" },
		{ "ModuleRelativePath", "Public/DMXFixtureActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialPointLight = { "DynamicMaterialPointLight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADMXFixtureActor, DynamicMaterialPointLight), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialPointLight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialPointLight_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADMXFixtureActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_QualityLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MinQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_MaxQuality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Base,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Yoke,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_Head,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightIntensityMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightDistanceMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightColorTemp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotlightIntensityScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointlightIntensityScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LightCastShadow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_UseDynamicOcclusion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DMX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_StaticMeshComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_OcclusionDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_LensMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_BeamMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_SpotLightMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_PointLightMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialLens,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialBeam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialSpotLight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADMXFixtureActor_Statics::NewProp_DynamicMaterialPointLight,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADMXFixtureActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADMXFixtureActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADMXFixtureActor_Statics::ClassParams = {
		&ADMXFixtureActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ADMXFixtureActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADMXFixtureActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADMXFixtureActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADMXFixtureActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADMXFixtureActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADMXFixtureActor, 550184701);
	template<> DMXFIXTURES_API UClass* StaticClass<ADMXFixtureActor>()
	{
		return ADMXFixtureActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADMXFixtureActor(Z_Construct_UClass_ADMXFixtureActor, &ADMXFixtureActor::StaticClass, TEXT("/Script/DMXFixtures"), TEXT("ADMXFixtureActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADMXFixtureActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
