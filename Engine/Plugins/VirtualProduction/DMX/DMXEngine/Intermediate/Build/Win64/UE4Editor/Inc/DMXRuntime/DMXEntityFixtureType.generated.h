// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDMXImport;
#ifdef DMXRUNTIME_DMXEntityFixtureType_generated_h
#error "DMXEntityFixtureType.generated.h already included, missing '#pragma once' in DMXEntityFixtureType.h"
#endif
#define DMXRUNTIME_DMXEntityFixtureType_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_216_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXFixtureMode_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXFixtureMode>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_197_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXCell_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXCell>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_166_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXFixtureMatrix>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_113_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXFixtureCellAttribute>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_41_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXFixtureFunction>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_RPC_WRAPPERS_NO_PURE_DECLS
#if WITH_EDITOR
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_EDITOR_ONLY_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetModesFromDMXImport);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetModesFromDMXImport);


#else
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_EDITOR_ONLY_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS
#endif //WITH_EDITOR
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXEntityFixtureType(); \
	friend struct Z_Construct_UClass_UDMXEntityFixtureType_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityFixtureType, UDMXEntity, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityFixtureType)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_INCLASS \
private: \
	static void StaticRegisterNativesUDMXEntityFixtureType(); \
	friend struct Z_Construct_UClass_UDMXEntityFixtureType_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityFixtureType, UDMXEntity, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityFixtureType)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityFixtureType(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEntityFixtureType) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityFixtureType); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityFixtureType); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityFixtureType(UDMXEntityFixtureType&&); \
	NO_API UDMXEntityFixtureType(const UDMXEntityFixtureType&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityFixtureType(UDMXEntityFixtureType&&); \
	NO_API UDMXEntityFixtureType(const UDMXEntityFixtureType&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityFixtureType); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityFixtureType); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXEntityFixtureType)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_258_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_EDITOR_ONLY_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h_262_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXEntityFixtureType>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixtureType_h


#define FOREACH_ENUM_EDMXPIXELMAPPINGDISTRIBUTION(op) \
	op(EDMXPixelMappingDistribution::TopLeftToRight) \
	op(EDMXPixelMappingDistribution::TopLeftToBottom) \
	op(EDMXPixelMappingDistribution::TopLeftToClockwise) \
	op(EDMXPixelMappingDistribution::TopLeftToAntiClockwise) \
	op(EDMXPixelMappingDistribution::TopRightToLeft) \
	op(EDMXPixelMappingDistribution::BottomLeftToTop) \
	op(EDMXPixelMappingDistribution::TopRightToAntiClockwise) \
	op(EDMXPixelMappingDistribution::BottomLeftToClockwise) \
	op(EDMXPixelMappingDistribution::BottomLeftToRight) \
	op(EDMXPixelMappingDistribution::TopRightToBottom) \
	op(EDMXPixelMappingDistribution::BottomLeftAntiClockwise) \
	op(EDMXPixelMappingDistribution::TopRightToClockwise) \
	op(EDMXPixelMappingDistribution::BottomRightToLeft) \
	op(EDMXPixelMappingDistribution::BottomRightToTop) \
	op(EDMXPixelMappingDistribution::BottomRightToClockwise) \
	op(EDMXPixelMappingDistribution::BottomRightToAntiClockwise) 

enum class EDMXPixelMappingDistribution : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXPixelMappingDistribution>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
