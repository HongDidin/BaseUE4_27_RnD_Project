// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDMXProtocolName;
struct FAssetData;
enum class EDMXPixelMappingDistribution : uint8;
class UDMXEntityFixturePatch;
struct FDMXCell;
struct FIntPoint;
struct FDMXAttributeName;
struct FDMXFixtureMatrix;
struct FDMXAttributeName; 
class UDMXSubsystem;
struct FDMXEntityFixturePatchRef;
enum class EDMXFixtureSignalFormat : uint8;
class UDMXLibrary;
class UDMXEntityController;
class UDMXEntityFixtureType;
struct FDMXFixtureCategory;
struct FDMXEntityFixtureTypeRef;
struct FDMXOutputPortReference;
struct FDMXInputPortReference;
 
enum class EDMXSendResult : uint8;
#ifdef DMXRUNTIME_DMXSubsystem_generated_h
#error "DMXSubsystem.generated.h already included, missing '#pragma once' in DMXSubsystem.h"
#endif
#define DMXRUNTIME_DMXSubsystem_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_22_DELEGATE \
struct _Script_DMXRuntime_eventProtocolReceivedDelegate_Parms \
{ \
	FDMXProtocolName Protocol; \
	int32 RemoteUniverse; \
	TArray<uint8> DMXBuffer; \
}; \
static inline void FProtocolReceivedDelegate_DelegateWrapper(const FMulticastScriptDelegate& ProtocolReceivedDelegate, FDMXProtocolName Protocol, int32 RemoteUniverse, TArray<uint8> const& DMXBuffer) \
{ \
	_Script_DMXRuntime_eventProtocolReceivedDelegate_Parms Parms; \
	Parms.Protocol=Protocol; \
	Parms.RemoteUniverse=RemoteUniverse; \
	Parms.DMXBuffer=DMXBuffer; \
	ProtocolReceivedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPixelMappingDistributionSort); \
	DECLARE_FUNCTION(execGetAllMatrixCells); \
	DECLARE_FUNCTION(execGetMatrixCell); \
	DECLARE_FUNCTION(execGetCellAttributes); \
	DECLARE_FUNCTION(execGetMatrixProperties); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsAbsolute); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsRelative); \
	DECLARE_FUNCTION(execGetMatrixCellValue); \
	DECLARE_FUNCTION(execSetMatrixCellValue); \
	DECLARE_FUNCTION(execGetAttributeLabel); \
	DECLARE_FUNCTION(execGetDMXSubsystem_Callable); \
	DECLARE_FUNCTION(execGetDMXSubsystem_Pure); \
	DECLARE_FUNCTION(execPatchIsOfSelectedType); \
	DECLARE_FUNCTION(execGetFunctionsValue); \
	DECLARE_FUNCTION(execGetFunctionsMapForPatch); \
	DECLARE_FUNCTION(execGetFunctionsMap); \
	DECLARE_FUNCTION(execGetFixturePatch); \
	DECLARE_FUNCTION(execGetNormalizedAttributeValue); \
	DECLARE_FUNCTION(execIntToNormalizedValue); \
	DECLARE_FUNCTION(execIntValueToBytes); \
	DECLARE_FUNCTION(execNormalizedValueToBytes); \
	DECLARE_FUNCTION(execBytesToNormalizedValue); \
	DECLARE_FUNCTION(execBytesToInt); \
	DECLARE_FUNCTION(execGetAllDMXLibraries); \
	DECLARE_FUNCTION(execGetControllerByName); \
	DECLARE_FUNCTION(execGetAllControllersInLibrary); \
	DECLARE_FUNCTION(execGetAllUniversesInController); \
	DECLARE_FUNCTION(execGetFixtureTypeByName); \
	DECLARE_FUNCTION(execGetAllFixtureTypesInLibrary); \
	DECLARE_FUNCTION(execGetFixtureByName); \
	DECLARE_FUNCTION(execGetAllFixturesInLibrary); \
	DECLARE_FUNCTION(execGetAllFixturesWithTag); \
	DECLARE_FUNCTION(execGetFixtureAttributes); \
	DECLARE_FUNCTION(execGetAllFixturesInUniverse); \
	DECLARE_FUNCTION(execGetAllFixturesOfCategory); \
	DECLARE_FUNCTION(execGetAllFixturesOfType); \
	DECLARE_FUNCTION(execGetDMXDataFromOutputPort); \
	DECLARE_FUNCTION(execGetDMXDataFromInputPort); \
	DECLARE_FUNCTION(execGetRawBuffer); \
	DECLARE_FUNCTION(execSendDMXToOutputPort); \
	DECLARE_FUNCTION(execSendDMXRaw); \
	DECLARE_FUNCTION(execSendDMX);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPixelMappingDistributionSort); \
	DECLARE_FUNCTION(execGetAllMatrixCells); \
	DECLARE_FUNCTION(execGetMatrixCell); \
	DECLARE_FUNCTION(execGetCellAttributes); \
	DECLARE_FUNCTION(execGetMatrixProperties); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsAbsolute); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsRelative); \
	DECLARE_FUNCTION(execGetMatrixCellValue); \
	DECLARE_FUNCTION(execSetMatrixCellValue); \
	DECLARE_FUNCTION(execGetAttributeLabel); \
	DECLARE_FUNCTION(execGetDMXSubsystem_Callable); \
	DECLARE_FUNCTION(execGetDMXSubsystem_Pure); \
	DECLARE_FUNCTION(execPatchIsOfSelectedType); \
	DECLARE_FUNCTION(execGetFunctionsValue); \
	DECLARE_FUNCTION(execGetFunctionsMapForPatch); \
	DECLARE_FUNCTION(execGetFunctionsMap); \
	DECLARE_FUNCTION(execGetFixturePatch); \
	DECLARE_FUNCTION(execGetNormalizedAttributeValue); \
	DECLARE_FUNCTION(execIntToNormalizedValue); \
	DECLARE_FUNCTION(execIntValueToBytes); \
	DECLARE_FUNCTION(execNormalizedValueToBytes); \
	DECLARE_FUNCTION(execBytesToNormalizedValue); \
	DECLARE_FUNCTION(execBytesToInt); \
	DECLARE_FUNCTION(execGetAllDMXLibraries); \
	DECLARE_FUNCTION(execGetControllerByName); \
	DECLARE_FUNCTION(execGetAllControllersInLibrary); \
	DECLARE_FUNCTION(execGetAllUniversesInController); \
	DECLARE_FUNCTION(execGetFixtureTypeByName); \
	DECLARE_FUNCTION(execGetAllFixtureTypesInLibrary); \
	DECLARE_FUNCTION(execGetFixtureByName); \
	DECLARE_FUNCTION(execGetAllFixturesInLibrary); \
	DECLARE_FUNCTION(execGetAllFixturesWithTag); \
	DECLARE_FUNCTION(execGetFixtureAttributes); \
	DECLARE_FUNCTION(execGetAllFixturesInUniverse); \
	DECLARE_FUNCTION(execGetAllFixturesOfCategory); \
	DECLARE_FUNCTION(execGetAllFixturesOfType); \
	DECLARE_FUNCTION(execGetDMXDataFromOutputPort); \
	DECLARE_FUNCTION(execGetDMXDataFromInputPort); \
	DECLARE_FUNCTION(execGetRawBuffer); \
	DECLARE_FUNCTION(execSendDMXToOutputPort); \
	DECLARE_FUNCTION(execSendDMXRaw); \
	DECLARE_FUNCTION(execSendDMX);


#if WITH_EDITOR
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_EDITOR_ONLY_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnAssetRegistryRemovedAsset); \
	DECLARE_FUNCTION(execOnAssetRegistryAddedAsset);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnAssetRegistryRemovedAsset); \
	DECLARE_FUNCTION(execOnAssetRegistryAddedAsset);


#else
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_EDITOR_ONLY_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS
#endif //WITH_EDITOR
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXSubsystem(); \
	friend struct Z_Construct_UClass_UDMXSubsystem_Statics; \
public: \
	DECLARE_CLASS(UDMXSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXSubsystem)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_INCLASS \
private: \
	static void StaticRegisterNativesUDMXSubsystem(); \
	friend struct Z_Construct_UClass_UDMXSubsystem_Statics; \
public: \
	DECLARE_CLASS(UDMXSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXSubsystem)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXSubsystem(UDMXSubsystem&&); \
	NO_API UDMXSubsystem(const UDMXSubsystem&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXSubsystem(UDMXSubsystem&&); \
	NO_API UDMXSubsystem(const UDMXSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXSubsystem)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LoadedDMXLibraries() { return STRUCT_OFFSET(UDMXSubsystem, LoadedDMXLibraries); }


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_30_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_EDITOR_ONLY_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_EDITOR_ONLY_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h_34_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
