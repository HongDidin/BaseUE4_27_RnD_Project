// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXEditor/Private/Sequencer/MovieSceneDMXLibraryTrackRecorder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneDMXLibraryTrackRecorder() {}
// Cross Module References
	DMXEDITOR_API UClass* Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_NoRegister();
	DMXEDITOR_API UClass* Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder();
	TAKETRACKRECORDERS_API UClass* Z_Construct_UClass_UMovieSceneTrackRecorder();
	UPackage* Z_Construct_UPackage__Script_DMXEditor();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
// End Cross Module References
	void UMovieSceneDMXLibraryTrackRecorder::StaticRegisterNativesUMovieSceneDMXLibraryTrackRecorder()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_NoRegister()
	{
		return UMovieSceneDMXLibraryTrackRecorder::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchRefs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchRefs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FixturePatchRefs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneTrackRecorder,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* Track recorder implementation for DMX libraries\n* Reuses logic of Animation/LiveLink Plugin in many areas.\n*/" },
		{ "IncludePath", "Sequencer/MovieSceneDMXLibraryTrackRecorder.h" },
		{ "ModuleRelativePath", "Private/Sequencer/MovieSceneDMXLibraryTrackRecorder.h" },
		{ "ToolTip", "Track recorder implementation for DMX libraries\nReuses logic of Animation/LiveLink Plugin in many areas." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs_Inner = { "FixturePatchRefs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs_MetaData[] = {
		{ "ModuleRelativePath", "Private/Sequencer/MovieSceneDMXLibraryTrackRecorder.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs = { "FixturePatchRefs", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneDMXLibraryTrackRecorder, FixturePatchRefs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::NewProp_FixturePatchRefs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneDMXLibraryTrackRecorder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::ClassParams = {
		&UMovieSceneDMXLibraryTrackRecorder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneDMXLibraryTrackRecorder, 2048123125);
	template<> DMXEDITOR_API UClass* StaticClass<UMovieSceneDMXLibraryTrackRecorder>()
	{
		return UMovieSceneDMXLibraryTrackRecorder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneDMXLibraryTrackRecorder(Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder, &UMovieSceneDMXLibraryTrackRecorder::StaticClass, TEXT("/Script/DMXEditor"), TEXT("UMovieSceneDMXLibraryTrackRecorder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneDMXLibraryTrackRecorder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
