// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDMXEntityFixturePatch;
struct FDMXNormalizedAttributeValueMap;
struct FDMXCell;
struct FIntPoint;
struct FDMXAttributeName;
struct FDMXFixtureMatrix;
struct FDMXAttributeName; 
class UDMXEntityController;
 
struct FDMXAttributeName; enum class EDMXFixtureSignalFormat : uint8;
struct FDMXAttributeName; struct FDMXFixtureFunction;
#ifdef DMXRUNTIME_DMXEntityFixturePatch_generated_h
#error "DMXEntityFixturePatch.generated.h already included, missing '#pragma once' in DMXEntityFixturePatch.h"
#endif
#define DMXRUNTIME_DMXEntityFixturePatch_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_38_DELEGATE \
struct DMXEntityFixturePatch_eventDMXOnFixturePatchReceivedDMXDelegate_Parms \
{ \
	UDMXEntityFixturePatch* FixturePatch; \
	FDMXNormalizedAttributeValueMap ValuePerAttribute; \
}; \
static inline void FDMXOnFixturePatchReceivedDMXDelegate_DelegateWrapper(const FMulticastScriptDelegate& DMXOnFixturePatchReceivedDMXDelegate, UDMXEntityFixturePatch* FixturePatch, FDMXNormalizedAttributeValueMap const& ValuePerAttribute) \
{ \
	DMXEntityFixturePatch_eventDMXOnFixturePatchReceivedDMXDelegate_Parms Parms; \
	Parms.FixturePatch=FixturePatch; \
	Parms.ValuePerAttribute=ValuePerAttribute; \
	DMXOnFixturePatchReceivedDMXDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAllMatrixCells); \
	DECLARE_FUNCTION(execGetMatrixCell); \
	DECLARE_FUNCTION(execGetCellAttributes); \
	DECLARE_FUNCTION(execGetMatrixProperties); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsAbsoluteWithValidation); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsAbsolute); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsRelative); \
	DECLARE_FUNCTION(execGetNormalizedMatrixCellValues); \
	DECLARE_FUNCTION(execGetMatrixCellValues); \
	DECLARE_FUNCTION(execSendNormalizedMatrixCellValue); \
	DECLARE_FUNCTION(execSendMatrixCellValueWithAttributeMap); \
	DECLARE_FUNCTION(execSendMatrixCellValue); \
	DECLARE_FUNCTION(execGetNormalizedAttributesValues); \
	DECLARE_FUNCTION(execGetAttributesValues); \
	DECLARE_FUNCTION(execGetNormalizedAttributeValue); \
	DECLARE_FUNCTION(execGetAttributeValue); \
	DECLARE_FUNCTION(execIsInControllersRange); \
	DECLARE_FUNCTION(execIsInControllerRange); \
	DECLARE_FUNCTION(execGetRelevantControllers); \
	DECLARE_FUNCTION(execConvertToValidMap); \
	DECLARE_FUNCTION(execContainsAttribute); \
	DECLARE_FUNCTION(execIsMapValid); \
	DECLARE_FUNCTION(execConvertAttributeMapToRawMap); \
	DECLARE_FUNCTION(execConvertRawMapToAttributeMap); \
	DECLARE_FUNCTION(execGetAttributeSignalFormats); \
	DECLARE_FUNCTION(execGetAttributeChannelAssignments); \
	DECLARE_FUNCTION(execGetAttributeDefaultMap); \
	DECLARE_FUNCTION(execGetAttributeFunctionsMap); \
	DECLARE_FUNCTION(execGetAllAttributesInActiveMode); \
	DECLARE_FUNCTION(execGetRemoteUniverse); \
	DECLARE_FUNCTION(execGetEndingChannel); \
	DECLARE_FUNCTION(execGetChannelSpan); \
	DECLARE_FUNCTION(execGetStartingChannel); \
	DECLARE_FUNCTION(execSendDMX);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAllMatrixCells); \
	DECLARE_FUNCTION(execGetMatrixCell); \
	DECLARE_FUNCTION(execGetCellAttributes); \
	DECLARE_FUNCTION(execGetMatrixProperties); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsAbsoluteWithValidation); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsAbsolute); \
	DECLARE_FUNCTION(execGetMatrixCellChannelsRelative); \
	DECLARE_FUNCTION(execGetNormalizedMatrixCellValues); \
	DECLARE_FUNCTION(execGetMatrixCellValues); \
	DECLARE_FUNCTION(execSendNormalizedMatrixCellValue); \
	DECLARE_FUNCTION(execSendMatrixCellValueWithAttributeMap); \
	DECLARE_FUNCTION(execSendMatrixCellValue); \
	DECLARE_FUNCTION(execGetNormalizedAttributesValues); \
	DECLARE_FUNCTION(execGetAttributesValues); \
	DECLARE_FUNCTION(execGetNormalizedAttributeValue); \
	DECLARE_FUNCTION(execGetAttributeValue); \
	DECLARE_FUNCTION(execIsInControllersRange); \
	DECLARE_FUNCTION(execIsInControllerRange); \
	DECLARE_FUNCTION(execGetRelevantControllers); \
	DECLARE_FUNCTION(execConvertToValidMap); \
	DECLARE_FUNCTION(execContainsAttribute); \
	DECLARE_FUNCTION(execIsMapValid); \
	DECLARE_FUNCTION(execConvertAttributeMapToRawMap); \
	DECLARE_FUNCTION(execConvertRawMapToAttributeMap); \
	DECLARE_FUNCTION(execGetAttributeSignalFormats); \
	DECLARE_FUNCTION(execGetAttributeChannelAssignments); \
	DECLARE_FUNCTION(execGetAttributeDefaultMap); \
	DECLARE_FUNCTION(execGetAttributeFunctionsMap); \
	DECLARE_FUNCTION(execGetAllAttributesInActiveMode); \
	DECLARE_FUNCTION(execGetRemoteUniverse); \
	DECLARE_FUNCTION(execGetEndingChannel); \
	DECLARE_FUNCTION(execGetChannelSpan); \
	DECLARE_FUNCTION(execGetStartingChannel); \
	DECLARE_FUNCTION(execSendDMX);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXEntityFixturePatch(); \
	friend struct Z_Construct_UClass_UDMXEntityFixturePatch_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityFixturePatch, UDMXEntity, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityFixturePatch)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUDMXEntityFixturePatch(); \
	friend struct Z_Construct_UClass_UDMXEntityFixturePatch_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityFixturePatch, UDMXEntity, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityFixturePatch)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityFixturePatch(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEntityFixturePatch) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityFixturePatch); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityFixturePatch); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityFixturePatch(UDMXEntityFixturePatch&&); \
	NO_API UDMXEntityFixturePatch(const UDMXEntityFixturePatch&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityFixturePatch(UDMXEntityFixturePatch&&); \
	NO_API UDMXEntityFixturePatch(const UDMXEntityFixturePatch&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityFixturePatch); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityFixturePatch); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXEntityFixturePatch)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__UniverseID() { return STRUCT_OFFSET(UDMXEntityFixturePatch, UniverseID); } \
	FORCEINLINE static uint32 __PPO__bAutoAssignAddress() { return STRUCT_OFFSET(UDMXEntityFixturePatch, bAutoAssignAddress); } \
	FORCEINLINE static uint32 __PPO__ManualStartingAddress() { return STRUCT_OFFSET(UDMXEntityFixturePatch, ManualStartingAddress); } \
	FORCEINLINE static uint32 __PPO__AutoStartingAddress() { return STRUCT_OFFSET(UDMXEntityFixturePatch, AutoStartingAddress); } \
	FORCEINLINE static uint32 __PPO__ParentFixtureTypeTemplate() { return STRUCT_OFFSET(UDMXEntityFixturePatch, ParentFixtureTypeTemplate); } \
	FORCEINLINE static uint32 __PPO__ActiveMode() { return STRUCT_OFFSET(UDMXEntityFixturePatch, ActiveMode); }


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_31_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXEntityFixturePatch>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityFixturePatch_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
