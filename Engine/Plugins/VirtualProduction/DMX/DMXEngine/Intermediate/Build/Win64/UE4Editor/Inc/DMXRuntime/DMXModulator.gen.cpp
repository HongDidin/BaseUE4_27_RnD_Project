// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Modulators/DMXModulator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXModulator() {}
// Cross Module References
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap();
// End Cross Module References
	DEFINE_FUNCTION(UDMXModulator::execModulateMatrix)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_FixturePatch);
		P_GET_TARRAY_REF(FDMXNormalizedAttributeValueMap,Z_Param_Out_InNormalizedMatrixAttributeValues);
		P_GET_TARRAY_REF(FDMXNormalizedAttributeValueMap,Z_Param_Out_OutNormalizedMatrixAttributeValues);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ModulateMatrix_Implementation(Z_Param_FixturePatch,Z_Param_Out_InNormalizedMatrixAttributeValues,Z_Param_Out_OutNormalizedMatrixAttributeValues);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXModulator::execModulate)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_FixturePatch);
		P_GET_TMAP_REF(FDMXAttributeName,float,Z_Param_Out_InNormalizedAttributeValues);
		P_GET_TMAP_REF(FDMXAttributeName,float,Z_Param_Out_OutNormalizedAttributeValues);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Modulate_Implementation(Z_Param_FixturePatch,Z_Param_Out_InNormalizedAttributeValues,Z_Param_Out_OutNormalizedAttributeValues);
		P_NATIVE_END;
	}
	static FName NAME_UDMXModulator_Modulate = FName(TEXT("Modulate"));
	void UDMXModulator::Modulate(UDMXEntityFixturePatch* FixturePatch, TMap<FDMXAttributeName,float> const& InNormalizedAttributeValues, TMap<FDMXAttributeName,float>& OutNormalizedAttributeValues)
	{
		DMXModulator_eventModulate_Parms Parms;
		Parms.FixturePatch=FixturePatch;
		Parms.InNormalizedAttributeValues=InNormalizedAttributeValues;
		Parms.OutNormalizedAttributeValues=OutNormalizedAttributeValues;
		ProcessEvent(FindFunctionChecked(NAME_UDMXModulator_Modulate),&Parms);
		OutNormalizedAttributeValues=Parms.OutNormalizedAttributeValues;
	}
	static FName NAME_UDMXModulator_ModulateMatrix = FName(TEXT("ModulateMatrix"));
	void UDMXModulator::ModulateMatrix(UDMXEntityFixturePatch* FixturePatch, TArray<FDMXNormalizedAttributeValueMap> const& InNormalizedMatrixAttributeValues, TArray<FDMXNormalizedAttributeValueMap>& OutNormalizedMatrixAttributeValues)
	{
		DMXModulator_eventModulateMatrix_Parms Parms;
		Parms.FixturePatch=FixturePatch;
		Parms.InNormalizedMatrixAttributeValues=InNormalizedMatrixAttributeValues;
		Parms.OutNormalizedMatrixAttributeValues=OutNormalizedMatrixAttributeValues;
		ProcessEvent(FindFunctionChecked(NAME_UDMXModulator_ModulateMatrix),&Parms);
		OutNormalizedMatrixAttributeValues=Parms.OutNormalizedMatrixAttributeValues;
	}
	void UDMXModulator::StaticRegisterNativesUDMXModulator()
	{
		UClass* Class = UDMXModulator::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Modulate", &UDMXModulator::execModulate },
			{ "ModulateMatrix", &UDMXModulator::execModulateMatrix },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXModulator_Modulate_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FixturePatch;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InNormalizedAttributeValues_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InNormalizedAttributeValues_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNormalizedAttributeValues_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_InNormalizedAttributeValues;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutNormalizedAttributeValues_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutNormalizedAttributeValues_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_OutNormalizedAttributeValues;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_FixturePatch = { "FixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXModulator_eventModulate_Parms, FixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_ValueProp = { "InNormalizedAttributeValues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_Key_KeyProp = { "InNormalizedAttributeValues_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues = { "InNormalizedAttributeValues", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXModulator_eventModulate_Parms, InNormalizedAttributeValues), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_OutNormalizedAttributeValues_ValueProp = { "OutNormalizedAttributeValues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_OutNormalizedAttributeValues_Key_KeyProp = { "OutNormalizedAttributeValues_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_OutNormalizedAttributeValues = { "OutNormalizedAttributeValues", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXModulator_eventModulate_Parms, OutNormalizedAttributeValues), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXModulator_Modulate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_FixturePatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_InNormalizedAttributeValues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_OutNormalizedAttributeValues_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_OutNormalizedAttributeValues_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_Modulate_Statics::NewProp_OutNormalizedAttributeValues,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXModulator_Modulate_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Modulators/DMXModulator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXModulator_Modulate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXModulator, nullptr, "Modulate", nullptr, nullptr, sizeof(DMXModulator_eventModulate_Parms), Z_Construct_UFunction_UDMXModulator_Modulate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXModulator_Modulate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXModulator_Modulate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXModulator_Modulate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXModulator_Modulate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXModulator_Modulate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FixturePatch;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InNormalizedMatrixAttributeValues_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InNormalizedMatrixAttributeValues_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InNormalizedMatrixAttributeValues;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutNormalizedMatrixAttributeValues_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutNormalizedMatrixAttributeValues;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_FixturePatch = { "FixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXModulator_eventModulateMatrix_Parms, FixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues_Inner = { "InNormalizedMatrixAttributeValues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues = { "InNormalizedMatrixAttributeValues", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXModulator_eventModulateMatrix_Parms, InNormalizedMatrixAttributeValues), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_OutNormalizedMatrixAttributeValues_Inner = { "OutNormalizedMatrixAttributeValues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_OutNormalizedMatrixAttributeValues = { "OutNormalizedMatrixAttributeValues", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXModulator_eventModulateMatrix_Parms, OutNormalizedMatrixAttributeValues), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_FixturePatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_InNormalizedMatrixAttributeValues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_OutNormalizedMatrixAttributeValues_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::NewProp_OutNormalizedMatrixAttributeValues,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Modulators/DMXModulator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXModulator, nullptr, "ModulateMatrix", nullptr, nullptr, sizeof(DMXModulator_eventModulateMatrix_Parms), Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXModulator_ModulateMatrix()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXModulator_ModulateMatrix_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXModulator_NoRegister()
	{
		return UDMXModulator::StaticClass();
	}
	struct Z_Construct_UClass_UDMXModulator_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXModulator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXModulator_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXModulator_Modulate, "Modulate" }, // 1448000272
		{ &Z_Construct_UFunction_UDMXModulator_ModulateMatrix, "ModulateMatrix" }, // 3251266907
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** \n * Base class for custom modulators. Override Modulate and ModulateMatrix functions in the blueprints to implement functionality.\n * Input maps hold all attribute values of the patch. Output Maps can be freely defined, but Attributes not present in the patch will be ignored.\n */" },
		{ "IncludePath", "Modulators/DMXModulator.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Modulators/DMXModulator.h" },
		{ "ToolTip", "Base class for custom modulators. Override Modulate and ModulateMatrix functions in the blueprints to implement functionality.\nInput maps hold all attribute values of the patch. Output Maps can be freely defined, but Attributes not present in the patch will be ignored." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXModulator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXModulator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXModulator_Statics::ClassParams = {
		&UDMXModulator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXModulator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXModulator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXModulator, 1279074743);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXModulator>()
	{
		return UDMXModulator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXModulator(Z_Construct_UClass_UDMXModulator, &UDMXModulator::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXModulator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXModulator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
