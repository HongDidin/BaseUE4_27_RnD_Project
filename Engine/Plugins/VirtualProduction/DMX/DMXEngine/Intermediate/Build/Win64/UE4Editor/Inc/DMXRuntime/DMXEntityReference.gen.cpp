// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXEntityReference.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXEntityReference() {}
// Cross Module References
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityReference();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityControllerRef();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityReferenceConversions_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityReferenceConversions();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityController_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixtureType_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FDMXEntityFixturePatchRef>() == std::is_polymorphic<FDMXEntityReference>(), "USTRUCT FDMXEntityFixturePatchRef cannot be polymorphic unless super FDMXEntityReference is polymorphic");

class UScriptStruct* FDMXEntityFixturePatchRef::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXEntityFixturePatchRef"), sizeof(FDMXEntityFixturePatchRef), Get_Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXEntityFixturePatchRef>()
{
	return FDMXEntityFixturePatchRef::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXEntityFixturePatchRef(FDMXEntityFixturePatchRef::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXEntityFixturePatchRef"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityFixturePatchRef
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityFixturePatchRef()
	{
		UScriptStruct::DeferCppStructOps<FDMXEntityFixturePatchRef>(FName(TEXT("DMXEntityFixturePatchRef")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityFixturePatchRef;
	struct Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents a Fixture Patch from a DMX Library.\n * Used to store a reference to a Fixture Patch outside the DMX Library\n */" },
		{ "DisplayName", "DMX Fixture Patch Ref" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "Represents a Fixture Patch from a DMX Library.\nUsed to store a reference to a Fixture Patch outside the DMX Library" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXEntityFixturePatchRef>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXEntityReference,
		&NewStructOps,
		"DMXEntityFixturePatchRef",
		sizeof(FDMXEntityFixturePatchRef),
		alignof(FDMXEntityFixturePatchRef),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXEntityFixturePatchRef"), sizeof(FDMXEntityFixturePatchRef), Get_Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Hash() { return 3525379722U; }

static_assert(std::is_polymorphic<FDMXEntityFixtureTypeRef>() == std::is_polymorphic<FDMXEntityReference>(), "USTRUCT FDMXEntityFixtureTypeRef cannot be polymorphic unless super FDMXEntityReference is polymorphic");

class UScriptStruct* FDMXEntityFixtureTypeRef::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXEntityFixtureTypeRef"), sizeof(FDMXEntityFixtureTypeRef), Get_Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXEntityFixtureTypeRef>()
{
	return FDMXEntityFixtureTypeRef::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXEntityFixtureTypeRef(FDMXEntityFixtureTypeRef::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXEntityFixtureTypeRef"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityFixtureTypeRef
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityFixtureTypeRef()
	{
		UScriptStruct::DeferCppStructOps<FDMXEntityFixtureTypeRef>(FName(TEXT("DMXEntityFixtureTypeRef")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityFixtureTypeRef;
	struct Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents a Fixture Type from a DMX Library.\n * Used to store a reference to a Fixture Type outside the DMX Library\n */" },
		{ "DisplayName", "DMX Fixture Type Ref" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "Represents a Fixture Type from a DMX Library.\nUsed to store a reference to a Fixture Type outside the DMX Library" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXEntityFixtureTypeRef>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXEntityReference,
		&NewStructOps,
		"DMXEntityFixtureTypeRef",
		sizeof(FDMXEntityFixtureTypeRef),
		alignof(FDMXEntityFixtureTypeRef),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXEntityFixtureTypeRef"), sizeof(FDMXEntityFixtureTypeRef), Get_Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Hash() { return 3709821049U; }

static_assert(std::is_polymorphic<FDMXEntityControllerRef>() == std::is_polymorphic<FDMXEntityReference>(), "USTRUCT FDMXEntityControllerRef cannot be polymorphic unless super FDMXEntityReference is polymorphic");

class UScriptStruct* FDMXEntityControllerRef::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXEntityControllerRef_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXEntityControllerRef, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXEntityControllerRef"), sizeof(FDMXEntityControllerRef), Get_Z_Construct_UScriptStruct_FDMXEntityControllerRef_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXEntityControllerRef>()
{
	return FDMXEntityControllerRef::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXEntityControllerRef(FDMXEntityControllerRef::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXEntityControllerRef"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityControllerRef
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityControllerRef()
	{
		UScriptStruct::DeferCppStructOps<FDMXEntityControllerRef>(FName(TEXT("DMXEntityControllerRef")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityControllerRef;
	struct Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Represents a Controller from a DMX Library.\n * Used to store a reference to a Controller outside the DMX Library\n */" },
		{ "DisplayName", "DMX Controller Ref" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "Represents a Controller from a DMX Library.\nUsed to store a reference to a Controller outside the DMX Library" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXEntityControllerRef>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXEntityReference,
		&NewStructOps,
		"DMXEntityControllerRef",
		sizeof(FDMXEntityControllerRef),
		alignof(FDMXEntityControllerRef),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityControllerRef()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXEntityControllerRef_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXEntityControllerRef"), sizeof(FDMXEntityControllerRef), Get_Z_Construct_UScriptStruct_FDMXEntityControllerRef_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXEntityControllerRef_Hash() { return 2688267890U; }
class UScriptStruct* FDMXEntityReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXEntityReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXEntityReference, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXEntityReference"), sizeof(FDMXEntityReference), Get_Z_Construct_UScriptStruct_FDMXEntityReference_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXEntityReference>()
{
	return FDMXEntityReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXEntityReference(FDMXEntityReference::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXEntityReference"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityReference
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityReference()
	{
		UScriptStruct::DeferCppStructOps<FDMXEntityReference>(FName(TEXT("DMXEntityReference")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXEntityReference;
	struct Z_Construct_UScriptStruct_FDMXEntityReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXLibrary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayLibraryPicker_MetaData[];
#endif
		static void NewProp_bDisplayLibraryPicker_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayLibraryPicker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EntityType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_EntityType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EntityId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EntityId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityReference_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Represents an Entity from a DMX Library.\n * Used to allow objects outside the DMX Library package to store references to UDMXEntity objects\n */" },
		{ "DisplayName", "DMX Entity Reference" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "Represents an Entity from a DMX Library.\nUsed to allow objects outside the DMX Library package to store references to UDMXEntity objects" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXEntityReference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_DMXLibrary_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * The parent DMX library of the Entity\n\x09 * Automatically set when calling SetEntity with a valid Entity.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "The parent DMX library of the Entity\nAutomatically set when calling SetEntity with a valid Entity." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_DMXLibrary = { "DMXLibrary", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXEntityReference, DMXLibrary), Z_Construct_UClass_UDMXLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_DMXLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_DMXLibrary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker_MetaData[] = {
		{ "Comment", "/** Display the DMX Library asset picker. True by default, for Blueprint variables */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "Display the DMX Library asset picker. True by default, for Blueprint variables" },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker_SetBit(void* Obj)
	{
		((FDMXEntityReference*)Obj)->bDisplayLibraryPicker = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker = { "bDisplayLibraryPicker", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXEntityReference), &Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityType_MetaData[] = {
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityType = { "EntityType", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXEntityReference, EntityType), Z_Construct_UClass_UDMXEntity_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityId_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** The entity's unique ID */// Without EditAnywhere here the value is not saved on components in a Level\n" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "The entity's unique ID // Without EditAnywhere here the value is not saved on components in a Level" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityId = { "EntityId", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXEntityReference, EntityId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXEntityReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_DMXLibrary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_bDisplayLibraryPicker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXEntityReference_Statics::NewProp_EntityId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXEntityReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXEntityReference",
		sizeof(FDMXEntityReference),
		alignof(FDMXEntityReference),
		Z_Construct_UScriptStruct_FDMXEntityReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXEntityReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXEntityReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXEntityReference"), sizeof(FDMXEntityReference), Get_Z_Construct_UScriptStruct_FDMXEntityReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXEntityReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXEntityReference_Hash() { return 3784696403U; }
	DEFINE_FUNCTION(UDMXEntityReferenceConversions::execConv_FixturePatchObjToRef)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InFixturePatch);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDMXEntityFixturePatchRef*)Z_Param__Result=UDMXEntityReferenceConversions::Conv_FixturePatchObjToRef(Z_Param_InFixturePatch);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityReferenceConversions::execConv_FixtureTypeObjToRef)
	{
		P_GET_OBJECT(UDMXEntityFixtureType,Z_Param_InFixtureType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDMXEntityFixtureTypeRef*)Z_Param__Result=UDMXEntityReferenceConversions::Conv_FixtureTypeObjToRef(Z_Param_InFixtureType);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityReferenceConversions::execConv_ControllerObjToRef)
	{
		P_GET_OBJECT(UDMXEntityController,Z_Param_InController);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDMXEntityControllerRef*)Z_Param__Result=UDMXEntityReferenceConversions::Conv_ControllerObjToRef(Z_Param_InController);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityReferenceConversions::execConv_FixturePatchRefToObj)
	{
		P_GET_STRUCT_REF(FDMXEntityFixturePatchRef,Z_Param_Out_InFixturePatchRef);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDMXEntityFixturePatch**)Z_Param__Result=UDMXEntityReferenceConversions::Conv_FixturePatchRefToObj(Z_Param_Out_InFixturePatchRef);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityReferenceConversions::execConv_FixtureTypeRefToObj)
	{
		P_GET_STRUCT_REF(FDMXEntityFixtureTypeRef,Z_Param_Out_InFixtureTypeRef);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDMXEntityFixtureType**)Z_Param__Result=UDMXEntityReferenceConversions::Conv_FixtureTypeRefToObj(Z_Param_Out_InFixtureTypeRef);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityReferenceConversions::execConv_ControllerRefToObj)
	{
		P_GET_STRUCT_REF(FDMXEntityControllerRef,Z_Param_Out_InControllerRef);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDMXEntityController**)Z_Param__Result=UDMXEntityReferenceConversions::Conv_ControllerRefToObj(Z_Param_Out_InControllerRef);
		P_NATIVE_END;
	}
	void UDMXEntityReferenceConversions::StaticRegisterNativesUDMXEntityReferenceConversions()
	{
		UClass* Class = UDMXEntityReferenceConversions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Conv_ControllerObjToRef", &UDMXEntityReferenceConversions::execConv_ControllerObjToRef },
			{ "Conv_ControllerRefToObj", &UDMXEntityReferenceConversions::execConv_ControllerRefToObj },
			{ "Conv_FixturePatchObjToRef", &UDMXEntityReferenceConversions::execConv_FixturePatchObjToRef },
			{ "Conv_FixturePatchRefToObj", &UDMXEntityReferenceConversions::execConv_FixturePatchRefToObj },
			{ "Conv_FixtureTypeObjToRef", &UDMXEntityReferenceConversions::execConv_FixtureTypeObjToRef },
			{ "Conv_FixtureTypeRefToObj", &UDMXEntityReferenceConversions::execConv_FixtureTypeRefToObj },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics
	{
		struct DMXEntityReferenceConversions_eventConv_ControllerObjToRef_Parms
		{
			UDMXEntityController* InController;
			FDMXEntityControllerRef ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InController;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::NewProp_InController = { "InController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_ControllerObjToRef_Parms, InController), Z_Construct_UClass_UDMXEntityController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_ControllerObjToRef_Parms, ReturnValue), Z_Construct_UScriptStruct_FDMXEntityControllerRef, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::NewProp_InController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToControllerRef (Controller)" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityReferenceConversions, nullptr, "Conv_ControllerObjToRef", nullptr, nullptr, sizeof(DMXEntityReferenceConversions_eventConv_ControllerObjToRef_Parms), Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics
	{
		struct DMXEntityReferenceConversions_eventConv_ControllerRefToObj_Parms
		{
			FDMXEntityControllerRef InControllerRef;
			UDMXEntityController* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InControllerRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InControllerRef;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_InControllerRef_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_InControllerRef = { "InControllerRef", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_ControllerRefToObj_Parms, InControllerRef), Z_Construct_UScriptStruct_FDMXEntityControllerRef, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_InControllerRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_InControllerRef_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_ControllerRefToObj_Parms, ReturnValue), Z_Construct_UClass_UDMXEntityController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_InControllerRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToController (ControllerReference)" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityReferenceConversions, nullptr, "Conv_ControllerRefToObj", nullptr, nullptr, sizeof(DMXEntityReferenceConversions_eventConv_ControllerRefToObj_Parms), Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics
	{
		struct DMXEntityReferenceConversions_eventConv_FixturePatchObjToRef_Parms
		{
			UDMXEntityFixturePatch* InFixturePatch;
			FDMXEntityFixturePatchRef ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InFixturePatch;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::NewProp_InFixturePatch = { "InFixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixturePatchObjToRef_Parms, InFixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixturePatchObjToRef_Parms, ReturnValue), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::NewProp_InFixturePatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToFixturePatchRef (FixturePatch)" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityReferenceConversions, nullptr, "Conv_FixturePatchObjToRef", nullptr, nullptr, sizeof(DMXEntityReferenceConversions_eventConv_FixturePatchObjToRef_Parms), Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics
	{
		struct DMXEntityReferenceConversions_eventConv_FixturePatchRefToObj_Parms
		{
			FDMXEntityFixturePatchRef InFixturePatchRef;
			UDMXEntityFixturePatch* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFixturePatchRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFixturePatchRef;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_InFixturePatchRef_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_InFixturePatchRef = { "InFixturePatchRef", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixturePatchRefToObj_Parms, InFixturePatchRef), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_InFixturePatchRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_InFixturePatchRef_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixturePatchRefToObj_Parms, ReturnValue), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_InFixturePatchRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToFixturePatch (FixturePatchReference)" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityReferenceConversions, nullptr, "Conv_FixturePatchRefToObj", nullptr, nullptr, sizeof(DMXEntityReferenceConversions_eventConv_FixturePatchRefToObj_Parms), Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics
	{
		struct DMXEntityReferenceConversions_eventConv_FixtureTypeObjToRef_Parms
		{
			UDMXEntityFixtureType* InFixtureType;
			FDMXEntityFixtureTypeRef ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InFixtureType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::NewProp_InFixtureType = { "InFixtureType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixtureTypeObjToRef_Parms, InFixtureType), Z_Construct_UClass_UDMXEntityFixtureType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixtureTypeObjToRef_Parms, ReturnValue), Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::NewProp_InFixtureType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToFixtureTypeRef (FixtureType)" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityReferenceConversions, nullptr, "Conv_FixtureTypeObjToRef", nullptr, nullptr, sizeof(DMXEntityReferenceConversions_eventConv_FixtureTypeObjToRef_Parms), Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics
	{
		struct DMXEntityReferenceConversions_eventConv_FixtureTypeRefToObj_Parms
		{
			FDMXEntityFixtureTypeRef InFixtureTypeRef;
			UDMXEntityFixtureType* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFixtureTypeRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFixtureTypeRef;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_InFixtureTypeRef_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_InFixtureTypeRef = { "InFixtureTypeRef", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixtureTypeRefToObj_Parms, InFixtureTypeRef), Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_InFixtureTypeRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_InFixtureTypeRef_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityReferenceConversions_eventConv_FixtureTypeRefToObj_Parms, ReturnValue), Z_Construct_UClass_UDMXEntityFixtureType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_InFixtureTypeRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::Function_MetaDataParams[] = {
		{ "BlueprintAutocast", "" },
		{ "Category", "Utilities|DMX" },
		{ "CompactNodeTitle", "->" },
		{ "DisplayName", "ToFixtureType (FixtureTypeReference)" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityReferenceConversions, nullptr, "Conv_FixtureTypeRefToObj", nullptr, nullptr, sizeof(DMXEntityReferenceConversions_eventConv_FixtureTypeRefToObj_Parms), Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXEntityReferenceConversions_NoRegister()
	{
		return UDMXEntityReferenceConversions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEntityReferenceConversions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerObjToRef, "Conv_ControllerObjToRef" }, // 2532212855
		{ &Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_ControllerRefToObj, "Conv_ControllerRefToObj" }, // 4020747296
		{ &Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchObjToRef, "Conv_FixturePatchObjToRef" }, // 2511134453
		{ &Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixturePatchRefToObj, "Conv_FixturePatchRefToObj" }, // 612159327
		{ &Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeObjToRef, "Conv_FixtureTypeObjToRef" }, // 3222398432
		{ &Z_Construct_UFunction_UDMXEntityReferenceConversions_Conv_FixtureTypeRefToObj, "Conv_FixtureTypeRefToObj" }, // 2664742819
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintThreadSafe", "" },
		{ "Comment", "/** Extend type conversions to handle Entity Reference structs */" },
		{ "IncludePath", "Library/DMXEntityReference.h" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityReference.h" },
		{ "ToolTip", "Extend type conversions to handle Entity Reference structs" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEntityReferenceConversions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::ClassParams = {
		&UDMXEntityReferenceConversions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEntityReferenceConversions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEntityReferenceConversions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEntityReferenceConversions, 3786243262);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXEntityReferenceConversions>()
	{
		return UDMXEntityReferenceConversions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEntityReferenceConversions(Z_Construct_UClass_UDMXEntityReferenceConversions, &UDMXEntityReferenceConversions::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXEntityReferenceConversions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEntityReferenceConversions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
