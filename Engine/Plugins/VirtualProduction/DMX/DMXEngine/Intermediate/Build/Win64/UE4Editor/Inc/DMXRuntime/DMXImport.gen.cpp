// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXImport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXImport() {}
// Cross Module References
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXColorCIE();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportFixtureType_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportFixtureType();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportAttributeDefinitions_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportAttributeDefinitions();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportWheels_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportWheels();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportPhysicalDescriptions_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportPhysicalDescriptions();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportModels_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportModels();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGeometries_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGeometries();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportDMXModes_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportDMXModes();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportProtocols_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportProtocols();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImport_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImport();
// End Cross Module References
class UScriptStruct* FDMXColorCIE::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXColorCIE_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXColorCIE, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXColorCIE"), sizeof(FDMXColorCIE), Get_Z_Construct_UScriptStruct_FDMXColorCIE_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXColorCIE>()
{
	return FDMXColorCIE::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXColorCIE(FDMXColorCIE::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXColorCIE"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXColorCIE
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXColorCIE()
	{
		UScriptStruct::DeferCppStructOps<FDMXColorCIE>(FName(TEXT("DMXColorCIE")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXColorCIE;
	struct Z_Construct_UScriptStruct_FDMXColorCIE_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YY_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_YY;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXColorCIE_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXColorCIE>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_X_MetaData[] = {
		{ "Category", "Color" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXColorCIE, X), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_Y_MetaData[] = {
		{ "Category", "Color" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXColorCIE, Y), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_YY_MetaData[] = {
		{ "Category", "Color" },
		{ "ClampMax", "255" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_YY = { "YY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXColorCIE, YY), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_YY_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_YY_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXColorCIE_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXColorCIE_Statics::NewProp_YY,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXColorCIE_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXColorCIE",
		sizeof(FDMXColorCIE),
		alignof(FDMXColorCIE),
		Z_Construct_UScriptStruct_FDMXColorCIE_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXColorCIE_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXColorCIE()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXColorCIE_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXColorCIE"), sizeof(FDMXColorCIE), Get_Z_Construct_UScriptStruct_FDMXColorCIE_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXColorCIE_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXColorCIE_Hash() { return 914854501U; }
	void UDMXImportFixtureType::StaticRegisterNativesUDMXImportFixtureType()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportFixtureType_NoRegister()
	{
		return UDMXImportFixtureType::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportFixtureType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportFixtureType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportFixtureType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportFixtureType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportFixtureType>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportFixtureType_Statics::ClassParams = {
		&UDMXImportFixtureType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportFixtureType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportFixtureType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportFixtureType()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportFixtureType_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportFixtureType, 2029317382);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportFixtureType>()
	{
		return UDMXImportFixtureType::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportFixtureType(Z_Construct_UClass_UDMXImportFixtureType, &UDMXImportFixtureType::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportFixtureType"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportFixtureType);
	void UDMXImportAttributeDefinitions::StaticRegisterNativesUDMXImportAttributeDefinitions()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportAttributeDefinitions_NoRegister()
	{
		return UDMXImportAttributeDefinitions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportAttributeDefinitions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::ClassParams = {
		&UDMXImportAttributeDefinitions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportAttributeDefinitions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportAttributeDefinitions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportAttributeDefinitions, 1255393379);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportAttributeDefinitions>()
	{
		return UDMXImportAttributeDefinitions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportAttributeDefinitions(Z_Construct_UClass_UDMXImportAttributeDefinitions, &UDMXImportAttributeDefinitions::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportAttributeDefinitions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportAttributeDefinitions);
	void UDMXImportWheels::StaticRegisterNativesUDMXImportWheels()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportWheels_NoRegister()
	{
		return UDMXImportWheels::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportWheels_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportWheels_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportWheels_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportWheels_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportWheels>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportWheels_Statics::ClassParams = {
		&UDMXImportWheels::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportWheels_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportWheels_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportWheels()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportWheels_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportWheels, 438302969);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportWheels>()
	{
		return UDMXImportWheels::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportWheels(Z_Construct_UClass_UDMXImportWheels, &UDMXImportWheels::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportWheels"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportWheels);
	void UDMXImportPhysicalDescriptions::StaticRegisterNativesUDMXImportPhysicalDescriptions()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportPhysicalDescriptions_NoRegister()
	{
		return UDMXImportPhysicalDescriptions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportPhysicalDescriptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::ClassParams = {
		&UDMXImportPhysicalDescriptions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportPhysicalDescriptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportPhysicalDescriptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportPhysicalDescriptions, 2358653886);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportPhysicalDescriptions>()
	{
		return UDMXImportPhysicalDescriptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportPhysicalDescriptions(Z_Construct_UClass_UDMXImportPhysicalDescriptions, &UDMXImportPhysicalDescriptions::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportPhysicalDescriptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportPhysicalDescriptions);
	void UDMXImportModels::StaticRegisterNativesUDMXImportModels()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportModels_NoRegister()
	{
		return UDMXImportModels::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportModels_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportModels_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportModels_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportModels_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportModels>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportModels_Statics::ClassParams = {
		&UDMXImportModels::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportModels_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportModels_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportModels()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportModels_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportModels, 1107665111);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportModels>()
	{
		return UDMXImportModels::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportModels(Z_Construct_UClass_UDMXImportModels, &UDMXImportModels::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportModels"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportModels);
	void UDMXImportGeometries::StaticRegisterNativesUDMXImportGeometries()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGeometries_NoRegister()
	{
		return UDMXImportGeometries::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGeometries_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGeometries_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGeometries_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGeometries_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGeometries>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGeometries_Statics::ClassParams = {
		&UDMXImportGeometries::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGeometries_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGeometries_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGeometries()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGeometries_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGeometries, 2357523544);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGeometries>()
	{
		return UDMXImportGeometries::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGeometries(Z_Construct_UClass_UDMXImportGeometries, &UDMXImportGeometries::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGeometries"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGeometries);
	void UDMXImportDMXModes::StaticRegisterNativesUDMXImportDMXModes()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportDMXModes_NoRegister()
	{
		return UDMXImportDMXModes::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportDMXModes_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportDMXModes_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportDMXModes_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportDMXModes_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportDMXModes>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportDMXModes_Statics::ClassParams = {
		&UDMXImportDMXModes::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportDMXModes_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportDMXModes_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportDMXModes()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportDMXModes_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportDMXModes, 3015672394);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportDMXModes>()
	{
		return UDMXImportDMXModes::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportDMXModes(Z_Construct_UClass_UDMXImportDMXModes, &UDMXImportDMXModes::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportDMXModes"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportDMXModes);
	void UDMXImportProtocols::StaticRegisterNativesUDMXImportProtocols()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportProtocols_NoRegister()
	{
		return UDMXImportProtocols::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportProtocols_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportProtocols_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportProtocols_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportProtocols_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportProtocols>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportProtocols_Statics::ClassParams = {
		&UDMXImportProtocols::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportProtocols_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportProtocols_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportProtocols()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportProtocols_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportProtocols, 2387225149);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportProtocols>()
	{
		return UDMXImportProtocols::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportProtocols(Z_Construct_UClass_UDMXImportProtocols, &UDMXImportProtocols::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportProtocols"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportProtocols);
	void UDMXImport::StaticRegisterNativesUDMXImport()
	{
	}
	UClass* Z_Construct_UClass_UDMXImport_NoRegister()
	{
		return UDMXImport::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImport_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixtureType_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FixtureType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeDefinitions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AttributeDefinitions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Wheels_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Wheels;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalDescriptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PhysicalDescriptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Models_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Models;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Geometries_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Geometries;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXModes_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXModes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Protocols_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Protocols;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImport_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_FixtureType_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_FixtureType = { "FixtureType", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, FixtureType), Z_Construct_UClass_UDMXImportFixtureType_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_FixtureType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_FixtureType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_AttributeDefinitions_MetaData[] = {
		{ "Category", "Attribute Definitions" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_AttributeDefinitions = { "AttributeDefinitions", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, AttributeDefinitions), Z_Construct_UClass_UDMXImportAttributeDefinitions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_AttributeDefinitions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_AttributeDefinitions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_Wheels_MetaData[] = {
		{ "Category", "Wheels" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_Wheels = { "Wheels", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, Wheels), Z_Construct_UClass_UDMXImportWheels_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_Wheels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_Wheels_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_PhysicalDescriptions_MetaData[] = {
		{ "Category", "Physical Descriptions" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_PhysicalDescriptions = { "PhysicalDescriptions", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, PhysicalDescriptions), Z_Construct_UClass_UDMXImportPhysicalDescriptions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_PhysicalDescriptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_PhysicalDescriptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_Models_MetaData[] = {
		{ "Category", "Models" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_Models = { "Models", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, Models), Z_Construct_UClass_UDMXImportModels_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_Models_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_Models_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_Geometries_MetaData[] = {
		{ "Category", "Geometries" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_Geometries = { "Geometries", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, Geometries), Z_Construct_UClass_UDMXImportGeometries_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_Geometries_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_Geometries_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_DMXModes_MetaData[] = {
		{ "Category", "DMXModes" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_DMXModes = { "DMXModes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, DMXModes), Z_Construct_UClass_UDMXImportDMXModes_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_DMXModes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_DMXModes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImport_Statics::NewProp_Protocols_MetaData[] = {
		{ "Category", "Protocols" },
		{ "EditInline", "" },
		{ "ModuleRelativePath", "Public/Library/DMXImport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImport_Statics::NewProp_Protocols = { "Protocols", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImport, Protocols), Z_Construct_UClass_UDMXImportProtocols_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::NewProp_Protocols_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::NewProp_Protocols_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_FixtureType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_AttributeDefinitions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_Wheels,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_PhysicalDescriptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_Models,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_Geometries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_DMXModes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImport_Statics::NewProp_Protocols,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImport_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImport>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImport_Statics::ClassParams = {
		&UDMXImport::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImport_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImport_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImport_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImport()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImport_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImport, 1232170242);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImport>()
	{
		return UDMXImport::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImport(Z_Construct_UClass_UDMXImport, &UDMXImport::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImport"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImport);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
