// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDMXEntityFixturePatch;
struct FDMXNormalizedAttributeValueMap;
struct FDMXAttributeName; 
#ifdef DMXRUNTIME_DMXModulator_generated_h
#error "DMXModulator.generated.h already included, missing '#pragma once' in DMXModulator.h"
#endif
#define DMXRUNTIME_DMXModulator_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_RPC_WRAPPERS \
	virtual void ModulateMatrix_Implementation(UDMXEntityFixturePatch* FixturePatch, TArray<FDMXNormalizedAttributeValueMap> const& InNormalizedMatrixAttributeValues, TArray<FDMXNormalizedAttributeValueMap>& OutNormalizedMatrixAttributeValues); \
	virtual void Modulate_Implementation(UDMXEntityFixturePatch* FixturePatch, TMap<FDMXAttributeName,float> const& InNormalizedAttributeValues, TMap<FDMXAttributeName,float>& OutNormalizedAttributeValues); \
 \
	DECLARE_FUNCTION(execModulateMatrix); \
	DECLARE_FUNCTION(execModulate);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execModulateMatrix); \
	DECLARE_FUNCTION(execModulate);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_EVENT_PARMS \
	struct DMXModulator_eventModulate_Parms \
	{ \
		UDMXEntityFixturePatch* FixturePatch; \
		TMap<FDMXAttributeName,float> InNormalizedAttributeValues; \
		TMap<FDMXAttributeName,float> OutNormalizedAttributeValues; \
	}; \
	struct DMXModulator_eventModulateMatrix_Parms \
	{ \
		UDMXEntityFixturePatch* FixturePatch; \
		TArray<FDMXNormalizedAttributeValueMap> InNormalizedMatrixAttributeValues; \
		TArray<FDMXNormalizedAttributeValueMap> OutNormalizedMatrixAttributeValues; \
	};


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_CALLBACK_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXModulator(); \
	friend struct Z_Construct_UClass_UDMXModulator_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUDMXModulator(); \
	friend struct Z_Construct_UClass_UDMXModulator_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXModulator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXModulator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator(UDMXModulator&&); \
	NO_API UDMXModulator(const UDMXModulator&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXModulator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator(UDMXModulator&&); \
	NO_API UDMXModulator(const UDMXModulator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXModulator)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_19_PROLOG \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_EVENT_PARMS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_CALLBACK_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXModulator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Modulators_DMXModulator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
