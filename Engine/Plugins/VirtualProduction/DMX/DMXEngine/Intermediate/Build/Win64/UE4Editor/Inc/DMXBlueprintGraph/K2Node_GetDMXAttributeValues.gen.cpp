// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXBlueprintGraph/Public/K2Node_GetDMXAttributeValues.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_GetDMXAttributeValues() {}
// Cross Module References
	DMXBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_GetDMXAttributeValues_NoRegister();
	DMXBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_GetDMXAttributeValues();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_EditablePinBase();
	UPackage* Z_Construct_UPackage__Script_DMXBlueprintGraph();
// End Cross Module References
	void UK2Node_GetDMXAttributeValues::StaticRegisterNativesUK2Node_GetDMXAttributeValues()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_GetDMXAttributeValues_NoRegister()
	{
		return UK2Node_GetDMXAttributeValues::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsExposed_MetaData[];
#endif
		static void NewProp_bIsExposed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsExposed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_EditablePinBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXBlueprintGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_GetDMXAttributeValues.h" },
		{ "ModuleRelativePath", "Public/K2Node_GetDMXAttributeValues.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed_MetaData[] = {
		{ "ModuleRelativePath", "Public/K2Node_GetDMXAttributeValues.h" },
	};
#endif
	void Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed_SetBit(void* Obj)
	{
		((UK2Node_GetDMXAttributeValues*)Obj)->bIsExposed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed = { "bIsExposed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UK2Node_GetDMXAttributeValues), &Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed_SetBit, METADATA_PARAMS(Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::NewProp_bIsExposed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_GetDMXAttributeValues>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::ClassParams = {
		&UK2Node_GetDMXAttributeValues::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_GetDMXAttributeValues()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_GetDMXAttributeValues, 283517941);
	template<> DMXBLUEPRINTGRAPH_API UClass* StaticClass<UK2Node_GetDMXAttributeValues>()
	{
		return UK2Node_GetDMXAttributeValues::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_GetDMXAttributeValues(Z_Construct_UClass_UK2Node_GetDMXAttributeValues, &UK2Node_GetDMXAttributeValues::StaticClass, TEXT("/Script/DMXBlueprintGraph"), TEXT("UK2Node_GetDMXAttributeValues"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_GetDMXAttributeValues);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
