// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Private/Modulators/DMXModulator_RGBtoCMY.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXModulator_RGBtoCMY() {}
// Cross Module References
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_RGBtoCMY_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_RGBtoCMY();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
// End Cross Module References
	void UDMXModulator_RGBtoCMY::StaticRegisterNativesUDMXModulator_RGBtoCMY()
	{
	}
	UClass* Z_Construct_UClass_UDMXModulator_RGBtoCMY_NoRegister()
	{
		return UDMXModulator_RGBtoCMY::StaticClass();
	}
	struct Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeRedToCyan_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeRedToCyan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeGreenToMagenta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeGreenToMagenta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeBlueToYellow_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeBlueToYellow;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXModulator,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "DMX" },
		{ "Comment", "/** Converts Attributes from RGB to CMY  */" },
		{ "DisplayName", "DMX Modulator RGB to CMY" },
		{ "IncludePath", "Modulators/DMXModulator_RGBtoCMY.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_RGBtoCMY.h" },
		{ "ToolTip", "Converts Attributes from RGB to CMY" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeRedToCyan_MetaData[] = {
		{ "AdvancedDisplay", "" },
		{ "Category", "RGB to CMY" },
		{ "Comment", "/** The name of the attribute that is converted from Red to Cyan */" },
		{ "DisplayName", "Red to Cyan" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_RGBtoCMY.h" },
		{ "ToolTip", "The name of the attribute that is converted from Red to Cyan" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeRedToCyan = { "AttributeRedToCyan", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_RGBtoCMY, AttributeRedToCyan), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeRedToCyan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeRedToCyan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeGreenToMagenta_MetaData[] = {
		{ "AdvancedDisplay", "" },
		{ "Category", "RGB to CMY" },
		{ "Comment", "/** The name of the attribute that is converted from Green to Magenta */" },
		{ "DisplayName", "Green to Magenta" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_RGBtoCMY.h" },
		{ "ToolTip", "The name of the attribute that is converted from Green to Magenta" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeGreenToMagenta = { "AttributeGreenToMagenta", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_RGBtoCMY, AttributeGreenToMagenta), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeGreenToMagenta_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeGreenToMagenta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeBlueToYellow_MetaData[] = {
		{ "AdvancedDisplay", "" },
		{ "Category", "RGB to CMY" },
		{ "Comment", "/** The name of the attribute that is converted from Blue to Yellow */" },
		{ "DisplayName", "Blue to Yellow" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_RGBtoCMY.h" },
		{ "ToolTip", "The name of the attribute that is converted from Blue to Yellow" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeBlueToYellow = { "AttributeBlueToYellow", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_RGBtoCMY, AttributeBlueToYellow), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeBlueToYellow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeBlueToYellow_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeRedToCyan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeGreenToMagenta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::NewProp_AttributeBlueToYellow,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXModulator_RGBtoCMY>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::ClassParams = {
		&UDMXModulator_RGBtoCMY::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXModulator_RGBtoCMY()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXModulator_RGBtoCMY, 3106909185);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXModulator_RGBtoCMY>()
	{
		return UDMXModulator_RGBtoCMY::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXModulator_RGBtoCMY(Z_Construct_UClass_UDMXModulator_RGBtoCMY, &UDMXModulator_RGBtoCMY::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXModulator_RGBtoCMY"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXModulator_RGBtoCMY);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
