// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDMXImportGDTFDMXMode;
struct FDMXImportGDTFChannelFunction;
class UDMXImportGDTFDMXModes;
#ifdef DMXRUNTIME_DMXImportGDTF_generated_h
#error "DMXImportGDTF.generated.h already included, missing '#pragma once' in DMXImportGDTF.h"
#endif
#define DMXRUNTIME_DMXImportGDTF_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_667_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFDMXMode>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_658_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFTMacro>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_640_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFRelation>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_614_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFDMXChannel>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_590_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFLogicalChannel>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_536_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFChannelFunction>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_515_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFChannelSet>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_495_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFDMXValue>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_468_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFGeneralGeometry>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_458_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFGeometryReference>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_445_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFBreak>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_439_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFilterShaper>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_432_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFilterGobo>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_425_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFilterColor>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_418_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFilterBeam>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_411_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFTypeGeometry>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_401_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFGeneralAxis>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_391_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFTypeAxis>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_357_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXImportGDTFGeometryBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFBeam>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_337_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFGeometryBase>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_316_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFModel>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_309_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFCRIs>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_303_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFDMXProfiles>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_279_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFColorSpace>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_258_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFEmitter>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_237_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFMeasurement>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_224_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFMeasurementPoint>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_212_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFWheel>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_194_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFWheelSlot>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_181_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFilter>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_154_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFAttribute>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_139_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFeatureGroup>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_130_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFFeature>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_121_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXImportGDTFActivationGroup>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFFixtureType(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFFixtureType, UDMXImportFixtureType, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFFixtureType)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFFixtureType(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFFixtureType, UDMXImportFixtureType, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFFixtureType)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFFixtureType(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFFixtureType) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFFixtureType); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFFixtureType); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFFixtureType(UDMXImportGDTFFixtureType&&); \
	NO_API UDMXImportGDTFFixtureType(const UDMXImportGDTFFixtureType&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFFixtureType(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFFixtureType(UDMXImportGDTFFixtureType&&); \
	NO_API UDMXImportGDTFFixtureType(const UDMXImportGDTFFixtureType&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFFixtureType); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFFixtureType); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFFixtureType)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_685_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_689_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFFixtureType>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFAttributeDefinitions(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFAttributeDefinitions, UDMXImportAttributeDefinitions, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFAttributeDefinitions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFAttributeDefinitions(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFAttributeDefinitions, UDMXImportAttributeDefinitions, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFAttributeDefinitions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFAttributeDefinitions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFAttributeDefinitions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFAttributeDefinitions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFAttributeDefinitions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFAttributeDefinitions(UDMXImportGDTFAttributeDefinitions&&); \
	NO_API UDMXImportGDTFAttributeDefinitions(const UDMXImportGDTFAttributeDefinitions&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFAttributeDefinitions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFAttributeDefinitions(UDMXImportGDTFAttributeDefinitions&&); \
	NO_API UDMXImportGDTFAttributeDefinitions(const UDMXImportGDTFAttributeDefinitions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFAttributeDefinitions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFAttributeDefinitions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFAttributeDefinitions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_717_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_721_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFAttributeDefinitions>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFWheels(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFWheels_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFWheels, UDMXImportWheels, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFWheels)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFWheels(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFWheels_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFWheels, UDMXImportWheels, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFWheels)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFWheels(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFWheels) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFWheels); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFWheels); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFWheels(UDMXImportGDTFWheels&&); \
	NO_API UDMXImportGDTFWheels(const UDMXImportGDTFWheels&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFWheels(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFWheels(UDMXImportGDTFWheels&&); \
	NO_API UDMXImportGDTFWheels(const UDMXImportGDTFWheels&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFWheels); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFWheels); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFWheels)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_739_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_743_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFWheels>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFPhysicalDescriptions(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFPhysicalDescriptions, UDMXImportPhysicalDescriptions, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFPhysicalDescriptions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFPhysicalDescriptions(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFPhysicalDescriptions, UDMXImportPhysicalDescriptions, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFPhysicalDescriptions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFPhysicalDescriptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFPhysicalDescriptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFPhysicalDescriptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFPhysicalDescriptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFPhysicalDescriptions(UDMXImportGDTFPhysicalDescriptions&&); \
	NO_API UDMXImportGDTFPhysicalDescriptions(const UDMXImportGDTFPhysicalDescriptions&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFPhysicalDescriptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFPhysicalDescriptions(UDMXImportGDTFPhysicalDescriptions&&); \
	NO_API UDMXImportGDTFPhysicalDescriptions(const UDMXImportGDTFPhysicalDescriptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFPhysicalDescriptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFPhysicalDescriptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFPhysicalDescriptions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_753_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_757_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFPhysicalDescriptions>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFModels(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFModels_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFModels, UDMXImportModels, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFModels)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFModels(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFModels_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFModels, UDMXImportModels, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFModels)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFModels(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFModels) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFModels); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFModels); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFModels(UDMXImportGDTFModels&&); \
	NO_API UDMXImportGDTFModels(const UDMXImportGDTFModels&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFModels(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFModels(UDMXImportGDTFModels&&); \
	NO_API UDMXImportGDTFModels(const UDMXImportGDTFModels&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFModels); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFModels); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFModels)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_777_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_781_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFModels>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFGeometries(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFGeometries_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFGeometries, UDMXImportGeometries, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFGeometries)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFGeometries(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFGeometries_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFGeometries, UDMXImportGeometries, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFGeometries)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFGeometries(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFGeometries) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFGeometries); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFGeometries); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFGeometries(UDMXImportGDTFGeometries&&); \
	NO_API UDMXImportGDTFGeometries(const UDMXImportGDTFGeometries&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFGeometries(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFGeometries(UDMXImportGDTFGeometries&&); \
	NO_API UDMXImportGDTFGeometries(const UDMXImportGDTFGeometries&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFGeometries); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFGeometries); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFGeometries)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_788_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_792_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFGeometries>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDMXChannelFunctions);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDMXChannelFunctions);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFDMXModes(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFDMXModes, UDMXImportDMXModes, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFDMXModes)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFDMXModes(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFDMXModes, UDMXImportDMXModes, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFDMXModes)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFDMXModes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFDMXModes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFDMXModes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFDMXModes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFDMXModes(UDMXImportGDTFDMXModes&&); \
	NO_API UDMXImportGDTFDMXModes(const UDMXImportGDTFDMXModes&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFDMXModes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFDMXModes(UDMXImportGDTFDMXModes&&); \
	NO_API UDMXImportGDTFDMXModes(const UDMXImportGDTFDMXModes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFDMXModes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFDMXModes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFDMXModes)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_799_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_803_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFDMXModes>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFProtocols(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFProtocols_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFProtocols, UDMXImportProtocols, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFProtocols)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTFProtocols(); \
	friend struct Z_Construct_UClass_UDMXImportGDTFProtocols_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTFProtocols, UDMXImportProtocols, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTFProtocols)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFProtocols(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFProtocols) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFProtocols); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFProtocols); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFProtocols(UDMXImportGDTFProtocols&&); \
	NO_API UDMXImportGDTFProtocols(const UDMXImportGDTFProtocols&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTFProtocols(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTFProtocols(UDMXImportGDTFProtocols&&); \
	NO_API UDMXImportGDTFProtocols(const UDMXImportGDTFProtocols&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTFProtocols); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTFProtocols); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTFProtocols)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_814_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_818_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTFProtocols>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDMXModes);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDMXModes);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXImportGDTF(); \
	friend struct Z_Construct_UClass_UDMXImportGDTF_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTF, UDMXImport, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTF)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_INCLASS \
private: \
	static void StaticRegisterNativesUDMXImportGDTF(); \
	friend struct Z_Construct_UClass_UDMXImportGDTF_Statics; \
public: \
	DECLARE_CLASS(UDMXImportGDTF, UDMXImport, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXImportGDTF)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTF(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTF) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTF); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTF); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTF(UDMXImportGDTF&&); \
	NO_API UDMXImportGDTF(const UDMXImportGDTF&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXImportGDTF(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXImportGDTF(UDMXImportGDTF&&); \
	NO_API UDMXImportGDTF(const UDMXImportGDTF&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXImportGDTF); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXImportGDTF); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXImportGDTF)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_825_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h_829_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXImportGDTF>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXImportGDTF_h


#define FOREACH_ENUM_EDMXIMPORTGDTFINTERPOLATIONTO(op) \
	op(EDMXImportGDTFInterpolationTo::Linear) \
	op(EDMXImportGDTFInterpolationTo::Step) \
	op(EDMXImportGDTFInterpolationTo::Log) 

enum class EDMXImportGDTFInterpolationTo : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFInterpolationTo>();

#define FOREACH_ENUM_EDMXIMPORTGDTFMODE(op) \
	op(EDMXImportGDTFMode::Custom) \
	op(EDMXImportGDTFMode::sRGB) \
	op(EDMXImportGDTFMode::ProPhoto) \
	op(EDMXImportGDTFMode::ANSI) 

enum class EDMXImportGDTFMode : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFMode>();

#define FOREACH_ENUM_EDMXIMPORTGDTFPHYSICALUNIT(op) \
	op(EDMXImportGDTFPhysicalUnit::None) \
	op(EDMXImportGDTFPhysicalUnit::Percent) \
	op(EDMXImportGDTFPhysicalUnit::Length) \
	op(EDMXImportGDTFPhysicalUnit::Mass) \
	op(EDMXImportGDTFPhysicalUnit::Time) \
	op(EDMXImportGDTFPhysicalUnit::Temperature) \
	op(EDMXImportGDTFPhysicalUnit::LuminousIntensity) \
	op(EDMXImportGDTFPhysicalUnit::Angle) \
	op(EDMXImportGDTFPhysicalUnit::Force) \
	op(EDMXImportGDTFPhysicalUnit::Frequency) \
	op(EDMXImportGDTFPhysicalUnit::Current) \
	op(EDMXImportGDTFPhysicalUnit::Voltage) \
	op(EDMXImportGDTFPhysicalUnit::Power) \
	op(EDMXImportGDTFPhysicalUnit::Energy) \
	op(EDMXImportGDTFPhysicalUnit::Area) \
	op(EDMXImportGDTFPhysicalUnit::Volume) \
	op(EDMXImportGDTFPhysicalUnit::Speed) \
	op(EDMXImportGDTFPhysicalUnit::Acceleration) \
	op(EDMXImportGDTFPhysicalUnit::AngularSpeed) \
	op(EDMXImportGDTFPhysicalUnit::AngularAccc) \
	op(EDMXImportGDTFPhysicalUnit::WaveLength) \
	op(EDMXImportGDTFPhysicalUnit::ColorComponent) 

enum class EDMXImportGDTFPhysicalUnit : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFPhysicalUnit>();

#define FOREACH_ENUM_EDMXIMPORTGDTFPRIMITIVETYPE(op) \
	op(EDMXImportGDTFPrimitiveType::Undefined) \
	op(EDMXImportGDTFPrimitiveType::Cube) \
	op(EDMXImportGDTFPrimitiveType::Cylinder) \
	op(EDMXImportGDTFPrimitiveType::Sphere) \
	op(EDMXImportGDTFPrimitiveType::Base) \
	op(EDMXImportGDTFPrimitiveType::Yoke) \
	op(EDMXImportGDTFPrimitiveType::Head) \
	op(EDMXImportGDTFPrimitiveType::Scanner) \
	op(EDMXImportGDTFPrimitiveType::Conventional) \
	op(EDMXImportGDTFPrimitiveType::Pigtail) 

enum class EDMXImportGDTFPrimitiveType : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFPrimitiveType>();

#define FOREACH_ENUM_EDMXIMPORTGDTFBEAMTYPE(op) \
	op(EDMXImportGDTFBeamType::Wash) \
	op(EDMXImportGDTFBeamType::Spot) \
	op(EDMXImportGDTFBeamType::None) 

enum class EDMXImportGDTFBeamType : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFBeamType>();

#define FOREACH_ENUM_EDMXIMPORTGDTFLAMPTYPE(op) \
	op(EDMXImportGDTFLampType::Discharge) \
	op(EDMXImportGDTFLampType::Tungsten) \
	op(EDMXImportGDTFLampType::Halogen) \
	op(EDMXImportGDTFLampType::LED) 

enum class EDMXImportGDTFLampType : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFLampType>();

#define FOREACH_ENUM_EDMXIMPORTGDTFDMXINVERT(op) \
	op(EDMXImportGDTFDMXInvert::Yes) \
	op(EDMXImportGDTFDMXInvert::No) 

enum class EDMXImportGDTFDMXInvert : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFDMXInvert>();

#define FOREACH_ENUM_EDMXIMPORTGDTFMASTER(op) \
	op(EDMXImportGDTFMaster::None) \
	op(EDMXImportGDTFMaster::Grand) \
	op(EDMXImportGDTFMaster::Group) 

enum class EDMXImportGDTFMaster : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFMaster>();

#define FOREACH_ENUM_EDMXIMPORTGDTFSNAP(op) \
	op(EDMXImportGDTFSnap::Yes) \
	op(EDMXImportGDTFSnap::No) \
	op(EDMXImportGDTFSnap::On) \
	op(EDMXImportGDTFSnap::Off) 

enum class EDMXImportGDTFSnap : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFSnap>();

#define FOREACH_ENUM_EDMXIMPORTGDTFTYPE(op) \
	op(EDMXImportGDTFType::Multiply) \
	op(EDMXImportGDTFType::Override) 

enum class EDMXImportGDTFType : uint8;
template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
