// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/DMXTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXTypes() {}
// Cross Module References
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXRawSACN();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXRequestBase();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXRawArtNetRequest();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXRequest();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
// End Cross Module References

static_assert(std::is_polymorphic<FDMXRawSACN>() == std::is_polymorphic<FDMXRequestBase>(), "USTRUCT FDMXRawSACN cannot be polymorphic unless super FDMXRequestBase is polymorphic");

class UScriptStruct* FDMXRawSACN::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXRawSACN_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXRawSACN, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXRawSACN"), sizeof(FDMXRawSACN), Get_Z_Construct_UScriptStruct_FDMXRawSACN_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXRawSACN>()
{
	return FDMXRawSACN::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXRawSACN(FDMXRawSACN::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXRawSACN"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRawSACN
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRawSACN()
	{
		UScriptStruct::DeferCppStructOps<FDMXRawSACN>(FName(TEXT("DMXRawSACN")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRawSACN;
	struct Z_Construct_UScriptStruct_FDMXRawSACN_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Universe_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Universe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Address;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawSACN_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXRawSACN>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Universe_MetaData[] = {
		{ "Category", "DMX|RawRequest" },
		{ "ClampMax", "63999" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "UIMax", "63999" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Universe = { "Universe", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRawSACN, Universe), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Universe_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Universe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Address_MetaData[] = {
		{ "Category", "DMX|RawRequest" },
		{ "ClampMax", "512" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "UIMax", "512" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRawSACN, Address), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Address_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXRawSACN_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Universe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRawSACN_Statics::NewProp_Address,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXRawSACN_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXRequestBase,
		&NewStructOps,
		"DMXRawSACN",
		sizeof(FDMXRawSACN),
		alignof(FDMXRawSACN),
		Z_Construct_UScriptStruct_FDMXRawSACN_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawSACN_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXRawSACN()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXRawSACN_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXRawSACN"), sizeof(FDMXRawSACN), Get_Z_Construct_UScriptStruct_FDMXRawSACN_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXRawSACN_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXRawSACN_Hash() { return 4196879154U; }

static_assert(std::is_polymorphic<FDMXRawArtNetRequest>() == std::is_polymorphic<FDMXRequestBase>(), "USTRUCT FDMXRawArtNetRequest cannot be polymorphic unless super FDMXRequestBase is polymorphic");

class UScriptStruct* FDMXRawArtNetRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXRawArtNetRequest, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXRawArtNetRequest"), sizeof(FDMXRawArtNetRequest), Get_Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXRawArtNetRequest>()
{
	return FDMXRawArtNetRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXRawArtNetRequest(FDMXRawArtNetRequest::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXRawArtNetRequest"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRawArtNetRequest
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRawArtNetRequest()
	{
		UScriptStruct::DeferCppStructOps<FDMXRawArtNetRequest>(FName(TEXT("DMXRawArtNetRequest")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRawArtNetRequest;
	struct Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Net_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Net;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubNet_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SubNet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Universe_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Universe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Address;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXRawArtNetRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Net_MetaData[] = {
		{ "Category", "DMX|RawRequest" },
		{ "ClampMax", "137" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "UIMax", "137" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Net = { "Net", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRawArtNetRequest, Net), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Net_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Net_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_SubNet_MetaData[] = {
		{ "Category", "DMX|RawRequest" },
		{ "ClampMax", "15" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "UIMax", "15" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_SubNet = { "SubNet", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRawArtNetRequest, SubNet), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_SubNet_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_SubNet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Universe_MetaData[] = {
		{ "Category", "DMX|RawRequest" },
		{ "ClampMax", "15" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "UIMax", "15" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Universe = { "Universe", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRawArtNetRequest, Universe), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Universe_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Universe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Address_MetaData[] = {
		{ "Category", "DMX|RawRequest" },
		{ "ClampMax", "512" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "UIMax", "512" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRawArtNetRequest, Address), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Address_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Net,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_SubNet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Universe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::NewProp_Address,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXRequestBase,
		&NewStructOps,
		"DMXRawArtNetRequest",
		sizeof(FDMXRawArtNetRequest),
		alignof(FDMXRawArtNetRequest),
		Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXRawArtNetRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXRawArtNetRequest"), sizeof(FDMXRawArtNetRequest), Get_Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Hash() { return 709103837U; }

static_assert(std::is_polymorphic<FDMXRequest>() == std::is_polymorphic<FDMXRequestBase>(), "USTRUCT FDMXRequest cannot be polymorphic unless super FDMXRequestBase is polymorphic");

class UScriptStruct* FDMXRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXRequest, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXRequest"), sizeof(FDMXRequest), Get_Z_Construct_UScriptStruct_FDMXRequest_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXRequest>()
{
	return FDMXRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXRequest(FDMXRequest::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXRequest"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRequest
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRequest()
	{
		UScriptStruct::DeferCppStructOps<FDMXRequest>(FName(TEXT("DMXRequest")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRequest;
	struct Z_Construct_UScriptStruct_FDMXRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DMXLibrary;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRequest_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRequest_Statics::NewProp_DMXLibrary_MetaData[] = {
		{ "Category", "DMX|Request" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FDMXRequest_Statics::NewProp_DMXLibrary = { "DMXLibrary", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRequest, DMXLibrary), Z_Construct_UClass_UDMXLibrary_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRequest_Statics::NewProp_DMXLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRequest_Statics::NewProp_DMXLibrary_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRequest_Statics::NewProp_DMXLibrary,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXRequestBase,
		&NewStructOps,
		"DMXRequest",
		sizeof(FDMXRequest),
		alignof(FDMXRequest),
		Z_Construct_UScriptStruct_FDMXRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXRequest"), sizeof(FDMXRequest), Get_Z_Construct_UScriptStruct_FDMXRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXRequest_Hash() { return 3552695068U; }
class UScriptStruct* FDMXRequestBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXRequestBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXRequestBase, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXRequestBase"), sizeof(FDMXRequestBase), Get_Z_Construct_UScriptStruct_FDMXRequestBase_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXRequestBase>()
{
	return FDMXRequestBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXRequestBase(FDMXRequestBase::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXRequestBase"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRequestBase
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRequestBase()
	{
		UScriptStruct::DeferCppStructOps<FDMXRequestBase>(FName(TEXT("DMXRequestBase")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXRequestBase;
	struct Z_Construct_UScriptStruct_FDMXRequestBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRequestBase_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXRequestBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXRequestBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXRequestBase_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXRequestBase_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXRequestBase, Value), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRequestBase_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRequestBase_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXRequestBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXRequestBase_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXRequestBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXRequestBase",
		sizeof(FDMXRequestBase),
		alignof(FDMXRequestBase),
		Z_Construct_UScriptStruct_FDMXRequestBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRequestBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXRequestBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXRequestBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXRequestBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXRequestBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXRequestBase"), sizeof(FDMXRequestBase), Get_Z_Construct_UScriptStruct_FDMXRequestBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXRequestBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXRequestBase_Hash() { return 3014596338U; }
class UScriptStruct* FDMXNormalizedAttributeValueMap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXNormalizedAttributeValueMap"), sizeof(FDMXNormalizedAttributeValueMap), Get_Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXNormalizedAttributeValueMap>()
{
	return FDMXNormalizedAttributeValueMap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXNormalizedAttributeValueMap(FDMXNormalizedAttributeValueMap::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXNormalizedAttributeValueMap"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXNormalizedAttributeValueMap
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXNormalizedAttributeValueMap()
	{
		UScriptStruct::DeferCppStructOps<FDMXNormalizedAttributeValueMap>(FName(TEXT("DMXNormalizedAttributeValueMap")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXNormalizedAttributeValueMap;
	struct Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Map_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Map_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Map_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Map;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "DMX" },
		{ "Comment", "// Holds an array Attribute Names with their normalized Values (expand the property to see the map)\n" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
		{ "ToolTip", "Holds an array Attribute Names with their normalized Values (expand the property to see the map)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXNormalizedAttributeValueMap>();
	}
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_ValueProp = { "Map", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_Key_KeyProp = { "Map_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/DMXTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map = { "Map", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXNormalizedAttributeValueMap, Map), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::NewProp_Map,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXNormalizedAttributeValueMap",
		sizeof(FDMXNormalizedAttributeValueMap),
		alignof(FDMXNormalizedAttributeValueMap),
		Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXNormalizedAttributeValueMap"), sizeof(FDMXNormalizedAttributeValueMap), Get_Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Hash() { return 2041010357U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
