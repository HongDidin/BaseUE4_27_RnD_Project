// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Sequencer/MovieSceneDMXLibrarySection.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneDMXLibrarySection() {}
// Cross Module References
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixturePatchChannel();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneFloatChannel();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UMovieSceneDMXLibrarySection_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UMovieSceneDMXLibrarySection();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister();
// End Cross Module References
class UScriptStruct* FDMXFixturePatchChannel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixturePatchChannel, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXFixturePatchChannel"), sizeof(FDMXFixturePatchChannel), Get_Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXFixturePatchChannel>()
{
	return FDMXFixturePatchChannel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixturePatchChannel(FDMXFixturePatchChannel::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXFixturePatchChannel"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixturePatchChannel
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixturePatchChannel()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixturePatchChannel>(FName(TEXT("DMXFixturePatchChannel")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixturePatchChannel;
	struct Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXLibrary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reference_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Reference;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionChannels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FunctionChannels;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActiveMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixturePatchChannel>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_DMXLibrary_MetaData[] = {
		{ "Comment", "/** The outer library of the channel */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "The outer library of the channel" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_DMXLibrary = { "DMXLibrary", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixturePatchChannel, DMXLibrary), Z_Construct_UClass_UDMXLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_DMXLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_DMXLibrary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_Reference_MetaData[] = {
		{ "Comment", "/** Points to the Fixture Patch */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Points to the Fixture Patch" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_Reference = { "Reference", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixturePatchChannel, Reference), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_Reference_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_Reference_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels_Inner = { "FunctionChannels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels_MetaData[] = {
		{ "Comment", "/** Fixture function float channels */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Fixture function float channels" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels = { "FunctionChannels", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixturePatchChannel, FunctionChannels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_ActiveMode_MetaData[] = {
		{ "Comment", "/**\n\x09 * Allows Sequencer to animate the fixture using a mode and not have it break\n\x09 * simply by the user changing the active mode in the DMX Library.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Allows Sequencer to animate the fixture using a mode and not have it break\nsimply by the user changing the active mode in the DMX Library." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_ActiveMode = { "ActiveMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixturePatchChannel, ActiveMode), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_ActiveMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_ActiveMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_DMXLibrary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_Reference,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_FunctionChannels,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::NewProp_ActiveMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXFixturePatchChannel",
		sizeof(FDMXFixturePatchChannel),
		alignof(FDMXFixturePatchChannel),
		Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixturePatchChannel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixturePatchChannel"), sizeof(FDMXFixturePatchChannel), Get_Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixturePatchChannel_Hash() { return 3049489390U; }
class UScriptStruct* FDMXFixtureFunctionChannel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXFixtureFunctionChannel"), sizeof(FDMXFixtureFunctionChannel), Get_Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXFixtureFunctionChannel>()
{
	return FDMXFixtureFunctionChannel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixtureFunctionChannel(FDMXFixtureFunctionChannel::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXFixtureFunctionChannel"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureFunctionChannel
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureFunctionChannel()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixtureFunctionChannel>(FName(TEXT("DMXFixtureFunctionChannel")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureFunctionChannel;
	struct Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Channel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_DefaultValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixtureFunctionChannel>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_Channel_MetaData[] = {
		{ "Comment", "/** Function animation curve. */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Function animation curve." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunctionChannel, Channel), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_Channel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Comment", "/** Default value to use when this Function is disabled in the track. */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Default value to use when this Function is disabled in the track." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunctionChannel, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_DefaultValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Comment", "/**\n\x09 * Whether or not to display this Function in the Patch's group\n\x09 * If false, the Function's default value is sent to DMX protocols.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Whether or not to display this Function in the Patch's group\nIf false, the Function's default value is sent to DMX protocols." },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FDMXFixtureFunctionChannel*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXFixtureFunctionChannel), &Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::NewProp_bEnabled,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXFixtureFunctionChannel",
		sizeof(FDMXFixtureFunctionChannel),
		alignof(FDMXFixtureFunctionChannel),
		Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixtureFunctionChannel"), sizeof(FDMXFixtureFunctionChannel), Get_Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureFunctionChannel_Hash() { return 204046978U; }
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execGetNumPatches)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetNumPatches();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execGetFixturePatches)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UDMXEntityFixturePatch*>*)Z_Param__Result=P_THIS->GetFixturePatches();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execGetFixturePatchChannelEnabled)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InPatch);
		P_GET_PROPERTY(FIntProperty,Z_Param_InChannelIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetFixturePatchChannelEnabled(Z_Param_InPatch,Z_Param_InChannelIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execToggleFixturePatchChannel)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InPatch);
		P_GET_PROPERTY(FIntProperty,Z_Param_InChannelIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleFixturePatchChannel(Z_Param_InPatch,Z_Param_InChannelIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execSetFixturePatchActiveMode)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InPatch);
		P_GET_PROPERTY(FIntProperty,Z_Param_InActiveMode);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFixturePatchActiveMode(Z_Param_InPatch,Z_Param_InActiveMode);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execContainsFixturePatch)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InPatch);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ContainsFixturePatch(Z_Param_InPatch);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execRemoveFixturePatch)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InPatch);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveFixturePatch(Z_Param_InPatch);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execAddFixturePatches)
	{
		P_GET_TARRAY_REF(FDMXEntityFixturePatchRef,Z_Param_Out_InFixturePatchRefs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddFixturePatches(Z_Param_Out_InFixturePatchRefs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneDMXLibrarySection::execAddFixturePatch)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InPatch);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddFixturePatch(Z_Param_InPatch);
		P_NATIVE_END;
	}
	void UMovieSceneDMXLibrarySection::StaticRegisterNativesUMovieSceneDMXLibrarySection()
	{
		UClass* Class = UMovieSceneDMXLibrarySection::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddFixturePatch", &UMovieSceneDMXLibrarySection::execAddFixturePatch },
			{ "AddFixturePatches", &UMovieSceneDMXLibrarySection::execAddFixturePatches },
			{ "ContainsFixturePatch", &UMovieSceneDMXLibrarySection::execContainsFixturePatch },
			{ "GetFixturePatchChannelEnabled", &UMovieSceneDMXLibrarySection::execGetFixturePatchChannelEnabled },
			{ "GetFixturePatches", &UMovieSceneDMXLibrarySection::execGetFixturePatches },
			{ "GetNumPatches", &UMovieSceneDMXLibrarySection::execGetNumPatches },
			{ "RemoveFixturePatch", &UMovieSceneDMXLibrarySection::execRemoveFixturePatch },
			{ "SetFixturePatchActiveMode", &UMovieSceneDMXLibrarySection::execSetFixturePatchActiveMode },
			{ "ToggleFixturePatchChannel", &UMovieSceneDMXLibrarySection::execToggleFixturePatchChannel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics
	{
		struct MovieSceneDMXLibrarySection_eventAddFixturePatch_Parms
		{
			UDMXEntityFixturePatch* InPatch;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPatch;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::NewProp_InPatch = { "InPatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventAddFixturePatch_Parms, InPatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::NewProp_InPatch,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Scene" },
		{ "Comment", "/** Adds a single patch to the section */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Adds a single patch to the section" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "AddFixturePatch", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventAddFixturePatch_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics
	{
		struct MovieSceneDMXLibrarySection_eventAddFixturePatches_Parms
		{
			TArray<FDMXEntityFixturePatchRef> InFixturePatchRefs;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFixturePatchRefs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFixturePatchRefs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InFixturePatchRefs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs_Inner = { "InFixturePatchRefs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs = { "InFixturePatchRefs", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventAddFixturePatches_Parms, InFixturePatchRefs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::NewProp_InFixturePatchRefs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Adds all patches to the secion */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Adds all patches to the secion" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "AddFixturePatches", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventAddFixturePatches_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics
	{
		struct MovieSceneDMXLibrarySection_eventContainsFixturePatch_Parms
		{
			UDMXEntityFixturePatch* InPatch;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPatch;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::NewProp_InPatch = { "InPatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventContainsFixturePatch_Parms, InPatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MovieSceneDMXLibrarySection_eventContainsFixturePatch_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MovieSceneDMXLibrarySection_eventContainsFixturePatch_Parms), &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::NewProp_InPatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Check if this Section animates a Fixture Patch's Functions */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Check if this Section animates a Fixture Patch's Functions" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "ContainsFixturePatch", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventContainsFixturePatch_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics
	{
		struct MovieSceneDMXLibrarySection_eventGetFixturePatchChannelEnabled_Parms
		{
			UDMXEntityFixturePatch* InPatch;
			int32 InChannelIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPatch;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InChannelIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_InPatch = { "InPatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventGetFixturePatchChannelEnabled_Parms, InPatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_InChannelIndex = { "InChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventGetFixturePatchChannelEnabled_Parms, InChannelIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MovieSceneDMXLibrarySection_eventGetFixturePatchChannelEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MovieSceneDMXLibrarySection_eventGetFixturePatchChannelEnabled_Parms), &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_InPatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_InChannelIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Returns whether a Fixture Patch's Function curve channel is currently enabled */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Returns whether a Fixture Patch's Function curve channel is currently enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "GetFixturePatchChannelEnabled", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventGetFixturePatchChannelEnabled_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics
	{
		struct MovieSceneDMXLibrarySection_eventGetFixturePatches_Parms
		{
			TArray<UDMXEntityFixturePatch*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventGetFixturePatches_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Get a list of the Fixture Patches being animated by this Section */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Get a list of the Fixture Patches being animated by this Section" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "GetFixturePatches", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventGetFixturePatches_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics
	{
		struct MovieSceneDMXLibrarySection_eventGetNumPatches_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventGetNumPatches_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Get the list of animated Fixture Patches and their curve channels */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Get the list of animated Fixture Patches and their curve channels" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "GetNumPatches", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventGetNumPatches_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics
	{
		struct MovieSceneDMXLibrarySection_eventRemoveFixturePatch_Parms
		{
			UDMXEntityFixturePatch* InPatch;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPatch;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::NewProp_InPatch = { "InPatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventRemoveFixturePatch_Parms, InPatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::NewProp_InPatch,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Remove all Functions of a Fixture Patch */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Remove all Functions of a Fixture Patch" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "RemoveFixturePatch", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventRemoveFixturePatch_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics
	{
		struct MovieSceneDMXLibrarySection_eventSetFixturePatchActiveMode_Parms
		{
			UDMXEntityFixturePatch* InPatch;
			int32 InActiveMode;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPatch;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InActiveMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::NewProp_InPatch = { "InPatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventSetFixturePatchActiveMode_Parms, InPatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::NewProp_InActiveMode = { "InActiveMode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventSetFixturePatchActiveMode_Parms, InActiveMode), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::NewProp_InPatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::NewProp_InActiveMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/** Sets the active mode for a Fixture Patch */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Sets the active mode for a Fixture Patch" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "SetFixturePatchActiveMode", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventSetFixturePatchActiveMode_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics
	{
		struct MovieSceneDMXLibrarySection_eventToggleFixturePatchChannel_Parms
		{
			UDMXEntityFixturePatch* InPatch;
			int32 InChannelIndex;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPatch;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InChannelIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::NewProp_InPatch = { "InPatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventToggleFixturePatchChannel_Parms, InPatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::NewProp_InChannelIndex = { "InChannelIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneDMXLibrarySection_eventToggleFixturePatchChannel_Parms, InChannelIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::NewProp_InPatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::NewProp_InChannelIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Section" },
		{ "Comment", "/**\n\x09 * Toggle the visibility and evaluation of a Fixture Patch's Function.\n\x09 * When invisible, the Function won't send its data to DMX Protocol.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "Toggle the visibility and evaluation of a Fixture Patch's Function.\nWhen invisible, the Function won't send its data to DMX Protocol." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneDMXLibrarySection, nullptr, "ToggleFixturePatchChannel", nullptr, nullptr, sizeof(MovieSceneDMXLibrarySection_eventToggleFixturePatchChannel_Parms), Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMovieSceneDMXLibrarySection_NoRegister()
	{
		return UMovieSceneDMXLibrarySection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseNormalizedValues_MetaData[];
#endif
		static void NewProp_bUseNormalizedValues_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseNormalizedValues;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchChannels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FixturePatchChannels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneSection,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatch, "AddFixturePatch" }, // 786388827
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_AddFixturePatches, "AddFixturePatches" }, // 3629303357
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ContainsFixturePatch, "ContainsFixturePatch" }, // 1018756869
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatchChannelEnabled, "GetFixturePatchChannelEnabled" }, // 776304780
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetFixturePatches, "GetFixturePatches" }, // 2879686221
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_GetNumPatches, "GetNumPatches" }, // 2346369525
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_RemoveFixturePatch, "RemoveFixturePatch" }, // 2486569072
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_SetFixturePatchActiveMode, "SetFixturePatchActiveMode" }, // 467214881
		{ &Z_Construct_UFunction_UMovieSceneDMXLibrarySection_ToggleFixturePatchChannel, "ToggleFixturePatchChannel" }, // 719139232
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** A DMX Fixture Patch section */" },
		{ "IncludePath", "Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "A DMX Fixture Patch section" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues_MetaData[] = {
		{ "Category", "Movie Scene" },
		{ "Comment", "/** \n\x09 * If true, all values are interpreted as normalized values (0.0 to 1.0) \n\x09 * and are mapped to the actual value range of a patch automatically. \n\x09 * \n\x09 * If false, values are interpreted as absolute values, depending on the data type of a patch:\n\x09 * 0-255 for 8bit, 0-65'536 for 16bit, 0-16'777'215 for 24bit. 32bit is not fully supported in this mode.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "If true, all values are interpreted as normalized values (0.0 to 1.0)\nand are mapped to the actual value range of a patch automatically.\n\nIf false, values are interpreted as absolute values, depending on the data type of a patch:\n0-255 for 8bit, 0-65'536 for 16bit, 0-16'777'215 for 24bit. 32bit is not fully supported in this mode." },
	};
#endif
	void Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues_SetBit(void* Obj)
	{
		((UMovieSceneDMXLibrarySection*)Obj)->bUseNormalizedValues = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues = { "bUseNormalizedValues", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMovieSceneDMXLibrarySection), &Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels_Inner = { "FixturePatchChannels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXFixturePatchChannel, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels_MetaData[] = {
		{ "Comment", "/** The Fixture Patches being controlled by this section and their respective chosen mode */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibrarySection.h" },
		{ "ToolTip", "The Fixture Patches being controlled by this section and their respective chosen mode" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels = { "FixturePatchChannels", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneDMXLibrarySection, FixturePatchChannels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_bUseNormalizedValues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::NewProp_FixturePatchChannels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneDMXLibrarySection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::ClassParams = {
		&UMovieSceneDMXLibrarySection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::PropPointers),
		0,
		0x003000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneDMXLibrarySection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneDMXLibrarySection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneDMXLibrarySection, 3772254388);
	template<> DMXRUNTIME_API UClass* StaticClass<UMovieSceneDMXLibrarySection>()
	{
		return UMovieSceneDMXLibrarySection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneDMXLibrarySection(Z_Construct_UClass_UMovieSceneDMXLibrarySection, &UMovieSceneDMXLibrarySection::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UMovieSceneDMXLibrarySection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneDMXLibrarySection);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UMovieSceneDMXLibrarySection)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
