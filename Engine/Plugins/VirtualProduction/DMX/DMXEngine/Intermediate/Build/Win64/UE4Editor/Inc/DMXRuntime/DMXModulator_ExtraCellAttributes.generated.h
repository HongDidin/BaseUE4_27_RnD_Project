// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXRUNTIME_DMXModulator_ExtraCellAttributes_generated_h
#error "DMXModulator_ExtraCellAttributes.generated.h already included, missing '#pragma once' in DMXModulator_ExtraCellAttributes.h"
#endif
#define DMXRUNTIME_DMXModulator_ExtraCellAttributes_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXModulator_ExtraCellAttributes(); \
	friend struct Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator_ExtraCellAttributes, UDMXModulator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator_ExtraCellAttributes)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDMXModulator_ExtraCellAttributes(); \
	friend struct Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator_ExtraCellAttributes, UDMXModulator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator_ExtraCellAttributes)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXModulator_ExtraCellAttributes(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXModulator_ExtraCellAttributes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator_ExtraCellAttributes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator_ExtraCellAttributes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator_ExtraCellAttributes(UDMXModulator_ExtraCellAttributes&&); \
	NO_API UDMXModulator_ExtraCellAttributes(const UDMXModulator_ExtraCellAttributes&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator_ExtraCellAttributes(UDMXModulator_ExtraCellAttributes&&); \
	NO_API UDMXModulator_ExtraCellAttributes(const UDMXModulator_ExtraCellAttributes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator_ExtraCellAttributes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator_ExtraCellAttributes); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXModulator_ExtraCellAttributes)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_11_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXModulator_ExtraCellAttributes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_ExtraCellAttributes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
