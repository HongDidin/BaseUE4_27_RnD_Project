// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Private/Modulators/DMXModulator_ExtraCellAttributes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXModulator_ExtraCellAttributes() {}
// Cross Module References
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_ExtraCellAttributes();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
// End Cross Module References
	void UDMXModulator_ExtraCellAttributes::StaticRegisterNativesUDMXModulator_ExtraCellAttributes()
	{
	}
	UClass* Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_NoRegister()
	{
		return UDMXModulator_ExtraCellAttributes::StaticClass();
	}
	struct Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ExtraAttributeNameToNormalizedValueMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExtraAttributeNameToNormalizedValueMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtraAttributeNameToNormalizedValueMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ExtraAttributeNameToNormalizedValueMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXModulator,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "DMX" },
		{ "Comment", "/** Adds matrix attributes that are not received (e.g. because DMX was generated from PixelMapping) to the DMX signal */" },
		{ "DisplayName", "DMX Modulator Extra Cell Attributes" },
		{ "IncludePath", "Modulators/DMXModulator_ExtraCellAttributes.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_ExtraCellAttributes.h" },
		{ "ToolTip", "Adds matrix attributes that are not received (e.g. because DMX was generated from PixelMapping) to the DMX signal" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_ValueProp = { "ExtraAttributeNameToNormalizedValueMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_Key_KeyProp = { "ExtraAttributeNameToNormalizedValueMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_MetaData[] = {
		{ "Category", "Extra Cell Attributes" },
		{ "Comment", "/** Adds the attributes with their values to the Output if they don't exist, or replaces them with the values specified */" },
		{ "DisplayName", "Attribute to Normalized Value Map" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_ExtraCellAttributes.h" },
		{ "ToolTip", "Adds the attributes with their values to the Output if they don't exist, or replaces them with the values specified" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap = { "ExtraAttributeNameToNormalizedValueMap", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_ExtraCellAttributes, ExtraAttributeNameToNormalizedValueMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::NewProp_ExtraAttributeNameToNormalizedValueMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXModulator_ExtraCellAttributes>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::ClassParams = {
		&UDMXModulator_ExtraCellAttributes::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXModulator_ExtraCellAttributes()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXModulator_ExtraCellAttributes_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXModulator_ExtraCellAttributes, 559754972);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXModulator_ExtraCellAttributes>()
	{
		return UDMXModulator_ExtraCellAttributes::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXModulator_ExtraCellAttributes(Z_Construct_UClass_UDMXModulator_ExtraCellAttributes, &UDMXModulator_ExtraCellAttributes::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXModulator_ExtraCellAttributes"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXModulator_ExtraCellAttributes);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
