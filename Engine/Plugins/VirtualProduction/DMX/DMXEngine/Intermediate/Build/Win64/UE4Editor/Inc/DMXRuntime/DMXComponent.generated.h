// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDMXEntityFixturePatch;
struct FDMXNormalizedAttributeValueMap;
#ifdef DMXRUNTIME_DMXComponent_generated_h
#error "DMXComponent.generated.h already included, missing '#pragma once' in DMXComponent.h"
#endif
#define DMXRUNTIME_DMXComponent_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_29_DELEGATE \
struct DMXComponent_eventDMXComponentFixturePatchReceivedSignature_Parms \
{ \
	UDMXEntityFixturePatch* FixturePatch; \
	FDMXNormalizedAttributeValueMap ValuePerAttribute; \
}; \
static inline void FDMXComponentFixturePatchReceivedSignature_DelegateWrapper(const FMulticastScriptDelegate& DMXComponentFixturePatchReceivedSignature, UDMXEntityFixturePatch* FixturePatch, FDMXNormalizedAttributeValueMap const& ValuePerAttribute) \
{ \
	DMXComponent_eventDMXComponentFixturePatchReceivedSignature_Parms Parms; \
	Parms.FixturePatch=FixturePatch; \
	Parms.ValuePerAttribute=ValuePerAttribute; \
	DMXComponentFixturePatchReceivedSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnFixturePatchReceivedDMX); \
	DECLARE_FUNCTION(execSetReceiveDMXFromPatch); \
	DECLARE_FUNCTION(execSetFixturePatch); \
	DECLARE_FUNCTION(execGetFixturePatch);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnFixturePatchReceivedDMX); \
	DECLARE_FUNCTION(execSetReceiveDMXFromPatch); \
	DECLARE_FUNCTION(execSetFixturePatch); \
	DECLARE_FUNCTION(execGetFixturePatch);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXComponent(); \
	friend struct Z_Construct_UClass_UDMXComponent_Statics; \
public: \
	DECLARE_CLASS(UDMXComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUDMXComponent(); \
	friend struct Z_Construct_UClass_UDMXComponent_Statics; \
public: \
	DECLARE_CLASS(UDMXComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXComponent(UDMXComponent&&); \
	NO_API UDMXComponent(const UDMXComponent&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXComponent(UDMXComponent&&); \
	NO_API UDMXComponent(const UDMXComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXComponent)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bReceiveDMXFromPatch() { return STRUCT_OFFSET(UDMXComponent, bReceiveDMXFromPatch); }


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_23_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Game_DMXComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
