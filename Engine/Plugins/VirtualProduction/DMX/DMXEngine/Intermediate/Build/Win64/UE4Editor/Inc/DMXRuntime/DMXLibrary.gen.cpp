// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXLibrary() {}
// Cross Module References
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXLibraryPortReferences();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXInputPortReference();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputPortReference();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXObjectBase();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity_NoRegister();
// End Cross Module References
class UScriptStruct* FDMXLibraryPortReferences::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXLibraryPortReferences, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXLibraryPortReferences"), sizeof(FDMXLibraryPortReferences), Get_Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXLibraryPortReferences>()
{
	return FDMXLibraryPortReferences::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXLibraryPortReferences(FDMXLibraryPortReferences::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXLibraryPortReferences"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXLibraryPortReferences
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXLibraryPortReferences()
	{
		UScriptStruct::DeferCppStructOps<FDMXLibraryPortReferences>(FName(TEXT("DMXLibraryPortReferences")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXLibraryPortReferences;
	struct Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputPortReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputPortReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputPortReferences;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputPortReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputPortReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputPortReferences;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Custom struct of in put and output port references for custom details customization with an enabled state */" },
		{ "ModuleRelativePath", "Public/Library/DMXLibrary.h" },
		{ "ToolTip", "Custom struct of in put and output port references for custom details customization with an enabled state" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXLibraryPortReferences>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences_Inner = { "InputPortReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXInputPortReference, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Map of input port references of a Library */" },
		{ "DisplayName", "Input Ports" },
		{ "ModuleRelativePath", "Public/Library/DMXLibrary.h" },
		{ "ToolTip", "Map of input port references of a Library" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences = { "InputPortReferences", nullptr, (EPropertyFlags)0x0010000400000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXLibraryPortReferences, InputPortReferences), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences_Inner = { "OutputPortReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXOutputPortReference, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Output ports of the Library of a Library */" },
		{ "DisplayName", "Output Ports" },
		{ "ModuleRelativePath", "Public/Library/DMXLibrary.h" },
		{ "ToolTip", "Output ports of the Library of a Library" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences = { "OutputPortReferences", nullptr, (EPropertyFlags)0x0010000400000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXLibraryPortReferences, OutputPortReferences), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_InputPortReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::NewProp_OutputPortReferences,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXLibraryPortReferences",
		sizeof(FDMXLibraryPortReferences),
		alignof(FDMXLibraryPortReferences),
		Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXLibraryPortReferences()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXLibraryPortReferences"), sizeof(FDMXLibraryPortReferences), Get_Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXLibraryPortReferences_Hash() { return 774633326U; }
	void UDMXLibrary::StaticRegisterNativesUDMXLibrary()
	{
	}
	UClass* Z_Construct_UClass_UDMXLibrary_NoRegister()
	{
		return UDMXLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UDMXLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PortReferences;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Entities_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Entities_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Entities;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXObjectBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXLibrary_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "DMX" },
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXLibrary.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXLibrary.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXLibrary_Statics::NewProp_PortReferences_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Input ports of the Library */" },
		{ "ModuleRelativePath", "Public/Library/DMXLibrary.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Input ports of the Library" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXLibrary_Statics::NewProp_PortReferences = { "PortReferences", nullptr, (EPropertyFlags)0x0020080400000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXLibrary, PortReferences), Z_Construct_UScriptStruct_FDMXLibraryPortReferences, METADATA_PARAMS(Z_Construct_UClass_UDMXLibrary_Statics::NewProp_PortReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXLibrary_Statics::NewProp_PortReferences_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities_Inner = { "Entities", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXEntity_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities_MetaData[] = {
		{ "Comment", "/** All Fixture Types and Fixture Patches in the library */" },
		{ "ModuleRelativePath", "Public/Library/DMXLibrary.h" },
		{ "ToolTip", "All Fixture Types and Fixture Patches in the library" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities = { "Entities", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXLibrary, Entities), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXLibrary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXLibrary_Statics::NewProp_PortReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXLibrary_Statics::NewProp_Entities,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXLibrary_Statics::ClassParams = {
		&UDMXLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXLibrary_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXLibrary_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXLibrary, 2868673861);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXLibrary>()
	{
		return UDMXLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXLibrary(Z_Construct_UClass_UDMXLibrary, &UDMXLibrary::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
