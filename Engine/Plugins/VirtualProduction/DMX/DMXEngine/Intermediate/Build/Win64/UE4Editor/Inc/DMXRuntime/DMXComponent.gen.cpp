// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Game/DMXComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXComponent() {}
// Cross Module References
	DMXRUNTIME_API UFunction* Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXComponent();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics
	{
		struct DMXComponent_eventDMXComponentFixturePatchReceivedSignature_Parms
		{
			UDMXEntityFixturePatch* FixturePatch;
			FDMXNormalizedAttributeValueMap ValuePerAttribute;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FixturePatch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValuePerAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ValuePerAttribute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_FixturePatch = { "FixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXComponent_eventDMXComponentFixturePatchReceivedSignature_Parms, FixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_ValuePerAttribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_ValuePerAttribute = { "ValuePerAttribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXComponent_eventDMXComponentFixturePatchReceivedSignature_Parms, ValuePerAttribute), Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_ValuePerAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_ValuePerAttribute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_FixturePatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::NewProp_ValuePerAttribute,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXComponent, nullptr, "DMXComponentFixturePatchReceivedSignature__DelegateSignature", nullptr, nullptr, sizeof(DMXComponent_eventDMXComponentFixturePatchReceivedSignature_Parms), Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00530000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UDMXComponent::execOnFixturePatchReceivedDMX)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_FixturePatch);
		P_GET_STRUCT_REF(FDMXNormalizedAttributeValueMap,Z_Param_Out_NormalizedValuePerAttribute);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnFixturePatchReceivedDMX(Z_Param_FixturePatch,Z_Param_Out_NormalizedValuePerAttribute);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXComponent::execSetReceiveDMXFromPatch)
	{
		P_GET_UBOOL(Z_Param_bReceive);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetReceiveDMXFromPatch(Z_Param_bReceive);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXComponent::execSetFixturePatch)
	{
		P_GET_OBJECT(UDMXEntityFixturePatch,Z_Param_InFixturePatch);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFixturePatch(Z_Param_InFixturePatch);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXComponent::execGetFixturePatch)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDMXEntityFixturePatch**)Z_Param__Result=P_THIS->GetFixturePatch();
		P_NATIVE_END;
	}
	void UDMXComponent::StaticRegisterNativesUDMXComponent()
	{
		UClass* Class = UDMXComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetFixturePatch", &UDMXComponent::execGetFixturePatch },
			{ "OnFixturePatchReceivedDMX", &UDMXComponent::execOnFixturePatchReceivedDMX },
			{ "SetFixturePatch", &UDMXComponent::execSetFixturePatch },
			{ "SetReceiveDMXFromPatch", &UDMXComponent::execSetReceiveDMXFromPatch },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics
	{
		struct DMXComponent_eventGetFixturePatch_Parms
		{
			UDMXEntityFixturePatch* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXComponent_eventGetFixturePatch_Parms, ReturnValue), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Gets the fixture patch used in the component */" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "Gets the fixture patch used in the component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXComponent, nullptr, "GetFixturePatch", nullptr, nullptr, sizeof(DMXComponent_eventGetFixturePatch_Parms), Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXComponent_GetFixturePatch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXComponent_GetFixturePatch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics
	{
		struct DMXComponent_eventOnFixturePatchReceivedDMX_Parms
		{
			UDMXEntityFixturePatch* FixturePatch;
			FDMXNormalizedAttributeValueMap NormalizedValuePerAttribute;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FixturePatch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalizedValuePerAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NormalizedValuePerAttribute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_FixturePatch = { "FixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXComponent_eventOnFixturePatchReceivedDMX_Parms, FixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_NormalizedValuePerAttribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_NormalizedValuePerAttribute = { "NormalizedValuePerAttribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXComponent_eventOnFixturePatchReceivedDMX_Parms, NormalizedValuePerAttribute), Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_NormalizedValuePerAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_NormalizedValuePerAttribute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_FixturePatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::NewProp_NormalizedValuePerAttribute,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Called when the fixture patch received DMX */" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "Called when the fixture patch received DMX" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXComponent, nullptr, "OnFixturePatchReceivedDMX", nullptr, nullptr, sizeof(DMXComponent_eventOnFixturePatchReceivedDMX_Parms), Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics
	{
		struct DMXComponent_eventSetFixturePatch_Parms
		{
			UDMXEntityFixturePatch* InFixturePatch;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InFixturePatch;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::NewProp_InFixturePatch = { "InFixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXComponent_eventSetFixturePatch_Parms, InFixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::NewProp_InFixturePatch,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Sets the fixture patch used in the component */" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "Sets the fixture patch used in the component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXComponent, nullptr, "SetFixturePatch", nullptr, nullptr, sizeof(DMXComponent_eventSetFixturePatch_Parms), Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXComponent_SetFixturePatch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXComponent_SetFixturePatch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics
	{
		struct DMXComponent_eventSetReceiveDMXFromPatch_Parms
		{
			bool bReceive;
		};
		static void NewProp_bReceive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReceive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::NewProp_bReceive_SetBit(void* Obj)
	{
		((DMXComponent_eventSetReceiveDMXFromPatch_Parms*)Obj)->bReceive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::NewProp_bReceive = { "bReceive", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXComponent_eventSetReceiveDMXFromPatch_Parms), &Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::NewProp_bReceive_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::NewProp_bReceive,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Sets whether the component receives dmx from the patch. Note, this is saved with the component when called in editor. */" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "Sets whether the component receives dmx from the patch. Note, this is saved with the component when called in editor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXComponent, nullptr, "SetReceiveDMXFromPatch", nullptr, nullptr, sizeof(DMXComponent_eventSetReceiveDMXFromPatch_Parms), Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXComponent_NoRegister()
	{
		return UDMXComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDMXComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFixturePatchReceived_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFixturePatchReceived;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchRef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReceiveDMXFromPatch_MetaData[];
#endif
		static void NewProp_bReceiveDMXFromPatch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReceiveDMXFromPatch;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature, "DMXComponentFixturePatchReceivedSignature__DelegateSignature" }, // 1711243151
		{ &Z_Construct_UFunction_UDMXComponent_GetFixturePatch, "GetFixturePatch" }, // 2116959280
		{ &Z_Construct_UFunction_UDMXComponent_OnFixturePatchReceivedDMX, "OnFixturePatchReceivedDMX" }, // 209188995
		{ &Z_Construct_UFunction_UDMXComponent_SetFixturePatch, "SetFixturePatch" }, // 4153192690
		{ &Z_Construct_UFunction_UDMXComponent_SetReceiveDMXFromPatch, "SetReceiveDMXFromPatch" }, // 3254434182
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "DMX" },
		{ "Comment", "/** \n * Component that receives DMX input each Tick from a fixture patch.  \n * NOTE: Does not support receive in Editor! Use the 'Get DMX Fixture Patch' and bind 'On Fixture Patch Received DMX' instead (requires the patch to be set to 'Receive DMX in Editor' in the library). \n */" },
		{ "HideCategories", "Variable Sockets Tags Activation Cooking ComponentReplication AssetUserData Collision Events" },
		{ "IncludePath", "Game/DMXComponent.h" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "Component that receives DMX input each Tick from a fixture patch.\nNOTE: Does not support receive in Editor! Use the 'Get DMX Fixture Patch' and bind 'On Fixture Patch Received DMX' instead (requires the patch to be set to 'Receive DMX in Editor' in the library)." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXComponent_Statics::NewProp_OnFixturePatchReceived_MetaData[] = {
		{ "Category", "Components|DMX" },
		{ "Comment", "/** Broadcast when the component's fixture patch received DMX */" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "Broadcast when the component's fixture patch received DMX" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UDMXComponent_Statics::NewProp_OnFixturePatchReceived = { "OnFixturePatchReceived", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXComponent, OnFixturePatchReceived), Z_Construct_UDelegateFunction_UDMXComponent_DMXComponentFixturePatchReceivedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UDMXComponent_Statics::NewProp_OnFixturePatchReceived_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXComponent_Statics::NewProp_OnFixturePatchReceived_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXComponent_Statics::NewProp_FixturePatchRef_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXComponent_Statics::NewProp_FixturePatchRef = { "FixturePatchRef", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXComponent, FixturePatchRef), Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(Z_Construct_UClass_UDMXComponent_Statics::NewProp_FixturePatchRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXComponent_Statics::NewProp_FixturePatchRef_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** If true, the component will receive DMX from the patch */" },
		{ "ModuleRelativePath", "Public/Game/DMXComponent.h" },
		{ "ToolTip", "If true, the component will receive DMX from the patch" },
	};
#endif
	void Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch_SetBit(void* Obj)
	{
		((UDMXComponent*)Obj)->bReceiveDMXFromPatch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch = { "bReceiveDMXFromPatch", nullptr, (EPropertyFlags)0x00200c0000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXComponent), &Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXComponent_Statics::NewProp_OnFixturePatchReceived,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXComponent_Statics::NewProp_FixturePatchRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXComponent_Statics::NewProp_bReceiveDMXFromPatch,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXComponent_Statics::ClassParams = {
		&UDMXComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXComponent, 691899995);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXComponent>()
	{
		return UDMXComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXComponent(Z_Construct_UClass_UDMXComponent, &UDMXComponent::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
