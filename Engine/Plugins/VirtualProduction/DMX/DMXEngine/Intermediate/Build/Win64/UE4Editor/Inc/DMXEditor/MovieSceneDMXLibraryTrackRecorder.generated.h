// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXEDITOR_MovieSceneDMXLibraryTrackRecorder_generated_h
#error "MovieSceneDMXLibraryTrackRecorder.generated.h already included, missing '#pragma once' in MovieSceneDMXLibraryTrackRecorder.h"
#endif
#define DMXEDITOR_MovieSceneDMXLibraryTrackRecorder_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneDMXLibraryTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneDMXLibraryTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/DMXEditor"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneDMXLibraryTrackRecorder)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneDMXLibraryTrackRecorder(); \
	friend struct Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneDMXLibraryTrackRecorder, UMovieSceneTrackRecorder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/DMXEditor"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneDMXLibraryTrackRecorder)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneDMXLibraryTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneDMXLibraryTrackRecorder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneDMXLibraryTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneDMXLibraryTrackRecorder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneDMXLibraryTrackRecorder(UMovieSceneDMXLibraryTrackRecorder&&); \
	NO_API UMovieSceneDMXLibraryTrackRecorder(const UMovieSceneDMXLibraryTrackRecorder&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneDMXLibraryTrackRecorder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneDMXLibraryTrackRecorder(UMovieSceneDMXLibraryTrackRecorder&&); \
	NO_API UMovieSceneDMXLibraryTrackRecorder(const UMovieSceneDMXLibraryTrackRecorder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneDMXLibraryTrackRecorder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneDMXLibraryTrackRecorder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneDMXLibraryTrackRecorder)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FixturePatchRefs() { return STRUCT_OFFSET(UMovieSceneDMXLibraryTrackRecorder, FixturePatchRefs); }


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_31_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXEDITOR_API UClass* StaticClass<class UMovieSceneDMXLibraryTrackRecorder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Sequencer_MovieSceneDMXLibraryTrackRecorder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
