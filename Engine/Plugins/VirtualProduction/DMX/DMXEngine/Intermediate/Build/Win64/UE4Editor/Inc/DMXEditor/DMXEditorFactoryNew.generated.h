// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXEDITOR_DMXEditorFactoryNew_generated_h
#error "DMXEditorFactoryNew.generated.h already included, missing '#pragma once' in DMXEditorFactoryNew.h"
#endif
#define DMXEDITOR_DMXEditorFactoryNew_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXEditorFactoryNew(); \
	friend struct Z_Construct_UClass_UDMXEditorFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UDMXEditorFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXEditor"), NO_API) \
	DECLARE_SERIALIZER(UDMXEditorFactoryNew)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUDMXEditorFactoryNew(); \
	friend struct Z_Construct_UClass_UDMXEditorFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UDMXEditorFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXEditor"), NO_API) \
	DECLARE_SERIALIZER(UDMXEditorFactoryNew)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEditorFactoryNew(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEditorFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEditorFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEditorFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEditorFactoryNew(UDMXEditorFactoryNew&&); \
	NO_API UDMXEditorFactoryNew(const UDMXEditorFactoryNew&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEditorFactoryNew(UDMXEditorFactoryNew&&); \
	NO_API UDMXEditorFactoryNew(const UDMXEditorFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEditorFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEditorFactoryNew); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXEditorFactoryNew)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_12_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXEDITOR_API UClass* StaticClass<class UDMXEditorFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Public_Factories_DMXEditorFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
