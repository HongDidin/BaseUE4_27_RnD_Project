// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDMXEntityFixturePatch;
struct FDMXEntityFixturePatchRef;
class UDMXEntityFixtureType;
struct FDMXEntityFixtureTypeRef;
class UDMXEntityController;
struct FDMXEntityControllerRef;
#ifdef DMXRUNTIME_DMXEntityReference_generated_h
#error "DMXEntityReference.generated.h already included, missing '#pragma once' in DMXEntityReference.h"
#endif
#define DMXRUNTIME_DMXEntityReference_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_141_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDMXEntityReference Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXEntityFixturePatchRef>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_123_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXEntityFixtureTypeRef_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDMXEntityReference Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXEntityFixtureTypeRef>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_105_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXEntityControllerRef_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FDMXEntityReference Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXEntityControllerRef>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXEntityReference_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__EntityType() { return STRUCT_OFFSET(FDMXEntityReference, EntityType); } \
	FORCEINLINE static uint32 __PPO__EntityId() { return STRUCT_OFFSET(FDMXEntityReference, EntityId); }


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXEntityReference>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execConv_FixturePatchObjToRef); \
	DECLARE_FUNCTION(execConv_FixtureTypeObjToRef); \
	DECLARE_FUNCTION(execConv_ControllerObjToRef); \
	DECLARE_FUNCTION(execConv_FixturePatchRefToObj); \
	DECLARE_FUNCTION(execConv_FixtureTypeRefToObj); \
	DECLARE_FUNCTION(execConv_ControllerRefToObj);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execConv_FixturePatchObjToRef); \
	DECLARE_FUNCTION(execConv_FixtureTypeObjToRef); \
	DECLARE_FUNCTION(execConv_ControllerObjToRef); \
	DECLARE_FUNCTION(execConv_FixturePatchRefToObj); \
	DECLARE_FUNCTION(execConv_FixtureTypeRefToObj); \
	DECLARE_FUNCTION(execConv_ControllerRefToObj);


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXEntityReferenceConversions(); \
	friend struct Z_Construct_UClass_UDMXEntityReferenceConversions_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityReferenceConversions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityReferenceConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_INCLASS \
private: \
	static void StaticRegisterNativesUDMXEntityReferenceConversions(); \
	friend struct Z_Construct_UClass_UDMXEntityReferenceConversions_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityReferenceConversions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityReferenceConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityReferenceConversions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEntityReferenceConversions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityReferenceConversions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityReferenceConversions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityReferenceConversions(UDMXEntityReferenceConversions&&); \
	NO_API UDMXEntityReferenceConversions(const UDMXEntityReferenceConversions&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityReferenceConversions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityReferenceConversions(UDMXEntityReferenceConversions&&); \
	NO_API UDMXEntityReferenceConversions(const UDMXEntityReferenceConversions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityReferenceConversions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityReferenceConversions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEntityReferenceConversions)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_152_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h_156_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXEntityReferenceConversions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityReference_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
