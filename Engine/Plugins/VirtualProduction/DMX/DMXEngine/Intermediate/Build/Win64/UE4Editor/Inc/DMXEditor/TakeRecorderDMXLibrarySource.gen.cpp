// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXEditor/Private/Sequencer/TakeRecorderDMXLibrarySource.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTakeRecorderDMXLibrarySource() {}
// Cross Module References
	DMXEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FAddAllPatchesButton();
	UPackage* Z_Construct_UPackage__Script_DMXEditor();
	DMXEDITOR_API UClass* Z_Construct_UClass_UTakeRecorderDMXLibrarySource_NoRegister();
	DMXEDITOR_API UClass* Z_Construct_UClass_UTakeRecorderDMXLibrarySource();
	TAKESCORE_API UClass* Z_Construct_UClass_UTakeRecorderSource();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef();
	DMXEDITOR_API UClass* Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_NoRegister();
// End Cross Module References
class UScriptStruct* FAddAllPatchesButton::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FAddAllPatchesButton_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAddAllPatchesButton, Z_Construct_UPackage__Script_DMXEditor(), TEXT("AddAllPatchesButton"), sizeof(FAddAllPatchesButton), Get_Z_Construct_UScriptStruct_FAddAllPatchesButton_Hash());
	}
	return Singleton;
}
template<> DMXEDITOR_API UScriptStruct* StaticStruct<FAddAllPatchesButton>()
{
	return FAddAllPatchesButton::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAddAllPatchesButton(FAddAllPatchesButton::StaticStruct, TEXT("/Script/DMXEditor"), TEXT("AddAllPatchesButton"), false, nullptr, nullptr);
static struct FScriptStruct_DMXEditor_StaticRegisterNativesFAddAllPatchesButton
{
	FScriptStruct_DMXEditor_StaticRegisterNativesFAddAllPatchesButton()
	{
		UScriptStruct::DeferCppStructOps<FAddAllPatchesButton>(FName(TEXT("AddAllPatchesButton")));
	}
} ScriptStruct_DMXEditor_StaticRegisterNativesFAddAllPatchesButton;
	struct Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Empty struct to have it customized in DetailsView to display a button on\n * the DMX Take Recorder properties. This is a required hack to customize the\n * properties in the TakeRecorder DetailsView because it has a customization\n * that overrides any class customization. So we need to tackle individual\n * property types instead.\n */" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "Empty struct to have it customized in DetailsView to display a button on\nthe DMX Take Recorder properties. This is a required hack to customize the\nproperties in the TakeRecorder DetailsView because it has a customization\nthat overrides any class customization. So we need to tackle individual\nproperty types instead." },
	};
#endif
	void* Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAddAllPatchesButton>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
		nullptr,
		&NewStructOps,
		"AddAllPatchesButton",
		sizeof(FAddAllPatchesButton),
		alignof(FAddAllPatchesButton),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAddAllPatchesButton()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAddAllPatchesButton_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AddAllPatchesButton"), sizeof(FAddAllPatchesButton), Get_Z_Construct_UScriptStruct_FAddAllPatchesButton_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAddAllPatchesButton_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAddAllPatchesButton_Hash() { return 753111721U; }
	DEFINE_FUNCTION(UTakeRecorderDMXLibrarySource::execAddAllPatches)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddAllPatches();
		P_NATIVE_END;
	}
	void UTakeRecorderDMXLibrarySource::StaticRegisterNativesUTakeRecorderDMXLibrarySource()
	{
		UClass* Class = UTakeRecorderDMXLibrarySource::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddAllPatches", &UTakeRecorderDMXLibrarySource::execAddAllPatches },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Adds all Patches from the active DMX Library as recording sources */" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "Adds all Patches from the active DMX Library as recording sources" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTakeRecorderDMXLibrarySource, nullptr, "AddAllPatches", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTakeRecorderDMXLibrarySource_NoRegister()
	{
		return UTakeRecorderDMXLibrarySource::StaticClass();
	}
	struct Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXLibrary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddAllPatchesDummy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AddAllPatchesDummy;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixturePatchRefs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixturePatchRefs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FixturePatchRefs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordNormalizedValues_MetaData[];
#endif
		static void NewProp_bRecordNormalizedValues_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordNormalizedValues;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReduceKeys_MetaData[];
#endif
		static void NewProp_bReduceKeys_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReduceKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackRecorder_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackRecorder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTakeRecorderSource,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTakeRecorderDMXLibrarySource_AddAllPatches, "AddAllPatches" }, // 1173370769
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::Class_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** A recording source for DMX data related to a DMX Library */" },
		{ "IncludePath", "Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "TakeRecorderDisplayName", "DMX Library" },
		{ "ToolTip", "A recording source for DMX data related to a DMX Library" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_DMXLibrary_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** DMX Library to record Patches' Fixture Functions from */" },
		{ "DisplayName", "DMX Library" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "DMX Library to record Patches' Fixture Functions from" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_DMXLibrary = { "DMXLibrary", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderDMXLibrarySource, DMXLibrary), Z_Construct_UClass_UDMXLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_DMXLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_DMXLibrary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_AddAllPatchesDummy_MetaData[] = {
		{ "Category", "My Category" },
		{ "Comment", "/**\n\x09 * Dummy property to be replaced with the \"Add all patches\" button.\n\x09 * @see FAddAllPatchesButton\n\x09 */" },
		{ "DisplayName", "" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "Dummy property to be replaced with the \"Add all patches\" button.\n@see FAddAllPatchesButton" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_AddAllPatchesDummy = { "AddAllPatchesDummy", nullptr, (EPropertyFlags)0x0010000000002001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderDMXLibrarySource, AddAllPatchesDummy), Z_Construct_UScriptStruct_FAddAllPatchesButton, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_AddAllPatchesDummy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_AddAllPatchesDummy_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs_Inner = { "FixturePatchRefs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXEntityFixturePatchRef, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** The Fixture Patches to record from the selected Library */" },
		{ "DisplayName", "Fixture Patches" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "The Fixture Patches to record from the selected Library" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs = { "FixturePatchRefs", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderDMXLibrarySource, FixturePatchRefs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues_MetaData[] = {
		{ "Category", "Movie Scene" },
		{ "Comment", "/** \n\x09 * If true, all values are recorded as normalized values (0.0 to 1.0).\n\x09 * \n\x09 * If false, values are recorded as absolute values, depending on the data type of a patch:\n\x09 * 0-255 for 8bit, 0-65'536 for 16bit, 0-16'777'215 for 24bit. 32bit is not fully supported in this mode.\n\x09 */" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "If true, all values are recorded as normalized values (0.0 to 1.0).\n\nIf false, values are recorded as absolute values, depending on the data type of a patch:\n0-255 for 8bit, 0-65'536 for 16bit, 0-16'777'215 for 24bit. 32bit is not fully supported in this mode." },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues_SetBit(void* Obj)
	{
		((UTakeRecorderDMXLibrarySource*)Obj)->bRecordNormalizedValues = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues = { "bRecordNormalizedValues", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderDMXLibrarySource), &Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys_MetaData[] = {
		{ "Category", "Source" },
		{ "Comment", "/** Eliminate repeated keyframe values after recording is done */" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "Eliminate repeated keyframe values after recording is done" },
	};
#endif
	void Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys_SetBit(void* Obj)
	{
		((UTakeRecorderDMXLibrarySource*)Obj)->bReduceKeys = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys = { "bReduceKeys", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTakeRecorderDMXLibrarySource), &Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_TrackRecorder_MetaData[] = {
		{ "Comment", "/** Track recorder used by this source */" },
		{ "ModuleRelativePath", "Private/Sequencer/TakeRecorderDMXLibrarySource.h" },
		{ "ToolTip", "Track recorder used by this source" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_TrackRecorder = { "TrackRecorder", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTakeRecorderDMXLibrarySource, TrackRecorder), Z_Construct_UClass_UMovieSceneDMXLibraryTrackRecorder_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_TrackRecorder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_TrackRecorder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_DMXLibrary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_AddAllPatchesDummy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_FixturePatchRefs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bRecordNormalizedValues,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_bReduceKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::NewProp_TrackRecorder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTakeRecorderDMXLibrarySource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::ClassParams = {
		&UTakeRecorderDMXLibrarySource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTakeRecorderDMXLibrarySource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTakeRecorderDMXLibrarySource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTakeRecorderDMXLibrarySource, 2209010258);
	template<> DMXEDITOR_API UClass* StaticClass<UTakeRecorderDMXLibrarySource>()
	{
		return UTakeRecorderDMXLibrarySource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTakeRecorderDMXLibrarySource(Z_Construct_UClass_UTakeRecorderDMXLibrarySource, &UTakeRecorderDMXLibrarySource::StaticClass, TEXT("/Script/DMXEditor"), TEXT("UTakeRecorderDMXLibrarySource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTakeRecorderDMXLibrarySource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
