// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXEntityController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXEntityController() {}
// Cross Module References
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityUniverseManaged_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityUniverseManaged();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXProtocolName();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityController_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityController();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType();
// End Cross Module References
	void UDMXEntityUniverseManaged::StaticRegisterNativesUDMXEntityUniverseManaged()
	{
	}
	UClass* Z_Construct_UClass_UDMXEntityUniverseManaged_NoRegister()
	{
		return UDMXEntityUniverseManaged::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEntityUniverseManaged_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceProtocol_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeviceProtocol;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXEntity,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// DEPRECATED 4.27, can't be flagged as such to retain upgrade path, some nodes could not be compiled anymore. All members are deprecated.\n" },
		{ "IncludePath", "Library/DMXEntityController.h" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
		{ "ToolTip", "DEPRECATED 4.27, can't be flagged as such to retain upgrade path, some nodes could not be compiled anymore. All members are deprecated." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::NewProp_DeviceProtocol_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::NewProp_DeviceProtocol = { "DeviceProtocol", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityUniverseManaged, DeviceProtocol), Z_Construct_UScriptStruct_FDMXProtocolName, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::NewProp_DeviceProtocol_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::NewProp_DeviceProtocol_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::NewProp_DeviceProtocol,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEntityUniverseManaged>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::ClassParams = {
		&UDMXEntityUniverseManaged::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEntityUniverseManaged()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEntityUniverseManaged_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEntityUniverseManaged, 349619890);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXEntityUniverseManaged>()
	{
		return UDMXEntityUniverseManaged::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEntityUniverseManaged(Z_Construct_UClass_UDMXEntityUniverseManaged, &UDMXEntityUniverseManaged::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXEntityUniverseManaged"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEntityUniverseManaged);
	void UDMXEntityController::StaticRegisterNativesUDMXEntityController()
	{
	}
	UClass* Z_Construct_UClass_UDMXEntityController_NoRegister()
	{
		return UDMXEntityController::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEntityController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CommunicationMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommunicationMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CommunicationMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniverseLocalStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniverseLocalStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniverseLocalNum_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniverseLocalNum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniverseLocalEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniverseLocalEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RemoteOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniverseRemoteStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniverseRemoteStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniverseRemoteEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniverseRemoteEnd;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AdditionalUnicastIPs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalUnicastIPs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionalUnicastIPs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEntityController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXEntityUniverseManaged,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// DEPRECATED 4.27, can't be flagged as such to retain upgrade path, some nodes could not be compiled anymore. All members are deprecated.\n" },
		{ "IncludePath", "Library/DMXEntityController.h" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
		{ "ToolTip", "DEPRECATED 4.27, can't be flagged as such to retain upgrade path, some nodes could not be compiled anymore. All members are deprecated." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode = { "CommunicationMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, CommunicationMode), Z_Construct_UEnum_DMXProtocol_EDMXCommunicationType, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalStart_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalStart = { "UniverseLocalStart", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, UniverseLocalStart), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalNum_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalNum = { "UniverseLocalNum", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, UniverseLocalNum), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalNum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalNum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalEnd_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalEnd = { "UniverseLocalEnd", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, UniverseLocalEnd), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_RemoteOffset_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_RemoteOffset = { "RemoteOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, RemoteOffset), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_RemoteOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_RemoteOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteStart_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteStart = { "UniverseRemoteStart", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, UniverseRemoteStart), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteEnd_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteEnd = { "UniverseRemoteEnd", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, UniverseRemoteEnd), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteEnd_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs_Inner = { "AdditionalUnicastIPs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Controllers are no longer in use. Use Ports instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityController.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs = { "AdditionalUnicastIPs", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityController, AdditionalUnicastIPs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXEntityController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_CommunicationMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalNum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseLocalEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_RemoteOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_UniverseRemoteEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityController_Statics::NewProp_AdditionalUnicastIPs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEntityController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEntityController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEntityController_Statics::ClassParams = {
		&UDMXEntityController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXEntityController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEntityController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEntityController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEntityController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEntityController, 4011441272);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXEntityController>()
	{
		return UDMXEntityController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEntityController(Z_Construct_UClass_UDMXEntityController, &UDMXEntityController::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXEntityController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEntityController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
