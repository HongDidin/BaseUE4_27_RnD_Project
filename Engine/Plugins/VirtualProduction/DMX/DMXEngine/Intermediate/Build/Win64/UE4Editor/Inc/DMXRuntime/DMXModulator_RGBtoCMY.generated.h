// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXRUNTIME_DMXModulator_RGBtoCMY_generated_h
#error "DMXModulator_RGBtoCMY.generated.h already included, missing '#pragma once' in DMXModulator_RGBtoCMY.h"
#endif
#define DMXRUNTIME_DMXModulator_RGBtoCMY_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXModulator_RGBtoCMY(); \
	friend struct Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator_RGBtoCMY, UDMXModulator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator_RGBtoCMY)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDMXModulator_RGBtoCMY(); \
	friend struct Z_Construct_UClass_UDMXModulator_RGBtoCMY_Statics; \
public: \
	DECLARE_CLASS(UDMXModulator_RGBtoCMY, UDMXModulator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXModulator_RGBtoCMY)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXModulator_RGBtoCMY(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXModulator_RGBtoCMY) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator_RGBtoCMY); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator_RGBtoCMY); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator_RGBtoCMY(UDMXModulator_RGBtoCMY&&); \
	NO_API UDMXModulator_RGBtoCMY(const UDMXModulator_RGBtoCMY&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXModulator_RGBtoCMY(UDMXModulator_RGBtoCMY&&); \
	NO_API UDMXModulator_RGBtoCMY(const UDMXModulator_RGBtoCMY&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXModulator_RGBtoCMY); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXModulator_RGBtoCMY); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXModulator_RGBtoCMY)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_11_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXModulator_RGBtoCMY>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Private_Modulators_DMXModulator_RGBtoCMY_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
