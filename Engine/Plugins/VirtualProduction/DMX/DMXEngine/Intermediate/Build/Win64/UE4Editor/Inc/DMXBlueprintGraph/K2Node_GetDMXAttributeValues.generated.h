// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXBLUEPRINTGRAPH_K2Node_GetDMXAttributeValues_generated_h
#error "K2Node_GetDMXAttributeValues.generated.h already included, missing '#pragma once' in K2Node_GetDMXAttributeValues.h"
#endif
#define DMXBLUEPRINTGRAPH_K2Node_GetDMXAttributeValues_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_GetDMXAttributeValues(); \
	friend struct Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics; \
public: \
	DECLARE_CLASS(UK2Node_GetDMXAttributeValues, UK2Node_EditablePinBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXBlueprintGraph"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_GetDMXAttributeValues)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_GetDMXAttributeValues(); \
	friend struct Z_Construct_UClass_UK2Node_GetDMXAttributeValues_Statics; \
public: \
	DECLARE_CLASS(UK2Node_GetDMXAttributeValues, UK2Node_EditablePinBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXBlueprintGraph"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_GetDMXAttributeValues)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_GetDMXAttributeValues(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_GetDMXAttributeValues) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_GetDMXAttributeValues); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_GetDMXAttributeValues); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_GetDMXAttributeValues(UK2Node_GetDMXAttributeValues&&); \
	NO_API UK2Node_GetDMXAttributeValues(const UK2Node_GetDMXAttributeValues&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_GetDMXAttributeValues(UK2Node_GetDMXAttributeValues&&); \
	NO_API UK2Node_GetDMXAttributeValues(const UK2Node_GetDMXAttributeValues&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_GetDMXAttributeValues); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_GetDMXAttributeValues); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UK2Node_GetDMXAttributeValues)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_21_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXBLUEPRINTGRAPH_API UClass* StaticClass<class UK2Node_GetDMXAttributeValues>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXBlueprintGraph_Public_K2Node_GetDMXAttributeValues_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
