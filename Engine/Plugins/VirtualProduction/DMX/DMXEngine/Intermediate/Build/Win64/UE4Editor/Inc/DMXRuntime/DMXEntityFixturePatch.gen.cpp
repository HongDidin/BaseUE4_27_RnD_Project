// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXEntityFixturePatch.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXEntityFixturePatch() {}
// Cross Module References
	DMXRUNTIME_API UFunction* Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixturePatch();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXCell();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureFunction();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureMatrix();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityController_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixtureType_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics
	{
		struct DMXEntityFixturePatch_eventDMXOnFixturePatchReceivedDMXDelegate_Parms
		{
			UDMXEntityFixturePatch* FixturePatch;
			FDMXNormalizedAttributeValueMap ValuePerAttribute;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FixturePatch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValuePerAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ValuePerAttribute;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_FixturePatch = { "FixturePatch", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventDMXOnFixturePatchReceivedDMXDelegate_Parms, FixturePatch), Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_ValuePerAttribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_ValuePerAttribute = { "ValuePerAttribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventDMXOnFixturePatchReceivedDMXDelegate_Parms, ValuePerAttribute), Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_ValuePerAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_ValuePerAttribute_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_FixturePatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::NewProp_ValuePerAttribute,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventDMXOnFixturePatchReceivedDMXDelegate_Parms), Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00530000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAllMatrixCells)
	{
		P_GET_TARRAY_REF(FDMXCell,Z_Param_Out_Cells);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetAllMatrixCells(Z_Param_Out_Cells);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetMatrixCell)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_STRUCT_REF(FDMXCell,Z_Param_Out_Cell);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetMatrixCell(Z_Param_Out_CellCoordinate,Z_Param_Out_Cell);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetCellAttributes)
	{
		P_GET_TARRAY_REF(FDMXAttributeName,Z_Param_Out_CellAttributes);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetCellAttributes(Z_Param_Out_CellAttributes);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetMatrixProperties)
	{
		P_GET_STRUCT_REF(FDMXFixtureMatrix,Z_Param_Out_MatrixProperties);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetMatrixProperties(Z_Param_Out_MatrixProperties);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetMatrixCellChannelsAbsoluteWithValidation)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_InCellCoordinate);
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_OutAttributeChannelMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetMatrixCellChannelsAbsoluteWithValidation(Z_Param_Out_InCellCoordinate,Z_Param_Out_OutAttributeChannelMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetMatrixCellChannelsAbsolute)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_AttributeChannelMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetMatrixCellChannelsAbsolute(Z_Param_Out_CellCoordinate,Z_Param_Out_AttributeChannelMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetMatrixCellChannelsRelative)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_AttributeChannelMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetMatrixCellChannelsRelative(Z_Param_Out_CellCoordinate,Z_Param_Out_AttributeChannelMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetNormalizedMatrixCellValues)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_TMAP_REF(FDMXAttributeName,float,Z_Param_Out_NormalizedValuePerAttribute);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetNormalizedMatrixCellValues(Z_Param_Out_CellCoordinate,Z_Param_Out_NormalizedValuePerAttribute);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetMatrixCellValues)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_ValuePerAttribute);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetMatrixCellValues(Z_Param_Out_CellCoordinate,Z_Param_Out_ValuePerAttribute);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execSendNormalizedMatrixCellValue)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_STRUCT_REF(FDMXAttributeName,Z_Param_Out_Attribute);
		P_GET_PROPERTY(FFloatProperty,Z_Param_RelativeValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SendNormalizedMatrixCellValue(Z_Param_Out_CellCoordinate,Z_Param_Out_Attribute,Z_Param_RelativeValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execSendMatrixCellValueWithAttributeMap)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_STRUCT_REF(FDMXAttributeName,Z_Param_Out_Attribute);
		P_GET_PROPERTY(FIntProperty,Z_Param_Value);
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_InAttributeNameChannelMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SendMatrixCellValueWithAttributeMap(Z_Param_Out_CellCoordinate,Z_Param_Out_Attribute,Z_Param_Value,Z_Param_Out_InAttributeNameChannelMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execSendMatrixCellValue)
	{
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_CellCoordinate);
		P_GET_STRUCT_REF(FDMXAttributeName,Z_Param_Out_Attribute);
		P_GET_PROPERTY(FIntProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SendMatrixCellValue(Z_Param_Out_CellCoordinate,Z_Param_Out_Attribute,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetNormalizedAttributesValues)
	{
		P_GET_STRUCT_REF(FDMXNormalizedAttributeValueMap,Z_Param_Out_NormalizedAttributesValues);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetNormalizedAttributesValues(Z_Param_Out_NormalizedAttributesValues);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAttributesValues)
	{
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_AttributesValues);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAttributesValues(Z_Param_Out_AttributesValues);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetNormalizedAttributeValue)
	{
		P_GET_STRUCT(FDMXAttributeName,Z_Param_Attribute);
		P_GET_UBOOL_REF(Z_Param_Out_bSuccess);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetNormalizedAttributeValue(Z_Param_Attribute,Z_Param_Out_bSuccess);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAttributeValue)
	{
		P_GET_STRUCT(FDMXAttributeName,Z_Param_Attribute);
		P_GET_UBOOL_REF(Z_Param_Out_bSuccess);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetAttributeValue(Z_Param_Attribute,Z_Param_Out_bSuccess);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execIsInControllersRange)
	{
		P_GET_TARRAY_REF(UDMXEntityController*,Z_Param_Out_InControllers);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsInControllersRange(Z_Param_Out_InControllers);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execIsInControllerRange)
	{
		P_GET_OBJECT(UDMXEntityController,Z_Param_InController);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsInControllerRange(Z_Param_InController);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetRelevantControllers)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UDMXEntityController*>*)Z_Param__Result=P_THIS->GetRelevantControllers();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execConvertToValidMap)
	{
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_FunctionMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FDMXAttributeName,int32>*)Z_Param__Result=P_THIS->ConvertToValidMap(Z_Param_Out_FunctionMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execContainsAttribute)
	{
		P_GET_STRUCT(FDMXAttributeName,Z_Param_FunctionAttribute);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ContainsAttribute(Z_Param_FunctionAttribute);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execIsMapValid)
	{
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_FunctionMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsMapValid(Z_Param_Out_FunctionMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execConvertAttributeMapToRawMap)
	{
		P_GET_TMAP_REF(FDMXAttributeName,int32,Z_Param_Out_FunctionMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<int32,uint8>*)Z_Param__Result=P_THIS->ConvertAttributeMapToRawMap(Z_Param_Out_FunctionMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execConvertRawMapToAttributeMap)
	{
		P_GET_TMAP_REF(int32,uint8,Z_Param_Out_RawMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FDMXAttributeName,int32>*)Z_Param__Result=P_THIS->ConvertRawMapToAttributeMap(Z_Param_Out_RawMap);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAttributeSignalFormats)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FDMXAttributeName,EDMXFixtureSignalFormat>*)Z_Param__Result=P_THIS->GetAttributeSignalFormats();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAttributeChannelAssignments)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FDMXAttributeName,int32>*)Z_Param__Result=P_THIS->GetAttributeChannelAssignments();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAttributeDefaultMap)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FDMXAttributeName,int32>*)Z_Param__Result=P_THIS->GetAttributeDefaultMap();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAttributeFunctionsMap)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FDMXAttributeName,FDMXFixtureFunction>*)Z_Param__Result=P_THIS->GetAttributeFunctionsMap();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetAllAttributesInActiveMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FDMXAttributeName>*)Z_Param__Result=P_THIS->GetAllAttributesInActiveMode();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetRemoteUniverse)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetRemoteUniverse();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetEndingChannel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetEndingChannel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetChannelSpan)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetChannelSpan();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execGetStartingChannel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetStartingChannel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDMXEntityFixturePatch::execSendDMX)
	{
		P_GET_TMAP(FDMXAttributeName,int32,Z_Param_AttributeMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendDMX(Z_Param_AttributeMap);
		P_NATIVE_END;
	}
	void UDMXEntityFixturePatch::StaticRegisterNativesUDMXEntityFixturePatch()
	{
		UClass* Class = UDMXEntityFixturePatch::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ContainsAttribute", &UDMXEntityFixturePatch::execContainsAttribute },
			{ "ConvertAttributeMapToRawMap", &UDMXEntityFixturePatch::execConvertAttributeMapToRawMap },
			{ "ConvertRawMapToAttributeMap", &UDMXEntityFixturePatch::execConvertRawMapToAttributeMap },
			{ "ConvertToValidMap", &UDMXEntityFixturePatch::execConvertToValidMap },
			{ "GetAllAttributesInActiveMode", &UDMXEntityFixturePatch::execGetAllAttributesInActiveMode },
			{ "GetAllMatrixCells", &UDMXEntityFixturePatch::execGetAllMatrixCells },
			{ "GetAttributeChannelAssignments", &UDMXEntityFixturePatch::execGetAttributeChannelAssignments },
			{ "GetAttributeDefaultMap", &UDMXEntityFixturePatch::execGetAttributeDefaultMap },
			{ "GetAttributeFunctionsMap", &UDMXEntityFixturePatch::execGetAttributeFunctionsMap },
			{ "GetAttributeSignalFormats", &UDMXEntityFixturePatch::execGetAttributeSignalFormats },
			{ "GetAttributesValues", &UDMXEntityFixturePatch::execGetAttributesValues },
			{ "GetAttributeValue", &UDMXEntityFixturePatch::execGetAttributeValue },
			{ "GetCellAttributes", &UDMXEntityFixturePatch::execGetCellAttributes },
			{ "GetChannelSpan", &UDMXEntityFixturePatch::execGetChannelSpan },
			{ "GetEndingChannel", &UDMXEntityFixturePatch::execGetEndingChannel },
			{ "GetMatrixCell", &UDMXEntityFixturePatch::execGetMatrixCell },
			{ "GetMatrixCellChannelsAbsolute", &UDMXEntityFixturePatch::execGetMatrixCellChannelsAbsolute },
			{ "GetMatrixCellChannelsAbsoluteWithValidation", &UDMXEntityFixturePatch::execGetMatrixCellChannelsAbsoluteWithValidation },
			{ "GetMatrixCellChannelsRelative", &UDMXEntityFixturePatch::execGetMatrixCellChannelsRelative },
			{ "GetMatrixCellValues", &UDMXEntityFixturePatch::execGetMatrixCellValues },
			{ "GetMatrixProperties", &UDMXEntityFixturePatch::execGetMatrixProperties },
			{ "GetNormalizedAttributesValues", &UDMXEntityFixturePatch::execGetNormalizedAttributesValues },
			{ "GetNormalizedAttributeValue", &UDMXEntityFixturePatch::execGetNormalizedAttributeValue },
			{ "GetNormalizedMatrixCellValues", &UDMXEntityFixturePatch::execGetNormalizedMatrixCellValues },
			{ "GetRelevantControllers", &UDMXEntityFixturePatch::execGetRelevantControllers },
			{ "GetRemoteUniverse", &UDMXEntityFixturePatch::execGetRemoteUniverse },
			{ "GetStartingChannel", &UDMXEntityFixturePatch::execGetStartingChannel },
			{ "IsInControllerRange", &UDMXEntityFixturePatch::execIsInControllerRange },
			{ "IsInControllersRange", &UDMXEntityFixturePatch::execIsInControllersRange },
			{ "IsMapValid", &UDMXEntityFixturePatch::execIsMapValid },
			{ "SendDMX", &UDMXEntityFixturePatch::execSendDMX },
			{ "SendMatrixCellValue", &UDMXEntityFixturePatch::execSendMatrixCellValue },
			{ "SendMatrixCellValueWithAttributeMap", &UDMXEntityFixturePatch::execSendMatrixCellValueWithAttributeMap },
			{ "SendNormalizedMatrixCellValue", &UDMXEntityFixturePatch::execSendNormalizedMatrixCellValue },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics
	{
		struct DMXEntityFixturePatch_eventContainsAttribute_Parms
		{
			FDMXAttributeName FunctionAttribute;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionAttribute;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::NewProp_FunctionAttribute = { "FunctionAttribute", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventContainsAttribute_Parms, FunctionAttribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventContainsAttribute_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventContainsAttribute_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::NewProp_FunctionAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**  Returns true if the fixture patch contains the attribute. */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns true if the fixture patch contains the attribute." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "ContainsAttribute", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventContainsAttribute_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics
	{
		struct DMXEntityFixturePatch_eventConvertAttributeMapToRawMap_Parms
		{
			TMap<FDMXAttributeName,int32> FunctionMap;
			TMap<int32,uint8> ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FunctionMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FunctionMap;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_ValueProp = { "FunctionMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_Key_KeyProp = { "FunctionMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap = { "FunctionMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventConvertAttributeMapToRawMap_Parms, FunctionMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventConvertAttributeMapToRawMap_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_FunctionMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns a map of function channels and their values.\n\x09 * Functions outside the Active Mode's channel span range are ignored.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns a map of function channels and their values.\nFunctions outside the Active Mode's channel span range are ignored." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "ConvertAttributeMapToRawMap", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventConvertAttributeMapToRawMap_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics
	{
		struct DMXEntityFixturePatch_eventConvertRawMapToAttributeMap_Parms
		{
			TMap<int32,uint8> RawMap;
			TMap<FDMXAttributeName,int32> ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RawMap_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RawMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RawMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RawMap;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_ValueProp = { "RawMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_Key_KeyProp = { "RawMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap = { "RawMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventConvertRawMapToAttributeMap_Parms, RawMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventConvertRawMapToAttributeMap_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_RawMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated 4.27. Please use DMXSubsystem's Bytes to Int instead, then create a map from that with Attribute Names ." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "ConvertRawMapToAttributeMap", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventConvertRawMapToAttributeMap_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics
	{
		struct DMXEntityFixturePatch_eventConvertToValidMap_Parms
		{
			TMap<FDMXAttributeName,int32> FunctionMap;
			TMap<FDMXAttributeName,int32> ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FunctionMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FunctionMap;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_ValueProp = { "FunctionMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_Key_KeyProp = { "FunctionMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap = { "FunctionMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventConvertToValidMap_Parms, FunctionMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventConvertToValidMap_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_FunctionMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**  Returns a map that only contains attributes used this patch. */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns a map that only contains attributes used this patch." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "ConvertToValidMap", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventConvertToValidMap_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics
	{
		struct DMXEntityFixturePatch_eventGetAllAttributesInActiveMode_Parms
		{
			TArray<FDMXAttributeName> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAllAttributesInActiveMode_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns an array of valid attributes for the currently active mode.\n\x09 * Attributes outside the Active Mode's channel span range are ignored.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns an array of valid attributes for the currently active mode.\nAttributes outside the Active Mode's channel span range are ignored." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAllAttributesInActiveMode", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAllAttributesInActiveMode_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics
	{
		struct DMXEntityFixturePatch_eventGetAllMatrixCells_Parms
		{
			TArray<FDMXCell> Cells;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cells_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Cells;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_Cells_Inner = { "Cells", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXCell, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_Cells = { "Cells", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAllMatrixCells_Parms, Cells), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetAllMatrixCells_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetAllMatrixCells_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_Cells_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_Cells,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**  Gets all matrix cells */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Gets all matrix cells" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAllMatrixCells", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAllMatrixCells_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics
	{
		struct DMXEntityFixturePatch_eventGetAttributeChannelAssignments_Parms
		{
			TMap<FDMXAttributeName,int32> ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributeChannelAssignments_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/** Returns a map of Attributes and their assigned channels */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns a map of Attributes and their assigned channels" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAttributeChannelAssignments", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAttributeChannelAssignments_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics
	{
		struct DMXEntityFixturePatch_eventGetAttributeDefaultMap_Parms
		{
			TMap<FDMXAttributeName,int32> ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributeDefaultMap_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns a map of function names and default values.\n\x09 * Functions outside the Active Mode's channel span range are ignored.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns a map of function names and default values.\nFunctions outside the Active Mode's channel span range are ignored." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAttributeDefaultMap", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAttributeDefaultMap_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics
	{
		struct DMXEntityFixturePatch_eventGetAttributeFunctionsMap_Parms
		{
			TMap<FDMXAttributeName,FDMXFixtureFunction> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDMXFixtureFunction, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributeFunctionsMap_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns a map of attributes and function names.\n\x09 * Attributes outside the Active Mode's channel span range are ignored.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns a map of attributes and function names.\nAttributes outside the Active Mode's channel span range are ignored." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAttributeFunctionsMap", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAttributeFunctionsMap_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics
	{
		struct DMXEntityFixturePatch_eventGetAttributeSignalFormats_Parms
		{
			TMap<FDMXAttributeName,EDMXFixtureSignalFormat> ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_ValueProp_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue_ValueProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributeSignalFormats_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue_ValueProp_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns a map of function names and their Data Types.\n\x09 * Functions outside the Active Mode's channel span range are ignored.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns a map of function names and their Data Types.\nFunctions outside the Active Mode's channel span range are ignored." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAttributeSignalFormats", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAttributeSignalFormats_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics
	{
		struct DMXEntityFixturePatch_eventGetAttributesValues_Parms
		{
			TMap<FDMXAttributeName,int32> AttributesValues;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AttributesValues_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributesValues_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AttributesValues;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::NewProp_AttributesValues_ValueProp = { "AttributesValues", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::NewProp_AttributesValues_Key_KeyProp = { "AttributesValues_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::NewProp_AttributesValues = { "AttributesValues", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributesValues_Parms, AttributesValues), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::NewProp_AttributesValues_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::NewProp_AttributesValues_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::NewProp_AttributesValues,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns the value of each attribute, or zero if no value was ever received.\n\x09 *\n\x09 * @param AttributesValues\x09Out: Resulting map of Attributes with their values\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns the value of each attribute, or zero if no value was ever received.\n\n@param AttributesValues      Out: Resulting map of Attributes with their values" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAttributesValues", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAttributesValues_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics
	{
		struct DMXEntityFixturePatch_eventGetAttributeValue_Parms
		{
			FDMXAttributeName Attribute;
			bool bSuccess;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributeValue_Parms, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetAttributeValue_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetAttributeValue_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetAttributeValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Retrieves the value of an Attribute. Will fail and return 0 if the Attribute doesn't exist.\n\x09 *\n\x09 * @param Attribute\x09The Attribute to try to get the value from.\n\x09 * @param bSuccess\x09Whether the Attribute was found in this Fixture Patch\n\x09 * @return\x09\x09\x09The value of the Function mapped to the selected Attribute, if found.\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Retrieves the value of an Attribute. Will fail and return 0 if the Attribute doesn't exist.\n\n@param Attribute     The Attribute to try to get the value from.\n@param bSuccess      Whether the Attribute was found in this Fixture Patch\n@return                      The value of the Function mapped to the selected Attribute, if found." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetAttributeValue", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetAttributeValue_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics
	{
		struct DMXEntityFixturePatch_eventGetCellAttributes_Parms
		{
			TArray<FDMXAttributeName> CellAttributes;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellAttributes_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CellAttributes;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_CellAttributes_Inner = { "CellAttributes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_CellAttributes = { "CellAttributes", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetCellAttributes_Parms, CellAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetCellAttributes_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetCellAttributes_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_CellAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_CellAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**  Gets all attributes names of a cell */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Gets all attributes names of a cell" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetCellAttributes", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetCellAttributes_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics
	{
		struct DMXEntityFixturePatch_eventGetChannelSpan_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetChannelSpan_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/** Returns the number of channels this Patch occupies with the Fixture functions from its Active Mode or 0 if the patch has no valid Active Mode */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns the number of channels this Patch occupies with the Fixture functions from its Active Mode or 0 if the patch has no valid Active Mode" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetChannelSpan", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetChannelSpan_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics
	{
		struct DMXEntityFixturePatch_eventGetEndingChannel_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetEndingChannel_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/** Returns the last channel of the patch */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns the last channel of the patch" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetEndingChannel", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetEndingChannel_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics
	{
		struct DMXEntityFixturePatch_eventGetMatrixCell_Parms
		{
			FIntPoint CellCoordinate;
			FDMXCell Cell;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cell;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCell_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_CellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_Cell = { "Cell", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCell_Parms, Cell), Z_Construct_UScriptStruct_FDMXCell, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetMatrixCell_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetMatrixCell_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_Cell,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetMatrixCell", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetMatrixCell_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics
	{
		struct DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsolute_Parms
		{
			FIntPoint CellCoordinate;
			TMap<FDMXAttributeName,int32> AttributeChannelMap;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AttributeChannelMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeChannelMap_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AttributeChannelMap;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsolute_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_CellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_AttributeChannelMap_ValueProp = { "AttributeChannelMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_AttributeChannelMap_Key_KeyProp = { "AttributeChannelMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_AttributeChannelMap = { "AttributeChannelMap", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsolute_Parms, AttributeChannelMap), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsolute_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsolute_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_AttributeChannelMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_AttributeChannelMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_AttributeChannelMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetMatrixCellChannelsAbsolute", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsolute_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics
	{
		struct DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsoluteWithValidation_Parms
		{
			FIntPoint InCellCoordinate;
			TMap<FDMXAttributeName,int32> OutAttributeChannelMap;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InCellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InCellCoordinate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OutAttributeChannelMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutAttributeChannelMap_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_OutAttributeChannelMap;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_InCellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_InCellCoordinate = { "InCellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsoluteWithValidation_Parms, InCellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_InCellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_InCellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_OutAttributeChannelMap_ValueProp = { "OutAttributeChannelMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_OutAttributeChannelMap_Key_KeyProp = { "OutAttributeChannelMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_OutAttributeChannelMap = { "OutAttributeChannelMap", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsoluteWithValidation_Parms, OutAttributeChannelMap), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsoluteWithValidation_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsoluteWithValidation_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_InCellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_OutAttributeChannelMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_OutAttributeChannelMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_OutAttributeChannelMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetMatrixCellChannelsAbsoluteWithValidation", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetMatrixCellChannelsAbsoluteWithValidation_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics
	{
		struct DMXEntityFixturePatch_eventGetMatrixCellChannelsRelative_Parms
		{
			FIntPoint CellCoordinate;
			TMap<FDMXAttributeName,int32> AttributeChannelMap;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AttributeChannelMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeChannelMap_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AttributeChannelMap;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellChannelsRelative_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_CellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_AttributeChannelMap_ValueProp = { "AttributeChannelMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_AttributeChannelMap_Key_KeyProp = { "AttributeChannelMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_AttributeChannelMap = { "AttributeChannelMap", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellChannelsRelative_Parms, AttributeChannelMap), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetMatrixCellChannelsRelative_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetMatrixCellChannelsRelative_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_AttributeChannelMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_AttributeChannelMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_AttributeChannelMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetMatrixCellChannelsRelative", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetMatrixCellChannelsRelative_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics
	{
		struct DMXEntityFixturePatch_eventGetMatrixCellValues_Parms
		{
			FIntPoint CellCoordinate;
			TMap<FDMXAttributeName,int32> ValuePerAttribute;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ValuePerAttribute_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ValuePerAttribute_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ValuePerAttribute;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellValues_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_CellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ValuePerAttribute_ValueProp = { "ValuePerAttribute", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ValuePerAttribute_Key_KeyProp = { "ValuePerAttribute_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ValuePerAttribute = { "ValuePerAttribute", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixCellValues_Parms, ValuePerAttribute), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetMatrixCellValues_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetMatrixCellValues_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ValuePerAttribute_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ValuePerAttribute_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ValuePerAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetMatrixCellValues", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetMatrixCellValues_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics
	{
		struct DMXEntityFixturePatch_eventGetMatrixProperties_Parms
		{
			FDMXFixtureMatrix MatrixProperties;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MatrixProperties;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::NewProp_MatrixProperties = { "MatrixProperties", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetMatrixProperties_Parms, MatrixProperties), Z_Construct_UScriptStruct_FDMXFixtureMatrix, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetMatrixProperties_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetMatrixProperties_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::NewProp_MatrixProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**  Gets the Matrix Fixture properties, returns false if the patch is not using a matrix fixture */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Gets the Matrix Fixture properties, returns false if the patch is not using a matrix fixture" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetMatrixProperties", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetMatrixProperties_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics
	{
		struct DMXEntityFixturePatch_eventGetNormalizedAttributesValues_Parms
		{
			FDMXNormalizedAttributeValueMap NormalizedAttributesValues;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NormalizedAttributesValues;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::NewProp_NormalizedAttributesValues = { "NormalizedAttributesValues", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetNormalizedAttributesValues_Parms, NormalizedAttributesValues), Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::NewProp_NormalizedAttributesValues,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Returns the normalized value of each attribute, or zero if no value was ever received.\n\x09 *\n\x09 * @param AttributesValues\x09Out: Resulting map of Attributes with their normalized values\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns the normalized value of each attribute, or zero if no value was ever received.\n\n@param AttributesValues      Out: Resulting map of Attributes with their normalized values" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetNormalizedAttributesValues", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetNormalizedAttributesValues_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics
	{
		struct DMXEntityFixturePatch_eventGetNormalizedAttributeValue_Parms
		{
			FDMXAttributeName Attribute;
			bool bSuccess;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetNormalizedAttributeValue_Parms, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetNormalizedAttributeValue_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetNormalizedAttributeValue_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetNormalizedAttributeValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**\n\x09 * Retrieves the normalized value of an Attribute. Will fail and return 0 if the Attribute doesn't exist.\n\x09 *\n\x09 * @param Attribute\x09The Attribute to try to get the value from.\n\x09 * @param bSuccess\x09Whether the Attribute was found in this Fixture Patch\n\x09 * @return\x09\x09\x09The value of the Function mapped to the selected Attribute, if found.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated 4.26. Use GetNormalizedAttributeValue instead. Note, new method returns normalized values!" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Retrieves the normalized value of an Attribute. Will fail and return 0 if the Attribute doesn't exist.\n\n@param Attribute     The Attribute to try to get the value from.\n@param bSuccess      Whether the Attribute was found in this Fixture Patch\n@return                      The value of the Function mapped to the selected Attribute, if found." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetNormalizedAttributeValue", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetNormalizedAttributeValue_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics
	{
		struct DMXEntityFixturePatch_eventGetNormalizedMatrixCellValues_Parms
		{
			FIntPoint CellCoordinate;
			TMap<FDMXAttributeName,float> NormalizedValuePerAttribute;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NormalizedValuePerAttribute_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NormalizedValuePerAttribute_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_NormalizedValuePerAttribute;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetNormalizedMatrixCellValues_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_CellCoordinate_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_NormalizedValuePerAttribute_ValueProp = { "NormalizedValuePerAttribute", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_NormalizedValuePerAttribute_Key_KeyProp = { "NormalizedValuePerAttribute_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_NormalizedValuePerAttribute = { "NormalizedValuePerAttribute", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetNormalizedMatrixCellValues_Parms, NormalizedValuePerAttribute), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventGetNormalizedMatrixCellValues_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventGetNormalizedMatrixCellValues_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_NormalizedValuePerAttribute_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_NormalizedValuePerAttribute_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_NormalizedValuePerAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetNormalizedMatrixCellValues", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetNormalizedMatrixCellValues_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics
	{
		struct DMXEntityFixturePatch_eventGetRelevantControllers_Parms
		{
			TArray<UDMXEntityController*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXEntityController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetRelevantControllers_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated 4.27. Controllers are replaced with DMX Ports." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetRelevantControllers", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetRelevantControllers_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics
	{
		struct DMXEntityFixturePatch_eventGetRemoteUniverse_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetRemoteUniverse_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated 4.27. No clear remote universe can be deduced from controllers (before 4.27) or ports (from 4.27 on)." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetRemoteUniverse", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetRemoteUniverse_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics
	{
		struct DMXEntityFixturePatch_eventGetStartingChannel_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventGetStartingChannel_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/** Return the starting channel */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Return the starting channel" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "GetStartingChannel", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventGetStartingChannel_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics
	{
		struct DMXEntityFixturePatch_eventIsInControllerRange_Parms
		{
			const UDMXEntityController* InController;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InController;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_InController_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_InController = { "InController", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventIsInControllerRange_Parms, InController), Z_Construct_UClass_UDMXEntityController_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_InController_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_InController_MetaData)) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventIsInControllerRange_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventIsInControllerRange_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_InController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated 4.27. Controllers are replaced with DMX Ports." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "IsInControllerRange", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventIsInControllerRange_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics
	{
		struct DMXEntityFixturePatch_eventIsInControllersRange_Parms
		{
			TArray<UDMXEntityController*> InControllers;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InControllers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InControllers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InControllers;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers_Inner = { "InControllers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXEntityController_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers = { "InControllers", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventIsInControllersRange_Parms, InControllers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers_MetaData)) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventIsInControllersRange_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventIsInControllersRange_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_InControllers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated 4.27. Controllers are replaced with DMX Ports." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "IsInControllersRange", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventIsInControllersRange_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics
	{
		struct DMXEntityFixturePatch_eventIsMapValid_Parms
		{
			TMap<FDMXAttributeName,int32> FunctionMap;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FunctionMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FunctionMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FunctionMap;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_ValueProp = { "FunctionMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_Key_KeyProp = { "FunctionMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap = { "FunctionMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventIsMapValid_Parms, FunctionMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_MetaData)) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventIsMapValid_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventIsMapValid_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_FunctionMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/**  Returns true if given function map is valid for this fixture. */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Returns true if given function map is valid for this fixture." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "IsMapValid", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventIsMapValid_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics
	{
		struct DMXEntityFixturePatch_eventSendDMX_Parms
		{
			TMap<FDMXAttributeName,int32> AttributeMap;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AttributeMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeMap_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AttributeMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::NewProp_AttributeMap_ValueProp = { "AttributeMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::NewProp_AttributeMap_Key_KeyProp = { "AttributeMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::NewProp_AttributeMap = { "AttributeMap", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendDMX_Parms, AttributeMap), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::NewProp_AttributeMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::NewProp_AttributeMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::NewProp_AttributeMap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**  Send DMX using attribute names and integer values. */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Send DMX using attribute names and integer values." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "SendDMX", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventSendDMX_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics
	{
		struct DMXEntityFixturePatch_eventSendMatrixCellValue_Parms
		{
			FIntPoint CellCoordinate;
			FDMXAttributeName Attribute;
			int32 Value;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValue_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_CellCoordinate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Attribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValue_Parms, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Attribute_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValue_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventSendMatrixCellValue_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventSendMatrixCellValue_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "SendMatrixCellValue", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventSendMatrixCellValue_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics
	{
		struct DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms
		{
			FIntPoint CellCoordinate;
			FDMXAttributeName Attribute;
			int32 Value;
			TMap<FDMXAttributeName,int32> InAttributeNameChannelMap;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InAttributeNameChannelMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InAttributeNameChannelMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InAttributeNameChannelMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_InAttributeNameChannelMap;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_CellCoordinate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Attribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Attribute_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_ValueProp = { "InAttributeNameChannelMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_Key_KeyProp = { "InAttributeNameChannelMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap = { "InAttributeNameChannelMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms, InAttributeNameChannelMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_MetaData)) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_InAttributeNameChannelMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Deprecated due to ambigous arguments CellCoordinate and InAttributeNameChannelMap. Use SendMatrixCellValue or SendNormalizedMatrixCellValue instead." },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "SendMatrixCellValueWithAttributeMap", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventSendMatrixCellValueWithAttributeMap_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics
	{
		struct DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms
		{
			FIntPoint CellCoordinate;
			FDMXAttributeName Attribute;
			float RelativeValue;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellCoordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellCoordinate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RelativeValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_CellCoordinate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_CellCoordinate = { "CellCoordinate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms, CellCoordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_CellCoordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_CellCoordinate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_Attribute_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_Attribute_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_RelativeValue = { "RelativeValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms, RelativeValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms), &Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_CellCoordinate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_RelativeValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMX|Fixture Patch" },
		{ "Comment", "/* Cell coordinate X/Y */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Cell coordinate X/Y" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixturePatch, nullptr, "SendNormalizedMatrixCellValue", nullptr, nullptr, sizeof(DMXEntityFixturePatch_eventSendNormalizedMatrixCellValue_Parms), Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXEntityFixturePatch_NoRegister()
	{
		return UDMXEntityFixturePatch::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEntityFixturePatch_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnFixturePatchReceivedDMX_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnFixturePatchReceivedDMX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniverseID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniverseID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoAssignAddress_MetaData[];
#endif
		static void NewProp_bAutoAssignAddress_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoAssignAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManualStartingAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ManualStartingAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutoStartingAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AutoStartingAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentFixtureTypeTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParentFixtureTypeTemplate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActiveMode;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CustomTags_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomTags_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CustomTags;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EditorColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReceiveDMXInEditor_MetaData[];
#endif
		static void NewProp_bReceiveDMXInEditor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReceiveDMXInEditor;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEntityFixturePatch_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXEntity,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXEntityFixturePatch_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_ContainsAttribute, "ContainsAttribute" }, // 419141797
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertAttributeMapToRawMap, "ConvertAttributeMapToRawMap" }, // 358379989
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertRawMapToAttributeMap, "ConvertRawMapToAttributeMap" }, // 1113228590
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_ConvertToValidMap, "ConvertToValidMap" }, // 1791074689
		{ &Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature, "DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature" }, // 426905255
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllAttributesInActiveMode, "GetAllAttributesInActiveMode" }, // 1493536127
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAllMatrixCells, "GetAllMatrixCells" }, // 3371161534
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeChannelAssignments, "GetAttributeChannelAssignments" }, // 2074658808
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeDefaultMap, "GetAttributeDefaultMap" }, // 2055331596
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeFunctionsMap, "GetAttributeFunctionsMap" }, // 3839845225
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeSignalFormats, "GetAttributeSignalFormats" }, // 10734348
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributesValues, "GetAttributesValues" }, // 1806647878
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetAttributeValue, "GetAttributeValue" }, // 4290704549
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetCellAttributes, "GetCellAttributes" }, // 4166670810
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetChannelSpan, "GetChannelSpan" }, // 2975837192
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetEndingChannel, "GetEndingChannel" }, // 4195026897
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCell, "GetMatrixCell" }, // 903906092
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsolute, "GetMatrixCellChannelsAbsolute" }, // 3892199691
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsAbsoluteWithValidation, "GetMatrixCellChannelsAbsoluteWithValidation" }, // 3994174819
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellChannelsRelative, "GetMatrixCellChannelsRelative" }, // 1033134161
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixCellValues, "GetMatrixCellValues" }, // 2375769541
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetMatrixProperties, "GetMatrixProperties" }, // 501524655
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributesValues, "GetNormalizedAttributesValues" }, // 3676425275
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedAttributeValue, "GetNormalizedAttributeValue" }, // 1670547467
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetNormalizedMatrixCellValues, "GetNormalizedMatrixCellValues" }, // 3553264676
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetRelevantControllers, "GetRelevantControllers" }, // 2772044323
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetRemoteUniverse, "GetRemoteUniverse" }, // 3253683012
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_GetStartingChannel, "GetStartingChannel" }, // 3334341280
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllerRange, "IsInControllerRange" }, // 1713360239
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_IsInControllersRange, "IsInControllersRange" }, // 72858309
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_IsMapValid, "IsMapValid" }, // 468116876
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_SendDMX, "SendDMX" }, // 997391280
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValue, "SendMatrixCellValue" }, // 1736452459
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_SendMatrixCellValueWithAttributeMap, "SendMatrixCellValueWithAttributeMap" }, // 13570456
		{ &Z_Construct_UFunction_UDMXEntityFixturePatch_SendNormalizedMatrixCellValue, "SendNormalizedMatrixCellValue" }, // 3504621454
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** \n * A DMX fixture patch that can be patch to channels in a DMX Universe via the DMX Library Editor. \n * \n * Use in DMXComponent or call SetReceiveDMXEnabled with 'true' to enable receiving DMX. \n */" },
		{ "DisplayName", "DMX Fixture Patch" },
		{ "IncludePath", "Library/DMXEntityFixturePatch.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "A DMX fixture patch that can be patch to channels in a DMX Universe via the DMX Library Editor.\n\nUse in DMXComponent or call SetReceiveDMXEnabled with 'true' to enable receiving DMX." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_OnFixturePatchReceivedDMX_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Broadcasts when the patch received dmx */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Broadcasts when the patch received dmx" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_OnFixturePatchReceivedDMX = { "OnFixturePatchReceivedDMX", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, OnFixturePatchReceivedDMX), Z_Construct_UDelegateFunction_UDMXEntityFixturePatch_DMXOnFixturePatchReceivedDMXDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_OnFixturePatchReceivedDMX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_OnFixturePatchReceivedDMX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_UniverseID_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "ClampMin", "0" },
		{ "Comment", "/** The local universe of the patch */" },
		{ "DisplayName", "Universe" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "The local universe of the patch" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_UniverseID = { "UniverseID", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, UniverseID), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_UniverseID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_UniverseID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "Comment", "/** Auto-assign address from drag/drop list order and available channels */" },
		{ "DisplayName", "Auto-Assign Address" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Auto-assign address from drag/drop list order and available channels" },
	};
#endif
	void Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress_SetBit(void* Obj)
	{
		((UDMXEntityFixturePatch*)Obj)->bAutoAssignAddress = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress = { "bAutoAssignAddress", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXEntityFixturePatch), &Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ManualStartingAddress_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "ClampMax", "512" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Starting channel for when auto-assign address is false */" },
		{ "DisplayName", "Manual Starting Address" },
		{ "EditCondition", "!bAutoAssignAddress" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Starting channel for when auto-assign address is false" },
		{ "UIMax", "512" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ManualStartingAddress = { "ManualStartingAddress", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, ManualStartingAddress), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ManualStartingAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ManualStartingAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_AutoStartingAddress_MetaData[] = {
		{ "Comment", "/** Starting channel from auto-assignment. Used when AutoAssignAddress is true */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Starting channel from auto-assignment. Used when AutoAssignAddress is true" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_AutoStartingAddress = { "AutoStartingAddress", nullptr, (EPropertyFlags)0x0020080400000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, AutoStartingAddress), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_AutoStartingAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_AutoStartingAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ParentFixtureTypeTemplate_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "Comment", "/** Property to point to the template parent fixture for details panel purposes */" },
		{ "DisplayName", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Property to point to the template parent fixture for details panel purposes" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ParentFixtureTypeTemplate = { "ParentFixtureTypeTemplate", nullptr, (EPropertyFlags)0x0020080000030015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, ParentFixtureTypeTemplate), Z_Construct_UClass_UDMXEntityFixtureType_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ParentFixtureTypeTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ParentFixtureTypeTemplate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ActiveMode_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "Comment", "/** The Index of the Mode in the Fixture Type the Patch uses */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "The Index of the Mode in the Fixture Type the Patch uses" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ActiveMode = { "ActiveMode", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, ActiveMode), METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ActiveMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ActiveMode_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags_Inner = { "CustomTags", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "Comment", "/** Custom tags for filtering patches  */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Custom tags for filtering patches" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags = { "CustomTags", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, CustomTags), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_EditorColor_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "Comment", "/** Color when displayed in the fixture patch editor */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "Color when displayed in the fixture patch editor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_EditorColor = { "EditorColor", nullptr, (EPropertyFlags)0x0010000800000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixturePatch, EditorColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_EditorColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_EditorColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor_MetaData[] = {
		{ "Category", "Fixture Patch" },
		{ "Comment", "/** \n\x09 * If true, the patch receives dmx and raises the OnFixturePatchReceivedDMX event in editor. \n\x09 * NOTE: DMXComponent does not support this. To receive the patch in editor, drag out from the patch and bind its OnFixturePatchReceivedDMX event!\n\x09 */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixturePatch.h" },
		{ "ToolTip", "If true, the patch receives dmx and raises the OnFixturePatchReceivedDMX event in editor.\nNOTE: DMXComponent does not support this. To receive the patch in editor, drag out from the patch and bind its OnFixturePatchReceivedDMX event!" },
	};
#endif
	void Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor_SetBit(void* Obj)
	{
		((UDMXEntityFixturePatch*)Obj)->bReceiveDMXInEditor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor = { "bReceiveDMXInEditor", nullptr, (EPropertyFlags)0x0010000800000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXEntityFixturePatch), &Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXEntityFixturePatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_OnFixturePatchReceivedDMX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_UniverseID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bAutoAssignAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ManualStartingAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_AutoStartingAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ParentFixtureTypeTemplate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_ActiveMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_CustomTags,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_EditorColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixturePatch_Statics::NewProp_bReceiveDMXInEditor,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEntityFixturePatch_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEntityFixturePatch>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEntityFixturePatch_Statics::ClassParams = {
		&UDMXEntityFixturePatch::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXEntityFixturePatch_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixturePatch_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEntityFixturePatch()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEntityFixturePatch_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEntityFixturePatch, 2766068223);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXEntityFixturePatch>()
	{
		return UDMXEntityFixturePatch::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEntityFixturePatch(Z_Construct_UClass_UDMXEntityFixturePatch, &UDMXEntityFixturePatch::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXEntityFixturePatch"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEntityFixturePatch);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
