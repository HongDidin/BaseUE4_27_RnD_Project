// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXRUNTIME_DMXEntityController_generated_h
#error "DMXEntityController.generated.h already included, missing '#pragma once' in DMXEntityController.h"
#endif
#define DMXRUNTIME_DMXEntityController_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXEntityUniverseManaged(); \
	friend struct Z_Construct_UClass_UDMXEntityUniverseManaged_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityUniverseManaged, UDMXEntity, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityUniverseManaged)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUDMXEntityUniverseManaged(); \
	friend struct Z_Construct_UClass_UDMXEntityUniverseManaged_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityUniverseManaged, UDMXEntity, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityUniverseManaged)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityUniverseManaged(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEntityUniverseManaged) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityUniverseManaged); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityUniverseManaged); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityUniverseManaged(UDMXEntityUniverseManaged&&); \
	NO_API UDMXEntityUniverseManaged(const UDMXEntityUniverseManaged&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityUniverseManaged() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityUniverseManaged(UDMXEntityUniverseManaged&&); \
	NO_API UDMXEntityUniverseManaged(const UDMXEntityUniverseManaged&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityUniverseManaged); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityUniverseManaged); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXEntityUniverseManaged)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_15_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXEntityUniverseManaged>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXEntityController(); \
	friend struct Z_Construct_UClass_UDMXEntityController_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityController, UDMXEntityUniverseManaged, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityController)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUDMXEntityController(); \
	friend struct Z_Construct_UClass_UDMXEntityController_Statics; \
public: \
	DECLARE_CLASS(UDMXEntityController, UDMXEntityUniverseManaged, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDMXEntityController)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXEntityController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityController(UDMXEntityController&&); \
	NO_API UDMXEntityController(const UDMXEntityController&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXEntityController() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXEntityController(UDMXEntityController&&); \
	NO_API UDMXEntityController(const UDMXEntityController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXEntityController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXEntityController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXEntityController)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_29_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXRUNTIME_API UClass* StaticClass<class UDMXEntityController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Library_DMXEntityController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
