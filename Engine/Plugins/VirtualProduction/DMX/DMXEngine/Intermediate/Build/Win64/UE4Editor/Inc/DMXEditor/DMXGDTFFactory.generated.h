// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXEDITOR_DMXGDTFFactory_generated_h
#error "DMXGDTFFactory.generated.h already included, missing '#pragma once' in DMXGDTFFactory.h"
#endif
#define DMXEDITOR_DMXGDTFFactory_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_SPARSE_DATA
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_RPC_WRAPPERS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDMXGDTFFactory(); \
	friend struct Z_Construct_UClass_UDMXGDTFFactory_Statics; \
public: \
	DECLARE_CLASS(UDMXGDTFFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXEditor"), NO_API) \
	DECLARE_SERIALIZER(UDMXGDTFFactory)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUDMXGDTFFactory(); \
	friend struct Z_Construct_UClass_UDMXGDTFFactory_Statics; \
public: \
	DECLARE_CLASS(UDMXGDTFFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DMXEditor"), NO_API) \
	DECLARE_SERIALIZER(UDMXGDTFFactory)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDMXGDTFFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDMXGDTFFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXGDTFFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXGDTFFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXGDTFFactory(UDMXGDTFFactory&&); \
	NO_API UDMXGDTFFactory(const UDMXGDTFFactory&); \
public:


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDMXGDTFFactory(UDMXGDTFFactory&&); \
	NO_API UDMXGDTFFactory(const UDMXGDTFFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDMXGDTFFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDMXGDTFFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDMXGDTFFactory)


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImportUI() { return STRUCT_OFFSET(UDMXGDTFFactory, ImportUI); }


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_14_PROLOG
#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_RPC_WRAPPERS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_INCLASS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_SPARSE_DATA \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DMXEDITOR_API UClass* StaticClass<class UDMXGDTFFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXEditor_Private_Factories_DMXGDTFFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
