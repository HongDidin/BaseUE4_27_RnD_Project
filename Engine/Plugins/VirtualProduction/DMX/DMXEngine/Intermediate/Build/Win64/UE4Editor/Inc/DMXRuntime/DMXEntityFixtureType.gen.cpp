// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXEntityFixtureType.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXEntityFixtureType() {}
// Cross Module References
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureMode();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureFunction();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureMatrix();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXCell();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureCellAttribute();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
	DMXPROTOCOL_API UEnum* Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixtureType_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntityFixtureType();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImport_NoRegister();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureCategory();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_NoRegister();
// End Cross Module References
	static UEnum* EDMXPixelMappingDistribution_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXPixelMappingDistribution"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXPixelMappingDistribution>()
	{
		return EDMXPixelMappingDistribution_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXPixelMappingDistribution(EDMXPixelMappingDistribution_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXPixelMappingDistribution"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution_Hash() { return 2337471123U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXPixelMappingDistribution"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXPixelMappingDistribution::TopLeftToRight", (int64)EDMXPixelMappingDistribution::TopLeftToRight },
				{ "EDMXPixelMappingDistribution::TopLeftToBottom", (int64)EDMXPixelMappingDistribution::TopLeftToBottom },
				{ "EDMXPixelMappingDistribution::TopLeftToClockwise", (int64)EDMXPixelMappingDistribution::TopLeftToClockwise },
				{ "EDMXPixelMappingDistribution::TopLeftToAntiClockwise", (int64)EDMXPixelMappingDistribution::TopLeftToAntiClockwise },
				{ "EDMXPixelMappingDistribution::TopRightToLeft", (int64)EDMXPixelMappingDistribution::TopRightToLeft },
				{ "EDMXPixelMappingDistribution::BottomLeftToTop", (int64)EDMXPixelMappingDistribution::BottomLeftToTop },
				{ "EDMXPixelMappingDistribution::TopRightToAntiClockwise", (int64)EDMXPixelMappingDistribution::TopRightToAntiClockwise },
				{ "EDMXPixelMappingDistribution::BottomLeftToClockwise", (int64)EDMXPixelMappingDistribution::BottomLeftToClockwise },
				{ "EDMXPixelMappingDistribution::BottomLeftToRight", (int64)EDMXPixelMappingDistribution::BottomLeftToRight },
				{ "EDMXPixelMappingDistribution::TopRightToBottom", (int64)EDMXPixelMappingDistribution::TopRightToBottom },
				{ "EDMXPixelMappingDistribution::BottomLeftAntiClockwise", (int64)EDMXPixelMappingDistribution::BottomLeftAntiClockwise },
				{ "EDMXPixelMappingDistribution::TopRightToClockwise", (int64)EDMXPixelMappingDistribution::TopRightToClockwise },
				{ "EDMXPixelMappingDistribution::BottomRightToLeft", (int64)EDMXPixelMappingDistribution::BottomRightToLeft },
				{ "EDMXPixelMappingDistribution::BottomRightToTop", (int64)EDMXPixelMappingDistribution::BottomRightToTop },
				{ "EDMXPixelMappingDistribution::BottomRightToClockwise", (int64)EDMXPixelMappingDistribution::BottomRightToClockwise },
				{ "EDMXPixelMappingDistribution::BottomRightToAntiClockwise", (int64)EDMXPixelMappingDistribution::BottomRightToAntiClockwise },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "BottomLeftAntiClockwise.Name", "EDMXPixelMappingDistribution::BottomLeftAntiClockwise" },
				{ "BottomLeftToClockwise.Name", "EDMXPixelMappingDistribution::BottomLeftToClockwise" },
				{ "BottomLeftToRight.Name", "EDMXPixelMappingDistribution::BottomLeftToRight" },
				{ "BottomLeftToTop.Name", "EDMXPixelMappingDistribution::BottomLeftToTop" },
				{ "BottomRightToAntiClockwise.Name", "EDMXPixelMappingDistribution::BottomRightToAntiClockwise" },
				{ "BottomRightToClockwise.Name", "EDMXPixelMappingDistribution::BottomRightToClockwise" },
				{ "BottomRightToLeft.Name", "EDMXPixelMappingDistribution::BottomRightToLeft" },
				{ "BottomRightToTop.Name", "EDMXPixelMappingDistribution::BottomRightToTop" },
				{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
				{ "TopLeftToAntiClockwise.Name", "EDMXPixelMappingDistribution::TopLeftToAntiClockwise" },
				{ "TopLeftToBottom.Name", "EDMXPixelMappingDistribution::TopLeftToBottom" },
				{ "TopLeftToClockwise.Name", "EDMXPixelMappingDistribution::TopLeftToClockwise" },
				{ "TopLeftToRight.Name", "EDMXPixelMappingDistribution::TopLeftToRight" },
				{ "TopRightToAntiClockwise.Name", "EDMXPixelMappingDistribution::TopRightToAntiClockwise" },
				{ "TopRightToBottom.Name", "EDMXPixelMappingDistribution::TopRightToBottom" },
				{ "TopRightToClockwise.Name", "EDMXPixelMappingDistribution::TopRightToClockwise" },
				{ "TopRightToLeft.Name", "EDMXPixelMappingDistribution::TopRightToLeft" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXPixelMappingDistribution",
				"EDMXPixelMappingDistribution",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FDMXFixtureMode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureMode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixtureMode, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXFixtureMode"), sizeof(FDMXFixtureMode), Get_Z_Construct_UScriptStruct_FDMXFixtureMode_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXFixtureMode>()
{
	return FDMXFixtureMode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixtureMode(FDMXFixtureMode::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXFixtureMode"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureMode
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureMode()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixtureMode>(FName(TEXT("DMXFixtureMode")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureMode;
	struct Z_Construct_UScriptStruct_FDMXFixtureMode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ModeName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Functions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Functions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Functions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelSpan_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChannelSpan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoChannelSpan_MetaData[];
#endif
		static void NewProp_bAutoChannelSpan_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoChannelSpan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixtureMatrixConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FixtureMatrixConfig;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixtureMode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ModeName_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ModeName = { "ModeName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMode, ModeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ModeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ModeName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions_Inner = { "Functions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXFixtureFunction, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayPriority", "15" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions = { "Functions", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMode, Functions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ChannelSpan_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMax", "512" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of channels (bytes) used by this mode's functions */" },
		{ "DisplayPriority", "10" },
		{ "EditCondition", "!bAutoChannelSpan" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Number of channels (bytes) used by this mode's functions" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ChannelSpan = { "ChannelSpan", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMode, ChannelSpan), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ChannelSpan_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ChannelSpan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * When enabled, ChannelSpan is automatically set based on the created functions and their data types.\n\x09 * If disabled, ChannelSpan can be manually set and functions and functions' channels beyond the\n\x09 * specified span will be ignored.\n\x09 */" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "When enabled, ChannelSpan is automatically set based on the created functions and their data types.\nIf disabled, ChannelSpan can be manually set and functions and functions' channels beyond the\nspecified span will be ignored." },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan_SetBit(void* Obj)
	{
		((FDMXFixtureMode*)Obj)->bAutoChannelSpan = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan = { "bAutoChannelSpan", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXFixtureMode), &Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_FixtureMatrixConfig_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_FixtureMatrixConfig = { "FixtureMatrixConfig", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMode, FixtureMatrixConfig), Z_Construct_UScriptStruct_FDMXFixtureMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_FixtureMatrixConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_FixtureMatrixConfig_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ModeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_Functions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_ChannelSpan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_bAutoChannelSpan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::NewProp_FixtureMatrixConfig,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXFixtureMode",
		sizeof(FDMXFixtureMode),
		alignof(FDMXFixtureMode),
		Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureMode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureMode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixtureMode"), sizeof(FDMXFixtureMode), Get_Z_Construct_UScriptStruct_FDMXFixtureMode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixtureMode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureMode_Hash() { return 3706170492U; }
class UScriptStruct* FDMXCell::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXCell_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXCell, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXCell"), sizeof(FDMXCell), Get_Z_Construct_UScriptStruct_FDMXCell_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXCell>()
{
	return FDMXCell::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXCell(FDMXCell::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXCell"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXCell
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXCell()
	{
		UScriptStruct::DeferCppStructOps<FDMXCell>(FName(TEXT("DMXCell")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXCell;
	struct Z_Construct_UScriptStruct_FDMXCell_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CellID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Coordinate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Coordinate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXCell_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXCell_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXCell>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_CellID_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMin", "0" },
		{ "Comment", "/** The cell index in a 1D Array (row order), starting from 0 */" },
		{ "DisplayName", "Cell ID" },
		{ "DisplayPriority", "20" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "The cell index in a 1D Array (row order), starting from 0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_CellID = { "CellID", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXCell, CellID), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_CellID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_CellID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_Coordinate_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** The cell coordinate in a 2D Array, starting from (0, 0) */" },
		{ "DisplayName", "Coordinate" },
		{ "DisplayPriority", "30" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "The cell coordinate in a 2D Array, starting from (0, 0)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_Coordinate = { "Coordinate", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXCell, Coordinate), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_Coordinate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_Coordinate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXCell_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_CellID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXCell_Statics::NewProp_Coordinate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXCell_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXCell",
		sizeof(FDMXCell),
		alignof(FDMXCell),
		Z_Construct_UScriptStruct_FDMXCell_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXCell_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXCell_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXCell_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXCell()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXCell_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXCell"), sizeof(FDMXCell), Get_Z_Construct_UScriptStruct_FDMXCell_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXCell_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXCell_Hash() { return 321460123U; }
class UScriptStruct* FDMXFixtureMatrix::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureMatrix_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixtureMatrix, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXFixtureMatrix"), sizeof(FDMXFixtureMatrix), Get_Z_Construct_UScriptStruct_FDMXFixtureMatrix_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXFixtureMatrix>()
{
	return FDMXFixtureMatrix::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixtureMatrix(FDMXFixtureMatrix::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXFixtureMatrix"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureMatrix
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureMatrix()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixtureMatrix>(FName(TEXT("DMXFixtureMatrix")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureMatrix;
	struct Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CellAttributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellAttributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CellAttributes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstCellChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FirstCellChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XCells_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_XCells;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YCells_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_YCells;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PixelMappingDistribution_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelMappingDistribution_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PixelMappingDistribution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixtureMatrix>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes_Inner = { "CellAttributes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXFixtureCellAttribute, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayName", "Cell Attributes" },
		{ "DisplayPriority", "60" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes = { "CellAttributes", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMatrix, CellAttributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_FirstCellChannel_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMax", "512" },
		{ "ClampMin", "1" },
		{ "DisplayName", "First Cell Channel" },
		{ "DisplayPriority", "20" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_FirstCellChannel = { "FirstCellChannel", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMatrix, FirstCellChannel), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_FirstCellChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_FirstCellChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_XCells_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMin", "0" },
		{ "DisplayName", "X Cells" },
		{ "DisplayPriority", "30" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_XCells = { "XCells", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMatrix, XCells), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_XCells_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_XCells_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_YCells_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMin", "0" },
		{ "DisplayName", "Y Cells" },
		{ "DisplayPriority", "40" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_YCells = { "YCells", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMatrix, YCells), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_YCells_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_YCells_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayName", "PixelMapping Distribution" },
		{ "DisplayPriority", "50" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution = { "PixelMappingDistribution", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureMatrix, PixelMappingDistribution), Z_Construct_UEnum_DMXRuntime_EDMXPixelMappingDistribution, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_CellAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_FirstCellChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_XCells,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_YCells,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::NewProp_PixelMappingDistribution,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXFixtureMatrix",
		sizeof(FDMXFixtureMatrix),
		alignof(FDMXFixtureMatrix),
		Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureMatrix()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureMatrix_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixtureMatrix"), sizeof(FDMXFixtureMatrix), Get_Z_Construct_UScriptStruct_FDMXFixtureMatrix_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixtureMatrix_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureMatrix_Hash() { return 4156844076U; }
class UScriptStruct* FDMXFixtureCellAttribute::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXFixtureCellAttribute"), sizeof(FDMXFixtureCellAttribute), Get_Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXFixtureCellAttribute>()
{
	return FDMXFixtureCellAttribute::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixtureCellAttribute(FDMXFixtureCellAttribute::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXFixtureCellAttribute"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureCellAttribute
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureCellAttribute()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixtureCellAttribute>(FName(TEXT("DMXFixtureCellAttribute")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureCellAttribute;
	struct Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_DefaultValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseLSBMode_MetaData[];
#endif
		static void NewProp_bUseLSBMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseLSBMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixtureCellAttribute>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Attribute_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * The Attribute name to map this Function to.\n\x09 * This is used to easily find the Function in Blueprints, using an Attribute\n\x09 * list instead of typing the Function name directly.\n\x09 * The list of Attributes can be edited on\n\x09 * Project Settings->Plugins->DMX Protocol->Fixture Settings->Fixture Function Attributes\n\x09 */" },
		{ "DisplayName", "Attribute Mapping" },
		{ "DisplayPriority", "11" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "The Attribute name to map this Function to.\nThis is used to easily find the Function in Blueprints, using an Attribute\nlist instead of typing the Function name directly.\nThe list of Attributes can be edited on\nProject Settings->Plugins->DMX Protocol->Fixture Settings->Fixture Function Attributes" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureCellAttribute, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Attribute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayName", "Description" },
		{ "DisplayPriority", "20" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureCellAttribute, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Initial value for this function when no value is set */" },
		{ "DisplayName", "Default Value" },
		{ "DisplayPriority", "30" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Initial value for this function when no value is set" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureCellAttribute, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DefaultValue_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** This function's data type. Defines the used number of channels (bytes) */" },
		{ "DisplayName", "Data Type" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "This function's data type. Defines the used number of channels (bytes)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType = { "DataType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureCellAttribute, DataType), Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * Least Significant Byte mode makes the individual bytes (channels) of the function be\n\x09 * interpreted with the first bytes being the lowest part of the number.\n\x09 *\n\x09 * E.g., given a 16 bit function with two channel values set to [0, 1],\n\x09 * they would be interpreted as the binary number 00000001 00000000, which means 256.\n\x09 * The first byte (0) became the lowest part in binary form and the following byte (1), the highest.\n\x09 *\n\x09 * Most Fixtures use MSB (Most Significant Byte) mode, which interprets bytes as highest first.\n\x09 * In MSB mode, the example above would be interpreted in binary as 00000000 00000001, which means 1.\n\x09 * The first byte (0) became the highest part in binary form and the following byte (1), the lowest.\n\x09 */" },
		{ "DisplayName", "Use LSB Mode" },
		{ "DisplayPriority", "29" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Least Significant Byte mode makes the individual bytes (channels) of the function be\ninterpreted with the first bytes being the lowest part of the number.\n\nE.g., given a 16 bit function with two channel values set to [0, 1],\nthey would be interpreted as the binary number 00000001 00000000, which means 256.\nThe first byte (0) became the lowest part in binary form and the following byte (1), the highest.\n\nMost Fixtures use MSB (Most Significant Byte) mode, which interprets bytes as highest first.\nIn MSB mode, the example above would be interpreted in binary as 00000000 00000001, which means 1.\nThe first byte (0) became the highest part in binary form and the following byte (1), the lowest." },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode_SetBit(void* Obj)
	{
		((FDMXFixtureCellAttribute*)Obj)->bUseLSBMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode = { "bUseLSBMode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXFixtureCellAttribute), &Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_DataType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::NewProp_bUseLSBMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXFixtureCellAttribute",
		sizeof(FDMXFixtureCellAttribute),
		alignof(FDMXFixtureCellAttribute),
		Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureCellAttribute()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixtureCellAttribute"), sizeof(FDMXFixtureCellAttribute), Get_Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureCellAttribute_Hash() { return 2107259484U; }
class UScriptStruct* FDMXFixtureFunction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureFunction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXFixtureFunction, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXFixtureFunction"), sizeof(FDMXFixtureFunction), Get_Z_Construct_UScriptStruct_FDMXFixtureFunction_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXFixtureFunction>()
{
	return FDMXFixtureFunction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXFixtureFunction(FDMXFixtureFunction::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXFixtureFunction"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureFunction
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureFunction()
	{
		UScriptStruct::DeferCppStructOps<FDMXFixtureFunction>(FName(TEXT("DMXFixtureFunction")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXFixtureFunction;
	struct Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FunctionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FunctionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_DefaultValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Channel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChannelOffset;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseLSBMode_MetaData[];
#endif
		static void NewProp_bUseLSBMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseLSBMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXFixtureFunction>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Attribute_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * The Attribute name to map this Function to.\n\x09 * This is used to easily find the Function in Blueprints, using an Attribute\n\x09 * list instead of typing the Function name directly.\n\x09 * The list of Attributes can be edited on\n\x09 * Project Settings->Plugins->DMX Protocol->Fixture Settings->Fixture Function Attributes\n\x09 */" },
		{ "DisplayName", "Attribute Mapping" },
		{ "DisplayPriority", "11" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "The Attribute name to map this Function to.\nThis is used to easily find the Function in Blueprints, using an Attribute\nlist instead of typing the Function name directly.\nThe list of Attributes can be edited on\nProject Settings->Plugins->DMX Protocol->Fixture Settings->Fixture Function Attributes" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, Attribute), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Attribute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_FunctionName_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayPriority", "10" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_FunctionName = { "FunctionName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, FunctionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_FunctionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_FunctionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "DMX" },
		{ "DisplayPriority", "20" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DefaultValue_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** Initial value for this function when no value is set */" },
		{ "DisplayPriority", "30" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Initial value for this function when no value is set" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DefaultValue = { "DefaultValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, DefaultValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DefaultValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Channel_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMax", "512" },
		{ "ClampMin", "1" },
		{ "Comment", "/** This function's starting channel */" },
		{ "DisplayName", "Channel Assignment" },
		{ "DisplayPriority", "2" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "This function's starting channel" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, Channel), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Channel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_ChannelOffset_MetaData[] = {
		{ "Category", "DMX" },
		{ "ClampMax", "511" },
		{ "ClampMin", "0" },
		{ "Comment", "/**\n\x09 * This function's channel offset.\n\x09 * E.g.: if the function's starting channel is supposed to be 10\n\x09 * and ChannelOffset = 5, the function's starting channel becomes 15\n\x09 * and all following functions follow it accordingly.\n\x09 */" },
		{ "DisplayName", "Channel Offset" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "This function's channel offset.\nE.g.: if the function's starting channel is supposed to be 10\nand ChannelOffset = 5, the function's starting channel becomes 15\nand all following functions follow it accordingly." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_ChannelOffset = { "ChannelOffset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, ChannelOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_ChannelOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_ChannelOffset_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/** This function's data type. Defines the used number of channels (bytes) */" },
		{ "DisplayPriority", "5" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "This function's data type. Defines the used number of channels (bytes)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType = { "DataType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXFixtureFunction, DataType), Z_Construct_UEnum_DMXProtocol_EDMXFixtureSignalFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode_MetaData[] = {
		{ "Category", "DMX" },
		{ "Comment", "/**\n\x09 * Least Significant Byte mode makes the individual bytes (channels) of the function be\n\x09 * interpreted with the first bytes being the lowest part of the number.\n\x09 * \n\x09 * E.g., given a 16 bit function with two channel values set to [0, 1],\n\x09 * they would be interpreted as the binary number 00000001 00000000, which means 256.\n\x09 * The first byte (0) became the lowest part in binary form and the following byte (1), the highest.\n\x09 * \n\x09 * Most Fixtures use MSB (Most Significant Byte) mode, which interprets bytes as highest first.\n\x09 * In MSB mode, the example above would be interpreted in binary as 00000000 00000001, which means 1.\n\x09 * The first byte (0) became the highest part in binary form and the following byte (1), the lowest.\n\x09 */" },
		{ "DisplayName", "Use LSB Mode" },
		{ "DisplayPriority", "29" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Least Significant Byte mode makes the individual bytes (channels) of the function be\ninterpreted with the first bytes being the lowest part of the number.\n\nE.g., given a 16 bit function with two channel values set to [0, 1],\nthey would be interpreted as the binary number 00000001 00000000, which means 256.\nThe first byte (0) became the lowest part in binary form and the following byte (1), the highest.\n\nMost Fixtures use MSB (Most Significant Byte) mode, which interprets bytes as highest first.\nIn MSB mode, the example above would be interpreted in binary as 00000000 00000001, which means 1.\nThe first byte (0) became the highest part in binary form and the following byte (1), the lowest." },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode_SetBit(void* Obj)
	{
		((FDMXFixtureFunction*)Obj)->bUseLSBMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode = { "bUseLSBMode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXFixtureFunction), &Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_FunctionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_ChannelOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_DataType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::NewProp_bUseLSBMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXFixtureFunction",
		sizeof(FDMXFixtureFunction),
		alignof(FDMXFixtureFunction),
		Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXFixtureFunction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureFunction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXFixtureFunction"), sizeof(FDMXFixtureFunction), Get_Z_Construct_UScriptStruct_FDMXFixtureFunction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXFixtureFunction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXFixtureFunction_Hash() { return 2220189451U; }
#if WITH_EDITOR
	DEFINE_FUNCTION(UDMXEntityFixtureType::execSetModesFromDMXImport)
	{
		P_GET_OBJECT(UDMXImport,Z_Param_DMXImportAsset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetModesFromDMXImport(Z_Param_DMXImportAsset);
		P_NATIVE_END;
	}
#endif //WITH_EDITOR
	void UDMXEntityFixtureType::StaticRegisterNativesUDMXEntityFixtureType()
	{
#if WITH_EDITOR
		UClass* Class = UDMXEntityFixtureType::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetModesFromDMXImport", &UDMXEntityFixtureType::execSetModesFromDMXImport },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
#endif // WITH_EDITOR
	}
#if WITH_EDITOR
	struct Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics
	{
		struct DMXEntityFixtureType_eventSetModesFromDMXImport_Parms
		{
			UDMXImport* DMXImportAsset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXImportAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::NewProp_DMXImportAsset = { "DMXImportAsset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXEntityFixtureType_eventSetModesFromDMXImport_Parms, DMXImportAsset), Z_Construct_UClass_UDMXImport_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::NewProp_DMXImportAsset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::Function_MetaDataParams[] = {
		{ "Category", "Fixture Settings" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXEntityFixtureType, nullptr, "SetModesFromDMXImport", nullptr, nullptr, sizeof(DMXEntityFixtureType_eventSetModesFromDMXImport_Parms), Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x24020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
#endif //WITH_EDITOR
	UClass* Z_Construct_UClass_UDMXEntityFixtureType_NoRegister()
	{
		return UDMXEntityFixtureType::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEntityFixtureType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_EDITOR
		static const FClassFunctionLinkInfo FuncInfo[];
#endif //WITH_EDITOR
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXImport_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DMXImport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXCategory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXCategory;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFixtureMatrixEnabled_MetaData[];
#endif
		static void NewProp_bFixtureMatrixEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFixtureMatrixEnabled;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Modes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputModulators_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InputModulators_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputModulators_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InputModulators;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEntityFixtureType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXEntity,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_EDITOR
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXEntityFixtureType_Statics::FuncInfo[] = {
#if WITH_EDITOR
		{ &Z_Construct_UFunction_UDMXEntityFixtureType_SetModesFromDMXImport, "SetModesFromDMXImport" }, // 3269702757
#endif //WITH_EDITOR
	};
#endif //WITH_EDITOR
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "DMX Fixture Type" },
		{ "IncludePath", "Library/DMXEntityFixtureType.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXImport_MetaData[] = {
		{ "Category", "Fixture Settings" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXImport = { "DMXImport", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixtureType, DMXImport), Z_Construct_UClass_UDMXImport_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXImport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXImport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXCategory_MetaData[] = {
		{ "Category", "Fixture Settings" },
		{ "DisplayName", "DMX Category" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXCategory = { "DMXCategory", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixtureType, DMXCategory), Z_Construct_UScriptStruct_FDMXFixtureCategory, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXCategory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXCategory_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled_MetaData[] = {
		{ "Category", "Fixture Settings" },
		{ "DisplayName", "Matrix Fixture" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	void Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled_SetBit(void* Obj)
	{
		((UDMXEntityFixtureType*)Obj)->bFixtureMatrixEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled = { "bFixtureMatrixEnabled", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDMXEntityFixtureType), &Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes_Inner = { "Modes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXFixtureMode, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes_MetaData[] = {
		{ "Category", "Fixture Settings" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes = { "Modes", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixtureType, Modes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_Inner_MetaData[] = {
		{ "Category", "Fixture Settings" },
		{ "Comment", "/** \n\x09 * Modulators applied right before a patch of this type is received. \n\x09 * NOTE: Modulators only affect the patch's normalized values! Untouched values are still available when accesing raw values. \n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Modulators applied right before a patch of this type is received.\nNOTE: Modulators only affect the patch's normalized values! Untouched values are still available when accesing raw values." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_Inner = { "InputModulators", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDMXModulator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_MetaData[] = {
		{ "Category", "Fixture Settings" },
		{ "Comment", "/** \n\x09 * Modulators applied right before a patch of this type is received. \n\x09 * NOTE: Modulators only affect the patch's normalized values! Untouched values are still available when accesing raw values. \n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXEntityFixtureType.h" },
		{ "ToolTip", "Modulators applied right before a patch of this type is received.\nNOTE: Modulators only affect the patch's normalized values! Untouched values are still available when accesing raw values." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators = { "InputModulators", nullptr, (EPropertyFlags)0x0010008000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntityFixtureType, InputModulators), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXEntityFixtureType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXImport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_DMXCategory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_bFixtureMatrixEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_Modes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntityFixtureType_Statics::NewProp_InputModulators,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEntityFixtureType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEntityFixtureType>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEntityFixtureType_Statics::ClassParams = {
		&UDMXEntityFixtureType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		IF_WITH_EDITOR(FuncInfo, nullptr),
		Z_Construct_UClass_UDMXEntityFixtureType_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		IF_WITH_EDITOR(UE_ARRAY_COUNT(FuncInfo), 0),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEntityFixtureType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntityFixtureType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEntityFixtureType()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEntityFixtureType_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEntityFixtureType, 457997140);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXEntityFixtureType>()
	{
		return UDMXEntityFixtureType::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEntityFixtureType(Z_Construct_UClass_UDMXEntityFixtureType, &UDMXEntityFixtureType::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXEntityFixtureType"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEntityFixtureType);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
