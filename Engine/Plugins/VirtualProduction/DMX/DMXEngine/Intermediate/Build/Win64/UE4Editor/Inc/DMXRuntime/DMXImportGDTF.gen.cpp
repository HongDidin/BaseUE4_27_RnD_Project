// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXImportGDTF.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXImportGDTF() {}
// Cross Module References
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFInterpolationTo();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMode();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPhysicalUnit();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPrimitiveType();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFBeamType();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFLampType();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFDMXInvert();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMaster();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFSnap();
	DMXRUNTIME_API UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFType();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFRelation();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFAttribute();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFWheel();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFEmitter();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilter();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFBreak();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFBeam();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFModel();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFCRIs();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXColorCIE();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFeature();
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFFixtureType_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFFixtureType();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportFixtureType();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportAttributeDefinitions();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFWheels_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFWheels();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportWheels();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportPhysicalDescriptions();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFModels_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFModels();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportModels();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFGeometries_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFGeometries();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGeometries();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFDMXModes_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFDMXModes();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportDMXModes();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFProtocols_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTFProtocols();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportProtocols();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTF_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImportGDTF();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXImport();
// End Cross Module References
	static UEnum* EDMXImportGDTFInterpolationTo_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFInterpolationTo, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFInterpolationTo"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFInterpolationTo>()
	{
		return EDMXImportGDTFInterpolationTo_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFInterpolationTo(EDMXImportGDTFInterpolationTo_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFInterpolationTo"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFInterpolationTo_Hash() { return 1371441714U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFInterpolationTo()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFInterpolationTo"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFInterpolationTo_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFInterpolationTo::Linear", (int64)EDMXImportGDTFInterpolationTo::Linear },
				{ "EDMXImportGDTFInterpolationTo::Step", (int64)EDMXImportGDTFInterpolationTo::Step },
				{ "EDMXImportGDTFInterpolationTo::Log", (int64)EDMXImportGDTFInterpolationTo::Log },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Linear.Name", "EDMXImportGDTFInterpolationTo::Linear" },
				{ "Log.Name", "EDMXImportGDTFInterpolationTo::Log" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "Step.Name", "EDMXImportGDTFInterpolationTo::Step" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFInterpolationTo",
				"EDMXImportGDTFInterpolationTo",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMode, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFMode"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFMode>()
	{
		return EDMXImportGDTFMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFMode(EDMXImportGDTFMode_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMode_Hash() { return 1520621939U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFMode"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFMode::Custom", (int64)EDMXImportGDTFMode::Custom },
				{ "EDMXImportGDTFMode::sRGB", (int64)EDMXImportGDTFMode::sRGB },
				{ "EDMXImportGDTFMode::ProPhoto", (int64)EDMXImportGDTFMode::ProPhoto },
				{ "EDMXImportGDTFMode::ANSI", (int64)EDMXImportGDTFMode::ANSI },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ANSI.Name", "EDMXImportGDTFMode::ANSI" },
				{ "BlueprintType", "true" },
				{ "Custom.Name", "EDMXImportGDTFMode::Custom" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "ProPhoto.Name", "EDMXImportGDTFMode::ProPhoto" },
				{ "sRGB.Name", "EDMXImportGDTFMode::sRGB" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFMode",
				"EDMXImportGDTFMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFPhysicalUnit_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPhysicalUnit, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFPhysicalUnit"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFPhysicalUnit>()
	{
		return EDMXImportGDTFPhysicalUnit_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFPhysicalUnit(EDMXImportGDTFPhysicalUnit_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFPhysicalUnit"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPhysicalUnit_Hash() { return 2915140094U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPhysicalUnit()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFPhysicalUnit"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPhysicalUnit_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFPhysicalUnit::None", (int64)EDMXImportGDTFPhysicalUnit::None },
				{ "EDMXImportGDTFPhysicalUnit::Percent", (int64)EDMXImportGDTFPhysicalUnit::Percent },
				{ "EDMXImportGDTFPhysicalUnit::Length", (int64)EDMXImportGDTFPhysicalUnit::Length },
				{ "EDMXImportGDTFPhysicalUnit::Mass", (int64)EDMXImportGDTFPhysicalUnit::Mass },
				{ "EDMXImportGDTFPhysicalUnit::Time", (int64)EDMXImportGDTFPhysicalUnit::Time },
				{ "EDMXImportGDTFPhysicalUnit::Temperature", (int64)EDMXImportGDTFPhysicalUnit::Temperature },
				{ "EDMXImportGDTFPhysicalUnit::LuminousIntensity", (int64)EDMXImportGDTFPhysicalUnit::LuminousIntensity },
				{ "EDMXImportGDTFPhysicalUnit::Angle", (int64)EDMXImportGDTFPhysicalUnit::Angle },
				{ "EDMXImportGDTFPhysicalUnit::Force", (int64)EDMXImportGDTFPhysicalUnit::Force },
				{ "EDMXImportGDTFPhysicalUnit::Frequency", (int64)EDMXImportGDTFPhysicalUnit::Frequency },
				{ "EDMXImportGDTFPhysicalUnit::Current", (int64)EDMXImportGDTFPhysicalUnit::Current },
				{ "EDMXImportGDTFPhysicalUnit::Voltage", (int64)EDMXImportGDTFPhysicalUnit::Voltage },
				{ "EDMXImportGDTFPhysicalUnit::Power", (int64)EDMXImportGDTFPhysicalUnit::Power },
				{ "EDMXImportGDTFPhysicalUnit::Energy", (int64)EDMXImportGDTFPhysicalUnit::Energy },
				{ "EDMXImportGDTFPhysicalUnit::Area", (int64)EDMXImportGDTFPhysicalUnit::Area },
				{ "EDMXImportGDTFPhysicalUnit::Volume", (int64)EDMXImportGDTFPhysicalUnit::Volume },
				{ "EDMXImportGDTFPhysicalUnit::Speed", (int64)EDMXImportGDTFPhysicalUnit::Speed },
				{ "EDMXImportGDTFPhysicalUnit::Acceleration", (int64)EDMXImportGDTFPhysicalUnit::Acceleration },
				{ "EDMXImportGDTFPhysicalUnit::AngularSpeed", (int64)EDMXImportGDTFPhysicalUnit::AngularSpeed },
				{ "EDMXImportGDTFPhysicalUnit::AngularAccc", (int64)EDMXImportGDTFPhysicalUnit::AngularAccc },
				{ "EDMXImportGDTFPhysicalUnit::WaveLength", (int64)EDMXImportGDTFPhysicalUnit::WaveLength },
				{ "EDMXImportGDTFPhysicalUnit::ColorComponent", (int64)EDMXImportGDTFPhysicalUnit::ColorComponent },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Acceleration.Name", "EDMXImportGDTFPhysicalUnit::Acceleration" },
				{ "Angle.Name", "EDMXImportGDTFPhysicalUnit::Angle" },
				{ "AngularAccc.Name", "EDMXImportGDTFPhysicalUnit::AngularAccc" },
				{ "AngularSpeed.Name", "EDMXImportGDTFPhysicalUnit::AngularSpeed" },
				{ "Area.Name", "EDMXImportGDTFPhysicalUnit::Area" },
				{ "BlueprintType", "true" },
				{ "ColorComponent.Name", "EDMXImportGDTFPhysicalUnit::ColorComponent" },
				{ "Current.Name", "EDMXImportGDTFPhysicalUnit::Current" },
				{ "Energy.Name", "EDMXImportGDTFPhysicalUnit::Energy" },
				{ "Force.Name", "EDMXImportGDTFPhysicalUnit::Force" },
				{ "Frequency.Name", "EDMXImportGDTFPhysicalUnit::Frequency" },
				{ "Length.Name", "EDMXImportGDTFPhysicalUnit::Length" },
				{ "LuminousIntensity.Name", "EDMXImportGDTFPhysicalUnit::LuminousIntensity" },
				{ "Mass.Name", "EDMXImportGDTFPhysicalUnit::Mass" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "None.Name", "EDMXImportGDTFPhysicalUnit::None" },
				{ "Percent.Name", "EDMXImportGDTFPhysicalUnit::Percent" },
				{ "Power.Name", "EDMXImportGDTFPhysicalUnit::Power" },
				{ "Speed.Name", "EDMXImportGDTFPhysicalUnit::Speed" },
				{ "Temperature.Name", "EDMXImportGDTFPhysicalUnit::Temperature" },
				{ "Time.Name", "EDMXImportGDTFPhysicalUnit::Time" },
				{ "Voltage.Name", "EDMXImportGDTFPhysicalUnit::Voltage" },
				{ "Volume.Name", "EDMXImportGDTFPhysicalUnit::Volume" },
				{ "WaveLength.Name", "EDMXImportGDTFPhysicalUnit::WaveLength" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFPhysicalUnit",
				"EDMXImportGDTFPhysicalUnit",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFPrimitiveType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPrimitiveType, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFPrimitiveType"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFPrimitiveType>()
	{
		return EDMXImportGDTFPrimitiveType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFPrimitiveType(EDMXImportGDTFPrimitiveType_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFPrimitiveType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPrimitiveType_Hash() { return 3996242216U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPrimitiveType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFPrimitiveType"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPrimitiveType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFPrimitiveType::Undefined", (int64)EDMXImportGDTFPrimitiveType::Undefined },
				{ "EDMXImportGDTFPrimitiveType::Cube", (int64)EDMXImportGDTFPrimitiveType::Cube },
				{ "EDMXImportGDTFPrimitiveType::Cylinder", (int64)EDMXImportGDTFPrimitiveType::Cylinder },
				{ "EDMXImportGDTFPrimitiveType::Sphere", (int64)EDMXImportGDTFPrimitiveType::Sphere },
				{ "EDMXImportGDTFPrimitiveType::Base", (int64)EDMXImportGDTFPrimitiveType::Base },
				{ "EDMXImportGDTFPrimitiveType::Yoke", (int64)EDMXImportGDTFPrimitiveType::Yoke },
				{ "EDMXImportGDTFPrimitiveType::Head", (int64)EDMXImportGDTFPrimitiveType::Head },
				{ "EDMXImportGDTFPrimitiveType::Scanner", (int64)EDMXImportGDTFPrimitiveType::Scanner },
				{ "EDMXImportGDTFPrimitiveType::Conventional", (int64)EDMXImportGDTFPrimitiveType::Conventional },
				{ "EDMXImportGDTFPrimitiveType::Pigtail", (int64)EDMXImportGDTFPrimitiveType::Pigtail },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Base.Name", "EDMXImportGDTFPrimitiveType::Base" },
				{ "BlueprintType", "true" },
				{ "Conventional.Name", "EDMXImportGDTFPrimitiveType::Conventional" },
				{ "Cube.Name", "EDMXImportGDTFPrimitiveType::Cube" },
				{ "Cylinder.Name", "EDMXImportGDTFPrimitiveType::Cylinder" },
				{ "Head.Name", "EDMXImportGDTFPrimitiveType::Head" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "Pigtail.Name", "EDMXImportGDTFPrimitiveType::Pigtail" },
				{ "Scanner.Name", "EDMXImportGDTFPrimitiveType::Scanner" },
				{ "Sphere.Name", "EDMXImportGDTFPrimitiveType::Sphere" },
				{ "Undefined.Name", "EDMXImportGDTFPrimitiveType::Undefined" },
				{ "Yoke.Name", "EDMXImportGDTFPrimitiveType::Yoke" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFPrimitiveType",
				"EDMXImportGDTFPrimitiveType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFBeamType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFBeamType, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFBeamType"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFBeamType>()
	{
		return EDMXImportGDTFBeamType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFBeamType(EDMXImportGDTFBeamType_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFBeamType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFBeamType_Hash() { return 2892111536U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFBeamType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFBeamType"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFBeamType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFBeamType::Wash", (int64)EDMXImportGDTFBeamType::Wash },
				{ "EDMXImportGDTFBeamType::Spot", (int64)EDMXImportGDTFBeamType::Spot },
				{ "EDMXImportGDTFBeamType::None", (int64)EDMXImportGDTFBeamType::None },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "None.Name", "EDMXImportGDTFBeamType::None" },
				{ "Spot.Name", "EDMXImportGDTFBeamType::Spot" },
				{ "Wash.Name", "EDMXImportGDTFBeamType::Wash" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFBeamType",
				"EDMXImportGDTFBeamType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFLampType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFLampType, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFLampType"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFLampType>()
	{
		return EDMXImportGDTFLampType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFLampType(EDMXImportGDTFLampType_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFLampType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFLampType_Hash() { return 2302243466U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFLampType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFLampType"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFLampType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFLampType::Discharge", (int64)EDMXImportGDTFLampType::Discharge },
				{ "EDMXImportGDTFLampType::Tungsten", (int64)EDMXImportGDTFLampType::Tungsten },
				{ "EDMXImportGDTFLampType::Halogen", (int64)EDMXImportGDTFLampType::Halogen },
				{ "EDMXImportGDTFLampType::LED", (int64)EDMXImportGDTFLampType::LED },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Discharge.Name", "EDMXImportGDTFLampType::Discharge" },
				{ "Halogen.Name", "EDMXImportGDTFLampType::Halogen" },
				{ "LED.Name", "EDMXImportGDTFLampType::LED" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "Tungsten.Name", "EDMXImportGDTFLampType::Tungsten" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFLampType",
				"EDMXImportGDTFLampType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFDMXInvert_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFDMXInvert, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFDMXInvert"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFDMXInvert>()
	{
		return EDMXImportGDTFDMXInvert_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFDMXInvert(EDMXImportGDTFDMXInvert_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFDMXInvert"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFDMXInvert_Hash() { return 1471755423U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFDMXInvert()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFDMXInvert"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFDMXInvert_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFDMXInvert::Yes", (int64)EDMXImportGDTFDMXInvert::Yes },
				{ "EDMXImportGDTFDMXInvert::No", (int64)EDMXImportGDTFDMXInvert::No },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "No.Name", "EDMXImportGDTFDMXInvert::No" },
				{ "Yes.Name", "EDMXImportGDTFDMXInvert::Yes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFDMXInvert",
				"EDMXImportGDTFDMXInvert",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFMaster_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMaster, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFMaster"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFMaster>()
	{
		return EDMXImportGDTFMaster_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFMaster(EDMXImportGDTFMaster_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFMaster"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMaster_Hash() { return 2239001722U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMaster()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFMaster"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMaster_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFMaster::None", (int64)EDMXImportGDTFMaster::None },
				{ "EDMXImportGDTFMaster::Grand", (int64)EDMXImportGDTFMaster::Grand },
				{ "EDMXImportGDTFMaster::Group", (int64)EDMXImportGDTFMaster::Group },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Grand.Name", "EDMXImportGDTFMaster::Grand" },
				{ "Group.Name", "EDMXImportGDTFMaster::Group" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "None.Name", "EDMXImportGDTFMaster::None" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFMaster",
				"EDMXImportGDTFMaster",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFSnap_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFSnap, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFSnap"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFSnap>()
	{
		return EDMXImportGDTFSnap_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFSnap(EDMXImportGDTFSnap_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFSnap"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFSnap_Hash() { return 3393930936U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFSnap()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFSnap"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFSnap_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFSnap::Yes", (int64)EDMXImportGDTFSnap::Yes },
				{ "EDMXImportGDTFSnap::No", (int64)EDMXImportGDTFSnap::No },
				{ "EDMXImportGDTFSnap::On", (int64)EDMXImportGDTFSnap::On },
				{ "EDMXImportGDTFSnap::Off", (int64)EDMXImportGDTFSnap::Off },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "No.Name", "EDMXImportGDTFSnap::No" },
				{ "Off.Name", "EDMXImportGDTFSnap::Off" },
				{ "On.Name", "EDMXImportGDTFSnap::On" },
				{ "Yes.Name", "EDMXImportGDTFSnap::Yes" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFSnap",
				"EDMXImportGDTFSnap",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDMXImportGDTFType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFType, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("EDMXImportGDTFType"));
		}
		return Singleton;
	}
	template<> DMXRUNTIME_API UEnum* StaticEnum<EDMXImportGDTFType>()
	{
		return EDMXImportGDTFType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDMXImportGDTFType(EDMXImportGDTFType_StaticEnum, TEXT("/Script/DMXRuntime"), TEXT("EDMXImportGDTFType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFType_Hash() { return 551617658U; }
	UEnum* Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDMXImportGDTFType"), 0, Get_Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDMXImportGDTFType::Multiply", (int64)EDMXImportGDTFType::Multiply },
				{ "EDMXImportGDTFType::Override", (int64)EDMXImportGDTFType::Override },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
				{ "Multiply.Name", "EDMXImportGDTFType::Multiply" },
				{ "Override.Name", "EDMXImportGDTFType::Override" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DMXRuntime,
				nullptr,
				"EDMXImportGDTFType",
				"EDMXImportGDTFType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FDMXImportGDTFDMXMode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFDMXMode"), sizeof(FDMXImportGDTFDMXMode), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFDMXMode>()
{
	return FDMXImportGDTFDMXMode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFDMXMode(FDMXImportGDTFDMXMode::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFDMXMode"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXMode
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXMode()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFDMXMode>(FName(TEXT("DMXImportGDTFDMXMode")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXMode;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Geometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Geometry;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXChannels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChannels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DMXChannels;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Relations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Relations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Relations;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FTMacros_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FTMacros_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FTMacros;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFDMXMode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXMode, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Geometry_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Geometry = { "Geometry", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXMode, Geometry), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Geometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Geometry_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels_Inner = { "DMXChannels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels = { "DMXChannels", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXMode, DMXChannels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations_Inner = { "Relations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFRelation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations = { "Relations", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXMode, Relations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros_Inner = { "FTMacros", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros = { "FTMacros", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXMode, FTMacros), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Geometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_DMXChannels,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_Relations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::NewProp_FTMacros,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFDMXMode",
		sizeof(FDMXImportGDTFDMXMode),
		alignof(FDMXImportGDTFDMXMode),
		Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFDMXMode"), sizeof(FDMXImportGDTFDMXMode), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode_Hash() { return 780934287U; }
class UScriptStruct* FDMXImportGDTFFTMacro::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFTMacro"), sizeof(FDMXImportGDTFFTMacro), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFTMacro>()
{
	return FDMXImportGDTFFTMacro::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFTMacro(FDMXImportGDTFFTMacro::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFTMacro"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFTMacro
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFTMacro()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFTMacro>(FName(TEXT("DMXImportGDTFFTMacro")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFTMacro;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFTMacro>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFTMacro, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFFTMacro",
		sizeof(FDMXImportGDTFFTMacro),
		alignof(FDMXImportGDTFFTMacro),
		Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFTMacro"), sizeof(FDMXImportGDTFFTMacro), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFTMacro_Hash() { return 3473680076U; }
class UScriptStruct* FDMXImportGDTFRelation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFRelation, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFRelation"), sizeof(FDMXImportGDTFRelation), Get_Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFRelation>()
{
	return FDMXImportGDTFRelation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFRelation(FDMXImportGDTFRelation::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFRelation"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFRelation
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFRelation()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFRelation>(FName(TEXT("DMXImportGDTFRelation")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFRelation;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Master_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Master;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Follower_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Follower;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFRelation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFRelation, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Master_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Master = { "Master", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFRelation, Master), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Master_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Master_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Follower_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Follower = { "Follower", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFRelation, Follower), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Follower_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Follower_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFRelation, Type), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Master,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Follower,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::NewProp_Type,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFRelation",
		sizeof(FDMXImportGDTFRelation),
		alignof(FDMXImportGDTFRelation),
		Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFRelation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFRelation"), sizeof(FDMXImportGDTFRelation), Get_Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFRelation_Hash() { return 23492550U; }
class UScriptStruct* FDMXImportGDTFDMXChannel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFDMXChannel"), sizeof(FDMXImportGDTFDMXChannel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFDMXChannel>()
{
	return FDMXImportGDTFDMXChannel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFDMXChannel(FDMXImportGDTFDMXChannel::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFDMXChannel"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXChannel
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXChannel()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFDMXChannel>(FName(TEXT("DMXImportGDTFDMXChannel")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXChannel;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXBreak_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DMXBreak;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Offset_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Offset_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Offset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Highlight_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Highlight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Geometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Geometry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogicalChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LogicalChannel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFDMXChannel>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_DMXBreak_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_DMXBreak = { "DMXBreak", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXChannel, DMXBreak), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_DMXBreak_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_DMXBreak_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset_Inner = { "Offset", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset = { "Offset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXChannel, Offset), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Default_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Default = { "Default", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXChannel, Default), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Default_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Highlight_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Highlight = { "Highlight", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXChannel, Highlight), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Highlight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Highlight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Geometry_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Geometry = { "Geometry", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXChannel, Geometry), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Geometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Geometry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_LogicalChannel_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_LogicalChannel = { "LogicalChannel", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXChannel, LogicalChannel), Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_LogicalChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_LogicalChannel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_DMXBreak,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Offset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Highlight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_Geometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::NewProp_LogicalChannel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFDMXChannel",
		sizeof(FDMXImportGDTFDMXChannel),
		alignof(FDMXImportGDTFDMXChannel),
		Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFDMXChannel"), sizeof(FDMXImportGDTFDMXChannel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXChannel_Hash() { return 3663055661U; }
class UScriptStruct* FDMXImportGDTFLogicalChannel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFLogicalChannel"), sizeof(FDMXImportGDTFLogicalChannel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFLogicalChannel>()
{
	return FDMXImportGDTFLogicalChannel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFLogicalChannel(FDMXImportGDTFLogicalChannel::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFLogicalChannel"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFLogicalChannel
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFLogicalChannel()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFLogicalChannel>(FName(TEXT("DMXImportGDTFLogicalChannel")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFLogicalChannel;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Snap_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Snap_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Snap;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Master_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Master_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Master;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MibFade_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MibFade;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXChangeTimeLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DMXChangeTimeLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChannelFunction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFLogicalChannel>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Attribute_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFLogicalChannel, Attribute), Z_Construct_UScriptStruct_FDMXImportGDTFAttribute, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Attribute_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap = { "Snap", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFLogicalChannel, Snap), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFSnap, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master = { "Master", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFLogicalChannel, Master), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMaster, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_MibFade_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_MibFade = { "MibFade", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFLogicalChannel, MibFade), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_MibFade_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_MibFade_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_DMXChangeTimeLimit_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_DMXChangeTimeLimit = { "DMXChangeTimeLimit", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFLogicalChannel, DMXChangeTimeLimit), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_DMXChangeTimeLimit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_DMXChangeTimeLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_ChannelFunction_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_ChannelFunction = { "ChannelFunction", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFLogicalChannel, ChannelFunction), Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_ChannelFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_ChannelFunction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Snap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_Master,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_MibFade,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_DMXChangeTimeLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::NewProp_ChannelFunction,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFLogicalChannel",
		sizeof(FDMXImportGDTFLogicalChannel),
		alignof(FDMXImportGDTFLogicalChannel),
		Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFLogicalChannel"), sizeof(FDMXImportGDTFLogicalChannel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFLogicalChannel_Hash() { return 88339255U; }
class UScriptStruct* FDMXImportGDTFChannelFunction::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFChannelFunction"), sizeof(FDMXImportGDTFChannelFunction), Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFChannelFunction>()
{
	return FDMXImportGDTFChannelFunction::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFChannelFunction(FDMXImportGDTFChannelFunction::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFChannelFunction"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFChannelFunction
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFChannelFunction()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFChannelFunction>(FName(TEXT("DMXImportGDTFChannelFunction")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFChannelFunction;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attribute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OriginalAttribute;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXFrom_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXFrom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalFrom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PhysicalFrom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PhysicalTo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RealFade_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RealFade;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Wheel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Wheel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Emitter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Emitter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Filter;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DMXInvert_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXInvert_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DMXInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModeMaster_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ModeMaster;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModeFrom_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModeFrom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModeTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModeTo;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChannelSets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelSets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ChannelSets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFChannelFunction>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Attribute_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Attribute = { "Attribute", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, Attribute), Z_Construct_UScriptStruct_FDMXImportGDTFAttribute, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Attribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Attribute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_OriginalAttribute_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_OriginalAttribute = { "OriginalAttribute", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, OriginalAttribute), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_OriginalAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_OriginalAttribute_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXFrom_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXFrom = { "DMXFrom", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, DMXFrom), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXFrom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXFrom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXValue_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXValue = { "DMXValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, DMXValue), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalFrom_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalFrom = { "PhysicalFrom", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, PhysicalFrom), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalFrom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalFrom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalTo_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalTo = { "PhysicalTo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, PhysicalTo), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalTo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_RealFade_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_RealFade = { "RealFade", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, RealFade), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_RealFade_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_RealFade_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Wheel_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Wheel = { "Wheel", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, Wheel), Z_Construct_UScriptStruct_FDMXImportGDTFWheel, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Wheel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Wheel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Emitter_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Emitter = { "Emitter", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, Emitter), Z_Construct_UScriptStruct_FDMXImportGDTFEmitter, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Emitter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Emitter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Filter_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, Filter), Z_Construct_UScriptStruct_FDMXImportGDTFFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Filter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Filter_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert = { "DMXInvert", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, DMXInvert), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFDMXInvert, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeMaster_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeMaster = { "ModeMaster", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, ModeMaster), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeMaster_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeMaster_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeFrom_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeFrom = { "ModeFrom", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, ModeFrom), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeFrom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeFrom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeTo_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeTo = { "ModeTo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, ModeTo), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeTo_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets_Inner = { "ChannelSets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets = { "ChannelSets", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelFunction, ChannelSets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Attribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_OriginalAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXFrom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalFrom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_PhysicalTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_RealFade,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Wheel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Emitter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_Filter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_DMXInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeMaster,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeFrom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ModeTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::NewProp_ChannelSets,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFChannelFunction",
		sizeof(FDMXImportGDTFChannelFunction),
		alignof(FDMXImportGDTFChannelFunction),
		Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFChannelFunction"), sizeof(FDMXImportGDTFChannelFunction), Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction_Hash() { return 2042594977U; }
class UScriptStruct* FDMXImportGDTFChannelSet::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFChannelSet"), sizeof(FDMXImportGDTFChannelSet), Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFChannelSet>()
{
	return FDMXImportGDTFChannelSet::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFChannelSet(FDMXImportGDTFChannelSet::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFChannelSet"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFChannelSet
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFChannelSet()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFChannelSet>(FName(TEXT("DMXImportGDTFChannelSet")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFChannelSet;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXFrom_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXFrom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalFrom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PhysicalFrom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PhysicalTo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WheelSlotIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_WheelSlotIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFChannelSet>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelSet, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_DMXFrom_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_DMXFrom = { "DMXFrom", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelSet, DMXFrom), Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_DMXFrom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_DMXFrom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalFrom_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalFrom = { "PhysicalFrom", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelSet, PhysicalFrom), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalFrom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalFrom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalTo_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalTo = { "PhysicalTo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelSet, PhysicalTo), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalTo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_WheelSlotIndex_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_WheelSlotIndex = { "WheelSlotIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFChannelSet, WheelSlotIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_WheelSlotIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_WheelSlotIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_DMXFrom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalFrom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_PhysicalTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::NewProp_WheelSlotIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFChannelSet",
		sizeof(FDMXImportGDTFChannelSet),
		alignof(FDMXImportGDTFChannelSet),
		Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFChannelSet"), sizeof(FDMXImportGDTFChannelSet), Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFChannelSet_Hash() { return 1339133097U; }
class UScriptStruct* FDMXImportGDTFDMXValue::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFDMXValue"), sizeof(FDMXImportGDTFDMXValue), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFDMXValue>()
{
	return FDMXImportGDTFDMXValue::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFDMXValue(FDMXImportGDTFDMXValue::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFDMXValue"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXValue
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXValue()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFDMXValue>(FName(TEXT("DMXImportGDTFDMXValue")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXValue;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ValueSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFDMXValue>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXValue, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_ValueSize_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_ValueSize = { "ValueSize", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFDMXValue, ValueSize), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_ValueSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_ValueSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::NewProp_ValueSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFDMXValue",
		sizeof(FDMXImportGDTFDMXValue),
		alignof(FDMXImportGDTFDMXValue),
		Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFDMXValue"), sizeof(FDMXImportGDTFDMXValue), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXValue_Hash() { return 1982199028U; }

static_assert(std::is_polymorphic<FDMXImportGDTFGeneralGeometry>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFGeneralGeometry cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFGeneralGeometry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFGeneralGeometry"), sizeof(FDMXImportGDTFGeneralGeometry), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFGeneralGeometry>()
{
	return FDMXImportGDTFGeneralGeometry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFGeneralGeometry(FDMXImportGDTFGeneralGeometry::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFGeneralGeometry"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeneralGeometry
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeneralGeometry()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFGeneralGeometry>(FName(TEXT("DMXImportGDTFGeneralGeometry")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeneralGeometry;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Geometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Geometry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterBeam_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterBeam;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterGobo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterGobo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterShaper_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilterShaper;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeometryReference_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeometryReference;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFGeneralGeometry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Axis_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, Axis), Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Axis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Geometry_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Geometry = { "Geometry", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, Geometry), Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Geometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Geometry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterBeam_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterBeam = { "FilterBeam", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, FilterBeam), Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterBeam_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterBeam_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterColor_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterColor = { "FilterColor", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, FilterColor), Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterGobo_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterGobo = { "FilterGobo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, FilterGobo), Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterGobo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterGobo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterShaper_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterShaper = { "FilterShaper", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, FilterShaper), Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterShaper_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterShaper_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_GeometryReference_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_GeometryReference = { "GeometryReference", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralGeometry, GeometryReference), Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_GeometryReference_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_GeometryReference_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Axis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_Geometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterBeam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterGobo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_FilterShaper,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::NewProp_GeometryReference,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFGeneralGeometry",
		sizeof(FDMXImportGDTFGeneralGeometry),
		alignof(FDMXImportGDTFGeneralGeometry),
		Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFGeneralGeometry"), sizeof(FDMXImportGDTFGeneralGeometry), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry_Hash() { return 4202678227U; }

static_assert(std::is_polymorphic<FDMXImportGDTFGeometryReference>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFGeometryReference cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFGeometryReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFGeometryReference"), sizeof(FDMXImportGDTFGeometryReference), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFGeometryReference>()
{
	return FDMXImportGDTFGeometryReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFGeometryReference(FDMXImportGDTFGeometryReference::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFGeometryReference"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeometryReference
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeometryReference()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFGeometryReference>(FName(TEXT("DMXImportGDTFGeometryReference")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeometryReference;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Breaks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Breaks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Breaks;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFGeometryReference>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks_Inner = { "Breaks", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFBreak, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks = { "Breaks", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeometryReference, Breaks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::NewProp_Breaks,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFGeometryReference",
		sizeof(FDMXImportGDTFGeometryReference),
		alignof(FDMXImportGDTFGeometryReference),
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFGeometryReference"), sizeof(FDMXImportGDTFGeometryReference), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryReference_Hash() { return 2636953466U; }
class UScriptStruct* FDMXImportGDTFBreak::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFBreak, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFBreak"), sizeof(FDMXImportGDTFBreak), Get_Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFBreak>()
{
	return FDMXImportGDTFBreak::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFBreak(FDMXImportGDTFBreak::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFBreak"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFBreak
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFBreak()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFBreak>(FName(TEXT("DMXImportGDTFBreak")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFBreak;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DMXOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXBreak_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DMXBreak;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFBreak>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXOffset_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXOffset = { "DMXOffset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBreak, DMXOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXBreak_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXBreak = { "DMXBreak", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBreak, DMXBreak), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXBreak_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXBreak_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::NewProp_DMXBreak,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFBreak",
		sizeof(FDMXImportGDTFBreak),
		alignof(FDMXImportGDTFBreak),
		Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFBreak()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFBreak"), sizeof(FDMXImportGDTFBreak), Get_Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFBreak_Hash() { return 1451352982U; }

static_assert(std::is_polymorphic<FDMXImportGDTFFilterShaper>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFFilterShaper cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFFilterShaper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFilterShaper"), sizeof(FDMXImportGDTFFilterShaper), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFilterShaper>()
{
	return FDMXImportGDTFFilterShaper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFilterShaper(FDMXImportGDTFFilterShaper::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFilterShaper"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterShaper
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterShaper()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFilterShaper>(FName(TEXT("DMXImportGDTFFilterShaper")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterShaper;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFilterShaper>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFFilterShaper",
		sizeof(FDMXImportGDTFFilterShaper),
		alignof(FDMXImportGDTFFilterShaper),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFilterShaper"), sizeof(FDMXImportGDTFFilterShaper), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterShaper_Hash() { return 1139088559U; }

static_assert(std::is_polymorphic<FDMXImportGDTFFilterGobo>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFFilterGobo cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFFilterGobo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFilterGobo"), sizeof(FDMXImportGDTFFilterGobo), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFilterGobo>()
{
	return FDMXImportGDTFFilterGobo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFilterGobo(FDMXImportGDTFFilterGobo::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFilterGobo"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterGobo
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterGobo()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFilterGobo>(FName(TEXT("DMXImportGDTFFilterGobo")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterGobo;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFilterGobo>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFFilterGobo",
		sizeof(FDMXImportGDTFFilterGobo),
		alignof(FDMXImportGDTFFilterGobo),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFilterGobo"), sizeof(FDMXImportGDTFFilterGobo), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterGobo_Hash() { return 1190961085U; }

static_assert(std::is_polymorphic<FDMXImportGDTFFilterColor>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFFilterColor cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFFilterColor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFilterColor"), sizeof(FDMXImportGDTFFilterColor), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFilterColor>()
{
	return FDMXImportGDTFFilterColor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFilterColor(FDMXImportGDTFFilterColor::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFilterColor"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterColor
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterColor()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFilterColor>(FName(TEXT("DMXImportGDTFFilterColor")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterColor;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFilterColor>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFFilterColor",
		sizeof(FDMXImportGDTFFilterColor),
		alignof(FDMXImportGDTFFilterColor),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFilterColor"), sizeof(FDMXImportGDTFFilterColor), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterColor_Hash() { return 4042308155U; }

static_assert(std::is_polymorphic<FDMXImportGDTFFilterBeam>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFFilterBeam cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFFilterBeam::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFilterBeam"), sizeof(FDMXImportGDTFFilterBeam), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFilterBeam>()
{
	return FDMXImportGDTFFilterBeam::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFilterBeam(FDMXImportGDTFFilterBeam::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFilterBeam"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterBeam
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterBeam()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFilterBeam>(FName(TEXT("DMXImportGDTFFilterBeam")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilterBeam;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFilterBeam>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFFilterBeam",
		sizeof(FDMXImportGDTFFilterBeam),
		alignof(FDMXImportGDTFFilterBeam),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFilterBeam"), sizeof(FDMXImportGDTFFilterBeam), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilterBeam_Hash() { return 3046420781U; }

static_assert(std::is_polymorphic<FDMXImportGDTFTypeGeometry>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFTypeGeometry cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFTypeGeometry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFTypeGeometry"), sizeof(FDMXImportGDTFTypeGeometry), Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFTypeGeometry>()
{
	return FDMXImportGDTFTypeGeometry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFTypeGeometry(FDMXImportGDTFTypeGeometry::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFTypeGeometry"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFTypeGeometry
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFTypeGeometry()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFTypeGeometry>(FName(TEXT("DMXImportGDTFTypeGeometry")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFTypeGeometry;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFTypeGeometry>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFTypeGeometry",
		sizeof(FDMXImportGDTFTypeGeometry),
		alignof(FDMXImportGDTFTypeGeometry),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFTypeGeometry"), sizeof(FDMXImportGDTFTypeGeometry), Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeGeometry_Hash() { return 91259235U; }

static_assert(std::is_polymorphic<FDMXImportGDTFGeneralAxis>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFGeneralAxis cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFGeneralAxis::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFGeneralAxis"), sizeof(FDMXImportGDTFGeneralAxis), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFGeneralAxis>()
{
	return FDMXImportGDTFGeneralAxis::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFGeneralAxis(FDMXImportGDTFGeneralAxis::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFGeneralAxis"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeneralAxis
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeneralAxis()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFGeneralAxis>(FName(TEXT("DMXImportGDTFGeneralAxis")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeneralAxis;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Axis_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Axis_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Axis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFGeneralAxis>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis_Inner = { "Axis", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis = { "Axis", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeneralAxis, Axis), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::NewProp_Axis,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFGeneralAxis",
		sizeof(FDMXImportGDTFGeneralAxis),
		alignof(FDMXImportGDTFGeneralAxis),
		Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFGeneralAxis"), sizeof(FDMXImportGDTFGeneralAxis), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeneralAxis_Hash() { return 3895959580U; }

static_assert(std::is_polymorphic<FDMXImportGDTFTypeAxis>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFTypeAxis cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFTypeAxis::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFTypeAxis"), sizeof(FDMXImportGDTFTypeAxis), Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFTypeAxis>()
{
	return FDMXImportGDTFTypeAxis::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFTypeAxis(FDMXImportGDTFTypeAxis::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFTypeAxis"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFTypeAxis
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFTypeAxis()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFTypeAxis>(FName(TEXT("DMXImportGDTFTypeAxis")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFTypeAxis;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Beams_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Beams_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Beams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFTypeAxis>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams_Inner = { "Beams", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFBeam, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams = { "Beams", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFTypeAxis, Beams), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::NewProp_Beams,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFTypeAxis",
		sizeof(FDMXImportGDTFTypeAxis),
		alignof(FDMXImportGDTFTypeAxis),
		Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFTypeAxis"), sizeof(FDMXImportGDTFTypeAxis), Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFTypeAxis_Hash() { return 3182832518U; }

static_assert(std::is_polymorphic<FDMXImportGDTFBeam>() == std::is_polymorphic<FDMXImportGDTFGeometryBase>(), "USTRUCT FDMXImportGDTFBeam cannot be polymorphic unless super FDMXImportGDTFGeometryBase is polymorphic");

class UScriptStruct* FDMXImportGDTFBeam::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFBeam, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFBeam"), sizeof(FDMXImportGDTFBeam), Get_Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFBeam>()
{
	return FDMXImportGDTFBeam::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFBeam(FDMXImportGDTFBeam::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFBeam"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFBeam
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFBeam()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFBeam>(FName(TEXT("DMXImportGDTFBeam")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFBeam;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LampType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LampType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LampType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PowerConsumption_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PowerConsumption;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LuminousFlux_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LuminousFlux;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorTemperature_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ColorTemperature;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeamAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BeamAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FieldAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeamRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BeamRadius;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BeamType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BeamType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BeamType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorRenderingIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorRenderingIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFBeam>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType = { "LampType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, LampType), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFLampType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_PowerConsumption_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_PowerConsumption = { "PowerConsumption", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, PowerConsumption), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_PowerConsumption_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_PowerConsumption_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LuminousFlux_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LuminousFlux = { "LuminousFlux", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, LuminousFlux), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LuminousFlux_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LuminousFlux_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorTemperature_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorTemperature = { "ColorTemperature", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, ColorTemperature), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorTemperature_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorTemperature_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamAngle_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamAngle = { "BeamAngle", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, BeamAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_FieldAngle_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_FieldAngle = { "FieldAngle", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, FieldAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_FieldAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_FieldAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamRadius_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamRadius = { "BeamRadius", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, BeamRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamRadius_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType = { "BeamType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, BeamType), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFBeamType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorRenderingIndex_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorRenderingIndex = { "ColorRenderingIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFBeam, ColorRenderingIndex), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorRenderingIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorRenderingIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LampType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_PowerConsumption,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_LuminousFlux,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorTemperature,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_FieldAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_BeamType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::NewProp_ColorRenderingIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase,
		&NewStructOps,
		"DMXImportGDTFBeam",
		sizeof(FDMXImportGDTFBeam),
		alignof(FDMXImportGDTFBeam),
		Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFBeam()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFBeam"), sizeof(FDMXImportGDTFBeam), Get_Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFBeam_Hash() { return 2438812536U; }
class UScriptStruct* FDMXImportGDTFGeometryBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFGeometryBase"), sizeof(FDMXImportGDTFGeometryBase), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFGeometryBase>()
{
	return FDMXImportGDTFGeometryBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFGeometryBase(FDMXImportGDTFGeometryBase::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFGeometryBase"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeometryBase
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeometryBase()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFGeometryBase>(FName(TEXT("DMXImportGDTFGeometryBase")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFGeometryBase;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Model_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Model;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFGeometryBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeometryBase, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Model_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Model = { "Model", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeometryBase, Model), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Model_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Model_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFGeometryBase, Position), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Position_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Model,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::NewProp_Position,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFGeometryBase",
		sizeof(FDMXImportGDTFGeometryBase),
		alignof(FDMXImportGDTFGeometryBase),
		Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFGeometryBase"), sizeof(FDMXImportGDTFGeometryBase), Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFGeometryBase_Hash() { return 332345864U; }
class UScriptStruct* FDMXImportGDTFModel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFModel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFModel, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFModel"), sizeof(FDMXImportGDTFModel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFModel_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFModel>()
{
	return FDMXImportGDTFModel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFModel(FDMXImportGDTFModel::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFModel"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFModel
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFModel()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFModel>(FName(TEXT("DMXImportGDTFModel")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFModel;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Length_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Length;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PrimitiveType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimitiveType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PrimitiveType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFModel>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFModel, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Length_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Length = { "Length", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFModel, Length), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Length_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Length_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFModel, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFModel, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Height_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType = { "PrimitiveType", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFModel, PrimitiveType), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPrimitiveType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Length,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::NewProp_PrimitiveType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFModel",
		sizeof(FDMXImportGDTFModel),
		alignof(FDMXImportGDTFModel),
		Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFModel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFModel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFModel"), sizeof(FDMXImportGDTFModel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFModel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFModel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFModel_Hash() { return 1628978196U; }
class UScriptStruct* FDMXImportGDTFCRIs::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFCRIs, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFCRIs"), sizeof(FDMXImportGDTFCRIs), Get_Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFCRIs>()
{
	return FDMXImportGDTFCRIs::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFCRIs(FDMXImportGDTFCRIs::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFCRIs"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFCRIs
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFCRIs()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFCRIs>(FName(TEXT("DMXImportGDTFCRIs")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFCRIs;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFCRIs>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFCRIs",
		sizeof(FDMXImportGDTFCRIs),
		alignof(FDMXImportGDTFCRIs),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFCRIs()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFCRIs"), sizeof(FDMXImportGDTFCRIs), Get_Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFCRIs_Hash() { return 3412339370U; }
class UScriptStruct* FDMXImportGDTFDMXProfiles::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFDMXProfiles"), sizeof(FDMXImportGDTFDMXProfiles), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFDMXProfiles>()
{
	return FDMXImportGDTFDMXProfiles::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFDMXProfiles(FDMXImportGDTFDMXProfiles::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFDMXProfiles"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXProfiles
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXProfiles()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFDMXProfiles>(FName(TEXT("DMXImportGDTFDMXProfiles")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFDMXProfiles;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFDMXProfiles>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFDMXProfiles",
		sizeof(FDMXImportGDTFDMXProfiles),
		alignof(FDMXImportGDTFDMXProfiles),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFDMXProfiles"), sizeof(FDMXImportGDTFDMXProfiles), Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles_Hash() { return 829012589U; }
class UScriptStruct* FDMXImportGDTFColorSpace::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFColorSpace"), sizeof(FDMXImportGDTFColorSpace), Get_Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFColorSpace>()
{
	return FDMXImportGDTFColorSpace::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFColorSpace(FDMXImportGDTFColorSpace::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFColorSpace"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFColorSpace
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFColorSpace()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFColorSpace>(FName(TEXT("DMXImportGDTFColorSpace")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFColorSpace;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Red_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Red;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Green_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Green;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Blue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhitePoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WhitePoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFColorSpace>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFColorSpace, Mode), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFColorSpace, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Red_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Red = { "Red", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFColorSpace, Red), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Red_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Red_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Green_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Green = { "Green", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFColorSpace, Green), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Green_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Green_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Blue_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Blue = { "Blue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFColorSpace, Blue), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Blue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Blue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_WhitePoint_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_WhitePoint = { "WhitePoint", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFColorSpace, WhitePoint), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_WhitePoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_WhitePoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Mode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Red,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Green,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_Blue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::NewProp_WhitePoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFColorSpace",
		sizeof(FDMXImportGDTFColorSpace),
		alignof(FDMXImportGDTFColorSpace),
		Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFColorSpace"), sizeof(FDMXImportGDTFColorSpace), Get_Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace_Hash() { return 1466953280U; }
class UScriptStruct* FDMXImportGDTFEmitter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFEmitter"), sizeof(FDMXImportGDTFEmitter), Get_Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFEmitter>()
{
	return FDMXImportGDTFEmitter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFEmitter(FDMXImportGDTFEmitter::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFEmitter"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFEmitter
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFEmitter()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFEmitter>(FName(TEXT("DMXImportGDTFEmitter")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFEmitter;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DominantWaveLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DominantWaveLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DiodePart_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DiodePart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Measurement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Measurement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFEmitter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFEmitter, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFEmitter, Color), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DominantWaveLength_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DominantWaveLength = { "DominantWaveLength", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFEmitter, DominantWaveLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DominantWaveLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DominantWaveLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DiodePart_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DiodePart = { "DiodePart", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFEmitter, DiodePart), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DiodePart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DiodePart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Measurement_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Measurement = { "Measurement", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFEmitter, Measurement), Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Measurement_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Measurement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DominantWaveLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_DiodePart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::NewProp_Measurement,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFEmitter",
		sizeof(FDMXImportGDTFEmitter),
		alignof(FDMXImportGDTFEmitter),
		Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFEmitter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFEmitter"), sizeof(FDMXImportGDTFEmitter), Get_Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFEmitter_Hash() { return 324209402U; }
class UScriptStruct* FDMXImportGDTFMeasurement::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFMeasurement"), sizeof(FDMXImportGDTFMeasurement), Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFMeasurement>()
{
	return FDMXImportGDTFMeasurement::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFMeasurement(FDMXImportGDTFMeasurement::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFMeasurement"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFMeasurement
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFMeasurement()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFMeasurement>(FName(TEXT("DMXImportGDTFMeasurement")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFMeasurement;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Physical_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Physical;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LuminousIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LuminousIntensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transmission_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Transmission;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InterpolationTo_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpolationTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InterpolationTo;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeasurementPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeasurementPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeasurementPoints;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFMeasurement>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Physical_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Physical = { "Physical", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurement, Physical), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Physical_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Physical_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_LuminousIntensity_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_LuminousIntensity = { "LuminousIntensity", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurement, LuminousIntensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_LuminousIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_LuminousIntensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Transmission_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Transmission = { "Transmission", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurement, Transmission), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Transmission_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Transmission_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo = { "InterpolationTo", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurement, InterpolationTo), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFInterpolationTo, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints_Inner = { "MeasurementPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints = { "MeasurementPoints", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurement, MeasurementPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Physical,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_LuminousIntensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_Transmission,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_InterpolationTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::NewProp_MeasurementPoints,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFMeasurement",
		sizeof(FDMXImportGDTFMeasurement),
		alignof(FDMXImportGDTFMeasurement),
		Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFMeasurement"), sizeof(FDMXImportGDTFMeasurement), Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurement_Hash() { return 2620896160U; }
class UScriptStruct* FDMXImportGDTFMeasurementPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFMeasurementPoint"), sizeof(FDMXImportGDTFMeasurementPoint), Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFMeasurementPoint>()
{
	return FDMXImportGDTFMeasurementPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFMeasurementPoint(FDMXImportGDTFMeasurementPoint::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFMeasurementPoint"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFMeasurementPoint
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFMeasurementPoint()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFMeasurementPoint>(FName(TEXT("DMXImportGDTFMeasurementPoint")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFMeasurementPoint;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaveLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Energy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Energy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFMeasurementPoint>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_WaveLength_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_WaveLength = { "WaveLength", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurementPoint, WaveLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_WaveLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_WaveLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_Energy_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_Energy = { "Energy", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFMeasurementPoint, Energy), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_Energy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_Energy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_WaveLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::NewProp_Energy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFMeasurementPoint",
		sizeof(FDMXImportGDTFMeasurementPoint),
		alignof(FDMXImportGDTFMeasurementPoint),
		Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFMeasurementPoint"), sizeof(FDMXImportGDTFMeasurementPoint), Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFMeasurementPoint_Hash() { return 1780390005U; }
class UScriptStruct* FDMXImportGDTFWheel::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFWheel, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFWheel"), sizeof(FDMXImportGDTFWheel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFWheel>()
{
	return FDMXImportGDTFWheel::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFWheel(FDMXImportGDTFWheel::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFWheel"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFWheel
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFWheel()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFWheel>(FName(TEXT("DMXImportGDTFWheel")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFWheel;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Slots_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Slots_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Slots;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFWheel>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFWheel, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots_Inner = { "Slots", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots = { "Slots", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFWheel, Slots), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::NewProp_Slots,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFWheel",
		sizeof(FDMXImportGDTFWheel),
		alignof(FDMXImportGDTFWheel),
		Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFWheel()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFWheel"), sizeof(FDMXImportGDTFWheel), Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheel_Hash() { return 2056469374U; }
class UScriptStruct* FDMXImportGDTFWheelSlot::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFWheelSlot"), sizeof(FDMXImportGDTFWheelSlot), Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFWheelSlot>()
{
	return FDMXImportGDTFWheelSlot::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFWheelSlot(FDMXImportGDTFWheelSlot::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFWheelSlot"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFWheelSlot
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFWheelSlot()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFWheelSlot>(FName(TEXT("DMXImportGDTFWheelSlot")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFWheelSlot;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Filter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaFileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFWheelSlot>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFWheelSlot, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFWheelSlot, Color), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Filter_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Filter = { "Filter", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFWheelSlot, Filter), Z_Construct_UScriptStruct_FDMXImportGDTFFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Filter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Filter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_MediaFileName_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_MediaFileName = { "MediaFileName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFWheelSlot, MediaFileName), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_MediaFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_MediaFileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_Filter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::NewProp_MediaFileName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFWheelSlot",
		sizeof(FDMXImportGDTFWheelSlot),
		alignof(FDMXImportGDTFWheelSlot),
		Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFWheelSlot"), sizeof(FDMXImportGDTFWheelSlot), Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFWheelSlot_Hash() { return 3034722477U; }
class UScriptStruct* FDMXImportGDTFFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFilter, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFilter"), sizeof(FDMXImportGDTFFilter), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFilter>()
{
	return FDMXImportGDTFFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFilter(FDMXImportGDTFFilter::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFilter"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilter
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilter()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFilter>(FName(TEXT("DMXImportGDTFFilter")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFilter;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFilter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFilter, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFilter, Color), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFFilter",
		sizeof(FDMXImportGDTFFilter),
		alignof(FDMXImportGDTFFilter),
		Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFilter"), sizeof(FDMXImportGDTFFilter), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFilter_Hash() { return 3714770401U; }
class UScriptStruct* FDMXImportGDTFAttribute::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFAttribute"), sizeof(FDMXImportGDTFAttribute), Get_Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFAttribute>()
{
	return FDMXImportGDTFAttribute::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFAttribute(FDMXImportGDTFAttribute::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFAttribute"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFAttribute
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFAttribute()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFAttribute>(FName(TEXT("DMXImportGDTFAttribute")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFAttribute;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pretty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Pretty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivationGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActivationGroup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Feature_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Feature;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MainAttribute_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MainAttribute;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PhysicalUnit_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PhysicalUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFAttribute>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Pretty_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Pretty = { "Pretty", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, Pretty), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Pretty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Pretty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_ActivationGroup_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_ActivationGroup = { "ActivationGroup", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, ActivationGroup), Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_ActivationGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_ActivationGroup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Feature_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Feature = { "Feature", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, Feature), Z_Construct_UScriptStruct_FDMXImportGDTFFeature, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Feature_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Feature_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_MainAttribute_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_MainAttribute = { "MainAttribute", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, MainAttribute), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_MainAttribute_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_MainAttribute_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit = { "PhysicalUnit", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, PhysicalUnit), Z_Construct_UEnum_DMXRuntime_EDMXImportGDTFPhysicalUnit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFAttribute, Color), Z_Construct_UScriptStruct_FDMXColorCIE, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Pretty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_ActivationGroup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Feature,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_MainAttribute,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_PhysicalUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFAttribute",
		sizeof(FDMXImportGDTFAttribute),
		alignof(FDMXImportGDTFAttribute),
		Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFAttribute()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFAttribute"), sizeof(FDMXImportGDTFAttribute), Get_Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFAttribute_Hash() { return 1101357024U; }
class UScriptStruct* FDMXImportGDTFFeatureGroup::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFeatureGroup"), sizeof(FDMXImportGDTFFeatureGroup), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFeatureGroup>()
{
	return FDMXImportGDTFFeatureGroup::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFeatureGroup(FDMXImportGDTFFeatureGroup::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFeatureGroup"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFeatureGroup
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFeatureGroup()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFeatureGroup>(FName(TEXT("DMXImportGDTFFeatureGroup")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFeatureGroup;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pretty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Pretty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Features_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Features_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Features;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFeatureGroup>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFeatureGroup, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Pretty_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Pretty = { "Pretty", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFeatureGroup, Pretty), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Pretty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Pretty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features_Inner = { "Features", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFFeature, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features = { "Features", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFeatureGroup, Features), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Pretty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::NewProp_Features,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFFeatureGroup",
		sizeof(FDMXImportGDTFFeatureGroup),
		alignof(FDMXImportGDTFFeatureGroup),
		Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFeatureGroup"), sizeof(FDMXImportGDTFFeatureGroup), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup_Hash() { return 348487489U; }
class UScriptStruct* FDMXImportGDTFFeature::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFFeature, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFFeature"), sizeof(FDMXImportGDTFFeature), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFFeature>()
{
	return FDMXImportGDTFFeature::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFFeature(FDMXImportGDTFFeature::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFFeature"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFeature
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFeature()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFFeature>(FName(TEXT("DMXImportGDTFFeature")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFFeature;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFFeature>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFFeature, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFFeature",
		sizeof(FDMXImportGDTFFeature),
		alignof(FDMXImportGDTFFeature),
		Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFFeature()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFFeature"), sizeof(FDMXImportGDTFFeature), Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFFeature_Hash() { return 3733867508U; }
class UScriptStruct* FDMXImportGDTFActivationGroup::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("DMXImportGDTFActivationGroup"), sizeof(FDMXImportGDTFActivationGroup), Get_Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FDMXImportGDTFActivationGroup>()
{
	return FDMXImportGDTFActivationGroup::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXImportGDTFActivationGroup(FDMXImportGDTFActivationGroup::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("DMXImportGDTFActivationGroup"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFActivationGroup
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFActivationGroup()
	{
		UScriptStruct::DeferCppStructOps<FDMXImportGDTFActivationGroup>(FName(TEXT("DMXImportGDTFActivationGroup")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFDMXImportGDTFActivationGroup;
	struct Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXImportGDTFActivationGroup>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXImportGDTFActivationGroup, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		nullptr,
		&NewStructOps,
		"DMXImportGDTFActivationGroup",
		sizeof(FDMXImportGDTFActivationGroup),
		alignof(FDMXImportGDTFActivationGroup),
		Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXImportGDTFActivationGroup"), sizeof(FDMXImportGDTFActivationGroup), Get_Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup_Hash() { return 1597227618U; }
	void UDMXImportGDTFFixtureType::StaticRegisterNativesUDMXImportGDTFFixtureType()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFFixtureType_NoRegister()
	{
		return UDMXImportGDTFFixtureType::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShortName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShortName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LongName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LongName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Manufacturer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Manufacturer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FixtureTypeID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FixtureTypeID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Thumbnail_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Thumbnail;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RefFT_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RefFT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportFixtureType,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, Name), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_ShortName_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_ShortName = { "ShortName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, ShortName), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_ShortName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_ShortName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_LongName_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_LongName = { "LongName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, LongName), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_LongName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_LongName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Manufacturer_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Manufacturer = { "Manufacturer", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, Manufacturer), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Manufacturer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Manufacturer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, Description), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_FixtureTypeID_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_FixtureTypeID = { "FixtureTypeID", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, FixtureTypeID), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_FixtureTypeID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_FixtureTypeID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Thumbnail_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Thumbnail = { "Thumbnail", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, Thumbnail), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Thumbnail_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Thumbnail_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_RefFT_MetaData[] = {
		{ "Category", "Fixture Type" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_RefFT = { "RefFT", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFFixtureType, RefFT), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_RefFT_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_RefFT_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_ShortName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_LongName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Manufacturer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_FixtureTypeID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_Thumbnail,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::NewProp_RefFT,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFFixtureType>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::ClassParams = {
		&UDMXImportGDTFFixtureType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFFixtureType()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFFixtureType_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFFixtureType, 320440146);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFFixtureType>()
	{
		return UDMXImportGDTFFixtureType::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFFixtureType(Z_Construct_UClass_UDMXImportGDTFFixtureType, &UDMXImportGDTFFixtureType::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFFixtureType"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFFixtureType);
	void UDMXImportGDTFAttributeDefinitions::StaticRegisterNativesUDMXImportGDTFAttributeDefinitions()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_NoRegister()
	{
		return UDMXImportGDTFAttributeDefinitions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActivationGroups_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivationGroups_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActivationGroups;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeatureGroups_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureGroups_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeatureGroups;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Attributes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Attributes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportAttributeDefinitions,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups_Inner = { "ActivationGroups", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFActivationGroup, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups = { "ActivationGroups", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFAttributeDefinitions, ActivationGroups), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups_Inner = { "FeatureGroups", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFFeatureGroup, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups = { "FeatureGroups", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFAttributeDefinitions, FeatureGroups), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes_Inner = { "Attributes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFAttribute, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes = { "Attributes", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFAttributeDefinitions, Attributes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_ActivationGroups,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_FeatureGroups,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::NewProp_Attributes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFAttributeDefinitions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::ClassParams = {
		&UDMXImportGDTFAttributeDefinitions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFAttributeDefinitions, 1649005956);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFAttributeDefinitions>()
	{
		return UDMXImportGDTFAttributeDefinitions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFAttributeDefinitions(Z_Construct_UClass_UDMXImportGDTFAttributeDefinitions, &UDMXImportGDTFAttributeDefinitions::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFAttributeDefinitions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFAttributeDefinitions);
	void UDMXImportGDTFWheels::StaticRegisterNativesUDMXImportGDTFWheels()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFWheels_NoRegister()
	{
		return UDMXImportGDTFWheels::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFWheels_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Wheels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Wheels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Wheels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFWheels_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportWheels,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFWheels_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels_Inner = { "Wheels", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFWheel, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels = { "Wheels", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFWheels, Wheels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFWheels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFWheels_Statics::NewProp_Wheels,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFWheels_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFWheels>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFWheels_Statics::ClassParams = {
		&UDMXImportGDTFWheels::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFWheels_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFWheels_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFWheels_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFWheels_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFWheels()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFWheels_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFWheels, 1613406388);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFWheels>()
	{
		return UDMXImportGDTFWheels::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFWheels(Z_Construct_UClass_UDMXImportGDTFWheels, &UDMXImportGDTFWheels::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFWheels"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFWheels);
	void UDMXImportGDTFPhysicalDescriptions::StaticRegisterNativesUDMXImportGDTFPhysicalDescriptions()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_NoRegister()
	{
		return UDMXImportGDTFPhysicalDescriptions::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Emitters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Emitters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Emitters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXProfiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXProfiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CRIs_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CRIs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportPhysicalDescriptions,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters_Inner = { "Emitters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFEmitter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters = { "Emitters", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFPhysicalDescriptions, Emitters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_ColorSpace_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_ColorSpace = { "ColorSpace", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFPhysicalDescriptions, ColorSpace), Z_Construct_UScriptStruct_FDMXImportGDTFColorSpace, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_ColorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_ColorSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_DMXProfiles_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_DMXProfiles = { "DMXProfiles", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFPhysicalDescriptions, DMXProfiles), Z_Construct_UScriptStruct_FDMXImportGDTFDMXProfiles, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_DMXProfiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_DMXProfiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_CRIs_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_CRIs = { "CRIs", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFPhysicalDescriptions, CRIs), Z_Construct_UScriptStruct_FDMXImportGDTFCRIs, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_CRIs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_CRIs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_Emitters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_ColorSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_DMXProfiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::NewProp_CRIs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFPhysicalDescriptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::ClassParams = {
		&UDMXImportGDTFPhysicalDescriptions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFPhysicalDescriptions, 1141334268);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFPhysicalDescriptions>()
	{
		return UDMXImportGDTFPhysicalDescriptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFPhysicalDescriptions(Z_Construct_UClass_UDMXImportGDTFPhysicalDescriptions, &UDMXImportGDTFPhysicalDescriptions::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFPhysicalDescriptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFPhysicalDescriptions);
	void UDMXImportGDTFModels::StaticRegisterNativesUDMXImportGDTFModels()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFModels_NoRegister()
	{
		return UDMXImportGDTFModels::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFModels_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Models_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Models_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Models;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFModels_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportModels,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFModels_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models_Inner = { "Models", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFModel, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models = { "Models", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFModels, Models), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFModels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFModels_Statics::NewProp_Models,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFModels_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFModels>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFModels_Statics::ClassParams = {
		&UDMXImportGDTFModels::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFModels_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFModels_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFModels_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFModels_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFModels()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFModels_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFModels, 2635016445);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFModels>()
	{
		return UDMXImportGDTFModels::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFModels(Z_Construct_UClass_UDMXImportGDTFModels, &UDMXImportGDTFModels::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFModels"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFModels);
	void UDMXImportGDTFGeometries::StaticRegisterNativesUDMXImportGDTFGeometries()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFGeometries_NoRegister()
	{
		return UDMXImportGDTFGeometries::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFGeometries_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GeneralGeometry_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeneralGeometry_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_GeneralGeometry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportGeometries,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry_Inner = { "GeneralGeometry", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFGeneralGeometry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry = { "GeneralGeometry", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFGeometries, GeneralGeometry), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::NewProp_GeneralGeometry,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFGeometries>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::ClassParams = {
		&UDMXImportGDTFGeometries::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFGeometries()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFGeometries_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFGeometries, 78416138);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFGeometries>()
	{
		return UDMXImportGDTFGeometries::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFGeometries(Z_Construct_UClass_UDMXImportGDTFGeometries, &UDMXImportGDTFGeometries::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFGeometries"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFGeometries);
	DEFINE_FUNCTION(UDMXImportGDTFDMXModes::execGetDMXChannelFunctions)
	{
		P_GET_STRUCT_REF(FDMXImportGDTFDMXMode,Z_Param_Out_InMode);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FDMXImportGDTFChannelFunction>*)Z_Param__Result=P_THIS->GetDMXChannelFunctions(Z_Param_Out_InMode);
		P_NATIVE_END;
	}
	void UDMXImportGDTFDMXModes::StaticRegisterNativesUDMXImportGDTFDMXModes()
	{
		UClass* Class = UDMXImportGDTFDMXModes::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDMXChannelFunctions", &UDMXImportGDTFDMXModes::execGetDMXChannelFunctions },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics
	{
		struct DMXImportGDTFDMXModes_eventGetDMXChannelFunctions_Parms
		{
			FDMXImportGDTFDMXMode InMode;
			TArray<FDMXImportGDTFChannelFunction> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InMode;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_InMode_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_InMode = { "InMode", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXImportGDTFDMXModes_eventGetDMXChannelFunctions_Parms, InMode), Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode, METADATA_PARAMS(Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_InMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_InMode_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFChannelFunction, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXImportGDTFDMXModes_eventGetDMXChannelFunctions_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_InMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMXGDTF|Import Data" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXImportGDTFDMXModes, nullptr, "GetDMXChannelFunctions", nullptr, nullptr, sizeof(DMXImportGDTFDMXModes_eventGetDMXChannelFunctions_Parms), Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFDMXModes_NoRegister()
	{
		return UDMXImportGDTFDMXModes::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DMXModes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DMXModes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DMXModes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportDMXModes,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXImportGDTFDMXModes_GetDMXChannelFunctions, "GetDMXChannelFunctions" }, // 2254181041
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes_Inner = { "DMXModes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXImportGDTFDMXMode, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes = { "DMXModes", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFDMXModes, DMXModes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::NewProp_DMXModes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFDMXModes>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::ClassParams = {
		&UDMXImportGDTFDMXModes::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFDMXModes()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFDMXModes_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFDMXModes, 409311349);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFDMXModes>()
	{
		return UDMXImportGDTFDMXModes::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFDMXModes(Z_Construct_UClass_UDMXImportGDTFDMXModes, &UDMXImportGDTFDMXModes::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFDMXModes"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFDMXModes);
	void UDMXImportGDTFProtocols::StaticRegisterNativesUDMXImportGDTFProtocols()
	{
	}
	UClass* Z_Construct_UClass_UDMXImportGDTFProtocols_NoRegister()
	{
		return UDMXImportGDTFProtocols::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTFProtocols_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Protocols_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Protocols_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Protocols;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImportProtocols,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols_Inner = { "Protocols", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols_MetaData[] = {
		{ "Category", "DMX" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols = { "Protocols", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTFProtocols, Protocols), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::NewProp_Protocols,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTFProtocols>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::ClassParams = {
		&UDMXImportGDTFProtocols::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTFProtocols()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTFProtocols_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTFProtocols, 3224044494);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTFProtocols>()
	{
		return UDMXImportGDTFProtocols::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTFProtocols(Z_Construct_UClass_UDMXImportGDTFProtocols, &UDMXImportGDTFProtocols::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTFProtocols"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTFProtocols);
	DEFINE_FUNCTION(UDMXImportGDTF::execGetDMXModes)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDMXImportGDTFDMXModes**)Z_Param__Result=P_THIS->GetDMXModes();
		P_NATIVE_END;
	}
	void UDMXImportGDTF::StaticRegisterNativesUDMXImportGDTF()
	{
		UClass* Class = UDMXImportGDTF::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDMXModes", &UDMXImportGDTF::execGetDMXModes },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics
	{
		struct DMXImportGDTF_eventGetDMXModes_Parms
		{
			UDMXImportGDTFDMXModes* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DMXImportGDTF_eventGetDMXModes_Parms, ReturnValue), Z_Construct_UClass_UDMXImportGDTFDMXModes_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::Function_MetaDataParams[] = {
		{ "Category", "DMXGDTF|Import Data" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDMXImportGDTF, nullptr, "GetDMXModes", nullptr, nullptr, sizeof(DMXImportGDTF_eventGetDMXModes_Parms), Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDMXImportGDTF_NoRegister()
	{
		return UDMXImportGDTF::StaticClass();
	}
	struct Z_Construct_UClass_UDMXImportGDTF_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceFilename;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXImportGDTF_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXImport,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDMXImportGDTF_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDMXImportGDTF_GetDMXModes, "GetDMXModes" }, // 2656683376
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTF_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Library/DMXImportGDTF.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXImportGDTF_Statics::NewProp_SourceFilename_MetaData[] = {
		{ "Comment", "/** The filename of the dmx we were created from. This may not always exist on disk, as we may have previously loaded and cached the font data inside this asset. */" },
		{ "ModuleRelativePath", "Public/Library/DMXImportGDTF.h" },
		{ "ToolTip", "The filename of the dmx we were created from. This may not always exist on disk, as we may have previously loaded and cached the font data inside this asset." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXImportGDTF_Statics::NewProp_SourceFilename = { "SourceFilename", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXImportGDTF, SourceFilename), METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTF_Statics::NewProp_SourceFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTF_Statics::NewProp_SourceFilename_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXImportGDTF_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXImportGDTF_Statics::NewProp_SourceFilename,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXImportGDTF_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXImportGDTF>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXImportGDTF_Statics::ClassParams = {
		&UDMXImportGDTF::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDMXImportGDTF_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTF_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXImportGDTF_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXImportGDTF_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXImportGDTF()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXImportGDTF_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXImportGDTF, 2751623592);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXImportGDTF>()
	{
		return UDMXImportGDTF::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXImportGDTF(Z_Construct_UClass_UDMXImportGDTF, &UDMXImportGDTF::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXImportGDTF"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXImportGDTF);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
