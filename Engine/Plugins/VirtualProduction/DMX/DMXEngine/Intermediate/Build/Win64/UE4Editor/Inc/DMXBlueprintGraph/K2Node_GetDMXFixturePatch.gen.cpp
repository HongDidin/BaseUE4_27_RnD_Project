// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXBlueprintGraph/Public/K2Node_GetDMXFixturePatch.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_GetDMXFixturePatch() {}
// Cross Module References
	DMXBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_GetDMXFixturePatch_NoRegister();
	DMXBLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_GetDMXFixturePatch();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_DMXBlueprintGraph();
// End Cross Module References
	void UK2Node_GetDMXFixturePatch::StaticRegisterNativesUK2Node_GetDMXFixturePatch()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_GetDMXFixturePatch_NoRegister()
	{
		return UK2Node_GetDMXFixturePatch::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXBlueprintGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_GetDMXFixturePatch.h" },
		{ "ModuleRelativePath", "Public/K2Node_GetDMXFixturePatch.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_GetDMXFixturePatch>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::ClassParams = {
		&UK2Node_GetDMXFixturePatch::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_GetDMXFixturePatch()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_GetDMXFixturePatch_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_GetDMXFixturePatch, 480059489);
	template<> DMXBLUEPRINTGRAPH_API UClass* StaticClass<UK2Node_GetDMXFixturePatch>()
	{
		return UK2Node_GetDMXFixturePatch::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_GetDMXFixturePatch(Z_Construct_UClass_UK2Node_GetDMXFixturePatch, &UK2Node_GetDMXFixturePatch::StaticClass, TEXT("/Script/DMXBlueprintGraph"), TEXT("UK2Node_GetDMXFixturePatch"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_GetDMXFixturePatch);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UK2Node_GetDMXFixturePatch)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
