// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXEditor/Public/DMXEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXEditorSettings() {}
// Cross Module References
	DMXEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor();
	UPackage* Z_Construct_UPackage__Script_DMXEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	DMXEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor();
	DMXEDITOR_API UClass* Z_Construct_UClass_UDMXEditorSettings_NoRegister();
	DMXEDITOR_API UClass* Z_Construct_UClass_UDMXEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FDMXMonitorSourceDescriptor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor, Z_Construct_UPackage__Script_DMXEditor(), TEXT("DMXMonitorSourceDescriptor"), sizeof(FDMXMonitorSourceDescriptor), Get_Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Hash());
	}
	return Singleton;
}
template<> DMXEDITOR_API UScriptStruct* StaticStruct<FDMXMonitorSourceDescriptor>()
{
	return FDMXMonitorSourceDescriptor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXMonitorSourceDescriptor(FDMXMonitorSourceDescriptor::StaticStruct, TEXT("/Script/DMXEditor"), TEXT("DMXMonitorSourceDescriptor"), false, nullptr, nullptr);
static struct FScriptStruct_DMXEditor_StaticRegisterNativesFDMXMonitorSourceDescriptor
{
	FScriptStruct_DMXEditor_StaticRegisterNativesFDMXMonitorSourceDescriptor()
	{
		UScriptStruct::DeferCppStructOps<FDMXMonitorSourceDescriptor>(FName(TEXT("DMXMonitorSourceDescriptor")));
	}
} ScriptStruct_DMXEditor_StaticRegisterNativesFDMXMonitorSourceDescriptor;
	struct Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMonitorAllPorts_MetaData[];
#endif
		static void NewProp_bMonitorAllPorts_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMonitorAllPorts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMonitorInputPorts_MetaData[];
#endif
		static void NewProp_bMonitorInputPorts_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMonitorInputPorts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MonitoredPortGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MonitoredPortGuid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Struct to describe a monitor source, so it can be stored in settings \n * Defaults to Monitor all Inputs.\n */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "Struct to describe a monitor source, so it can be stored in settings\nDefaults to Monitor all Inputs." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXMonitorSourceDescriptor>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts_MetaData[] = {
		{ "Comment", "/** True if all ports should be monitored */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "True if all ports should be monitored" },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts_SetBit(void* Obj)
	{
		((FDMXMonitorSourceDescriptor*)Obj)->bMonitorAllPorts = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts = { "bMonitorAllPorts", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXMonitorSourceDescriptor), &Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts_MetaData[] = {
		{ "Comment", "/** True if Input Ports should be monitored. Only relevant if bMonitorAllPorts */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "True if Input Ports should be monitored. Only relevant if bMonitorAllPorts" },
	};
#endif
	void Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts_SetBit(void* Obj)
	{
		((FDMXMonitorSourceDescriptor*)Obj)->bMonitorInputPorts = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts = { "bMonitorInputPorts", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDMXMonitorSourceDescriptor), &Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_MonitoredPortGuid_MetaData[] = {
		{ "Comment", "/** The monitored Port Guid. Only relevant if !bMonitorAllPorts*/" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "The monitored Port Guid. Only relevant if !bMonitorAllPorts" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_MonitoredPortGuid = { "MonitoredPortGuid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXMonitorSourceDescriptor, MonitoredPortGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_MonitoredPortGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_MonitoredPortGuid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorAllPorts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_bMonitorInputPorts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::NewProp_MonitoredPortGuid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
		nullptr,
		&NewStructOps,
		"DMXMonitorSourceDescriptor",
		sizeof(FDMXMonitorSourceDescriptor),
		alignof(FDMXMonitorSourceDescriptor),
		Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXMonitorSourceDescriptor"), sizeof(FDMXMonitorSourceDescriptor), Get_Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor_Hash() { return 503362149U; }
class UScriptStruct* FDMXOutputConsoleFaderDescriptor::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor, Z_Construct_UPackage__Script_DMXEditor(), TEXT("DMXOutputConsoleFaderDescriptor"), sizeof(FDMXOutputConsoleFaderDescriptor), Get_Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Hash());
	}
	return Singleton;
}
template<> DMXEDITOR_API UScriptStruct* StaticStruct<FDMXOutputConsoleFaderDescriptor>()
{
	return FDMXOutputConsoleFaderDescriptor::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDMXOutputConsoleFaderDescriptor(FDMXOutputConsoleFaderDescriptor::StaticStruct, TEXT("/Script/DMXEditor"), TEXT("DMXOutputConsoleFaderDescriptor"), false, nullptr, nullptr);
static struct FScriptStruct_DMXEditor_StaticRegisterNativesFDMXOutputConsoleFaderDescriptor
{
	FScriptStruct_DMXEditor_StaticRegisterNativesFDMXOutputConsoleFaderDescriptor()
	{
		UScriptStruct::DeferCppStructOps<FDMXOutputConsoleFaderDescriptor>(FName(TEXT("DMXOutputConsoleFaderDescriptor")));
	}
} ScriptStruct_DMXEditor_StaticRegisterNativesFDMXOutputConsoleFaderDescriptor;
	struct Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FaderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FaderName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaxValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MinValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniversID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UniversID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartingAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StartingAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndingAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EndingAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProtocolName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ProtocolName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Struct to describe a single fader, so it can be stored in the config */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "Struct to describe a single fader, so it can be stored in the config" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDMXOutputConsoleFaderDescriptor>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_FaderName_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_FaderName = { "FaderName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, FaderName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_FaderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_FaderName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_Value_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, Value), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MaxValue_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MaxValue = { "MaxValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, MaxValue), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MaxValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MaxValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MinValue_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MinValue = { "MinValue", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, MinValue), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MinValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MinValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_UniversID_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_UniversID = { "UniversID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, UniversID), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_UniversID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_UniversID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_StartingAddress_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_StartingAddress = { "StartingAddress", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, StartingAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_StartingAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_StartingAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_EndingAddress_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_EndingAddress = { "EndingAddress", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, EndingAddress), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_EndingAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_EndingAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_ProtocolName_MetaData[] = {
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_ProtocolName = { "ProtocolName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDMXOutputConsoleFaderDescriptor, ProtocolName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_ProtocolName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_ProtocolName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_FaderName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MaxValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_MinValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_UniversID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_StartingAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_EndingAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::NewProp_ProtocolName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
		nullptr,
		&NewStructOps,
		"DMXOutputConsoleFaderDescriptor",
		sizeof(FDMXOutputConsoleFaderDescriptor),
		alignof(FDMXOutputConsoleFaderDescriptor),
		Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DMXOutputConsoleFaderDescriptor"), sizeof(FDMXOutputConsoleFaderDescriptor), Get_Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor_Hash() { return 1373432205U; }
	void UDMXEditorSettings::StaticRegisterNativesUDMXEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UDMXEditorSettings_NoRegister()
	{
		return UDMXEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputConsoleFaders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputConsoleFaders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputConsoleFaders;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelsMonitorUniverseID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ChannelsMonitorUniverseID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelsMonitorSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChannelsMonitorSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityMonitorSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActivityMonitorSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityMonitorMinUniverseID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActivityMonitorMinUniverseID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityMonitorMaxUniverseID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ActivityMonitorMaxUniverseID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings that holds editor configurations. Not accessible in Project Settings. TODO: Idealy rename to UDMXEditorConfiguration */" },
		{ "DisplayName", "DMXEditor" },
		{ "IncludePath", "DMXEditorSettings.h" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "Settings that holds editor configurations. Not accessible in Project Settings. TODO: Idealy rename to UDMXEditorConfiguration" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders_Inner = { "OutputConsoleFaders", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDMXOutputConsoleFaderDescriptor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders_MetaData[] = {
		{ "Comment", "/** Stores the faders specified in Output Console */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "Stores the faders specified in Output Console" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders = { "OutputConsoleFaders", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEditorSettings, OutputConsoleFaders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorUniverseID_MetaData[] = {
		{ "Comment", "/** The Universe ID to be monitored in the Channels Monitor  */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "The Universe ID to be monitored in the Channels Monitor" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorUniverseID = { "ChannelsMonitorUniverseID", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEditorSettings, ChannelsMonitorUniverseID), METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorUniverseID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorUniverseID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorSource_MetaData[] = {
		{ "Comment", "/** Source for the channels monitor */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "Source for the channels monitor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorSource = { "ChannelsMonitorSource", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEditorSettings, ChannelsMonitorSource), Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor, METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorSource_MetaData[] = {
		{ "Comment", "/** Source for the DMX Activity Monitor */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "Source for the DMX Activity Monitor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorSource = { "ActivityMonitorSource", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEditorSettings, ActivityMonitorSource), Z_Construct_UScriptStruct_FDMXMonitorSourceDescriptor, METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMinUniverseID_MetaData[] = {
		{ "Comment", "/** ID of the first universe to monitor in the DMX Activity Monitor  */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "ID of the first universe to monitor in the DMX Activity Monitor" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMinUniverseID = { "ActivityMonitorMinUniverseID", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEditorSettings, ActivityMonitorMinUniverseID), METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMinUniverseID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMinUniverseID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMaxUniverseID_MetaData[] = {
		{ "Comment", "/** ID of the last universe to monitor in the DMX Activity Monitor */" },
		{ "ModuleRelativePath", "Public/DMXEditorSettings.h" },
		{ "ToolTip", "ID of the last universe to monitor in the DMX Activity Monitor" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMaxUniverseID = { "ActivityMonitorMaxUniverseID", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEditorSettings, ActivityMonitorMaxUniverseID), METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMaxUniverseID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMaxUniverseID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_OutputConsoleFaders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorUniverseID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ChannelsMonitorSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMinUniverseID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEditorSettings_Statics::NewProp_ActivityMonitorMaxUniverseID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEditorSettings_Statics::ClassParams = {
		&UDMXEditorSettings::StaticClass,
		"DMXEditor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEditorSettings, 895806054);
	template<> DMXEDITOR_API UClass* StaticClass<UDMXEditorSettings>()
	{
		return UDMXEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEditorSettings(Z_Construct_UClass_UDMXEditorSettings, &UDMXEditorSettings::StaticClass, TEXT("/Script/DMXEditor"), TEXT("UDMXEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
