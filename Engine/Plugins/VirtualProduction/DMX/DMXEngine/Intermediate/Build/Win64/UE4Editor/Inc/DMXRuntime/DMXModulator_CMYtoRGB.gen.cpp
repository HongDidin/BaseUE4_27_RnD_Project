// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Private/Modulators/DMXModulator_CMYtoRGB.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXModulator_CMYtoRGB() {}
// Cross Module References
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_CMYtoRGB_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator_CMYtoRGB();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXModulator();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXPROTOCOL_API UScriptStruct* Z_Construct_UScriptStruct_FDMXAttributeName();
// End Cross Module References
	void UDMXModulator_CMYtoRGB::StaticRegisterNativesUDMXModulator_CMYtoRGB()
	{
	}
	UClass* Z_Construct_UClass_UDMXModulator_CMYtoRGB_NoRegister()
	{
		return UDMXModulator_CMYtoRGB::StaticClass();
	}
	struct Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeCyanToRed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeCyanToRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeMagentaToGreen_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeMagentaToGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeYellowToBlue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AttributeYellowToBlue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXModulator,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "DMX" },
		{ "Comment", "/** Converts Attributes from CMY to RGB. */" },
		{ "DisplayName", "DMX Modulator CMY to RGB" },
		{ "IncludePath", "Modulators/DMXModulator_CMYtoRGB.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_CMYtoRGB.h" },
		{ "ToolTip", "Converts Attributes from CMY to RGB." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeCyanToRed_MetaData[] = {
		{ "AdvancedDisplay", "" },
		{ "Category", "CMY to RGB" },
		{ "Comment", "/** The name of the attribute that is converted from Cyan to Red */" },
		{ "DisplayName", "Cyan to Red" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_CMYtoRGB.h" },
		{ "ToolTip", "The name of the attribute that is converted from Cyan to Red" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeCyanToRed = { "AttributeCyanToRed", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_CMYtoRGB, AttributeCyanToRed), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeCyanToRed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeCyanToRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeMagentaToGreen_MetaData[] = {
		{ "AdvancedDisplay", "" },
		{ "Category", "CMY to RGB" },
		{ "Comment", "/** The name of the attribute that is converted from Magenta to Green */" },
		{ "DisplayName", "Magenta to Green" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_CMYtoRGB.h" },
		{ "ToolTip", "The name of the attribute that is converted from Magenta to Green" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeMagentaToGreen = { "AttributeMagentaToGreen", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_CMYtoRGB, AttributeMagentaToGreen), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeMagentaToGreen_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeMagentaToGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeYellowToBlue_MetaData[] = {
		{ "AdvancedDisplay", "" },
		{ "Category", "CMY to RGB" },
		{ "Comment", "/** The name of the attribute that is converted from Yellow to Blue */" },
		{ "DisplayName", "Yellow to Blue" },
		{ "ModuleRelativePath", "Private/Modulators/DMXModulator_CMYtoRGB.h" },
		{ "ToolTip", "The name of the attribute that is converted from Yellow to Blue" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeYellowToBlue = { "AttributeYellowToBlue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXModulator_CMYtoRGB, AttributeYellowToBlue), Z_Construct_UScriptStruct_FDMXAttributeName, METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeYellowToBlue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeYellowToBlue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeCyanToRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeMagentaToGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::NewProp_AttributeYellowToBlue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXModulator_CMYtoRGB>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::ClassParams = {
		&UDMXModulator_CMYtoRGB::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXModulator_CMYtoRGB()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXModulator_CMYtoRGB_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXModulator_CMYtoRGB, 576505354);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXModulator_CMYtoRGB>()
	{
		return UDMXModulator_CMYtoRGB::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXModulator_CMYtoRGB(Z_Construct_UClass_UDMXModulator_CMYtoRGB, &UDMXModulator_CMYtoRGB::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXModulator_CMYtoRGB"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXModulator_CMYtoRGB);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
