// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Library/DMXEntity.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXEntity() {}
// Cross Module References
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity_NoRegister();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXEntity();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXObjectBase();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UDMXLibrary_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
	void UDMXEntity::StaticRegisterNativesUDMXEntity()
	{
	}
	UClass* Z_Construct_UClass_UDMXEntity_NoRegister()
	{
		return UDMXEntity::StaticClass();
	}
	struct Z_Construct_UClass_UDMXEntity_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentLibrary_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_ParentLibrary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXEntity_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDMXObjectBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntity_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**  Base class for all entity types */" },
		{ "DisplayName", "DMX Entity" },
		{ "IncludePath", "Library/DMXEntity.h" },
		{ "ModuleRelativePath", "Public/Library/DMXEntity.h" },
		{ "ToolTip", "Base class for all entity types" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntity_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Entity Properties" },
		{ "DisplayName", "Name" },
		{ "DisplayPriority", "1" },
		{ "ModuleRelativePath", "Public/Library/DMXEntity.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDMXEntity_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntity, Name), METADATA_PARAMS(Z_Construct_UClass_UDMXEntity_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntity_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntity_Statics::NewProp_ParentLibrary_MetaData[] = {
		{ "ModuleRelativePath", "Public/Library/DMXEntity.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UDMXEntity_Statics::NewProp_ParentLibrary = { "ParentLibrary", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntity, ParentLibrary), Z_Construct_UClass_UDMXLibrary_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXEntity_Statics::NewProp_ParentLibrary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntity_Statics::NewProp_ParentLibrary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXEntity_Statics::NewProp_Id_MetaData[] = {
		{ "Comment", "/** Uniquely identifies the parameter, used for fixing up Blueprints that reference this Entity when renaming. */" },
		{ "ModuleRelativePath", "Public/Library/DMXEntity.h" },
		{ "ToolTip", "Uniquely identifies the parameter, used for fixing up Blueprints that reference this Entity when renaming." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDMXEntity_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0020080000200000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXEntity, Id), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UClass_UDMXEntity_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntity_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXEntity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntity_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntity_Statics::NewProp_ParentLibrary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXEntity_Statics::NewProp_Id,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXEntity_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXEntity>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXEntity_Statics::ClassParams = {
		&UDMXEntity::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXEntity_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntity_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXEntity_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXEntity_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXEntity()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXEntity_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXEntity, 1483474656);
	template<> DMXRUNTIME_API UClass* StaticClass<UDMXEntity>()
	{
		return UDMXEntity::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXEntity(Z_Construct_UClass_UDMXEntity, &UDMXEntity::StaticClass, TEXT("/Script/DMXRuntime"), TEXT("UDMXEntity"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXEntity);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
