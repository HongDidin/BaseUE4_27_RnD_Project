// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXRUNTIME_MovieSceneDMXLibraryTemplate_generated_h
#error "MovieSceneDMXLibraryTemplate.generated.h already included, missing '#pragma once' in MovieSceneDMXLibraryTemplate.h"
#endif
#define DMXRUNTIME_MovieSceneDMXLibraryTemplate_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Sequencer_MovieSceneDMXLibraryTemplate_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Section() { return STRUCT_OFFSET(FMovieSceneDMXLibraryTemplate, Section); } \
	typedef FMovieSceneEvalTemplate Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FMovieSceneDMXLibraryTemplate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_Sequencer_MovieSceneDMXLibraryTemplate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
