// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXRuntime/Public/Sequencer/MovieSceneDMXLibraryTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneDMXLibraryTemplate() {}
// Cross Module References
	DMXRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate();
	UPackage* Z_Construct_UPackage__Script_DMXRuntime();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneEvalTemplate();
	DMXRUNTIME_API UClass* Z_Construct_UClass_UMovieSceneDMXLibrarySection_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneDMXLibraryTemplate>() == std::is_polymorphic<FMovieSceneEvalTemplate>(), "USTRUCT FMovieSceneDMXLibraryTemplate cannot be polymorphic unless super FMovieSceneEvalTemplate is polymorphic");

class UScriptStruct* FMovieSceneDMXLibraryTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DMXRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate, Z_Construct_UPackage__Script_DMXRuntime(), TEXT("MovieSceneDMXLibraryTemplate"), sizeof(FMovieSceneDMXLibraryTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Hash());
	}
	return Singleton;
}
template<> DMXRUNTIME_API UScriptStruct* StaticStruct<FMovieSceneDMXLibraryTemplate>()
{
	return FMovieSceneDMXLibraryTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneDMXLibraryTemplate(FMovieSceneDMXLibraryTemplate::StaticStruct, TEXT("/Script/DMXRuntime"), TEXT("MovieSceneDMXLibraryTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_DMXRuntime_StaticRegisterNativesFMovieSceneDMXLibraryTemplate
{
	FScriptStruct_DMXRuntime_StaticRegisterNativesFMovieSceneDMXLibraryTemplate()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneDMXLibraryTemplate>(FName(TEXT("MovieSceneDMXLibraryTemplate")));
	}
} ScriptStruct_DMXRuntime_StaticRegisterNativesFMovieSceneDMXLibraryTemplate;
	struct Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Section_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Section;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Template that performs evaluation of Fixture Patch sections */" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibraryTemplate.h" },
		{ "ToolTip", "Template that performs evaluation of Fixture Patch sections" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneDMXLibraryTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::NewProp_Section_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Sequencer/MovieSceneDMXLibraryTemplate.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::NewProp_Section = { "Section", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneDMXLibraryTemplate, Section), Z_Construct_UClass_UMovieSceneDMXLibrarySection_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::NewProp_Section_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::NewProp_Section_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::NewProp_Section,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DMXRuntime,
		Z_Construct_UScriptStruct_FMovieSceneEvalTemplate,
		&NewStructOps,
		"MovieSceneDMXLibraryTemplate",
		sizeof(FMovieSceneDMXLibraryTemplate),
		alignof(FMovieSceneDMXLibraryTemplate),
		Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DMXRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneDMXLibraryTemplate"), sizeof(FMovieSceneDMXLibraryTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneDMXLibraryTemplate_Hash() { return 21763782U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
