// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DMXRUNTIME_DMXTypes_generated_h
#error "DMXTypes.generated.h already included, missing '#pragma once' in DMXTypes.h"
#endif
#define DMXRUNTIME_DMXTypes_generated_h

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXTypes_h_73_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXRawSACN_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXRequestBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXRawSACN>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXTypes_h_52_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXRawArtNetRequest_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXRequestBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXRawArtNetRequest>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXTypes_h_40_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXRequest_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct(); \
	typedef FDMXRequestBase Super;


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXRequest>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXTypes_h_28_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXRequestBase_Statics; \
	DMXRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXRequestBase>();

#define Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXTypes_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDMXNormalizedAttributeValueMap_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DMXRUNTIME_API UScriptStruct* StaticStruct<struct FDMXNormalizedAttributeValueMap>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_VirtualProduction_DMX_DMXEngine_Source_DMXRuntime_Public_DMXTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
