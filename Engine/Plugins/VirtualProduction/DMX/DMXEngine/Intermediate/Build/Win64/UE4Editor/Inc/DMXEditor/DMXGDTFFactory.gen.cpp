// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DMXEditor/Private/Factories/DMXGDTFFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDMXGDTFFactory() {}
// Cross Module References
	DMXEDITOR_API UClass* Z_Construct_UClass_UDMXGDTFFactory_NoRegister();
	DMXEDITOR_API UClass* Z_Construct_UClass_UDMXGDTFFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_DMXEditor();
	DMXEDITOR_API UClass* Z_Construct_UClass_UDMXGDTFImportUI_NoRegister();
// End Cross Module References
	void UDMXGDTFFactory::StaticRegisterNativesUDMXGDTFFactory()
	{
	}
	UClass* Z_Construct_UClass_UDMXGDTFFactory_NoRegister()
	{
		return UDMXGDTFFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDMXGDTFFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportUI_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportUI;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDMXGDTFFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_DMXEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXGDTFFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/DMXGDTFFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/DMXGDTFFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDMXGDTFFactory_Statics::NewProp_ImportUI_MetaData[] = {
		{ "ModuleRelativePath", "Private/Factories/DMXGDTFFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDMXGDTFFactory_Statics::NewProp_ImportUI = { "ImportUI", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDMXGDTFFactory, ImportUI), Z_Construct_UClass_UDMXGDTFImportUI_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDMXGDTFFactory_Statics::NewProp_ImportUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXGDTFFactory_Statics::NewProp_ImportUI_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDMXGDTFFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDMXGDTFFactory_Statics::NewProp_ImportUI,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDMXGDTFFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDMXGDTFFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDMXGDTFFactory_Statics::ClassParams = {
		&UDMXGDTFFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDMXGDTFFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDMXGDTFFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDMXGDTFFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDMXGDTFFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDMXGDTFFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDMXGDTFFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDMXGDTFFactory, 129884252);
	template<> DMXEDITOR_API UClass* StaticClass<UDMXGDTFFactory>()
	{
		return UDMXGDTFFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDMXGDTFFactory(Z_Construct_UClass_UDMXGDTFFactory, &UDMXGDTFFactory::StaticClass, TEXT("/Script/DMXEditor"), TEXT("UDMXGDTFFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDMXGDTFFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
