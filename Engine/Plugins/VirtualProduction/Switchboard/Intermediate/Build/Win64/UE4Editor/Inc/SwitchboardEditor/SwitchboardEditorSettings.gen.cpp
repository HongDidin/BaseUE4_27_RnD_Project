// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SwitchboardEditor/Public/SwitchboardEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSwitchboardEditorSettings() {}
// Cross Module References
	SWITCHBOARDEDITOR_API UClass* Z_Construct_UClass_USwitchboardEditorSettings_NoRegister();
	SWITCHBOARDEDITOR_API UClass* Z_Construct_UClass_USwitchboardEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_SwitchboardEditor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References
	DEFINE_FUNCTION(USwitchboardEditorSettings::execGetSwitchboardEditorSettings)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(USwitchboardEditorSettings**)Z_Param__Result=USwitchboardEditorSettings::GetSwitchboardEditorSettings();
		P_NATIVE_END;
	}
	void USwitchboardEditorSettings::StaticRegisterNativesUSwitchboardEditorSettings()
	{
		UClass* Class = USwitchboardEditorSettings::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetSwitchboardEditorSettings", &USwitchboardEditorSettings::execGetSwitchboardEditorSettings },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics
	{
		struct SwitchboardEditorSettings_eventGetSwitchboardEditorSettings_Parms
		{
			USwitchboardEditorSettings* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SwitchboardEditorSettings_eventGetSwitchboardEditorSettings_Parms, ReturnValue), Z_Construct_UClass_USwitchboardEditorSettings_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Switchboard" },
		{ "Comment", "/** Get Settings object for Switchboard*/" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "ToolTip", "Get Settings object for Switchboard" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USwitchboardEditorSettings, nullptr, "GetSwitchboardEditorSettings", nullptr, nullptr, sizeof(SwitchboardEditorSettings_eventGetSwitchboardEditorSettings_Parms), Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USwitchboardEditorSettings_NoRegister()
	{
		return USwitchboardEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_USwitchboardEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PythonInterpreterPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PythonInterpreterPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwitchboardPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SwitchboardPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommandlineArguments_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CommandlineArguments;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenerPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ListenerPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ListenerCommandlineArguments_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ListenerCommandlineArguments;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwitchboardOSCListener_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SwitchboardOSCListener;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USwitchboardEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_SwitchboardEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USwitchboardEditorSettings_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USwitchboardEditorSettings_GetSwitchboardEditorSettings, "GetSwitchboardEditorSettings" }, // 3533979766
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SwitchboardEditorSettings.h" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_PythonInterpreterPath_MetaData[] = {
		{ "Category", "Switchboard" },
		{ "Comment", "/** Switchboard installs its own python interpreter on its first launch. If you prefer\n\x09 * to use your own, specify the path to the python executable.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "ToolTip", "Switchboard installs its own python interpreter on its first launch. If you prefer\nto use your own, specify the path to the python executable." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_PythonInterpreterPath = { "PythonInterpreterPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USwitchboardEditorSettings, PythonInterpreterPath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_PythonInterpreterPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_PythonInterpreterPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardPath_MetaData[] = {
		{ "Category", "Switchboard" },
		{ "Comment", "/** Path to Switchboard itself. */" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "ToolTip", "Path to Switchboard itself." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardPath = { "SwitchboardPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USwitchboardEditorSettings, SwitchboardPath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_CommandlineArguments_MetaData[] = {
		{ "Category", "Switchboard" },
		{ "Comment", "/** Arguments that should be passed to Switchboard. */" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "ToolTip", "Arguments that should be passed to Switchboard." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_CommandlineArguments = { "CommandlineArguments", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USwitchboardEditorSettings, CommandlineArguments), METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_CommandlineArguments_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_CommandlineArguments_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerPath_MetaData[] = {
		{ "Category", "Listener" },
		{ "Comment", "/** Path to Switchboard Listener executable. */" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "ToolTip", "Path to Switchboard Listener executable." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerPath = { "ListenerPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USwitchboardEditorSettings, ListenerPath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerCommandlineArguments_MetaData[] = {
		{ "Category", "Listener" },
		{ "Comment", "/** Arguments that should be passed to the Switchboard Listener. */" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "ToolTip", "Arguments that should be passed to the Switchboard Listener." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerCommandlineArguments = { "ListenerCommandlineArguments", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USwitchboardEditorSettings, ListenerCommandlineArguments), METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerCommandlineArguments_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerCommandlineArguments_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardOSCListener_MetaData[] = {
		{ "Category", "OSC" },
		{ "DisplayName", "Default Switchboard OSC Listener" },
		{ "ModuleRelativePath", "Public/SwitchboardEditorSettings.h" },
		{ "Tooltip", "The OSC listener for Switchboard. An OSC server can be started on launch via the Virtual Production Editor section of the Project Settings. Switchboard uses port 8000 by default, but this can be configured in your Switchboard config settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardOSCListener = { "SwitchboardOSCListener", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USwitchboardEditorSettings, SwitchboardOSCListener), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardOSCListener_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardOSCListener_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USwitchboardEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_PythonInterpreterPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_CommandlineArguments,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_ListenerCommandlineArguments,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USwitchboardEditorSettings_Statics::NewProp_SwitchboardOSCListener,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USwitchboardEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USwitchboardEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USwitchboardEditorSettings_Statics::ClassParams = {
		&USwitchboardEditorSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_USwitchboardEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USwitchboardEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USwitchboardEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USwitchboardEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USwitchboardEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USwitchboardEditorSettings, 2140954430);
	template<> SWITCHBOARDEDITOR_API UClass* StaticClass<USwitchboardEditorSettings>()
	{
		return USwitchboardEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USwitchboardEditorSettings(Z_Construct_UClass_USwitchboardEditorSettings, &USwitchboardEditorSettings::StaticClass, TEXT("/Script/SwitchboardEditor"), TEXT("USwitchboardEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USwitchboardEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
