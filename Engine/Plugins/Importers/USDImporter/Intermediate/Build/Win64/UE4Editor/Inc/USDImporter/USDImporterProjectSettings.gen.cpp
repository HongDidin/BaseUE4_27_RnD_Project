// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Public/USDImporterProjectSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDImporterProjectSettings() {}
// Cross Module References
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_NoRegister();
// End Cross Module References
	void UDEPRECATED_UUSDImporterProjectSettings::StaticRegisterNativesUDEPRECATED_UUSDImporterProjectSettings()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_NoRegister()
	{
		return UDEPRECATED_UUSDImporterProjectSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionalPluginDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalPluginDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionalPluginDirectories;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomPrimResolver_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CustomPrimResolver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "USDImporter" },
		{ "IncludePath", "USDImporterProjectSettings.h" },
		{ "ModuleRelativePath", "Public/USDImporterProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories_Inner = { "AdditionalPluginDirectories", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Public/USDImporterProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories = { "AdditionalPluginDirectories", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDImporterProjectSettings, AdditionalPluginDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_CustomPrimResolver_MetaData[] = {
		{ "Comment", "/**\n\x09 * Allows a custom class to be specified that will resolve UsdPrim objects in a usd file to actors and assets\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDImporterProjectSettings.h" },
		{ "ToolTip", "Allows a custom class to be specified that will resolve UsdPrim objects in a usd file to actors and assets" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_CustomPrimResolver = { "CustomPrimResolver", nullptr, (EPropertyFlags)0x0014000020004000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDImporterProjectSettings, CustomPrimResolver_DEPRECATED), Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_CustomPrimResolver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_CustomPrimResolver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_AdditionalPluginDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::NewProp_CustomPrimResolver,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDImporterProjectSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::ClassParams = {
		&UDEPRECATED_UUSDImporterProjectSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::PropPointers),
		0,
		0x020002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDImporterProjectSettings, 2449015817);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDImporterProjectSettings>()
	{
		return UDEPRECATED_UUSDImporterProjectSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDImporterProjectSettings(Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings, &UDEPRECATED_UUSDImporterProjectSettings::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDImporterProjectSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDImporterProjectSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
