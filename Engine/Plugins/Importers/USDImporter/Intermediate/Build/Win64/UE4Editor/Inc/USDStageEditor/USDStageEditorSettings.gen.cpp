// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStageEditor/Private/USDStageEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDStageEditorSettings() {}
// Cross Module References
	USDSTAGEEDITOR_API UClass* Z_Construct_UClass_UUsdStageEditorSettings_NoRegister();
	USDSTAGEEDITOR_API UClass* Z_Construct_UClass_UUsdStageEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDStageEditor();
// End Cross Module References
	void UUsdStageEditorSettings::StaticRegisterNativesUUsdStageEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UUsdStageEditorSettings_NoRegister()
	{
		return UUsdStageEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UUsdStageEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectionSynced_MetaData[];
#endif
		static void NewProp_bSelectionSynced_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectionSynced;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdStageEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDStageEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDStageEditorSettings.h" },
		{ "ModuleRelativePath", "Private/USDStageEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Whether our prim selection in SUSDStageTreeView is kept synchronized with the viewport selection */" },
		{ "ModuleRelativePath", "Private/USDStageEditorSettings.h" },
		{ "ToolTip", "Whether our prim selection in SUSDStageTreeView is kept synchronized with the viewport selection" },
	};
#endif
	void Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced_SetBit(void* Obj)
	{
		((UUsdStageEditorSettings*)Obj)->bSelectionSynced = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced = { "bSelectionSynced", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageEditorSettings), &Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdStageEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageEditorSettings_Statics::NewProp_bSelectionSynced,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdStageEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdStageEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdStageEditorSettings_Statics::ClassParams = {
		&UUsdStageEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdStageEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdStageEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdStageEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdStageEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdStageEditorSettings, 632584154);
	template<> USDSTAGEEDITOR_API UClass* StaticClass<UUsdStageEditorSettings>()
	{
		return UUsdStageEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdStageEditorSettings(Z_Construct_UClass_UUsdStageEditorSettings, &UUsdStageEditorSettings::StaticClass, TEXT("/Script/USDStageEditor"), TEXT("UUsdStageEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdStageEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
