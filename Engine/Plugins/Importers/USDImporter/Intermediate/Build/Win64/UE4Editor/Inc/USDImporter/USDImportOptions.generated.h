// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDImportOptions_generated_h
#error "USDImportOptions.generated.h already included, missing '#pragma once' in USDImportOptions.h"
#endif
#define USDIMPORTER_USDImportOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDImportOptions(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDImportOptions(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDImportOptions(UDEPRECATED_UUSDImportOptions&&); \
	NO_API UDEPRECATED_UUSDImportOptions(const UDEPRECATED_UUSDImportOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDImportOptions(UDEPRECATED_UUSDImportOptions&&); \
	NO_API UDEPRECATED_UUSDImportOptions(const UDEPRECATED_UUSDImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_42_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_45_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDImportOptions>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDSceneImportOptions(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDSceneImportOptions, UDEPRECATED_UUSDImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDSceneImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDSceneImportOptions(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDSceneImportOptions, UDEPRECATED_UUSDImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDSceneImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDSceneImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDSceneImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDSceneImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDSceneImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDSceneImportOptions(UDEPRECATED_UUSDSceneImportOptions&&); \
	NO_API UDEPRECATED_UUSDSceneImportOptions(const UDEPRECATED_UUSDSceneImportOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDSceneImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDSceneImportOptions(UDEPRECATED_UUSDSceneImportOptions&&); \
	NO_API UDEPRECATED_UUSDSceneImportOptions(const UDEPRECATED_UUSDSceneImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDSceneImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDSceneImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDSceneImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_71_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_74_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDSceneImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDSceneImportOptions>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDBatchImportOptionsSubTask(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDBatchImportOptionsSubTask, UObject, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDBatchImportOptionsSubTask)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDBatchImportOptionsSubTask(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDBatchImportOptionsSubTask, UObject, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDBatchImportOptionsSubTask)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDBatchImportOptionsSubTask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDBatchImportOptionsSubTask) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDBatchImportOptionsSubTask); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDBatchImportOptionsSubTask); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDBatchImportOptionsSubTask(UDEPRECATED_UUSDBatchImportOptionsSubTask&&); \
	NO_API UDEPRECATED_UUSDBatchImportOptionsSubTask(const UDEPRECATED_UUSDBatchImportOptionsSubTask&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDBatchImportOptionsSubTask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDBatchImportOptionsSubTask(UDEPRECATED_UUSDBatchImportOptionsSubTask&&); \
	NO_API UDEPRECATED_UUSDBatchImportOptionsSubTask(const UDEPRECATED_UUSDBatchImportOptionsSubTask&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDBatchImportOptionsSubTask); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDBatchImportOptionsSubTask); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDBatchImportOptionsSubTask)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_117_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_120_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDBatchImportOptionsSubTask."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDBatchImportOptionsSubTask>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDBatchImportOptions(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDBatchImportOptions, UDEPRECATED_UUSDImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDBatchImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDBatchImportOptions(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDBatchImportOptions, UDEPRECATED_UUSDImportOptions, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDBatchImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDBatchImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDBatchImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDBatchImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDBatchImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDBatchImportOptions(UDEPRECATED_UUSDBatchImportOptions&&); \
	NO_API UDEPRECATED_UUSDBatchImportOptions(const UDEPRECATED_UUSDBatchImportOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDBatchImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDBatchImportOptions(UDEPRECATED_UUSDBatchImportOptions&&); \
	NO_API UDEPRECATED_UUSDBatchImportOptions(const UDEPRECATED_UUSDBatchImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDBatchImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDBatchImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDBatchImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_135_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h_138_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDBatchImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDBatchImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImportOptions_h


#define FOREACH_ENUM_EUSDMESHIMPORTTYPE(op) \
	op(EUsdMeshImportType::StaticMesh) 

enum class EUsdMeshImportType : uint8;
template<> USDIMPORTER_API UEnum* StaticEnum<EUsdMeshImportType>();

#define FOREACH_ENUM_EEXISTINGASSETPOLICY(op) \
	op(EExistingAssetPolicy::Reimport) \
	op(EExistingAssetPolicy::Ignore) 

enum class EExistingAssetPolicy : uint8;
template<> USDIMPORTER_API UEnum* StaticEnum<EExistingAssetPolicy>();

#define FOREACH_ENUM_EEXISTINGACTORPOLICY(op) \
	op(EExistingActorPolicy::Replace) \
	op(EExistingActorPolicy::UpdateTransform) \
	op(EExistingActorPolicy::Ignore) 

enum class EExistingActorPolicy : uint8;
template<> USDIMPORTER_API UEnum* StaticEnum<EExistingActorPolicy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
