// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDCLASSES_USDProjectSettings_generated_h
#error "USDProjectSettings.generated.h already included, missing '#pragma once' in USDProjectSettings.h"
#endif
#define USDCLASSES_USDProjectSettings_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdProjectSettings(); \
	friend struct Z_Construct_UClass_UUsdProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UUsdProjectSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDClasses"), USDCLASSES_API) \
	DECLARE_SERIALIZER(UUsdProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUUsdProjectSettings(); \
	friend struct Z_Construct_UClass_UUsdProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UUsdProjectSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDClasses"), USDCLASSES_API) \
	DECLARE_SERIALIZER(UUsdProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDCLASSES_API UUsdProjectSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdProjectSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDCLASSES_API, UUsdProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdProjectSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDCLASSES_API UUsdProjectSettings(UUsdProjectSettings&&); \
	USDCLASSES_API UUsdProjectSettings(const UUsdProjectSettings&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDCLASSES_API UUsdProjectSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDCLASSES_API UUsdProjectSettings(UUsdProjectSettings&&); \
	USDCLASSES_API UUsdProjectSettings(const UUsdProjectSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDCLASSES_API, UUsdProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdProjectSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdProjectSettings)


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_11_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDCLASSES_API UClass* StaticClass<class UUsdProjectSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDProjectSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
