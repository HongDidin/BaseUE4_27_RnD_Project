// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Private/USDAssetImportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDAssetImportFactory() {}
// Cross Module References
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUSDAssetImportContext();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUsdImportContext();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FUSDAssetImportContext>() == std::is_polymorphic<FUsdImportContext>(), "USTRUCT FUSDAssetImportContext cannot be polymorphic unless super FUsdImportContext is polymorphic");

class UScriptStruct* FUSDAssetImportContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDIMPORTER_API uint32 Get_Z_Construct_UScriptStruct_FUSDAssetImportContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUSDAssetImportContext, Z_Construct_UPackage__Script_USDImporter(), TEXT("USDAssetImportContext"), sizeof(FUSDAssetImportContext), Get_Z_Construct_UScriptStruct_FUSDAssetImportContext_Hash());
	}
	return Singleton;
}
template<> USDIMPORTER_API UScriptStruct* StaticStruct<FUSDAssetImportContext>()
{
	return FUSDAssetImportContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUSDAssetImportContext(FUSDAssetImportContext::StaticStruct, TEXT("/Script/USDImporter"), TEXT("USDAssetImportContext"), false, nullptr, nullptr);
static struct FScriptStruct_USDImporter_StaticRegisterNativesFUSDAssetImportContext
{
	FScriptStruct_USDImporter_StaticRegisterNativesFUSDAssetImportContext()
	{
		UScriptStruct::DeferCppStructOps<FUSDAssetImportContext>(FName(TEXT("USDAssetImportContext")));
	}
} ScriptStruct_USDImporter_StaticRegisterNativesFUSDAssetImportContext;
	struct Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/USDAssetImportFactory.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUSDAssetImportContext>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
		Z_Construct_UScriptStruct_FUsdImportContext,
		&NewStructOps,
		"USDAssetImportContext",
		sizeof(FUSDAssetImportContext),
		alignof(FUSDAssetImportContext),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUSDAssetImportContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUSDAssetImportContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("USDAssetImportContext"), sizeof(FUSDAssetImportContext), Get_Z_Construct_UScriptStruct_FUSDAssetImportContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUSDAssetImportContext_Hash() { return 683412238U; }
	void UDEPRECATED_UUSDAssetImportFactory::StaticRegisterNativesUDEPRECATED_UUSDAssetImportFactory()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_NoRegister()
	{
		return UDEPRECATED_UUSDAssetImportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImportContext;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDAssetImportFactory.h" },
		{ "ModuleRelativePath", "Private/USDAssetImportFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportContext_MetaData[] = {
		{ "ModuleRelativePath", "Private/USDAssetImportFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportContext = { "ImportContext", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDAssetImportFactory, ImportContext), Z_Construct_UScriptStruct_FUSDAssetImportContext, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportContext_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportOptions_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use the new USDStageImporter module instead" },
		{ "ModuleRelativePath", "Private/USDAssetImportFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportOptions = { "ImportOptions", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDAssetImportFactory, ImportOptions_DEPRECATED), Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::NewProp_ImportOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDAssetImportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::ClassParams = {
		&UDEPRECATED_UUSDAssetImportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::PropPointers),
		0,
		0x020002A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDAssetImportFactory, 115499200);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDAssetImportFactory>()
	{
		return UDEPRECATED_UUSDAssetImportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDAssetImportFactory(Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory, &UDEPRECATED_UUSDAssetImportFactory::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDAssetImportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDAssetImportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
