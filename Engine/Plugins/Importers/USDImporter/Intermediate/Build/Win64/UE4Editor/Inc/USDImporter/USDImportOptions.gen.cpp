// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Public/USDImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDImportOptions() {}
// Cross Module References
	USDIMPORTER_API UEnum* Z_Construct_UEnum_USDImporter_EUsdMeshImportType();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
	USDIMPORTER_API UEnum* Z_Construct_UEnum_USDImporter_EExistingAssetPolicy();
	USDIMPORTER_API UEnum* Z_Construct_UEnum_USDImporter_EExistingActorPolicy();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImportOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UNREALED_API UEnum* Z_Construct_UEnum_UnrealEd_EMaterialSearchLocation();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions();
// End Cross Module References
	static UEnum* EUsdMeshImportType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDImporter_EUsdMeshImportType, Z_Construct_UPackage__Script_USDImporter(), TEXT("EUsdMeshImportType"));
		}
		return Singleton;
	}
	template<> USDIMPORTER_API UEnum* StaticEnum<EUsdMeshImportType>()
	{
		return EUsdMeshImportType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUsdMeshImportType(EUsdMeshImportType_StaticEnum, TEXT("/Script/USDImporter"), TEXT("EUsdMeshImportType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDImporter_EUsdMeshImportType_Hash() { return 198493729U; }
	UEnum* Z_Construct_UEnum_USDImporter_EUsdMeshImportType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUsdMeshImportType"), 0, Get_Z_Construct_UEnum_USDImporter_EUsdMeshImportType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUsdMeshImportType::StaticMesh", (int64)EUsdMeshImportType::StaticMesh },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/USDImportOptions.h" },
				{ "StaticMesh.Name", "EUsdMeshImportType::StaticMesh" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDImporter,
				nullptr,
				"EUsdMeshImportType",
				"EUsdMeshImportType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EExistingAssetPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDImporter_EExistingAssetPolicy, Z_Construct_UPackage__Script_USDImporter(), TEXT("EExistingAssetPolicy"));
		}
		return Singleton;
	}
	template<> USDIMPORTER_API UEnum* StaticEnum<EExistingAssetPolicy>()
	{
		return EExistingAssetPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExistingAssetPolicy(EExistingAssetPolicy_StaticEnum, TEXT("/Script/USDImporter"), TEXT("EExistingAssetPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDImporter_EExistingAssetPolicy_Hash() { return 3207845968U; }
	UEnum* Z_Construct_UEnum_USDImporter_EExistingAssetPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExistingAssetPolicy"), 0, Get_Z_Construct_UEnum_USDImporter_EExistingAssetPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExistingAssetPolicy::Reimport", (int64)EExistingAssetPolicy::Reimport },
				{ "EExistingAssetPolicy::Ignore", (int64)EExistingAssetPolicy::Ignore },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Ignore.Comment", "/** Ignores existing assets and doesnt reimport them */" },
				{ "Ignore.Name", "EExistingAssetPolicy::Ignore" },
				{ "Ignore.ToolTip", "Ignores existing assets and doesnt reimport them" },
				{ "ModuleRelativePath", "Public/USDImportOptions.h" },
				{ "Reimport.Comment", "/** Reimports existing assets */" },
				{ "Reimport.Name", "EExistingAssetPolicy::Reimport" },
				{ "Reimport.ToolTip", "Reimports existing assets" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDImporter,
				nullptr,
				"EExistingAssetPolicy",
				"EExistingAssetPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EExistingActorPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDImporter_EExistingActorPolicy, Z_Construct_UPackage__Script_USDImporter(), TEXT("EExistingActorPolicy"));
		}
		return Singleton;
	}
	template<> USDIMPORTER_API UEnum* StaticEnum<EExistingActorPolicy>()
	{
		return EExistingActorPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExistingActorPolicy(EExistingActorPolicy_StaticEnum, TEXT("/Script/USDImporter"), TEXT("EExistingActorPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDImporter_EExistingActorPolicy_Hash() { return 2308815511U; }
	UEnum* Z_Construct_UEnum_USDImporter_EExistingActorPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExistingActorPolicy"), 0, Get_Z_Construct_UEnum_USDImporter_EExistingActorPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExistingActorPolicy::Replace", (int64)EExistingActorPolicy::Replace },
				{ "EExistingActorPolicy::UpdateTransform", (int64)EExistingActorPolicy::UpdateTransform },
				{ "EExistingActorPolicy::Ignore", (int64)EExistingActorPolicy::Ignore },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Ignore.Comment", "/** Ignore any existing actor with the same name */" },
				{ "Ignore.Name", "EExistingActorPolicy::Ignore" },
				{ "Ignore.ToolTip", "Ignore any existing actor with the same name" },
				{ "ModuleRelativePath", "Public/USDImportOptions.h" },
				{ "Replace.Comment", "/** Replaces existing actors with new ones */" },
				{ "Replace.Name", "EExistingActorPolicy::Replace" },
				{ "Replace.ToolTip", "Replaces existing actors with new ones" },
				{ "UpdateTransform.Comment", "/** Update transforms on existing actors but do not replace actor the actor class or any other data */" },
				{ "UpdateTransform.Name", "EExistingActorPolicy::UpdateTransform" },
				{ "UpdateTransform.ToolTip", "Update transforms on existing actors but do not replace actor the actor class or any other data" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDImporter,
				nullptr,
				"EExistingActorPolicy",
				"EExistingActorPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDEPRECATED_UUSDImportOptions::StaticRegisterNativesUDEPRECATED_UUSDImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_NoRegister()
	{
		return UDEPRECATED_UUSDImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MeshImportType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshImportType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MeshImportType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGenerateUniquePathPerUSDPrim_MetaData[];
#endif
		static void NewProp_bGenerateUniquePathPerUSDPrim_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateUniquePathPerUSDPrim;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyWorldTransformToGeometry_MetaData[];
#endif
		static void NewProp_bApplyWorldTransformToGeometry_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyWorldTransformToGeometry;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaterialSearchLocation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSearchLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialSearchLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDImportOptions.h" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Defines what should happen with existing actors */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Defines what should happen with existing actors" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType = { "MeshImportType", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDImportOptions, MeshImportType), Z_Construct_UEnum_USDImporter_EUsdMeshImportType, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/**\n\x09 * If checked, To enforce unique asset paths, all assets will be created in directories that match with their prim path\n\x09 * e.g a USD path /root/myassets/myprim_mesh will generate the path in the game directory \"/Game/myassets/\" with a mesh asset called \"myprim_mesh\" within that path.\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "If checked, To enforce unique asset paths, all assets will be created in directories that match with their prim path\ne.g a USD path /root/myassets/myprim_mesh will generate the path in the game directory \"/Game/myassets/\" with a mesh asset called \"myprim_mesh\" within that path." },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDImportOptions*)Obj)->bGenerateUniquePathPerUSDPrim = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim = { "bGenerateUniquePathPerUSDPrim", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDImportOptions*)Obj)->bApplyWorldTransformToGeometry = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry = { "bApplyWorldTransformToGeometry", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation_MetaData[] = {
		{ "Category", "Mesh|Materials" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation = { "MaterialSearchLocation", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDImportOptions, MaterialSearchLocation), Z_Construct_UEnum_UnrealEd_EMaterialSearchLocation, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDImportOptions, Scale), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MeshImportType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bGenerateUniquePathPerUSDPrim,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_bApplyWorldTransformToGeometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_MaterialSearchLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::NewProp_Scale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::ClassParams = {
		&UDEPRECATED_UUSDImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::PropPointers),
		0,
		0x021002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDImportOptions, 982554402);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDImportOptions>()
	{
		return UDEPRECATED_UUSDImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDImportOptions(Z_Construct_UClass_UDEPRECATED_UUSDImportOptions, &UDEPRECATED_UUSDImportOptions::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDImportOptions);
	void UDEPRECATED_UUSDSceneImportOptions::StaticRegisterNativesUDEPRECATED_UUSDSceneImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_NoRegister()
	{
		return UDEPRECATED_UUSDSceneImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PurposesToImport_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PurposesToImport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlattenHierarchy_MetaData[];
#endif
		static void NewProp_bFlattenHierarchy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlattenHierarchy;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExistingActorPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistingActorPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExistingActorPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportProperties_MetaData[];
#endif
		static void NewProp_bImportProperties_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportMeshes_MetaData[];
#endif
		static void NewProp_bImportMeshes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportMeshes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathForAssets_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PathForAssets;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExistingAssetPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistingAssetPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExistingAssetPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGenerateUniqueMeshes_MetaData[];
#endif
		static void NewProp_bGenerateUniqueMeshes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateUniqueMeshes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDEPRECATED_UUSDImportOptions,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDImportOptions.h" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PurposesToImport_MetaData[] = {
		{ "Bitmask", "" },
		{ "BitmaskEnum", "EUsdPurpose" },
		{ "Category", "General" },
		{ "Comment", "/* Only import prims with these specific purposes from the USD file */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Only import prims with these specific purposes from the USD file" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PurposesToImport = { "PurposesToImport", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportOptions, PurposesToImport), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PurposesToImport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PurposesToImport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** If checked, all actors generated will have a world space transform and will not have any attachment hierarchy */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "If checked, all actors generated will have a world space transform and will not have any attachment hierarchy" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDSceneImportOptions*)Obj)->bFlattenHierarchy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy = { "bFlattenHierarchy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDSceneImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Defines what should happen with existing actors */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Defines what should happen with existing actors" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy = { "ExistingActorPolicy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportOptions, ExistingActorPolicy), Z_Construct_UEnum_USDImporter_EExistingActorPolicy, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Whether or not to import custom properties and set their unreal equivalent on spawned actors */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Whether or not to import custom properties and set their unreal equivalent on spawned actors" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDSceneImportOptions*)Obj)->bImportProperties = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties = { "bImportProperties", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDSceneImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Whether or not to import mesh geometry or to just spawn actors using existing meshes */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Whether or not to import mesh geometry or to just spawn actors using existing meshes" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDSceneImportOptions*)Obj)->bImportMeshes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes = { "bImportMeshes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDSceneImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PathForAssets_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** The path where new assets are imported */" },
		{ "ContentDir", "" },
		{ "EditCondition", "bImportMeshes" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "The path where new assets are imported" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PathForAssets = { "PathForAssets", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportOptions, PathForAssets), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PathForAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PathForAssets_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** What should happen with existing assets */" },
		{ "EditCondition", "bImportMeshes" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "What should happen with existing assets" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy = { "ExistingAssetPolicy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportOptions, ExistingAssetPolicy), Z_Construct_UEnum_USDImporter_EExistingAssetPolicy, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/**\n\x09 * This setting determines what to do if more than one USD prim is found with the same name.  If this setting is true a unique name will be generated and a unique asset will be imported\n\x09 * If this is false, the first asset found is generated. Assets will be reused when spawning actors into the world.\n\x09 */" },
		{ "EditCondition", "bImportMeshes" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "This setting determines what to do if more than one USD prim is found with the same name.  If this setting is true a unique name will be generated and a unique asset will be imported\nIf this is false, the first asset found is generated. Assets will be reused when spawning actors into the world." },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDSceneImportOptions*)Obj)->bGenerateUniqueMeshes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes = { "bGenerateUniqueMeshes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDSceneImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PurposesToImport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bFlattenHierarchy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingActorPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bImportMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_PathForAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_ExistingAssetPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::NewProp_bGenerateUniqueMeshes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDSceneImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::ClassParams = {
		&UDEPRECATED_UUSDSceneImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::PropPointers),
		0,
		0x021002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDSceneImportOptions, 3499214644);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDSceneImportOptions>()
	{
		return UDEPRECATED_UUSDSceneImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDSceneImportOptions(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions, &UDEPRECATED_UUSDSceneImportOptions::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDSceneImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDSceneImportOptions);
	void UDEPRECATED_UUSDBatchImportOptionsSubTask::StaticRegisterNativesUDEPRECATED_UUSDBatchImportOptionsSubTask()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_NoRegister()
	{
		return UDEPRECATED_UUSDBatchImportOptionsSubTask::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourcePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourcePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DestPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ErrorMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ErrorMessage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDImportOptions.h" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_SourcePath_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Path in the USD stage to import from */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Path in the USD stage to import from" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_SourcePath = { "SourcePath", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDBatchImportOptionsSubTask, SourcePath), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_SourcePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_SourcePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_DestPath_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Path to import asset as */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Path to import asset as" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_DestPath = { "DestPath", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDBatchImportOptionsSubTask, DestPath), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_DestPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_DestPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_ErrorMessage_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_ErrorMessage = { "ErrorMessage", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDBatchImportOptionsSubTask, ErrorMessage), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_ErrorMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_ErrorMessage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_SourcePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_DestPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::NewProp_ErrorMessage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDBatchImportOptionsSubTask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::ClassParams = {
		&UDEPRECATED_UUSDBatchImportOptionsSubTask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::PropPointers),
		0,
		0x021002A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDBatchImportOptionsSubTask, 552607843);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDBatchImportOptionsSubTask>()
	{
		return UDEPRECATED_UUSDBatchImportOptionsSubTask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask, &UDEPRECATED_UUSDBatchImportOptionsSubTask::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDBatchImportOptionsSubTask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDBatchImportOptionsSubTask);
	void UDEPRECATED_UUSDBatchImportOptions::StaticRegisterNativesUDEPRECATED_UUSDBatchImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_NoRegister()
	{
		return UDEPRECATED_UUSDBatchImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportMeshes_MetaData[];
#endif
		static void NewProp_bImportMeshes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportMeshes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathForAssets_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PathForAssets;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExistingAssetPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistingAssetPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExistingAssetPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGenerateUniqueMeshes_MetaData[];
#endif
		static void NewProp_bGenerateUniqueMeshes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateUniqueMeshes;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SubTasks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubTasks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SubTasks;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDEPRECATED_UUSDImportOptions,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDImportOptions.h" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Whether or not to import mesh geometry or to just spawn actors using existing meshes */" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "Whether or not to import mesh geometry or to just spawn actors using existing meshes" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDBatchImportOptions*)Obj)->bImportMeshes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes = { "bImportMeshes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDBatchImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_PathForAssets_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** The path where new assets are imported */" },
		{ "ContentDir", "" },
		{ "EditCondition", "bImportMeshes" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "The path where new assets are imported" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_PathForAssets = { "PathForAssets", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDBatchImportOptions, PathForAssets), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_PathForAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_PathForAssets_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** What should happen with existing assets */" },
		{ "EditCondition", "bImportMeshes" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "What should happen with existing assets" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy = { "ExistingAssetPolicy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDBatchImportOptions, ExistingAssetPolicy), Z_Construct_UEnum_USDImporter_EExistingAssetPolicy, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/**\n\x09 * This setting determines what to do if more than one USD prim is found with the same name.  If this setting is true a unique name will be generated and a unique asset will be imported\n\x09 * If this is false, the first asset found is generated. Assets will be reused when spawning actors into the world.\n\x09 */" },
		{ "EditCondition", "bImportMeshes" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
		{ "ToolTip", "This setting determines what to do if more than one USD prim is found with the same name.  If this setting is true a unique name will be generated and a unique asset will be imported\nIf this is false, the first asset found is generated. Assets will be reused when spawning actors into the world." },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes_SetBit(void* Obj)
	{
		((UDEPRECATED_UUSDBatchImportOptions*)Obj)->bGenerateUniqueMeshes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes = { "bGenerateUniqueMeshes", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUSDBatchImportOptions), &Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks_Inner = { "SubTasks", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptionsSubTask_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use the new USDStageImporter module instead" },
		{ "ModuleRelativePath", "Public/USDImportOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks = { "SubTasks", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDBatchImportOptions, SubTasks_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bImportMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_PathForAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_ExistingAssetPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_bGenerateUniqueMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::NewProp_SubTasks,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDBatchImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::ClassParams = {
		&UDEPRECATED_UUSDBatchImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::PropPointers),
		0,
		0x021002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDBatchImportOptions, 447049353);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDBatchImportOptions>()
	{
		return UDEPRECATED_UUSDBatchImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDBatchImportOptions(Z_Construct_UClass_UDEPRECATED_UUSDBatchImportOptions, &UDEPRECATED_UUSDBatchImportOptions::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDBatchImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDBatchImportOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
