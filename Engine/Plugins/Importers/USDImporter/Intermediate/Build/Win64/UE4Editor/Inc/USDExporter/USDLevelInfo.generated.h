// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_USDLevelInfo_generated_h
#error "USDLevelInfo.generated.h already included, missing '#pragma once' in USDLevelInfo.h"
#endif
#define USDEXPORTER_USDLevelInfo_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSaveUSD);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSaveUSD);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUSDLevelInfo(); \
	friend struct Z_Construct_UClass_AUSDLevelInfo_Statics; \
public: \
	DECLARE_CLASS(AUSDLevelInfo, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(AUSDLevelInfo)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAUSDLevelInfo(); \
	friend struct Z_Construct_UClass_AUSDLevelInfo_Statics; \
public: \
	DECLARE_CLASS(AUSDLevelInfo, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(AUSDLevelInfo)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUSDLevelInfo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUSDLevelInfo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUSDLevelInfo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUSDLevelInfo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUSDLevelInfo(AUSDLevelInfo&&); \
	NO_API AUSDLevelInfo(const AUSDLevelInfo&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUSDLevelInfo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUSDLevelInfo(AUSDLevelInfo&&); \
	NO_API AUSDLevelInfo(const AUSDLevelInfo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUSDLevelInfo); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUSDLevelInfo); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUSDLevelInfo)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_10_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class USDLevelInfo."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class AUSDLevelInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Private_USDLevelInfo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
