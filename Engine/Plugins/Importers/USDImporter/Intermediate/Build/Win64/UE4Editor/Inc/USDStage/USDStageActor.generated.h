// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
class USceneComponent;
enum class EUsdInitialLoadSet : uint8;
#ifdef USDSTAGE_USDStageActor_generated_h
#error "USDStageActor.generated.h already included, missing '#pragma once' in USDStageActor.h"
#endif
#define USDSTAGE_USDStageActor_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSourcePrimPath); \
	DECLARE_FUNCTION(execGetGeneratedAssets); \
	DECLARE_FUNCTION(execGetGeneratedComponent); \
	DECLARE_FUNCTION(execSetTime); \
	DECLARE_FUNCTION(execGetTime); \
	DECLARE_FUNCTION(execSetRenderContext); \
	DECLARE_FUNCTION(execSetPurposesToLoad); \
	DECLARE_FUNCTION(execSetInitialLoadSet); \
	DECLARE_FUNCTION(execSetRootLayer);


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSourcePrimPath); \
	DECLARE_FUNCTION(execGetGeneratedAssets); \
	DECLARE_FUNCTION(execGetGeneratedComponent); \
	DECLARE_FUNCTION(execSetTime); \
	DECLARE_FUNCTION(execGetTime); \
	DECLARE_FUNCTION(execSetRenderContext); \
	DECLARE_FUNCTION(execSetPurposesToLoad); \
	DECLARE_FUNCTION(execSetInitialLoadSet); \
	DECLARE_FUNCTION(execSetRootLayer);


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(AUsdStageActor, USDSTAGE_API)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUsdStageActor(); \
	friend struct Z_Construct_UClass_AUsdStageActor_Statics; \
public: \
	DECLARE_CLASS(AUsdStageActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDStage"), USDSTAGE_API) \
	DECLARE_SERIALIZER(AUsdStageActor) \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_ARCHIVESERIALIZER


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_INCLASS \
private: \
	static void StaticRegisterNativesAUsdStageActor(); \
	friend struct Z_Construct_UClass_AUsdStageActor_Statics; \
public: \
	DECLARE_CLASS(AUsdStageActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDStage"), USDSTAGE_API) \
	DECLARE_SERIALIZER(AUsdStageActor) \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_ARCHIVESERIALIZER


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDSTAGE_API AUsdStageActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUsdStageActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDSTAGE_API, AUsdStageActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUsdStageActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDSTAGE_API AUsdStageActor(AUsdStageActor&&); \
	USDSTAGE_API AUsdStageActor(const AUsdStageActor&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDSTAGE_API AUsdStageActor(AUsdStageActor&&); \
	USDSTAGE_API AUsdStageActor(const AUsdStageActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDSTAGE_API, AUsdStageActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUsdStageActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUsdStageActor)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneComponent() { return STRUCT_OFFSET(AUsdStageActor, SceneComponent); } \
	FORCEINLINE static uint32 __PPO__Time() { return STRUCT_OFFSET(AUsdStageActor, Time); } \
	FORCEINLINE static uint32 __PPO__StartTimeCode_DEPRECATED() { return STRUCT_OFFSET(AUsdStageActor, StartTimeCode_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__EndTimeCode_DEPRECATED() { return STRUCT_OFFSET(AUsdStageActor, EndTimeCode_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__TimeCodesPerSecond_DEPRECATED() { return STRUCT_OFFSET(AUsdStageActor, TimeCodesPerSecond_DEPRECATED); } \
	FORCEINLINE static uint32 __PPO__LevelSequence() { return STRUCT_OFFSET(AUsdStageActor, LevelSequence); } \
	FORCEINLINE static uint32 __PPO__RootUsdTwin() { return STRUCT_OFFSET(AUsdStageActor, RootUsdTwin); } \
	FORCEINLINE static uint32 __PPO__PrimsToAnimate() { return STRUCT_OFFSET(AUsdStageActor, PrimsToAnimate); } \
	FORCEINLINE static uint32 __PPO__ObjectsToWatch() { return STRUCT_OFFSET(AUsdStageActor, ObjectsToWatch); } \
	FORCEINLINE static uint32 __PPO__AssetCache() { return STRUCT_OFFSET(AUsdStageActor, AssetCache); } \
	FORCEINLINE static uint32 __PPO__Transactor() { return STRUCT_OFFSET(AUsdStageActor, Transactor); }


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_32_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGE_API UClass* StaticClass<class AUsdStageActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDStageActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
