// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/StaticMeshExporterUSDOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStaticMeshExporterUSDOptions() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UStaticMeshExporterUSDOptions_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UStaticMeshExporterUSDOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	USDCLASSES_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions();
// End Cross Module References
	void UStaticMeshExporterUSDOptions::StaticRegisterNativesUStaticMeshExporterUSDOptions()
	{
	}
	UClass* Z_Construct_UClass_UStaticMeshExporterUSDOptions_NoRegister()
	{
		return UStaticMeshExporterUSDOptions::StaticClass();
	}
	struct Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StageOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsePayload_MetaData[];
#endif
		static void NewProp_bUsePayload_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePayload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PayloadFormat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Options for exporting static meshes to USD format.\n */" },
		{ "IncludePath", "StaticMeshExporterUSDOptions.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/StaticMeshExporterUSDOptions.h" },
		{ "ToolTip", "Options for exporting static meshes to USD format." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_StageOptions_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "ModuleRelativePath", "Public/StaticMeshExporterUSDOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_StageOptions = { "StageOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshExporterUSDOptions, StageOptions), Z_Construct_UScriptStruct_FUsdStageOptions, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_StageOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_StageOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** If true, the mesh data is exported to yet another \"payload\" file, and referenced via a payload composition arc */" },
		{ "ModuleRelativePath", "Public/StaticMeshExporterUSDOptions.h" },
		{ "ToolTip", "If true, the mesh data is exported to yet another \"payload\" file, and referenced via a payload composition arc" },
	};
#endif
	void Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload_SetBit(void* Obj)
	{
		((UStaticMeshExporterUSDOptions*)Obj)->bUsePayload = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload = { "bUsePayload", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UStaticMeshExporterUSDOptions), &Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload_SetBit, METADATA_PARAMS(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_PayloadFormat_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** USD format to use for exported payload files */" },
		{ "EditCondition", "bUsePayload" },
		{ "GetOptions", "USDExporter.LevelExporterUSDOptions.GetUsdExtensions" },
		{ "ModuleRelativePath", "Public/StaticMeshExporterUSDOptions.h" },
		{ "ToolTip", "USD format to use for exported payload files" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_PayloadFormat = { "PayloadFormat", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UStaticMeshExporterUSDOptions, PayloadFormat), METADATA_PARAMS(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_PayloadFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_PayloadFormat_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_StageOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_bUsePayload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::NewProp_PayloadFormat,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UStaticMeshExporterUSDOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::ClassParams = {
		&UStaticMeshExporterUSDOptions::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UStaticMeshExporterUSDOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStaticMeshExporterUSDOptions, 4033407976);
	template<> USDEXPORTER_API UClass* StaticClass<UStaticMeshExporterUSDOptions>()
	{
		return UStaticMeshExporterUSDOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStaticMeshExporterUSDOptions(Z_Construct_UClass_UStaticMeshExporterUSDOptions, &UStaticMeshExporterUSDOptions::StaticClass, TEXT("/Script/USDExporter"), TEXT("UStaticMeshExporterUSDOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStaticMeshExporterUSDOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
