// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDCLASSES_USDAssetCache_generated_h
#error "USDAssetCache.generated.h already included, missing '#pragma once' in USDAssetCache.h"
#endif
#define USDCLASSES_USDAssetCache_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UUsdAssetCache, NO_API)


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdAssetCache(); \
	friend struct Z_Construct_UClass_UUsdAssetCache_Statics; \
public: \
	DECLARE_CLASS(UUsdAssetCache, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDClasses"), NO_API) \
	DECLARE_SERIALIZER(UUsdAssetCache) \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_ARCHIVESERIALIZER


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUUsdAssetCache(); \
	friend struct Z_Construct_UClass_UUsdAssetCache_Statics; \
public: \
	DECLARE_CLASS(UUsdAssetCache, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDClasses"), NO_API) \
	DECLARE_SERIALIZER(UUsdAssetCache) \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_ARCHIVESERIALIZER


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdAssetCache(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdAssetCache) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdAssetCache); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdAssetCache); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdAssetCache(UUsdAssetCache&&); \
	NO_API UUsdAssetCache(const UUsdAssetCache&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdAssetCache(UUsdAssetCache&&); \
	NO_API UUsdAssetCache(const UUsdAssetCache&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdAssetCache); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdAssetCache); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUsdAssetCache)


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TransientStorage() { return STRUCT_OFFSET(UUsdAssetCache, TransientStorage); } \
	FORCEINLINE static uint32 __PPO__PersistentStorage() { return STRUCT_OFFSET(UUsdAssetCache, PersistentStorage); } \
	FORCEINLINE static uint32 __PPO__bAllowPersistentStorage() { return STRUCT_OFFSET(UUsdAssetCache, bAllowPersistentStorage); } \
	FORCEINLINE static uint32 __PPO__OwnedAssets() { return STRUCT_OFFSET(UUsdAssetCache, OwnedAssets); } \
	FORCEINLINE static uint32 __PPO__PrimPathToAssets() { return STRUCT_OFFSET(UUsdAssetCache, PrimPathToAssets); }


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_11_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDCLASSES_API UClass* StaticClass<class UUsdAssetCache>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetCache_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
