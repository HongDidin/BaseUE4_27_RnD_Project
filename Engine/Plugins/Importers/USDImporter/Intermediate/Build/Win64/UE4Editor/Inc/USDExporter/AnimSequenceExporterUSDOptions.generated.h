// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_AnimSequenceExporterUSDOptions_generated_h
#error "AnimSequenceExporterUSDOptions.generated.h already included, missing '#pragma once' in AnimSequenceExporterUSDOptions.h"
#endif
#define USDEXPORTER_AnimSequenceExporterUSDOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimSequenceExporterUSDOptions(); \
	friend struct Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(UAnimSequenceExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UAnimSequenceExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUAnimSequenceExporterUSDOptions(); \
	friend struct Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(UAnimSequenceExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UAnimSequenceExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimSequenceExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimSequenceExporterUSDOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimSequenceExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimSequenceExporterUSDOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimSequenceExporterUSDOptions(UAnimSequenceExporterUSDOptions&&); \
	NO_API UAnimSequenceExporterUSDOptions(const UAnimSequenceExporterUSDOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimSequenceExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimSequenceExporterUSDOptions(UAnimSequenceExporterUSDOptions&&); \
	NO_API UAnimSequenceExporterUSDOptions(const UAnimSequenceExporterUSDOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimSequenceExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimSequenceExporterUSDOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimSequenceExporterUSDOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_16_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UAnimSequenceExporterUSDOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSDOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
