// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Private/UsdTestActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUsdTestActor() {}
// Cross Module References
	USDIMPORTER_API UEnum* Z_Construct_UEnum_USDImporter_EUsdTestEnum();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUsdTestStruct();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUsdTestStruct2();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUsdTestComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector4();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_ADEPRECATED_AUsdTestActor_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_ADEPRECATED_AUsdTestActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
	static UEnum* EUsdTestEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDImporter_EUsdTestEnum, Z_Construct_UPackage__Script_USDImporter(), TEXT("EUsdTestEnum"));
		}
		return Singleton;
	}
	template<> USDIMPORTER_API UEnum* StaticEnum<EUsdTestEnum>()
	{
		return EUsdTestEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUsdTestEnum(EUsdTestEnum_StaticEnum, TEXT("/Script/USDImporter"), TEXT("EUsdTestEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDImporter_EUsdTestEnum_Hash() { return 1983929442U; }
	UEnum* Z_Construct_UEnum_USDImporter_EUsdTestEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUsdTestEnum"), 0, Get_Z_Construct_UEnum_USDImporter_EUsdTestEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUsdTestEnum::EnumVal1", (int64)EUsdTestEnum::EnumVal1 },
				{ "EUsdTestEnum::EnumVal2", (int64)EUsdTestEnum::EnumVal2 },
				{ "EUsdTestEnum::EnumVal3", (int64)EUsdTestEnum::EnumVal3 },
				{ "EUsdTestEnum::EnumVal4", (int64)EUsdTestEnum::EnumVal4 },
				{ "EUsdTestEnum::EnumVal5", (int64)EUsdTestEnum::EnumVal5 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "EnumVal1.Name", "EUsdTestEnum::EnumVal1" },
				{ "EnumVal2.Name", "EUsdTestEnum::EnumVal2" },
				{ "EnumVal3.Name", "EUsdTestEnum::EnumVal3" },
				{ "EnumVal4.Name", "EUsdTestEnum::EnumVal4" },
				{ "EnumVal5.Name", "EUsdTestEnum::EnumVal5" },
				{ "ModuleRelativePath", "Private/UsdTestActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDImporter,
				nullptr,
				"EUsdTestEnum",
				"EUsdTestEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FUsdTestStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDIMPORTER_API uint32 Get_Z_Construct_UScriptStruct_FUsdTestStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUsdTestStruct, Z_Construct_UPackage__Script_USDImporter(), TEXT("UsdTestStruct"), sizeof(FUsdTestStruct), Get_Z_Construct_UScriptStruct_FUsdTestStruct_Hash());
	}
	return Singleton;
}
template<> USDIMPORTER_API UScriptStruct* StaticStruct<FUsdTestStruct>()
{
	return FUsdTestStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUsdTestStruct(FUsdTestStruct::StaticStruct, TEXT("/Script/USDImporter"), TEXT("UsdTestStruct"), false, nullptr, nullptr);
static struct FScriptStruct_USDImporter_StaticRegisterNativesFUsdTestStruct
{
	FScriptStruct_USDImporter_StaticRegisterNativesFUsdTestStruct()
	{
		UScriptStruct::DeferCppStructOps<FUsdTestStruct>(FName(TEXT("UsdTestStruct")));
	}
} ScriptStruct_USDImporter_StaticRegisterNativesFUsdTestStruct;
	struct Z_Construct_UScriptStruct_FUsdTestStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumProperty_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ColorProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MapTest_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MapTest_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapTest_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_MapTest;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InnerStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUsdTestStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_IntProperty_MetaData[] = {
		{ "Category", "UsdTestStruct" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_IntProperty = { "IntProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct, IntProperty), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_IntProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_IntProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty_MetaData[] = {
		{ "Category", "UsdTestStruct" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty = { "EnumProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct, EnumProperty), Z_Construct_UEnum_USDImporter_EUsdTestEnum, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty_Inner = { "ColorProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty_MetaData[] = {
		{ "Category", "UsdTestStruct" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty = { "ColorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct, ColorProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_ValueProp = { "MapTest", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FUsdTestStruct2, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_Key_KeyProp = { "MapTest_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_MetaData[] = {
		{ "Category", "UsdTestStruct" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest = { "MapTest", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct, MapTest), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_InnerStruct_MetaData[] = {
		{ "Category", "UsdTestStruct" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_InnerStruct = { "InnerStruct", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct, InnerStruct), Z_Construct_UScriptStruct_FUsdTestStruct2, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_InnerStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_InnerStruct_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUsdTestStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_IntProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_EnumProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_ColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_MapTest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct_Statics::NewProp_InnerStruct,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUsdTestStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
		nullptr,
		&NewStructOps,
		"UsdTestStruct",
		sizeof(FUsdTestStruct),
		alignof(FUsdTestStruct),
		Z_Construct_UScriptStruct_FUsdTestStruct_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUsdTestStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUsdTestStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UsdTestStruct"), sizeof(FUsdTestStruct), Get_Z_Construct_UScriptStruct_FUsdTestStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUsdTestStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUsdTestStruct_Hash() { return 1148298595U; }
class UScriptStruct* FUsdTestStruct2::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDIMPORTER_API uint32 Get_Z_Construct_UScriptStruct_FUsdTestStruct2_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUsdTestStruct2, Z_Construct_UPackage__Script_USDImporter(), TEXT("UsdTestStruct2"), sizeof(FUsdTestStruct2), Get_Z_Construct_UScriptStruct_FUsdTestStruct2_Hash());
	}
	return Singleton;
}
template<> USDIMPORTER_API UScriptStruct* StaticStruct<FUsdTestStruct2>()
{
	return FUsdTestStruct2::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUsdTestStruct2(FUsdTestStruct2::StaticStruct, TEXT("/Script/USDImporter"), TEXT("UsdTestStruct2"), false, nullptr, nullptr);
static struct FScriptStruct_USDImporter_StaticRegisterNativesFUsdTestStruct2
{
	FScriptStruct_USDImporter_StaticRegisterNativesFUsdTestStruct2()
	{
		UScriptStruct::DeferCppStructOps<FUsdTestStruct2>(FName(TEXT("UsdTestStruct2")));
	}
} ScriptStruct_USDImporter_StaticRegisterNativesFUsdTestStruct2;
	struct Z_Construct_UScriptStruct_FUsdTestStruct2_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringProperty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUsdTestStruct2>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_FloatProperty_MetaData[] = {
		{ "Category", "UsdTestStruct2" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_FloatProperty = { "FloatProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct2, FloatProperty), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_FloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_FloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_StringProperty_MetaData[] = {
		{ "Category", "UsdTestStruct2" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_StringProperty = { "StringProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdTestStruct2, StringProperty), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_StringProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_StringProperty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_FloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::NewProp_StringProperty,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
		nullptr,
		&NewStructOps,
		"UsdTestStruct2",
		sizeof(FUsdTestStruct2),
		alignof(FUsdTestStruct2),
		Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUsdTestStruct2()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUsdTestStruct2_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UsdTestStruct2"), sizeof(FUsdTestStruct2), Get_Z_Construct_UScriptStruct_FUsdTestStruct2_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUsdTestStruct2_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUsdTestStruct2_Hash() { return 3088278583U; }
	void UDEPRECATED_UUsdTestComponent::StaticRegisterNativesUDEPRECATED_UUsdTestComponent()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_NoRegister()
	{
		return UDEPRECATED_UUsdTestComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int16Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Int16Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int64Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Int64Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt16Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_UnsignedInt16Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt32Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_UnsignedInt32Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt64Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_UnsignedInt64Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DoubleProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolProperty_MetaData[];
#endif
		static void NewProp_BoolProperty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector2Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector2Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector3Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector3Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector4Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector4Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearColorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotatorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotatorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumProperty_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumProperty;
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int8ArrayProperty;
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Int16ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int16ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int16ArrayProperty;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int32ArrayProperty;
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Int64ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int64ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int64ArrayProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ByteArrayProperty;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_UnsignedInt16ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt16ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnsignedInt16ArrayProperty;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_UnsignedInt32ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt32ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnsignedInt32ArrayProperty;
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_UnsignedInt64ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt64ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnsignedInt64ArrayProperty;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FloatArrayProperty;
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DoubleArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DoubleArrayProperty;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NameArrayProperty;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoolArrayProperty;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StringArrayProperty;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TextArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector2ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector2ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vector2ArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector3ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector3ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vector3ArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector4ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector4ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vector4ArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearColorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearColorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LinearColorArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotatorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotatorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RotatorArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ColorArrayProperty;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectArrayProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumArrayProperty_Inner_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnumArrayProperty;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "UsdTestActor.h" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "Comment", "// Basic properties\n" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
		{ "ToolTip", "Basic properties" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8Property = { "Int8Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int8Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16Property = { "Int16Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int16Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32Property = { "Int32Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int32Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64Property = { "Int64Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int64Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteProperty = { "ByteProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, ByteProperty), nullptr, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16Property = { "UnsignedInt16Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, UnsignedInt16Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32Property = { "UnsignedInt32Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, UnsignedInt32Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64Property = { "UnsignedInt64Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, UnsignedInt64Property), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatProperty = { "FloatProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, FloatProperty), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleProperty = { "DoubleProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, DoubleProperty), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameProperty = { "NameProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, NameProperty), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	void Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty_SetBit(void* Obj)
	{
		((UDEPRECATED_UUsdTestComponent*)Obj)->BoolProperty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty = { "BoolProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDEPRECATED_UUsdTestComponent), &Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringProperty = { "StringProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, StringProperty), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextProperty = { "TextProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, TextProperty), METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2Property = { "Vector2Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Vector2Property), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3Property = { "Vector3Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Vector3Property), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4Property_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4Property = { "Vector4Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Vector4Property), Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorProperty = { "LinearColorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, LinearColorProperty), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorProperty = { "RotatorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, RotatorProperty), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorProperty = { "ColorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, ColorProperty), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectProperty = { "ObjectProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, ObjectProperty), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty_MetaData[] = {
		{ "Category", "Component|BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty = { "EnumProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, EnumProperty), Z_Construct_UEnum_USDImporter_EUsdTestEnum, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty_MetaData)) };
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty_Inner = { "Int8ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "Comment", "// Arrays\n" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
		{ "ToolTip", "Arrays" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty = { "Int8ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int8ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty_Inner = { "Int16ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty = { "Int16ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int16ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty_Inner = { "Int32ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty = { "Int32ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int32ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty_Inner = { "Int64ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty = { "Int64ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Int64ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty_Inner = { "ByteArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty = { "ByteArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, ByteArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty_Inner = { "UnsignedInt16ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty = { "UnsignedInt16ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, UnsignedInt16ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty_Inner = { "UnsignedInt32ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty = { "UnsignedInt32ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, UnsignedInt32ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty_Inner = { "UnsignedInt64ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty = { "UnsignedInt64ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, UnsignedInt64ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty_Inner = { "FloatArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty = { "FloatArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, FloatArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty_Inner = { "DoubleArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty = { "DoubleArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, DoubleArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty_Inner = { "NameArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty = { "NameArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, NameArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty_Inner = { "BoolArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty = { "BoolArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, BoolArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty_Inner = { "StringArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty = { "StringArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, StringArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty_Inner = { "TextArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty = { "TextArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, TextArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty_Inner = { "Vector2ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty = { "Vector2ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Vector2ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty_Inner = { "Vector3ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty = { "Vector3ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Vector3ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty_Inner = { "Vector4ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty = { "Vector4ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, Vector4ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty_Inner = { "LinearColorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty = { "LinearColorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, LinearColorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty_Inner = { "RotatorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty = { "RotatorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, RotatorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty_Inner = { "ColorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty = { "ColorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, ColorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty_Inner = { "ObjectArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty = { "ObjectArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, ObjectArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_Inner_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_Inner = { "EnumArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_USDImporter_EUsdTestEnum, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_MetaData[] = {
		{ "Category", "Component|ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty = { "EnumArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdTestComponent, EnumArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int8ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int16ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int32ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Int64ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ByteArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt16ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt32ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_UnsignedInt64ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_FloatArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_DoubleArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_NameArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_BoolArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_StringArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_TextArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector2ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector3ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_Vector4ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_LinearColorArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_RotatorArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ColorArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_ObjectArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_Inner_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::NewProp_EnumArrayProperty,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUsdTestComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::ClassParams = {
		&UDEPRECATED_UUsdTestComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::PropPointers),
		0,
		0x02A002A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUsdTestComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUsdTestComponent, 3216036104);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUsdTestComponent>()
	{
		return UDEPRECATED_UUsdTestComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUsdTestComponent(Z_Construct_UClass_UDEPRECATED_UUsdTestComponent, &UDEPRECATED_UUsdTestComponent::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUsdTestComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUsdTestComponent);
	void ADEPRECATED_AUsdTestActor::StaticRegisterNativesADEPRECATED_AUsdTestActor()
	{
	}
	UClass* Z_Construct_UClass_ADEPRECATED_AUsdTestActor_NoRegister()
	{
		return ADEPRECATED_AUsdTestActor::StaticClass();
	}
	struct Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int16Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Int16Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int64Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Int64Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt16Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_UnsignedInt16Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt32Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_UnsignedInt32Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt64Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_UnsignedInt64Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DoubleProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolProperty_MetaData[];
#endif
		static void NewProp_BoolProperty_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector2Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector2Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector3Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector3Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector4Property_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector4Property;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearColorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotatorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotatorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumProperty_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumProperty;
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int8ArrayProperty;
		static const UE4CodeGen_Private::FInt16PropertyParams NewProp_Int16ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int16ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int16ArrayProperty;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int32ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int32ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int32ArrayProperty;
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Int64ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int64ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Int64ArrayProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ByteArrayProperty;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_UnsignedInt16ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt16ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnsignedInt16ArrayProperty;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_UnsignedInt32ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt32ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnsignedInt32ArrayProperty;
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_UnsignedInt64ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnsignedInt64ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UnsignedInt64ArrayProperty;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FloatArrayProperty;
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_DoubleArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoubleArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DoubleArrayProperty;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NameArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NameArrayProperty;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoolArrayProperty;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StringArrayProperty;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TextArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector2ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector2ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vector2ArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector3ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector3ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vector3ArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vector4ArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vector4ArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vector4ArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearColorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LinearColorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LinearColorArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotatorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotatorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RotatorArrayProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ColorArrayProperty;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectArrayProperty;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EnumArrayProperty_Inner_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EnumArrayProperty_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnumArrayProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnumArrayProperty;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TestStructProperty_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TestStructProperty;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TestStructPropertyArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TestStructPropertyArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TestStructPropertyArray;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TestStructPropertyArray2_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TestStructPropertyArray2_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TestStructPropertyArray2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TestComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TestComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "UsdTestActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "Comment", "// Basic properties\n" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
		{ "ToolTip", "Basic properties" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8Property = { "Int8Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int8Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16Property = { "Int16Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int16Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32Property = { "Int32Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int32Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64Property = { "Int64Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int64Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteProperty = { "ByteProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, ByteProperty), nullptr, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16Property = { "UnsignedInt16Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, UnsignedInt16Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32Property = { "UnsignedInt32Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, UnsignedInt32Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64Property = { "UnsignedInt64Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, UnsignedInt64Property), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatProperty = { "FloatProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, FloatProperty), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleProperty = { "DoubleProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, DoubleProperty), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameProperty = { "NameProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, NameProperty), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	void Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty_SetBit(void* Obj)
	{
		((ADEPRECATED_AUsdTestActor*)Obj)->BoolProperty = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty = { "BoolProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADEPRECATED_AUsdTestActor), &Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringProperty = { "StringProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, StringProperty), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextProperty = { "TextProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, TextProperty), METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2Property = { "Vector2Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Vector2Property), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3Property = { "Vector3Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Vector3Property), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4Property_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4Property = { "Vector4Property", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Vector4Property), Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4Property_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4Property_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorProperty = { "LinearColorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, LinearColorProperty), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorProperty = { "RotatorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, RotatorProperty), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorProperty = { "ColorProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, ColorProperty), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectProperty = { "ObjectProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, ObjectProperty), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty_MetaData[] = {
		{ "Category", "BasicProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty = { "EnumProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, EnumProperty), Z_Construct_UEnum_USDImporter_EUsdTestEnum, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty_MetaData)) };
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty_Inner = { "Int8ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "Comment", "// Arrays\n" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
		{ "ToolTip", "Arrays" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty = { "Int8ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int8ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FInt16PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty_Inner = { "Int16ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty = { "Int16ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int16ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty_Inner = { "Int32ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty = { "Int32ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int32ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty_Inner = { "Int64ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty = { "Int64ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Int64ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty_Inner = { "ByteArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty = { "ByteArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, ByteArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty_Inner = { "UnsignedInt16ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty = { "UnsignedInt16ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, UnsignedInt16ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty_Inner = { "UnsignedInt32ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty = { "UnsignedInt32ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, UnsignedInt32ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty_Inner = { "UnsignedInt64ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty = { "UnsignedInt64ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, UnsignedInt64ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty_Inner = { "FloatArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty = { "FloatArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, FloatArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty_Inner = { "DoubleArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty = { "DoubleArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, DoubleArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty_Inner = { "NameArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty = { "NameArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, NameArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty_Inner = { "BoolArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty = { "BoolArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, BoolArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty_Inner = { "StringArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty = { "StringArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, StringArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty_Inner = { "TextArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty = { "TextArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, TextArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty_Inner = { "Vector2ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty = { "Vector2ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Vector2ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty_Inner = { "Vector3ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty = { "Vector3ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Vector3ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty_Inner = { "Vector4ArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector4, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty = { "Vector4ArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, Vector4ArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty_Inner = { "LinearColorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty = { "LinearColorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, LinearColorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty_Inner = { "RotatorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty = { "RotatorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, RotatorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty_Inner = { "ColorArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty = { "ColorArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, ColorArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty_Inner = { "ObjectArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty = { "ObjectArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, ObjectArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_Inner_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_Inner = { "EnumArrayProperty", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_USDImporter_EUsdTestEnum, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_MetaData[] = {
		{ "Category", "ArrayProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty = { "EnumArrayProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, EnumArrayProperty), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructProperty_MetaData[] = {
		{ "Category", "StructProperties" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructProperty = { "TestStructProperty", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, TestStructProperty), Z_Construct_UScriptStruct_FUsdTestStruct, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructProperty_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructProperty_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray_Inner = { "TestStructPropertyArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUsdTestStruct, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray_MetaData[] = {
		{ "Category", "ArrayDeepNesting" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray = { "TestStructPropertyArray", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, TestStructPropertyArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2_Inner = { "TestStructPropertyArray2", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FUsdTestStruct, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2_MetaData[] = {
		{ "Category", "ArrayDeepNesting" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2 = { "TestStructPropertyArray2", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, TestStructPropertyArray2), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestComponent_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use the new USDStageImporter module instead" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/UsdTestActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestComponent = { "TestComponent", nullptr, (EPropertyFlags)0x0010000020080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADEPRECATED_AUsdTestActor, TestComponent_DEPRECATED), Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4Property,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int8ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int16ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int32ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Int64ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ByteArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt16ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt32ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_UnsignedInt64ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_FloatArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_DoubleArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_NameArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_BoolArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_StringArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TextArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector2ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector3ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_Vector4ArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_LinearColorArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_RotatorArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ColorArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_ObjectArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_Inner_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_EnumArrayProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructProperty,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestStructPropertyArray2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::NewProp_TestComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADEPRECATED_AUsdTestActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::ClassParams = {
		&ADEPRECATED_AUsdTestActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::PropPointers),
		0,
		0x028002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADEPRECATED_AUsdTestActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADEPRECATED_AUsdTestActor, 2815291357);
	template<> USDIMPORTER_API UClass* StaticClass<ADEPRECATED_AUsdTestActor>()
	{
		return ADEPRECATED_AUsdTestActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADEPRECATED_AUsdTestActor(Z_Construct_UClass_ADEPRECATED_AUsdTestActor, &ADEPRECATED_AUsdTestActor::StaticClass, TEXT("/Script/USDImporter"), TEXT("ADEPRECATED_AUsdTestActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADEPRECATED_AUsdTestActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
