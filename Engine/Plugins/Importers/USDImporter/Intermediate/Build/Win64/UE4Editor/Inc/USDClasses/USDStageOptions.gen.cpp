// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDClasses/Public/USDStageOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDStageOptions() {}
// Cross Module References
	USDCLASSES_API UEnum* Z_Construct_UEnum_USDClasses_EUsdUpAxis();
	UPackage* Z_Construct_UPackage__Script_USDClasses();
	USDCLASSES_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions();
// End Cross Module References
	static UEnum* EUsdUpAxis_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDClasses_EUsdUpAxis, Z_Construct_UPackage__Script_USDClasses(), TEXT("EUsdUpAxis"));
		}
		return Singleton;
	}
	template<> USDCLASSES_API UEnum* StaticEnum<EUsdUpAxis>()
	{
		return EUsdUpAxis_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUsdUpAxis(EUsdUpAxis_StaticEnum, TEXT("/Script/USDClasses"), TEXT("EUsdUpAxis"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDClasses_EUsdUpAxis_Hash() { return 1276216961U; }
	UEnum* Z_Construct_UEnum_USDClasses_EUsdUpAxis()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDClasses();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUsdUpAxis"), 0, Get_Z_Construct_UEnum_USDClasses_EUsdUpAxis_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUsdUpAxis::YAxis", (int64)EUsdUpAxis::YAxis },
				{ "EUsdUpAxis::ZAxis", (int64)EUsdUpAxis::ZAxis },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/USDStageOptions.h" },
				{ "YAxis.Name", "EUsdUpAxis::YAxis" },
				{ "ZAxis.Name", "EUsdUpAxis::ZAxis" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDClasses,
				nullptr,
				"EUsdUpAxis",
				"EUsdUpAxis",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FUsdStageOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDCLASSES_API uint32 Get_Z_Construct_UScriptStruct_FUsdStageOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUsdStageOptions, Z_Construct_UPackage__Script_USDClasses(), TEXT("UsdStageOptions"), sizeof(FUsdStageOptions), Get_Z_Construct_UScriptStruct_FUsdStageOptions_Hash());
	}
	return Singleton;
}
template<> USDCLASSES_API UScriptStruct* StaticStruct<FUsdStageOptions>()
{
	return FUsdStageOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUsdStageOptions(FUsdStageOptions::StaticStruct, TEXT("/Script/USDClasses"), TEXT("UsdStageOptions"), false, nullptr, nullptr);
static struct FScriptStruct_USDClasses_StaticRegisterNativesFUsdStageOptions
{
	FScriptStruct_USDClasses_StaticRegisterNativesFUsdStageOptions()
	{
		UScriptStruct::DeferCppStructOps<FUsdStageOptions>(FName(TEXT("UsdStageOptions")));
	}
} ScriptStruct_USDClasses_StaticRegisterNativesFUsdStageOptions;
	struct Z_Construct_UScriptStruct_FUsdStageOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetersPerUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MetersPerUnit;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_UpAxis_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpAxis_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_UpAxis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/USDStageOptions.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUsdStageOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_MetersPerUnit_MetaData[] = {
		{ "Category", "Stage options" },
		{ "Comment", "/** MetersPerUnit to use for the stage. Defaults to 0.01 (i.e. 1cm per unit, which equals UE units) */" },
		{ "ModuleRelativePath", "Public/USDStageOptions.h" },
		{ "ToolTip", "MetersPerUnit to use for the stage. Defaults to 0.01 (i.e. 1cm per unit, which equals UE units)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_MetersPerUnit = { "MetersPerUnit", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageOptions, MetersPerUnit), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_MetersPerUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_MetersPerUnit_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis_MetaData[] = {
		{ "Category", "Stage options" },
		{ "Comment", "/** UpAxis to use for the stage. Defaults to ZAxis, which equals the UE convention */" },
		{ "ModuleRelativePath", "Public/USDStageOptions.h" },
		{ "ToolTip", "UpAxis to use for the stage. Defaults to ZAxis, which equals the UE convention" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis = { "UpAxis", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageOptions, UpAxis), Z_Construct_UEnum_USDClasses_EUsdUpAxis, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUsdStageOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_MetersPerUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageOptions_Statics::NewProp_UpAxis,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUsdStageOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDClasses,
		nullptr,
		&NewStructOps,
		"UsdStageOptions",
		sizeof(FUsdStageOptions),
		alignof(FUsdStageOptions),
		Z_Construct_UScriptStruct_FUsdStageOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUsdStageOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDClasses();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UsdStageOptions"), sizeof(FUsdStageOptions), Get_Z_Construct_UScriptStruct_FUsdStageOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUsdStageOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUsdStageOptions_Hash() { return 873196177U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
