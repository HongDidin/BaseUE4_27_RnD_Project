// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealUSDWrapper/Public/UnrealUSDWrapper.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnrealUSDWrapper() {}
// Cross Module References
	UNREALUSDWRAPPER_API UEnum* Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet();
	UPackage* Z_Construct_UPackage__Script_UnrealUSDWrapper();
	UNREALUSDWRAPPER_API UEnum* Z_Construct_UEnum_UnrealUSDWrapper_EUsdPurpose();
// End Cross Module References
	static UEnum* EUsdInitialLoadSet_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet, Z_Construct_UPackage__Script_UnrealUSDWrapper(), TEXT("EUsdInitialLoadSet"));
		}
		return Singleton;
	}
	template<> UNREALUSDWRAPPER_API UEnum* StaticEnum<EUsdInitialLoadSet>()
	{
		return EUsdInitialLoadSet_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUsdInitialLoadSet(EUsdInitialLoadSet_StaticEnum, TEXT("/Script/UnrealUSDWrapper"), TEXT("EUsdInitialLoadSet"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet_Hash() { return 3290082243U; }
	UEnum* Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_UnrealUSDWrapper();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUsdInitialLoadSet"), 0, Get_Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUsdInitialLoadSet::LoadAll", (int64)EUsdInitialLoadSet::LoadAll },
				{ "EUsdInitialLoadSet::LoadNone", (int64)EUsdInitialLoadSet::LoadNone },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "LoadAll.Name", "EUsdInitialLoadSet::LoadAll" },
				{ "LoadNone.Name", "EUsdInitialLoadSet::LoadNone" },
				{ "ModuleRelativePath", "Public/UnrealUSDWrapper.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_UnrealUSDWrapper,
				nullptr,
				"EUsdInitialLoadSet",
				"EUsdInitialLoadSet",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EUsdPurpose_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_UnrealUSDWrapper_EUsdPurpose, Z_Construct_UPackage__Script_UnrealUSDWrapper(), TEXT("EUsdPurpose"));
		}
		return Singleton;
	}
	template<> UNREALUSDWRAPPER_API UEnum* StaticEnum<EUsdPurpose>()
	{
		return EUsdPurpose_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUsdPurpose(EUsdPurpose_StaticEnum, TEXT("/Script/UnrealUSDWrapper"), TEXT("EUsdPurpose"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_UnrealUSDWrapper_EUsdPurpose_Hash() { return 1666980311U; }
	UEnum* Z_Construct_UEnum_UnrealUSDWrapper_EUsdPurpose()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_UnrealUSDWrapper();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUsdPurpose"), 0, Get_Z_Construct_UEnum_UnrealUSDWrapper_EUsdPurpose_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUsdPurpose::Default", (int64)EUsdPurpose::Default },
				{ "EUsdPurpose::Proxy", (int64)EUsdPurpose::Proxy },
				{ "EUsdPurpose::Render", (int64)EUsdPurpose::Render },
				{ "EUsdPurpose::Guide", (int64)EUsdPurpose::Guide },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Bitflags", "" },
				{ "Default.Hidden", "" },
				{ "Default.Name", "EUsdPurpose::Default" },
				{ "Guide.Name", "EUsdPurpose::Guide" },
				{ "ModuleRelativePath", "Public/UnrealUSDWrapper.h" },
				{ "Proxy.Name", "EUsdPurpose::Proxy" },
				{ "Render.Name", "EUsdPurpose::Render" },
				{ "UseEnumValuesAsMaskValuesInEditor", "true" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_UnrealUSDWrapper,
				nullptr,
				"EUsdPurpose",
				"EUsdPurpose",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
