// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDAssetImportFactory_generated_h
#error "USDAssetImportFactory.generated.h already included, missing '#pragma once' in USDAssetImportFactory.h"
#endif
#define USDIMPORTER_USDAssetImportFactory_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUSDAssetImportContext_Statics; \
	USDIMPORTER_API static class UScriptStruct* StaticStruct(); \
	typedef FUsdImportContext Super;


template<> USDIMPORTER_API UScriptStruct* StaticStruct<struct FUSDAssetImportContext>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDAssetImportFactory(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDAssetImportFactory, UFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDAssetImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDAssetImportFactory(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDAssetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDAssetImportFactory, UFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDAssetImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDAssetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDAssetImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDAssetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDAssetImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDAssetImportFactory(UDEPRECATED_UUSDAssetImportFactory&&); \
	NO_API UDEPRECATED_UUSDAssetImportFactory(const UDEPRECATED_UUSDAssetImportFactory&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDAssetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDAssetImportFactory(UDEPRECATED_UUSDAssetImportFactory&&); \
	NO_API UDEPRECATED_UUSDAssetImportFactory(const UDEPRECATED_UUSDAssetImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDAssetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDAssetImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDAssetImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImportContext() { return STRUCT_OFFSET(UDEPRECATED_UUSDAssetImportFactory, ImportContext); } \
	FORCEINLINE static uint32 __PPO__ImportOptions_DEPRECATED() { return STRUCT_OFFSET(UDEPRECATED_UUSDAssetImportFactory, ImportOptions_DEPRECATED); }


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_22_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDAssetImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDAssetImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDAssetImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
