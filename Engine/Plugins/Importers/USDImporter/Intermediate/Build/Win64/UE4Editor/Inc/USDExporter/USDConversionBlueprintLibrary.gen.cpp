// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/USDConversionBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDConversionBlueprintLibrary() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UUsdConversionBlueprintLibrary_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UUsdConversionBlueprintLibrary();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UUsdConversionBlueprintLibrary::execAddPayload)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ReferencingStagePath);
		P_GET_PROPERTY(FStrProperty,Z_Param_ReferencingPrimPath);
		P_GET_PROPERTY(FStrProperty,Z_Param_TargetStagePath);
		P_FINISH;
		P_NATIVE_BEGIN;
		UUsdConversionBlueprintLibrary::AddPayload(Z_Param_ReferencingStagePath,Z_Param_ReferencingPrimPath,Z_Param_TargetStagePath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUsdConversionBlueprintLibrary::execInsertSubLayer)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ParentLayerPath);
		P_GET_PROPERTY(FStrProperty,Z_Param_SubLayerPath);
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		UUsdConversionBlueprintLibrary::InsertSubLayer(Z_Param_ParentLayerPath,Z_Param_SubLayerPath,Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUsdConversionBlueprintLibrary::execMakePathRelativeToLayer)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AnchorLayerPath);
		P_GET_PROPERTY(FStrProperty,Z_Param_PathToMakeRelative);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UUsdConversionBlueprintLibrary::MakePathRelativeToLayer(Z_Param_AnchorLayerPath,Z_Param_PathToMakeRelative);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUsdConversionBlueprintLibrary::execGetActorsToConvert)
	{
		P_GET_OBJECT(UWorld,Z_Param_World);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSet<AActor*>*)Z_Param__Result=UUsdConversionBlueprintLibrary::GetActorsToConvert(Z_Param_World);
		P_NATIVE_END;
	}
	void UUsdConversionBlueprintLibrary::StaticRegisterNativesUUsdConversionBlueprintLibrary()
	{
		UClass* Class = UUsdConversionBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddPayload", &UUsdConversionBlueprintLibrary::execAddPayload },
			{ "GetActorsToConvert", &UUsdConversionBlueprintLibrary::execGetActorsToConvert },
			{ "InsertSubLayer", &UUsdConversionBlueprintLibrary::execInsertSubLayer },
			{ "MakePathRelativeToLayer", &UUsdConversionBlueprintLibrary::execMakePathRelativeToLayer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics
	{
		struct UsdConversionBlueprintLibrary_eventAddPayload_Parms
		{
			FString ReferencingStagePath;
			FString ReferencingPrimPath;
			FString TargetStagePath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferencingStagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReferencingStagePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferencingPrimPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReferencingPrimPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetStagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TargetStagePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingStagePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingStagePath = { "ReferencingStagePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventAddPayload_Parms, ReferencingStagePath), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingStagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingStagePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingPrimPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingPrimPath = { "ReferencingPrimPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventAddPayload_Parms, ReferencingPrimPath), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingPrimPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingPrimPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_TargetStagePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_TargetStagePath = { "TargetStagePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventAddPayload_Parms, TargetStagePath), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_TargetStagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_TargetStagePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingStagePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_ReferencingPrimPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::NewProp_TargetStagePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::Function_MetaDataParams[] = {
		{ "Category", "Layer utils" },
		{ "ModuleRelativePath", "Public/USDConversionBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdConversionBlueprintLibrary, nullptr, "AddPayload", nullptr, nullptr, sizeof(UsdConversionBlueprintLibrary_eventAddPayload_Parms), Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics
	{
		struct UsdConversionBlueprintLibrary_eventGetActorsToConvert_Parms
		{
			UWorld* World;
			TSet<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_ElementProp;
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventGetActorsToConvert_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::NewProp_ReturnValue_ElementProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventGetActorsToConvert_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::NewProp_ReturnValue_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::Function_MetaDataParams[] = {
		{ "Category", "World utils" },
		{ "ModuleRelativePath", "Public/USDConversionBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdConversionBlueprintLibrary, nullptr, "GetActorsToConvert", nullptr, nullptr, sizeof(UsdConversionBlueprintLibrary_eventGetActorsToConvert_Parms), Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics
	{
		struct UsdConversionBlueprintLibrary_eventInsertSubLayer_Parms
		{
			FString ParentLayerPath;
			FString SubLayerPath;
			int32 Index;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentLayerPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ParentLayerPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubLayerPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SubLayerPath;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_ParentLayerPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_ParentLayerPath = { "ParentLayerPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventInsertSubLayer_Parms, ParentLayerPath), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_ParentLayerPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_ParentLayerPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_SubLayerPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_SubLayerPath = { "SubLayerPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventInsertSubLayer_Parms, SubLayerPath), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_SubLayerPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_SubLayerPath_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventInsertSubLayer_Parms, Index), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_ParentLayerPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_SubLayerPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::NewProp_Index,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::Function_MetaDataParams[] = {
		{ "Category", "Layer utils" },
		{ "CPP_Default_Index", "-1" },
		{ "ModuleRelativePath", "Public/USDConversionBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdConversionBlueprintLibrary, nullptr, "InsertSubLayer", nullptr, nullptr, sizeof(UsdConversionBlueprintLibrary_eventInsertSubLayer_Parms), Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics
	{
		struct UsdConversionBlueprintLibrary_eventMakePathRelativeToLayer_Parms
		{
			FString AnchorLayerPath;
			FString PathToMakeRelative;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnchorLayerPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AnchorLayerPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathToMakeRelative_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PathToMakeRelative;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_AnchorLayerPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_AnchorLayerPath = { "AnchorLayerPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventMakePathRelativeToLayer_Parms, AnchorLayerPath), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_AnchorLayerPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_AnchorLayerPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_PathToMakeRelative_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_PathToMakeRelative = { "PathToMakeRelative", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventMakePathRelativeToLayer_Parms, PathToMakeRelative), METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_PathToMakeRelative_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_PathToMakeRelative_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdConversionBlueprintLibrary_eventMakePathRelativeToLayer_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_AnchorLayerPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_PathToMakeRelative,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::Function_MetaDataParams[] = {
		{ "Category", "Layer utils" },
		{ "ModuleRelativePath", "Public/USDConversionBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdConversionBlueprintLibrary, nullptr, "MakePathRelativeToLayer", nullptr, nullptr, sizeof(UsdConversionBlueprintLibrary_eventMakePathRelativeToLayer_Parms), Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUsdConversionBlueprintLibrary_NoRegister()
	{
		return UUsdConversionBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUsdConversionBlueprintLibrary_AddPayload, "AddPayload" }, // 1115083853
		{ &Z_Construct_UFunction_UUsdConversionBlueprintLibrary_GetActorsToConvert, "GetActorsToConvert" }, // 279896884
		{ &Z_Construct_UFunction_UUsdConversionBlueprintLibrary_InsertSubLayer, "InsertSubLayer" }, // 1453936648
		{ &Z_Construct_UFunction_UUsdConversionBlueprintLibrary_MakePathRelativeToLayer, "MakePathRelativeToLayer" }, // 3364284473
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Wrapped static conversion functions from the UsdUtilities module, so that they can be used via scripting */" },
		{ "IncludePath", "USDConversionBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Public/USDConversionBlueprintLibrary.h" },
		{ "ScriptName", "UsdConversionLibrary" },
		{ "ToolTip", "Wrapped static conversion functions from the UsdUtilities module, so that they can be used via scripting" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdConversionBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::ClassParams = {
		&UUsdConversionBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdConversionBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdConversionBlueprintLibrary, 1722888153);
	template<> USDEXPORTER_API UClass* StaticClass<UUsdConversionBlueprintLibrary>()
	{
		return UUsdConversionBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdConversionBlueprintLibrary(Z_Construct_UClass_UUsdConversionBlueprintLibrary, &UUsdConversionBlueprintLibrary::StaticClass, TEXT("/Script/USDExporter"), TEXT("UUsdConversionBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdConversionBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
