// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheUSD/Public/GeometryCacheUSDComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCacheUSDComponent() {}
// Cross Module References
	GEOMETRYCACHEUSD_API UClass* Z_Construct_UClass_UGeometryCacheUsdComponent_NoRegister();
	GEOMETRYCACHEUSD_API UClass* Z_Construct_UClass_UGeometryCacheUsdComponent();
	GEOMETRYCACHE_API UClass* Z_Construct_UClass_UGeometryCacheComponent();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheUSD();
// End Cross Module References
	void UGeometryCacheUsdComponent::StaticRegisterNativesUGeometryCacheUsdComponent()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCacheUsdComponent_NoRegister()
	{
		return UGeometryCacheUsdComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCacheUsdComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGeometryCacheComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheUSD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering Experimental" },
		{ "Comment", "/** GeometryCacheUSDComponent, encapsulates a transient GeometryCache asset instance that fetches its data from a USD file and implements functionality for rendering and playback */" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "USD Geometry Cache" },
		{ "HideCategories", "Object LOD Mobility Trigger" },
		{ "IncludePath", "GeometryCacheUSDComponent.h" },
		{ "ModuleRelativePath", "Public/GeometryCacheUSDComponent.h" },
		{ "ToolTip", "GeometryCacheUSDComponent, encapsulates a transient GeometryCache asset instance that fetches its data from a USD file and implements functionality for rendering and playback" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCacheUsdComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::ClassParams = {
		&UGeometryCacheUsdComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x04B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCacheUsdComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCacheUsdComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCacheUsdComponent, 3921443209);
	template<> GEOMETRYCACHEUSD_API UClass* StaticClass<UGeometryCacheUsdComponent>()
	{
		return UGeometryCacheUsdComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCacheUsdComponent(Z_Construct_UClass_UGeometryCacheUsdComponent, &UGeometryCacheUsdComponent::StaticClass, TEXT("/Script/GeometryCacheUSD"), TEXT("UGeometryCacheUsdComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCacheUsdComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
