// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/MaterialExporterUSDOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMaterialExporterUSDOptions() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UMaterialExporterUSDOptions_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UMaterialExporterUSDOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	MATERIALBAKING_API UScriptStruct* Z_Construct_UScriptStruct_FPropertyEntry();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UMaterialExporterUSDOptions::StaticRegisterNativesUMaterialExporterUSDOptions()
	{
	}
	UClass* Z_Construct_UClass_UMaterialExporterUSDOptions_NoRegister()
	{
		return UMaterialExporterUSDOptions::StaticClass();
	}
	struct Z_Construct_UClass_UMaterialExporterUSDOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Properties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Properties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTextureSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultTextureSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TexturesDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TexturesDir;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Options for exporting materials to USD format.\n * We use a dedicated object instead of reusing the MaterialBaking module as automated export tasks\n * can only have one options object, and we need to also provide the textures directory.\n */" },
		{ "IncludePath", "MaterialExporterUSDOptions.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MaterialExporterUSDOptions.h" },
		{ "ToolTip", "Options for exporting materials to USD format.\nWe use a dedicated object instead of reusing the MaterialBaking module as automated export tasks\ncan only have one options object, and we need to also provide the textures directory." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties_Inner = { "Properties", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPropertyEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties_MetaData[] = {
		{ "Category", "MaterialBakeSettings" },
		{ "Comment", "/** Properties which are supposed to be baked out for the material */" },
		{ "ExposeOnSpawn", "" },
		{ "ModuleRelativePath", "Public/MaterialExporterUSDOptions.h" },
		{ "ToolTip", "Properties which are supposed to be baked out for the material" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0011000000004005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMaterialExporterUSDOptions, Properties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_DefaultTextureSize_MetaData[] = {
		{ "Category", "MaterialBakeSettings" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Size of the baked texture for all properties that don't have a CustomSize set */" },
		{ "ExposeOnSpawn", "" },
		{ "ModuleRelativePath", "Public/MaterialExporterUSDOptions.h" },
		{ "ToolTip", "Size of the baked texture for all properties that don't have a CustomSize set" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_DefaultTextureSize = { "DefaultTextureSize", nullptr, (EPropertyFlags)0x0011000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMaterialExporterUSDOptions, DefaultTextureSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_DefaultTextureSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_DefaultTextureSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_TexturesDir_MetaData[] = {
		{ "Category", "Textures" },
		{ "Comment", "/** Where baked textures are placed */" },
		{ "ExposeOnSpawn", "" },
		{ "ModuleRelativePath", "Public/MaterialExporterUSDOptions.h" },
		{ "ToolTip", "Where baked textures are placed" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_TexturesDir = { "TexturesDir", nullptr, (EPropertyFlags)0x0011000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMaterialExporterUSDOptions, TexturesDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_TexturesDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_TexturesDir_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_Properties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_DefaultTextureSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::NewProp_TexturesDir,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMaterialExporterUSDOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::ClassParams = {
		&UMaterialExporterUSDOptions::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMaterialExporterUSDOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMaterialExporterUSDOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMaterialExporterUSDOptions, 2022811060);
	template<> USDEXPORTER_API UClass* StaticClass<UMaterialExporterUSDOptions>()
	{
		return UMaterialExporterUSDOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMaterialExporterUSDOptions(Z_Construct_UClass_UMaterialExporterUSDOptions, &UMaterialExporterUSDOptions::StaticClass, TEXT("/Script/USDExporter"), TEXT("UMaterialExporterUSDOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMaterialExporterUSDOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
