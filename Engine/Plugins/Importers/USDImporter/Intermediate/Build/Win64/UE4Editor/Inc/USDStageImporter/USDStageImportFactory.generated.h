// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDSTAGEIMPORTER_USDStageImportFactory_generated_h
#error "USDStageImportFactory.generated.h already included, missing '#pragma once' in USDStageImportFactory.h"
#endif
#define USDSTAGEIMPORTER_USDStageImportFactory_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdStageImportFactory(); \
	friend struct Z_Construct_UClass_UUsdStageImportFactory_Statics; \
public: \
	DECLARE_CLASS(UUsdStageImportFactory, USceneImportFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDStageImporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUUsdStageImportFactory(); \
	friend struct Z_Construct_UClass_UUsdStageImportFactory_Statics; \
public: \
	DECLARE_CLASS(UUsdStageImportFactory, USceneImportFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDStageImporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageImportFactory(UUsdStageImportFactory&&); \
	NO_API UUsdStageImportFactory(const UUsdStageImportFactory&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageImportFactory(UUsdStageImportFactory&&); \
	NO_API UUsdStageImportFactory(const UUsdStageImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImportContext() { return STRUCT_OFFSET(UUsdStageImportFactory, ImportContext); }


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_18_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UsdStageImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGEIMPORTER_API UClass* StaticClass<class UUsdStageImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
