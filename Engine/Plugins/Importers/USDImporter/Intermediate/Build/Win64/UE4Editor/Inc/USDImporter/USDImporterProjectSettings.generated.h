// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDImporterProjectSettings_generated_h
#error "USDImporterProjectSettings.generated.h already included, missing '#pragma once' in USDImporterProjectSettings.h"
#endif
#define USDIMPORTER_USDImporterProjectSettings_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDImporterProjectSettings(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDImporterProjectSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDImporterProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDImporterProjectSettings(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDImporterProjectSettings_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDImporterProjectSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDImporterProjectSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDImporterProjectSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDImporterProjectSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDImporterProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDImporterProjectSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDImporterProjectSettings(UDEPRECATED_UUSDImporterProjectSettings&&); \
	NO_API UDEPRECATED_UUSDImporterProjectSettings(const UDEPRECATED_UUSDImporterProjectSettings&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDImporterProjectSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDImporterProjectSettings(UDEPRECATED_UUSDImporterProjectSettings&&); \
	NO_API UDEPRECATED_UUSDImporterProjectSettings(const UDEPRECATED_UUSDImporterProjectSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDImporterProjectSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDImporterProjectSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDImporterProjectSettings)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_13_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDImporterProjectSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporterProjectSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
