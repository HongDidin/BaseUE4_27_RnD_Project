// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDSTAGEIMPORTER_USDStageAssetImportFactory_generated_h
#error "USDStageAssetImportFactory.generated.h already included, missing '#pragma once' in USDStageAssetImportFactory.h"
#endif
#define USDSTAGEIMPORTER_USDStageAssetImportFactory_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdStageAssetImportFactory(); \
	friend struct Z_Construct_UClass_UUsdStageAssetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UUsdStageAssetImportFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDStageImporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageAssetImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUUsdStageAssetImportFactory(); \
	friend struct Z_Construct_UClass_UUsdStageAssetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UUsdStageAssetImportFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDStageImporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageAssetImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageAssetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageAssetImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageAssetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageAssetImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageAssetImportFactory(UUsdStageAssetImportFactory&&); \
	NO_API UUsdStageAssetImportFactory(const UUsdStageAssetImportFactory&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageAssetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageAssetImportFactory(UUsdStageAssetImportFactory&&); \
	NO_API UUsdStageAssetImportFactory(const UUsdStageAssetImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageAssetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageAssetImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageAssetImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImportContext() { return STRUCT_OFFSET(UUsdStageAssetImportFactory, ImportContext); }


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_15_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UsdStageAssetImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGEIMPORTER_API UClass* StaticClass<class UUsdStageAssetImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Private_USDStageAssetImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
