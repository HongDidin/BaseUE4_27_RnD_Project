// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_UsdTestActor_generated_h
#error "UsdTestActor.generated.h already included, missing '#pragma once' in UsdTestActor.h"
#endif
#define USDIMPORTER_UsdTestActor_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_34_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUsdTestStruct_Statics; \
	USDIMPORTER_API static class UScriptStruct* StaticStruct();


template<> USDIMPORTER_API UScriptStruct* StaticStruct<struct FUsdTestStruct>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUsdTestStruct2_Statics; \
	USDIMPORTER_API static class UScriptStruct* StaticStruct();


template<> USDIMPORTER_API UScriptStruct* StaticStruct<struct FUsdTestStruct2>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUsdTestComponent(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUsdTestComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUsdTestComponent)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUsdTestComponent(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUsdTestComponent_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUsdTestComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUsdTestComponent)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUsdTestComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUsdTestComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUsdTestComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUsdTestComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUsdTestComponent(UDEPRECATED_UUsdTestComponent&&); \
	NO_API UDEPRECATED_UUsdTestComponent(const UDEPRECATED_UUsdTestComponent&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUsdTestComponent(UDEPRECATED_UUsdTestComponent&&); \
	NO_API UDEPRECATED_UUsdTestComponent(const UDEPRECATED_UUsdTestComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUsdTestComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUsdTestComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDEPRECATED_UUsdTestComponent)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_52_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_55_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUsdTestComponent>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADEPRECATED_AUsdTestActor(); \
	friend struct Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics; \
public: \
	DECLARE_CLASS(ADEPRECATED_AUsdTestActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(ADEPRECATED_AUsdTestActor)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_INCLASS \
private: \
	static void StaticRegisterNativesADEPRECATED_AUsdTestActor(); \
	friend struct Z_Construct_UClass_ADEPRECATED_AUsdTestActor_Statics; \
public: \
	DECLARE_CLASS(ADEPRECATED_AUsdTestActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(ADEPRECATED_AUsdTestActor)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADEPRECATED_AUsdTestActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADEPRECATED_AUsdTestActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADEPRECATED_AUsdTestActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADEPRECATED_AUsdTestActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADEPRECATED_AUsdTestActor(ADEPRECATED_AUsdTestActor&&); \
	NO_API ADEPRECATED_AUsdTestActor(const ADEPRECATED_AUsdTestActor&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADEPRECATED_AUsdTestActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADEPRECATED_AUsdTestActor(ADEPRECATED_AUsdTestActor&&); \
	NO_API ADEPRECATED_AUsdTestActor(const ADEPRECATED_AUsdTestActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADEPRECATED_AUsdTestActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADEPRECATED_AUsdTestActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADEPRECATED_AUsdTestActor)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_197_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h_200_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AUsdTestActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class ADEPRECATED_AUsdTestActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_UsdTestActor_h


#define FOREACH_ENUM_EUSDTESTENUM(op) \
	op(EUsdTestEnum::EnumVal1) \
	op(EUsdTestEnum::EnumVal2) \
	op(EUsdTestEnum::EnumVal3) \
	op(EUsdTestEnum::EnumVal4) \
	op(EUsdTestEnum::EnumVal5) 

enum class EUsdTestEnum : uint8;
template<> USDIMPORTER_API UEnum* StaticEnum<EUsdTestEnum>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
