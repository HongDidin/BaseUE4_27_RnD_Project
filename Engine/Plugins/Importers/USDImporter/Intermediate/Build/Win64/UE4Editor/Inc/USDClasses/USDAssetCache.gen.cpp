// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDClasses/Public/USDAssetCache.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDAssetCache() {}
// Cross Module References
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdAssetCache_NoRegister();
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdAssetCache();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDClasses();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UUsdAssetCache::StaticRegisterNativesUUsdAssetCache()
	{
	}
	UClass* Z_Construct_UClass_UUsdAssetCache_NoRegister()
	{
		return UUsdAssetCache::StaticClass();
	}
	struct Z_Construct_UClass_UUsdAssetCache_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TransientStorage_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TransientStorage_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransientStorage_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_TransientStorage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PersistentStorage_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PersistentStorage_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PersistentStorage_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PersistentStorage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowPersistentStorage_MetaData[];
#endif
		static void NewProp_bAllowPersistentStorage_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowPersistentStorage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnedAssets_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnedAssets_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_OwnedAssets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PrimPathToAssets_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrimPathToAssets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimPathToAssets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PrimPathToAssets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdAssetCache_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDClasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetCache_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Owns the assets generated and reused by the USD stage, allowing thread-safe retrieval/storage */" },
		{ "IncludePath", "USDAssetCache.h" },
		{ "ModuleRelativePath", "Public/USDAssetCache.h" },
		{ "ToolTip", "Owns the assets generated and reused by the USD stage, allowing thread-safe retrieval/storage" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_ValueProp = { "TransientStorage", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_Key_KeyProp = { "TransientStorage_Key", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_MetaData[] = {
		{ "Category", "Assets" },
		{ "ModuleRelativePath", "Public/USDAssetCache.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage = { "TransientStorage", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdAssetCache, TransientStorage), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_ValueProp = { "PersistentStorage", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_Key_KeyProp = { "PersistentStorage_Key", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_MetaData[] = {
		{ "Category", "Assets" },
		{ "ModuleRelativePath", "Public/USDAssetCache.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage = { "PersistentStorage", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdAssetCache, PersistentStorage), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage_MetaData[] = {
		{ "Category", "Assets" },
		{ "ModuleRelativePath", "Public/USDAssetCache.h" },
	};
#endif
	void Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage_SetBit(void* Obj)
	{
		((UUsdAssetCache*)Obj)->bAllowPersistentStorage = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage = { "bAllowPersistentStorage", nullptr, (EPropertyFlags)0x0040040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdAssetCache), &Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets_ElementProp = { "OwnedAssets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets_MetaData[] = {
		{ "Comment", "// Points to the assets in primary storage, used to quickly check if we own an asset\n" },
		{ "ModuleRelativePath", "Public/USDAssetCache.h" },
		{ "ToolTip", "Points to the assets in primary storage, used to quickly check if we own an asset" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets = { "OwnedAssets", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdAssetCache, OwnedAssets), METADATA_PARAMS(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_ValueProp = { "PrimPathToAssets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_Key_KeyProp = { "PrimPathToAssets_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_MetaData[] = {
		{ "Comment", "// Keeps associations from prim paths to assets that we own in primary storage\n" },
		{ "ModuleRelativePath", "Public/USDAssetCache.h" },
		{ "ToolTip", "Keeps associations from prim paths to assets that we own in primary storage" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets = { "PrimPathToAssets", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdAssetCache, PrimPathToAssets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdAssetCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_TransientStorage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PersistentStorage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_bAllowPersistentStorage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_OwnedAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetCache_Statics::NewProp_PrimPathToAssets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdAssetCache_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdAssetCache>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdAssetCache_Statics::ClassParams = {
		&UUsdAssetCache::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdAssetCache_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdAssetCache_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetCache_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdAssetCache()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdAssetCache_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdAssetCache, 4238255531);
	template<> USDCLASSES_API UClass* StaticClass<UUsdAssetCache>()
	{
		return UUsdAssetCache::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdAssetCache(Z_Construct_UClass_UUsdAssetCache, &UUsdAssetCache::StaticClass, TEXT("/Script/USDClasses"), TEXT("UUsdAssetCache"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdAssetCache);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UUsdAssetCache)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
