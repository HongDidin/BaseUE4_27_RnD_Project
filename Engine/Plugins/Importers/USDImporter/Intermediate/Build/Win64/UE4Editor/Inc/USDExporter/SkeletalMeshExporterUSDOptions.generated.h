// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_SkeletalMeshExporterUSDOptions_generated_h
#error "SkeletalMeshExporterUSDOptions.generated.h already included, missing '#pragma once' in SkeletalMeshExporterUSDOptions.h"
#endif
#define USDEXPORTER_SkeletalMeshExporterUSDOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics; \
	static class UScriptStruct* StaticStruct();


template<> USDEXPORTER_API UScriptStruct* StaticStruct<struct FSkeletalMeshExporterUSDInnerOptions>();

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSkeletalMeshExporterUSDOptions(); \
	friend struct Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(USkeletalMeshExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(USkeletalMeshExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_INCLASS \
private: \
	static void StaticRegisterNativesUSkeletalMeshExporterUSDOptions(); \
	friend struct Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(USkeletalMeshExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(USkeletalMeshExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkeletalMeshExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkeletalMeshExporterUSDOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkeletalMeshExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkeletalMeshExporterUSDOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkeletalMeshExporterUSDOptions(USkeletalMeshExporterUSDOptions&&); \
	NO_API USkeletalMeshExporterUSDOptions(const USkeletalMeshExporterUSDOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USkeletalMeshExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USkeletalMeshExporterUSDOptions(USkeletalMeshExporterUSDOptions&&); \
	NO_API USkeletalMeshExporterUSDOptions(const USkeletalMeshExporterUSDOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USkeletalMeshExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USkeletalMeshExporterUSDOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USkeletalMeshExporterUSDOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_37_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h_40_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class USkeletalMeshExporterUSDOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_SkeletalMeshExporterUSDOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
