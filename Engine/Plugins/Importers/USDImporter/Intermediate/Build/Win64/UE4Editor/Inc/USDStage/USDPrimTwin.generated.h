// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDSTAGE_USDPrimTwin_generated_h
#error "USDPrimTwin.generated.h already included, missing '#pragma once' in USDPrimTwin.h"
#endif
#define USDSTAGE_USDPrimTwin_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdPrimTwin(); \
	friend struct Z_Construct_UClass_UUsdPrimTwin_Statics; \
public: \
	DECLARE_CLASS(UUsdPrimTwin, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDStage"), USDSTAGE_API) \
	DECLARE_SERIALIZER(UUsdPrimTwin)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUUsdPrimTwin(); \
	friend struct Z_Construct_UClass_UUsdPrimTwin_Statics; \
public: \
	DECLARE_CLASS(UUsdPrimTwin, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDStage"), USDSTAGE_API) \
	DECLARE_SERIALIZER(UUsdPrimTwin)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDSTAGE_API UUsdPrimTwin(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdPrimTwin) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDSTAGE_API, UUsdPrimTwin); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdPrimTwin); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDSTAGE_API UUsdPrimTwin(UUsdPrimTwin&&); \
	USDSTAGE_API UUsdPrimTwin(const UUsdPrimTwin&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDSTAGE_API UUsdPrimTwin(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDSTAGE_API UUsdPrimTwin(UUsdPrimTwin&&); \
	USDSTAGE_API UUsdPrimTwin(const UUsdPrimTwin&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDSTAGE_API, UUsdPrimTwin); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdPrimTwin); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdPrimTwin)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Children() { return STRUCT_OFFSET(UUsdPrimTwin, Children); } \
	FORCEINLINE static uint32 __PPO__Parent() { return STRUCT_OFFSET(UUsdPrimTwin, Parent); }


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_19_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGE_API UClass* StaticClass<class UUsdPrimTwin>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDPrimTwin_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
