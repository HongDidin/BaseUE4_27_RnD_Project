// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEUSD_GeometryCacheTrackUSD_generated_h
#error "GeometryCacheTrackUSD.generated.h already included, missing '#pragma once' in GeometryCacheTrackUSD.h"
#endif
#define GEOMETRYCACHEUSD_GeometryCacheTrackUSD_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGeometryCacheTrackUsd(); \
	friend struct Z_Construct_UClass_UGeometryCacheTrackUsd_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheTrackUsd, UGeometryCacheTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GeometryCacheUSD"), NO_API) \
	DECLARE_SERIALIZER(UGeometryCacheTrackUsd)


#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGeometryCacheTrackUsd(); \
	friend struct Z_Construct_UClass_UGeometryCacheTrackUsd_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheTrackUsd, UGeometryCacheTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GeometryCacheUSD"), NO_API) \
	DECLARE_SERIALIZER(UGeometryCacheTrackUsd)


#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGeometryCacheTrackUsd(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGeometryCacheTrackUsd) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGeometryCacheTrackUsd); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheTrackUsd); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGeometryCacheTrackUsd(UGeometryCacheTrackUsd&&); \
	NO_API UGeometryCacheTrackUsd(const UGeometryCacheTrackUsd&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGeometryCacheTrackUsd(UGeometryCacheTrackUsd&&); \
	NO_API UGeometryCacheTrackUsd(const UGeometryCacheTrackUsd&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGeometryCacheTrackUsd); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheTrackUsd); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGeometryCacheTrackUsd)


#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_14_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEUSD_API UClass* StaticClass<class UGeometryCacheTrackUsd>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_GeometryCacheUSD_Public_GeometryCacheTrackUSD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
