// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Public/USDImporter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDImporter() {}
// Cross Module References
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUSDSceneImportContext();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUsdImportContext();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporter_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporter();
// End Cross Module References

static_assert(std::is_polymorphic<FUSDSceneImportContext>() == std::is_polymorphic<FUsdImportContext>(), "USTRUCT FUSDSceneImportContext cannot be polymorphic unless super FUsdImportContext is polymorphic");

class UScriptStruct* FUSDSceneImportContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDIMPORTER_API uint32 Get_Z_Construct_UScriptStruct_FUSDSceneImportContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUSDSceneImportContext, Z_Construct_UPackage__Script_USDImporter(), TEXT("USDSceneImportContext"), sizeof(FUSDSceneImportContext), Get_Z_Construct_UScriptStruct_FUSDSceneImportContext_Hash());
	}
	return Singleton;
}
template<> USDIMPORTER_API UScriptStruct* StaticStruct<FUSDSceneImportContext>()
{
	return FUSDSceneImportContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUSDSceneImportContext(FUSDSceneImportContext::StaticStruct, TEXT("/Script/USDImporter"), TEXT("USDSceneImportContext"), false, nullptr, nullptr);
static struct FScriptStruct_USDImporter_StaticRegisterNativesFUSDSceneImportContext
{
	FScriptStruct_USDImporter_StaticRegisterNativesFUSDSceneImportContext()
	{
		UScriptStruct::DeferCppStructOps<FUSDSceneImportContext>(FName(TEXT("USDSceneImportContext")));
	}
} ScriptStruct_USDImporter_StaticRegisterNativesFUSDSceneImportContext;
	struct Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_World_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExistingActors_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ExistingActors_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistingActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ExistingActors;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ActorsToDestroy_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorsToDestroy_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorsToDestroy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EmptyActorFactory_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EmptyActorFactory;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UsedFactories_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_UsedFactories_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UsedFactories_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_UsedFactories;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUSDSceneImportContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_World_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUSDSceneImportContext, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_World_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_World_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_ValueProp = { "ExistingActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_Key_KeyProp = { "ExistingActors_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors = { "ExistingActors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUSDSceneImportContext, ExistingActors), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy_Inner = { "ActorsToDestroy", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy = { "ActorsToDestroy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUSDSceneImportContext, ActorsToDestroy), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_EmptyActorFactory_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_EmptyActorFactory = { "EmptyActorFactory", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUSDSceneImportContext, EmptyActorFactory), Z_Construct_UClass_UActorFactory_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_EmptyActorFactory_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_EmptyActorFactory_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_ValueProp = { "UsedFactories", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UActorFactory_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_Key_KeyProp = { "UsedFactories_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories = { "UsedFactories", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUSDSceneImportContext, UsedFactories), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ExistingActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_ActorsToDestroy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_EmptyActorFactory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::NewProp_UsedFactories,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
		Z_Construct_UScriptStruct_FUsdImportContext,
		&NewStructOps,
		"USDSceneImportContext",
		sizeof(FUSDSceneImportContext),
		alignof(FUSDSceneImportContext),
		Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUSDSceneImportContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUSDSceneImportContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("USDSceneImportContext"), sizeof(FUSDSceneImportContext), Get_Z_Construct_UScriptStruct_FUSDSceneImportContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUSDSceneImportContext_Hash() { return 2233745173U; }
class UScriptStruct* FUsdImportContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDIMPORTER_API uint32 Get_Z_Construct_UScriptStruct_FUsdImportContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUsdImportContext, Z_Construct_UPackage__Script_USDImporter(), TEXT("UsdImportContext"), sizeof(FUsdImportContext), Get_Z_Construct_UScriptStruct_FUsdImportContext_Hash());
	}
	return Singleton;
}
template<> USDIMPORTER_API UScriptStruct* StaticStruct<FUsdImportContext>()
{
	return FUsdImportContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUsdImportContext(FUsdImportContext::StaticStruct, TEXT("/Script/USDImporter"), TEXT("UsdImportContext"), false, nullptr, nullptr);
static struct FScriptStruct_USDImporter_StaticRegisterNativesFUsdImportContext
{
	FScriptStruct_USDImporter_StaticRegisterNativesFUsdImportContext()
	{
		UScriptStruct::DeferCppStructOps<FUsdImportContext>(FName(TEXT("UsdImportContext")));
	}
} ScriptStruct_USDImporter_StaticRegisterNativesFUsdImportContext;
	struct Z_Construct_UScriptStruct_FUsdImportContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportPathName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ImportPathName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimResolver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PrimResolver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdImportContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUsdImportContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_Parent_MetaData[] = {
		{ "Comment", "/** Parent package to import a single mesh to */" },
		{ "ModuleRelativePath", "Public/USDImporter.h" },
		{ "ToolTip", "Parent package to import a single mesh to" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdImportContext, Parent), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ObjectName_MetaData[] = {
		{ "Comment", "/** Name to use when importing a single mesh */" },
		{ "ModuleRelativePath", "Public/USDImporter.h" },
		{ "ToolTip", "Name to use when importing a single mesh" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ObjectName = { "ObjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdImportContext, ObjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ObjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ObjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportPathName_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportPathName = { "ImportPathName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdImportContext, ImportPathName), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportPathName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportPathName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportOptions_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use the new USDStageImporter module instead" },
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportOptions = { "ImportOptions", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdImportContext, ImportOptions_DEPRECATED), Z_Construct_UClass_UDEPRECATED_UUSDImportOptions_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_PrimResolver_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use the new USDStageImporter module instead" },
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_PrimResolver = { "PrimResolver", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdImportContext, PrimResolver_DEPRECATED), Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_PrimResolver_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_PrimResolver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUsdImportContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ObjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportPathName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_ImportOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdImportContext_Statics::NewProp_PrimResolver,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUsdImportContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
		nullptr,
		&NewStructOps,
		"UsdImportContext",
		sizeof(FUsdImportContext),
		alignof(FUsdImportContext),
		Z_Construct_UScriptStruct_FUsdImportContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdImportContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdImportContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUsdImportContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUsdImportContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDImporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UsdImportContext"), sizeof(FUsdImportContext), Get_Z_Construct_UScriptStruct_FUsdImportContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUsdImportContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUsdImportContext_Hash() { return 1396969232U; }
	void UDEPRECATED_UUsdSceneImportContextContainer::StaticRegisterNativesUDEPRECATED_UUsdSceneImportContextContainer()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_NoRegister()
	{
		return UDEPRECATED_UUsdSceneImportContextContainer::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImportContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Used to make ImportContext visible to the garbage collector so that it doesn't unload its references\n" },
		{ "IncludePath", "USDImporter.h" },
		{ "ModuleRelativePath", "Public/USDImporter.h" },
		{ "ToolTip", "Used to make ImportContext visible to the garbage collector so that it doesn't unload its references" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::NewProp_ImportContext_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::NewProp_ImportContext = { "ImportContext", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUsdSceneImportContextContainer, ImportContext), Z_Construct_UScriptStruct_FUSDSceneImportContext, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::NewProp_ImportContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::NewProp_ImportContext_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::NewProp_ImportContext,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUsdSceneImportContextContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::ClassParams = {
		&UDEPRECATED_UUsdSceneImportContextContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::PropPointers),
		0,
		0x021002A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUsdSceneImportContextContainer, 2018980923);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUsdSceneImportContextContainer>()
	{
		return UDEPRECATED_UUsdSceneImportContextContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUsdSceneImportContextContainer(Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer, &UDEPRECATED_UUsdSceneImportContextContainer::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUsdSceneImportContextContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUsdSceneImportContextContainer);
	void UDEPRECATED_UUSDImporter::StaticRegisterNativesUDEPRECATED_UUSDImporter()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporter_NoRegister()
	{
		return UDEPRECATED_UUSDImporter::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDImporter.h" },
		{ "ModuleRelativePath", "Public/USDImporter.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDImporter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::ClassParams = {
		&UDEPRECATED_UUSDImporter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x021002A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDImporter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDImporter, 2159735810);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDImporter>()
	{
		return UDEPRECATED_UUSDImporter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDImporter(Z_Construct_UClass_UDEPRECATED_UUSDImporter, &UDEPRECATED_UUSDImporter::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDImporter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDImporter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
