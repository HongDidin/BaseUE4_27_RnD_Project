// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStageImporter/Public/USDStageImportOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDStageImportOptions() {}
// Cross Module References
	USDSTAGEIMPORTER_API UEnum* Z_Construct_UEnum_USDStageImporter_EReplaceAssetPolicy();
	UPackage* Z_Construct_UPackage__Script_USDStageImporter();
	USDSTAGEIMPORTER_API UEnum* Z_Construct_UEnum_USDStageImporter_EReplaceActorPolicy();
	USDSTAGEIMPORTER_API UClass* Z_Construct_UClass_UUsdStageImportOptions_NoRegister();
	USDSTAGEIMPORTER_API UClass* Z_Construct_UClass_UUsdStageImportOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	USDCLASSES_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions();
// End Cross Module References
	static UEnum* EReplaceAssetPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDStageImporter_EReplaceAssetPolicy, Z_Construct_UPackage__Script_USDStageImporter(), TEXT("EReplaceAssetPolicy"));
		}
		return Singleton;
	}
	template<> USDSTAGEIMPORTER_API UEnum* StaticEnum<EReplaceAssetPolicy>()
	{
		return EReplaceAssetPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EReplaceAssetPolicy(EReplaceAssetPolicy_StaticEnum, TEXT("/Script/USDStageImporter"), TEXT("EReplaceAssetPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDStageImporter_EReplaceAssetPolicy_Hash() { return 1603999017U; }
	UEnum* Z_Construct_UEnum_USDStageImporter_EReplaceAssetPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDStageImporter();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EReplaceAssetPolicy"), 0, Get_Z_Construct_UEnum_USDStageImporter_EReplaceAssetPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EReplaceAssetPolicy::Append", (int64)EReplaceAssetPolicy::Append },
				{ "EReplaceAssetPolicy::Replace", (int64)EReplaceAssetPolicy::Replace },
				{ "EReplaceAssetPolicy::Ignore", (int64)EReplaceAssetPolicy::Ignore },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Append.Comment", "/** Create new assets with numbered suffixes */" },
				{ "Append.Name", "EReplaceAssetPolicy::Append" },
				{ "Append.ToolTip", "Create new assets with numbered suffixes" },
				{ "BlueprintType", "true" },
				{ "Ignore.Comment", "/** Ignores the new asset and keeps the existing asset */" },
				{ "Ignore.Name", "EReplaceAssetPolicy::Ignore" },
				{ "Ignore.ToolTip", "Ignores the new asset and keeps the existing asset" },
				{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
				{ "Replace.Comment", "/** Replaces existing asset with new asset */" },
				{ "Replace.Name", "EReplaceAssetPolicy::Replace" },
				{ "Replace.ToolTip", "Replaces existing asset with new asset" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDStageImporter,
				nullptr,
				"EReplaceAssetPolicy",
				"EReplaceAssetPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EReplaceActorPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_USDStageImporter_EReplaceActorPolicy, Z_Construct_UPackage__Script_USDStageImporter(), TEXT("EReplaceActorPolicy"));
		}
		return Singleton;
	}
	template<> USDSTAGEIMPORTER_API UEnum* StaticEnum<EReplaceActorPolicy>()
	{
		return EReplaceActorPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EReplaceActorPolicy(EReplaceActorPolicy_StaticEnum, TEXT("/Script/USDStageImporter"), TEXT("EReplaceActorPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_USDStageImporter_EReplaceActorPolicy_Hash() { return 1994439542U; }
	UEnum* Z_Construct_UEnum_USDStageImporter_EReplaceActorPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_USDStageImporter();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EReplaceActorPolicy"), 0, Get_Z_Construct_UEnum_USDStageImporter_EReplaceActorPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EReplaceActorPolicy::Append", (int64)EReplaceActorPolicy::Append },
				{ "EReplaceActorPolicy::Replace", (int64)EReplaceActorPolicy::Replace },
				{ "EReplaceActorPolicy::UpdateTransform", (int64)EReplaceActorPolicy::UpdateTransform },
				{ "EReplaceActorPolicy::Ignore", (int64)EReplaceActorPolicy::Ignore },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Append.Comment", "/** Spawn new actors and components alongside the existing ones */" },
				{ "Append.Name", "EReplaceActorPolicy::Append" },
				{ "Append.ToolTip", "Spawn new actors and components alongside the existing ones" },
				{ "BlueprintType", "true" },
				{ "Ignore.Comment", "/** Ignore any conflicting new assets and components, keeping the old ones */" },
				{ "Ignore.Name", "EReplaceActorPolicy::Ignore" },
				{ "Ignore.ToolTip", "Ignore any conflicting new assets and components, keeping the old ones" },
				{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
				{ "Replace.Comment", "/** Replaces existing actors and components with new ones */" },
				{ "Replace.Name", "EReplaceActorPolicy::Replace" },
				{ "Replace.ToolTip", "Replaces existing actors and components with new ones" },
				{ "UpdateTransform.Comment", "/** Update transforms on existing actors but do not replace actors or components */" },
				{ "UpdateTransform.Name", "EReplaceActorPolicy::UpdateTransform" },
				{ "UpdateTransform.ToolTip", "Update transforms on existing actors but do not replace actors or components" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_USDStageImporter,
				nullptr,
				"EReplaceActorPolicy",
				"EReplaceActorPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUsdStageImportOptions::StaticRegisterNativesUUsdStageImportOptions()
	{
	}
	UClass* Z_Construct_UClass_UUsdStageImportOptions_NoRegister()
	{
		return UUsdStageImportOptions::StaticClass();
	}
	struct Z_Construct_UClass_UUsdStageImportOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportActors_MetaData[];
#endif
		static void NewProp_bImportActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportGeometry_MetaData[];
#endif
		static void NewProp_bImportGeometry_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportGeometry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportSkeletalAnimations_MetaData[];
#endif
		static void NewProp_bImportSkeletalAnimations_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportSkeletalAnimations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bImportMaterials_MetaData[];
#endif
		static void NewProp_bImportMaterials_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bImportMaterials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PurposesToImport_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PurposesToImport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderContextToImport_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RenderContextToImport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ImportTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideStageOptions_MetaData[];
#endif
		static void NewProp_bOverrideStageOptions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideStageOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StageOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReuseIdenticalAssets_MetaData[];
#endif
		static void NewProp_bReuseIdenticalAssets_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReuseIdenticalAssets;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExistingActorPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistingActorPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExistingActorPolicy;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExistingAssetPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExistingAssetPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExistingAssetPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrimPathFolderStructure_MetaData[];
#endif
		static void NewProp_bPrimPathFolderStructure_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrimPathFolderStructure;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCollapse_MetaData[];
#endif
		static void NewProp_bCollapse_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCollapse;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInterpretLODs_MetaData[];
#endif
		static void NewProp_bInterpretLODs_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInterpretLODs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdStageImportOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDStageImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDStageImportOptions.h" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors_MetaData[] = {
		{ "Category", "DataToImport" },
		{ "DisplayName", "Actors" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bImportActors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors = { "bImportActors", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry_MetaData[] = {
		{ "Category", "DataToImport" },
		{ "DisplayName", "Geometry" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bImportGeometry = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry = { "bImportGeometry", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations_MetaData[] = {
		{ "Category", "DataToImport" },
		{ "Comment", "/** Whether to try importing UAnimSequence skeletal animation assets for each encountered UsdSkelAnimQuery */" },
		{ "DisplayName", "Skeletal Animations" },
		{ "EditCondition", "bImportGeometry" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Whether to try importing UAnimSequence skeletal animation assets for each encountered UsdSkelAnimQuery" },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bImportSkeletalAnimations = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations = { "bImportSkeletalAnimations", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials_MetaData[] = {
		{ "Category", "DataToImport" },
		{ "DisplayName", "Materials & Textures" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bImportMaterials = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials = { "bImportMaterials", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_PurposesToImport_MetaData[] = {
		{ "Bitmask", "" },
		{ "BitmaskEnum", "EUsdPurpose" },
		{ "Category", "USD options" },
		{ "Comment", "/** Only import prims with these specific purposes from the USD file */" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Only import prims with these specific purposes from the USD file" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_PurposesToImport = { "PurposesToImport", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageImportOptions, PurposesToImport), METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_PurposesToImport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_PurposesToImport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_RenderContextToImport_MetaData[] = {
		{ "Category", "USD options" },
		{ "Comment", "/** Specifies which set of shaders to use, defaults to universal. */" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Specifies which set of shaders to use, defaults to universal." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_RenderContextToImport = { "RenderContextToImport", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageImportOptions, RenderContextToImport), METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_RenderContextToImport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_RenderContextToImport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ImportTime_MetaData[] = {
		{ "Category", "USD options" },
		{ "Comment", "/** Time to evaluate the USD Stage for import */" },
		{ "DisplayName", "Time" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Time to evaluate the USD Stage for import" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ImportTime = { "ImportTime", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageImportOptions, ImportTime), METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ImportTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ImportTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions_MetaData[] = {
		{ "Category", "USD options" },
		{ "Comment", "/** Whether to use the specified StageOptions instead of the stage's own settings */" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Whether to use the specified StageOptions instead of the stage's own settings" },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bOverrideStageOptions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions = { "bOverrideStageOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_StageOptions_MetaData[] = {
		{ "Category", "USD options" },
		{ "Comment", "/** Custom StageOptions to use for the stage */" },
		{ "EditCondition", "bOverrideStageOptions" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Custom StageOptions to use for the stage" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_StageOptions = { "StageOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageImportOptions, StageOptions), Z_Construct_UScriptStruct_FUsdStageOptions, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_StageOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_StageOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets_MetaData[] = {
		{ "Category", "Collision" },
		{ "Comment", "/**\n\x09 * If enabled, whenever two different prims import into identical assets, only one of those assets will be kept and reused.\n\x09 */" },
		{ "EditCondition", "bImportGeometry" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "If enabled, whenever two different prims import into identical assets, only one of those assets will be kept and reused." },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bReuseIdenticalAssets = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets = { "bReuseIdenticalAssets", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy_MetaData[] = {
		{ "Category", "Collision" },
		{ "Comment", "/** What should happen when imported actors and components try to overwrite existing actors and components */" },
		{ "EditCondition", "bImportActors" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "What should happen when imported actors and components try to overwrite existing actors and components" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy = { "ExistingActorPolicy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageImportOptions, ExistingActorPolicy), Z_Construct_UEnum_USDStageImporter_EReplaceActorPolicy, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData[] = {
		{ "Category", "Collision" },
		{ "Comment", "/** What should happen when imported assets try to overwrite existing assets */" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "What should happen when imported assets try to overwrite existing assets" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy = { "ExistingAssetPolicy", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageImportOptions, ExistingAssetPolicy), Z_Construct_UEnum_USDStageImporter_EReplaceAssetPolicy, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure_MetaData[] = {
		{ "Category", "Processing" },
		{ "Comment", "/**\n\x09 * When enabled, assets will be imported into a content folder structure according to their prim path. When disabled,\n\x09 * assets are imported into content folders according to asset type (e.g. 'Materials', 'StaticMeshes', etc).\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "When enabled, assets will be imported into a content folder structure according to their prim path. When disabled,\nassets are imported into content folders according to asset type (e.g. 'Materials', 'StaticMeshes', etc)." },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bPrimPathFolderStructure = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure = { "bPrimPathFolderStructure", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse_MetaData[] = {
		{ "Category", "Processing" },
		{ "Comment", "/** Attempt to combine assets and components whenever possible */" },
		{ "DisplayName", "Collapse assets and components" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "Attempt to combine assets and components whenever possible" },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bCollapse = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse = { "bCollapse", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs_MetaData[] = {
		{ "Category", "Processing" },
		{ "Comment", "/** When true, if a prim has a \"LOD\" variant set with variants named \"LOD0\", \"LOD1\", etc. where each contains a UsdGeomMesh, the importer will attempt to parse the meshes as separate LODs of a single UStaticMesh. When false, only the selected variant will be parsed as LOD0 of the UStaticMesh.  */" },
		{ "DisplayName", "Interpret LOD variant sets" },
		{ "EditCondition", "bImportGeometry" },
		{ "ModuleRelativePath", "Public/USDStageImportOptions.h" },
		{ "ToolTip", "When true, if a prim has a \"LOD\" variant set with variants named \"LOD0\", \"LOD1\", etc. where each contains a UsdGeomMesh, the importer will attempt to parse the meshes as separate LODs of a single UStaticMesh. When false, only the selected variant will be parsed as LOD0 of the UStaticMesh." },
	};
#endif
	void Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs_SetBit(void* Obj)
	{
		((UUsdStageImportOptions*)Obj)->bInterpretLODs = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs = { "bInterpretLODs", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUsdStageImportOptions), &Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdStageImportOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportGeometry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportSkeletalAnimations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bImportMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_PurposesToImport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_RenderContextToImport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ImportTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bOverrideStageOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_StageOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bReuseIdenticalAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingActorPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_ExistingAssetPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bPrimPathFolderStructure,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bCollapse,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageImportOptions_Statics::NewProp_bInterpretLODs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdStageImportOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdStageImportOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdStageImportOptions_Statics::ClassParams = {
		&UUsdStageImportOptions::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdStageImportOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdStageImportOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageImportOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdStageImportOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdStageImportOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdStageImportOptions, 4154532379);
	template<> USDSTAGEIMPORTER_API UClass* StaticClass<UUsdStageImportOptions>()
	{
		return UUsdStageImportOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdStageImportOptions(Z_Construct_UClass_UUsdStageImportOptions, &UUsdStageImportOptions::StaticClass, TEXT("/Script/USDStageImporter"), TEXT("UUsdStageImportOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdStageImportOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
