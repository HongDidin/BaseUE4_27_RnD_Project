// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWorld;
class AActor;
#ifdef USDEXPORTER_USDConversionBlueprintLibrary_generated_h
#error "USDConversionBlueprintLibrary.generated.h already included, missing '#pragma once' in USDConversionBlueprintLibrary.h"
#endif
#define USDEXPORTER_USDConversionBlueprintLibrary_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddPayload); \
	DECLARE_FUNCTION(execInsertSubLayer); \
	DECLARE_FUNCTION(execMakePathRelativeToLayer); \
	DECLARE_FUNCTION(execGetActorsToConvert);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddPayload); \
	DECLARE_FUNCTION(execInsertSubLayer); \
	DECLARE_FUNCTION(execMakePathRelativeToLayer); \
	DECLARE_FUNCTION(execGetActorsToConvert);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdConversionBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UUsdConversionBlueprintLibrary, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdConversionBlueprintLibrary)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUUsdConversionBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UUsdConversionBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UUsdConversionBlueprintLibrary, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdConversionBlueprintLibrary)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdConversionBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdConversionBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdConversionBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdConversionBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdConversionBlueprintLibrary(UUsdConversionBlueprintLibrary&&); \
	NO_API UUsdConversionBlueprintLibrary(const UUsdConversionBlueprintLibrary&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdConversionBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdConversionBlueprintLibrary(UUsdConversionBlueprintLibrary&&); \
	NO_API UUsdConversionBlueprintLibrary(const UUsdConversionBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdConversionBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdConversionBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdConversionBlueprintLibrary)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_10_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UUsdConversionBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDConversionBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
