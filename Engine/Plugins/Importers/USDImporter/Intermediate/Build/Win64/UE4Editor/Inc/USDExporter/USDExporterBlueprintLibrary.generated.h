// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AInstancedFoliageActor;
class UFoliageType;
class ULevel;
struct FTransform;
class UObject;
#ifdef USDEXPORTER_USDExporterBlueprintLibrary_generated_h
#error "USDExporterBlueprintLibrary.generated.h already included, missing '#pragma once' in USDExporterBlueprintLibrary.h"
#endif
#define USDEXPORTER_USDExporterBlueprintLibrary_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetInstanceTransforms); \
	DECLARE_FUNCTION(execGetSource); \
	DECLARE_FUNCTION(execGetUsedFoliageTypes); \
	DECLARE_FUNCTION(execGetInstancedFoliageActorForLevel);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetInstanceTransforms); \
	DECLARE_FUNCTION(execGetSource); \
	DECLARE_FUNCTION(execGetUsedFoliageTypes); \
	DECLARE_FUNCTION(execGetInstancedFoliageActorForLevel);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdExporterBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UUsdExporterBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdExporterBlueprintLibrary)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUUsdExporterBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UUsdExporterBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdExporterBlueprintLibrary)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdExporterBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdExporterBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdExporterBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdExporterBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdExporterBlueprintLibrary(UUsdExporterBlueprintLibrary&&); \
	NO_API UUsdExporterBlueprintLibrary(const UUsdExporterBlueprintLibrary&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdExporterBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdExporterBlueprintLibrary(UUsdExporterBlueprintLibrary&&); \
	NO_API UUsdExporterBlueprintLibrary(const UUsdExporterBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdExporterBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdExporterBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdExporterBlueprintLibrary)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_18_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UUsdExporterBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_USDExporterBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
