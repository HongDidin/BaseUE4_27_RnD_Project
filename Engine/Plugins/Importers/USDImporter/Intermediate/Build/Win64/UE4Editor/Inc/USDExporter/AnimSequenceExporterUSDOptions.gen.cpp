// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/AnimSequenceExporterUSDOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimSequenceExporterUSDOptions() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UAnimSequenceExporterUSDOptions_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UAnimSequenceExporterUSDOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	USDCLASSES_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions();
	USDEXPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions();
// End Cross Module References
	void UAnimSequenceExporterUSDOptions::StaticRegisterNativesUAnimSequenceExporterUSDOptions()
	{
	}
	UClass* Z_Construct_UClass_UAnimSequenceExporterUSDOptions_NoRegister()
	{
		return UAnimSequenceExporterUSDOptions::StaticClass();
	}
	struct Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StageOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportPreviewMesh_MetaData[];
#endif
		static void NewProp_bExportPreviewMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportPreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMeshOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviewMeshOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Options for exporting skeletal mesh animations to USD format.\n */" },
		{ "IncludePath", "AnimSequenceExporterUSDOptions.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/AnimSequenceExporterUSDOptions.h" },
		{ "ToolTip", "Options for exporting skeletal mesh animations to USD format." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_StageOptions_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** Export options to use for the layer where the animation is emitted */" },
		{ "ModuleRelativePath", "Public/AnimSequenceExporterUSDOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Export options to use for the layer where the animation is emitted" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_StageOptions = { "StageOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAnimSequenceExporterUSDOptions, StageOptions), Z_Construct_UScriptStruct_FUsdStageOptions, METADATA_PARAMS(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_StageOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_StageOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** Whether to also export the skeletal mesh data of the preview mesh */" },
		{ "ModuleRelativePath", "Public/AnimSequenceExporterUSDOptions.h" },
		{ "ToolTip", "Whether to also export the skeletal mesh data of the preview mesh" },
	};
#endif
	void Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh_SetBit(void* Obj)
	{
		((UAnimSequenceExporterUSDOptions*)Obj)->bExportPreviewMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh = { "bExportPreviewMesh", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAnimSequenceExporterUSDOptions), &Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_PreviewMeshOptions_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** Export options to use for the preview mesh, if enabled */" },
		{ "EditCondition", "bExportPreviewMesh" },
		{ "ModuleRelativePath", "Public/AnimSequenceExporterUSDOptions.h" },
		{ "ToolTip", "Export options to use for the preview mesh, if enabled" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_PreviewMeshOptions = { "PreviewMeshOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAnimSequenceExporterUSDOptions, PreviewMeshOptions), Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions, METADATA_PARAMS(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_PreviewMeshOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_PreviewMeshOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_StageOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_bExportPreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::NewProp_PreviewMeshOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnimSequenceExporterUSDOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::ClassParams = {
		&UAnimSequenceExporterUSDOptions::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnimSequenceExporterUSDOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnimSequenceExporterUSDOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnimSequenceExporterUSDOptions, 1333662295);
	template<> USDEXPORTER_API UClass* StaticClass<UAnimSequenceExporterUSDOptions>()
	{
		return UAnimSequenceExporterUSDOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnimSequenceExporterUSDOptions(Z_Construct_UClass_UAnimSequenceExporterUSDOptions, &UAnimSequenceExporterUSDOptions::StaticClass, TEXT("/Script/USDExporter"), TEXT("UAnimSequenceExporterUSDOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnimSequenceExporterUSDOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
