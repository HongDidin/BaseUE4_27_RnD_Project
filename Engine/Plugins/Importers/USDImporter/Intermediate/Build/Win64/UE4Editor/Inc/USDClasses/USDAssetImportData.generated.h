// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDCLASSES_USDAssetImportData_generated_h
#error "USDAssetImportData.generated.h already included, missing '#pragma once' in USDAssetImportData.h"
#endif
#define USDCLASSES_USDAssetImportData_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdAssetImportData(); \
	friend struct Z_Construct_UClass_UUsdAssetImportData_Statics; \
public: \
	DECLARE_CLASS(UUsdAssetImportData, UAssetImportData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDClasses"), USDCLASSES_API) \
	DECLARE_SERIALIZER(UUsdAssetImportData) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUUsdAssetImportData(); \
	friend struct Z_Construct_UClass_UUsdAssetImportData_Statics; \
public: \
	DECLARE_CLASS(UUsdAssetImportData, UAssetImportData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDClasses"), USDCLASSES_API) \
	DECLARE_SERIALIZER(UUsdAssetImportData) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDCLASSES_API UUsdAssetImportData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdAssetImportData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDCLASSES_API, UUsdAssetImportData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdAssetImportData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDCLASSES_API UUsdAssetImportData(UUsdAssetImportData&&); \
	USDCLASSES_API UUsdAssetImportData(const UUsdAssetImportData&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDCLASSES_API UUsdAssetImportData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDCLASSES_API UUsdAssetImportData(UUsdAssetImportData&&); \
	USDCLASSES_API UUsdAssetImportData(const UUsdAssetImportData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDCLASSES_API, UUsdAssetImportData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdAssetImportData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdAssetImportData)


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_10_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDCLASSES_API UClass* StaticClass<class UUsdAssetImportData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDClasses_Public_USDAssetImportData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
